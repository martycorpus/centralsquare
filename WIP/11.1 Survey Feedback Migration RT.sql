/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Survey Feedback Object Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 11.1 Survey Feedback Migration PB.sql

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data in the Local Database
---------------------------------------------------------------------------------

USE Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_Prod', 'Account', 'yes' ;
EXEC SF_Refresh 'MC_Tritech_Prod', 'Contact', 'yes' ;

USE Superion_FUllSB
EXEC SF_Refresh 'RT_Superion_FullSB', 'Contact', 'yes'
EXEC SF_Refresh 'RT_Superion_FullSB', 'RecordType', 'yes' ;
EXEC SF_Refresh 'RT_Superion_FullSB', 'Account', 'yes' ;

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Survey_Feedback__c_PreLoad_RT') 
DROP TABLE Survey_Feedback__c_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.[ID] as Legacy_Id__c,
	'TriTech' as Legacy_Source_System__c,
	'true' as Migrated_Record__c,
	RT.ID as RecordTypeID,
	a.NPS_Score__c as Legacy_NPS_Score__c,
	a.NPS_Survey_Contact__c as NPS_Survey_Contact_Original,
	Cont.ID as Contact2__c,
	a.Survey_Notes__c as Customer_Comments__c,
	a.ID as Account_Original__c,
	Acct.ID as Account__c,
	a.Survey_Date__c as CreatedDate
INTO Survey_Feedback__c_PreLoad_RT
FROM Tritech_PROD.dbo.Account a
LEFT OUTER JOIN Superion_FULLSB.dbo.RecordType RT on 'NPS' = RT.[Name]							----> Record Type
LEFT OUTER JOIN Superion_FULLSB.dbo.Contact Cont ON a.NPS_Survey_Contact__c = Cont.Legacy_ID__c ----> Contact
LEFT OUTER JOIN Superion_FULLSB.dbo.Account Acct ON a.ID = Acct.LegacySFDCAccountId__c			----> Account
WHERE a.NPS_Score__c is not null

---------------------------------------------------------------------------------
-- Drop Load tables if exist
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Survey_Feedback__c_Load_RT') 
DROP TABLE Survey_Feedback__c_Load_RT;

SELECT *
INTO Staging_SB.dbo.Survey_Feedback__c_Load_RT
FROM Staging_SB.dbo.Survey_Feedback__c_PreLoad_RT

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE  Staging_SB.dbo.Survey_Feedback__c_Load_PB
ADD [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB

EXEC SF_BulkOps 'upsert:bulkapi,batchsize(5000)', 'RT_Superion_FullSB', 'Survey_Feedback__c_Load_RT', 'Legacy_Id__c';

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------
SELECT ERROR, count(*)
FROM Survey_Feedback__c_Load_PB
GROUP BY ERROR

select * from Account_Load_PB where error not like '%success%'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE Superion_FUllSB

EXEC SF_Refresh 'RT_Superion_FullSB', 'Survey_Feedback', 'yes'
