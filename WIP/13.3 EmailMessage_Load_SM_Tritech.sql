/*
-- Author     : Shivani Mogullapalli
-- Date       : 06/11/2018
-- Description: Migrate the Case data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0827
Scope:
Migrate all records that relate to a migrated case and records that relate to migrated tasks.
*/

--use Tritech_Prod
--EXEC SF_Refresh 'MC_Tritech_PROD','EmailMessage','yes'
--EXEC SF_Refresh 'MC_TRITECH_PROD','EmailMEssageRelation','Yes'

-- use superion_FullSb
--EXEC SF_refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
--EXEC SF_refresh 'Sl_Superion_Fullsb','Task','yes'
/* Turn off the Email message master process builder */

-- select count(*) from Tritech_PROD.dbo.EmailMessage
-- 198852
use staging_sb

go

-- drop table EmailMessage_Tritech_SFDC_Preload
declare @defaultuser varchar(18)
select @defaultuser = id  from [Superion_FULLSB].dbo.[user]
							where name like 'Superion API'

	SELECT		 
          		ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_email.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
				,ActivityID_orig						= tr_email.[ActivityId]
				,ActivityId=target_task.id
				,BccAddress_orig						 = tr_email.[BccAddress]
				,BCcAddress								 = replace(cast([BccAddress] as nvarchar(max)),'@','@dummy.')
				,CCAddress_orig							 = tr_email.[CcAddress]
				,CcAddress								 =	replace(cast([CcAddress] as nvarchar(max)),'@','@dummy.')
				,CreatedById_orig						= tr_email.[CreatedById]
				,CreatedById							= iif(Tar_CreateID.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Tar_createID.id )
				,CreatedDate							= tr_email.[CreatedDate]
				,FromAddress_orig						= tr_email.[FromAddress]
				,FromAddress							= replace([FromAddress],'@','@dummy.')
				,FromName								= tr_email.[FromName]
			
														  --,tr_email.[HasAttachment]
				,Headers								= tr_email.[Headers]
				,HTMLBody								= tr_email.[HtmlBody]
				,id_orig								= tr_email.[Id]
				,Incoming								= tr_email.[Incoming]
				,IsClientManaged_orig						= tr_email.[IsClientManaged]
				,IsClientManaged						='True'
														  --,tr_email.[IsDeleted]
				,IsExternallyVisible					= tr_email.[IsExternallyVisible]
														  --,tr_email.[LastModifiedById]
														  --,tr_email.[LastModifiedDate]
				,MessageDate							= tr_email.[MessageDate]
				,MessageIdentifier						= tr_email.[MessageIdentifier]
						,ParentID_orig					=	tr_email.[ParentId]
						,ParentID						=  Target_case.id
				,RelatedToId_orig							= tr_email.[RelatedToId]
				,ReplyToEmailMessageId_orig					= tr_email.[ReplyToEmailMessageId]
				,ReplyToEmailMessageId=iif(tr_email.ReplyToEmailMessageId is null,null,'Legacy_Id__C:' + tr_email.ReplyToEmailMessageId)
				,Status							= tr_email.[Status]
				
				,Subject								= tr_email.[Subject]
														  --,tr_email.[SystemModstamp]
				,TextBody								= tr_email.[TextBody]
				,ThreadIdentifier						= tr_email.[ThreadIdentifier]
				,ToAddress_orig							= tr_email.[ToAddress]
				,ToAddress								= replace(cast(tr_email.[ToAddress] as nvarchar(max)),'@','@dummy.')
				--,ValidatedFromAddress					= replace(cast(tr_email.[ValidatedFromAddress] as nvarchar(max)),'@','@dummy.')
					,tt_emr_EmailMessageId=tt_emr.EmailMessageId	
					,tt_emr_EmailMessagerelationId=tt_emr.id	
					,RelationId_orig=tt_emr.RelationId	
					,RelationId_sup=target_cnt.id
					,RelationType_orig=tt_emr.RelationType
					,sp_task_whoid=target_task.whoid 
					,tt_task_whoid=tt_task.whoid
					 into EmailMessage_Tritech_SFDC_Preload
 FROM [Tritech_PROD].[dbo].[EmailMessage] tr_email
  inner join Superion_FULLSB.dbo.[case] target_case
  on target_Case.legacy_ID__c= tr_Email.ParentId
  -----------------------------------------------------------------------------------
  left outer join Tritech_PROD.dbo.[EmailMessageRelation] tt_emr
  on tr_email.id=tt_emr.EmailMessageId and tt_emr.RelationObjectType='Contact'
  -----------------------------------------------------------------------------------
  left outer join  Superion_FULLSB.dbo.contact target_cnt
  on target_cnt.legacy_id__c= tt_emr.RelationId
  -----------------------------------------------------------------------------------
  left outer join  Tritech_PROD.dbo.task tt_task
  on tt_task.id= tr_email.ActivityId
 
  -----------------------------------------------------------------------------------
  left outer join  Superion_FULLSB.dbo.task target_task
  on target_task.legacy_id__c= tr_email.ActivityId
  -----------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Tar_CreateID
  on Tar_CreateID.Legacy_Tritech_Id__c=tr_email.CreatedById
  and Tar_CreateId.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------
  left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_email.createdbyId
  ------------------------------------------------------------------------------------
  where 1=1
  
  ;

  --(173383 row(s) affected)



  select  * from EmailMessage_Tritech_SFDC_Preload 
   where parentid_orig='5001E00001GlnNxQAJ'
  
  select * from EmailMessage_Tritech_SFDC_preload   where activityid is null and activityid_orig is not null ;--1

  select count(*) from EmailMessage_Tritech_SFDC_Preload;--173383

  select * from EmailMessage_Tritech_SFDC_Preload
where sp_task_whoid is null and ActivityID_orig is not null and tt_task_whoid is not null;




  -- check for the duplicates.
  select legacy_id__c ,count(*) from EmailMessage_Tritech_SFDC_Preload
  group by legacy_id__c
  having count(*)>1;--0

  -- drop table EmailMessage_Tritech_SFDC_load
  select  
  * 
  into EmailMessage_Tritech_SFDC_load
  from EmailMessage_Tritech_SFDC_Preload
  where 1=1
  and ReplyToEmailMessageId_orig is null
   -- order by parentid_orig,MessageDate
  
    --(142778 row(s) affected)

	select count(*) from EmailMessage_Tritech_SFDC_load;
  
  
--: Run batch program to create EmailMessage related to cases
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'EmailMessage_Tritech_SFDC_load' 

--check column names
  

/*
Salesforce object EmailMessage does not contain column ActivityID_orig
Salesforce object EmailMessage does not contain column BccAddress_orig
Salesforce object EmailMessage does not contain column CCAddress_orig
Salesforce object EmailMessage does not contain column CreatedById_orig
Salesforce object EmailMessage does not contain column FromAddress_orig
Salesforce object EmailMessage does not contain column id_orig
Salesforce object EmailMessage does not contain column IsClientManaged_orig
Salesforce object EmailMessage does not contain column ParentID_orig
Salesforce object EmailMessage does not contain column RelatedToId_orig
Salesforce object EmailMessage does not contain column ReplyToEmailMessageId_orig
Salesforce object EmailMessage does not contain column ToAddress_orig
Salesforce object EmailMessage does not contain column tt_emr_EmailMessageId
Salesforce object EmailMessage does not contain column tt_emr_EmailMessagerelationId
Salesforce object EmailMessage does not contain column RelationId_orig
Salesforce object EmailMessage does not contain column RelationId_sup
Salesforce object EmailMessage does not contain column RelationType_orig
Salesforce object EmailMessage does not contain column sp_task_whoid
Salesforce object EmailMessage does not contain column tt_task_whoid
*/

select error, count(*) from EmailMessage_Tritech_SFDC_load
group by error;

-------------------------------------------------------------------
--drop table EmailMessage_Tritech_SFDC_load_with_Series
select *, ntile(4) over(order by parentid,messageDate) series
into EmailMessage_Tritech_SFDC_load_with_Series
from EmailMessage_Tritech_SFDC_load a;--142778
--4 mins
-------------------------------------------------------------------------------------------------------

Select Series,Count(*) from EmailMessage_Tritech_SFDC_load_with_Series
group by Series

-------------------------------------------------------------------------------------------------------

--Series1

select * into EmailMessage_Tritech_SFDC_load_with_Series_1 from EmailMessage_Tritech_SFDC_load_with_Series
where series=1;--35695

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_with_Series_1'

select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_1
group by error

-------------------------------------------------------------------------------------------------------

--Series2

select * into EmailMessage_Tritech_SFDC_load_with_Series_2 from EmailMessage_Tritech_SFDC_load_with_Series
where series=2;--35695

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_with_Series_2'

select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_2
group by error

-------------------------------------------------------------------------------------------------------

--Series3

select * into EmailMessage_Tritech_SFDC_load_with_Series_3 from EmailMessage_Tritech_SFDC_load_with_Series
where series=3

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_with_Series_3'

select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_3
group by error;

select * 
into EmailMessage_Tritech_SFDC_load_with_Series_3_errors
from EmailMessage_Tritech_SFDC_load_with_Series_3
where error<>'Operation Successful.';

select * 
into EmailMessage_Tritech_SFDC_load_with_Series_3_errors_bkp
from EmailMessage_Tritech_SFDC_load_with_Series_3
where error<>'Operation Successful.';

--update EmailMessage_Tritech_SFDC_load_with_Series_3_errors
 set toaddress=substring(toaddress,1,3979)
where error like 'Value too large max length:4000 Your length: %'

select * from EmailMessage_Tritech_SFDC_load_with_Series_3_errors;

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_with_Series_3_errors'

select * 
--delete
from EmailMessage_Tritech_SFDC_load_with_Series_3
where error<>'Operation Successful.';

--insert into EmailMessage_Tritech_SFDC_load_with_Series_3
select * from EmailMessage_Tritech_SFDC_load_with_Series_3_errors


-------------------------------------------------------------------------------------------------------

--Series4

select * into EmailMessage_Tritech_SFDC_load_with_Series_4 from EmailMessage_Tritech_SFDC_load_with_Series
where series=4

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_with_Series_4'

select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_4
group by error

-------------------------------------------------------------------------------------------------------
select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_1
group by error
;
select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_2
group by error
;
select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_3
group by error
;
select error,count(*) from EmailMessage_Tritech_SFDC_load_with_Series_4
group by error
;
-------------------------------------------------------------------------------------

select *
--update a set a.id=b.id,error='Operation Successful.'
from EmailMessage_Tritech_SFDC_load a
left outer join (select id,legacy_id__C from EmailMessage_Tritech_SFDC_load_with_Series_1
                    where error='Operation Successful.'
					union
					select id,legacy_id__C from EmailMessage_Tritech_SFDC_load_with_Series_2
                    where error='Operation Successful.'
					union
					select id,legacy_id__C from EmailMessage_Tritech_SFDC_load_with_Series_3
                    where error='Operation Successful.'
					union
					select id,legacy_id__C from EmailMessage_Tritech_SFDC_load_with_Series_4
                    where error='Operation Successful.'
					) b
 on a.Legacy_id__c=b.legacy_id__C;--142778

select * from EmailMessage_Tritech_SFDC_load 
where error<> 'Operation Successful.';

select error,count(*) from EmailMessage_Tritech_SFDC_load
group by error;

select id,count(*) from EmailMessage_Tritech_SFDC_load
group by id
having count(*)>1;


--------------------------------------------------
-- drop table EmailMessage_Tritech_SFDC_load_replyto
  select IDENTITY(INT, 1, 1) sort_order,
     * 
  into EmailMessage_Tritech_SFDC_load_replyto
  from EmailMessage_Tritech_SFDC_Preload
  where 1=1
  and ReplyToEmailMessageId_orig is not null
  order by parentid,messagedate
  --(30605 row(s) affected)

  
  select * from EmailMessage_Tritech_SFDC_load_replyto;

--: Run batch program to create EmailMessage related to Cases
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'EmailMessage_Tritech_SFDC_load_replyto' 

--check column names
  
--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_replyto'


select * from EmailMessage_Tritech_SFDC_load_replyto
where error<>'Operation Successful.'


select error, count(*) from EmailMessage_Tritech_SFDC_load_replyto
group by error;


--check EmailMessagerelation


