-------------------------------------------------------------------------------
--- Chatter Feed Item  Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 28, 2018
--- Last Updated: November 28, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Replicate Data 
--------------------------------------------------------------------------------
Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'FeedItem', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'CollaborationGroup', 'yes';

Use Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Case', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'Opportunity', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'Contact', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'Account', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'CollaborationGroup', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes';
--Exec SF_Refresh 'PB_Superion_FULLSB', 'Write_In__c', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'Task', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'EngineeringIssue__c', 'yes';
--Exec SF_Refresh 'PB_Superion_FULLSB', 'Pricing_Request_Form__c', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'Event', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'Procurement_Activity__c', 'yes';


-------------------------------------------------------------------------------------
-- Creating ChatterParent reference 
-----------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='map_ChatterParent') 
DROP TABLE Staging_SB.dbo.map_ChatterParent;

Select * INTO staging_sb.dbo.map_ChatterParent from
(
		-- CASE
		Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.[Case] where legacy_id__c is not null
		UNION
		-- Opportunity
		Select Legacy_Opportunity_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Opportunity where Legacy_Opportunity_ID__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Contact
		Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Contact where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Account
		select LegacySFDCAccountID__c as LegacyID, ID from Superion_FULLSB.dbo.Account where LegacySFDCAccountID__c is not null 
		UNION
		-- Collaboration Group
		Select GroupSource.ID as LegacyID, GroupTarget.ID as ID  
		from Tritech_PROD.dbo.CollaborationGroup GroupSource
		LEFT OUTER JOIN Superion_FULLSB.dbo.CollaborationGroup GroupTarget ON GroupSource.[Name] = GroupTarget.[Name]
		WHERE GroupTarget.ID is NOT NULL
		UNION
		-- User
		Select Legacy_Tritech_Id__c as LegacyID, ID from Superion_FULLSB.dbo.[User] where Legacy_Tritech_Id__c is not null and Migrated_Record__c = 'true'
		UNION
		---- Write_IN
		--Select Legacy_Id__c as LegacyID, ID from Superion_FULLSB.dbo.Write_In__c where legacy_id__c is not null  and Migrated_Record__c = 'true'
		--UNION
		-- Task
		Select Legacy_Id__c as LegacyID, ID from Superion_FULLSB.dbo.Task where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Engineering Issue
		Select Legacy_CRMId__c as LegacyID, ID from Superion_FULLSB.dbo.EngineeringIssue__c where Legacy_CRMId__c is not null
		UNION
		---- Pricing
		--Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Pricing_Request_Form__c where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		--UNION
		-- Demo Request
		Select a.ID as LegacyID, Oppty.ID as ID from Tritech_PROD.dbo.Demo_Request__c a
		INNER JOIN Superion_FULLSB.dbo.Opportunity Oppty ON a.Opportunity__c = Oppty.Legacy_Opportunity_ID__c
		UNION
		-- Event
		Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Event where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		UNION
		--Procurement activity
		Select Legacy_ID__c as Legacyid, ID from Superion_FULLSB.dbo.Procurement_Activity__c where Legacy_id__c is not null and Migrated_Record__c = 'true'
)t

--Drop Index ChatterParent_idx on map_ChatterParent

CREATE INDEX ChatterParent_idx
ON [map_ChatterParent] (LegacyID) 


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedItem_Load_PB') 
DROP TABLE Staging_SB.dbo.FeedItem_Load_PB;


Select
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.Body, 
	a.CreatedByID as CreatedBy_Orig,
	isNull(CreatedBy.ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as CreatedById,
	a.CreatedDate as CreatedDate,
	isNull(a.LinkUrl, '') as LinkURL,
	a.IsRichText as IsRichText,
	a.ParentID as Parent_Orig,
	Parent.Id as ParentID,
	--RelatedRecord.ID as RelatedRecordID,
	--ContentFileName as ContentFileName,
	--a.Revision as Revision,
	isNull(a.Title, '') as Title,
	a.[Type] as [Type],
	a.ID as Legacy_ID__c
	INTO FeedItem_Load_PB
	from TriTech_Prod.dbo.FeedItem a
	---CreatedBy
	LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c
	---Parent
	Inner JOIN Staging_SB.dbo.map_ChatterParent Parent ON a.ParentID = Parent.LegacyID
	WHERE a.[Type] in ('LinkPost', 'TextPost')


---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performan
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.FeedItem_load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Staging_SB
Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'FeedItem_load_PB'

-------------------------------
-------------------------------
-- select * from FeedItem_Load_PB where error not like '%success%' and error not like '%task%'

