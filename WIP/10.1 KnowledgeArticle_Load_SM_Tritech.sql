/*
-- Author     : Shivani Mogullapalli
-- Date       : 06/11/2018
-- Description: Migrate the Case data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0834

Scope:
a. Migrate all the Knowledge Articles.
b. Migrate only the current knowledge Article Version.


*/
use Staging_SB
go 
-- drop table knowledgeArticle_Tritech_SFDC_Preload
SELECT 
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_knowAt.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
													,tr_knowAt.[ArchivedById]
													  ,tr_knowAt.[ArchivedDate]
													  ,tr_knowAt.[ArticleArchivedById]
													  ,tr_knowAt.[ArticleArchivedDate]
													  ,tr_knowAt.[ArticleCaseAttachCount]
													  ,tr_knowAt.[ArticleCreatedById]
													  ,tr_knowAt.[ArticleCreatedDate]
													  ,tr_knowAt.[ArticleNumber]
													  ,tr_knowAt.[ArticleTotalViewCount]
													  ,tr_knowAt.[ArticleType]
													  ,tr_knowAt.[AssignedById]
													  ,tr_knowAt.[AssignedToId]
													  ,tr_knowAt.[AssignmentDate]
													  ,tr_knowAt.[AssignmentDueDate]
													  ,tr_knowAt.[AssignmentNote]
					,Createdbyid_orig					= tr_knowAt.[CreatedById]
					,CreatedById						= Target_Create.ID
					,Createddate						= tr_knowAt.[CreatedDate]
													  ,tr_knowAt.[FirstPublishedDate]
													  ,ID_orig = tr_knowAt.[Id]
													  ,tr_knowAt.[IsDeleted]
													  ,tr_knowAt.[IsLatestVersion]
													  ,tr_knowAt.[IsMasterLanguage]
													  ,tr_knowAt.[IsVisibleInApp]
													  ,tr_knowAt.[IsVisibleInCsp]
													  ,tr_knowAt.[IsVisibleInPkb]
													  ,tr_knowAt.[IsVisibleInPrm]
													  ,tr_knowAt.[KnowledgeArticleId]
													  ,tr_knowAt.[Language]
													  ,tr_knowAt.[LastModifiedById]
													  ,tr_knowAt.[LastModifiedDate]
													  ,tr_knowAt.[LastPublishedDate]
													  ,tr_knowAt.[MasterVersionId]
													  ,OwnerID_orig =tr_knowAt.[OwnerId]
													  ,OwnerID = Target_Owner.ID
													  ,tr_knowAt.[PublishStatus]
													  ,tr_knowAt.[SourceId]
						,Summary					= tr_knowAt.[Summary]
													  ,tr_knowAt.[SystemModstamp]
						,Title						= tr_knowAt.[Title]
						,URLName					= tr_knowAt.[UrlName]
													  ,tr_knowAt.[VersionNumber]
				--into knowledgeArticle_Tritech_SFDC_Preload
  FROM [Tritech_PROD].[dbo].[KnowledgeArticleVersion] Tr_KnowAt
   left join Superion_FULLSB.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=Tr_KnowAt.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=Tr_KnowAt.OwnerID
  and Target_Owner.Legacy_Source_System__c='Tritech'


  select distinct articletype from Tritech_PROD.dbo.KnowledgeArticleVersion