/*
-- Author     : Shivani Mogullapalli
-- Date       : 06/11/2018
-- Description: Migrate the Task data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0380
Scope:
Migrate all tasks that relate to a migrated record on a migrated object and migrated record and IS NOT owned by ActOnSync.

*/

/*
Use Tritech_PROD
EXEC SF_ReplicateIAD 'MC_TRITECH_PROD','task' --,'Yes'
EXEC SF_Replicate 'MC_TRITECH_PROD','EmailMessage'


Use Superion_FULLSB
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Task','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Opportunity','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Account','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Case','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Sales_Request__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Procurement_Activity__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact_Reference_Link__c','yes'
*/
-- select count(*) from tritech_prod.dbo.[Task]
--1671317

-- drop table Task_Tritech_SFDC_Preload
use staging_sb
go
 declare @defaultuser nvarchar(18)
 select @defaultuser = id from Superion_FULLSB.dbo.[user] where name = 'Superion API'
;With CteWhatData as
(
select 
a.LegacySFDCAccountId__c  Legacy_id_orig,
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Account' as What_parent_object_name 
from Superion_FULLSB.dbo.Account a
where 1=1 
and a.LegacySFDCAccountId__c is not null

						union
select
a.Legacy_Opportunity_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Opportunity' as What_parent_object_name 
from Superion_FULLSB.dbo.Opportunity a
where 1=1
and a.Legacy_Opportunity_ID__c is not null

						union 
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Case' as What_parent_object_name 
from Superion_FULLSB.dbo.[case] a
where 1=1
and a.Legacy_ID__c is not null
						union 
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Sales_Request__c a
where 1=1
and a.Legacy_Record_ID__c is not null
			union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Procurement_Activity__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Procurement_Activity__c a
where 1=1
and a.Legacy_ID__c is not null
				union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact_Reference_Link__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Contact_Reference_Link__c a
where 1=1
and a.Legacy_ID__c is not null

) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from Superion_FULLSB.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null				
 )   

SELECT  
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_task.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_task.[Account_Name__c]
														  --,tr_task.[Account_Name_DE__c]
				,AccountId_orig							= tr_task.[AccountId]
				,AccountId								= Tar_ACcount.ID
				,Activity_Type_For_Reporting__c			= tr_task.[Activity_Type_For_Reporting__c]
				,ActivityDate							= tr_task.[ActivityDate]
														  --,tr_task.[Brand__c]
				,CallDisposition						= tr_task.[CallDisposition]
				,CallDurationInSeconds					= tr_task.[CallDurationInSeconds]
				,CallObject								= tr_task.[CallObject]
				,CallType								= tr_task.[CallType]
														  --,tr_task.[Completed_Date_Time__c]
														  --,tr_task.[Contact_Count__c]
				,CreatedById_orig						= tr_task.[CreatedById]
				,legacy_createdby_name=Legacyuser.name
				,CreatedById_target							= Target_Create.ID
				,CreatedById							= iif(Target_Create.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_task.[CreatedDate]
														 -- ,tr_task.[Customer_Risk_Level__c]
				--,Description_orig							= tr_task.[Description]
														  --,tr_task.[DummyTouchField__c]
				,Id_orig								= tr_task.[Id]
				,Isarchieved__orig = tr_task.[IsArchived]
				,IsClosed_orig=tr_task.[IsClosed]
			    ,IsDeleted_orig=    tr_task.[IsDeleted]
														  --,tr_task.[IsHighPriority]
				,IsRecurrence							= tr_task.[IsRecurrence]
				,IsReminderSet							= tr_task.[IsReminderSet]
														  --,tr_task.[LastModifiedById]
														  --,tr_task.[LastModifiedDate]
				,OwnerId_orig							= tr_task.[OwnerId]
				,legacy_owner_name=Legacyuser1.name
				,OwnerId_target							= Target_Owner.ID
				,OwnerId							    = iif(Target_owner.id is null,@defaultuser
																		--iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,Priority								= tr_task.[Priority]
				,RecurrenceActivityId_orig				= tr_task.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_task.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_task.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_task.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_task.[RecurrenceInstance]
				,RecurrenceInterval						= tr_task.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_task.[RecurrenceMonthOfYear]
				,RecurrenceRegeneratedType				= tr_task.[RecurrenceRegeneratedType]
				,RecurrenceStartDateOnly				= tr_task.[RecurrenceStartDateOnly]
				,RecurrenceTimeZoneSidKey				= tr_task.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_task.[RecurrenceType]
				,ReminderDateTime						= tr_task.[ReminderDateTime]
				,Resolution_Notes__c					= tr_task.[Resolution_Notes__c]
														  --,tr_task.[Solution_s__c]
				,Status									= tr_task.[Status]
				,Subject								= tr_task.[Subject]
														  --,tr_task.[SystemModstamp]
				--,Description							= iif(tr_task.[Task_Notes__c] is not null,concat(tr_task.[Createddate],tr_task.[Task_Notes__c]),null)
				,TaskSubtype							= tr_task.[TaskSubtype]
														 -- ,tr_task.[Time_Elapsed__c]
				,Type									= tr_task.[Type]
														  --,tr_task.[WhatCount]
				,whatId_orig							= tr_task.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
				    
														  --,tr_task.[WhoCount]
				,WhoID_orig								= tr_task.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_task.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_task.[Description]
				,Description_Comments_summary__C_orig = tr_task.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_task.[Task_Notes__c]
				--,[Description]  = concat( iif(tr_task.[Description] is not null,'Description::',''),tr_task.description,CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_task.createddate),''),tr_task.[Comments_Summary__c],CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_task.createddate),''),tr_task.[Task_Notes__c]
				--						  )
				--,DESCRIPTION = IIF(tr_task.Description IS NOT NULL
				--							,IIF(tr_task.tASK_nOTES__c IS NOT NULL, 
				--								CONCAT(tr_task.DESCRIPTION,CHAR(13)+char(10),tr_task.CREATEDDATE,' ',tr_task.task_notes__c),tr_task.description),null)
				,DESCRIPTION=concat(isnull(tr_task.Description,''),
				iif(tr_task.Task_Notes__c IS NOT NULL,concat(CHAR(13)+char(10)+cast(tr_task.CREATEDDATE as nvarchar(40))+' ',
				cast(tr_task.Task_Notes__c as nvarchar(max))),''))
               -- ,tr_email_id=tr_email.id
							into Task_Tritech_SFDC_Preload
  FROM [Tritech_PROD].[dbo].[Task] tr_task
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_task.WhatId
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_task.WhoId
  -----------------------------------------------------------------------------------------------------------------------
    left join Superion_FULLSB.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_task.AccountId
--and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_task.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_task.OwnerID
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_task.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_task.OwnerId
    -----------------------------------------------------------------------------------------------------------------------------
   where 1=1
  and  tr_task.IsDeleted='False' and Legacyuser1.name <>'Act-On Sync'
  and (Wt.parent_id IS NOT NULL or Wo.parent_id IS NOT NULL or Tar_Account.ID IS NOT NULL)
  and (tr_task.IsArchived='false' or Wt.parent_id like '500%')
 ;

  --(264354 row(s) affected)

 -- COUNT OF THE TASK RECORDS
select count(*) 
from Task_Tritech_SFDC_Preload
--   264354 row(s) affected)

select * from Task_Tritech_SFDC_Preload
where legacy_id__C='00T8000006ewdzfEAA'


-- CHECK FOR THE DUPLICATES.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_Preload
group by Legacy_ID__c
having count(*)>1;--0

--drop table Task_Tritech_SFDC_load
select * 
into Task_Tritech_SFDC_load
from Task_Tritech_SFDC_Preload
where 1=1
 and (whoid is not null or whatid is not null);
 
--(263642 row(s) affected)

select * 
from Task_Tritech_SFDC_load;

-- LOAD TABLE RECORD COUNT
select count(*) 
from Task_Tritech_SFDC_load
--263642

-- check for the duplicates in the final load table.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_load
group by Legacy_ID__c
having count(*)>1


select * from Task_Tritech_SFDC_load;

--: Run batch program to create task
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'Task_Tritech_SFDC_load' 

--check column names
/*

Salesforce object Task does not contain column Legacy_Source_System__c
Salesforce object Task does not contain column AccountId_orig
Salesforce object Task does not contain column CreatedById_orig
Salesforce object Task does not contain column legacy_createdby_name
Salesforce object Task does not contain column CreatedById_target
Salesforce object Task does not contain column Id_orig
Salesforce object Task does not contain column Isarchieved__orig
Salesforce object Task does not contain column IsClosed_orig
Salesforce object Task does not contain column IsDeleted_orig
Salesforce object Task does not contain column OwnerId_orig
Salesforce object Task does not contain column legacy_owner_name
Salesforce object Task does not contain column OwnerId_target
Salesforce object Task does not contain column RecurrenceActivityId_orig
Salesforce object Task does not contain column whatId_orig
Salesforce object Task does not contain column whatid_parent_object_name
Salesforce object Task does not contain column whatid_legacy_id_orig
Salesforce object Task does not contain column WhoID_orig
Salesforce object Task does not contain column whoId_parent_object_name
Salesforce object Task does not contain column whoId_legacy_id_orig
Salesforce object Task does not contain column Logged_Wellness_Check__c
Salesforce object Task does not contain column Description__orig
Salesforce object Task does not contain column Description_Comments_summary__C_orig
Salesforce object Task does not contain column Description_Task_notes__C_orig
Column AccountId is not insertable into the salesforce object Task

*/



--drop table Task_Tritech_SFDC_load_with_Series
select *, ntile(5) over(order by whatid,whoid) series
into Task_Tritech_SFDC_load_with_Series
from Task_Tritech_SFDC_load a
--2mins
-------------------------------------------------------------------------------------------------------

Select Series,Count(*) from Task_Tritech_SFDC_load_with_Series
group by Series

-------------------------------------------------------------------------------------------------------

--Series1

select * into Task_Tritech_SFDC_load_with_Series_1 from Task_Tritech_SFDC_load_with_Series
where series=1;--52729

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_with_Series_1'

select error,count(*) from Task_Tritech_SFDC_load_with_Series_1
group by error

-------------------------------------------------------------------------------------------------------

--Series2

select * into Task_Tritech_SFDC_load_with_Series_2 from Task_Tritech_SFDC_load_with_Series
where series=2;--52729

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_with_Series_2'

select error,count(*) from Task_Tritech_SFDC_load_with_Series_2
group by error

-------------------------------------------------------------------------------------------------------

--Series3

select * into Task_Tritech_SFDC_load_with_Series_3 from Task_Tritech_SFDC_load_with_Series
where series=3

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_with_Series_3'

select error,count(*) from Task_Tritech_SFDC_load_with_Series_3
group by error

-------------------------------------------------------------------------------------------------------

--Series4

select * into Task_Tritech_SFDC_load_with_Series_4 from Task_Tritech_SFDC_load_with_Series
where series=4

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_with_Series_4'

select error,count(*) from Task_Tritech_SFDC_load_with_Series_4
group by error

-------------------------------------------------------------------------------------------------------

--Series5

select * into Task_Tritech_SFDC_load_with_Series_5 from Task_Tritech_SFDC_load_with_Series
where series=5

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_with_Series_5'

select error,count(*) from Task_Tritech_SFDC_load_with_Series_5
group by error

-------------------------------------------------------------------------------------------------------

select a.id,error=cast(space(255) as nvarchar(255)),Legacy_id__c_orig=a.Legacy_id__c 
into Task_Tritech_SFDC_Delete_recurrence_tasks
from Superion_FULLSB.dbo.[task] a
left outer join Task_Tritech_SFDC_load b
on a.id=b.id
where a.Migrated_Record__c='true'
and b.id is null;--380

--Exec SF_BulkOps 'Delete:batchsize(50)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_Delete_recurrence_tasks'
-------------------------------------------------------------------------------------------------------

select count(*) from Task_Tritech_SFDC_load 
where error<>'Operation Successful.'

--1401 

-- drop table Task_Tritech_SFDC_load_errors
select *
into Task_Tritech_SFDC_load_errors
from Task_Tritech_SFDC_load 
where error <>'Operation Successful.'
--(1401 row(s) affected)

-- drop table Task_Tritech_SFDC_load_errors_bkp
select *
into Task_Tritech_SFDC_load_errors_bkp
from Task_Tritech_SFDC_load 
where error <>'Operation Successful.'

--(1401 row(s) affected)


--select *
-- update a set description = substring(description,1,31599)
-- from Task_Tritech_SFDC_load_errors a where error like 'value too large max length:%' or len(Description)>31999
 

--  (28 row(s) affected)

  select * 
 -- update a set ActivityDate='2099-01-01 00:00:00.0000000'
 from Task_Tritech_SFDC_load_errors a where error like 'Due Date Only: invalid date: Sat Jan 01 00:00:00 GMT 4501%'

 --(1 row(s) affected)



 --Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_errors'


 select error , count(*) from Task_Tritech_SFDC_load_errors
 group by error;

 select * from Task_Tritech_SFDC_load_errors

 select *
 --delete 
 from Task_Tritech_SFDC_load where Legacy_id__c in
 (select Legacy_id__c from Task_Tritech_SFDC_load_errors where error='Operation Successful.')

 --(1401 row(s) affected)

 -- insert into Task_Tritech_SFDC_Load
 select * from Task_Tritech_SFDC_load_errors where error='Operation Successful.'
 --(1401 row(s) affected)

 select error , count(*) from Task_Tritech_SFDC_load
 group by error;

 -- drop table Task_Tritech_SFDC_load_errors1
 select *
 into Task_Tritech_SFDC_load_errors1
 from Task_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'

 --(5 row(s) affected)


 -- drop table Task_Tritech_SFDC_load_errors1_bkp
 select *
 into Task_Tritech_SFDC_load_errors1_bkp
 from Task_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'

 --(5 row(s) affected)


 -- --Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_errors1'

  select error , count(*) from Task_Tritech_SFDC_load_errors1
 group by error;


 select *
 --delete 
 from Task_Tritech_SFDC_load where Legacy_id__c in
 (select Legacy_id__c from Task_Tritech_SFDC_load_errors1 where error='Operation Successful.')

 -- (5 row(s) affected)

 -- insert into Task_Tritech_SFDC_Load
 select * from Task_Tritech_SFDC_load_errors1 where error='Operation Successful.'

 --
--(5 row(s) affected)


select error, count(*) from Task_Tritech_SFDC_Load 
group by error

--141396

select legacy_id__c,count(*) from Task_Tritech_SFDC_load
group by Legacy_id__c
having count(*)>1



select legacy_id__c ,count(*) from Superion_FULLSB.dbo.task
group by Legacy_Id__c
having count(*)>1




