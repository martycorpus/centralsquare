/*
-- Author     : Shivani Mogullapalli
-- Date       :07/12/2018
-- Description: Migrate the EmailMessage from the Archieved Tasks.
Requirement Number:
Scope:
Migrate all the EmailMessage where relatedtoid is in the RelatedToId.
*/
/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_TRITECH_PROD','EmailMEssage','Yes'
EXEC SF_Refresh 'MC_TRITECH_PROD','EmailMEssageRelation','Yes'

EXEC SF_ReplicateIAD 'MC_TRITECH_PROD','task' 


Use Superion_FULLSB
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Task','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Opportunity','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Account','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Write_In__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Pricing_Request_Form__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Procurement_Activity__C','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Demo_Request__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact_reference_Link__c','yes'


*/
-- select count(*) from tritech_prod.dbo.[Task]
--1671317


use staging_sb
go
-- drop table EmailMessage_Tritech_SFDC_Preload_task
declare @defaultuser varchar(18)
select @defaultuser = id  from [Superion_FULLSB].dbo.[user]
							where name like 'Superion API'
;With CteWhatData as
(
select 
a.LegacySFDCAccountId__c  Legacy_id_orig,
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Account' as What_parent_object_name 
from Superion_FULLSB.dbo.Account a
where 1=1 
and a.LegacySFDCAccountId__c is not null

						union
select
a.Legacy_Opportunity_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Opportunity' as What_parent_object_name 
from Superion_FULLSB.dbo.Opportunity a
where 1=1
and a.Legacy_Opportunity_ID__c is not null

						union 
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Case' as What_parent_object_name 
from Superion_FULLSB.dbo.[case] a
where 1=1
and a.Legacy_ID__c is not null
						union 
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Sales_Request__c a
where 1=1
and a.Legacy_Record_ID__c is not null
			union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Procurement_Activity__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Procurement_Activity__c a
where 1=1
and a.Legacy_ID__c is not null
				union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact_Reference_Link__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Contact_Reference_Link__c a
where 1=1
and a.Legacy_ID__c is not null

) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from Superion_FULLSB.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null				
 )   

SELECT		
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_email.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
				,ActivityID_orig						= tr_email.[ActivityId]
				,ActivityId_sp=target_task.id
				
				,BccAddress_orig						 = tr_email.[BccAddress]
				,BCcAddress								 = replace(cast([BccAddress] as nvarchar(max)),'@','@dummy.')
				,CCAddress_orig							 = tr_email.[CcAddress]
				,CcAddress								 =	replace(cast([CcAddress] as nvarchar(max)),'@','@dummy.')
				,CreatedById_orig						= tr_email.[CreatedById]
				,CreatedById							= iif(Tar_CreateID.id is null, 
																		iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Tar_createID.id )
				,CreatedDate							= tr_email.[CreatedDate]
				,FromAddress_orig						= tr_email.[FromAddress]
				,FromAddress							= replace([FromAddress],'@','@dummy.')
				,FromName								= tr_email.[FromName]
														  --,tr_email.[HasAttachment]
				,Headers								= tr_email.[Headers]
				,HTMLBody								= tr_email.[HtmlBody]
				,id_orig								= tr_email.[Id]
				,Incoming								= tr_email.[Incoming]
				,IsClientManaged_orig						= tr_email.[IsClientManaged]
				,IsClientManaged						='True'
														  --,tr_email.[IsDeleted]
				,IsExternallyVisible					= tr_email.[IsExternallyVisible]
														  --,tr_email.[LastModifiedById]
														  --,tr_email.[LastModifiedDate]
				,MessageDate							= tr_email.[MessageDate]
				,MessageIdentifier						= tr_email.[MessageIdentifier]
						,ParentID_orig					=	tr_email.[ParentId]
						--,ParentID						=  Target_case.id
				,RelateddtoId_orig						= tr_email.[RelatedToId]
				,Relatedtoid							= Act.Parent_id
				,Relatedtoid_Legacy_Source_System__c	= act.Legacy_Source_System
				,What_parent_object_name				= act.what_parent_object_name
				 ,ReplyToEmailMessageId_orig=tr_email.[ReplyToEmailMessageId]
				 ,ReplyToEmailMessageId_sp=iif(tr_email.ReplyToEmailMessageId is null,null,'Legacy_Id__C:' + tr_email.ReplyToEmailMessageId)
				,Status							= tr_email.[Status]
				
				,Subject								= tr_email.[Subject]
														  --,tr_email.[SystemModstamp]
				,TextBody								= tr_email.[TextBody]
														  -- n,tr_email.[ThreadIdentifier]
				,ToAddress_orig							= tr_email.[ToAddress]
				,ToAddress								= replace(cast([ToAddress] as nvarchar(max)),'@','@dummy.')
				--,ValidatedFromAddress					= replace(cast(tr_email.[ValidatedFromAddress] as nvarchar(max)),'@','@dummy.')
				
				,Isdeleted_orig							= tr_task.IsDeleted
				,IsArchived_orig								= tr_task.IsArchived
				,Task_ownerid_orig							= Tr_task.OwnerId
				,Task_ownerid								= Tar_owner.id
				,Task_whoid_orig							= Tr_Task.WhoId
				,Task_whoid									= Whoid.Parent_id
				,Task_whoparent_object_name					= whoid.Who_parent_object_name
				,Task_legacy_source_system					= whoid.Legacy_Source_System
				-------
				,tt_emr_EmailMessageId=tt_emr.EmailMessageId	
					,tt_emr_EmailMessagerelationId=tt_emr.id	
					,RelationId_orig=tt_emr.RelationId	
					,RelationId_sup=target_cnt.id
					,RelationType_orig=tt_emr.RelationType
					,sp_task_whoid=target_task.whoid 
					,tt_task_whoid=tt_task.whoid
---------------------------------

						 into EmailMessage_Tritech_SFDC_Preload_task
 FROM [Tritech_PROD].[dbo].[EmailMessage] tr_email
 -----------------------------------------------------------------------------------
  left outer join Tritech_PROD.dbo.[EmailMessageRelation] tt_emr
  on tr_email.id=tt_emr.EmailMessageId and tt_emr.RelationObjectType='Contact'
  -----------------------------------------------------------------------------------
  left outer join  Superion_FULLSB.dbo.contact target_cnt
  on target_cnt.legacy_id__c= tt_emr.RelationId
  -----------------------------------------------------------------------------------
  left outer join  Tritech_PROD.dbo.task tt_task
  on tt_task.id= tr_email.ActivityId
   -----------------------------------------------------------------------------------
  left outer join  Superion_FULLSB.dbo.task target_task
  on target_task.legacy_id__c= tr_email.ActivityId
  ----------------------------------------------------------------------------------
 left join Superion_FULLSB.dbo.[User] Tar_CreateID
  on Tar_CreateID.Legacy_Tritech_Id__c=tr_email.CreatedById
  and Tar_CreateId.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------
  left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_email.createdbyId
  ------------------------------------------------------------------------------------
  inner join Tritech_Prod.dbo.task Tr_Task
  on tr_task.id = tr_email.ActivityId and tr_task.IsDeleted='false'
  -------------------------------------------------------------------------------------
  inner join ctewhatdata act 
  on act.Legacy_id_orig =Tr_email.RelatedToId
  --and act.Legacy_Source_System='Tritech'
  ----------------------------------------------------------------------------------------
  left join ctewhodata Whoid
  on whoid.Legacy_id_orig = tr_task.WhoId
  and act.Legacy_Source_System='Tritech'
  ----------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Tar_owner
  on Tar_owner.Legacy_Tritech_Id__c=Tr_task.OwnerId
  and Tar_owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------
 where 1=1 and 
 tr_Email.RelatedToId is not null and tr_email.ParentId is null  
 
 --4 mins
 -- 23189 row(s) affected)


 select * from EmailMessage_Tritech_SFDC_Preload_task
 where ActivityID_orig is not null and ActivityId_sp is null;--14784

 select count(*) from EmailMessage_Tritech_SFDC_Preload_task
 where ActivityID_orig is not null and ActivityId_sp is not null;--8405



  select * from EmailMessage_Tritech_SFDC_Preload_task
 where ReplyToEmailMessageId_orig is not null;--0


 -- drop table EmailMessage_Tritech_SFDC_load_task
 select  *
 into EmailMessage_Tritech_SFDC_load_task
 from EmailMessage_Tritech_SFDC_Preload_task tr_task
 where 1=1 ;
 
 --(23189 row(s) affected)
 --drop table Task_Tritech_SFDC_Delete_Email_Tasks
 select  distinct id=activityid_sp,activityid_orig,error=cast(space(255) as nvarchar(255)) 
 into Task_Tritech_SFDC_Delete_Email_Tasks
 from EmailMessage_Tritech_SFDC_load_task
 where activityid_sp is not null;

 --Exec SF_BulkOps 'Delete:batchsize(90)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_Delete_Email_Tasks'


 select * from EmailMessage_Tritech_SFDC_load_task

 -- check for the duplicates in the final load table.
select Legacy_ID__c,Count(*) from EmailMessage_Tritech_SFDC_load_task
group by Legacy_ID__c
having count(*)>1


select * from EmailMessage_Tritech_SFDC_load_task;

--: Run batch program to create task
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'EmailMessage_Tritech_SFDC_load_task' 

--check column names
  
--Exec SF_BulkOps 'Insert:batchsize(90)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_task'
/*
Salesforce object EmailMessage does not contain column ActivityID_orig
Salesforce object EmailMessage does not contain column ActivityId_sp
Salesforce object EmailMessage does not contain column BccAddress_orig
Salesforce object EmailMessage does not contain column CCAddress_orig
Salesforce object EmailMessage does not contain column CreatedById_orig
Salesforce object EmailMessage does not contain column FromAddress_orig
Salesforce object EmailMessage does not contain column id_orig
Salesforce object EmailMessage does not contain column IsClientManaged_orig
Salesforce object EmailMessage does not contain column ParentID_orig
Salesforce object EmailMessage does not contain column RelateddtoId_orig
Salesforce object EmailMessage does not contain column Relatedtoid_Legacy_Source_System__c
Salesforce object EmailMessage does not contain column What_parent_object_name
Salesforce object EmailMessage does not contain column ReplyToEmailMessageId_orig
Salesforce object EmailMessage does not contain column ReplyToEmailMessageId_sp
Salesforce object EmailMessage does not contain column ToAddress_orig
Salesforce object EmailMessage does not contain column Isdeleted_orig
Salesforce object EmailMessage does not contain column IsArchived_orig
Salesforce object EmailMessage does not contain column Task_ownerid_orig
Salesforce object EmailMessage does not contain column Task_ownerid
Salesforce object EmailMessage does not contain column Task_whoid_orig
Salesforce object EmailMessage does not contain column Task_whoid
Salesforce object EmailMessage does not contain column Task_whoparent_object_name
Salesforce object EmailMessage does not contain column Task_legacy_source_system
Salesforce object EmailMessage does not contain column tt_emr_EmailMessageId
Salesforce object EmailMessage does not contain column tt_emr_EmailMessagerelationId
Salesforce object EmailMessage does not contain column RelationId_orig
Salesforce object EmailMessage does not contain column RelationId_sup
Salesforce object EmailMessage does not contain column RelationType_orig
Salesforce object EmailMessage does not contain column sp_task_whoid
Salesforce object EmailMessage does not contain column tt_task_whoid
*/

select error,count(*) from EmailMessage_Tritech_SFDC_load_task
group by error



select count(*) from EmailMessage_Tritech_SFDC_load_task 
where error<>'Operation Successful.'
--180

-- drop table EmailMessage_Tritech_SFDC_load_task_errors
select * 
into EmailMessage_Tritech_SFDC_load_task_errors
from EmailMessage_Tritech_SFDC_load_task
where error <>'Operation Successful.'

-- drop table EmailMessage_Tritech_SFDC_load_task_errors_bkp
select * 
into EmailMessage_Tritech_SFDC_load_task_errors_bkp
from EmailMessage_Tritech_SFDC_load_task
where error <>'Operation Successful.'

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_task_errors'

select *
--delete 
 from EmailMessage_Tritech_SFDC_load_task 
where error<>'Operation Successful.';--180


--insert into EmailMessage_Tritech_SFDC_load_task
select * from EmailMessage_Tritech_SFDC_load_task_errors

select error, count(*) from EmailMessage_Tritech_SFDC_load_task
group by error

select legacy_id__c from EmailMessage_Tritech_SFDC_load_task where error like 'duplicate%'
--02s8000000ZQo4JAAT

select * from Superion_FULLSB.dbo.EmailMessage where Legacy_Id__c='02s8000000ZQo4JAAT'


--Use Superion_FULLSB
--EXEC SF_Refresh 'SL_SUPERION_FULLSB','EmailMessage','yes'

use Staging_SB
go
-- drop table Task_tritech_SFDC_Preload_Update_whoid
declare @defaultuser1 nvarchar(18)= (select  id  from [Superion_FULLSB].dbo.[user]
							where name like 'Superion API')

select 
					 Id								= em.ActivityId
					,Id_orig						= loadtk.ActivityID_orig			
					,Error							= cast(space(255) as nvarchar(255)) 
					,OwnerID_sp						= loadtk.Task_ownerid
					,Ownerid  =iif( loadtk.Task_ownerid is null, @defaultuser1,loadtk.Task_ownerid)
					,Ownnerid_orig					= loadtk.Task_ownerid_orig
					,WhoId							= loadtk.Task_whoid
					,whoid_orig						= loadtk.Task_whoid_orig
		into Task_tritech_SFDC_Preload_Update_whoid
from Superion_FULLSB.dbo.EmailMessage em 
inner join EmailMessage_Tritech_SFDC_load_task loadtk
on em.id = loadtk.id

-- (23188 row(s) affected)

select * from Task_tritech_SFDC_Preload_Update_whoid 

select count(*) from Task_tritech_SFDC_Preload_Update_whoid 

-- check duplicates.
select id, count(*) from Task_tritech_SFDC_Preload_Update_whoid
group by id 
having count(*)>1

-- drop table Task_tritech_SFDC_load_Update_whoid
select * 
into Task_tritech_SFDC_load_Update_whoid
from Task_tritech_SFDC_Preload_Update_whoid 
where 1=1 ;--23188

select * from Task_tritech_SFDC_load_Update_whoid

-- (23188 row(s) affected)


--: Run batch program to create task
  
--exec SF_ColCompare 'Update','SL_SUPERION_FULLSB', 'Task_tritech_SFDC_load_Update_whoid' 

--check column names
  
--Exec SF_BulkOps 'Update:batchsize(90)','SL_SUPERION_FULLSB','Task_tritech_SFDC_load_Update_whoid'

/*

Salesforce object Task does not contain column Id_orig
Salesforce object Task does not contain column OwnerID_sp
Salesforce object Task does not contain column Ownnerid_orig
Salesforce object Task does not contain column whoid_orig
*/

select error,count(*) from Task_tritech_SFDC_load_Update_whoid
group by error;

