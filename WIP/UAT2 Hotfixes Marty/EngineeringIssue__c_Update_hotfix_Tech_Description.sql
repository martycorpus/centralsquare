select * 
from sys.tables
where name like '%Engineering%'
order by create_date desc;

select error, count(*)
from tablename
group by error;

Select *
from information_schema.columns
where table_name = ''

-- drop table EngineeringIssue__c_Update_hotfix_Tech_Description 
select t1.id
      ,t1.error, t1.Legacy_id__c
      ,t2.CCB_Notes__c as CCB_Notes_orig
      ,t2.Description__c as descrption_orig
      ,t2.TFS_Comments__c as TFS_Comments_orig
      ,case
        when cast(t2.Description__c as nvarchar(max)) is null and cast(t2.TFS_Comments__c as nvarchar(max)) is null 
        then 'CCB: ' + char(10) + cast(t2.CCB_Notes__c as nvarchar(max))
        when cast(t2.Description__c as nvarchar(max)) is  not null and cast(t2.TFS_Comments__c as nvarchar(max)) is null
        then cast(t2.Description__c as nvarchar(max)) + char(10)  + 'CCB: ' + char(10) + cast(t2.CCB_Notes__c as nvarchar(max))
        when cast(t2.Description__c as nvarchar(max)) is  null and cast(t2.TFS_Comments__c as nvarchar(max)) is not null
        then cast(t2.TFS_Comments__c as nvarchar(max)) + char(10) + 'CCB: ' + char(10) + cast(t2.CCB_Notes__c as nvarchar(max))
        else 
        cast(t2.Description__c as nvarchar(max))  + char(10)  + cast(t2.TFS_Comments__c as nvarchar(max)) + ' ' + char(10) + 'CCB: ' + char(10) + cast(t2.CCB_Notes__c as nvarchar(max))
      end 
as Technical_Description__c   
into EngineeringIssue__c_Update_hotfix_Tech_Description
from EngineeringIssue__c_Load_slim t1
join Tritech_Prod.dbo.Potential_Defect__c t2 on t2.id = t1.Legacy_id__c
where t2.CCB_Notes__c is not null


select * from EngineeringIssue__c_Update_hotfix_Tech_Description

exec sf_bulkops 'Update','MC_superion_fullsb','EngineeringIssue__c_Update_hotfix_Tech_Description'


--- Starting SF_BulkOps for EngineeringIssue__c_Update_hotfix_Tech_Description V3.6.9
--18:59:28: Run the DBAmp.exe program.
--18:59:28: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
--18:59:28: Updating Salesforce using EngineeringIssue__c_Update_hotfix_Tech_Description (SQL01 / Staging_SB) .
--18:59:28: DBAmp is using the SQL Native Client.
--18:59:28: SOAP Headers: 
--18:59:29: Warning: Column 'CCB_Notes_orig' ignored because it does not exist in the EngineeringIssue__c object.
--18:59:29: Warning: Column 'descrption_orig' ignored because it does not exist in the EngineeringIssue__c object.
--18:59:29: Warning: Column 'TFS_Comments_orig' ignored because it does not exist in the EngineeringIssue__c object.
--18:59:42: 1378 rows read from SQL Table.
--18:59:42: 1378 rows successfully processed.
--18:59:42: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.




