-- References__c_Update_MC_Tritech_account.sql

select t1.id, cast(null as nvarchar(255)) as error,t3.id as Account__c, t4.name as name_orig,t2.Account__c as Account_orig
into Contact_Reference_Link__c_Update_MC_Tritech_account
from Contact_Reference_Link__c_Load_MC_Tritech t1
join [Tritech_PROD].[dbo].[REFERENCES__C] t2 on t2.id = t1.Legacy_Id__c
join [Tritech_PROD].[dbo].[Account] t4 on t4.id = t2.Account__c
join [MC_SUPERION_FULLSB]...account t3 on t3.[LegacySFDCAccountId__c] = t2.Account__c

select * from References__c_Update_MC_Tritech_account

exec SF_BulkOps 'Update','mc_superion_fullsb','Contact_Reference_Link__c_Update_MC_Tritech_account'

select Legacy_Id__c, count(*)
from Contact_Reference_Link__c_Load_MC_Tritech
group by Legacy_Id__c
having count(*) > 1


select * from [Tritech_PROD].[dbo].[REFERENCES__C] t2


select t1.id, cast(null as nvarchar(255)) as error, t1.Legacy_Id__c --,t3.id as Account__c
from Contact_Reference_Link__c_Load_MC_Tritech t1
 join [Tritech_PROD].[dbo].[REFERENCES__C] t2 on t2.id = t1.Legacy_Id__c
where t2.id is null


select * from [Account_Tritech_SFDC_Load] where [LegacySFDCAccountId__c] = '0018000000tzBaXAAU'
