select id, name, * from Account_Tritech_SFDC_Load
where name like '%,%'

select id, error, name as name_old, replace(name,' ,',', ') as name , 'MC 12/10: Change format of Name field from a space comma to a comma space.' as Comment_Apps
into Account_Update_MC_Hotfix_CommaSpace
from Account_Tritech_SFDC_Load
where name like '%,%' --(23269 row(s) affected)

select * from Account_Update_MC_Hotfix_CommaSpace
update Account_Update_MC_Hotfix_CommaSpace set error = null

exec sf_bulkops 'Update','mc_superion_fullsb','Account_Update_MC_Hotfix_CommaSpace'
