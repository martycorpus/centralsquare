select * from sys.tables
where name like 'Account%'
order by create_date desc

select * from Account_Tritech_SFDC_Load



select *
from information_schema.columns
where table_name = 'Account_Tritech_SFDC_Load'
and column_name like 'Last%'

select id, error, 
Last_SW_Purchase_Date_911__c,
Last_SW_Purchase_Date_ePCR__c,
Last_SW_Purchase_Date_FBR__c,
Last_SW_Purchase_Date_Fire_RMS__c,
Last_SW_Purchase_Date_Mapping__c,
Last_SW_Purchase_Date_Mobile__c
into Account_Tritech_SFDC_update_last_sw_purchase
from Account_Tritech_SFDC_Load
where 
Last_SW_Purchase_Date_911__c is not null or
Last_SW_Purchase_Date_ePCR__c is not null or
Last_SW_Purchase_Date_FBR__c is not null or
Last_SW_Purchase_Date_Fire_RMS__c is not null or
Last_SW_Purchase_Date_Mapping__c is not null or
Last_SW_Purchase_Date_Mobile__c is not null 


exec sf_bulkops 'update','mc_superion_fullsb','Account_Tritech_SFDC_update_last_sw_purchase'

/*
Last SW Purchase Date (911)
Last SW Purchase Date (ePCR)
Last SW Purchase Date (FBR)
Last SW Purchase Date (Fire RMS)
Last SW Purchase Date (Law RMS)
Last SW Purchase Date (Mapping)
Last SW Purchase Date (Mobile)

*/