drop table Case_update_MC_Parent_Eng_Issue

select t4.id
      ,cast(null as nvarchar(255)) as Error
      ,t3.id as Parent_Engineering_Issue__c
      ,t1.id as legacy_Case_id_orig
      ,t1.Defect_Number__c as Defect_Number_orig
      ,t2.id as [legacy_potential_defect_id]
Into Case_update_MC_Parent_Eng_Issue      
from tritech_Prod.dbo.[case] t1
join tritech_Prod.dbo.[potential_defect__c] t2 on t2.Defect_Ticket_ID__c = t1.Defect_Number__c
left join EngineeringIssue__c_Load_slim t3 on t3.Legacy_Id__c = t2.id
--left
  join Case_Tritech_SFDC_load t4 on t4.Legacy_Id__c = t1.id
where t1.Defect_Number__c is not null


select * from Case_update_MC_Parent_Eng_Issue 

exec sf_colcompare 'Update','mc_superion_fullsb','Case_update_MC_Parent_Eng_Issue'

exec sf_bulkops 'Update:batchsize(5)','mc_superion_fullsb','Case_update_MC_Parent_Eng_Issue'

--- Starting SF_BulkOps for Case_update_MC_Parent_Eng_Issue V3.6.9
--02:19:32: Run the DBAmp.exe program.
--02:19:32: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
--02:19:32: Updating Salesforce using Case_update_MC_Parent_Eng_Issue (SQL01 / Staging_SB) .
--02:19:33: DBAmp is using the SQL Native Client.
--02:19:33: Batch size reset to 5 rows per batch.
--02:19:33: SOAP Headers: 
--02:19:33: Warning: Column 'legacy_Case_id_orig' ignored because it does not exist in the Case object.
--02:19:33: Warning: Column 'Defect_Number_orig' ignored because it does not exist in the Case object.
--02:19:33: Warning: Column 'legacy_potential_defect_id' ignored because it does not exist in the Case object.
--02:35:08: 10638 rows read from SQL Table.
--02:35:08: 10638 rows successfully processed.
--02:35:08: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

update Case_update_MC_Parent_Eng_Issue
set id = null
where error is not null or error = space(0)





select t4.id
      ,cast(null as nvarchar(255)) as Error
      ,t3.id as Parent_Engineering_Issue__c
      ,t1.id as legacy_Case_id_orig
      ,t1.Defect_Number__c as Defect_Number_orig
      ,t2.id as [legacy_potential_defect_id]
--Into Case_update_MC_Parent_Eng_Issue_try2      
from tritech_Prod.dbo.[case] t1
join tritech_Prod.dbo.[potential_defect__c] t2 on t2.Defect_Ticket_ID__c = t1.Defect_Number__c
left join EngineeringIssue__c_Load_slim t3 on t3.Legacy_Id__c = t2.id
--left
  join Case_Tritech_SFDC_load t4 on t4.Legacy_Id__c = t1.id
where t1.Defect_Number__c is not null
and 


select * from EngineeringIssue__c_Load_slim



select *
from 
Case_update_MC_Parent_Eng_Issue
where Defect_Number_orig
= 'Def-125536'

alter table Case_update_MC_Parent_Eng_Issue add ident int identity(1,1)

drop tABLE Case_update_MC_Parent_Eng_Issue_Def_125536
select *
into Case_update_MC_Parent_Eng_Issue_Def_125536
from Case_update_MC_Parent_Eng_Issue where ident = 120



exec sf_bulkops 'Update','mc_superion_fullsb','Case_update_MC_Parent_Eng_Issue_Def_125536'
select * from Case_update_MC_Parent_Eng_Issue_Def_125536
