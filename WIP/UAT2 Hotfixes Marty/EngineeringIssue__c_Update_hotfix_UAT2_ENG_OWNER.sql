-- drop table EngineeringIssue__c_Update_hotfix_UAT2_ENG_OWNER

select id, error, null as ENGOwner__c, ENGOwner__c as ENGOwner_orig
into EngineeringIssue__c_Update_hotfix_UAT2_ENG_OWNER
from EngineeringIssue__c_Load_slim
where ENGOwner__c is not null;

exec sf_bulkops 'Update:batchsize(5)','mc_superion_fullsb','EngineeringIssue__c_Update_hotfix_UAT2_ENG_OWNER'