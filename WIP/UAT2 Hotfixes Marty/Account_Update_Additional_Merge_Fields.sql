--drop table Account_Update_Additional_Merge_Fields_MC

WITH ctescope
     AS ( SELECT t1.id,
                 t1.legacy_source_system__c,
                 t1.migrated_record__c,
                 t1.legacysfdcaccountid__c,
                 t1.NAME,
                 t2.id AS TT_AccountID
          FROM   mc_superion_fullsb...ACCOUNT t1
                 JOIN tritech_prod.dbo.ACCOUNT t2
                   ON t2.id = t1.legacysfdcaccountid__c
          WHERE  t1.migrated_record__c = 'false' )
SELECT cte.id                                   AS ID,
       Cast( NULL AS NVARCHAR(255))             AS Error,
       Replace(cte.name,' ,',', ')              as name, -- fix name comma 
       cte.Name                                 AS SupAccountName_orig,
       tta.id                                   AS TT_AccountID_orig,
       tta.name                                 AS TT_AccountName_orig,
       active_client_product_families__c        AS Legacy_TT_Client_Product_Families__c,
       active_contract_product_groups__c        AS Legacy_TT_Client_Product_Groups__c,
       billing_installed_year__c                AS Last_SW_Purchase_Date_Billing__c,
       call_volume_annual__c                    AS Annual_Calls_for_Service_Received__c,
       contract_types__c                        AS Legacy_TT_Active_Contract_Types__c,
       ems_customer_number_wmp__c               AS Legacy_TT_Customer_Number__c,
       epcr_installed_year__c                   AS Last_SW_Purchase_Date_ePCR__c,
       fbr_installed_year__c                    AS Last_SW_Purchase_Date_FBR__c,
       IIF(tta.ORI__c IS NULL,tta.FDID__c,tta.ORI__c)                                 AS ORI__c,
       fire_fighter_paid_per_call__c            AS Fire_Paid_Per_Call__c,
       fire_installed_year__c                   AS Last_SW_Purchase_Date_Fire_RMS__c,
       inactive_contract_types__c               AS Legacy_TT_Inactive_Contract_Types__c,
       incident_volume__c                       AS NumberofCallsforService__c,
       --"Mailing_Billing_Country_WMP__c as New Field: Restricted Picklist
       --Field Name: Billing Country,"
       mapping__c                               AS Last_SW_Purchase_Date_Mapping__c,
       mobile_installed_year__c                 AS Last_SW_Purchase_Date_Mobile__c,
       parent_active_client_product_families__c AS Legacy_TT_Parent_Active_Client_Product_F__c,
       parent_active_contract_types__c          AS Legacy_TT_Parent_Active_Contract_Types__c,
       rms_installed_year__c                    AS Last_SW_Purchase_Date_Law_RMS__c,
       --"Shipping_Country_WMP__c as New Field (Restricted Picklist)
       --Field Name: Shipping Country,"
       CASE
          WHEN TriCON_Attendee__c IS NOT NULL and Z1_Attendee__c IS NULL
              THEN CONCAT('TriCON ',REPLACE(CAST(TriCON_Attendee__c as varchar),';',';TriCON '))
          WHEN Z1_Attendee__c IS NOT NULL and TriCON_Attendee__c IS NULL
             THEN CONCAT('z1 ',REPLACE(CAST(Z1_Attendee__c as varchar),';',';z1 '))
          WHEN TriCON_Attendee__c IS NOT NULL and Z1_Attendee__c IS  NOT NULL
             THEN CONCAT(CONCAT('TriCON ',REPLACE(CAST(TriCON_Attendee__c as varchar),';',';TriCON ')),';',CONCAT('z1 ',REPLACE(CAST(Z1_Attendee__c as varchar),';',';z1 ')))
          END as User_Conference_Attendee__c,
       x911_telephony_installed_year__c         AS Last_SW_Purchase_Date_911__c,
       z_data_conversion_analyst__c             AS Legacy_TT_Data_Conversion_Analyst__c,
       z_gis_analyst__c                         AS Legacy_TT_GIS_Analyst__c,
       z_install_name__c                        AS Install_Name__c,
       account_at_risk__c                       AS TT_Account_At_Risk__c,
       comments__c                              AS TT_Account_Status_Comments__c,
       issues_problems__c                       AS TT_Account_Status_Issues_Problems__c,
       client_relations_contact_goal__c         AS Wellness_Check_Contact_Goal__c,
       z_account_renewal_confidence__c          AS Account_Renewal_Confidence__c,
       z_account_renewal_notes__c               AS Account_Renewal_Notes__c,
       z_change_in_leadership__c                AS Change_in_Leadership__c,
       z_completed_yearly_contacts__c           AS Completed_Yearly_Wellness_Checks__c,
       z_last_yearly_contact_date__c            AS Last_Wellness_Check_Date__c,
       z_last_yearly_contact_type__c            AS Last_Wellness_Check_Type__c,
       z_low_agency_ownership_user_adoption__c  AS Low_Agency_Ownership_User_Adoption__c,
       z_maintenance_lapse__c                   AS Maintenance_Lapse__c,
       z_next_renewal_date__c                   AS Next_Renewal_Date__c,
       z_no_communication_from_customer__c      AS No_Communication_From_Customer__c,
       z_not_a_full_suite_customer__c           AS Not_a_Full_Suite_Customer__c,
       z_renewal_expected_next_year__c          AS Renewal_Expected_Next_Year__c,
       z_total_maintenance_amount__c            AS Total_Maintenance_Amount__c,
       z_vocal_with_issues__c                   AS Vocal_with_Issues__c,
       'Additional Merge fields update - Marty' AS Apps_Hotfix_Comment      
INTO   Account_Update_Additional_Merge_Fields_MC       
FROM   tritech_prod.dbo.ACCOUNT tta
       JOIN ctescope cte
         ON cte.tt_accountid = tta.id;
         
         
exec sf_colcompare 'Update','mc_superion_fullsb','Account_Update_Additional_Merge_Fields_MC'         
/*
--- Starting SF_ColCompare V3.6.9
Problems found with Account_Update_Additional_Merge_Fields_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134
--- Ending SF_ColCompare. Operation FAILED.
Salesforce object Account does not contain column SupAccountName_orig
Salesforce object Account does not contain column TT_AccountID_orig
Salesforce object Account does not contain column TT_AccountName_orig
Salesforce object Account does not contain column Apps_Hotfix_Comment
*/         

exec sf_bulkops 'Update','mc_superion_fullsb','Account_Update_Additional_Merge_Fields_MC'    
        