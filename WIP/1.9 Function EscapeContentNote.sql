
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EscapeContentNote]'))
Drop Function [dbo].[EscapeContentNote]
GO 

CREATE FUNCTION EscapeContentNote(@nstring AS nvarchar(MAX))      
RETURNS varchar(MAX) AS
BEGIN    

Declare @rstring as varchar(max)

set @rstring  = replace(
				replace( 
                replace( 
                replace( 
                replace( @nstring
                ,    '&', '&amp;' )
                ,    '<', '&lt;' )
                ,    '>', '&gt;' )
                ,    '"', '&quot;' )
				,    '''', '&#39;'  )


RETURN @rstring;
End;
