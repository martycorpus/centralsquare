-------------------------------------------------------------------------------
--- User Migration Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 9 November 2018
--- Last Updated: 10 November 2018 - PAB
--- Change Log: 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_Load_PB') 
	DROP TABLE Staging_SB.dbo.[User_Load_PB];


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Superion_FULLSB
Exec SF_Replicate 'PB_Superion_FULLSB', 'Profile'

USE Tritech_PROD
Exec SF_Replicate 'PB_Tritech_Prod', 'User'
Exec SF_Replicate 'PB_Tritech_Prod', 'Profile'

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Tritech_Id__c,
		'Tritech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		Concat(a.[Username], '.CSFullSB') as Username,
		a.[LastName] as LastName,
		isNull(a.[FirstName], '') as FirstName,
		isNull(a.[CompanyName], '') as CompanyName,
		isNull(a.[Division], '') as Division,
		isNull(a.[Department], '') as Department,
		isNull(a.[Title], '') as Title,
		isNull(a.[Street], '') as Street,
		isNull(a.[City], '') as City,
		isNull(a.[State], '') as State,
		isNull(a.[PostalCode], '') as PostalCode,
		isNull(a.[Country], '') as Country,
		Concat(a.[Email], '.CSFullSB') as Email,
		isNull(a.[Phone], '') as Phone,
		isNull(a.[Fax], '') as Fax,
		isNull(a.[MobilePhone], '') as MobilePhone,
		a.[Alias] as Alias,
		Concat(a.[CommunityNickname], '1') as CommunityNickname,
		'false' as IsActive,
		a.[TimeZoneSidKey] as TimeZoneSidKey,
		a.[LocaleSidKey] as LocaleSidKey,
		a.[ReceivesInfoEmails] as ReceivesInfoEmails,
		a.[ReceivesAdminInfoEmails] as ReceivesAdminInfoEmails,
		a.[EmailEncodingKey] as EmailEncodingKey,
		Spr_Profile.ID as ProfileID,
		a.[LanguageLocaleKey] as LanguageLocaleKey,
		isNull(a.[EmployeeNumber], '') as EmployeeNumber,
		isNull(a.[UserPermissionsMarketingUser], '') as UserPermissionsMarketingUser,
		isNull(a.[UserPermissionsOfflineUser], '') as UserPermissionsOfflineUser,
		isNull(a.[UserPermissionsCallCenterAutoLogin], '') as UserPermissionsCallCenterAutoLogin,
		isNull(a.[UserPermissionsMobileUser], '') as UserPermissionsMobileUser,
		isNull(a.[UserPermissionsSFContentUser], '') as UserPermissionsSFContentUser,
		isNull(a.[UserPermissionsKnowledgeUser], '') as UserPermissionsKnowledgeUser,
		isNUll(a.[UserPermissionsInteractionUser], '') as UserPermissionsInteractionUser,
		isNull(a.[UserPermissionsSupportUser], '') as UserPermissionsSupportUser,
		a.[ForecastEnabled] as ForecastEnabled,
		a.[UserPreferencesActivityRemindersPopup] as UserPreferencesActivityRemindersPopup,
		a.[UserPreferencesEventRemindersCheckboxDefault] as UserPreferencesEventRemindersCheckboxDefault,
		a.[UserPreferencesTaskRemindersCheckboxDefault] as UserPreferencesTaskRemindersCheckboxDefault,
		a.[UserPreferencesReminderSoundOff] as UserPreferencesReminderSoundOff,
		a.[UserPreferencesApexPagesDeveloperMode] as UserPreferencesApexPagesDeveloperMode,
		a.[UserPreferencesHideCSNGetChatterMobileTask] as UserPreferencesHideCSNGetChatterMobileTask,
		a.[UserPreferencesHideCSNDesktopTask] as UserPreferencesHideCSNDesktopTask,
		a.[UserPreferencesSortFeedByComment] as UserPreferencesSortFeedByComment,
		a.[UserPreferencesLightningExperiencePreferred] as UserPreferencesLightningExperiencePreferred,
		isNull(a.[CallCenterId], '') as CallCenterId,
		isNull(a.[Extension], '') as Extension,
		isNull(a.[PortalRole], '') as PortalRole,
		isNull(a.[FederationIdentifier], '') as FederationIdentifier,
		isNull(a.[AboutMe], '') as AboutMe,
		a.[DigestFrequency] as DigestFrequency,
		isNull(a.[Bomgar_Username__c], '') as Bomgar_Username__c
		INTO Staging_SB.dbo.User_Load_PB
		FROM Tritech_PROD.dbo.[User] a
		-- TriTech Profiles
		LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] TT_Profile ON  a.ProfileId = TT_Profile.ID
		-- Superion Profile
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Profile] Spr_Profile ON Spr_Profile.[Name] = 
								CASE TT_Profile.[Name] 
									WHEN 'TriTech Portal Read Only with Tickets' THEN 'Superion Community Standard - Login'
									WHEN 'TriTech Portal Read-Only User' THEN 'Superion Community Standard - Login'
									WHEN 'Overage High Volume Customer Portal User' THEN 'Superion Community Standard - Login'
									WHEN 'Overage Customer Portal Manager Custom' THEN 'Superion Community Standard - Login'
									WHEN 'TriTech Portal Standard User' THEN 'Superion Community Standard - Login'
									WHEN 'TriTech Portal Manager' THEN 'Superion Community Standard - Login'
									ELSE 'Superion standard user' END
		
		WHERE a.isActive = 'true'
		-- Following code is to prevent migration of User records that were precreated
		AND a.[Email] NOT IN
				(	Select SPR.email
					FROM Superion_FULLSB.dbo.[User] SPR
					WHERE Migrated_Record__c = 'false'
				)
		-- Eliminates Community Users licensed Users from being migrated as part of this script, to be migrated separately later
		AND TT_Profile.[Name] Not IN
					('TriTech Portal Read Only with Tickets',
					 'TriTech Portal Read-Only User',
					 'Overage High Volume Customer Portal User',
					 'Overage Customer Portal Manager Custom',
					 'TriTech Portal Standard User',
					 'TriTech Portal Manager')




---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.user_Load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'upsert:bulkapi,batchsize(400)', 'PB_Superion_FULLSB', 'User_Load_PB', 'Legacy_Tritech_ID__c'


--select * from user_Load_PB where error not like '%success%'


---------------------------------------------------------------------------------
-- Populate External ID for precreated Users
-- Only users were a matching email exists on a SINGLE record will be populated
-- User records whose email exists on multiple records will NOT be populated and must be manually resolved
---------------------------------------------------------------------------------

USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_ExtID_Update_PB') 
	DROP TABLE Staging_SB.dbo.[User_ExtID_Update_PB];



Select 
	SPR.ID as ID, 
	Cast('' as nvarchar(255)) as Error,
	TT.ID as Legacy_Tritech_Id__c,
	'Tritech' as Legacy_Source_System__c
	INTO Staging_SB.dbo.User_ExtID_Update_PB
	FROM Superion_FULLSB.dbo.[User] SPR
	INNER JOIN Tritech_PROD.dbo.[User] TT on SPR.email = TT.Email
	-- Following code eliminates records where the email address is repeated multiple times.
	WHERE tt.email NOT IN
						(Select SPR.email
						FROM Superion_FULLSB.dbo.[User] SPR
						INNER JOIN Tritech_PROD.dbo.[User] TT on SPR.email = TT.Email
						Group by SPR.email
						having count(*) > 1
						)
	AND SPR.Migrated_Record__c = 'false'  -- ensures that only pre-existing User records are included

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'update', 'PB_Superion_FULLSB', 'User_ExtID_Update_PB'
