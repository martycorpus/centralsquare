-------------------------------------------------------------------------------
--- CommunityUser Disablement Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 30, 2018
--- Last Updated: November 30, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 1. Must have access to the original load table User_CommunityUserLoad_Update on the Staging_SB database
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
USE E2E_Delete
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Profile', 'yes'

--------------------------------------------------------------------------------
-- Drop Staging Table
--------------------------------------------------------------------------------

Use E2E_Delete

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUser_Disable') 
DROP TABLE E2E_Delete.dbo.User_CommunityUser_Disable;

--------------------------------------------------------------------------------
-- Build Staging Table
--------------------------------------------------------------------------------

select 
	Usr.ID as ID,
	CAST('' as nvarchar(255)) as error,
	'false' as isPortalEnabled,
	Concat('yyy', Usr.Username) as Username,
	Concat('yyy', Usr.Email) as Email,
	Concat('yyy', Usr.LastName) as LastName,
	Concat('yyy', substring(Usr.Legacy_Tritech_Id__c, 4, 15)) as Legacy_Tritech_Id__c,
	Concat('yyy', CommunityNickname) as CommunityNickname
	INTO User_CommunityUser_Disable
	from E2E_Delete.dbo.[user] Usr
	LEFT OUTER JOIN [E2E_Delete].dbo.[Profile] Prof ON Usr.ProfileId = Prof.id
	WHERE 
	usr.Migrated_Record__c = 'true'
	and usr.Legacy_Source_System__c = 'tritech'
	and 
	isNull(usr.Legacy_Tritech_Id__c, '') <> ''
	and
	Prof.[Name] in ('CentralSquare Community Standard - Login', 'CentralSquare Community Standard - Dedicated')

--------------------------------------------------------------------------------
-- Upload 
--------------------------------------------------------------------------------
Exec sf_bulkops 'Update','PB_Superion_FULLSB','User_CommunityUser_Disable'

--Select * from User_CommunityUser_Disable where error not like '%Success%'

Update User_CommunityUser_Disable 
set Legacy_Tritech_ID__c = concat('z', substring(Legacy_Tritech_ID__c, 2,17)) 
--from User_CommunityUser_Disable
where error not like '%success%'

delete from User_CommunityUser_Disable where error like '%success%'
