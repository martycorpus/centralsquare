-------------------------------------------------------------------------------
--- ChatterGroup Feed (CollaborationGroupFeed) Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 28, 2018
--- Last Updated: November 28, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'CollaborationGroup', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'CollaborationGroupFeed', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes';

Use Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'CollaborationGroup', 'yes';

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroupFeed_Load_PB') 
DROP TABLE Staging_SB.dbo.CollaborationGroupFeed_Load_PB;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------

USE Staging_SB

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		isNull(a.Body, '') as Body,
		isNull(a.Title, '') as Title,
		a.InsertedByID as InsertedByID_Orig,
		isNull(InsertedBy.ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as InsertedByID, 
		a.IsRichText as IsRichText,
		a.ParentID as ParentID_Orig,
		Group_Target.ID as ParentID,
		a.LinkURL as LinkURL,
		a.[Type] as [Type]		
		INTO Superion_FullSB.dbo.CollaborationGroupFeed_Load_PB
		From Tritech_Prod.dbo.CollaborationGroupFeed a
		---InsertedBy
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [InsertedBy] ON a.InsertedByID = [InsertedBy].Legacy_Tritech_Id__c
		---- Chatter Group
		LEFT OUTER JOIN Tritech_PROD.dbo.[CollaborationGroup] [Group_Source] ON a.ParentID = [Group_Source].[ID]
		INNER JOIN Superion_FULLSB.dbo.[CollaborationGroup] [Group_Target] ON Group_Source.[Name] = [Group_Target].[Name]
		WHERE a.[Type] in ('LinkPost', 'TextPost')

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performan
---------------------------------------------------------------------------------
Alter table Superion_FULLSB.dbo.CollaborationGroupFeed_load_PB
Add [Sort] int identity (1,1)



---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Superion_FULLSB
Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'CollaborationGroupFeed_load_PB'

--select * from CollaborationGroupFeed_load_PB 

