USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FullSB', 'Opportunity' 


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Opportunity_Tritech_Update_PB') 
DROP TABLE Staging_SB.dbo.Opportunity_Tritech_Update_PB;

Use Staging_SB

select 
Legacy_Opportunity_ID__c as ID, 
Cast('' as nvarchar(255)) as Error, 
'true' as Migrated_to_Superion__c
INTO Opportunity_Tritech_Update_PB
From Superion_FULLSB.dbo.Opportunity
where isNull(Legacy_Opportunity_ID__c, '') <> ''
and Migrated_Record__c = 'true'


Use Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(5000)', 'PB_TriTech_Prod', 'Opportunity_Tritech_Update_PB';

-- Select * from Opportunity_Tritech_Update_PB where error not like '%success%'