-------------------------------------------------------------------------------
--- Script to Set EmailMessage Status to correct value post Attachment Load
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: December 1, 2018
--- Last Updated: December 1, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'EmailMessage', 'yes';

Use Superion_FUllSB
Exec SF_Refresh 'PB_Superion_FullSB', 'EmailMessage', 'yes';

---------------------------------------------------------------------------------
-- Drop Map_NoteParentID_PB
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='EmailMessage_StatusUpdate') 
DROP TABLE Staging_SB.dbo.EmailMessage_StatusUpdate;



---------------------------------------------------------------------------------
-- Load ContentNote Staging Table
---------------------------------------------------------------------------------
Use Staging_SB;

select 
CAST(a.ID as nvarchar(255)) as ID,
CAST('' as nvarchar(255)) as error,
Src_Email.[Status] as [Status]
INTO EmailMessage_StatusUpdate
From Superion_FULLSB.dbo.EmailMessage a
INNER JOIN Tritech_Prod.dbo.EmailMessage Src_Email on a.Legacy_Id__c = Src_Email.id
WHERE a.Migrated_Record__c = 'true'
and a.Legacy_Source_System__c = 'TriTech'


--------------------------------------------------------------------------------
-- Upload ContentNote
--------------------------------------------------------------------------------
Exec sf_bulkops 'Update','PB_Superion_FULLSB','EmailMessage_StatusUpdate'

--Select * from EmailMessage_StatusUpdate where error like '%Success%'
--
-- select status, * from Superion_FULLSB.dbo.EmailMessage where id = '02s0v000000PHX5AAO'

