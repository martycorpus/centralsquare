/*
--C:\Users\mcorpus\Documents\Superion\Central Square Repo\WIP\ContentVersion_Insert_MC_Tritech.sql


2018-11-03 MartyC Created.
           Migrate Tritech attachments to Superion Files.
		   objects: 
		   Account 001; Campaign 701; Case 500; Contact 003; Event	00U
           Lead	00Q; Opportunity 006; Task 00T 
		   Pending Items:
		   1) CTE source tables.


001	Account
003	Contacts
006	Opportunity
00Q	Lead
00T	Task
00U	Event
00X	
02i	Asset
02s	
500	Cases (Tickets)
701	Campaigns
800	Contracts
810	Service Contracts
a08	
a0B	Potential Defects
a0g	Projects
a0H	Demo Requests
a0i	Procurement Activities
a0J	Win Strategy
a0K	SalesForce Help Desk
a0z	Opportunity Plans
a1k	
a1q	Write-In
a2C	Pricing/Proposal Request Forms


*/

Use [Superion_FULLSB];

--Exec sf_replicate MC_SUPERION_FULLSB,'USER'

USE Tritech_Prod;

-- REFRESH
-- exec SF_Refresh MC_Tritech_PROD,Attachment


select count(*) from Tritech_Prod.dbo.Attachment
select top 1 * from Tritech_Prod.dbo.Attachment

--Part 1.

USE Staging_SB;

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'ContentVersion_Insert_Attachments_MC' AND
            table_schema = 'dbo' )
  DROP TABLE dbo.ContentVersion_Insert_Attachments_MC; --drop the load table if it exists;


WITH Attachment_Scope_CTE AS
(
select * from sourcetables -- pending.
)
SELECT Cast( NULL AS NCHAR(18))            AS Id,
       Cast( NULL AS NVARCHAR(255))        AS Error,
       a.id                                AS Legacy_Source_Id__c,
	   'true'                              AS Migrate_Record__c,
	   'Tritech'                           AS Legacy_Source_System__c,
       a.parentid                          AS ParentID_orig,
       cte.id                              AS ParentId,
       a.NAME                              AS Title,
       body                                AS VersionData,
	   duo.Id                              AS OwnerId,
       a.ownerid                           AS OwnerId_orig,
	   uo.NAME + ' IsActive: ' + uo.isactive AS OwnerIdName_orig,
       a.NAME                              AS pathonclient, --4/16
       du.id                               AS CreatedById,
       a.createdbyid                       AS CreatedById_orig,
       u.NAME + ' IsActive: ' + u.isactive AS CreatedByName_orig,
       a.createddate
INTO   ContentVersion_Insert_Attachments_MC
FROM   Tritech_Prod.dbo.Attachment a
       JOIN ATTACHMENT_SCOPE_CTE cte
         ON cte.id_orig = a.parentid
       LEFT JOIN Tritech_Prod.dbo.[USER] u
              ON u.id = a.createdbyid
       LEFT JOIN Tritech_Prod.dbo.[USER] uo
              ON uo.id = a.OwnerId
       LEFT JOIN Superion_FULLSB.dbo.[USER] du
              ON du.Legacy_ID_SFDC__c = u.id; 
       LEFT JOIN Superion_FULLSB.dbo.[USER] duo
              ON duo.Legacy_ID_SFDC__c = uo.id;

ALTER TABLE ContentVersion_Insert_Attachments_MC ADD Ident INT Identity(1,1);

SELECT * FROM ContentVersion_Insert_Attachments_MC;

-- Exec SF_ColCompare  'Insert','MC_Superion_FULLSB','ContentVersion_Insert_Attachments_MC';

/*
check

--check for files with no file type extension in the pathonclient field.
--if missing, manually append the file type extenstion based what is indicated on the contentype field.
select name, contenttype
from ContentVersion_Insert_Attachments_MC
where substring(right(pathonclient,4),1,1)  <> '.'
and substring(right(pathonclient,5),1,1)  <> '.'


*/

-- Exec SF_BulkOps 'Insert:batchsize(1)','MC_Superion_FULLSB','ContentVersion_Insert_Attachments_MC';

--PART 2. --update the load table with ContentDocumentIds that where created in step 1.
--note: when inserting into the ContenVersion object in SF, a record gets 
--      automatically created in the ContentDocument object for that record that was
--      created in ContentVersion. The field LatestPublishedVersionId in ContentDocument
--      contains the ID of the record in ContenVersion.

USE Staging_SB;

alter table ContentVersion_Insert_Attachments_MC add ContentDocumentId nchar(18);

-- EXEC SF_Replicate 'MC_Superion_FULLSB','ContentDocument';

EXEC SF_Refresh 'MC_Superion_FULLSB','ContentDocument','Yes';


UPDATE i
SET    ContentDocumentId = cd.Id
FROM   ContentDocument cd
       JOIN ContentVersion_Insert_Attachments_MC i
         ON i.id = cd.latestpublishedversionid; --(2557 row(s) affected) 4/11
		 
		

select * from ContentVersion_Insert_Attachments_MC;

--PART 3.
--Create a ContentDocumentLink record to relate the file to the parentID.

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'ContentDocumentLink_Insert_MC' AND
            table_schema = 'dbo' )
  DROP TABLE dbo.ContentDocumentLink_Insert_MC; --drop the load table if it exists;

select * 
into ContentDocumentLink_Insert_MC
from 
( 
 SELECT cast(null as nchar(18)) as Id,
       cast(null as nvarchar(255)) as Error,
	   ContentDocumentId,
	   ParentId as LinkedEntityId,
	   'V' as ShareType,
	   'AllUsers' as Visibility,
	   Id as ContentVersionId_xref,
	   AttachmentId_Orig,
	   Title as FileName_ref
	   from ContentVersion_Insert_Attachments_MC --link the parent object to the content document
union 
 SELECT cast(null as nchar(18)) as Id,
        cast(null as nvarchar(255)) as Error,
	   ContentDocumentId,
	   '00D1b000000DFdiEAG' as LinkedEntityId,
	   'V' as ShareType,
	   'AllUsers' as Visibility,
	   Id as ContentVersionId_xref,
	   AttachmentId_Orig,
	   Title as FileName_ref
	   from ContentVersion_Insert_Attachments_MC --provide view permissions company wide.
) x
where contentdocumentid is not null
and LinkedEntityId is not null
order by contentdocumentid;


-- exec sf_colcompare 'Insert','MC_Superion_FULLSB','ContentDocumentLink_Insert_MC';



-- exec sf_bulkops 'Insert','MC_Superion_FULLSB','ContentDocumentLink_Insert_MC';








