-------------------------------------------------------------------------------
--- Opportunity Migration Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 11 November 2018
--- Last Updated: 14 November 2018 - PAB
--- Change Log: 
---
--- Prerequisites:
---	1. ID values for competitors must be updated from 'SandboxID' to 'ProductionID' to reflect the appropriate environment for Competitors
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Opportunity_Load_PB') 
	DROP TABLE Staging_SB.dbo.[Opportunity_Load_PB];


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Tritech_PROD
Exec SF_Replicate 'PB_Tritech_Prod', 'User'
Exec SF_Replicate 'PB_Tritech_Prod', 'Account'
Exec SF_Replicate 'PB_Tritech_Prod', 'Opportunity'
Exec SF_Replicate 'PB_Tritech_Prod', 'RecordType'


USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Account'
Exec SF_Refresh 'PB_Superion_FULLSB', 'User'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Contact'
Exec SF_Refresh 'PB_Superion_FULLSB', 'RecordType'

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.ID as Legacy_Opportunity_ID__c,
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		a.[Accident_Report_Interface__c] as [Accident_Report_Interface__c],
		a.[Accident_Reports__c] as [Accident_Reports__c],
		isNull(a.[AccountId], '') as AccountID_Original,
		isNull(trg_Acct.ID, '') as [AccountID],
		isNull(CAST(a.[alternate_opp_value__c] as varchar(19)), '') as [alternate_opp_value__c],
		isNull(CAST(a.[Amount] as varchar(19)), '') as [Total_Contract_Value_Forecast__c],
		isNull(a.[Approval_Manager_Email__c], '') as [Approval_Manager_Email__c],
		isNull(a.[Associate_Account_Manager__c], '') as [Legacy_Associate_Account_Manager__c],
		--a.[Associated_Opportunity__c] as [Associated_Opportunity__c],
		isNull(CONVERT(VARCHAR(19), a.[Bid_No_Bid_Decision_Date__c], 120), '') as [Bid_No_Bid_Decision_Date__c],
		--Bill_To__c as Bill_To__c,  -- need to review with Yahel Gaver if needed.
		--a.[Bill_To_City__c] as [Bill_To_City__c]_N/A,
		--a.[Bill_To_Country__c] as [Bill_To_Country__c]_N/A,
		--a.[Bill_To_State__c] as [Bill_To_State__c]_N/A,
		--a.[Bill_To_Street__c] as [Bill_To_Street__c]_N/A,
		--a.[Bill_To_Zip__c] as [Bill_To_Zip__c]_N/A,
		isNull(CONVERT(VARCHAR(19), a.[Booked_Date__c], 120), '') as [Booked_Date__c],
		isNull(CAST(a.[Booked_Value__c] as varchar(19)), '') as [Booked_Value__c],
		isNull(CAST(a.[Budget_Total__c] as varchar(19)), '') as [Budget_Total__c],
		isNull(a.[Business_Issues_WMP__c], '') as [Customer_s_Defined_Initiatives__c],
		a.[Citation_Interface__c] as [Citation_Interface__c],
		a.[Citations__c] as [Citations__c],
		a.[CloseDate] as [CloseDate],
		isNull(Cast(a.[Comm_Value_Sync__c] as varchar(19)), '') as [Commissionable_Value__c],
		isNull(Concat(a.[Comments_Reason_TriTech_Lost__c], a.[Comments_Reason_TriTech_Won__c]), '') as [Win_Loss_Comments__c],
		isNull(Cast(a.[Commissionable_GM__c] as varchar(19)), '') as [Commissionable_GM__c],
		isNull(a.[Compelling_Event__c], '') as [Compelling_Reason__c],
		isNull(a.[Competitors_WMP__c],'') as [Competitors__c],  --- Needs resolved, lookup to competitor
		isNull(ConCapMgr.ID, '') as [Contract_Capture_Manager__c],
		isNull(a.[CPQ_Quote_Number__c], '') as [CPQ_Quote_Number__c],
		a.[CreatedByID] as CreatedByID_Original,
		--isNull(CreatedBy.ID, (Select top 1 ID from Superion_FULLSB.dbo.[user] where [Name] = 'Superion API')) as [CreatedByID],
		--a.[CreatedDate] as [CreatedDate],
		isNULL(Cast(a.[Current_Annual_Support__c] as varchar(19)), '') as [Current_Annual_Support__c],
		isNull(a.[Customer_Number__c], '') as [Legacy_TT_Customer_Number__c],
		isNull(Cast(a.[Daily_Fee__c] as varchar(19)), '') as [Daily_Fee__c],
		isNull(CONVERT(VARCHAR(19), a.[Date_Grant_Funds_Expire__c], 120), '') as [Date_Grant_Funds_Expire__c],
		isNull(a.[Deconfliction_Type__c], '') as [Deconfliction_Type__c],
		isNull(a.[Description], '') as [Description],
		isNull(Cast(a.[Discount__c] as varchar(19)), '') as [Discount__c],
		isNull(Cast(a.[Discount_Amount__c] as varchar(19)), '') as [Discount_Amount__c],
		isNull(CONVERT(VARCHAR(19), a.[End_Date__c], 120), '') as [End_Date__c],
		isNull(a.[Funding_Comments__c], '') as [Funding_Comments__c],
		isNull(Cast(a.[Grant_Amount__c] as varchar(19)), '') as [Grant_Amount__c],
		isNull(Cast(a.[Gross_Margin__c] as varchar(19)), '') as [Gross_Margin__c],
		isNull(a.[If_No_Bid_Primary_Reason__c], '') as [If_No_Bid_Primary_Reason__c],
		isNull(CAST(a.[Implementation_Fees__c] as varchar(30)), '') as [Implementation_Fees__c],
		isNull(IncumbentVdr.SandboxID, '') as [Incumbent_Software_Vendor__c],
		isNull(a.[INTERNAL_Note_to_Approvers__c], '') as [INTERNAL_Note_to_Approvers__c],
		isNull(CONVERT(VARCHAR(19), a.[Last_Approval_Date__c], 120), '') as [Last_Approval_Date__c],
		isNull(Convert(VARCHAR(19), a.[Last_Rejection_Date__c], 120), '') as [Last_Rejection_Date__c],
		isNull(Convert(VARCHAR(19), a.[Monthly_Fee__c], 120), '') as [Monthly_Fee__c],
		IsNull(a.[Most_Recent_Approval_Level__c], '') as [Most_Recent_Approval_Level__c],
		Cast(a.[Name] as varchar(80)) as [Name],
		a.[Name] as [Legacy_TT_Opportunity_Name__c],
		isNull(a.[Name_of_Grant_Program_s__c], '') as [Name_of_Grant_Program_s__c],
		isNull(CONVERT(VARCHAR(19), a.[Next_Steps_Synopsis_Modified_Date__c], 120), '') as [Next_Steps_Last_Modified_Date__c],
		isNull(a.[NextStep], '') as [NextStep],
		isNull([OwnerID].ID, (Select top 1 ID from Superion_FULLSB.dbo.[User] where [name] = 'Superion API')) as [OwnerID],
		isNull(CAST(a.[Prepaid_Subscriptions__c] as varchar(19)), '') as [Prepaid_Subscriptions__c],
		isNull(a.[Price_Book__c], '') as [Price_Book__c],
		isNull(Cast(a.[Probability] as varchar(19)), '') as [Probability],
		isNull(a.[Products_Required__c], '') as [Product_s_Required__c],
		isNull(CAST(a.[Project_Related_Fees__c] as varchar(19)), '') as [Project_Related_Fees__c],
		isNull(a.Proposal_Writer__c, '') as Proposal_Manager__c_original,
		isNull(PropMgr.ID, '') as [Proposal_Manager__c],
		isNull(CONVERT( Varchar(19), [Purchase_Order_Date__c],120), '') as [Purchase_Order_Date__c],
		isNull(a.[Purchase_Order_Number__c], '') as [Purchase_Order_Number__c],
		isNull(a.[Quote_Number__c], '') as [Quote_Number__c],
		isNull(Cast(a.[Quote_Subtotal__c] as varchar(19)), '') as [Quote_Subtotal__c],
		isNull(CAST(a.[Quote_Total__c] as varchar(19)), '') as [Quote_Total__c],
		isNull(a.[RecordTypeId], '') as RecordTypeID_Original,
		trg_RT.ID as RecordTypeID,
		isNull(Src_Rt.[Name], '') as TT_Legacy_Opportunity_Record_Type__c,
		isNull(CAST(a.[Recurring_Fees__c] as varchar(19)), '') as [Recurring_Fees__c],
		isNull(a.[Registered_Affiliates__c], '') as [Registered_Affiliates__c],
		isNull(a.[Return_to_Approval_Step__c], '') as [Return_to_Approval_Step__c],
		isNull(a.[Sales_Order_Number__c], '') as [Sales_Order_Number__c],
		isNull(CONVERT(varchar(19), a.[Selection_Date__c], 120), '') as [Selection_Date__c],
		--a.[Ship_To_City__c] as [Ship_To_City__c]_NA,
		--a.[Ship_To_Country__c] as [Ship_To_Country__c_NA],
		--a.[Ship_To_State__c] as [Ship_To_State__c]_NA,
		--a.[Ship_To_Street__c] as [Ship_To_Street__c]_NA,
		--a.[Ship_To_Zip__c] as [Ship_To_Zip__c]_NA,
		isNull(CONVERT(varchar(19), a.[Shortlisted_Date__c], 120), '') as [Shortlisted_Date__c],
		isNull(a.[StageName], '') as [StageName],
		isNull(CONVERT(varchar(19), a.[Start_Date__c], 120), '') as [Start_Date__c],
		isNull(CAST(a.[Subscription_Term_Years__c] as varchar(19)), '') as [Subscription_Term_Years__c],
		isNull(CAST(a.[Tax__c] as varchar(19)), '') as [Tax__c],
		isNull(CAST(a.[Third_Party_GM__c] as varchar(19)), '') as [Third_Party_GM__c],
		isNull(CAST(a.[Total_Custom_Solution__c] as varchar(19)), '') as [Total_Custom_Solution__c],
		isNull(CAST(a.[Total_Hardware__c] as varchar(19)), '') as [Total_Hardware__c],
		isNull(CAST(a.[Total_Implementation_Service_Fees__c] as varchar(19)), '') as [Total_Implementation_Service_Fees__c],
		isNull(CAST(a.[Total_Maintenance__c] as varchar(19)), '') as [Total_Maintenance__c],
		isNull(CAST(a.[Total_Project_Related_Fees__c] as varchar(19)), '') as [Total_Project_Related_Fees__c],
		isNull(CAST(a.[Total_Quoted_Amount__c] as varchar(19)), '') as [Total_Quoted_Amount__c],
		isNull(CAST(a.[Total_Recurring_Fees__c] as varchar(19)), '') as [Total_Recurring_Fees__c],
		isNull(CAST(a.[Total_Software__c] as varchar(19)), '') as [TT_Total_Software__c],
		isNull(CAST(a.[Total_T_and_E_Amount__c] as varchar(19)), '') as [Total_T_and_E_Amount__c],
		isNull(CAST(a.[Total_Third_Party__c] as varchar(19)), '') as [Total_Third_Party__c],
		isNull(WinComp.ID, '') as [Winning_SW_Vendor_Lookup__c],
		isNull(Cast(a.[Yearly_Fee__c] as varchar(19)), '') as [Yearly_Fee__c],
		isNull(Cast(a.[Z_Annual_Hosting_Fee__c] as varchar(19)), '') as [Annual_Hosting_Fee__c],
		a.[Z_CAD_911__c] as [CAD_911__c],
		isNull(CAST(a.[Z_COGS__c] as varchar(19)), '') as [COGS__c],
		isNull(a.[Z_Data_Conversion__c], '') as [Data_Conversion__c],
		isNull(CONVERT(varchar(19), a.[Z_Date_Affiliate_Form_Signed__c], 120), '') as [Date_Affiliate_Form_Signed__c],
		isNull(a.[Z_Discount_Notes__c], '') as [Discount_Notes__c],
		isNull(CONVERT(varchar(19), a.[Z_Expected_RFP_Release_Date__c], 120), '') as [Expected_RFP_Release_Date__c],
		isNull(a.[Z_Forecast_Category__c], '') as [Forecasting_Category__c],
		isNull(a.[Z_GIS_Conversion__c], '') as [GIS_Conversion__c],
		isNull(Cast(a.[Z_Hardware__c] as varchar(19)), '') as [Hardware__c],
		isNull(a.[Z_Interface_Hours__c], '') as [Interface_Hours__c],
		isNull(Cast(a.[Z_Interfaces__c] as varchar(19)), '') as [Interfaces__c],
		isNull(a.[Z_Interfaces_Required__c], '') as [Interfaces_Required__c],
		isNull(Cast(a.[Z_Maintenance_Discount__c] as varchar(19)), '') as [Z_Maintenance_Discount__c],
		isNull(a.[Z_Migration_Discount__c], '') as [Migration_Discount__c],
		a.[Z_NCIC__c] as [NCIC__c],
		isNull(a.[Z_NCIC_Notes__c], '') as [NCIC_Notes__c],
		isNull(CONVERT(Varchar(19), a.[Z_NCIC_Quote_Requested__c], 120), '') as [NCIC_Quote_Requested__c],
		isNull(CAST(a.[Z_Pre_Paid_Maintenance__c] as varchar(19)), '') as [Prepaid_Maintenance__c],
		isNull(a.[Z_Product_Enhancement_Hours__c], '') as [Product_Enhancement_Hours__c],
		isNull(a.[Z_Product_Enhancements_Required__c], '') as [Product_Enhancements_Required__c],
		isNull(Cast(a.[Z_Recurring_COGS__c] as varchar(18)), '') as [Recurring_COGS__c],
		isNull(a.[Z_RFP_Notes__c], '') as [RFP_Notes__c],
		isNull(a.[Z_RFP_Required__c], '') as [RFP_Required__c],
		isNull(Cast(a.[Z_Server_Cost__c] as varchar(19)), '') as [Server_Cost__c],
		isNull(a.[Z_Server_Type__c], '') as [Server_Type__c],
		isNull(Cast(a.[Z_Services__c] as varchar(19)), '') as [Services__c],
		a.[Z_Short_Term_Holding_Only__c] as [Short_Term_Holding_Only__c],
		isNull(Signatory.ID, '') as [Signatory__c],
		isNull(Cast(a.[Z_Software__c] as varchar(19)), '') as [Software__c],
		isNull(Cast(a.[Z_Software_Discount__c] as varchar(19)), '') as  [Software_Discount__c],
		isNull(a.[Z_Special_Maintenance_Terms__c], '') as [Z_Special_Maintenance_Terms__c],
		isNull(a.[Z_Special_Payment_Terms__c], '') as [Special_Payment_Terms__c],
		isNull(Cast(a.[Z_Subscription_Fees__c] as varchar(19)), '') as [Subscriptions_Year_1__c],
		isNull(a.[Z_Sys_Ops_Notes__c], '') as [Sys_Ops_Notes__c],
		isNull(a.[Z_Training_Days__c], '') as [Training_Days__c],
		isNull(Cast(a.[Z_Year_1_Maintenance__c]  as varchar(19)), '') as [Maintenance_Year_1__c],
		isNull(a.[Z_ZT_Spec_Provided__c], '') as [Spec_Provided__c],
		'Public Safety & Justice' as Solution_Family__c,
		----a.[Actual_Opportunity_Value__c] as [Actual_Opportunity_Value__c_Config],
		----a.[Booked_Actual_Value_Not_Equal__c] as [Booked_Actual_Value_Not_Equal__c_Config],
		----a.[Estimated_Cost_per_Sworn_Officer__c] as [Estimated_Cost_per_Sworn_Officer__c_Config],
		----a.[Forecast_Value__c] as [Forecast_Value__c_Config],
		----a.[Primary_Quote_Contact__c] as [Primary_Quote_Contact__c_Config],
		----a.[Credited_Sales_Team__c] as [Credited_Sales_Team__c_Follow-Up],
		----a.[Deal_Type__c] as [Deal_Type__c_Follow-Up],
		----a.[Deconfliction_Next_Step_Date__c] as [Deconfliction_Next_Step_Date__c_Follow-Up],
		----a.[Deconfliction_Next_Steps__c] as [Deconfliction_Next_Steps__c_Follow-Up],
		----a.[Deconfliction_Status__c] as [Deconfliction_Status__c_Follow-Up],
		----a.[Edited_Last_30_days__c] as [Edited_Last_30_days__c_Follow-Up],
		----a.[Implementation_Definition_Review_Date__c] as [Implementation_Definition_Review_Date__c_Follow-Up],
		----a.[LeadSource] as [LeadSource_Follow-Up],
		----a.[Original_Superion_Close_Date__c] as [Original_Superion_Close_Date__c_Follow-Up],
		----a.[Original_Superion_TCV__c] as [Original_Superion_TCV__c_Follow-Up],
		----a.[Reason_TriTech_Lost__c] as [Reason_TriTech_Lost__c_Follow-Up],
		----a.[Reason_TriTech_Won__c] as [Reason_TriTech_Won__c_Follow-Up],
		----a.[Red_Team_Date__c] as [Red_Team_Date__c_Follow-Up],
		----a.[step__c] as [step__c_Follow-Up],
		----a.[Superion_Stage__c] as [Superion_Stage__c_Follow-Up],
		----a.[TriTech_Selected__c] as [TriTech_Selected__c_Follow-Up],
		----a.[TriTech_Shortlisted__c] as [TriTech_Shortlisted__c_Follow-Up],
		----a.[Type] as [Type_Follow-Up],
		----a.[Weighted_Amount__c] as [Weighted_Amount__c_Follow-Up]
		----a.[Account_ID__c] as [Account_ID__c_N/A],
		----a.[AE_is_it_a_ZT_Spec__c] as [AE_is_it_a_ZT_Spec__c_N/A],
		----a.[Agencies_being_served__c] as [Agencies_being_served__c_N/A],
		----a.[Agency_Types_Being_Served__c] as [Agency_Types_Being_Served__c_N/A],
		----a.[Approved_Budget__c] as [Approved_Budget__c_N/A],
		----a.[Associate_Account_Manager_Email__c] as [Associate_Account_Manager_Email__c_N/A],
		----a.[BANT_Approved__c] as [BANT_Approved__c_N/A],
		----a.[BANT_Complete__c] as [BANT_Complete__c_N/A],
		----a.[BANT_Completed_Date__c] as [BANT_Completed_Date__c_N/A],
		----a.[BANT_Paid__c] as [BANT_Paid__c_N/A],
		----a.[Bid_Bond_Amount__c] as [Bid_Bond_Amount__c_N/A],
		----a.[Bid_Bond_Issued__c] as [Bid_Bond_Issued__c_N/A],
		----a.[Bid_Bond_Percentage__c] as [Bid_Bond_Percentage__c_N/A],
		----a.[Bid_Bond_Required__c] as [Bid_Bond_Required__c_N/A],
		----a.[Bid_Bond_Returned__c] as [Bid_Bond_Returned__c_N/A],
		----a.[Bidder_s_Conference_Date__c] as [Bidder_s_Conference_Date__c_N/A],
		----a.[Bidders_Conference_Required__c] as [Bidders_Conference_Required__c_N/A],
		----a.[CAD_Dispatcher_Call_Taker_Seats__c] as [CAD_Dispatcher_Call_Taker_Seats__c_N/A],
		----a.[CampaignId] as [CampaignId_N/A],
		----a.[Change_Order_Additions_Total__c] as [Change_Order_Additions_Total__c_N/A],
		----a.[Change_Order_Deletions_Total__c] as [Change_Order_Deletions_Total__c_N/A],
		----a.[Charge_Types__c] as [Charge_Types__c_N/A],
		----a.[Close_Date_Certainty__c] as [Close_Date_Certainty__c_N/A],
		----a.[Close_Date_Confidence_Current_Close_Date__c] as [Close_Date_Confidence_Current_Close_Date__c_N/A],
		----a.[Close_Date_Confidence_Following_Month__c] as [Close_Date_Confidence_Following_Month__c_N/A],
		----a.[Close_Date_Confidence_Next_Month__c] as [Close_Date_Confidence_Next_Month__c_N/A],
		----a.[Close_Date_Confidence_Total__c] as [Close_Date_Confidence_Total__c_N/A],
		----a.[Commisionable__c] as [Commisionable__c_N/A],
		----a.[Common_Partners__c] as [Common_Partners__c_N/A],
		----a.[Competitive_Selection_Approved__c] as [Competitive_Selection_Approved__c_N/A],
		----a.[Conference_Attendee__c] as [Conference_Attendee__c_N/A],
		----a.[Contact__c] as [Contact__c_N/A],
		----a.[Contract_Required__c] as [Contract_Required__c_N/A],
		----a.[Contract_Sent_to_Client__c] as [Contract_Sent_to_Client__c_N/A],
		----a.[Count_of_Contacts__c] as [Count_of_Contacts__c_N/A],
		----a.[Deadline_for_Vendor_Questions__c] as [Deadline_for_Vendor_Questions__c_N/A],
		----a.[Discovery_results_attached__c] as [Discovery_results_attached__c_N/A],
		----a.[Do_we_know_how_much_is_budgeted__c] as [Do_we_know_how_much_is_budgeted__c_N/A],
		----a.[Dummyrecordtouchfield__c] as [Dummyrecordtouchfield__c_N/A],
		----a.[Executive_Summary_Modifed_Date__c] as [Executive_Summary_Modifed_Date__c_N/A],
		----a.[Extension_Requested__c] as [Extension_Requested__c_N/A],
		----a.[Fiscal] as [Fiscal_N/A],
		----a.[FiscalQuarter] as [FiscalQuarter_N/A],
		----a.[FiscalYear] as [FiscalYear_N/A],
		----a.[Forecast__c] as [Forecast__c_N/A],
		----a.[ForecastCategory] as [ForecastCategory_N/A],
		----a.[ForecastCategoryName] as [ForecastCategoryName_N/A],
		----a.[GM_Target_Acheived__c] as [GM_Target_Acheived__c_N/A],
		----a.[Green_Team_Date__c] as [Green_Team_Date__c_N/A],
		----a.[HasOpenActivity] as [HasOpenActivity_N/A],
		----a.[HasOpportunityLineItem] as [HasOpportunityLineItem_N/A],
		----a.[HasOverdueTask] as [HasOverdueTask_N/A],
		----a.[Have_we_engaged_the_committee__c] as [Have_we_engaged_the_committee__c_N/A],
		----a.[Host_Agency_Acknowledgement_Form__c] as [Host_Agency_Acknowledgement_Form__c_N/A],
		----a.[Host_Agency_Acknowledgement_Form_Complet__c] as [Host_Agency_Acknowledgement_Form_Complet__c_N/A],
		----a.[Identified_Pre_Go_Live__c] as [Identified_Pre_Go_Live__c_N/A],
		----a.[Identify_Committee_Members__c] as [Identify_Committee_Members__c_N/A],
		----a.[If_Closed_Other_State_Reason__c] as [If_Closed_Other_State_Reason__c_N/A],
		----a.[Internal_RFP_Link__c] as [Internal_RFP_Link__c_N/A],
		----a.[Internal_RFP_Link_1__c] as [Internal_RFP_Link_1__c_N/A],
		----a.[Internal_RFP_Location__c] as [Internal_RFP_Location__c_N/A],
		----a.[Internal_RFP_Location_Link__c] as [Internal_RFP_Location_Link__c_N/A],
		----a.[IsClosed] as [IsClosed_N/A],
		----a.[IsDeleted] as [IsDeleted_N/A],
		----a.[IsWon] as [IsWon_N/A],
		----a.[Jail_Beds__c] as [Jail_Beds__c_N/A],
		----a.[Last_Modified_Target_Opportunity__c] as [Last_Modified_Target_Opportunity__c_N/A],
		----a.[LastActivityDate] as [LastActivityDate_N/A],
		----a.[LastModifiedById] as [LastModifiedById_N/A],
		----a.[LastModifiedDate] as [LastModifiedDate_N/A],
		----a.[LastReferencedDate] as [LastReferencedDate_N/A],
		----a.[LastViewedDate] as [LastViewedDate_N/A],
		----a.[Legacy_Zuercher_Created_By__c] as [Legacy_Zuercher_Created_By__c_N/A],
		----a.[Legacy_Zuercher_Created_Date_Time__c] as [Legacy_Zuercher_Created_Date_Time__c_N/A],
		----a.[Letter_of_Intent_Due_Date__c] as [Letter_of_Intent_Due_Date__c_N/A],
		----a.[Letter_of_Intent_Required__c] as [Letter_of_Intent_Required__c_N/A],
		----a.[New_Contract_Required__c] as [New_Contract_Required__c_N/A],
		----a.[New_INTERNAL_Notes_to_Approvers__c] as [New_INTERNAL_Notes_to_Approvers__c_N/A],
		----a.[Next_Steps_Synopsis_Modified_By__c] as [Next_Steps_Synopsis_Modified_By__c_N/A],
		----a.[No_Bid_Letter_Required__c] as [No_Bid_Letter_Required__c_N/A],
		----a.[No_Bid_Letter_Required_Date__c] as [No_Bid_Letter_Required_Date__c_N/A],
		----a.[No_Of_Reference__c] as [No_Of_Reference__c_N/A],
		----a.[Number_of_Electronic_Copies_Required__c] as [Number_of_Electronic_Copies_Required__c_N/A],
		----a.[Number_of_Hard_Copies_Required__c] as [Number_of_Hard_Copies_Required__c_N/A],
		----a.[of_Authorized_Users__c] as [of_Authorized_Users__c_N/A],
		----a.[On_Site_Discovery_Date__c] as [On_Site_Discovery_Date__c_N/A],
		----a.[Opportunity_Long_ID__c] as [Opportunity_Long_ID__c_N/A],
		----a.[Opportunity_Owner_Percent__c] as [Opportunity_Owner_Percent__c_N/A],
		----a.[Opportunity_URL__c] as [Opportunity_URL__c_N/A],
		----a.[OpportunityURL__c] as [OpportunityURL__c_N/A],
		----a.[Original_Budget__c] as [Original_Budget__c_N/A],
		----a.[Performance_Bond_Percentage__c] as [Performance_Bond_Percentage__c_N/A],
		----a.[Performance_Bond_Required__c] as [Performance_Bond_Required__c_N/A],
		----a.[Performed_On_Site_Demo__c] as [Performed_On_Site_Demo__c_N/A],
		----a.[Performed_On_Site_Discovery__c] as [Performed_On_Site_Discovery__c_N/A],
		----a.[Pricebook2Id] as [Pricebook2Id_N/A],
		----a.[Pricing_Manager__c] as [Pricing_Manager__c_N/A],
		----a.[Procurement_Plans__c] as [Procurement_Plans__c_N/A],
		----a.[Procurement_Type__c] as [Procurement_Type__c_N/A],
		----a.[Product_Family_WMP__c] as [Product_Family_WMP__c_N/A],
		----a.[Project_Sponsor__c] as [Project_Sponsor__c_N/A],
		----a.[Proposal_Due_Date__c] as [Proposal_Due_Date__c_N/A],
		----a.[Proposal_Request_Received_WMP__c] as [Proposal_Request_Received_WMP__c_N/A],
		----a.[Proposal_Ship_Date__c] as [Proposal_Ship_Date__c_N/A],
		----a.[Proposl_Submitted_WMP__c] as [Proposl_Submitted_WMP__c_N/A],
		----a.[Qty_of_Local_References__c] as [Qty_of_Local_References__c_N/A],
		----a.[Qty_of_Negative_Local_References__c] as [Qty_of_Negative_Local_References__c_N/A],
		----a.[Red_Team_Assignments_Due_Date__c] as [Red_Team_Assignments_Due_Date__c_N/A],
		----a.[Relationship_Quality_w_the_Key_contact__c] as [Relationship_Quality_w_the_Key_contact__c_N/A],
		----a.[Required_Products__c] as [Required_Products__c_N/A],
		----a.[RFP_Contact__c] as [RFP_Contact__c_N/A],
		----a.[RFP_Contact_Address__c] as [RFP_Contact_Address__c_N/A],
		----a.[RFP_Contact_Email__c] as [RFP_Contact_Email__c_N/A],
		----a.[RFP_Contact_ID__c] as [RFP_Contact_ID__c_N/A],
		----a.[RFP_Contact_Number__c] as [RFP_Contact_Number__c_N/A],
		----a.[RFP_Team_CAD_Mobile_911__c] as [RFP_Team_CAD_Mobile_911__c_N/A],
		----a.[RFP_Team_RMS_FBR_Jail_IQ__c] as [RFP_Team_RMS_FBR_Jail_IQ__c_N/A],
		----a.[RFP_Team_Solutions_Architect__c] as [RFP_Team_Solutions_Architect__c_N/A],
		----a.[RMS_Type__c] as [RMS_Type__c_N/A],
		----a.[SAR_Meeting_Required__c] as [SAR_Meeting_Required__c_N/A],
		----a.[Selected_Director_Approved__c] as [Selected_Director_Approved__c_N/A],
		----a.[Selected_VP_Approved__c] as [Selected_VP_Approved__c_N/A],
		----a.[Service_Population_Tier__c] as [Service_Population_Tier__c_N/A],
		----a.[Solutions_Definition_Review_Date__c] as [Solutions_Definition_Review_Date__c_N/A],
		----a.[Source_of_budget_information__c] as [Source_of_budget_information__c_N/A],
		----a.[Source_of_Shortlist_Announcement__c] as [Source_of_Shortlist_Announcement__c_N/A],
		----a.[Split_Opportunity_Owner__c] as [Split_Opportunity_Owner__c_N/A],
		----a.[Stalled__c] as [Stalled__c_N/A],
		----a.[SystemModstamp] as [SystemModstamp_N/A],
		----a.[Target_Opp_Scoring_Account_Plan_Created__c] as [Target_Opp_Scoring_Account_Plan_Created__c_N/A],
		----a.[Target_Opp_Scoring_Demo__c] as [Target_Opp_Scoring_Demo__c_N/A],
		----a.[Target_Opp_Scoring_Meeting_In_Person__c] as [Target_Opp_Scoring_Meeting_In_Person__c_N/A],
		----a.[Target_Opp_Scoring_Quote_Created__c] as [Target_Opp_Scoring_Quote_Created__c_N/A],
		----a.[Target_Opportunity__c] as [Target_Opportunity__c_N/A],
		----a.[Target_Opportunity_Paid__c] as [Target_Opportunity_Paid__c_N/A],
		----a.[Target_Opportunity_Score__c] as [Target_Opportunity_Score__c_N/A],
		----a.[Type_of_Selection__c] as [Type_of_Selection__c_N/A],
		----a.[What_is_the_potential_deal_size__c] as [What_is_the_potential_deal_size__c_N/A],
		----a.[What_is_the_purchasing_structure__c] as [What_is_the_purchasing_structure__c_N/A],
		----a.[What_is_TriTech_s_competitive_position__c] as [What_is_TriTech_s_competitive_position__c_N/A],
		----a.[Win_Strategy_PDP_Date__c] as [Win_Strategy_PDP_Date__c_N/A],
		----a.[Z_AE_Have_they_seen_the_product__c] as [Z_AE_Have_they_seen_the_product__c_N/A],
		----a.[Z_AE_of_visits__c] as [Z_AE_of_visits__c_N/A],
		----a.[Z_AE_What_are_our_win_themes__c] as [Z_AE_What_are_our_win_themes__c_N/A],
		----a.[Z_AE_Who_are_the_decision_makers__c] as [Z_AE_Who_are_the_decision_makers__c_N/A],
		----a.[Z_Approval_Date__c] as [Z_Approval_Date__c_N/A],
		----a.[Z_Are_there_product_gaps__c] as [Z_Are_there_product_gaps__c_N/A],
		----a.[Z_CAD_Advanced__c] as [Z_CAD_Advanced__c_N/A],
		----a.[Z_Civil_Advanced__c] as [Z_Civil_Advanced__c_N/A],
		----a.[Z_Close_Date_Notes__c] as [Z_Close_Date_Notes__c_N/A],
		----a.[Z_Concurrent_Users__c] as [Z_Concurrent_Users__c_N/A],
		----a.[Z_Content_creation_notes__c] as [Z_Content_creation_notes__c_N/A],
		----a.[Z_Contract_concerns__c] as [Z_Contract_concerns__c_N/A],
		----a.[Z_Core_State__c] as [Z_Core_State__c_N/A],
		----a.[Z_Decision__c] as [Z_Decision__c_N/A],
		----a.[Z_Decision_Comments__c] as [Z_Decision_Comments__c_N/A],
		----a.[Z_Do_we_have_to_be_on_state_contract__c] as [Z_Do_we_have_to_be_on_state_contract__c_N/A],
		----a.[Z_Due_Date__c] as [Z_Due_Date__c_N/A],
		----a.[Z_Financial_Advanced__c] as [Z_Financial_Advanced__c_N/A],
		----a.[Z_Jail_Advanced__c] as [Z_Jail_Advanced__c_N/A],
		----a.[Z_Meet_reference_requirements__c] as [Z_Meet_reference_requirements__c_N/A],
		----a.[Z_Modules__c] as [Z_Modules__c_N/A],
		----a.[Z_Municipal_Courts_Advanced__c] as [Z_Municipal_Courts_Advanced__c_N/A],
		----a.[Z_Other_Agencies__c] as [Z_Other_Agencies__c_N/A],
		----a.[Z_Personnel_Advanced__c] as [Z_Personnel_Advanced__c_N/A],
		----a.[Z_Product_Experts__c] as [Z_Product_Experts__c_N/A],
		----a.[Z_Project_timeline_concerns__c] as [Z_Project_timeline_concerns__c_N/A],
		----a.[Z_Records_Advanced__c] as [Z_Records_Advanced__c_N/A],
		----a.[Z_RFP_timeline_concerns__c] as [Z_RFP_timeline_concerns__c_N/A],
		----a.[Z_SOW_Complete__c] as [Z_SOW_Complete__c_N/A],
		----a.[Z_State_Specific_things_needed__c] as [Z_State_Specific_things_needed__c_N/A],
		----a.[Z_Summary__c] as [Z_Summary__c_N/A],
		'' as blank
		INTO Opportunity_Load_PB
		FROM Tritech_PROD.dbo.Opportunity a
		LEFT OUTER JOIN Superion_FULLSB.dbo.Account trg_Acct ON a.AccountID = trg_Acct.LegacySFDCAccountId__c
		LEFT OUTER JOIN Tritech_PROD.dbo.RecordType Src_RT ON a.RecordTypeID = Src_RT.ID
		LEFT OUTER JOIN Superion_FULLSB.dbo.RecordType Trg_RT ON Trg_RT.[Name] = CASE Src_RT.[Name]
																						WHEN 'TriTech Competitive' THEN 'Products and Services'
																						WHEN 'TriTech Non-Competitive' THEN 'Products and Services'
																						WHEN 'Zuercher Competitive' THEN 'Products and Services'
																						WHEN 'Zuercher Non-Competitive' THEN 'Products and Services'
																						WHEN 'TriTech Change Order' THEN 'Change Order'
																						WHEN 'Zuercher Change Order' THEN 'Change Order'
																						ELSE 'Products and Services' END
		-- Created By
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON a.[CreatedByID] = [CreatedBy].Legacy_Tritech_Id__c
		-- Owner 
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] OwnerID ON a.[OwnerID] = [OwneriD].Legacy_Tritech_ID__c
		-- Contract Capture Manager
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] ConCapMgr ON a.[Contract_Capture_Manager__c] = ConCapMgr.Legacy_Tritech_ID__c
		-- Incumbent Software Vendor
		LEFT OUTER JOIN Staging_SB.dbo.[map_Competitor_Vendor] IncumbentVdr ON Cast(a.[Incumbent_to_be_Replaced__c] as varchar(80)) = IncumbentVdr.Tritech_Competitor
		-- Proposal Manager
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] PropMgr ON a.Proposal_Writer__c = PropMgr.Legacy_Tritech_Id__c
		-- Winning Competitor
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] WinComp ON a.[Winning_Competitor__c] = WinComp.Legacy_Tritech_Id__c
		-- Signatory
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Contact] Signatory ON a.[Z_Signatory__c] = Signatory.Legacy_ID__c
		
		WHERE 
		(IsClosed = 'false' OR (IsClosed = 'true' AND CloseDate > Cast('1/1/2016' as datetime)))

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.Opportunity_Load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'upsert:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'Opportunity_Load_PB', 'Legacy_Opportunity_ID__c'


--select * from Opportunity_Load_PB where error not like '%success%' and error not like '%createdbyid%'




---------------------------------------------------------------------------------
-- Load Associated Opportunities
---------------------------------------------------------------------------------

Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Opportunity_AssociatedOppty_PB') 
DROP TABLE Staging_SB.dbo.Opportunity_AssociatedOppty_PB;

---------------------------------------------------------------------------------
USE Superion_FULLSB
Exec SF_Replicate 'PB_Superion_FullSB', 'Opportunity' 

USE Staging_SB

select 
oppty.Id [Id], 
Cast('' as nvarchar(255)) as Error, 
assoc.Id [Associated_Opportunity__c]
into Staging_SB.dbo.Opportunity_AssociatedOppty_PB
from Tritech_Prod.dbo.Opportunity a
INNER JOIN Superion_FullSB.dbo.[Opportunity] oppty ON a.Id =  oppty.Legacy_Opportunity_ID__c
INNER JOIN Superion_FullSB.dbo.[Opportunity] assoc ON a.Associated_Opportunity__c =  assoc.Legacy_Opportunity_ID__c
where a.Associated_Opportunity__c is not null;

Use Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(5000)', 'PB_Superion_FullSB', 'Opportunity_AssociatedOppty_PB';