-------------------------------------------------------------------------------
--- Account Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: October 31, 2019
--- Last Updated: November 8, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 1. Script requires the creation of the table Staging_sb.dbo.map_Competitor_Vendor.  Source of data is the map_competitor_vendor worksheet on the TriTech/Central Square Mapping Master
--- spreadsheet located at url: docs.google.com/spreadsheets/d/1SEv8gVxzKn1qj_Rn1YwNrobS6GUE7sWHBzQ5IPUuRnw/edit#gid=1127580252
--- 2. Joins which relate to the map_Competitor_Vendor table are set to reference 'SandboxID'.  These references should be set to 'ProductionID' prior to execution in production.
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Account_Load_PB') 
DROP TABLE Staging_SB.dbo.Account_Load_PB;


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Replicate 'PB_Tritech_Prod', 'User';
Exec SF_Replicate 'PB_Tritech_Prod', 'Account' ;
Exec SF_Replicate 'PB_Tritech_Prod', 'RecordType' ;

Use Superion_FUllSB
Exec SF_Replicate 'PB_Superion_FullSB', 'User'
Exec SF_Replicate 'PB_Superion_FullSB', 'RecordType' ;
--Exec SF_Replicate 'PB_Superion_FullSB', 'Competitor__c' ;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
Use Staging_SB;

WITH USER_Conference_Attendee AS
(
		Select CASE value 
					WHEN '2016' THEN 'z1 2016'
					WHEN '2017' THEN 'z1 2017'
					WHEN '2018' THEN 'z1 2018'
					WHEN '2019' THEN 'z1 2019'
					ELSE '' END as Conference,
		ID FROM Tritech_Prod.dbo.Account
		CROSS Apply string_split(Cast([Z1_Attendee__c] as varchar(100)), ';')
		WHERE Z1_Attendee__c is not null

		UNION
		Select CASE value
					WHEN '2016' THEN 'TriCON 2016'
					WHEN '2017' THEN 'TriCON 2017'
					WHEN '2018' THEN 'TriCON 2018'
					WHEN '2019' THEN 'TriCON 2019'
					ELSE '' END as Conference,
		ID From Tritech_Prod.dbo.Account
		CROSS Apply String_split(CAST(TriCon_Attendee__c as varchar(100)), ';')
		WHERE TriCon_Attendee__c is not null
)

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as LegacySFDCAccountId__c,
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		isNull(a.[Entity_Name__c], '') as Entity_Name__c,
		isNull(a.[Active_Client_Product_Families__c], '') as Legacy_TT_Client_Product_Families__c,
		isNull(a.[Active_Contract_Product_Groups__c], '') as Legacy_TT_Client_Product_Groups__c,
		isNull(a.[Advertise_Relationship__c], '') as Advertise_Relationship__c,
		isNull(Convert(Varchar(19), a.[Agreement_Effective_Date__c], 120), '') as Agreement_Effective_Date__c,
		isNull(a.[Agreement_Status__c], '') as Agreement_Status__c,
		isNull(a.[Alarm_Management_Vendor__c], '') as Alarm_Mgmt_Current_SW_Vendor__Original,
		isNull(AlarmMgtVendor.SandboxID, '') as Alarm_Mgmt_Current_SW_Vendor__c,  -- update field based on sandbox/producton environment
		isNull(Cast(a.[Average_Inmate_Daily_Population__c] as varchar(10)), '') as Average_Inmate_Daily_Population__c,
		isNull(a.[Billing_Installed_Year__c], '') as Billing_Installed_Year__c,
		--isNULL(a.[Billing_Vendor__c], '') as Billing_Current_Vendor__c_Original,
		--isNull(BillingVendor.SandboxID, '') as Billing_Current_Vendor__c,  -- update field based on sandbox/producton environment  -- Remmed out due to field unavailable/configuration needed
		a.[Bomgar_Approved_Client__c] as Bomgar_Approved_Client__c,
		isNull(Cast(a.[CAD_Dispatcher_Call_Taker_Seats__c] as varchar(12)), '') as CAD_Dispatcher_Call_Taker_Seats__c,
		CASE WHEN isNumeric(a.[CAD_Installed_Year__c]) = 1 THEN isNull(Cast(a.[CAD_Installed_Year__c] as varchar(4)), '')
			ELSE '' END as Last_SW_Purchase_Date_CAD__c,
		isNull(Cast(a.[CAD_Parent_Fire_EMS_Users__c] as varchar(10)), '') as CAD_Parent_Fire_EMS_Users__c,
		isNull(Cast(a.[CAD_Parent_Total_Sworn_Officers__c] as varchar(10)), '') as CAD_Parent_Total_Sworn_Officers__c,
		CASE WHEN a.[CAD_PSAP_Type__c] is not null THEN 'true' ELSE 'false' END as PSAP__c,
		isNull(a.[CAD_PSAP_Type__c], '') as PSAP_Type__c, 
		isNull(a.[CAD_Vendor__c], '') as CurrentCADSWVendor__c_Original,
		isNull(CADVendor.SandboxID, '') as CurrentCADSWVendor__c,  -- update field based on sandbox/producton environment  
		isNull(Cast(a.[Call_Volume_Annual__c] as varchar(10)), '') as Annual_Calls_for_Service_Received__c,
		a.Client__c as Client__c_Original,
		SrcRT.[Name] as RecordType_Original,
		TrgRT.ID as RecordTypeID,  -- Logic in Join statement for determining RecordType
		isNull(a.[Contract_Types__c], '') as Legacy_TT_Active_Contract_Types__c,  
		isNull(a.[County__c], '') as County__c, 
		isNull(a.[County__c], '') as Ship_To_County__c,
		--CreatedBy.ID as CreatedByID,
		--a.CreatedDate as CreatedDate,
		isNull(a.[Description], '') as TT_Legacy_Description__c,
		isNull(a.[EMS_Customer_Number_WMP__c], '') as Legacy_TT_Customer_Number__c,
		CASE WHEN isNumeric(a.[ePCR_Installed_Year__c]) = 1 THEN isNull(a.[ePCR_Installed_Year__c], '')
				ELSE '' END as Last_SW_Purchase_Date_ePCR__c,
		--isNull(a.[ePCR_Vendor__c], '') as ePCR_Current_Vendor__c_Original,
		--isNull(PCRVendor.SandboxID, '') as ePCR_Current_Vendor__c,  -- update field based on sandbox/producton environment -- Remmed out due to field unavailable/configuration needed
		isNull(a.[Fax], '') as [Fax],
		CASE WHEN isNumeric(a.[FBR_Installed_Year__c]) = 1 THEN isNull(a.[FBR_Installed_Year__c], '') 
				ELSE '' END as Last_SW_Purchase_Date_FBR__c, 
		--isNull(a.[FBR_Vendor__c], '') as FBR_Current_Vendor__c_Original,
		--isNull(FBRVendor.SandboxID, '') as FBR_Current_Vendor__c, -- update field based on sandbox/producton environment -- Remmed out due to field unavailable/configuration needed
		isNull(Coalesce(a.[ORI__C], a.[FDID__c]), '') as ORI__c,
		isNull(Cast(a.[Fire_Career__c] as varchar(10)), '') as Fire_Career__c,
		isNull(Cast(a.[Fire_Civilian_Personnel__c] as varchar(10)), '') as Fire_Civilian_Personnel__c,
		isNull(Cast(a.[Fire_Fighter_Paid_Per_Call__c] as varchar(10)), '') as Fire_Fighter_Paid_Per_Call__c,
		CASE WHEN isNumeric(a.[Fire_Installed_Year__c]) = 1 THEN isNull(a.[Fire_Installed_Year__c], '') 
				ELSE '' END as Last_SW_Purchase_Date_Fire_RMS__c, 
		isNull(Cast(a.[Fire_Mobile_Units__c] as varchar(10)), '') as Fire_Mobile_Units__c,
		isNull(Cast(a.[Fire_RMS_Users__c] as varchar(10)), '') as Fire_RMS_Users__c,
		--isNull(a.[Fire_Vendor__c], '') as Fire_RMS_Current_Vendor__c_Original,
		--isNull(FireVendor.SandboxID, '') as Fire_RMS_Current_Vendor__c,  -- update field based on sandbox/producton environment -- Remmed out due to field unavailable/configuration needed
		isNull(Cast(a.[Fire_Volunteer_Firefighting__c] as varchar(18)), '') as Fire_Volunteer_Firefighting__c,
		isNull(Cast(a.[Fire_Volunteer_Non_Firefighting__c] as varchar(18)), '') as Fire_Volunteer_Non_Firefighting__c,
		isNull(a.[Inactive_Contract_Types__c], '') as Legacy_TT_Inactive_Contract_Types__c,
		isNull(Cast(a.[Incident_Volume__c] as varchar(10)), '') as Yearly_Incident_Report_Volume__c,
		isNull(Cast(a.[Jail_Beds__c] as varchar(18)), '') as NumberofBeds__c,
		isNull(a.[Jail_Installed_Year__c], '') as Last_SW_Purchase_Date_JMS__c,
		isNull(a.[Jail_Vendor__c], '') as Current_Justice_SW_Vendor__c_Original,
		isNull(JailVendor.SandboxID, '') as Current_Justice_SW_Vendor__c,  -- update field based on sandbox/producton environment  
		isNull(Cast(a.[Law_Civilian_Personnel__c] as varchar(18)), '') as Law_Civilian_Personnel__c,
		isNull(Cast(a.[Law_Mobile_Units__c] as varchar(18)), '') as NumberofMobileUnits__c,
		isNull(Cast(a.[Law_RMS_Users__c] as varchar(18)), '') as Law_RMS_Users__c,
		isNull(a.[Mailing_Billing_City_WMP__c], '') as BillingCity,
		isNull(a.[Mailing_Billing_Country_WMP__c], '') as BillingCountry,
		isNull(a.[Mailing_Billing_State_WMP__c], '') as BillingState,
		isNull(a.[Mailing_Billing_Street_WMP__c], '') as BillingStreet,
		isNull(a.[Mailing_Billing_Zip_Postal_Code_WMP__c], '') as BillingPostalCode,
		isNull(a.[Mapping__c], '') as Last_SW_Purchase_Date_Mapping__c,
		--isNull(a.[Mapping_Vendor__c], '') as Mapping_Current_Vendor__c_original,
		--isNull(MappingVendor.SandboxID, '') as Mapping_Current_Vendor__c, -- update field based on sandbox/producton environment -- Remmed out due to field unavailable/configuration needed
		CASE WHEN isNumeric(a.[Mobile_Installed_Year__c]) = 1 THEN isNull(a.[Mobile_Installed_Year__c], '')
				ELSE '' END as Last_SW_Purchase_Date_Mobile__c,
		isNull(a.[Mobile_Vendor__c], '') as Current_Mobile_SW_Vendor__c_original,
		isNull(MobileVendor.SandboxID, '') as Current_Mobile_SW_Vendor__c, -- update field based on sandbox/producton environment  
		CASE 
			WHEN SrcRt.[Name] = 'Agency' THEN 
												CASE WHEN substring(Right(a.[Name], 3), 1, 1) = ' '
												THEN Concat(Substring(a.[Name], 1, LEN(a.[Name]) - 3), ', ', Right(a.[Name], 2))
												ELSE a.[Name] END
			ELSE a.[Name] End as [Name],
		isNull(CONVERT(VARCHAR(19), a.[NDA_Expiration__c] , 120), '') as NDA_Expiration__c,
		isNull(a.[NDA_in_Place__c], '') as NDA_in_Place__c,
		isNull(CAST(a.[Number_of_Sworn_Personnel_WMP__c] as varchar(18)),'') as Number_of_Sworn_Personnel_WMP__c,
		isNull(Cast(a.[NumberOfEmployees] as varchar(18)), '') as NumberOfEmployees,
		isNull(Cast(a.[of_Fire_Stations__c] as varchar(18)), '') as of_Fire_Stations__c,
		isNull(a.[of_firefighters_at_largest_PSAP_agency__c], '') as of_firefighters_at_largest_PSAP_agency__c,  
		isNull(Cast(a.[of_Jail_Users__c] as varchar(18)), '') as of_Jail_Users__c,
		[Owner].ID as [OwnerID],
		a.[OwnerID] as [Owner_Original],
		isNull(CASE a.[Client__c] WHEN 'false' THEN AE.ID ELSE NULL END, '') as Account_Executive_Fin__c,
		isNull(CASE a.[Client__c] WHEN 'true' THEN AE.ID ELSE NULL END, '') as Installed_PA__c,
		isNull(a.[Parent_Active_Client_Product_Families__c], '') as Legacy_TT_Parent_Active_Client_Product_F__c,
		isNull(a.[Parent_Active_Contract_Types__c], '') as Legacy_TT_Parent_Active_Contract_Types__c,
		isNull(a.[Parent_Info_Not_Validated__c], '') as Parent_Info_Not_Validated__c,
		--a.[ParentId] as Parent,
		isNull(a.[Phone], '') as Phone,
		isNull(CAST(a.[Population_of_Area_Served_WMP__c] as varchar(18)), '') as Population__c,
		isNull(a.[Reference_List_Use__c], '') as Reference_List_Use__c,
		CASE WHEN isNumeric(a.[RMS_Installed_Year__c]) = 1 THEN isNull(Cast(a.[RMS_Installed_Year__c] as varchar(4)), '')
				ELSE '' END as Last_SW_Purchase_Date_Law_RMS__c, 
		isNull(Cast(a.[RMS_Parent_Total_Sworn_Officers__c] as varchar(18)), '') as RMS_Parent_Total_Sworn_Officers__c,
		--isNull(a.[RMS_Vendor__c], '') as Law_RMS_Current_Vendor__c_original,
		--isNull(LawVendor.SandboxID, '') as Law_RMS_Current_Vendor__c,  -- update field based on sandbox/producton environment -- Remmed out due to field unavailable/configuration needed
		isNull(a.[Sales_Tax_ID__c], '') as Sales_Tax_ID__c,
		isNull(Cast(a.[Sales_Tax_Rate__c] as varchar(19)), '') as Sales_Tax_Rate__c,
		isNull(a.[Sensitive_Account_WMP__c], '') as Account_Notes__c,
		isNull(a.[Sensitive_Account_WMP__c], '') as SupportNotes__c,
		isNull(a.[Service_Type__c], '') as PS_Agency_Type__c,  
		isNull(a.[Shipping_City_WMP__c], '') as ShippingCity,
		isNull(a.[Shipping_Country_WMP__c], '') as ShippingCountry,
		isNull(a.[Shipping_State_WMP__c], '') as ShippingState,
		isNull(a.[Shipping_Street_WMP__c], '') as ShippingStreet,
		isNull(a.[Shipping_Zip_Postal_Code__c], '') as ShippingPostalCode, 
		isNull(a.[TriTech_Team_Manager__c], '') as TriTech_Team_Manager__c,
		isNull(a.[Website], '') as Website,
		isNull(a.[X3rd_Party_Type__c], '') as X3rd_Party_Type__c,
		isNull(a.[X911_Call_Taker_Positions__c], '') as X911_Call_Taker_Positions__c,
		isNull(Cast(a.[X911_Parent_Total_Sworn_Officers__c] as varchar(18)), '') as X911_Parent_Total_Sworn_Officers__c,
		isNull(a.[X911_System_Manufacturer__c], '') as [x911_Current_Vendor__c_Original] ,
		isNull([911Vendor].SandboxID, '') as [x911_Current_Vendor__c] ,  -- update field based on sandbox/producton environment
		CASE WHEN isNumeric(a.[X911_Telephony_Installed_Year__c]) = 1 THEN isNull(a.[X911_Telephony_Installed_Year__c], '') 
				ELSE '' END as Last_SW_Purchase_Date_911__c,
		a.[Z_Data_Conversion_Analyst__c] as Data_Conversion_Analyst__c_Original,
		isNull(DataAnalyst.ID, '') as Data_Conversion_Analyst__c,  
		a.[Z_GIS_Analyst__c] as GIS_Analyst__c_Original,
		isNull(GISAnalyst.ID, '') as GIS_Analyst__c, 
		isNull(a.[Z_Install_Name__c],'') as Install_Name__c,
		isNull(STUFF ((Select ';' + [Conference].[Conference] as [text()]
				FROM USER_Conference_Attendee Conference
				WHERE Conference.id = a.id
				FOR XML PATH('') ), 1, 1, ''), '')
				AS User_Conference_Attendee__c,   
		isNull(a.[Account_At_Risk__c], '') as TT_Account_At_Risk__c,
		isNull(a.[Account_Status__c], '') as TT_Account_Status__c,
		isNull(a.[Actions_Taken_Required__c], '') as Actions_Taken_Required__c,
		isNull(a.[Comments__c], '') as TT_Account_Status_Comments__c,
		isNull(a.[Issues_Problems__c], '') as TT_Account_Status_Issues_Problems__c, 
		isNull(a.[Last_Modified_Actions__c], '') as Last_Modified_Actions__c,
		isNull(a.[Last_Modified_Comments__c], '') as Last_Modified_Comments__c,
		isNull(a.[Last_Modified_Issues__c], '') as Last_Modified_Issues__c,
		isNull(Cast(a.[Client_Relations_Contact_Goal__c] as varchar(18)), '') as Wellness_Check_Contact_Goal__c,
		isNull(Cast(a.[Z_Account_Renewal_Confidence__c] as varchar(18)), '') as Account_Renewal_Confidence__c,
		isNull(a.[Z_Account_Renewal_Notes__c], '') as Account_Renewal_Notes__c,
		isNull(a.[Z_Change_in_Leadership__c], '') as Change_in_Leadership__c,
		isNull(Cast(a.[Z_Completed_Yearly_Contacts__c] as varchar(18)), '') as Completed_Yearly_Wellness_Checks__c,
		isNull(CONVERT(VARCHAR(19), a.[Z_Last_Yearly_Contact_Date__c], 120), '') as Last_Wellness_Check_Date__c,
		isNull(a.[Z_Last_Yearly_Contact_Type__c], '') as Last_Wellness_Check_Type__c,
		isNull(a.[Z_Low_Agency_Ownership_User_Adoption__c], '') as Low_Agency_Ownership_user_Adoption__c,
		isNull(a.[Z_Maintenance_Lapse__c], '') as Maintenance_Lapse__c,
		isNull(CONVERT(Varchar(19), a.[Z_Next_Renewal_Date__c], 120), '') as Next_Renewal_Date__c,
		isNull(a.[Z_No_Communication_From_Customer__c], '') as No_Communication_From_Customer__c,
		isNull(a.[Z_Not_a_Full_Suite_Customer__c], '') as Not_a_Full_Suite_Customer__c,
		isNull(a.[Z_Renewal_Expected_Next_Year__c], '') as Renewal_Expected_Next_Year__c,
		isNull(CAST(a.[Z_Total_Maintenance_Amount__c] as varchar(19)), '') as Total_Maintenance_Amount__c,
		isNull(a.[Z_Vocal_with_Issues__c], '') as Vocal_with_Issues__c
		--a.[Account_Manager__c] as [Account_Manager__c_Config],
		--a.[Assigned_BA_1__c] as [Assigned_BA_1__c_Config],
		--a.[Assigned_BA_2__c] as [Assigned_BA_2__c_Config],
		--a.[Assigned_BA_3__c] as [Assigned_BA_3__c_Config],
		--a.[Assigned_PM__c] as [Assigned_PM__c_Config],
		--a.[Assigned_PM_2__c] as [Assigned_PM_2__c_Config],
		--a.[CAD_Support_Manager__c] as [CAD_Support_Manager__c_Config],
		--a.[Customer_Success_Liaison__c] as Customer_Success_Liaison__c_Config,
		--a.[IQ_Account_Owner__c] as IQ_Account_Owner__c_Config,
		--a.[Resale_Company__c] as Resale_Company__c_Config,
		--a.[RMS_Jail_Support_Manager__c] as RMS_Jail_Support_Manager__c_Config,
		--a.[RMS_Parent_Account__c] as RMS_Parent_Account__c_Config,
		--a.[X911_Account_Owner__c] as X911_Account_Owner__c_Config,
		--a.[X911_Parent_Account__c] as X911_Parent_Account__c_Config,
		--a.[X911_Support_Manager__c] as X911_Support_Manager__c_Config,
		--a.[Z_Implementation_Analyst__c] as Z_Implementation_Analyst__c_Config,
		--a.[Z_Implementation_Analyst_2__c] as Z_Implementation_Analyst_2__c_Config,
		--a.[Z_Interface_Analyst__c] as Z_Interface_Analyst__c_Config,
		--a.[Z_Project_Manager__c] as Z_Project_Manager__c_Config,
		--a.[AccountSource] as AccountSource_FollowUp,
		--a.[Actions_Taken_Required__c] as Actions_Taken_Required__c_FollowUp,
		--a.[Insight_SQL_Server_Address__c] as Insight_SQL_Server_Address__c_FollowUp,
		--a.[Insight_Version__c] as Insight_Version__c_FollowUp,
		--a.[Planning_to_replace_911_provider__c] as Planning_to_replace_911_provider__c_FollowUp,
		--a.[Planning_to_replace_CAD_provider__c] as Planning_to_replace_CAD_provider__c_FollowUp,
		--a.[Planning_to_replace_Jail_provider__c] as Planning_to_replace_Jail_provider__c_FollowUp,
		--a.[Planning_to_replace_RMS_provider__c] as Planning_to_replace_RMS_provider__c_FollowUp,
		--a.[Post_Go_Live_Admin_Satisfaction_Survey__c] as Post_Go_Live_Admin_Satisfaction_Survey__c_FollowUp,
		--a.[Post_Go_Live_User_Satisfaction_Survey__c] as Post_Go_Live_User_Satisfaction_Survey__c_FollowUp,
		--a.[Relationship_w_Decision_Maker_Sponsor__c] as Relationship_w_Decision_Maker_Sponsor__c_FollowUp,
		--a.[Secure_Folder_ID__c] as Secure_Folder_ID__c_FollowUp,
		--a.[UCR_NIBRS__c] as UCR_NIBRS__c_FollowUp
		--a.[Account_Score__c] as Account_Score__c-N/A,
		--a.[Account_Score_Complete__c] as Account_Score_Complete__c-N/A,
		--a.[Account_Score_Director_Approved__c] as Account_Score_Director_Approved__c-N/A,
		--a.[Account_Score_Needed__c] as Account_Score_Needed__c-N/A,
		--a.[Account_Scoring_Comments__c] as Account_Scoring_Comments__c-N/A,
		--a.[Active__c] as Active__c-N/A,
		--a.[Active_Service_Contract_Value_Rollup__c] as Active_Service_Contract_Value_Rollup__c-N/A,
		--a.[Admin_Contact__c] as Admin_Contact__c-N/A,
		--a.[Agency_Facebook_Address__c] as Agency_Facebook_Address__c-N/A,
		--a.[Agreement_Expiration__c] as Agreement_Expiration__c-N/A,
		--a.[Agreement_Location__c] as Agreement_Location__c-N/A,
		--a.[AM_Migration_Target__c] as AM_Migration_Target__c-N/A,
		--a.[AM_Migration_Target_Complete__c] as AM_Migration_Target_Complete__c-N/A,
		--a.[Area_Served_Sq_Miles__c] as Area_Served_Sq_Miles__c-N/A,
		--a.[Assets_Audited_Date__c] as Assets_Audited_Date__c-N/A,
		--a.[Bad_Debt_WMP__c] as Bad_Debt_WMP__c-N/A,
		--a.[BDS_Discovery__c] as BDS_Discovery__c-N/A,
		--a.[BillingCity] as BillingCity-N/A,
		--a.[BillingCountry] as BillingCountry-N/A,
		--a.[BillingGeocodeAccuracy] as BillingGeocodeAccuracy-N/A,
		--a.[BillingLatitude] as BillingLatitude-N/A,
		--a.[BillingLongitude] as BillingLongitude-N/A,
		--a.[BillingPostalCode] as BillingPostalCode-N/A,
		--a.[BillingState] as BillingState-N/A,
		--a.[BillingStreet] as BillingStreet-N/A,
		--a.[CAD_BA_Email__c] as CAD_BA_Email__c-N/A,
		--a.[CAD_Go_Live_Date__c] as CAD_Go_Live_Date__c-N/A,
		--a.[CAD_Parent_Sworn_Officer_Band__c] as CAD_Parent_Sworn_Officer_Band__c-N/A,
		--a.[Campaign__c] as Campaign__c-N/A,
		--a.[Clearinghouse_Vendor__c] as Clearinghouse_Vendor__c-N/A,
		--a.[Client_Tiburon__c] as Client_Tiburon__c-N/A,
		--a.[Client_Zuercher__c] as Client_Zuercher__c-N/A,
		--a.[Comm_Center__c] as Comm_Center__c-N/A,
		--a.[Competitive_position_with_this_Prospect__c] as Competitive_position_with_this_Prospect__c-N/A,
		--a.[Core_Company__c] as Core_Company__c-N/A,
		--a.[Customer_ID__c] as Customer_ID__c-N/A,
		--a.[Delete_Ken_Test_Child_Accounts__c] as Delete_Ken_Test_Child_Accounts__c-N/A,
		--a.[DummyTouchField__c] as DummyTouchField__c-N/A,
		--a.[EMS_INET_Password__c] as EMS_INET_Password__c-N/A,
		--a.[Estimated_System_Replacement_Year__c] as Estimated_System_Replacement_Year__c-N/A,
		--a.[Executive_Sponsor__c] as Executive_Sponsor__c-N/A,
		--a.[FBR_BA_Email__c] as FBR_BA_Email__c-N/A,
		--a.[Fiscal_Anniversary_Date__c] as Fiscal_Anniversary_Date__c-N/A,
		--a.[Go_Live_Date_911__c] as Go_Live_Date_911__c-N/A,
		--a.[Go_Live_Date_Accident_Reporting__c] as Go_Live_Date_Accident_Reporting__c-N/A,
		--a.[Go_Live_Date_Admin__c] as Go_Live_Date_Admin__c-N/A,
		--a.[Go_Live_Date_CAD__c] as Go_Live_Date_CAD__c-N/A,
		--a.[Go_Live_Date_Civil__c] as Go_Live_Date_Civil__c-N/A,
		--a.[Go_Live_Date_Community_Data_Platform__c] as Go_Live_Date_Community_Data_Platform__c-N/A,
		--a.[Go_Live_Date_Crimemapping_com__c] as Go_Live_Date_Crimemapping_com__c-N/A,
		--a.[Go_Live_Date_Crimeview_FireView_Dashbrd__c] as Go_Live_Date_Crimeview_FireView_Dashbrd__c-N/A,
		--a.[Go_Live_Date_Crimeview_Fireview_Deskto__c] as Go_Live_Date_Crimeview_Fireview_Deskto__c-N/A,
		--a.[Go_Live_Date_Detective__c] as Go_Live_Date_Detective__c-N/A,
		--a.[Go_Live_Date_eCitation__c] as Go_Live_Date_eCitation__c-N/A,
		--a.[Go_Live_Date_Financial__c] as Go_Live_Date_Financial__c-N/A,
		--a.[Go_Live_Date_Fire_RMS__c] as Go_Live_Date_Fire_RMS__c-N/A,
		--a.[Go_Live_Date_Inform_Me_Field_Ops__c] as Go_Live_Date_Inform_Me_Field_Ops__c-N/A,
		--a.[Go_Live_Date_IQ_Analytics_ARM__c] as Go_Live_Date_IQ_Analytics_ARM__c-N/A,
		--a.[Go_Live_Date_IQ_Search__c] as Go_Live_Date_IQ_Search__c-N/A,
		--a.[Go_Live_Date_IQ_Suite__c] as Go_Live_Date_IQ_Suite__c-N/A,
		--a.[Go_Live_Date_Jail__c] as Go_Live_Date_Jail__c-N/A,
		--a.[Go_Live_Date_Law_RMS__c] as Go_Live_Date_Law_RMS__c-N/A,
		--a.[Go_Live_Date_Mapping__c] as Go_Live_Date_Mapping__c-N/A,
		--a.[Go_Live_Date_Mobile__c] as Go_Live_Date_Mobile__c-N/A,
		--a.[Go_Live_Date_NEARme__c] as Go_Live_Date_NEARme__c-N/A,
		--a.[Go_Live_Date_Predictive_Missions__c] as Go_Live_Date_Predictive_Missions__c-N/A,
		--a.[Go_Live_Date_Respond_Billing__c] as Go_Live_Date_Respond_Billing__c-N/A,
		--a.[Go_Live_Date_Responsemapping_com__c] as Go_Live_Date_Responsemapping_com__c-N/A,
		--a.[Go_Live_Date_Tritech_Com_Biilling__c] as Go_Live_Date_Tritech_Com_Biilling__c-N/A,
		--a.[Hierarchy_Completed_by__c] as Hierarchy_Completed_by__c-N/A,
		--a.[Hierarchy_Completed_by_Manager_Email__c] as Hierarchy_Completed_by_Manager_Email__c-N/A,
		--a.[Hierarchy_Director_Approved__c] as Hierarchy_Director_Approved__c-N/A,
		--a.[Hierarchy_Director_Approved_Last_Modifie__c] as Hierarchy_Director_Approved_Last_Modifie__c-N/A,
		--a.[Hierarchy_Updated__c] as Hierarchy_Updated__c-N/A,
		--a.[Hierarchy_Updated_Last_Modified__c] as Hierarchy_Updated_Last_Modified__c-N/A,
		--a.[Highest_relationship_level_with_Account__c] as Highest_relationship_level_with_Account__c-N/A,
		--a.[Initial_Contract_Signed__c] as Initial_Contract_Signed__c-N/A,
		--a.[Install_Support_Vendor__c] as Install_Support_Vendor__c-N/A,
		--a.[Interfaces_Addons__c] as Interfaces_Addons__c-N/A,
		--a.[Jigsaw] as Jigsaw-N/A,
		--a.[Last_Feature_Request_Saved__c] as Last_Feature_Request_Saved__c-N/A,
		--a.[Last_Modified_At_Risk__c] as Last_Modified_At_Risk__c-N/A,
		--a.[Last_Yearly_Contact_Customer_Risk_Level__c] as Last_Yearly_Contact_Customer_Risk_Level__c-N/A,
		--a.[LastModifiedById] as LastModifiedById-N/A,
		--a.[LastModifiedDate] as LastModifiedDate-N/A,
		--a.[Latitude__c] as Latitude__c-N/A,
		--a.[Legacy_Account_Number__c] as Legacy_Account_Number__c-N/A,
		--a.[Legacy_ETI_ID__c] as Legacy_ETI_ID__c-N/A,
		--a.[Legacy_TCS_HEAT_ID__c] as Legacy_TCS_HEAT_ID__c-N/A,
		--a.[Longitude__c] as Longitude__c-N/A,
		--a.[Manager_Email__c] as Manager_Email__c-N/A,
		--a.[Market_Map_Update__c] as Market_Map_Update__c-N/A,
		--a.[Market_Tier__c] as Market_Tier__c-N/A,
		--a.[Master__c] as Master__c-N/A,
		--a.[Master_Account_Type__c] as Master_Account_Type__c-N/A,
		--a.[Master_Name__c] as Master_Name__c-N/A,
		--a.[Migration_Target__c] as Migration_Target__c-N/A,
		--a.[Mobile_BA_Email__c] as Mobile_BA_Email__c-N/A,
		--a.[Mobile_Go_Live_Date__c] as Mobile_Go_Live_Date__c-N/A,
		--a.[Multi_jurisdictional_system__c] as Multi_jurisdictional_system__c-N/A,
		--a.[NDA_Location__c] as NDA_Location__c-N/A,
		--a.[Nearby_Airports_and_Hotels__c] as Nearby_Airports_and_Hotels__c-N/A,
		--a.[Number_of_Sworn_Officers_Band__c] as Number_of_Sworn_Officers_Band__c-N/A,
		--a.[PM_1_Email__c] as PM_1_Email__c-N/A,
		--a.[PM_2_Email__c] as PM_2_Email__c-N/A,
		--a.[Population_Served__c] as Population_Served__c-N/A,
		--a.[Potential_deal_size_of_this_opportunity__c] as Potential_deal_size_of_this_opportunity__c-N/A,
		--a.[Pricing_Manager__c] as Pricing_Manager__c-N/A,
		--a.[Product_Family__c] as Product_Family__c-N/A,
		--a.[Product_Family_WMP__c] as Product_Family_WMP__c-N/A,
		--a.[PSAP_or_Individual_agency_sworn_Band__c] as PSAP_or_Individual_agency_sworn_Band__c-N/A,
		--a.[qualtrics__Net_Promoter_Score__c] as qualtrics__Net_Promoter_Score__c-N/A,
		--a.[qualtrics__NPS_Date__c] as qualtrics__NPS_Date__c-N/A,
		--a.[Quest_ID_WMP__c] as Quest_ID_WMP__c-N/A,
		--a.[Radio_Installed_Year__c] as Radio_Installed_Year__c-N/A,
		--a.[Radio_Vendor__c] as Radio_Vendor__c-N/A,
		--a.[RecordTypeId] as RecordTypeId-N/A,
		--a.[Related_to_a_Client__c] as Related_to_a_Client__c-N/A,
		--a.[Replace_Window_Needed__c] as Replace_Window_Needed__c-N/A,
		--a.[RMS_BA_Email__c] as RMS_BA_Email__c-N/A,
		--a.[RMS_Go_Live_Date__c] as RMS_Go_Live_Date__c-N/A,
		--a.[ROID__c] as ROID__c-N/A,
		--a.[Secondary_Support_Vendor__c] as Secondary_Support_Vendor__c-N/A,
		--a.[Service_Population_Tier__c] as Service_Population_Tier__c-N/A,
		--a.[ShippingCity] as ShippingCity-N/A,
		--a.[ShippingCountry] as ShippingCountry-N/A,
		--a.[ShippingGeocodeAccuracy] as ShippingGeocodeAccuracy-N/A,
		--a.[ShippingLatitude] as ShippingLatitude-N/A,
		--a.[ShippingLongitude] as ShippingLongitude-N/A,
		--a.[ShippingPostalCode] as ShippingPostalCode-N/A,
		--a.[ShippingState] as ShippingState-N/A,
		--a.[ShippingStreet] as ShippingStreet-N/A,
		--a.[SicDesc] as SicDesc-N/A,
		--a.[Strategic_Development__c] as Strategic_Development__c-N/A,
		--a.[Strong_relationship_w_functional_roles__c] as Strong_relationship_w_functional_roles__c-N/A,
		--a.[Target_Account__c] as Target_Account__c-N/A,
		--a.[Target_Year_to_Replace_911__c] as Target_Year_to_Replace_911__c-N/A,
		--a.[Telemetrry_Enabled__c] as Telemetrry_Enabled__c-N/A,
		--a.[Test_List__c] as Test_List__c-N/A,
		--a.[Top_100_PSAP__c] as Top_100_PSAP__c-N/A,
		--a.[Top_200_PSAP__c] as Top_200_PSAP__c-N/A,
		--a.[Total_Fire_EMS__c] as Total_Fire_EMS__c-N/A,
		--a.[Traffic_Citation_eFiling__c] as Traffic_Citation_eFiling__c-N/A,
		--a.[Type] as Type-N/A,
		--a.[Validated_Date__c] as Validated_Date__c-N/A,
		--a.[Voice_Logging_Recorder_Installed_Year__c] as Voice_Logging_Recorder_Installed_Year__c-N/A,
		--a.[Voice_Logging_Recorder_Recording_Type__c] as Voice_Logging_Recorder_Recording_Type__c-N/A,
		--a.[Voice_Logging_Recorder_Vendor__c] as Voice_Logging_Recorder_Vendor__c-N/A,
		--a.[What_activity__c] as What_activity__c-N/A,
		--a.[What_is_Prospect_s_purchasing_process__c] as What_is_Prospect_s_purchasing_process__c-N/A,
		--a.[when_activity__c] as when_activity__c-N/A,
		--a.[Who_Activity__c] as Who_Activity__c-N/A,
		--a.[x2016_Maintenance_Fee_Snap_Shot__c] as x2016_Maintenance_Fee_Snap_Shot__c-N/A,
		--a.[X911_Comments__c] as X911_Comments__c-N/A,
		--a.[X911_Replacement_Vendor_Choice__c] as X911_Replacement_Vendor_Choice__c-N/A,
		--a.[X911_System_Go_Live_Date__c] as X911_System_Go_Live_Date__c-N/A,
		--a.[X911_System_Model__c] as X911_System_Model__c-N/A,
		--a.[Z_BDS_NPS_Survey_Date__c] as Z_BDS_NPS_Survey_Date__c-N/A,
		--a.[Z_BDS_NPS_Survey_Score__c] as Z_BDS_NPS_Survey_Score__c-N/A,
		--a.[Z_Interfaces__c] as Z_Interfaces__c-N/A,
		--a.[Z_LETG_Version__c] as Z_LETG_Version__c-N/A,
		--a.[Z_Other_Competitor_Notes__c] as Z_Other_Competitor_Notes__c-N/A,
		--a.[Z_Previous_Vendor__c] as Z_Previous_Vendor__c-N/A,
		--a.[Z_Product_Lines__c] as Z_Product_Lines__c-N/A,
		--a.[Z1_Attendee_Status__c] as Z1_Attendee_Status__c-N/A,
		--a.[ZTunnel__c] as ZTunnel__c-N/A,
		--a.[Zuercher_Intro_Complete__c] as Zuercher_Intro_Complete__c-N/A,
		--a.[Zuercher_Intro_Director_Approved__c] as Zuercher_Intro_Director_Approved__c-N/A,
		--a.[Zuercher_Intro_Paid__c] as Zuercher_Intro_Paid__c-N/A
		INTO STAGING_SB.dbo.Account_Load_PB
		FROM Tritech_Prod.dbo.Account a
		-- Determine Source RecordType
		LEFT OUTER JOIN TriTech_Prod.dbo.RecordType SrcRT on a.RecordTypeId = SrcRT.ID 
		-- Target RecordType Logic
		LEFT OUTER JOIN Superion_FULLSB.dbo.RecordType TrgRT on TrgRT.[Name] =
						CASE WHEN a.Client__c = 'true' THEN 'Standard'
							 WHEN SrcRT.[Name] = '3rd Party' THEN 'Partner/Vendor/Consultant'
							 ELSE 'Prospect' END
						AND TrgRT.SObjectType = 'Account'
		-- Created By Lookup
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON 'Superion API' = CreatedBy.[Name]
		-- Owner
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [Owner] ON 'Deborah Solorzano' = [Owner].[Name] and [Owner].isActive = 'true'
		-- Account Exec (AE) (Utilized for Installed_PA__c and Account_Executive_Fin__c)
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [AE] ON a.OwnerID = [AE].Legacy_Tritech_ID__c
		-- Data Analyst
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] DataAnalyst ON a.[Z_Data_Conversion_Analyst__c] = DataAnalyst.[Name]
		-- GIS Analyst
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] GISAnalyst ON a.[Z_GIS_Analyst__c] = GISAnalyst.[Name]
		-- Alarm Management Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor AlarmMgtVendor ON a.[Alarm_Management_Vendor__c] = AlarmMgtVendor.TriTech_Competitor
		-- Billing Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor BillingVendor ON a.[Billing_Vendor__c] = BillingVendor.TriTech_Competitor
		-- CAD Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor CADVendor ON a.[CAD_Vendor__c] = CADVendor.TriTech_Competitor
		-- PCR Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor PCRVendor ON a.[ePCR_Vendor__c] = PCRVendor.TriTech_Competitor
		-- FBR Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor FBRVendor ON a.[FBR_Vendor__c] = FBRVendor.TriTech_Competitor
		-- FIRE Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor FireVendor ON a.[Fire_Vendor__c] = FireVendor.TriTech_Competitor
		-- Jail Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor JailVendor ON a.[Jail_Vendor__c] = JailVendor.TriTech_Competitor
		-- Mapping Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor MappingVendor ON a.[Mapping_Vendor__c] = MappingVendor.TriTech_Competitor
		-- Mobile Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor MobileVendor ON a.[Mobile_Vendor__c] = MobileVendor.TriTech_Competitor
		-- Law Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor LawVendor ON a.[RMS_Vendor__c] = LawVendor.TriTech_Competitor
		-- 911 Vendor
		LEFT OUTER JOIN Staging_SB.dbo.map_Competitor_Vendor [911Vendor] ON a.[X911_System_Manufacturer__c] = [911Vendor].TriTech_Competitor


---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table  Staging_SB.dbo.Account_Load_PB
Add [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
Use Staging_SB
Exec SF_BulkOps 'upsert:bulkapi,batchsize(5000)', 'PB_Superion_FullSB', 'Account_Load_PB', 'LegacySFDCAccountId__c';


--select * from Account_Load_PB where error not like '%success%'

---------------------------------------------------------------------------------
-- Load Account Parent Staging
---------------------------------------------------------------------------------

Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Account_Parent_PB') 
DROP TABLE Staging_SB.dbo.Account_Parent_PB;

---------------------------------------------------------------------------------
USE Superion_FULLSB
Exec SF_Replicate 'PB_Superion_FullSB', 'Account' 

USE Staging_SB

select 
acct.Id [Id], 
Cast('' as nvarchar(255)) as Error, 
parent.Id [ParentId]
into Staging_SB.dbo.Account_Parent_PB
from Tritech_Prod.dbo.Account a
INNER JOIN Superion_FullSB.dbo.[Account] acct ON a.Id =  acct.LegacySFDCAccountId__c
INNER JOIN Superion_FullSB.dbo.[Account] parent ON a.ParentId =  parent.LegacySFDCAccountId__c
where a.ParentId is not null;

Use Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(5000)', 'PB_Superion_FullSB', 'Account_Parent_PB';

