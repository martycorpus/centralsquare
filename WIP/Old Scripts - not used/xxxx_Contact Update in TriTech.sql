USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FullSB', 'Contact' 


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Contact_Tritech_Update_PB') 
DROP TABLE Staging_SB.dbo.Contact_Tritech_Update_PB;

Use Staging_SB

select 
Legacy_ID__c as ID, 
Cast('' as nvarchar(255)) as Error, 
'true' as Migrated_to_Superion__c
INTO Contact_Tritech_Update_PB
From Superion_FULLSB.dbo.Contact
where isNull(Legacy_ID__c, '') <> ''
and Migrated_Record__c = 'true'


Use Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(5000)', 'PB_TriTech_Prod', 'Contact_Tritech_Update_PB';

-- Select * from Contact_Tritech_Update_PB where error not like '%success%'