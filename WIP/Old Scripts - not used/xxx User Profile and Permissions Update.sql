-------------------------------------------------------------------------------
--- User Profile & Permissions Hotfix
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 5 December 2018
--- Last Updated: 5 December 2018 - PAB
--- Change Log: 
---
--- Prerequisites: Staging_SB.dbo.map_Profile must exist
-------------------------------------------------------------------------------



---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'Profile', 'yes' ;

Use Superion_FUllSB
Exec SF_Refresh 'PB_Superion_FullSB', 'User', 'yes'
Exec SF_Refresh 'PB_Superion_FullSB', 'Profile', 'yes'


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_ProfileAndPermissionsUpdate_PB') 
	DROP TABLE Staging_SB.dbo.[User_ProfileAndPermissionsUpdate_PB];

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------

Use Staging_SB

Select
a.ID as ID,
Cast('' as nvarchar(255)) as error,
Src_Prof.[Name] as SourcProfile_Original,
Trg_Prof.[Name] as TargetProfile_Original,
Trg_Prof.ID as ProfileID,
isNull(Map_Prof.[Service Cloud User], '') as UserPermissionsSupportUser,
isNull(Map_Prof.[Service Cloud User], '') as UserPermissionsKnowledgeUser
INTO User_ProfileAndPermissionsUpdate_PB
from Superion_FULLSB.dbo.[User] a
-- Source User
LEFT OUTER JOIN Tritech_PROD.dbo.[User] Src_Usr ON a.Legacy_Tritech_Id__c = Src_Usr.ID
-- Source Profile
LEFT OUTER JOIN Tritech_Prod.dbo.[Profile] Src_Prof ON Src_Usr.ProfileId = Src_Prof.ID
-- Map Profile
LEFT OUTER JOIN Staging_SB.dbo.map_Profile Map_Prof ON Src_Prof.[Name] = Map_Prof.[Tritech Profile Name]
-- Target Profile
LEFT OUTER JOIN Superion_FULLSB.dbo.[Profile] Trg_Prof ON Map_Prof.[Superion Profile Name] = Trg_Prof.[Name]

WHERE a.Migrated_Record__c = 'true'
and isNull(a.legacy_tritech_id__c, '') <> ''
and a.Legacy_Source_System__c = 'Tritech'
and substring(a.legacy_tritech_id__c, 1, 3) <> 'xxx'


---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.User_ProfileAndPermissionsUpdate_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(400)', 'PB_Superion_FULLSB', 'User_ProfileAndPermissionsUpdate_PB'


--select ID, SourcProfile_Original, TargetProfile_Original from User_ProfileAndPermissionsUpdate_PB where error not like '%success%'
