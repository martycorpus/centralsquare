-------------------------------------------------------------------------------
--- Contact Migration Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 11 November 2018
--- Last Updated: 11 November 2018 - PAB
--- Change Log: 
---
--- Prerequisites:
---	1. Duplicate Contact Rules must be disabled
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Contact_Load_PB') 
	DROP TABLE Staging_SB.dbo.[Contact_Load_PB];


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Tritech_PROD
Exec SF_Refresh 'PB_Tritech_Prod', 'User'
Exec SF_Refresh 'PB_Tritech_Prod', 'Account'
Exec SF_Refresh 'PB_Tritech_Prod', 'Contact', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Opportunity', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Case'
EXEC SF_Refresh 'PB_TriTech_Prod', 'BGIntegration__BomgarSession__c'

USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Account'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Contact', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'User'

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.ID as Legacy_ID__c,
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		Target_Acct.ID as [AccountID],
		isNull(a.[Alternate_Email__c], '') as [Alternate_Email__c],
		a.[Billing_Contact__c] as [Billing_Contact__c],
		--isNull(CreatedBy.ID,(Select top 1 ID from Superion_FULLSB.dbo.[User] where [Name] = 'Deborah Solorzano' and IsActive = 'true' )) as [CreatedByID],
		--a.[CreatedDate] as [CreatedDate],
		a.[Customer_Survey_Contact__c] as [Customer_Survey_Contact__c],
		--isNull(a.[Department], '') as [Department],
		isNull(Concat(a.[Description], 
					CASE WHEN a.[Department] IS NOT NULL THEN ' Department: ' ELSE '' END, isNull(a.[Department], ''),
					CASE WHEN a.[Notes__c] IS NOT NULL THEN ' Notes: ' ELSE '' END,  isNull(a.[Notes__c], '')),
					'') as [Description],
		isNull(a.[Email], '') as [Email],
		isNull(Cast(a.[Extension__c] as varchar(10)), '') as [Extension__c],
		isNull(a.[Fax], '') as [Fax],
		isNull(a.[FirstName], '') as [FirstName],
		a.[HasOptedOutOfEmail] as [HasOptedOutOfEmail],
		a.[HasOptedOutOfFax] as [HasOptedOutOfFax],
		isNull(a.[HomePhone], '') as [HomePhone],
		CASE a.[Inactive_Contact__c] 
			WHEN 'true' THEN 'Inactive'
			WHEN 'false' THEN 'Active' END as [Status__c],
		CASE a.[Is_Account_a_Client__c] 
			WHEN 'Yes' THEN 'true'
			ELSE 'false' END as [Install_Contact__c],
		isNull(Convert(Varchar(19), a.[Last_Referenced_Date_WMP__c], 120), '') as [Last_Verified_PS__c],
		isNull(a.[LastName], '') as [LastName],
		isNull(a.[MailingCity], '') as [MailingCity],
		isNull(a.[MailingCountry], '') as [MailingCountry],
		isNull(a.[MailingPostalCode], '') as [MailingPostalCode],
		isNull(a.[MailingState], '') as [MailingState],
		isNull(a.[MailingStreet], '') as [MailingStreet],
		CASE a.[Marketing_Contact__c] 
			WHEN 'true' THEN 'Marketing � Marketing User' 
			ELSE isNull(a.[Executive_Contact__c], '') END as [Role__c],
		isNull(a.[MobilePhone], '') as [MobilePhone],
		isNull(a.[OtherPhone], '') as [Phone_2__c],
		isNull(CreatedBy.ID,(Select top 1 ID from Superion_FULLSB.dbo.[User] where [Name] = 'Deborah Solorzano' and IsActive = 'true' )) as [OwnerID],
		isNull(a.[Phone], '') as [Phone],
		CASE 
			WHEN a.[Prefers_Email__c] = 'true' THEN 'Email'
			WHEN a.[Prefers_Phone__c] = 'true' THEN 'Phone'
			ELSE '' END	as [Preferred_Method_of_Contact__c],
		isNull(a.[Primary_Contact_WMP__c], '') as [PrimaryContact__c],
		isNull(a.[Primary_Support_Contact__c], '') as [SupportContact__c],
		isNull(a.[Reference_WMP__c], '') as [Reference__c],
		--a.[ReportsToId] as [ReportsTo],
		isNull(a.[Salutation], '') as [Salutation],
		isNull(a.[Support_Contact__c], '') as [Designated_Support_Contact__c],
		isNull(a.[Title], '') as [Title]
		--a.[After_Hours_Contact_WMP__c] as [After_Hours_Contact_WMP__c_Follow-Up],
		--a.[GIS_Contact__c] as [GIS_Contact__c_Follow-Up],
		--a.[IT_Contact__c] as [IT_Contact__c_Follow-Up],
		--a.[LeadSource] as [LeadSource_Follow-Up],
		--a.[MR_Contact__c] as [MR_Contact__c_Follow-Up],
		--a.[NIBRS_Contact__c] as [NIBRS_Contact__c_Follow-Up],
		--a.[Stakeholder__c] as [Stakeholder__c_Follow-Up],
		--a.[Stakeholder_Areas__c] as [Stakeholder_Areas__c_Follow-Up],
		--a.[Stakeholder_Products__c] as [Stakeholder_Products__c_Follow-Up],
		--a.[Steering_Areas__c] as [Steering_Areas__c_Follow-Up],
		--a.[Steering_Committee_Member__c] as [Steering_Committee_Member__c_Follow-Up],
		--a.[Steering_Products__c] as [Steering_Products__c_Follow-Up],
		--a.[AccountName__c] as [AccountName__c_N/A],
		--a.[Act_On_Lead_Score__c] as [Act_On_Lead_Score__c_N/A],
		--a.[Active__c] as [Active__c_N/A],
		--a.[ActOn_Sync_Test_Field__c] as [ActOn_Sync_Test_Field__c_N/A],
		--a.[Assistant_Email__c] as [Assistant_Email__c_N/A],
		--a.[AssistantName] as [AssistantName_N/A],
		--a.[AssistantPhone] as [AssistantPhone_N/A],
		--a.[Campaign__c] as [Campaign__c_N/A],
		--a.[Completed_Training_WMP_del__c] as [Completed_Training_WMP_del__c_N/A],
		--a.[Contact_ID_WMP__c] as [Contact_ID_WMP__c_N/A],
		--a.[Contact_Long_ID__c] as [Contact_Long_ID__c_N/A],
		--a.[Contact_Picture__c] as [Contact_Picture__c_N/A],
		--a.[ContactURL__c] as [ContactURL__c_N/A],
		--a.[CSC_Portal_Access_Request__c] as [CSC_Portal_Access_Request__c_N/A],
		--a.[Customer_Portal_User__c] as [Customer_Portal_User__c_N/A],
		--a.[Customer_Survey__c] as [Customer_Survey__c_N/A],
		--a.[Do_Not_Solicit_WMP__c] as [Do_Not_Solicit_WMP__c_N/A],
		--a.[DoNotCall] as [DoNotCall_N/A],
		--a.[DummyTouchField__c] as [DummyTouchField__c_N/A],
		--a.[EmailBouncedDate] as [EmailBouncedDate_N/A],
		--a.[EmailBouncedReason] as [EmailBouncedReason_N/A],
		--a.[IsDeleted] as [IsDeleted_N/A],
		--a.[IsEmailBounced] as [IsEmailBounced_N/A],
		--a.[Jigsaw] as [Jigsaw_N/A],
		--a.[JigsawContactId] as [JigsawContactId_N/A],
		--a.[LA_Client__c] as [LA_Client__c_N/A],
		--a.[LastActivityDate] as [LastActivityDate_N/A],
		--a.[LastCURequestDate] as [LastCURequestDate_N/A],
		--a.[LastCUUpdateDate] as [LastCUUpdateDate_N/A],
		--a.[LastModifiedById] as [LastModifiedById_N/A],
		--a.[LastModifiedDate] as [LastModifiedDate_N/A],
		--a.[LastReferencedDate] as [LastReferencedDate_N/A],
		--a.[LastViewedDate] as [LastViewedDate_N/A],
		--a.[Lead_Details__c] as [Lead_Details__c_N/A],
		--a.[LinkedIn__c] as [LinkedIn__c_N/A],
		--a.[MailingGeocodeAccuracy] as [MailingGeocodeAccuracy_N/A],
		--a.[MailingLatitude] as [MailingLatitude_N/A],
		--a.[MailingLongitude] as [MailingLongitude_N/A],
		--a.[Market_Map_Update__c] as [Market_Map_Update__c_N/A],
		--a.[Marketo_Sync_Dummy_Field__c] as [Marketo_Sync_Dummy_Field__c_N/A],
		--a.[MasterRecordId] as [MasterRecordId_N/A],
		--a.[Name] as [Name_N/A],
		--a.[Newsletter_Contact__c] as [Newsletter_Contact__c_N/A],
		--a.[Opt_In_IQ_Analytics__c] as [Opt_In_IQ_Analytics__c_N/A],
		--a.[Opt_In_VisiNet_Advisories__c] as [Opt_In_VisiNet_Advisories__c_N/A],
		--a.[Opt_In_VisiNet_Tech_Communications__c] as [Opt_In_VisiNet_Tech_Communications__c_N/A],
		--a.[Opt_Out_Company_Client_Product_News__c] as [Opt_Out_Company_Client_Product_News__c_N/A],
		--a.[Opt_Out_Technical_and_Product_Bulletins__c] as [Opt_Out_Technical_and_Product_Bulletins__c_N/A],
		--a.[Opt_Out_Tradeshows_and_Demos__c] as [Opt_Out_Tradeshows_and_Demos__c_N/A],
		--a.[Opt_Out_Training_Information__c] as [Opt_Out_Training_Information__c_N/A],
		--a.[Opt_Out_TriTech_com_Outage_Notifications__c] as [Opt_Out_TriTech_com_Outage_Notifications__c_N/A],
		--a.[OptIn_Sales_Promos_Training__c] as [OptIn_Sales_Promos_Training__c_N/A],
		--a.[OptIn_VisionCAD__c] as [OptIn_VisionCAD__c_N/A],
		--a.[OptIn_VisionFBR__c] as [OptIn_VisionFBR__c_N/A],
		--a.[OptIn_VisionFIRE__c] as [OptIn_VisionFIRE__c_N/A],
		--a.[OptIn_VisionFORCE__c] as [OptIn_VisionFORCE__c_N/A],
		--a.[OptIn_VisionINFORM__c] as [OptIn_VisionINFORM__c_N/A],
		--a.[OptIn_VisionJAIL__c] as [OptIn_VisionJAIL__c_N/A],
		--a.[OptIn_VisionLMS__c] as [OptIn_VisionLMS__c_N/A],
		--a.[OptIn_VisionMOBILE__c] as [OptIn_VisionMOBILE__c_N/A],
		--a.[OptIn_VisionRMS__c] as [OptIn_VisionRMS__c_N/A],
		--a.[ORI_FDID__c] as [ORI_FDID__c_N/A],
		--a.[OtherCity] as [OtherCity_N/A],
		--a.[OtherCountry] as [OtherCountry_N/A],
		--a.[OtherGeocodeAccuracy] as [OtherGeocodeAccuracy_N/A],
		--a.[OtherLatitude] as [OtherLatitude_N/A],
		--a.[OtherLongitude] as [OtherLongitude_N/A],
		--a.[OtherPostalCode] as [OtherPostalCode_N/A],
		--a.[OtherState] as [OtherState_N/A],
		--a.[OtherStreet] as [OtherStreet_N/A],
		--a.[Owner_ID__c] as [Owner_ID__c_N/A],
		--a.[PhotoUrl] as [PhotoUrl_N/A],
		--a.[PictId__c] as [PictId__c_N/A],
		--a.[Portal_Access_Requested__c] as [Portal_Access_Requested__c_N/A],
		--a.[Product_Family2__c] as [Product_Family2__c_N/A],
		--a.[qualtrics__Informed_Consent__c] as [qualtrics__Informed_Consent__c_N/A],
		--a.[qualtrics__Informed_Consent_Date__c] as [qualtrics__Informed_Consent_Date__c_N/A],
		--a.[qualtrics__Last_Survey_Invitation__c] as [qualtrics__Last_Survey_Invitation__c_N/A],
		--a.[qualtrics__Last_Survey_Response__c] as [qualtrics__Last_Survey_Response__c_N/A],
		--a.[qualtrics__Net_Promoter_Score__c] as [qualtrics__Net_Promoter_Score__c_N/A],
		--a.[RFP_Contact__c] as [RFP_Contact__c_N/A],
		--a.[ROID__c] as [ROID__c_N/A],
		--a.[Role_WMP__c] as [Role_WMP__c_N/A],
		--a.[Selection_Committee_Member__c] as [Selection_Committee_Member__c_N/A],
		--a.[Subscribed_Events__c] as [Subscribed_Events__c_N/A],
		--a.[SystemModstamp] as [SystemModstamp_N/A],
		--a.[Unsubscribe_Email_Digest__c] as [Unsubscribe_Email_Digest__c_N/A],
		--a.[Z_Newsletter_Contact__c] as [Z_Newsletter_Contact__c_N/A]
		INTO Staging_SB.dbo.Contact_Load_PB
		FROM Tritech_PROD.dbo.Contact a
		LEFT OUTER JOIN Superion_FULLSB.dbo.Account Target_Acct ON a.AccountID = Target_Acct.LegacySFDCAccountId__c
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON Target_Acct.OwnerID = CreatedBy.Legacy_Tritech_ID__c
		-- Links Contacts to the User record for Community Users
		LEFT OUTER JOIN Tritech_PROD.dbo.[User] Usr ON a.ID = Usr.ContactId
		WHERE Inactive_Contact__c = 'false'
		OR 
		--- Logic pulls all contacts associated with a logically migrated CASE
		a.ID IN  
				(select ContactID from TriTech_Prod.dbo.[Case] tr_Case
					LEFT OUTER JOIN TriTech_Prod.dbo.[Account] tr_Act ON tr_Case.AccountId = tr_Act.ID
					WHERE
					(tr_case.IsClosed='false')
									or
					(tr_case.ClosedDate >'1/1/2016')
									or
					(tr_case.CreatedDate>'1/1/2016')
									or
					(tr_case.Id in(select distinct Case__c 
									from [Tritech_PROD].dbo.BGIntegration__BomgarSession__c
									where Case__c is not null))
								   or
					(tr_act.name in (
										'Tiburon'
										,'Milwaukee Police Department WI' 
										,'City of Baltimore MD'
										,'Inform'
										,'Dane County WI'
										,'City of San Antonio TX'
										,'City of Austin TX'
										,'California Highway Patrol (CHP) CA'
										,'Zuercher'
										,'Rapid City Police Department SD' 
										,'South Dakota Highway Patrol SD' 
										,'Pennington County Sheriff''s Office SD' 
										,'Lafourche County Sheriff''s Office LA'
										,'Rock Hill Police Department SC'
										,'Douglas County Sheriff''s Office GA'
									)
					)
				)
		OR
		-- PULL all Contacts associated with either an OPEN opportunity or a Closed Opportunity closed since January 1, 2016
		a.ID IN
			(
				Select Contact__c from TriTech_Prod.dbo.Opportunity Oppty WHERE (Oppty.IsClosed = 'false' OR (Oppty.IsClosed = 'true' AND Oppty.CloseDate > Cast('1/1/2016' as datetime)))
				UNION
				Select Z_Signatory__c from TriTech_Prod.dbo.Opportunity Oppty WHERE (Oppty.IsClosed = 'false' OR (Oppty.IsClosed = 'true' AND Oppty.CloseDate > Cast('1/1/2016' as datetime)))
				UNION
				Select Primary_Quote_Contact__c from TriTech_Prod.dbo.Opportunity Oppty WHERE (Oppty.IsClosed = 'false' OR (Oppty.IsClosed = 'true' AND Oppty.CloseDate > Cast('1/1/2016' as datetime)))
				UNION
				Select RFP_Contact__c from TriTech_Prod.dbo.Opportunity Oppty WHERE (Oppty.IsClosed = 'false' OR (Oppty.IsClosed = 'true' AND Oppty.CloseDate > Cast('1/1/2016' as datetime)))
			)
		OR
		 -- Inclusion of all Active Community Users not previously included
		 (
			Usr.ContactID is not null and Usr.IsActive = 'true'
		 )
---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.Contact_Load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'upsert:bulkapi,batchsize(6000)', 'PB_Superion_FULLSB', 'Contact_Load_PB', 'Legacy_ID__c'


--select * from Contact_Load_PB where error not like '%success%' and error not like '%createdbyid%'

--Select Legacy_ID__c, LastName, FirstName, substring(Error, 57, 255) as Error from Contact_Load_PB where error not like '%success%'
