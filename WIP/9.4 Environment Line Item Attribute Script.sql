-------------------------------------------------------------------------------
--- Environment Line Attribute Migration
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2019
--- Created Date: January 8, 2019
--- Last Updated: January 8, 2019 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
--- Follow Up Notes for Ron:  The listed fields Version and Patch_Version__c at last check did not exist.  They were requested from Maria.  Patch_version__c was a new request.
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'Hardware_Software__c', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'RecordType', 'yes';

Use Superion_FUllSB
Exec SF_Refresh 'PB_Superion_FullSB', 'RecordType', 'yes'
Exec SF_Refresh 'PB_Superion_FullSB', 'Environment__c', 'yes'

---------------------------------------------------------------------------------
-- Drop Staging Table
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment_Product_Line_Attribute_c_Load_PB') 
DROP TABLE Staging_SB.dbo.Environment_Product_Line_Attribute_c_Load_PB;


----------------------------------------------------------------------------
--- ENVIRONMENT TYPE BREAKOUT
----------------------------------------------------------------------------
;WITH Environment_Type 
AS
(
		select 
			ID as Legacy_ID__c, 
			'Prod' as Environment_Type
			from Tritech_PROD.dbo.Hardware_Software__c 

		UNION
		SELECT 
			ID as Legacy_ID__c, 
			'Test' as Environment_Type 
			from Tritech_PROD.dbo.Hardware_Software__c 
			where 
			(
			isnull(VisiNet_Test_System_Version__c, '') <> ''
			OR isNull(VisiNet_Mobile_Test_Version__c, '') <> ''
			OR isnull(FBR_Test_Software_Version__c, '') <> ''
			OR isNULL(TTMS_Test_Software_Version__c, '') <> ''
			OR isNull(CIM_Test_Software_Version__c, '') <> ''
			OR isNull(RMS_Test_Software_Version__c, '') <> ''
			OR isNull(RMS_Web_Test_Software_Version__c, '') <> ''
			OR isNull(Tib_DEV_Build__c, '') <> ''
			)

		UNION
		SELECT
			a.ID as Legacy_ID__c, 
			'Train' as Environment_Type 
			from Tritech_PROD.dbo.Hardware_Software__c a
			where 
			(
			isnull(VisiNet_Training_System_Version__c, '') <> ''
			OR isNull(VisiNet_Mobile_Training_Version__c, '') <> ''
			OR isnull(FBR_Training_Software_Version__c, '') <> ''
			OR isNULL(TTMS_Training_Software_Version__c, '') <> ''
			OR isNull(CIM_Training_Software_Version__c, '') <> ''
			OR isNull(RMS_Training_Software_Version__c, '') <> ''
			OR isNull(RMS_Web_Training_Software_Version__c, '') <> ''
			OR isNull(Tib_TRN_Build__c, '') <> ''
			)
),

----------------------------------------------------------------------------
--- PRODUCT FAMILY BREAK OUT
----------------------------------------------------------------------------

Product_Family AS
(
		-- INFORM 911
		SELECT a.ID as Legacy_ID__c,  ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - 911' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = '911 Hardware and Software'
		UNION
		-- INFORM - CAD
		SELECT a.ID as Legacy_ID__c,  ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - CAD' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Inform Hardware and Software'
				AND
				(isNull(a.VisiCAD_Version__c, '') <> '' OR
				 isNull(VisiNet_Test_System_Version__c, '') <> '' OR	
				 isnull(VisiNet_Training_System_Version__c, '') <> ''	
				) 

		UNION
		-- INFORM - MOBILE
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - Mobile' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Inform Hardware and Software'
				AND
				(isNull(a.VisiNet_Mobile_Version__c, '') <> '' OR
				 isNull(a.VisiNet_Mobile_Test_Version__c, '') <> '' OR
				 isNull(a.VisiNet_Mobile_Training_Version__c, '') <> ''
				)
		UNION
		-- INFORM - TTMS		 
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - TTMS' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.TTMS_Software_Version__c, '') <> '' OR
				 isNULL(a.TTMS_Test_Software_Version__c, '') <> '' OR
				 isNULL(a.TTMS_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - CIM
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - CIM' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.CIM_Software_Version__c, '') <> '' OR
				 isNULL(a.CIM_Test_Software_Version__c, '') <> '' OR
				 isNULL(a.CIM_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - RMS WEB
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - RMS Web' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.RMS_Web_Software_Version__c, '') <> '' OR
				 isNULL(a.RMS_Web_Test_Software_Version__c, '') <> '' OR
				 isNULL(a.RMS_Web_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - FIRE
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - Fire' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_Fire_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - JAIL
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - Jail' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.Jail_Software_Version__c, '') <> ''
				)
		UNION
		-- Inform - RMS Classic
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - RMS Classic' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_RMS_Software_Version__c, '') <> '' OR
				 isNull(RMS_Test_Software_Version__c, '') <> '' OR
				 isNull(RMS_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- Inform - FBR
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - FBR' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_FBR_Software_Version__c, '') <> '' OR
				 isNull(a.FBR_Test_Software_Version__c, '') <> '' OR
				 isNull(a.FBR_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- Inform - ME
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - ME' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Inform Hardware and Software'
				AND
				(isNull(a.Inform_Me_Version__c, '') <> '' 
				)
		UNION
		-- IMC
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'IMC' as Product_Family, 'IMC' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'IMC Hardware and Software'
		UNION
		-- Impact
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Impact' as Product_Family, 'Impact' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Impact Hardware and Software'
		UNION
		-- Respond
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Respond' as Product_Family, 'Respond' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Respond Hardware and Software'
		UNION
		-- Tiburon
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Tiburon' as Product_Family, 'Tiburon' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Tiburon Hardware and Software'
		UNION
		-- VisionAir - CAD
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'VisionAir' as Product_Family, 'VisionAir - CAD' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_CAD_Software_Version__c, '') <> '' 
				)
		UNION
		-- VisionAir - Mobile
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'VisionAir' as Product_Family, 'VisionAir - CAD' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_Mobile_Software_Version__c, '') <> '' 
				)
)

-----------------------------------END CTE SECTION----------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------

--Select * from Product_Family order by Legacy_ID__c

---------------------------------------------------------------------------------
-- Load Environment Product Attribute Staging Table
---------------------------------------------------------------------------------
Select 
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.[ID] as LegacySFDCAccountId__c,
	'TriTech' as Legacy_Source_System__c,
	'true' as Migrated_Record__c,
	pf.Environment_Type as Environment_Type_Original,
	pf.Product_Family as ProductFamily_Original,
	pf.Product_Group as ProductGroup_Original,
	Env.ID as Environment__c,
	isnull(a.Backup_Software_WMP__c, '') as Backup_Software__c,
	isnull(CONVERT(Varchar(19), a.Certificate_Expiration_Date__c, 120), '') as RMS_Certificate_Expiration_Date__c,
	CASE WHEN pf.Environment_Type = 'Prod' AND pf.Product_Group = 'Inform - CIM' THEN isnull(a.CIM_Additional_Information__c, '') ELSE '' END as CIM_Additional_Information__c,
	CASE WHEN pf.Environment_Type = 'Test' AND pf.Product_Group = 'Inform - CIM' THEN isnull(a.CIM_Test_Additional_Information__c, '') ELSE '' END  as CIM_Test_Additional_Information__c,
	CASE WHEN pf.Environment_Type = 'Train' AND pf.Product_Group = 'Inform - CIM' THEN isnull(a.CIM_Training_Additional_Information__c, '') ELSE '' END as CIM_Training_Additional_Information__c,
	isnull(a.CIM_DR__c, '') as CIM_DR__c,
	isnull(a.CIM_DR_Software__c, '') as CIM_DR_Software__c,
	CASE WHEN pf.Environment_Type = 'Prod' AND pf.Product_Group = 'Inform - CIM' THEN isnull(CONVERT(Varchar(19), a.CIM_Software_Expiration_Date__c, 120), '')
		 WHEN pf.Environment_Type = 'Test' AND pf.Product_Group = 'Inform - CIM' THEN isnull(Convert(varchar(19), a.CIM_Test_Software_Expiration_Date__c, 120), '') 
		 WHEN pf.Environment_Type = 'Train' AND pf.Product_Group = 'Inform - CIM' THEN isnull(Convert(Varchar(19), a.CIM_Training_Software_Expiration_Date__c, 120), '') 
		 ELSE '' END as CIM_Software_Expiration_Date__c,
	isnull(a.Current_Pervasive_Version__c, '') as Current_Pervasive_Version__c,
	isnull(a.Custom_Interfaces__c, '') as Custom_Interfaces__c,
	isnull(a.DR__c, '') as DR__c,  
	isnull(a.DR_Mobile__c, '') as DR_Mobile__c,
	isnull(a.External_VisiNet_Browser__c, '') as External_VisiNet_Browser__c,
	isnull(a.FBR_DR__c, '') as FBR_DR__c,
	CASE pf.Environment_Type WHEN 'Test' THEN isnull(a.FBR_Test_Additional_Information__c, '') ELSE '' END as VA_FBR_Additional_Information__c,
	CASE pf.Environment_Type WHEN 'Test' THEN isnull(a.FBR_Test_OS__c, '')
							 WHEN 'Train' THEN isnull(a.FBR_Training_OS__c, '')
							 WHEN 'Prod' THEN isnull(a.VA_FBR_OS__c, '') 
							 ELSE '' END as VA_FBR_OS__c,
	CASE WHEN pf.Environment_Type = 'Test'  and pf.Product_Group = 'Inform - FBR' THEN isnull(a.FBR_Test_SQL_Version__c, '')
		 WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - FBR' THEN isnull(a.FBR_Training_SQL_Version__c, '') 
		 WHEN pf.Environment_Type = 'Prod'  and pf.Product_Group = 'Inform - FBR' THEN isnull(a.VA_FBR_SQL_Version__c, '')
		 ELSE '' END as VA_FBR_SQL_Version__c,
	isnull(a.Geo_Installed_Test__c, '') as Telemetry_Enabled__c,
	CASE pf.Environment_Type WHEN 'Test' THEN isnull(a.GIS_Framework_Test_Version__c, '')
							 WHEN 'Train' THEN isnull(a.GIS_Framework_Training_Version__c, '')
							 WHEN 'Prod' THEN isnull(a.GIS_Framework_Version__c, '') 
							 ELSE '' END as GIS_Framework_Version__c,
	isnull(a.IMP_Academy_System__c, '') as Academy_System__c,
	isnull(a.IMP_Advanced_Mobile_Online__c, '') as Advanced_Mobile_Online__c,
	isnull(a.IMP_ATLAS_CLIENT__c, '') as ATLAS_CLIENT__c,
	isnull(a.IMP_Authentication__c, '') as Authentication__c,
	isnull(a.IMP_Bar_Coding__c, '') as Bar_Coding__c,
	isnull(a.IMP_INT_Biometrics__c, '') as Biometrics__c,
	isnull(a.IMP_INT_CAD_Incident_Data_Export__c, '') as CAD_Incident_Data_Export__c,
	isnull(a.IMP_INT_Comnetix_LiveScan_CardScan__c, '') as Comnetix_LiveScan_CardScan__c,
	isnull(a.IMP_INT_Coplogic__c, '') as Coplogic__c,
	isnull(a.IMP_INT_E911__c, '') as E911__c,
	isnull(a.IMP_INT_N_DEX_Data_Exchange__c, '') as N_DEX_Data_Exchange__c,
	isnull(a.IMP_INT_NCIC__c, '') as NCIC__c,
	isnull(a.IMP_INT_NIEM_Data_Exchange__c, '') as NIEM_Data_Exchange__C,
	isnull(a.IMP_INT_NYS_TRACS_Products__c, '') as NYS_TRACS_Products__c,
	isnull(a.IMP_INT_PICTOMETRY__c, '') as PICTOMETRY__c,
	isnull(a.IMP_INT_Priority_Dispatch_Pro_QA_EMD__c, '') as Priority_Dispatch_Pro_QA_EMD__c,
	isnull(a.IMP_INT_Red_Alert_Incident_Per_Fire_Agen__c, '') as Red_Alert_Incident_Per_Fire_Agency__c,
	isnull(a.IMP_INT_SEI_Court__c, '') as SEI_Court__c,
	isnull(a.IMP_IRIS_IMPACT_Remote_Intel_Service__c, '') as IRIS_IMPACT_Remote_Intel_Service__c,
	isnull(a.IMP_Jewelry_Pawn_Shop_System__c, '') as Jewelry_Pawn_Shop_System__c,
	isnull(a.IMP_Mobile_Ticketing_IMT__c, '') as Mobile_Ticketing_IMT__c,
	isnull(a.IMP_Public_Safety_Records_Mgmt_Software__c, '') as Public_Safety_Records_Mgmt_Software__c,
	isnull(a.IMP_Taxi_Limonsine_Permiting_System__c, '') as Taxi_Limonsine_Permiting_System__c,
	isnull(a.IMP_VCAD_Visual_Computer_Aided_Dispatch__c, '') as VCAD_Visual_Computer_Aided_Dispatch__c,
	isnull(a.IMP_VSTAT__c, '') as VSTAT__c,
	isnull(a.Jail_Additional_Information__c, '') as Jail_Additional_Information__c,
	isnull(a.Jail_OS__c, '') as Jail_OS__c,
	isnull(a.Jail_SQL_Version__c, '') as Jail_SQL_Version__c,
	Concat(CASE WHEN a.Misc_Info_WMP__c Is Null THEN '' ELSE 'Misc Info: ' END, isnull(a.Misc_Info_WMP__c, ''),
		   CASE WHEN a.Notes_Area_WMP__c Is Null THEN '' ELSE ' Area: ' END, isNull(a.Notes_Area_WMP__c, ''),
		   CASE WHEN a.VA_Additional_Information__c Is NULL THEN '' ELSE 'Additional Information: ' END, isnull(a.VA_Additional_Information__c, ''),
		   CASE WHEN Src_rt.[Name] = '911 Hardware and Software' and a.X911_Additional_System_Information__c is NOT Null THEN 'Additional System Information: ' ELSE '' END, isnull(a.X911_Additional_System_Information__c, '')
	)
	 as Notes__c,
	isnull(a.Mobile_Maps_Enabled__c, '') as Mobile_Maps_Enabled__c,
	isnull(a.New_Queues__c, '') as New_Queues__c,
	isnull(a.Number_of_Reporting_Servers__c, '') as Number_of_Reporting_Servers__c,
	Trg_RT.id as RecordType,  
	isnull(a.Reporting_Server__c, '') as Reporting_Server__c,
	isnull(a.RMS_DR__c, '') as RMS_DR__c,
	isnull(a.RMS_DR_Software__c, '') as RMS_DR_Software__c,
	CASE WHEN pf.Environment_Type = 'Prod' and pf.Product_Group = 'Inform - RMS Classic' THEN isnull(a.VA_RMS_Additional_Information__c, '')
		 WHEN pf.Environment_Type = 'Test' and pf.Product_Group = 'Inform - RMS Classic' THEN isnull(a.RMS_Test_Additional_Information__c, '') 
		 WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - RMS Classic' THEN isnull(a.RMS_Training_Additional_Information__c, '') 
							 ELSE '' END as VA_RMS_Additional_Information__c,
	CASE WHEN pf.Environment_Type = 'Prod' AND pf.Product_Group in ('Inform - RMS Classic', 'Inform - RMS Web') THEN isnull(a.RMS_Integration_Version__c, '') 
		 WHEN pf.Environment_Type = 'Train' AND pf.Product_Group in ('Inform - RMS Classic', 'Inform - RMS Web') THEN isnull(a.RMS_Training_Integration_Version__c, '') 
							 ELSE '' END as RMS_Integration_Version__c,
	CASE WHEN pf.Environment_Type = 'Test' AND pf.Product_Group in ('Inform - RMS Classic', 'Inform - RMS Web') THEN isnull(a.RMS_Test_Integration_Version__c, '') 
							 ELSE '' END as RMS_Test_Integration_Version__c,
	CASE WHEN pf.Environment_Type = 'Test'  AND pf.Product_Group = 'Inform - RMS Classic' THEN isnull(a.RMS_Test_OS__c, '') 
		 WHEN pf.Environment_Type = 'Train' AND pf.Product_Group = 'Inform - RMS Classic' THEN isnull(a.RMS_Training_OS__c, '')
		 ELSE '' END as VA_RMS_OS__c,
	CASE WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - RMS Classic' THEN isnull(a.RMS_Training_Software_Version__c, '') 
		 WHEN pf.Environment_Type = 'Prod' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Software_Version__c, '') 
		 WHEN pf.Environment_Type = 'Test' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Test_Software_Version__c, '') 
		 WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Training_Software_Version__c, '') 
		 WHEN pf.Environment_Type = 'Prod' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Software_Version__c, '') 
		 WHEN pf.Environment_Type = 'Test' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Test_Software_Version__c, '') 
		 WHEN pf.Environment_Type = 'Train' and pf.Product_group = 'Inform - TTMS' THEN isnull(a.TTMS_Training_Software_Version__c, '') 
		 ELSE '' END as Version__c,
	CASE WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - RMS Classic' THEN isnull(a.RMS_Training_Software_Version_Patch__c, '') 
		 WHEN pf.Environment_Type = 'Prod' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Software_Version_Patch__c, '') 
		 WHEN pf.Environment_Type = 'Test' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Test_Software_Version_Patch__c, '') 
		 WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Training_Software_Version_Patch__c, '')
		 WHEN pf.Environment_Type = 'Prod' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Software_Version_Patch__c, '') 
		 WHEN pf.Environment_Type = 'Test' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Test_Software_Version_Patch__c, '') 
		 WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Training_Software_Version_Patch__c, '')
		 ELSE '' END as Patch_Version__c,
	CASE WHEN pf.Environment_Type = 'Prod' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_API_Version__c, '') 
		 WHEN pf.Environment_Type = 'Test' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Test_API_Version__c, '')
		 WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Training_API_Version__c, '')
		 ELSE '' END as RMS_Web_API_Version__c,
	CASE WHEN pf.Environment_Type = 'Prod' AND pf.Product_Group = 'Inform - RMS Web' THEN isNull(a.RMS_Web_OS__c, '')
		 WHEN pf.Environment_Type = 'Test' AND pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Test_OS__c, '') 
		 WHEN pf.Environment_Type = 'Train' AND pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Training_OS__c, '') 
		 ELSE '' END as RMS_Web_OS__c,
	CASE WHEN pf.Environment_Type = 'Prod' AND pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_SQL_Version__c, '') 
		 WHEN pf.Environment_Type = 'Test' AND pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Test_SQL_Version__c, '') 
		 WHEN pf.Environment_Type = 'Train' AND pf.Product_Group = 'Inform - RMS Web' THEN isnull(a.RMS_Web_Training_SQL_Version__c, '') 
		 ELSE '' END as RMS_Web_SQL_Version__c,
	isnull(a.Tib_Application__c, '') as Application__c,
	isnull(a.Tib_Application_Version__c, '') as Application_Version__c,
	isnull(a.Tib_DEV_Build__c, '') as DEV_Build__c,
	isnull(Convert(Varchar(19), a.Tib_DEV_Build_Date__c, 120), '') as DEV_Build_Date__c,
	isnull(Convert(varchar(19), a.Tib_Go_Live__c, 120), '') as Go_Live__c,
	isnull(a.Tib_Hosted__c, '') as Hosted__c,
	isnull(a.Tib_Multi_Agency__c, '') as Multi_Agency__c,
	isnull(a.Tib_PRD_Build__c, '') as PRD_Build__c,
	isnull(Convert(Varchar(19), a.Tib_PRD_Build_Date__c, 120), '') as PRD_Build_Date__c,
	isnull(a.Tib_Support_Level__c, '') as Support_Level__c,
	isnull(a.Tib_TRN_Build__c, '') as TRN_Build__c,
	isnull(Convert(Varchar(19), a.Tib_TRN_Build_Date__c, 120), '') as TRN_Build_Date__c,
	CASE WHEN pf.Environment_Type = 'Prod' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Additional_Information__c, '') 
		 WHEN pf.Environment_Type = 'Test' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Test_Additional_Information__c, '') 
		 WHEN pf.Environment_Type = 'Train' and pf.Product_Group = 'Inform - TTMS' THEN isnull(a.TTMS_Training_Additional_Information__c, '') 
		 ELSE '' END as TTMS_Additional_Information__c,
	CASE WHEN pf.Environment_Type = 'Prod' AND pf.Product_Group = 'Inform - TTMS' THEN isnull(Convert(Varchar(19), a.TTMS_Certificate_Expiration_Date__c, 120), '') 
		 WHEN pf.Environment_Type = 'Test' AND pf.Product_Group = 'Inform - TTMS' THEN isnull(Convert(Varchar(19), a.TTMS_Test_Certificate_Expiration_Date__c, 120), '')  
		 WHEN pf.Environment_Type = 'Train' AND pf.Product_Group = 'Inform - TTMS' THEN isnull(Convert(Varchar(19), a.TTMS_Training_Certificate_Expiration_Dat__c, 120), '') 
		 ELSE '' END as TTMS_Certificate_Expiration_Date__c,
	isnull(a.TTMS_DR__c, '') as TTMS_DR__c,
	isnull(a.TTMS_DR_Software__c, '') as TTMS_DR_Software__c,
	isnull(a.VA_CAD_Additional_Information__c, '') as CAD_Additional_Information__c,
	isnull(a.VA_FBR_Additional_Information__c, '') as FBR_Additional_Information__c,
	isnull(a.VA_Fire_Additional_Information__c, '') as Fire_Additional_Information__c,
	isnull(a.VA_Fire_OS__c, '') as Fire_OS__c,
	isnull(a.VA_Fire_SQL_Version__c, '') as Fire_SQL_Version__c,
	isnull(a.VA_Inform_Additional_Information__c, '') as IQ_Inform_Additional_Information__c,
	isnull(a.VA_Inform_OS__c, '') as IQ_OS__c,
	isnull(a.VA_Inform_SQL_Version__c, '') as IQ_SQL_Version__c,
	isnull(a.VA_Mobile_Additional_Information__c, '') as Mobile_Additional_Information__c,
	isnull(a.VA_Mobile_OS__c, '') as Mobile_OS__c,
	isnull(a.VA_Mobile_SQL_Version__c, '') as Mobile_SQL_Version__c,
	isnull(a.VA_Preferred_Connection_Method__c, '') as Preferred_Connection_Method__c,
	isnull(a.VA_RMS_OS__c, '') as RMS_OS__c,
	CASE WHEN pf.Environment_Type = 'Prod' AND pf.Product_Group = ('Inform - RMS Classic') THEN isnull(a.VA_RMS_SQL_Version__c, '')
		 WHEN pf.Environment_Type = 'Test' AND pf.Product_Group = ('Inform - RMS Classic') THEN isnull(a.RMS_Test_SQL_Version__c, '')
		 WHEN pf.Environment_Type = 'Train' AND pf.Product_Group = ('Inform - RMS Classic') THEN isnull(a.RMS_Training_SQL_Version__c, '') 
		 ELSE '' END as VA_RMS_SQL_Version__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_Admin_Stations__c, '') ELSE '' END as X911_Admin_Stations__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Admin_Trunks__c, '') ELSE '' END as X911_Admin_Trunks__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_ALI_Provider__c, '') ELSE '' END as X911_ALI_Provider__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_Audiocodes_Type__c, '') ELSE '' END as X911_Audiocodes_Type__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(Cast(a.X911_Available_MAC_Programming_On_Site__c as varchar(20)), '') ELSE '' END as X911_Available_MAC_Programming_On_Site__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(Convert(varchar(19), a.X911_Certificate_Expiration_Date__c, 120), '') ELSE '' END as X911_Certificate_Expiration_Date__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_CO_ALI_Server_OS_SW_Versio__c, '') ELSE '' END as X911_CO_ALI_Server__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Full_Positions__c, '') ELSE '' END as X911_Full_Positions__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_HA_System__c, '') ELSE '' END as X911_HA_System__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Half_Positions__c, '') ELSE '' END as X911_Half_Positions__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_Inform_911_CTI_Patch_Level__c, '') ELSE '' END as X911_CTI_Patch_Level__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_Inform_911_Server_Patch_Level__c, '') ELSE '' END as X911_Server_Patch_Level__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(Convert(Varchar(19), a.X911_Install_Upgrade_Refresh_Date__c, 120), '') ELSE '' END as X911_Install_Upgrade_Refresh_Date__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Landline_911_Trunks__c, '') ELSE '' END as Landline_911_Trunks__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_PBX_Type__c, '')  ELSE '' END as X911_PBX_Type,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_PSAP_ID__c, '')  ELSE '' END as X911_PSAP_ID__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_QRConnect_Patch_Level__c, '') ELSE '' END  as X911_QRConnect_Patch_Level__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Radio_and_Radio_Interface__c, '')  ELSE '' END as X911_Radio_and_Radio_Interface__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Reverse_ALI__c, '') ELSE '' END  as X911_Reverse_ALI__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Ring_Down_Circuits__c, '')  ELSE '' END as X911_Ring_Down_Circuits__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_SQL_Server_OS_SW_Version__c, '') ELSE '' END  as X911_SQL_Server_OS_SW_Version__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_SQL_Version__c, '')  ELSE '' END as X911_SQL_Version__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Telco__c, '')  ELSE '' END as X911_Telco__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Trunk_Type__c, '') ELSE '' END  as X911_Trunk_Type__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_VLR_Vendor__c, '')  ELSE '' END as X911_VLR_Vendor__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.x911_VMWare_Version__c, '')  ELSE '' END as X911_VMWare_Version__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Wireless_911_Trunks__c, '')  ELSE '' END as Wireless_911_Trunks__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN isnull(a.X911_Workstation_OS_SW_Version__c, '')  ELSE '' END as X911_Workstation_OS__c,
	'' as blank
	--INTO Staging_SB.dbo.Environment_Product_Line_Attribute_c_Load_PB
	FROM TriTech_Prod.dbo.Hardware_Software__c a
	-- SOURCE RECORDTYPE
	LEFT OUTER JOIN Tritech_PROD.dbo.RecordType Src_RT ON a.RecordTypeId = Src_RT.id
	-- TARGET RECORDTYPE
	LEFT OUTER JOIN Superion_FULLSB.dbo.RecordType Trg_RT ON Trg_RT.[Name] = CASE Src_RT.[Name] WHEN	'911 Hardware and Software' THEN 'Inform 911'
																								WHEN	'IMC Hardware and Software' THEN 'IMC'
																								WHEN	'Impact Hardware and Software' THEN 'Impact'
																								WHEN	'Inform Hardware and Software' THEN 'Inform'
																								WHEN	'Respond Hardware and Software' THEN 'Respond'
																								WHEN	'Tiburon Hardware and Software' THEN 'Tiburon'
																								WHEN	'VisionAIR Hardware and Software' THEN 'VisionAIR' END
	-- BREAKS OUT RECORDS BY PRODUCT FAMILYS AND ENVIRONMENTS WITH LOGIC IN CTES
	INNER JOIN Product_Family PF on a.ID = PF.Legacy_ID__c 
	-- ASSIGNS TO ENVIRONMENT
	LEFT OUTER JOIN Superion_FULLSB.dbo.ENVIRONMENT__c Env ON Env.Legacy_SystemId__c = Concat(a.ID, '-', PF.Environment_type) 

		


--------------------------------------------------------------------------------
-- Upload
--------------------------------------------------------------------------------
Exec sf_bulkops 'Upsert','PB_Superion_FULLSB','Environment_Product_Line_Attribute_c_Load_PB', 'Legacy_SystemID__c'
