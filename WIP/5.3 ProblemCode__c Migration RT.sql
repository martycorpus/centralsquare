/*****************************************************************************************************************************************************
REQ #		: REQ-0386
TITLE		: Migrate Problem Codes (Product Sub Module) - Source System to SFDC
DEVELOPER	: RTECSON and MCORPUS
CREATED DT  : 01/15/2019
DETAIL		: 	
				Per Maria Huemmer 1/10/2019,
				
				Also, Shane and I made it through the full mapping, please find it attached. We have specified the product names and then the Product Group and Line to search for a match. The query is like this

				For Product Name XYZ, select all Products where Eligible for Support is TRUE AND Product Group is <GROUP> and Product Line is <LINE> and insert a Problem Code record for each submodule.

											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
01/15/2019		Ron Tecson			Initial
01/18/2019		Ron Tecson			Reload the Problem Code based from the update file provide by Shane on 1/17.

DEPENDENCY:
	- Need to import "Submodule Mapping by Product.xls" file provided by Maria or Shane. Modify the excel file to include the Salesforce 18 Digit Char.
	- Once the Submodule Mapping by Product.xls has been modified, import the data to the Staging_SB database and name the table, PROBLEM_CODE_MAPPING.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'Product_Sub_Module__c', 'yes' ;

Exec SF_Replicate 'PB_Tritech_Prod', 'Product_Sub_Module__c';


Use Superion_FUllSB
--Exec SF_Refresh 'PB_Superion_FullSB', 'Contact', 'yes'
--Exec SF_Refresh 'PB_Superion_FullSB', 'RecordType', 'yes' ;

Exec SF_Refresh 'PB_Superion_FullSB', 'PRODUCT2', 'yes'

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

--select * INTO Staging_SB.dbo.PROBLEM_CODE_MAPPING_01182019 from Staging_SB.dbo.PROBLEM_CODE_MAPPING
--drop table Staging_SB.dbo.PROBLEM_CODE_MAPPING

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ProblemCode__c_PreLoad_RT') 
DROP TABLE ProblemCode__c_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
Use Staging_SB;

SELECT --product_name, count(*)
*
INTO STAGING_SB.dbo.ProblemCode__c_PreLoad_RT
FROM   ( SELECT Cast( '' AS NVARCHAR(18))                                    AS [ID],
                Cast( '' AS NVARCHAR(255))                                   AS Error,
                a.[product_sub_module_id]                                    AS Legacy_CRMID__c,
                'TriTech'                                                    AS Legacy_Source_System__c,
                'true'                                                       AS Migrated_Record__c,
                Concat( a.[product_family1], ' ', a.product_name )           AS [Name],--**mapped --use Col D + ' ' + Col I from excel file
                'true'                                                       AS Active__c,--**mapped: Default to True
                a.product_name                                               AS Product_Original,
                prod.id                                                      AS Product__c,--**mapped
                --a.product_sub_module_name                                    AS ProblemCodeName__c,--**mapped use Col A from the excel file.
				sPSM.Name													 AS ProblemCodeName__c,
                'Mapping Excel file where CentralSquare Line(s) IS NOT null' AS Apps_Note,--MC added 1/15 for reference
                prod.acronym_list__c                                         AS Acronym_List,--MC added 1/15 for reference
                prod.product_group__c                                        AS Product_Group,--MC added 1/15 for reference
                prod.product_line__c                                         AS Product_Line,--MC added 1/15 for reference
                prod.NAME                                                    AS Product_Name --MC added 1/15 for reference
				--a.CentralSquare_Line										 AS CentralSquare_Line
         FROM   staging_sb.dbo.PROBLEM_CODE_MAPPING a --change this to the staging table (from excel file import)
                -- Product2
                LEFT OUTER JOIN superion_fullsb.dbo.PRODUCT2 prod ON a.[centralsquare_group] = prod.product_group__c AND a.centralsquare_line = prod.product_line__c --AND prod.acronym_list__c = 'legacy ttz'
				LEFT OUTER JOIN PB_Tritech_Prod...Product_Sub_Module__c sPSM ON CAST(a.[Product_Sub_Module_ID_18Digits] AS NVARCHAR(18)) = CAST(sPSM.Id AS NVARCHAR(18))
         WHERE  prod.eligible_for_support__c = 'true' AND
                product_line__c IS NOT NULL AND
                a.[centralsquare_group] <> 'Don''t move'
         UNION
         SELECT Cast( '' AS NVARCHAR(18))                                AS [ID],
                Cast( '' AS NVARCHAR(255))                               AS Error,
                a.[product_sub_module_id]                                AS Legacy_CRMID__c,
                'TriTech'                                                AS Legacy_Source_System__c,
                'true'                                                   AS Migrated_Record__c,
                Concat( a.[product_family1], ' ', a.product_name )       AS [Name],--**mapped --use Col D + ' ' + Col I from excel file
                'true'                                                   AS Active__c,--**mapped: Default to True
                a.product_name                                           AS Product_Original,
                prod.id                                                  AS Product__c,--**mapped
                --a.product_sub_module_name                                    AS ProblemCodeName__c,--**mapped use Col A from the excel file.
				sPSM.Name												 AS ProblemCodeName__c,
                'Mapping Excel file where CentralSquare Line(s) IS null' AS Apps_Note,--MC added 1/15 for reference
                prod.acronym_list__c                                     AS Acronym_List,--MC added 1/15 for reference
                prod.product_group__c                                    AS Product_Group,--MC added 1/15 for reference
                prod.product_line__c                                     AS Product_Line,--MC added 1/15 for reference
                prod.NAME                                                AS Product_Name --MC added 1/15 for reference
				--,a.CentralSquare_Line									 AS CentralSquare_Line
         FROM   staging_sb.dbo.PROBLEM_CODE_MAPPING a --change this to the staging table (from excel file import)
                LEFT OUTER JOIN superion_fullsb.dbo.PRODUCT2 prod ON a.[centralsquare_group] = prod.product_group__c --and a.CentralSquare_Line = Prod.Product_Line__c and prod.acronym_list__c = 'legacy ttz'
				LEFT OUTER JOIN PB_Tritech_Prod...Product_Sub_Module__c sPSM ON CAST(a.[Product_Sub_Module_ID_18Digits] AS NVARCHAR(18)) = CAST(sPSM.Id AS NVARCHAR(18))
         WHERE  prod.eligible_for_support__c = 'true'
                AND CentralSquare_Line is null
                AND a.[centralsquare_group] <> 'Don''t move' 
) x
order by ProblemCodeName__c

---------------------------------------------------------------------------------
-- Drop Load table 
---------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ProblemCode__c_Load_RT') 
DROP TABLE ProblemCode__c_Load_RT;

select * INTO Staging_SB.dbo.ProblemCode__c_Load_RT from Staging_SB.dbo.ProblemCode__c_PreLoad_RT

select count(*) from STAGING_SB.dbo.ProblemCode__c_Load_RT

--------------------------------------------------------------------------------
-- Upload
--------------------------------------------------------------------------------

Exec SF_ColCompare 'Insert','PB_Superion_FULLSB','ProblemCode__c_Load_RT'

Exec sf_bulkops 'INSERT','PB_Superion_FULLSB','ProblemCode__c_Load_RT'

--- Starting SF_BulkOps for ProblemCode__c_Load_RT V3.6.9
15:15:31: Run the DBAmp.exe program.
15:15:31: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:15:31: Inserting ProblemCode__c_Load_RT (SQL01 / Staging_SB).
15:15:32: DBAmp is using the SQL Native Client.
15:15:32: SOAP Headers: 
15:15:32: Warning: Column 'Legacy_Source_System__c' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Active__c' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Original' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Apps_Note' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Acronym_List' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Group' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Line' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Name' ignored because it does not exist in the ProblemCode__c object.
15:17:09: 8884 rows read from SQL Table.
15:17:09: 8884 rows successfully processed.
15:17:09: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

---------------------------------------------------------------------------------
-- Validate for any Error / Success
---------------------------------------------------------------------------------

select error, count(*) from ProblemCode__c_Load_RT
group by error

select ID --INTO staging_sb.dbo.ProblemCode__c_For_Deletion_RT
from PB_Superion_FULLSB...ProblemCode__c where Migrated_Record__c = 'true'

alter table staging_sb.dbo.ProblemCode__c_For_Deletion_RT
add Error NVARCHAR(255)

select * from staging_sb.dbo.ProblemCode__c_For_Deletion_RT

USE Staging_SB

Exec sf_bulkops 'DELETE','PB_Superion_FULLSB','ProblemCode__c_For_Deletion_RT'

select * into staging_sb.dbo.PROBLEM_CODE_MAPPING_V1 from staging_sb.dbo.PROBLEM_CODE_MAPPING

select * from  staging_sb.dbo.PROBLEM_CODE_MAPPING