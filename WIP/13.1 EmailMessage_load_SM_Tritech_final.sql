/*
-- Author     : Shivani Mogullapalli
-- Date       : 06/11/2018
-- Description: Migrate the Case data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0827
Scope:
Migrate all records that relate to a migrated case and records that relate to migrated tasks.
*/

--use Tritech_Prod
--EXEC SF_Refresh 'MC_Tritech_PROD','EmailMessage','yes'

-- use superion_FullSb
--EXEC SF_Replicate 'Sl_Superion_Fullsb','EmailMessage','yes'
/* Turn off the Email message master process builder */

-- select count(*) from Tritech_PROD.dbo.EmailMessage
-- 198852
use staging_sb

go

-- drop table EmailMessage_Tritech_SFDC_Preload
declare @defaultuser varchar(18)
select @defaultuser = id  from [Superion_FULLSB].dbo.[user]
							where name like 'Superion API'

	SELECT		
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_email.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
				,ActivityID_sp						= tr_email.[ActivityId]
				,BccAddress_orig						 = tr_email.[BccAddress]
				,BCcAddress								 = replace(cast([BccAddress] as nvarchar(max)),'@','@dummy.')
				,CCAddress_orig							 = tr_email.[CcAddress]
				,CcAddress								 =	replace(cast([CcAddress] as nvarchar(max)),'@','@dummy.')
				,CreatedById_orig						= tr_email.[CreatedById]
				,CreatedById							= iif(Tar_CreateID.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Tar_createID.id )
				,CreatedDate							= tr_email.[CreatedDate]
				,FromAddress_orig						= tr_email.[FromAddress]
				,FromAddress							= replace([FromAddress],'@','@dummy.')
				,FromName								= tr_email.[FromName]
			
														  --,tr_email.[HasAttachment]
				,Headers								= tr_email.[Headers]
				,HTMLBody								= tr_email.[HtmlBody]
				,id_orig								= tr_email.[Id]
				,Incoming								= tr_email.[Incoming]
				,IsClientManaged_orig						= tr_email.[IsClientManaged]
				,IsClientManaged						='True'
														  --,tr_email.[IsDeleted]
				,IsExternallyVisible					= tr_email.[IsExternallyVisible]
														  --,tr_email.[LastModifiedById]
														  --,tr_email.[LastModifiedDate]
				,MessageDate							= tr_email.[MessageDate]
				,MessageIdentifier						= tr_email.[MessageIdentifier]
						,ParentID_orig					=	tr_email.[ParentId]
						,ParentID						=  Target_case.id
				--,RelatedToId							= tr_email.[RelatedToId]
			,ReplyToEmailMessageId_orig					= tr_email.[ReplyToEmailMessageId]
				,Status							= tr_email.[Status]
				
				,Subject								= tr_email.[Subject]
														  --,tr_email.[SystemModstamp]
				,TextBody								= tr_email.[TextBody]
				,ThreadIdentifier						= tr_email.[ThreadIdentifier]
				,ToAddress_orig							= tr_email.[ToAddress]
				,ToAddress								= replace(cast(tr_email.[ToAddress] as nvarchar(max)),'@','@dummy.')
				--,ValidatedFromAddress					= replace(cast(tr_email.[ValidatedFromAddress] as nvarchar(max)),'@','@dummy.')
														  
						 --into EmailMessage_Tritech_SFDC_Preload
 FROM [Tritech_PROD].[dbo].[EmailMessage] tr_email
  inner join Superion_FULLSB.dbo.[case] target_case
  on target_Case.legacy_ID__c= tr_Email.ParentId
  -----------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.task target_task
  on target_task.legacy_id__c= tr_email.ActivityId
  -----------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Tar_CreateID
  on Tar_CreateID.Legacy_Tritech_Id__c=tr_email.CreatedById
  and Tar_CreateId.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------
  left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_email.createdbyId
  ------------------------------------------------------------------------------------
  where 1=1

  --(172581 row(s) affected)

  select count(*) from EmailMessage_Tritech_SFDC_Preload
  -- Total Count to be loaded::::



  -- check for the duplicates.
  select legacy_id__c ,count(*) from EmailMessage_Tritech_SFDC_Preload
  group by legacy_id__c
  having count(*)>1
---------------------------------------------------------------------------------------------------------------
										  /* STEP 1
										ActivitiyId and Replytoemailmessageid is null
										*/
select Count(*) from EmailMessage_Tritech_SFDC_Preload
 where activityid_sp is  null and ReplyToEmailMessageId_orig is  null
 -- COUNT::::: 
----------------------------------------------------------------------------------------------------------------
  -- drop table EmailMessage_Tritech_SFDC_load_step1
  select  * 
  into EmailMessage_Tritech_SFDC_load_step1
  from EmailMessage_Tritech_SFDC_Preload
  where activityid_sp is  null and ReplyToEmailMessageId_orig is  null
  --(172581 row(s) affected)

--: Run batch program to create EmailMessages
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'EmailMessage_Tritech_SFDC_load_Step1' 

--check column names
  
--Exec SF_BulkOps 'Insert:batchsize(50)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_step1'

/*
Salesforce object EmailMessage does not contain column ActivityID_orig
Salesforce object EmailMessage does not contain column BccAddress_orig
Salesforce object EmailMessage does not contain column CCAddress_orig
Salesforce object EmailMessage does not contain column CreatedById_orig
Salesforce object EmailMessage does not contain column FromAddress_orig
Salesforce object EmailMessage does not contain column id_orig
Salesforce object EmailMessage does not contain column IsClientManaged_orig
Salesforce object EmailMessage does not contain column ParentID_orig
Salesforce object EmailMessage does not contain column ToAddress_orig
*/

select error, count(*) from EmailMessage_Tritech_SFDC_load_step1
group by error;


select * from EmailMessage_Tritech_SFDC_load_step1
where error<> 'Operation Successful.'


select error, count(*) from EmailMessage_Tritech_SFDC_load_step1
group by error

-- drop table EmailMessage_Tritech_SFDC_load_step1_errors
select * 
into EmailMessage_Tritech_SFDC_load_step1_errors
from EmailMessage_Tritech_SFDC_load_step1
where error<>'Operation Successful.'
--( row(s) affected)


-- drop table EmailMessage_Tritech_SFDC_load_Step1_errors_bkp
select * 
into EmailMessage_Tritech_SFDC_load_step1_errors_bkp
from EmailMessage_Tritech_SFDC_load_step1
where error<>'Operation Successful.'

--( row(s) affected)


select *
-- update a set a.toaddress=substring(a.toaddress,1,3979)
from EmailMessage_Tritech_SFDC_load_Step1_errors a
where a.error like 'Value too large max length:4000 Your length:%'

-- ( row(s) affected)

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_errors'


select *
-- delete
from EmailMessage_Tritech_SFDC_load_step1
where error<>'Operation Successful.'
--( row(s) affected)


-- insert into EmailMessage_Tritech_SFDC_load_step1
select * from EmailMessage_Tritech_SFDC_load_step1_errors 
where error='Operation Successful.'

select error, count(*) from EmailMessage_Tritech_SFDC_load_step1
group by error
----------------------------------------------------------------------------------------------------------------------------
										/*STEP 2 
										 ActivitiyId is not null  and Replytoemailmessageid is null
										 */
 select legacy_id__C from EmailMessage_Tritech_SFDC_Preload
 where activityid_sp is not null and ReplyToEmailMessageId_orig is  null
 
 --Count::::0

 -- drop table EmailMessage_Tritech_SFDC_load_Step2
 select a.*,a.activityid_sp as Activityid
 --into EmailMessage_Tritech_SFDC_load_Step2
 from EmailMessage_Tritech_SFDC_Preload a
 left join Superion_FULLSB.dbo.EmailMessage e 
 on a.legacy_id__c = e.Legacy_Id__c
 where a.activityid_sp is not null and a.ReplyToEmailMessageId_orig is  null and e.Legacy_Id__c is null
  
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_Step2'
----------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/
 ------------------------------------------------------------------------------------------------------------
 /*STEP 3
 ActivitiyId is  null  and Replytoemailmessageid is not null
 */
  -- drop table EmailMEssage_Tritech_SFDC_load_step3 
 select a.*
 , ReplyToEmailMessageId= iif(b.id is null, 'ReplyTo Not Found',b.id)
-- INTO EmailMEssage_Tritech_SFDC_load_step3
 from EmailMessage_Tritech_SFDC_Preload a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null

 --( row(s) affected)

 select count(*)
  from EmailMEssage_Tritech_SFDC_load_step3 where ReplyToEmailMessageId='Replyto not Found'
--
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_Step3'
 ----------------------------------------------------------------------------------------------------------------------
 /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/
------------------------------------------------------------------------------------------------------------------------
-- drop table EmailMEssage_Tritech_SFDC_load_step3_errors
select * 
into EmailMEssage_Tritech_SFDC_load_step3_errors
from EmailMEssage_Tritech_SFDC_load_step3
where error <>'Operation Successful.'
--( row(s) affected)

 select a.*
 , ReplyToEmailMessageId_new1= iif(b.id is null, 'ReplyTo Not Found',b.id)
--INTO EmailMEssage_Tritech_SFDC_load_step3_errors1
 from EmailMEssage_Tritech_SFDC_load_step3_errors a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new1
 from EmailMEssage_Tritech_SFDC_load_step3_errors1 a

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors1 where replytoemailmessageid ='ReplyTo Not Found'

 --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors1'
 ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/

 -- drop table EmailMEssage_Tritech_SFDC_load_step3_errors2
 select a.*
 , ReplyToEmailMessageId_new2= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step3_errors2
 from EmailMEssage_Tritech_SFDC_load_step3_errors1 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new2
 from EmailMEssage_Tritech_SFDC_load_step3_errors2 a
 --( row(s) affected)

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors2
  where replytoemailmessageid ='ReplyTo Not Found'
  --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors2'
 ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/

 -- drop table EmailMEssage_Tritech_SFDC_load_step3_errors3


 select a.*
 , ReplyToEmailMessageId_new3= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step3_errors3
 from EmailMEssage_Tritech_SFDC_load_step3_errors2 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new3
 from EmailMEssage_Tritech_SFDC_load_step3_errors3 a
 --( row(s) affected)

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors3
  where replytoemailmessageid ='ReplyTo Not Found'
  --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors3'
 ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/

 -- drop table EmailMEssage_Tritech_SFDC_load_step3_errors4


 select a.*
 , ReplyToEmailMessageId_new4= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step3_errors4
 from EmailMEssage_Tritech_SFDC_load_step3_errors3 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new4
 from EmailMEssage_Tritech_SFDC_load_step3_errors4 a
 --( row(s) affected)

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors4
  where replytoemailmessageid ='ReplyTo Not Found'
  --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors4'
 ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/

 -- drop table EmailMEssage_Tritech_SFDC_load_step3_errors5


 select a.*
 , ReplyToEmailMessageId_new5= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step3_errors5
 from EmailMEssage_Tritech_SFDC_load_step3_errors4 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new5
 from EmailMEssage_Tritech_SFDC_load_step3_errors5 a
 --( row(s) affected)

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors5
  where replytoemailmessageid ='ReplyTo Not Found'
  --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors5'
 ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/

 -- drop table EmailMEssage_Tritech_SFDC_load_step3_errors6


 select a.*
 , ReplyToEmailMessageId_new6= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step3_errors6
 from EmailMEssage_Tritech_SFDC_load_step3_errors5 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new6
 from EmailMEssage_Tritech_SFDC_load_step3_errors6 a
 --( row(s) affected)

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors6
  where replytoemailmessageid ='ReplyTo Not Found'
  --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors6'

------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/

 -- drop table EmailMEssage_Tritech_SFDC_load_step3_errors7


 select a.*
 , ReplyToEmailMessageId_new7= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step3_errors7
 from EmailMEssage_Tritech_SFDC_load_step3_errors6 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new7
 from EmailMEssage_Tritech_SFDC_load_step3_errors7 a
 --( row(s) affected)

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors7
  where replytoemailmessageid ='ReplyTo Not Found'
  --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors7'

------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/

 -- drop table EmailMEssage_Tritech_SFDC_load_step3_errors8


 select a.*
 , ReplyToEmailMessageId_new8= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step3_errors8
 from EmailMEssage_Tritech_SFDC_load_step3_errors7 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is  null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new8
 from EmailMEssage_Tritech_SFDC_load_step3_errors8 a
 --( row(s) affected)

 select count(*) from EmailMEssage_Tritech_SFDC_load_step3_errors8
  where replytoemailmessageid ='ReplyTo Not Found'
  --
 --Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step3_errors8'
 -----------------------------------------------------------------------------------------------------------------------------------
 ----------------------------------------------------------------------------------------------------------------------------
  ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/
-----------------------------------------------------------------------------------------------------------------
										/* STEP 4
										Activityid is not null and replytoemailmessageid is not null
										*/


select count(*) from EmailMessage_Tritech_SFDC_Preload
 where activityid_sp is not null and ReplyToEmailMessageId_orig is not null

-- drop table EmailMEssage_Tritech_SFDC_load_step4
select a.*
 , ReplyToEmailMessageId=iif(c.id is not null, c.id,'ReplyTo Not Found'),ActivityId=a.ActivityId_sp
INTO EmailMEssage_Tritech_SFDC_load_step4
 from EmailMessage_Tritech_SFDC_Preload a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.legacy_id__C=b.Legacy_Id__c
left outer join [Superion_FULLSB].dbo.EmailMessage c
on a.ReplyToEmailMessageId_orig=c.Legacy_Id__c
where activityid_sp is not null and ReplyToEmailMessageId_orig is not null
 and b.Legacy_Id__c is null 

 --( row(s) affected)

  select count(*) from EmailMEssage_Tritech_SFDC_load_step4
  where replytoemailmessageid ='ReplyTo Not Found'

--
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step4'
  ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/


select count(*)
from EmailMEssage_Tritech_SFDC_load_step4 a
 where error<>'Operation Successful.'

  -- drop table EmailMEssage_Tritech_SFDC_load_step4_errors
select * 
into EmailMEssage_Tritech_SFDC_load_step4_errors
from EmailMEssage_Tritech_SFDC_load_step4
where error <>'Operation Successful.'

select a.*
from EmailMEssage_Tritech_SFDC_load_step4_errors a
 where error<>'Operation Successful.'
--

select a.*
 , ReplyToEmailMessageId_new1= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step4_errors1
 from EmailMEssage_Tritech_SFDC_load_step4_errors a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is not null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new1
 from EmailMEssage_Tritech_SFDC_load_step4_errors1 a
 --( row(s) affected)

  select count(*) from EmailMEssage_Tritech_SFDC_load_step4_errors1
  where replytoemailmessageid ='ReplyTo Not Found'

--
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step4_errors1'

 ------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/


select a.*
from EmailMEssage_Tritech_SFDC_load_step4_errors1 a
 where error<>'Operation Successful.'
--

select a.*
 , ReplyToEmailMessageId_new2= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step4_errors2
 from EmailMEssage_Tritech_SFDC_load_step4_errors1 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is not null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new2
 from EmailMEssage_Tritech_SFDC_load_step4_errors2 a
 --(134 row(s) affected)

  select count(*) from EmailMEssage_Tritech_SFDC_load_step4_errors2
  where replytoemailmessageid ='ReplyTo Not Found'

--38
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step4_errors2'
--------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/


select a.*
from EmailMEssage_Tritech_SFDC_load_step4_errors2 a
 where error<>'Operation Successful.'
--

select a.*
 , ReplyToEmailMessageId_new3= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step4_errors3
 from EmailMEssage_Tritech_SFDC_load_step4_errors2 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is not null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --( row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new3
 from EmailMEssage_Tritech_SFDC_load_step4_errors3 a
 --( row(s) affected)

  select count(*) from EmailMEssage_Tritech_SFDC_load_step4_errors3
  where replytoemailmessageid ='ReplyTo Not Found'

--10
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step4_errors3'
------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/


select a.*
from EmailMEssage_Tritech_SFDC_load_step4_errors3 a where error<>'Operation Successful.'
--10

select a.*
 , ReplyToEmailMessageId_new4= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step4_errors4
 from EmailMEssage_Tritech_SFDC_load_step4_errors3 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is not null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --(10 row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new4
 from EmailMEssage_Tritech_SFDC_load_step4_errors4 a
 --(10 row(s) affected)

  select count(*) from EmailMEssage_Tritech_SFDC_load_step4_errors4
  where replytoemailmessageid ='ReplyTo Not Found'

--2
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step4_errors4'
---------------------------------------------------
------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/


select a.*
from EmailMEssage_Tritech_SFDC_load_step4_errors4 a where error<>'Operation Successful.'
--2

select a.*
 , ReplyToEmailMessageId_new5= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step4_errors5
 from EmailMEssage_Tritech_SFDC_load_step4_errors4 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is not null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --(2 row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new5
 from EmailMEssage_Tritech_SFDC_load_step4_errors5 a
 --(2 row(s) affected)

  select count(*) from EmailMEssage_Tritech_SFDC_load_step4_errors5
  where replytoemailmessageid ='ReplyTo Not Found'

--2
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step4_errors5'
---------------------------------------------------
------------------------------------------------------------------------------------------------
  /* Refresh Email Message */
/*
 use superion_FullSb
EXEC SF_Refresh 'Sl_Superion_Fullsb','EmailMessage','yes'
*/


select a.*
from EmailMEssage_Tritech_SFDC_load_step4_errors5 a where error<>'Operation Successful.'
--2

select a.*
 , ReplyToEmailMessageId_new6= iif(b.id is null, 'ReplyTo Not Found',b.id)
INTO EmailMEssage_Tritech_SFDC_load_step4_errors6
 from EmailMEssage_Tritech_SFDC_load_step4_errors5 a
left outer join [Superion_FULLSB].dbo.EmailMessage b
on a.ReplyToEmailMessageId_orig=b.Legacy_Id__c
 where activityid_sp is not null and ReplyToEmailMessageId_orig is not null
 and a.error<>'Operation Successful.'

 --(2 row(s) affected)
 select *
 --update a set replytoemailmessageid=a.ReplyToEmailMessageId_new6
 from EmailMEssage_Tritech_SFDC_load_step4_errors6 a
 --(2 row(s) affected)

  select count(*) from EmailMEssage_Tritech_SFDC_load_step4_errors6
  where replytoemailmessageid ='ReplyTo Not Found'

--2
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','EmailMEssage_Tritech_SFDC_load_step4_errors6'
---------------------------------------------------

select * 
--update  a set a.id=b.id
from emailmessage_Tritech_SFDC_preload a
inner join Superion_FULLSB.dbo.EmailMessage b
on a.legacy_id__c = b.Legacy_Id__c
--(172581 row(s) affected)




