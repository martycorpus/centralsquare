-------------------------------------------------------------------------------
--- CommunityUser Permission Migration Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 26 November 2018
--- Last Updated: 26 November 2018 - PAB
--- Change Log: 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Profile', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Contact', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes'
Exec SF_Refresh 'PB_Superion_FullSB', 'PermissionSet', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'PermissionSetAssignment', 'yes';

USE Tritech_PROD
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Case', 'yes'


---------------------------------------------------------------------------------
-- DROP STAGING TABLE IF EXISTENT 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='PERMISSIONSETASSIGNMENT_LOAD_PB') 
	DROP TABLE Staging_SB.dbo.[PERMISSIONSETASSIGNMENT_LOAD_PB];


---------------------------------------------------------------------------------
-- CREATE PERMISSION SET ASSIGNMENT STAGING TABLE 
---------------------------------------------------------------------------------

Select
	Cast('' as nvarchar(18)) as ID,
	Cast('' as nvarchar(255)) as Error,
	a.Legacy_Tritech_ID__c as AssigneeID_Original,
	a.ID as AssigneeID,
	SrcProf.[Name] as PermissionSetID_original,
	Perm.ID as PermissionSetID
	INTO PERMISSIONSETASSIGNMENT_LOAD_PB
	FROM Superion_FullSB.dbo.[User] a
	-- Source User
	LEFT OUTER JOIN TriTech_Prod.dbo.[user] SrcUser ON a.Legacy_Tritech_Id__c = SrcUser.ID
	-- Source Profile
	LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] SrcProf ON SrcUser.ProfileId = SrcProf.ID
	-- Permission Set
	LEFT OUTER JOIN Superion_FULLSB.dbo.[PermissionSet] Perm ON Perm.[Name] = 
																			CASE SrcProf.[Name] WHEN 'TriTech Portal Read Only with Tickets' THEN 'Community_Case_Access_Readonly'
																								WHEN 'TriTech Portal Standard User' THEN 'Community_Case_Access'
																								WHEN 'TriTech Portal Manager' THEN 'Community_Delegated_Admin_Dedicated'
																								ELSE '' END
	WHERE migrated_record__c = 'true'
	and SrcProf.[Name] in ('TriTech Portal Read Only with Tickets', 'TriTech Portal Standard User', 'TriTech Portal Manager')


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
-- Set related community users to 'Active'
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='USER_Update_toActive_PB') 
	DROP TABLE Staging_SB.dbo.[USER_Update_toActive_PB];

Select AssigneeID as ID,
CAST('' as nvarchar(255)) as error,
'true' as isActive
INTO Staging_SB.dbo.USER_Update_toActive_PB
From Staging_SB.dbo.PERMISSIONSETASSIGNMENT_LOAD_PB

Alter table Staging_SB.dbo.User_Update_toActive_PB
Add [Sort] int identity (1,1)

USE Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(800)', 'PB_Superion_FULLSB', 'User_Update_toActive_PB'

--Select * from User_Update_toActive_PB where error not like '%success%'

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.PERMISSIONSETASSIGNMENT_LOAD_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'insert:bulkapi,batchsize(800)', 'PB_Superion_FULLSB', 'PERMISSIONSETASSIGNMENT_LOAD_PB'


--select * from PERMISSIONSETASSIGNMENT_LOAD_PB where error not like '%success%'


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
-- Set related community users to 'inActive'
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='USER_Update_toInActive_PB') 
	DROP TABLE Staging_SB.dbo.[USER_Update_toInActive_PB];

Select AssigneeID as ID,
CAST('' as nvarchar(255)) as error,
'false' as isActive
INTO Staging_SB.dbo.USER_Update_toInActive_PB
From Staging_SB.dbo.PERMISSIONSETASSIGNMENT_LOAD_PB

Alter table Staging_SB.dbo.User_Update_toInActive_PB
Add [Sort] int identity (1,1)

USE Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(800)', 'PB_Superion_FULLSB', 'User_Update_toInActive_PB'

--Select * from User_Update_toActive_PB where error not like '%success%'