-------------------------------------------------------------------------------
--- ChatterGroup (CollaborationGroup) Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 28, 2018
--- Last Updated: November 28, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'CollaborationGroup', 'yes';


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroup_Load_PB') 
DROP TABLE Staging_SB.dbo.CollaborationGroup_Load_PB;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.Name as Name,
		a.CollaborationType as CollaborationType,
		isNull(a.[Description], '') as [Description],
		isNull(a.InformationTitle, '') as InformationTitle,
		isNull(a.InformationBody, '') as InformationBody,
		a.CanHaveGuests as CanHaveGuests,
		a.IsArchived as IsArchived,
		a.IsAutoArchiveDisabled as IsAutoArchiveDisabled,
		a.IsBroadcast as IsBroadCast
		INTO CollaborationGroup_Load_PB
		From TriTech_Prod.dbo.CollaborationGroup a
		WHERE a.IsArchived = 'false'
		AND a.MemberCount > 0
		AND a.LastFeedModifiedDate >= Cast('09/30/2018' as Datetime)



---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performan
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.CollaborationGroup_load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Staging_SB
Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'CollaborationGroup_load_PB'

