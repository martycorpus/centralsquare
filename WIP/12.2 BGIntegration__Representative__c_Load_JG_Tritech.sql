 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Representative__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

 /*
Use Tritech_PROD
EXEC SF_Replicate 'SL_Tritech_PROD','BGIntegration__Representative__c'

Use Superion_FULLSB
EXEC SF_Refresh 'SL_Superion_FULLSB','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_Superion_FULLSB','User','Yes'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__Representative__c'
*/ 

Use Staging_SB;

--Drop table BGIntegration__Representative__c_Tritech_SFDC_Preload;

Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,BGIntegration__BomgarSession__c_orig      =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c           =  bs.Id

,BGIntegration__Display_Name__c            =  bg.BGIntegration__Display_Name__c
,BGIntegration__Display_Number__c          =  bg.BGIntegration__Display_Number__c
,BGIntegration__Embassy__c                 =  bg.BGIntegration__Embassy__c
,BGIntegration__GS_Number__c               =  bg.BGIntegration__GS_Number__c
,BGIntegration__Hostname__c                =  bg.BGIntegration__Hostname__c
,BGIntegration__Invited__c                 =  bg.BGIntegration__Invited__c
,BGIntegration__Operating_System__c        =  bg.BGIntegration__Operating_System__c
,BGIntegration__Owner__c                   =  bg.BGIntegration__Owner__c 
,BGIntegration__Primary__c                 =  bg.BGIntegration__Primary__c
,BGIntegration__Private_Display_Name__c    =  bg.BGIntegration__Private_Display_Name__c
,BGIntegration__Private_IP__c              =  bg.BGIntegration__Private_IP__c
,BGIntegration__Public_Display_Name__c     =  bg.BGIntegration__Public_Display_Name__c
,BGIntegration__Public_IP__c               =  bg.BGIntegration__Public_IP__c
,BGIntegration__Representative_ID__c       =  bg.BGIntegration__Representative_ID__c
,BGIntegration__Seconds_Involved__c        =  bg.BGIntegration__Seconds_Involved__c

,CreatedById_orig                          =  bg.CreatedById
,CreatedById                               =  createdbyuser.Id

,CreatedDate                               =  bg.CreatedDate
--,LegacyID__c                             =  Id
,Name                                      =  bg.[Name]

into BGIntegration__Representative__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__Representative__c bg

--Fetching CreatedById(UserLookup)
left join Superion_FULLSB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join Superion_FULLSB.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

----For Delta load
--left join Superion_FULLSB.dbo.BGIntegration__Representative__c trgt on
--trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(113054 row(s) affected)

-----------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Representative__c;--113054

Select count(*) from BGIntegration__Representative__c_Tritech_SFDC_Preload;--113054

Select Legacy_Id__c,count(*) from Staging_SB.dbo.BGIntegration__Representative__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB.dbo.BGIntegration__Representative__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__Representative__c_Tritech_SFDC_Load
from BGIntegration__Representative__c_Tritech_SFDC_Preload; --(113054 row(s) affected)

Select * from Staging_SB.dbo.BGIntegration__Representative__c_Tritech_SFDC_Load
where CreatedById is null ;


--Exec SF_ColCompare 'Insert','SL_Superion_FullSB', 'BGIntegration__Representative__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__Representative__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Representative__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','BGIntegration__Representative__c_Tritech_SFDC_Load'

----------------------------------------------------------------------------------------
--Updating Legacy_Source_System__c,Migrated_Record__c

--Use Staging_SB;

--Drop table BGIntegration__Representative__c_Tritech_SFDC_LoadUpdate

Select 

 Id =bg.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Legacy_id__c
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

into BGIntegration__Representative__c_Tritech_SFDC_LoadUpdate
from Staging_SB.dbo.BGIntegration__Representative__c_Tritech_SFDC_Load bg
;--(105858 row(s) affected)

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB','BGIntegration__Representative__c_Tritech_SFDC_LoadUpdate' 

/*
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','BGIntegration__Representative__c_Tritech_SFDC_LoadUpdate' 

-----------------------------------------------------------------------------------------------------

Select *
--delete 
from BGIntegration__Representative__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --

--insert into BGIntegration__Representative__c_Tritech_SFDC_Load
select * from BGIntegration__Representative__c_Tritech_SFDC_Load_Delta; 