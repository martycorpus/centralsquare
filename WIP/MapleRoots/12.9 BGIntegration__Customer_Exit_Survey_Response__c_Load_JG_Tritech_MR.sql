/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Customer_Exit_Survey_Response__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'MC_Tritech_PROD','BGIntegration__Customer_Exit_Survey_Response__c'

Use SUPERION_MAPLESB
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','BGIntegration__Customer_Exit_Survey_Response__c'
*/ 

Use Staging_SB_MapleRoots;

--Drop table BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_MAPLESB.dbo.[User] 
where Name like 'Bomgar Site Guest User');

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__BomgarSession__c_orig  =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c  =  bs.Id

,BGIntegration__GS_Number__c      =  bg.BGIntegration__GS_Number__c
,BGIntegration__Value__c          =  bg.BGIntegration__Value__c

,CreatedById_orig                 =  bg.CreatedById
,CreatedById                      =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                      =  bg.CreatedDate
--,Legacy_ID__c                     =  Id
,Name                             =  bg.[Name]

into BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_MAPLESB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_MAPLESB.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

----For Delta Load
--left join Superion_FULLSB.dbo.BGIntegration__Customer_Exit_Survey_Response__c trgt
--on trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(28070 row(s) affected)

--------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c;--28070

Select count(*) from BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload;--28070

Select Legacy_Id__c,count(*) from Staging_SB_MapleRoots.dbo.BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB_MapleRoots.dbo.BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load
from BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload; --(28070 row(s) affected)

Select * from Staging_SB_MapleRoots.dbo.BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load ;


--Exec SF_ColCompare_test 'Insert','SL_SUPERION_MAPLESB', 'BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__Customer_Exit_Survey_Response__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Customer_Exit_Survey_Response__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load'

----------------------------------------------------------------------------------------

Select *
--delete 
from BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --8

--insert into BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load
select * from BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Load_Delta; 