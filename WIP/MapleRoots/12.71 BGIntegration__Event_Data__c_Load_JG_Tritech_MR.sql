 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Event_Data__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'MC_Tritech_PROD','BGIntegration__Event_Data__c'

Use SUPERION_MAPLESB
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','User'
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','BGIntegration__Event__c'
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c'
*/ 

Use Staging_SB_MapleRoots;

--Drop table BGIntegration__Event_Data__c_Tritech_SFDC_Preload;
 
 DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_MAPLESB.dbo.[User] 
where Name like 'Bomgar Site Guest User');

 Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__Event__c_orig  =  bg.BGIntegration__Event__c
,BGIntegration__Event__c  =  evnt.Id

,BGIntegration__Value__c  =  bg.BGIntegration__Value__c

,CreatedById_orig         =  bg.CreatedById
,CreatedById              =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate              =  bg.CreatedDate
--,LegacyID__c              =  Id
,Name                     =  bg.[Name]

into BGIntegration__Event_Data__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__Event_Data__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_MAPLESB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BGIntegrationEventId(BGIntegration__Event__c Lookup)
left join SUPERION_MAPLESB.dbo.BGIntegration__Event__c evnt
on evnt.Legacy_id__c=bg.BGIntegration__Event__c

----For Delta Load
--left join Superion_FULLSB.dbo.BGIntegration__Event_Data__c trgt on
--trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(3669033 row(s) affected)

-------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Event_Data__c;--3669033

Select count(*) from BGIntegration__Event_Data__c_Tritech_SFDC_Preload;--3669033

Select Legacy_Id__c,count(*) from Staging_SB_MapleRoots.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB_MapleRoots.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__Event_Data__c_Tritech_SFDC_Load
from BGIntegration__Event_Data__c_Tritech_SFDC_Preload; --(3669033 row(s) affected)

Select count(*) from Staging_SB_MapleRoots.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Load;


--Exec SF_ColCompare 'Insert','SL_SUPERION_MAPLESB', 'BGIntegration__Event_Data__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__Event_Data__c does not contain column BGIntegration__Event__c_orig
Salesforce object BGIntegration__Event_Data__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c_Tritech_SFDC_Load' (4:01:10)

select *, ntile(4) over(order by Legacy_id__c) series
into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
from BGIntegration__Event_Data__c_Tritech_SFDC_Load a;--3669033

--Series1
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_1 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=1;--917259

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_1'

--Series2
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_2 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=2;--917258

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_2'

--Series3
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_3 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=3;--917258

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_3'

--Series4
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_4 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=4;--917258

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_4'

--Drop table BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series;

--Select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_1;

--Insert into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_2;

--Insert into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_3;

--Insert into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_4;

Select count(*) from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series;--3669033

--Drop table BGIntegration__Event_Data__c_Tritech_SFDC_Load;

--Select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load
 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series;

 Select count(*) from BGIntegration__Event_Data__c_Tritech_SFDC_Load;--3669033
----------------------------------------------------------------------------------------

--Updating Legacy_Source_System__c,Migrated_Record__c

--Use Staging_SB_MapleRoots;

--Drop table BGIntegration__Event_Data__c_Tritech_SFDC_LoadUpdate

Select 

 Id =bg.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Legacy_id__c
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

into BGIntegration__Event_Data__c_Tritech_SFDC_LoadUpdate
from Staging_SB_MapleRoots.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Load bg
;--(2985915 row(s) affected)

 --Exec SF_ColCompare 'Update','SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c_Tritech_SFDC_LoadUpdate' 

/*
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_MAPLESB','BGIntegration__Event_Data__c_Tritech_SFDC_LoadUpdate' 

---------------------------------------------------------

Select count(*)
--delete 
from BGIntegration__Event_Data__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --156975

--insert into BGIntegration__Event_Data__c_Tritech_SFDC_Load
select * from BGIntegration__Event_Data__c_Tritech_SFDC_Load_Delta; 