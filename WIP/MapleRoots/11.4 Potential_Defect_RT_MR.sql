/*****************************************************************************************************************************************************
REQ #		: REQ-0831
TITLE		: Migrate Potential_Defect__c - Source System to SFDC
DEVELOPER	: RTECSON
CREATED DT  : 12/07/2018
DETAIL		: 	
				Selection Criteria:
				a. Migrate all related defects related to a migrated case.

				Discussion: Will migrate to Engineering_Issue__c
				need to Validate with Engineering what their requirements are, and need to also confirm requirements specific to defects NOT related to a case.
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
12/07/2018		Ron Tecson			Initial
12/10/2018		Ron Tecson			Fixed the Original Customer logic. Needed to reload the target table.
12/11/2018		Ron Tecson		    Fixed Legacy_TT_Defect_Number to Legacy_TT_Defect_Number__c.
									De-scripted sPD1.Legacy_Defect_Ticket_Number__c per Gabe.
									Set the transformation for the Priority__c per Gabe.
									Added TFS_Work_Item__c to the script.
12/14/2018		Ron Tecson			Modified to add the transformation logic of the Priority__c field. Proposed transformation was approved by Gabe.
12/15/2018		Ron Tecson			Per Gabe. Descript the ENGOwner__c.
01/02/2019		Ron	Tecson			Fixed the Technical Description related to RES-2237 (TEST-0280). https://superion--sandbox.cs66.my.salesforce.com/a050v000001eBg9
01/04/2019		Ron Tecson			Added the additional fields and new logics from the data mapping.
01/09/2019		Ron Tecson			Fixed the target field ReleasedIn__c.
02/04/2019		Ron Tecson			Added the following for integration with TFS:
									- 'TFS' as Target_System__c
									- sPD1.TFS_Work_Item__c as ENGIssue__c
									Run a hotfix to populate the 2 fields for TFS.
02/28/2019		Ron Tecson			Loaded data against MapleRoots.
03/20/2019		Ron Tecson			Loaded data against MapleRoots and log time and errors.
03/26/2019		Ron Tecson			Modified based from additional new fields and changed logic from Gabe and Jeff Beard for Integration Requirements.

DECISIONS:

ERROR/FAILURES:

DATE			ERROR													RESOLUTION
=============== ======================================================= ========================================================================================
03/20/2019		Support Account: id value of incorrect type: Account	Accounts associated to the Tritech Cases doesn't exists in the target Org.
					Not Found

******************************************************************************************************************************************************/


USE Tritech_PROD

EXEC SF_Refresh 'MC_TRITECH_PROD',Potential_Defect__c, Yes 


USE SUPERION_MAPLESB

	EXEC SF_Refresh 'RT_Superion_MAPLEROOTS',EngineeringIssue__c, Yes
	EXEC SF_Refresh 'RT_Superion_MAPLEROOTS',Account, Yes
	EXEC SF_Refresh 'RT_Superion_MAPLEROOTS','User', Yes

USE Staging_SB_MapleRoots

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'EngineeringIssue__c_Preload' AND
            table_schema = 'dbo' )
  DROP TABLE Staging_SB_MapleRoots.[dbo].[EngineeringIssue__c_Preload]; 

DECLARE @UserDefault as NVARCHAR(18) = (select id from SUPERION_MAPLESB.dbo.[USER] where Name = 'Superion API'),
		@VersionDefault as NVARCHAR(18) = (select id from SUPERION_MAPLESB.dbo.[Version__c] where Name = 'PSJ-Inform v1' and LegacyTTZProductGroup__c  = 'TC CAD/Mobile');

select 
ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
,sPD1.Id AS Legacy_Id__c
,IIF(tAcc2.Id IS NOT NULL, sPD1.Original_Customer__c, sPD1.Account_Name__c) AS AccountId_Orig
,sPD1.Original_Contact__c AS Contact_Orig
,IIF(tCon1.Id IS NOT NULL,tCon1.ID,NULL) AS Support_Contact__c
,CASE sPD1.Area_Path__c
	WHEN 'CustomInterface' THEN 'TriTech-IntegratedSolutions'
	WHEN 'EMSBilling' THEN 'Respond-Billing'
	WHEN 'ePCR' THEN 'Respond-ePCR'
	WHEN 'Inform 5' THEN 'TriTech-IQ'
	WHEN 'Inform 911' THEN 'Tritech-911'
	WHEN 'InformCAD911' THEN 'Inform-CAD911'
	WHEN 'IntegratedSolutions' THEN 'TriTech-IntegratedSolutions'
	WHEN 'TTMS' THEN 'TriTech-MessageSwitch'
	WHEN 'VisionCAD MOBILE MOL' THEN 'Vision-CADMobile'
	WHEN 'VisionRMSJAILFBRFIRE MOL' THEN 'Inform-RMS'
	ELSE sPD1.Area_Path__c
 END AS ENGProject__c
,sPD1.CreatedById AS CreatedById_Orig
,IIF(tUser1.Id IS NOT NULL, tUser1.Id, @UserDefault) AS CreatedById -- 0056A000000lfqkQAA
,sPD1.CreatedDate AS CreatedDate
,sPD1.Defect_Ticket_ID__c AS Legacy_TT_Defect_Number__c
,CASE 
	WHEN sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NOT NULL AND sPD1.CCB_Notes__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10),CAST(sPD1.TFS_Comments__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.TFS_Comments__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10), CAST(sPD1.TFS_Comments__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CONCAT('CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CAST(sPD1.Description__c as nvarchar(max))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CAST(sPD1.TFS_Comments__c as nvarchar(max))
	ELSE NULL
END AS Technical_Description__c
--,sPD1.Found_in_Build__c AS Version__c
,sPD1.Id AS Legacy_CRMId
,sPD1.Internal_Steps_to_Recreate_Issue__c AS TroubleshootingSteps__c
--,sPD1.Known_Issues_Description__c AS KnownIssueDescription__c -- 1/3 RT. Descript. This field should be sourced from Case.
--,sPD1.Legacy_Defect_Ticket_Number__c AS Legacy_CRMId__c
,sPD1.MOL_Score__c AS ENGEngineering_Value__c
,sPD1.Name AS Summary__c
,sPD1.OwnerId  AS OwnerID_Orig
--,IIF(tOwner.Id IS NOT NULL, tOwner.Id, @UserDefault) AS ENGOwner__c
,sPD1.Patch_Release_Branch__c AS Patch_Release_Branch__c
,sPD1.Patch_Status_Comments__c AS Patch_Status_Comments__c
,CASE 
	WHEN sPD1.Priority__c like '%1' THEN '1 - Urgent'
	WHEN sPD1.Priority__c like '%2' THEN '2 - Critical'
	WHEN sPD1.Priority__c like '%3' THEN '3 - Non Critical'
	ELSE '4 - Minor'
 END AS Priority__c
--,sPD1.Product_Sub_Module__c AS Problem_Code__c
,sPD1.Released_In__c AS ReleasedIn__c
--,sPD1.Reported_Major_Version__c AS Version__c
,sPD1.Resolution_Notes__c AS ENGResolution__c
--,sPD1.Severity__c AS Severity__c
,sPD1.Priority__c as sPD1_Priority__c_Orig
,sPD1.Priority__c AS Severity__c
,tSOwner.Id AS Support_Owner__c
,sPD1.Target_Patch_Release_Date__c AS Target_Patch_Release_Date__c
,sPD1.TFS_Area_Path__c AS sPD1_TFS_Area_Path__c_Orig
--,IIF(sPD1.TFS_Area_Path__c = NULL AND sPD1.TFS_Area_Path__c = '', sPD1.Area_Path__c, sPD1.TFS_Area_Path__c) AS ENGAreaPath__c
,IIF(sPD1.TFS_Area_Path__c IS NULL, sPD1.Area_Path__c, sPD1.TFS_Area_Path__c) AS ENGAreaPath__c
,sPD1.TFS_Collection__c AS Collection__c
, CASE	
	WHEN sPD1.Ticket_Status__c = 'Assessing Issue ' OR sPD1.Ticket_Status__c = 'Fix for Release in Progress' THEN 'Committed'
	WHEN sPD1.Ticket_Status__c = 'Duplicate ' THEN 'Removed' 
	WHEN sPD1.Ticket_Status__c = 'Fix ready for Release' THEN 'Done' 
	WHEN sPD1.Ticket_Status__c = 'Future Consideration' THEN 'New'
  END AS ENGEngineering_Status__c
, sCase1.Patch_Requested_Date_Time__c AS Patch_Requested_Date_Time__c
, CASE
	WHEN sCase1.Known_Issue_Confirmed__c = 'true' THEN 'Approved' 
	WHEN sCase1.Known_Issues_List__c = 'true' and sCase1.Known_Issue_Confirmed__c = 'false' THEN 'Submitted'
END AS KnownIssueStatus__c
, sCase1.Patch_Status__c as Patch_Status__c_Orig
, CASE 
	WHEN sCase1.Patch_Status__c = 'Assess for Patch' OR sCase1.Patch_Status__c = 'Pending Decision to Assess' OR sCase1.Patch_Status__c = 'Pending Decision to Patch' THEN 'Patch Requested'
	ELSE sCase1.Patch_Status__c
END AS Patch_Status__c
, IIF(sCase1.Known_Issue_Confirmed__c = 'true', sCase1.Known_Issue_Description__c, NULL) AS KnownIssueDescription__c
--, IIF(sCase1.Known_Issues_List__c = 'true' and sCase1.Known_Issue_Confirmed__c = 'false', 'Submitted', NULL)
, sPD1.TFS_Work_Item__c AS TFSWorkItemLegacyID__c
, tCase2.AccountId AS Support_Account__c
, 'TFS' as Target_System__c
, sPD1.TFS_Work_Item__c as ENGIssue__c
, sCase1.Id as srcCaseId
, sAccount.Name AS src_AccountName
, sAccount.Id As Legacy_Account_Id
, @VersionDefault AS Version__c
INTO Staging_SB_MapleRoots.[dbo].[EngineeringIssue__c_Preload_Hotfix]
FROM tritech_Prod.dbo.[case] sCase1
JOIN [Tritech_PROD].[dbo].[Potential_Defect__c] sPD1 ON sPD1.Defect_Ticket_ID__c = sCase1.Defect_Number__c
LEFT JOIN SUPERION_MAPLESB.dbo.Account tAcc1 ON tAcc1.LegacySFDCAccountId__c = sPD1.Account_Name__c
LEFT JOIN SUPERION_MAPLESB.dbo.[User] tUser1 ON tUser1.Legacy_Tritech_Id__c = sPD1.CreatedById
LEFT JOIN SUPERION_MAPLESB.dbo.[User] tOwner ON tOwner.Legacy_Tritech_Id__c = sPD1.OwnerId
LEFT JOIN SUPERION_MAPLESB.dbo.[User] tSOwner ON tSOwner.Email = sPD1.Support_Ticket_Owner_Email__c
LEFT JOIN SUPERION_MAPLESB.dbo.Account tAcc2 ON tAcc2.LegacySFDCAccountId__c = sPD1.Original_Customer__c
LEFT JOIN SUPERION_MAPLESB.dbo.Contact tCon1 ON tCon1.Legacy_ID__c = sPD1.Original_Contact__c
LEFT JOIN Staging_SB_MapleRoots.dbo.Case_Tritech_SFDC_load tCase2 ON tCase2.Legacy_id__c = sCase1.Id
LEFT JOIN tritech_Prod.dbo.Account sAccount ON sAccount.Id = sCase1.AccountId -- 3/20 RT. Added for troublshooting purposes.
WHERE
sPD1.Ticket_Status__c in (
'Assessing Issue',
'Duplicate',
'Fix for Release in Progress',
'Fix ready for Release',
'Future Consideration')

-- 3/20 -- (16267 rows affected)

IF EXISTS
( SELECT *
    FROM   information_schema.TABLES
    WHERE  table_name = 'EngineeringIssue__c_Load' AND
        table_schema = 'dbo' )
DROP TABLE Staging_SB_MapleRoots.[dbo].[EngineeringIssue__c_Load]; 

SELECT *
INTO Staging_SB_MapleRoots.dbo.EngineeringIssue__c_Load
FROM Staging_SB_MapleRoots.dbo.EngineeringIssue__c_Preload 

-- (15725 rows affected) 01/04
-- (16157 rows affected) 02/28
-- (16267 rows affected) 03/20


USE Staging_SB_MapleRoots

EXEC SF_ColCompare 'Insert','RT_SUPERION_MAPLEROOTS', 'EngineeringIssue__c_Load' 

select * from RT_SUPERION_MAPLEROOTS...EngineeringIssue__c where EngIssue__c = '251318'

/************************************* L   O   G   S **************************************
3/20
Salesforce object EngineeringIssue__c does not contain column AccountId_Orig
Salesforce object EngineeringIssue__c does not contain column Contact_Orig
Salesforce object EngineeringIssue__c does not contain column CreatedById_Orig
Salesforce object EngineeringIssue__c does not contain column Legacy_CRMId
Salesforce object EngineeringIssue__c does not contain column OwnerID_Orig
Salesforce object EngineeringIssue__c does not contain column Patch_Status__c_Orig
Column Target_System__c is not insertable into the salesforce object EngineeringIssue__c

Salesforce object EngineeringIssue__c does not contain column AccountId_Orig
Salesforce object EngineeringIssue__c does not contain column Contact_Orig
Salesforce object EngineeringIssue__c does not contain column CreatedById_Orig
Salesforce object EngineeringIssue__c does not contain column Legacy_CRMId
Salesforce object EngineeringIssue__c does not contain column OwnerID_Orig
Salesforce object EngineeringIssue__c does not contain column Released_In__c
Salesforce object EngineeringIssue__c does not contain column Patch_Status__c_Orig

************************************** L   O   G   S **************************************/

EXEC SF_BulkOps 'INSERT','RT_SUPERION_MAPLEROOTS', 'EngineeringIssue__c_Load'

/************************************** L   O   G   S **************************************

3/20
--- Starting SF_BulkOps for EngineeringIssue__c_Load V3.6.7
12:45:35: Run the DBAmp.exe program.
12:45:35: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
12:45:35: Inserting EngineeringIssue__c_Load (SQL01 / Staging_SB_MapleRoots).
12:45:36: DBAmp is using the SQL Native Client.
12:45:38: SOAP Headers: 
12:45:38: Warning: Column 'AccountId_Orig' ignored because it does not exist in the EngineeringIssue__c object.
12:45:38: Warning: Column 'Contact_Orig' ignored because it does not exist in the EngineeringIssue__c object.
12:45:38: Warning: Column 'CreatedById_Orig' ignored because it does not exist in the EngineeringIssue__c object.
12:45:38: Warning: Column 'Legacy_CRMId' ignored because it does not exist in the EngineeringIssue__c object.
12:45:38: Warning: Column 'OwnerID_Orig' ignored because it does not exist in the EngineeringIssue__c object.
12:45:38: Warning: Column 'Patch_Status__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
12:45:38: Warning: Column 'Target_System__c' ignored because it not insertable in the EngineeringIssue__c object.
12:48:56: 16267 rows read from SQL Table.
12:48:56: 319 rows failed. See Error column of row for more information.
12:48:56: 15948 rows successfully processed.
12:48:56: Errors occurred. See Error column of row for more information.
12:48:57: Percent Failed = 2.000.
12:48:57: Error: DBAmp.exe was unsuccessful.
12:48:57: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert EngineeringIssue__c_Load "SQL01"  "Staging_SB_MapleRoots"  "RT_SUPERION_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 201]
SF_BulkOps Error: 12:45:35: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC12:45:35: Inserting EngineeringIssue__c_Load (SQL01 / Staging_SB_MapleRoots).12:45:36: DBAmp is using the SQL Native Client.12:45:38: SOAP Headers: 12:45:38: Warning: Column 'AccountId_Orig' ignored because it does not exist in the EngineeringIssue__c object.12:45:38: Warning: Column 'Contact_Orig' ignored because it does not exist in the EngineeringIssue__c object.12:45:38: Warning: Column 'CreatedById_Orig' ignored because it does not exist in the EngineeringIssue__c object.12:45:38: Warning: Column 'Legacy_CRMId' ignored because it does not exist in the EngineeringIssue__c object.12:45:38: Warning: Column 'OwnerID_Orig' ignored because it does not exist in the EngineeringIssue__c object.12:45:38: Warning: Column 'Patch_Status__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.12:45:38: Warning: Column 'Target_System__c' ignored because it not insertable in the EngineeringIssue__c object.12:48:56: 16267 rows read from SQL Table.12:48:56: 319 rows failed. See Error column of row for more information.12:48:56: 15948 rows successfully processed.12:48:56: Errors occurred. See Error column of row for more information.


--- Starting SF_BulkOps for EngineeringIssue__c_Load V3.6.7
19:22:02: Run the DBAmp.exe program.
19:22:02: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
19:22:02: Inserting EngineeringIssue__c_Load (SQL01 / Staging_SB_MapleRoots).
19:22:03: DBAmp is using the SQL Native Client.
19:22:03: SOAP Headers: 
19:22:04: Warning: Column 'AccountId_Orig' ignored because it does not exist in the EngineeringIssue__c object.
19:22:04: Warning: Column 'Contact_Orig' ignored because it does not exist in the EngineeringIssue__c object.
19:22:04: Warning: Column 'CreatedById_Orig' ignored because it does not exist in the EngineeringIssue__c object.
19:22:04: Warning: Column 'Legacy_CRMId' ignored because it does not exist in the EngineeringIssue__c object.
19:22:04: Warning: Column 'OwnerID_Orig' ignored because it does not exist in the EngineeringIssue__c object.
19:22:04: Warning: Column 'Patch_Status__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
19:22:04: Warning: Column 'Target_System__c' ignored because it not insertable in the EngineeringIssue__c object.
19:24:54: 16157 rows read from SQL Table.
19:24:54: 16157 rows successfully processed.
19:24:54: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

************************************** L   O   G   S **************************************/

select ERROR, count(*)
from EngineeringIssue__c_Load
group by ERROR

select * from EngineeringIssue__c_Load where error = 'Value too large max length:30000 Your length: 31013'

select * from EngineeringIssue__c_Load where Error = 'Patch Status: bad value for restricted picklist field: Patch Requested'

'insufficient access rights on cross-reference id: 0018000001KXiSf'

select * from Superion_FULLSB.dbo.Account where Id = '0018000001KXiSfAAL'

select * from EngineeringIssue__c_Load where error not like '%success%'

select Id, AccountId, count(*) from Staging_SB_MapleRoots.dbo.Case_Tritech_SFDC_load where AccountId = 'Account Not Found'
group by Id, AccountId

/************************************** REFRESH LOCAL DATABASE **************************************/

USE SUPERION_MAPLESB
EXEC SF_Refresh 'RT_Superion_MAPLEROOTS',EngineeringIssue__c, Yes

-------------------------------------------------------------------------
-- Remove Columns that generates errors

DROP TABLE [STAGING_SB].[dbo].[EngineeringIssue__c_Load_slim];

select * into Staging_SB.dbo.EngineeringIssue__c_Load_slim 
from Staging_SB.dbo.EngineeringIssue__c_Load -- (13360 rows affected)

select * from Staging_SB.dbo.EngineeringIssue__c_Load_slim

ALTER TABLE Staging_SB.dbo.EngineeringIssue__c_Load_slim 
DROP COLUMN Version__c;

ALTER TABLE Staging_SB.dbo.EngineeringIssue__c_Load_slim 
DROP COLUMN Problem_Code__c;

ALTER TABLE Staging_SB.dbo.EngineeringIssue__c_Load_slim 
DROP COLUMN ENGEngineering_Status__c;

---------------------------------------------------------------------
-- 12/10 Created the secondary table to eliminate the error in the Load Table. Had to drop 3 fields to insert the records in the Target.

USE Staging_SB

EXEC SF_BulkOps 'INSERT','MC_SUPERION_FULLSB', 'EngineeringIssue__c_Load_slim'

select error, count(*) from
Staging_SB.dbo.EngineeringIssue__c_Load_slim
group by error order by error

select * from Staging_SB.dbo.EngineeringIssue__c_Load_slim
where Error = 'insufficient access rights on cross-reference id: 0038000000bEccU' -- Support_Contact__c

select * from Staging_SB.dbo.EngineeringIssue__c_Load_slim
where Error = 'Problem Code: id value of incorrect type: a0A8000000a6gxdEAA' -- Problem_Code__c

ALTER TABLE Staging_SB.dbo.EngineeringIssue__c_Load_slim 
DROP COLUMN Problem_Code__c;

--EXEC SF_BulkOps 'DELETE','MC_SUPERION_FULLSB', 'EngineeringIssue__c_Load_slim'

/*
Mostly, the Potential Defect doesn't have any Account association (Account_Name__c), will they be orphan records?
If the Original CreatedById is not in the Target, what should be the default?
If the Original OwnerId is not in the Target, what should be the default?
Source Fields: Original_Contact__c and Original_Customer__c are reference fields. No transformation?
Explain the transformation rule for the Potential_Defect Support_Ticket_Owner_Email__c, "Use to look up related User"?
Concatenating TFS_Comments__c and Description__c for Technical_Description__c


select * from Superion_FULLSB.dbo.Account tAcc1 where tAcc1.LegacySFDCAccountId__c in (
'0018000000Rl2OwAAJ',
'0018000000qvSAxAAM',
'0018000000QrqIhAAJ',
'0018000000rdN9yAAE',
'0018000000QrqL2AAJ')

select top 5 * from Superion_FULLSB.dbo.Contact where Legacy_Id__c in (
'00380000016rdSYAAY',
'00380000010zhSLAAY',
'00380000017cBDFAA2',
'0038000000bEduQAAS')
*/

--select id, Name from Superion_FULLSB.dbo.[USer] where Name = 'Superion API'

select error, count(*) from
Staging_SB.dbo.EngineeringIssue__c_Load where error not like 'Current%'
and error not like 'Engineering%'
group by error order by error

select * from Staging_SB.dbo.EngineeringIssue__c_Load
where error = 'insufficient access rights on cross-reference id: 0038000001B9Px8'
--where error = 'Current Version: id value of incorrect type: 1'
where error = 'Error: Batch error occurred with this set of rows.The content of elements must consist of well-formed character data or markup.'

TFSWorkItemLegacyID__c

use Superion_FULLSB

select * from contact where Id = '0038000001B9Px8AAF'


select error, count(*) from Staging_SB.dbo.EngineeringIssue__c_Load_slim 
group by error

select Problem__Code__c from Staging_SB.dbo.EngineeringIssue__c_Load_slim 

ENGEngineering_Status__c

select * from Staging_SB.dbo.EngineeringIssue__c_Load_slim 


/* Refresh Superion_FULLSB after successful load */

USE Superion_FULLSB

	EXEC SF_Refresh 'MC_SUPERION_FULLSB',EngineeringIssue__c, Yes


/************ HOTFIX *************/
USE Staging_SB

ALTER TABLE EngineeringIssue__c_Load_slim
  ADD Legacy_TT_Defect_Number__c NVARCHAR(15);


update EngineeringIssue__c_Load_slim
set Legacy_TT_Defect_Number__c = Legacy_TT_Defect_Number

select ID, ERROR, Legacy_TT_Defect_Number__c --INTO EngineeringIssue__c_Load_slim_hotfix
from EngineeringIssue__c_Load_slim

EXEC SF_BulkOps 'UPDATE','MC_SUPERION_FULLSB', 'EngineeringIssue__c_Load_slim_hotfix'

select A.ID, A.Legacy_TT_Defect_Number__c, B.Id, B.Legacy_TT_Defect_Number__c 
from Staging_SB.dbo.EngineeringIssue__c_Load_slim A --_hotfix A
inner join MC_SUPERION_FULLSB...EngineeringIssue__c B ON A.ID = B.ID

/**************************************************************
select t4.id
      ,cast(null as nvarchar(255)) as Error
      ,t3.id as Parent_Engineering_Issue__c
      ,t1.id as legacy_Case_id_orig
      ,t1.Defect_Number__c as Defect_Number_orig
      ,t2.id as [legacy_potential_defect_id]
--Into Case_update_MC_Parent_Eng_Issue      
from tritech_Prod.dbo.[case] t1
join tritech_Prod.dbo.[potential_defect__c] t2 on t2.Defect_Ticket_ID__c = t1.Defect_Number__c
left join EngineeringIssue__c_Load_slim t3 on t3.Legacy_Id__c = t2.id
--left
  join Case_Tritech_SFDC_load t4 on t4.Legacy_Id__c = t1.id
where t1.Defect_Number__c is not null

***************************************************************/

select TFS_Work_Item__c, count(*) from tritech_Prod.dbo.[potential_defect__c]
group by TFS_Work_Item__c

/*************** Hotfix for the Priority__c - 12/14/2018 *****************************************************/
select ID,ERROR,
CASE 
	WHEN Priority__c like '%1%' THEN '1 - Urgent'
	WHEN Priority__c like '%2%' THEN '2 - Critical'
	WHEN Priority__c like '%3%' THEN '3 - Non Critical'
	ELSE '4 - Minor'
END Priority__c 
INTO EngineeringIssue__c_Load_slim_hotfix_priority
from EngineeringIssue__c_Load_slim
order by 1

select distinct Priority__c from EngineeringIssue__c_Load_slim

EXEC SF_BulkOps 'UPDATE','MC_SUPERION_FULLSB', 'EngineeringIssue__c_Load_slim_hotfix_priority'

--- Starting SF_BulkOps for EngineeringIssue__c_Load_slim_hotfix_priority V3.6.9
12:58:58: Run the DBAmp.exe program.
12:58:58: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
12:58:58: Updating Salesforce using EngineeringIssue__c_Load_slim_hotfix_priority (SQL01 / Staging_SB) .
12:58:58: DBAmp is using the SQL Native Client.
12:58:59: SOAP Headers: 
13:00:30: 16225 rows read from SQL Table.
13:00:30: 14 rows failed. See Error column of row for more information.
13:00:30: 16211 rows successfully processed.
13:00:30: Errors occurred. See Error column of row for more information.
13:00:30: Percent Failed = 0.100.
13:00:30: Error: DBAmp.exe was unsuccessful.
13:00:30: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe update EngineeringIssue__c_Load_slim_hotfix_priority "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 23]
SF_BulkOps Error: 12:58:58: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC12:58:58: Updating Salesforce using EngineeringIssue__c_Load_slim_hotfix_priority (SQL01 / Staging_SB) .12:58:58: DBAmp is using the SQL Native Client.12:58:59: SOAP Headers: 13:00:30: 16225 rows read from SQL Table.13:00:30: 14 rows failed. See Error column of row for more information.13:00:30: 16211 rows successfully processed.13:00:30: Errors occurred. See Error column of row for more information.

select ERROR, Count(*) from EngineeringIssue__c_Load_slim_hotfix_priority
group by ERROR

select Id from EngineeringIssue__c_Load_slim_hotfix_priority where ERROR = 'invalid cross reference id'


----------------------------------

select * from Staging_SB.dbo.EngineeringIssue__c_Load

USE Staging_SB

select ID,
ERROR,
Released_In__c as ReleasedIn__c
INTO Staging_SB.dbo.EngineeringIssue__c_Load_HF_RES_2722
from
Staging_SB.dbo.EngineeringIssue__c_Load

select * from Staging_SB.dbo.EngineeringIssue__c_Load_HF_RES_2722

update Staging_SB.dbo.EngineeringIssue__c_Load_HF_RES_2722
set error = NULL

EXEC SF_ColCompare 'Insert','MC_SUPERION_FULLSB', 'EngineeringIssue__c_Load_HF_RES_2722'

EXEC SF_BulkOps 'UPDATE','MC_SUPERION_FULLSB', 'EngineeringIssue__c_Load_HF_RES_2722'

select ReleasedIn__c
from
EngineeringIssue__c_Load_HF_RES_2722
where
id = 'a930v00000051aAAAQ'

------------------ Hotfix for TFS

select ID, Error,
'TFS' as Target_System__c,
TFSWorkItemLegacyID__c as ENGIssue__c 
INTO Staging_SB.dbo.EngineeringIssue__c_HF_ForTFS_Load_RT
from Staging_SB.dbo.EngineeringIssue__c_Load

USE Staging_SB

select Target_System__c, ENGIssue__c from Staging_SB.dbo.EngineeringIssue__c_HF_ForTFS_Load_RT

EXEC SF_ColCompare 'UPDATE','MC_SUPERION_FULLSB', 'EngineeringIssue__c_HF_ForTFS_Load_RT'

select * from EngineeringIssue__c_HF_ForTFS_Load_RT

update EngineeringIssue__c_HF_ForTFS_Load_RT
set error = null

select Target_System__c from RT_SUPERION_FULLSB...EngineeringIssue__c where EngIssue__c = '207234'

EXEC SF_BulkOps 'UPDATE','MC_SUPERION_FULLSB', 'EngineeringIssue__c_HF_ForTFS_Load_RT'

select count(*) from EngineeringIssue__c_HF_ForTFS_Load_RT
where ENGIssue__c is null

14765
960

select ENGIssue__c as TFSWorkItemLegacyID__c 
 from [Tritech_PROD].[dbo].[Potential_Defect__c]

select * from Staging_SB.dbo.Case_Tritech_SFDC_load

select id, error, ENGIssue__c  as EngIssue_orig, ENGIssue__c as TFSWorkItemLegacyID__c 
--into EngineeringIssue__c_update_TFSWorkItemLegacyID
from staging_sb.dbo.EngineeringIssue__c_Load_slim
where ENGIssue__c is not null;