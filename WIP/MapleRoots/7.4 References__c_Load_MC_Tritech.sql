/*
-- Author     : Marty Corpus	
-- Date       : 11/6/2018
-- Description: Migrate Requirement__c data from the Tritech Prod org to the Superion org (References, API: Contact_Reference_Link__c)
Requirement Number:REQ-0846
Scope:
Migrate all records that relate to a migrated opportunity.

2018-11-06 Missing 2 custome fields - config needed.
           1. [Reference_Document_Event__c]
		   2. [If_Other_Please_List_Document_Event__c]
2018-11-10 Scripted Missing fields.
           Opp load table pending - waiting for Patrick to load for UAT1
2018-11-12 Executed for UAT1 Load.
2018-12-04 Executed for UAT2 Load.
2018-12-18 Fix join to get account id.
2019-01-02 MartyC. Executed for E2E load.
2019-02-28 MartyC. Executed for MapleRoots load.
2019-03-21 MartyC. Executed for MapleRoots load2.

*/
-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-02-28 Executed in MapleRoots FullSbx.
-- 2019-03-21 Executed in MapleRoots FullSbx.
-- 
/*
--checks:

--check for dups: select Legacy_Id__c, count(*) from Contact_Reference_Link__c_Load_MC_Tritech group by Legacy_Id__c having count(*) > 1 --3/21 Mroots MC 0 records
--checK for account ids not found: select * from Contact_Reference_Link__c_Load_MC_Tritech where account__c is null or account__c = 'ID NOT FOUND'  --3/21 Mroots MC MC 1 records
--check for   oppty ids not found: select * from Contact_Reference_Link__c_Load_MC_Tritech where Opportunity__c is null or Opportunity__c = 'ID NOT FOUND' --3/21 Mroots MC 1 records
--check count of legacy source table: select count(*) from [Tritech_PROD].[dbo].[REFERENCES__C] --173 3/21 TT
--check piclist values: select distinct Reference_Document_Event__c from Contact_Reference_Link__c_Load_MC_Tritech;
/*Demo
Other
Proactive Proposal
Reference Letter
RFX Response
*/
*/

-- exec sf_colcompare 'Insert','MC_SUPERION_MAPLEROOTS','Contact_Reference_Link__c_Load_MC_Tritech'
/*
--- Starting SF_ColCompare V3.6.7
Problems found with Contact_Reference_Link__c_Load_MC_Tritech. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 42]
--- Ending SF_ColCompare. Operation FAILED.

ErrorDesc
Salesforce object Contact_Reference_Link__c does not contain column CreatedbyIdName_orig
Salesforce object Contact_Reference_Link__c does not contain column Account_orig
Salesforce object Contact_Reference_Link__c does not contain column CreatedById_orig
Salesforce object Contact_Reference_Link__c does not contain column CreatedDate_orig
Salesforce object Contact_Reference_Link__c does not contain column isdeleted_orig
Salesforce object Contact_Reference_Link__c does not contain column LastActivityDate_orig
Salesforce object Contact_Reference_Link__c does not contain column LastModifiedById_orig
Salesforce object Contact_Reference_Link__c does not contain column LastModifiedDate_orig
Salesforce object Contact_Reference_Link__c does not contain column Name_orig
Salesforce object Contact_Reference_Link__c does not contain column Opportunity_orig
Salesforce object Contact_Reference_Link__c does not contain column SystemModstamp_orig


*/

-- alter table Contact_Reference_Link__c_Load_MC_Tritech add ident int identity(1,1);
select * from Contact_Reference_Link__c_Load_MC_Tritech;



 -- exec SF_BulkOps 'Insert','MC_SUPERION_MAPLEROOTS','Contact_Reference_Link__c_Load_MC_Tritech';

 /*
--- Starting SF_BulkOps for Contact_Reference_Link__c_Load_MC_Tritech V3.6.7
18:28:41: Run the DBAmp.exe program.
18:28:41: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:28:41: Inserting Contact_Reference_Link__c_Load_MC_Tritech (SQL01 / Staging_SB_MapleRoots).
18:28:41: DBAmp is using the SQL Native Client.
18:28:43: SOAP Headers: 
18:28:43: Warning: Column 'CreatedbyIdName_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'Account_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'CreatedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'CreatedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'isdeleted_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'LastActivityDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'LastModifiedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'LastModifiedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'Name_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'Opportunity_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'SystemModstamp_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:43: Warning: Column 'ident' ignored because it does not exist in the Contact_Reference_Link__c object.
18:28:44: 173 rows read from SQL Table.
18:28:44: 1 rows failed. See Error column of row for more information.
18:28:44: 172 rows successfully processed.
18:28:44: Errors occurred. See Error column of row for more information.
18:28:44: Percent Failed = 0.600.
18:28:44: Error: DBAmp.exe was unsuccessful.
18:28:44: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Contact_Reference_Link__c_Load_MC_Tritech "SQL01"  "Staging_SB_MapleRoots"  "MC_SUPERION_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 70]
SF_BulkOps Error: 18:28:41: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC18:28:41: Inserting Contact_Reference_Link__c_Load_MC_Tritech (SQL01 / Staging_SB_MapleRoots).18:28:41: DBAmp is using the SQL Native Client.18:28:43: SOAP Headers: 18:28:43: Warning: Column 'CreatedbyIdName_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'Account_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'CreatedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'CreatedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'isdeleted_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'LastActivityDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'LastModifiedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'LastModifiedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'Name_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'Opportunity_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'SystemModstamp_orig' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:43: Warning: Column 'ident' ignored because it does not exist in the Contact_Reference_Link__c object.18:28:44: 173 rows read from SQL Table.18:28:44: 1 rows failed. See Error column of row for more information.18:28:44: 172 rows successfully processed.18:28:44: Errors occurred. See Error column of row for more information.

select * from Contact_Reference_Link__c_Load_MC_Tritech where error not like '%success%';
the TT customer is an internal account Account_orig = '0018000000Quv78AAB'; out of scope.
*/



USE [Tritech_PROD]

EXEC Sf_refresh 'MC_TRITECH_PROD','References__c','Yes';
--- Starting SF_Refresh for References__c V3.6.7
--18:16:59: Using Schema Error Action of yes
--18:17:00: Using last run time of 2019-02-28 19:03:00
--18:17:00: Identified 0 updated/inserted rows.
--18:17:00: Identified 0 deleted rows.
--- Ending SF_Refresh. Operation successful.


EXEC SF_Refresh 'MC_TRITECH_PROD','user','yes'
--- Starting SF_Refresh for user V3.6.7
--18:17:24: Using Schema Error Action of yes
--18:17:24: Using last run time of 2019-03-20 04:35:00
--18:17:27: Identified 612 updated/inserted rows.
--18:17:27: Identified 0 deleted rows.
--18:17:27: Adding updated/inserted rows into user
--- Ending SF_Refresh. Operation successful.


use SUPERION_MapleSB;

EXEC SF_Refresh 'MC_SUPERION_MAPLEROOTS','Opportunity','yes'
----- Starting SF_Refresh for Opportunity V3.6.7
--18:18:19: Using Schema Error Action of yes
--18:18:21: Using last run time of 2019-03-21 10:48:00
--18:19:44: Identified 6869 updated/inserted rows.
--18:19:45: Identified 0 deleted rows.
--18:19:45: Adding updated/inserted rows into Opportunity
----- Ending SF_Refresh. Operation successful.


EXEC SF_Refresh 'MC_SUPERION_MAPLEROOTS','User','yes'

--- Starting SF_Refresh for User V3.6.7
--18:20:36: Using Schema Error Action of yes
--18:20:37: Using last run time of 2019-03-21 08:23:00
--18:20:37: Identified 0 updated/inserted rows.
--18:20:37: Identified 0 deleted rows.
--- Ending SF_Refresh. Operation successful.





USE [Staging_SB_MapleRoots]



go

-- drop table Contact_Reference_Link__c_Load_MC_Tritech

-- User: 0056A000000lfqkQAA	Superion API Fullsb
-- User: 0056A000000lfqkQAA Superion API MapleRoots

-- select id, name from [SUPERION_MAPLESB].dbo.[user] where name like '%Superion API%'

SELECT Cast( NULL AS NCHAR(18))         AS Id,
       Cast( NULL AS NVARCHAR(255))     AS Error,
       rf.[id]                          AS Legacy_Id__c,
       'true'                           AS Migrated_Record__c,
       'Tritech'                        AS Legacy_Source_System__c,
       coalesce(u.id,'0056A000000lfqkQAA')                            AS Ownerid,
	   tu.name + ' | ' + tu.Isactive       as CreatedbyIdName_orig,
	   rf.Account__c                    AS Account_orig ,
       coalesce(a.id,'ID NOT FOUND')    AS Account__c, --12/18
       coalesce(opp.id,'ID NOT FOUND')  AS Opportunity__c, --11/9 waiting on Partick's opp load.
       rf.[reference_document_event__c] AS [Reference_Document_Event__c], --11/9
       rf.[if_other_please_list_document_event__c] as [If_Other_Please_List_Document_Event__c], --11/9
       rf.[createdbyid]                 [CreatedById_orig],
       rf.[createddate]                 AS [CreatedDate_orig],
       rf.[isdeleted]                   as [isdeleted_orig]  ,
       rf.[lastactivitydate]            AS [LastActivityDate_orig],
       rf.[lastmodifiedbyid]            AS [LastModifiedById_orig],
       rf.[lastmodifieddate]            AS [LastModifiedDate_orig],
       rf.[name]                        AS [Name_orig],
       rf.[opportunity__c]              AS [Opportunity_orig],
       rf.[systemmodstamp]              AS [SystemModstamp_orig]
INTO   Contact_Reference_Link__c_Load_MC_Tritech
FROM   [Tritech_PROD].[dbo].[REFERENCES__C] rf
       LEFT JOIN [Tritech_PROD].[dbo].[user] tu on tu.id = rf.CreatedById
       LEFT JOIN SUPERION_MapleSB.dbo.[Opportunity] opp
              ON opp.legacy_opportunity_id__c = rf.opportunity__c --11/9 waiting on Partick's opp load.
       LEFT JOIN SUPERION_MapleSB.dbo.[USER] u
              ON u.[Legacy_Tritech_Id__c] = rf.createdbyid
       LEFT JOIN [MC_SUPERION_MAPLEROOTS]...[Account] a --12/18
              ON a.legacysfdcaccountid__c = rf.account__c; --(150 row(s) affected) 11/12
			  --(162 row(s) affected)  1/2 E2E MC
			  --(173 row(s) affected) 2/28 MRoots MC
			  --(173 row(s) affected) 3/21 MC

  


select  * from Contact_Reference_Link__c_Load_MC_Tritech



