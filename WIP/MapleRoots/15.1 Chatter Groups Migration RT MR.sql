/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: ChatterGroup (CollaborationGroup) - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.1 Chatter Groups Migration PB.sql
02/26/2019		Ron Tecson			Configured for MapleRoots connection. Loaded 13 records.
03/19/2019		Ron Tecson			Loaded 16 records.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------
Use Tritech_Prod

Exec SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
Use Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroup_PreLoad_RT') 
DROP TABLE Staging_SB_MapleRoots.dbo.CollaborationGroup_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots;

Select
	CAST('' as nchar(18)) as [ID],
	CAST('' as nvarchar(255)) as Error,
	a.Name as Name,
	a.CollaborationType as CollaborationType,
	a.[Description] as [Description],
	a.InformationTitle as InformationTitle,
	a.InformationBody as InformationBody,
	a.CanHaveGuests as CanHaveGuests,
	a.IsArchived as IsArchived,
	a.IsAutoArchiveDisabled as IsAutoArchiveDisabled,
	a.IsBroadcast as IsBroadCast
INTO Staging_SB_MapleRoots.dbo.CollaborationGroup_PreLoad_RT
FROM TriTech_Prod.dbo.CollaborationGroup a
WHERE a.IsArchived = 'false'
	AND a.MemberCount > 0
	AND a.LastFeedModifiedDate >= CAST('09/30/2018' as Datetime)

-- 2/26 (13 rows affected)
-- 3/19 (16 rows affected)

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroup_Load_RT') 
DROP TABLE Staging_SB_MapleRoots.dbo.CollaborationGroup_Load_RT;

SELECT *
INTO Staging_SB_MapleRoots.dbo.CollaborationGroup_Load_RT
FROM Staging_SB_MapleRoots.dbo.CollaborationGroup_PreLoad_RT

-- 2/26 (13 rows affected)
-- 3/19 (16 rows affected)

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE Staging_SB_MapleRoots.dbo.CollaborationGroup_load_RT
ADD [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Staging_SB_MapleRoots

Exec SF_ColCompare 'insert', 'RT_Superion_MAPLEROOTS', 'CollaborationGroup_load_RT'

/********************************** COLCOMPARE L   O   G   ********************************************************

--- Starting SF_ColCompare V3.6.7
Problems found with CollaborationGroup_load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 86]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object CollaborationGroup does not contain column Sort

********************************** L   O   G   ********************************************************/

Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'RT_Superion_MAPLEROOTS', 'CollaborationGroup_load_RT'

/********************************** L   O   G   ********************************************************

3/19
--- Starting SF_BulkOps for CollaborationGroup_load_RT V3.6.7
12:41:54: Run the DBAmp.exe program.
12:41:54: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
12:41:54: Inserting CollaborationGroup_load_RT (SQL01 / Staging_SB_MapleRoots).
12:41:55: DBAmp is using the SQL Native Client.
12:41:55: Batch size reset to 4000 rows per batch.
12:41:55: Sort column will be used to order input rows.
12:41:55: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
12:41:56: Job 7500r000000033QAAQ created.
12:41:56: Batch 7510r00000005xLAAQ created with 16 rows.
12:41:57: Job submitted.
12:41:57: 16 rows read from SQL Table.
12:41:57: Job still running.
12:42:13: Job Complete.
12:42:14: DBAmp is using the SQL Native Client.
12:42:14: 16 rows successfully processed.
12:42:14: 0 rows failed.
12:42:14: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


2/26
--- Starting SF_BulkOps for CollaborationGroup_load_RT V3.6.7
21:35:32: Run the DBAmp.exe program.
21:35:32: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
21:35:32: Inserting CollaborationGroup_load_RT (SQL01 / Staging_SB_MapleRoots).
21:35:32: DBAmp is using the SQL Native Client.
21:35:32: Batch size reset to 4000 rows per batch.
21:35:33: Sort column will be used to order input rows.
21:35:33: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
21:35:34: Job 7501h000001x3M7AAI created.
21:35:34: Batch 7511h000001v8URAAY created with 13 rows.
21:35:35: Job submitted.
21:35:35: 13 rows read from SQL Table.
21:35:36: Job still running.
21:35:51: Job Complete.
21:35:52: DBAmp is using the SQL Native Client.
21:35:52: 13 rows successfully processed.
21:35:52: 0 rows failed.
21:35:52: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

********************************** L   O   G   ********************************************************/

---------------------------------------------------------------------------------
-- Refresh Data to the local database
---------------------------------------------------------------------------------
USE Superion_MAPLESB;

EXEC SF_Refresh 'RT_Superion_MAPLEROOTS', 'CollaborationGroup', 'yes';