﻿/*****************************************************************************************************************************************************
REQ #		: REQ-0811
DEVELOPER	: RTECSON
CREATED DT  : 11/19/2018
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
11/19/2018		Ron Tecson			Initial
11/26/2018		Ron Tecson			Modified the script to accomodate the multiple win strategy record to a opportunity using the Rank function.
12/05/2018		Ron Tecson			Added the logic changes on the transformation: Customers_Competitive_Alternative__c, Customer_s_Defined_Initiatives__c
										Deal_Strategy__c and What_are_your_Vulnerabilities__c.
12/15/2018		Ron Tecson			Corrected the What_are_your_Vulnerabilities__c to use TriTech_Weaknesses__c instead of the Pricing_and_Funding_Issues__c.
01/02/2019		Ron Tecson			Removed the Customer_s_Defined_Initiatives__c and added the new field "Description" provided by Jen in the data mapping.
01/03/2019		Ron Tecson			Fixed the transformation of the Customer Executive Priorities to transform the Contact Id to the Contact Name. 
02/28/2019		Ron Tecson			Loaded Data against MapleRoots.
03/20/2019		Ron Tecson			Loaded Data against MapleRoots. No Errors.
									
DECISIONS:

******************************************************************************************************************************************************/

USE SUPERION_MAPLESB

	EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS',Opportunity, Yes

USE Tritech_PROD

	EXEC SF_Refresh 'MC_TRITECH_PROD',Win_Strategy__c, Yes 

	select * from Tritech_PROD.dbo.Win_Strategy__c

USE Staging_SB_MapleRoots

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'Opportunity_Tritech_PreLoad_1st' AND
            table_schema = 'dbo' )
  DROP TABLE Staging_SB_MapleRoots.[dbo].[Opportunity_Tritech_PreLoad_1st]; 

WITH
cte_sfdc_Opty AS
(	
	SELECT
		Id AS OptyId
		, Customers_Competitive_Alternative__c AS ccAlternative
		, Customer_s_Defined_Initiatives__c AS cdInitiatives
		, Customer_s_Executive_Priorities__c AS cePriorities
		, What_are_your_Vulnerabilities__c AS wayVulnerabilities
		, Deal_Strategy__c AS dStrategy
		, What_are_your_Differentiators__c AS wayDifferentiators
		, Legacy_Opportunity_ID__c AS legacyOptyId
		, Description AS Description
	FROM SUPERION_MAPLESB.dbo.Opportunity
),

cte_Contact AS
(
	SELECT
		Id as Legacy_ID__c
		, Name as Name
	FROM [Tritech_PROD].[dbo].[Contact]
),

cte_Main AS
(
			SELECT
				 RANK ()  OVER(PARTITION BY tOPTY1.OptyId ORDER BY sWS1.CreatedDate) AS RankRank
				 , tOPTY1.OptyId AS ID
				 , CAST(NULL AS NVARCHAR(255)) AS Error
				 , sWS1.Id AS Win_Strategy_Id_Orig
				 , sWS1.Opportunity__c as Opty_Id_Orig
				 , CASE 
					WHEN tOPTY1.ccAlternative IS NOT NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NOT NULL THEN
							CONCAT(tOPTY1.ccAlternative,CHAR(10),cast(sWS1.CreatedDate as NVARCHAR(10)),':',CAST(sWS1.Competitor_Strengths_Weaknesses__c AS NVARCHAR(MAX)))
					WHEN tOPTY1.ccAlternative IS NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NOT NULL THEN
							CONCAT(cast(sWS1.CreatedDate as NVARCHAR(10)),' : ',CAST(sWS1.Competitor_Strengths_Weaknesses__c AS NVARCHAR(MAX)))
					WHEN tOPTY1.ccAlternative IS NOT NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NULL THEN
							tOPTY1.ccAlternative
					ELSE
						NULL
				  END AS Customers_Competitive_Alternative__c
				  /*, IIF(tOPTY1.cdInitiatives IS NOT NULL OR sWS1.Evaluation_Criteria__c IS NOT NULL OR sWS1.General_Overview_of_Project_Scope__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.cdInitiatives IS NOT NULL, CONCAT(tOPTY1.cdInitiatives,CHAR(10)),NULL),
							IIF(sWS1.Evaluation_Criteria__c IS NOT NULL, CONCAT(Evaluation_Criteria__c, CHAR(10)),NULL),
							IIF(sWS1.General_Overview_of_Project_Scope__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.General_Overview_of_Project_Scope__c), NULL)
							), NULL) AS Customer_s_Defined_Initiatives__c */
				  /*, IIF(tOPTY1.cdInitiatives IS NOT NULL OR sWS1.Evaluation_Criteria__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.cdInitiatives IS NOT NULL, CONCAT(tOPTY1.cdInitiatives,CHAR(10)),NULL),
							IIF(sWS1.Evaluation_Criteria__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', Evaluation_Criteria__c),NULL)
							), NULL) AS Customer_s_Defined_Initiatives__c  */ -- Commented out 01/02/2018
				  /*, CONCAT(IIF(tOPTY1.dStrategy IS NOT NULL, CONCAT(CAST(tOPTY1.dStrategy AS NVARCHAR(MAX)),CHAR(10)),NULL),
							IIF(sWS1.Strategy_Statement__c IS NOT NULL,CONCAT(sWS1.Strategy_Statement__c,CHAR(10)),NULL),
							IIF(sWS1.Strategy_to_Maximize_TriTech_s_Scoring__c IS NOT NULL,CONCAT(Strategy_to_Maximize_TriTech_s_Scoring__c,CHAR(10)),NULL),
							IIF(sWS1.Executive_Summary__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Executive_Summary__c), NULL)) AS Deal_Strategy__c */
				  ,IIF(tOPTY1.dStrategy IS NOT NULL OR sWS1.Strategy_Statement__c IS NOT NULL OR sWS1.Strategy_to_Maximize_TriTech_s_Scoring__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.dStrategy IS NOT NULL, CONCAT(CAST(tOPTY1.dStrategy AS NVARCHAR(MAX)),CHAR(10)),NULL),
							IIF(sWS1.Strategy_Statement__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Strategy_Statement__c,CHAR(10)),NULL),
							IIF(sWS1.Strategy_to_Maximize_TriTech_s_Scoring__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', Strategy_to_Maximize_TriTech_s_Scoring__c),NULL)),
							NULL) AS Deal_Strategy__c

				  , IIF(tOPTY1.wayDifferentiators IS NOT NULL OR sWS1.Top_3_or_More_Differentiators__c IS NOT NULL OR sWS1.TriTech_Strengths__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.wayDifferentiators IS NOT NULL, CONCAT(tOPTY1.wayDifferentiators,CHAR(10)),NULL),
							IIF(sWS1.Top_3_or_More_Differentiators__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Top_3_or_More_Differentiators__c, CHAR(10)), NULL),
							IIF(sWS1.TriTech_Strengths__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Strengths__c), NULL)) 
							, NULL) 		
							AS What_are_your_Differentiators__c
				  /*, IIF (tOPTY1.wayVulnerabilities IS NOT NULL OR sWS1.Pricing_and_Funding_Issues__c IS NOT NULL OR sWS1.TriTech_Weaknesses__c IS NOT NULL,
							CONCAT(IIF(tOPTY1.wayVulnerabilities IS NOT NULL, CONCAT(tOPTY1.wayVulnerabilities,CHAR(10)),NULL),
									IIF(sWS1.Pricing_and_Funding_Issues__c IS NOT NULL, CONCAT(sWS1.Pricing_and_Funding_Issues__c, CHAR(10)),NULL),
									IIF(sWS1.TriTech_Weaknesses__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Weaknesses__c), NULL)) 
							, NULL)	AS What_are_your_Vulnerabilities__c */
				  , IIF (tOPTY1.wayVulnerabilities IS NOT NULL OR sWS1.Pricing_and_Funding_Issues__c IS NOT NULL ,
							CONCAT(IIF(tOPTY1.wayVulnerabilities IS NOT NULL, CONCAT(tOPTY1.wayVulnerabilities,CHAR(10)),NULL),
								   IIF(sWS1.Pricing_and_Funding_Issues__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Pricing_and_Funding_Issues__c),NULL),
								   IIF(sWS1.TriTech_Weaknesses__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Weaknesses__c),NULL))
							, NULL)	AS What_are_your_Vulnerabilities__c
				  , CONCAT(
							IIF(tOPTY1.cePriorities IS NOT NULL, CONCAT(tOPTY1.cePriorities, CHAR(10)),NULL),
							IIF(sWS1.Executive_Summary__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Executive_Summary__c, CHAR(10)), NULL),
				
							IIF(sCON1.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_1_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON1.Name IS NOT NULL, CONCAT(sCON1.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_1_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_1_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							),

							IIF(sCON2.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_2_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON2.Name IS NOT NULL, CONCAT(sCON2.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_2_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_2_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							),

							IIF(sCON3.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_3_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON3.Name IS NOT NULL, CONCAT(sCON3.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_3_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_3_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							)
							) AS Customer_s_Executive_Priorities__c
					, sWS1.CreatedDate AS Win_Strategy_CreatedDate
					, IIF(tOpty1.Description IS NOT NULL OR sWS1.Evaluation_Criteria__c IS NOT NULL OR sWS1.General_Overview_of_Project_Scope__c IS NOT NULL,
						CONCAT(IIF(tOpty1.Description IS NOT NULL, CONCAT(tOpty1.Description, CHAR(10)),NULL),
								IIF(sWS1.Evaluation_Criteria__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Evaluation_Criteria__c,CHAR(10)),NULL), 
								IIF(sWS1.General_Overview_of_Project_Scope__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.General_Overview_of_Project_Scope__c,CHAR(10)),NULL)),
								NULL) AS Description
			FROM 
			[Tritech_PROD].[dbo].[Win_Strategy__c] sWS1
			  LEFT JOIN cte_sfdc_Opty tOPTY1 ON sWS1.Opportunity__c = tOPTY1.legacyOptyId
			  LEFT JOIN cte_Contact sCON1 ON sWS1.Key_Decision_Maker_1__c = sCON1.Legacy_ID__c
			  LEFT JOIN cte_Contact sCON2 ON sWS1.Key_Decision_Maker_2__c = sCON2.Legacy_ID__c
			  LEFT JOIN cte_Contact sCON3 ON sWS1.Key_Decision_Maker_3__c = sCON3.Legacy_ID__c
			--where 
			--	 tOPTY1.OptyId IN ('0060v000004Wqv9AAC',
			--						'0060v000004WoG4AAK',
			--						'0060v000004Wm0GAAS',
			--						'0060v000004WtBlAAK',
			--						'0060v000004WtGcAAK',
			--						'0060v000004WtSvAAK',
			--						'0060v000004WtVgAAK')
		--) MAIN
)

select * INTO Staging_SB_MapleRoots.dbo.Opportunity_Tritech_PreLoad_1st
from cte_Main 
where RankRank = 1 --order by 1

-- 3/20 -- (149 rows affected)
-- 2/28 -- 147 Records

-- select * from Staging_SB.dbo.Opportunity_Tritech_PreLoad_1st_HF_tcePriorities

IF EXISTS
( SELECT *
    FROM   information_schema.TABLES
    WHERE  table_name = 'Opportunity_Tritech_Load_1st' AND
        table_schema = 'dbo' )
DROP TABLE [Staging_SB_MapleRoots].[dbo].[Opportunity_Tritech_Load_1st]; 

SELECT *
INTO Staging_SB_MapleRoots.dbo.Opportunity_Tritech_Load_1st
FROM Staging_SB_MapleRoots.dbo.Opportunity_Tritech_PreLoad_1st --where ID = '0060v000004Wqv9AAC'

EXEC SF_ColCompare 'Update','RT_Superion_MAPLEROOTS', 'Opportunity_Tritech_Load_1st' 

/*****************************************************************************************************************************************************
3/20 -- 67 secs
2/28 -- 60 secs

--- Starting SF_ColCompare V3.6.7
Problems found with Opportunity_Tritech_Load_1st. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 207]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Opportunity does not contain column RankRank
Salesforce object Opportunity does not contain column Win_Strategy_Id_Orig
Salesforce object Opportunity does not contain column Opty_Id_Orig
Salesforce object Opportunity does not contain column Win_Strategy_CreatedDate

******************************************************************************************************************************************************/

EXEC SF_BulkOps 'Update','RT_Superion_MAPLEROOTS', 'Opportunity_Tritech_Load_1st'

/*****************************************************************************************************************************************************
3/20: < 1 min.
--- Starting SF_BulkOps for Opportunity_Tritech_Load_1st V3.6.7
15:15:17: Run the DBAmp.exe program.
15:15:17: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:15:17: Updating Salesforce using Opportunity_Tritech_Load_1st (SQL01 / Staging_SB_MapleRoots) .
15:15:18: DBAmp is using the SQL Native Client.
15:15:19: SOAP Headers: 
15:15:20: Warning: Column 'RankRank' ignored because it does not exist in the Opportunity object.
15:15:20: Warning: Column 'Win_Strategy_Id_Orig' ignored because it does not exist in the Opportunity object.
15:15:20: Warning: Column 'Opty_Id_Orig' ignored because it does not exist in the Opportunity object.
15:15:20: Warning: Column 'Win_Strategy_CreatedDate' ignored because it does not exist in the Opportunity object.
15:15:35: 149 rows read from SQL Table.
15:15:35: 149 rows successfully processed.
15:15:35: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

2/28: < 1 min.

--- Starting SF_BulkOps for Opportunity_Tritech_PreLoad_1st V3.6.7
20:02:01: Run the DBAmp.exe program.
20:02:01: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
20:02:01: Updating Salesforce using Opportunity_Tritech_PreLoad_1st (SQL01 / Staging_SB_MapleRoots) .
20:02:01: DBAmp is using the SQL Native Client.
20:02:02: SOAP Headers: 
20:02:02: Warning: Column 'RankRank' ignored because it does not exist in the Opportunity object.
20:02:02: Warning: Column 'Win_Strategy_Id_Orig' ignored because it does not exist in the Opportunity object.
20:02:02: Warning: Column 'Opty_Id_Orig' ignored because it does not exist in the Opportunity object.
20:02:02: Warning: Column 'Win_Strategy_CreatedDate' ignored because it does not exist in the Opportunity object.
20:02:12: 147 rows read from SQL Table.
20:02:12: 147 rows successfully processed.
20:02:12: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.



--- Starting SF_BulkOps for Opportunity_Tritech_Load_1st V3.6.7
05:46:00: Run the DBAmp.exe program.
05:46:00: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
05:46:00: Updating Salesforce using Opportunity_Tritech_Load_1st (SQL01 / Staging_SB) .
05:46:01: DBAmp is using the SQL Native Client.
05:46:01: SOAP Headers: 
05:46:02: Warning: Column 'RankRank' ignored because it does not exist in the Opportunity object.
05:46:02: Warning: Column 'Win_Strategy_Id_Orig' ignored because it does not exist in the Opportunity object.
05:46:02: Warning: Column 'Opty_Id_Orig' ignored because it does not exist in the Opportunity object.
05:46:02: Warning: Column 'Win_Strategy_CreatedDate' ignored because it does not exist in the Opportunity object.
05:46:21: 127 rows read from SQL Table.
05:46:21: 6 rows failed. See Error column of row for more information.
05:46:21: 121 rows successfully processed.
05:46:21: Errors occurred. See Error column of row for more information.
05:46:21: Percent Failed = 4.700.
05:46:21: Error: DBAmp.exe was unsuccessful.
05:46:21: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe update Opportunity_Tritech_Load_1st "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 171]
SF_BulkOps Error: 05:46:00: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC05:46:00: Updating Salesforce using Opportunity_Tritech_Load_1st (SQL01 / Staging_SB) .05:46:01: DBAmp is using the SQL Native Client.05:46:01: SOAP Headers: 05:46:02: Warning: Column 'RankRank' ignored because it does not exist in the Opportunity object.05:46:02: Warning: Column 'Win_Strategy_Id_Orig' ignored because it does not exist in the Opportunity object.05:46:02: Warning: Column 'Opty_Id_Orig' ignored because it does not exist in the Opportunity object.05:46:02: Warning: Column 'Win_Strategy_CreatedDate' ignored because it does not exist in the Opportunity object.05:46:21: 127 rows read from SQL Table.05:46:21: 6 rows failed. See Error column of row for more information.05:46:21: 121 rows successfully processed.05:46:21: Errors occurred. See Error column of row for more information.

******************************************************************************************************************************************************/


SELECT ERROR, count(*) Row_Count FROM Staging_SB_MapleRoots.dbo.Opportunity_Tritech_Load_1st
GROUP BY ERROR

select * from Staging_SB_MapleRoots.dbo.Opportunity_Tritech_Load_1st where error = 'Operation Successful.'

/***************************** 2nd Pass: There are multiple Win Strategy Records to a single Opportunity ****************************/

USE Superion_MapleSB

	EXEC SF_Refresh 'MC_SUPERION_MAPLEROOTS',Opportunity, Yes

USE Staging_SB_MapleRoots

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'Opportunity_Tritech_PreLoad_2nd' AND
            table_schema = 'dbo' )
  DROP TABLE [Staging_SB_MapleRoots].[dbo].[Opportunity_Tritech_PreLoad_2nd]; 

WITH
cte_sfdc_Opty AS
(	
	SELECT
		Id AS OptyId
		, Customers_Competitive_Alternative__c AS ccAlternative
		, Customer_s_Defined_Initiatives__c AS cdInitiatives
		, Customer_s_Executive_Priorities__c AS cePriorities
		, What_are_your_Vulnerabilities__c AS wayVulnerabilities
		, Deal_Strategy__c AS dStrategy
		, What_are_your_Differentiators__c AS wayDifferentiators
		, Legacy_Opportunity_ID__c AS legacyOptyId
		, Description AS Description
	FROM Superion_MAPLESB.dbo.Opportunity
),

cte_Contact AS
(
	SELECT
		Id as Legacy_ID__c
		, Name as Name
	FROM [Tritech_PROD].[dbo].[Contact]
),

cte_Main AS
(
			SELECT
				 RANK ()  OVER(PARTITION BY tOPTY1.OptyId ORDER BY sWS1.CreatedDate) AS RankRank
				 , tOPTY1.OptyId AS ID
				 , CAST(NULL AS NVARCHAR(255)) AS Error
				 , sWS1.Id AS Win_Strategy_Id_Orig
				 , sWS1.Opportunity__c as Opty_Id_Orig
				 , CASE 
					WHEN tOPTY1.ccAlternative IS NOT NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NOT NULL THEN
							CONCAT(tOPTY1.ccAlternative,CHAR(10),cast(sWS1.CreatedDate as NVARCHAR(10)),':',CAST(sWS1.Competitor_Strengths_Weaknesses__c AS NVARCHAR(MAX)))
					WHEN tOPTY1.ccAlternative IS NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NOT NULL THEN
							CONCAT(cast(sWS1.CreatedDate as NVARCHAR(10)),' : ',CAST(sWS1.Competitor_Strengths_Weaknesses__c AS NVARCHAR(MAX)))
					WHEN tOPTY1.ccAlternative IS NOT NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NULL THEN
							tOPTY1.ccAlternative
					ELSE
						NULL
				  END AS Customers_Competitive_Alternative__c
				  /*, IIF(tOPTY1.cdInitiatives IS NOT NULL OR sWS1.Evaluation_Criteria__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.cdInitiatives IS NOT NULL, CONCAT(tOPTY1.cdInitiatives,CHAR(10)),NULL),
							IIF(sWS1.Evaluation_Criteria__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', Evaluation_Criteria__c),NULL)
							), NULL) AS Customer_s_Defined_Initiatives__c */ -- Commented out 01/02/2018
				  ,IIF(tOPTY1.dStrategy IS NOT NULL OR sWS1.Strategy_Statement__c IS NOT NULL OR sWS1.Strategy_to_Maximize_TriTech_s_Scoring__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.dStrategy IS NOT NULL, CONCAT(CAST(tOPTY1.dStrategy AS NVARCHAR(MAX)),CHAR(10)),NULL),
							IIF(sWS1.Strategy_Statement__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Strategy_Statement__c,CHAR(10)),NULL),
							IIF(sWS1.Strategy_to_Maximize_TriTech_s_Scoring__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', Strategy_to_Maximize_TriTech_s_Scoring__c),NULL)),
							NULL) AS Deal_Strategy__c
				  , IIF(tOPTY1.wayDifferentiators IS NOT NULL OR sWS1.Top_3_or_More_Differentiators__c IS NOT NULL OR sWS1.TriTech_Strengths__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.wayDifferentiators IS NOT NULL, CONCAT(tOPTY1.wayDifferentiators,CHAR(10)),NULL),
							IIF(sWS1.Top_3_or_More_Differentiators__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Top_3_or_More_Differentiators__c, CHAR(10)), NULL),
							IIF(sWS1.TriTech_Strengths__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Strengths__c), NULL)) 
							, NULL) 		
							AS What_are_your_Differentiators__c
				  , IIF (tOPTY1.wayVulnerabilities IS NOT NULL OR sWS1.Pricing_and_Funding_Issues__c IS NOT NULL ,
							CONCAT(IIF(tOPTY1.wayVulnerabilities IS NOT NULL, CONCAT(tOPTY1.wayVulnerabilities,CHAR(10)),NULL),
								   IIF(sWS1.Pricing_and_Funding_Issues__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Pricing_and_Funding_Issues__c),NULL),
								   IIF(sWS1.TriTech_Weaknesses__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Weaknesses__c),NULL))
							, NULL)	AS What_are_your_Vulnerabilities__c
				  , CONCAT(
							IIF(tOPTY1.cePriorities IS NOT NULL, CONCAT(tOPTY1.cePriorities, CHAR(10)),NULL),
							IIF(sWS1.Executive_Summary__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Executive_Summary__c, CHAR(10)), NULL),
				
							IIF(sCON1.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_1_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON1.Name IS NOT NULL, CONCAT(sCON1.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_1_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_1_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							),

							IIF(sCON2.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_2_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON2.Name IS NOT NULL, CONCAT(sCON2.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_2_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_2_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							),

							IIF(sCON3.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_3_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON3.Name IS NOT NULL, CONCAT(sCON3.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_3_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_3_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							)
							) AS Customer_s_Executive_Priorities__c
					, sWS1.CreatedDate AS Win_Strategy_CreatedDate
					, IIF(tOpty1.Description IS NOT NULL OR sWS1.Evaluation_Criteria__c IS NOT NULL OR sWS1.General_Overview_of_Project_Scope__c IS NOT NULL,
						CONCAT(IIF(tOpty1.Description IS NOT NULL, CONCAT(tOpty1.Description, CHAR(10)),NULL),
								IIF(sWS1.Evaluation_Criteria__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Evaluation_Criteria__c,CHAR(10)),NULL), 
								IIF(sWS1.General_Overview_of_Project_Scope__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.General_Overview_of_Project_Scope__c,CHAR(10)),NULL)),
								NULL) AS Description
			FROM 
			[Tritech_PROD].[dbo].[Win_Strategy__c] sWS1
			  LEFT JOIN cte_sfdc_Opty tOPTY1 ON sWS1.Opportunity__c = tOPTY1.legacyOptyId
			  LEFT JOIN cte_Contact sCON1 ON sWS1.Key_Decision_Maker_1__c = sCON1.Legacy_ID__c
			  LEFT JOIN cte_Contact sCON2 ON sWS1.Key_Decision_Maker_2__c = sCON2.Legacy_ID__c
			  LEFT JOIN cte_Contact sCON3 ON sWS1.Key_Decision_Maker_3__c = sCON3.Legacy_ID__c
)

select * into Staging_SB_MapleRoots.dbo.Opportunity_Tritech_PreLoad_2nd from cte_Main 
where RankRank = 2 --order by 1

-- 3/20 -- (8 rows affected)  5 secs

IF EXISTS
( SELECT *
    FROM   information_schema.TABLES
    WHERE  table_name = 'Opportunity_Tritech_Load_2nd' AND
        table_schema = 'dbo' )
DROP TABLE [Staging_SB_MapleRoots].[dbo].[Opportunity_Tritech_Load_2nd]; 

SELECT *
INTO Staging_SB_MapleRoots.dbo.Opportunity_Tritech_Load_2nd
FROM Staging_SB_MapleRoots.dbo.Opportunity_Tritech_PreLoad_2nd

select 
ID,
Error,
len(cast(Customers_Competitive_Alternative__c as nvarchar(max))) Customers_Competitive_Alternative__c,
len(cast(Customer_s_Defined_Initiatives__c as nvarchar(max))) Customer_s_Defined_Initiatives__c,
len(cast(Customer_s_Executive_Priorities__c as nvarchar(max))) Customer_s_Executive_Priorities__c,
len(cast(What_are_your_Vulnerabilities__c as nvarchar(max))) What_are_your_Vulnerabilities__c,
len(cast(Deal_Strategy__c as nvarchar(max))) Deal_Strategy__c,
len(cast(What_are_your_Differentiators__c as nvarchar(max))) What_are_your_Differentiators__c
--INTO Staging_SB.dbo.Opportunity_Tritech_Load_1st_Error
FROM Staging_SB.dbo.Opportunity_Tritech_Load_2nd
where ERROR <> 'Operation Successful.' and  Id is not null

EXEC SF_ColCompare 'Update','RT_SUPERION_MAPLEROOTS', 'Opportunity_Tritech_Load_2nd' 

/*****************************************************************************************************************************************************
3/20 and 2/28 -- 60 secs

--- Starting SF_ColCompare V3.6.7
Problems found with Opportunity_Tritech_Load_1st. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 207]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Opportunity does not contain column RankRank
Salesforce object Opportunity does not contain column Win_Strategy_Id_Orig
Salesforce object Opportunity does not contain column Opty_Id_Orig
Salesforce object Opportunity does not contain column Win_Strategy_CreatedDate

******************************************************************************************************************************************************/


EXEC SF_BulkOps 'Update','RT_SUPERION_MAPLEROOTS', 'Opportunity_Tritech_Load_2nd'

/******************************************************************************************************************************************************
-- 3/20

--- Starting SF_BulkOps for Opportunity_Tritech_Load_2nd V3.6.7
15:19:37: Run the DBAmp.exe program.
15:19:37: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:19:37: Updating Salesforce using Opportunity_Tritech_Load_2nd (SQL01 / Staging_SB_MapleRoots) .
15:19:38: DBAmp is using the SQL Native Client.
15:19:38: SOAP Headers: 
15:19:38: Warning: Column 'RankRank' ignored because it does not exist in the Opportunity object.
15:19:38: Warning: Column 'Win_Strategy_Id_Orig' ignored because it does not exist in the Opportunity object.
15:19:38: Warning: Column 'Opty_Id_Orig' ignored because it does not exist in the Opportunity object.
15:19:38: Warning: Column 'Win_Strategy_CreatedDate' ignored because it does not exist in the Opportunity object.
15:19:40: 8 rows read from SQL Table.
15:19:40: 8 rows successfully processed.
15:19:40: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.
******************************************************************************************************************************************************/


--select * from Opportunity_Tritech_PreLoad_2nd_HF_tcePriorities --> Value too large max length:10000 Your length: 13063 on the Customer_s_Executive_Priorities__c

/***************************** 3rd Pass: There are multiple Win Strategy Records to a single Opportunity ****************************/

USE Superion_MAPLESB

	EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS',Opportunity, Yes

USE Staging_SB_MapleRoots

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'Opportunity_Tritech_PreLoad_3rd' AND
            table_schema = 'dbo' )
  DROP TABLE Staging_SB_MapleRoots.[dbo].[Opportunity_Tritech_PreLoad_3rd]; 

WITH
cte_sfdc_Opty AS
(	
	SELECT
		Id AS OptyId
		, Customers_Competitive_Alternative__c AS ccAlternative
		, Customer_s_Defined_Initiatives__c AS cdInitiatives
		, Customer_s_Executive_Priorities__c AS cePriorities
		, What_are_your_Vulnerabilities__c AS wayVulnerabilities
		, Deal_Strategy__c AS dStrategy
		, What_are_your_Differentiators__c AS wayDifferentiators
		, Legacy_Opportunity_ID__c AS legacyOptyId
		, Description AS Description
	FROM Superion_MAPLESB.dbo.Opportunity
),

cte_Contact AS
(
	SELECT
		Id as Legacy_ID__c
		, Name as Name
	FROM [Tritech_PROD].[dbo].[Contact]
),

cte_Main AS
(
			SELECT
				 RANK ()  OVER(PARTITION BY tOPTY1.OptyId ORDER BY sWS1.CreatedDate) AS RankRank
				 , tOPTY1.OptyId AS ID
				 , CAST(NULL AS NVARCHAR(255)) AS Error
				 , sWS1.Id AS Win_Strategy_Id_Orig
				 , sWS1.Opportunity__c as Opty_Id_Orig
				 , CASE 
					WHEN tOPTY1.ccAlternative IS NOT NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NOT NULL THEN
							CONCAT(tOPTY1.ccAlternative,CHAR(10),cast(sWS1.CreatedDate as NVARCHAR(10)),':',CAST(sWS1.Competitor_Strengths_Weaknesses__c AS NVARCHAR(MAX)))
					WHEN tOPTY1.ccAlternative IS NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NOT NULL THEN
							CONCAT(cast(sWS1.CreatedDate as NVARCHAR(10)),' : ',CAST(sWS1.Competitor_Strengths_Weaknesses__c AS NVARCHAR(MAX)))
					WHEN tOPTY1.ccAlternative IS NOT NULL AND
							sWS1.Competitor_Strengths_Weaknesses__c IS NULL THEN
							tOPTY1.ccAlternative
					ELSE
						NULL
				  END AS Customers_Competitive_Alternative__c
				  /*, IIF(tOPTY1.cdInitiatives IS NOT NULL OR sWS1.Evaluation_Criteria__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.cdInitiatives IS NOT NULL, CONCAT(tOPTY1.cdInitiatives,CHAR(10)),NULL),
							IIF(sWS1.Evaluation_Criteria__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', Evaluation_Criteria__c),NULL)
							), NULL) AS Customer_s_Defined_Initiatives__c */ -- Commented out 01/02/2018
				  ,IIF(tOPTY1.dStrategy IS NOT NULL OR sWS1.Strategy_Statement__c IS NOT NULL OR sWS1.Strategy_to_Maximize_TriTech_s_Scoring__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.dStrategy IS NOT NULL, CONCAT(CAST(tOPTY1.dStrategy AS NVARCHAR(MAX)),CHAR(10)),NULL),
							IIF(sWS1.Strategy_Statement__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Strategy_Statement__c,CHAR(10)),NULL),
							IIF(sWS1.Strategy_to_Maximize_TriTech_s_Scoring__c IS NOT NULL,CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', Strategy_to_Maximize_TriTech_s_Scoring__c),NULL)),
							NULL) AS Deal_Strategy__c
				  , IIF(tOPTY1.wayDifferentiators IS NOT NULL OR sWS1.Top_3_or_More_Differentiators__c IS NOT NULL OR sWS1.TriTech_Strengths__c IS NOT NULL,
					CONCAT(IIF(tOPTY1.wayDifferentiators IS NOT NULL, CONCAT(tOPTY1.wayDifferentiators,CHAR(10)),NULL),
							IIF(sWS1.Top_3_or_More_Differentiators__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Top_3_or_More_Differentiators__c, CHAR(10)), NULL),
							IIF(sWS1.TriTech_Strengths__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Strengths__c), NULL)) 
							, NULL) 		
							AS What_are_your_Differentiators__c
				  , IIF (tOPTY1.wayVulnerabilities IS NOT NULL OR sWS1.Pricing_and_Funding_Issues__c IS NOT NULL ,
							CONCAT(IIF(tOPTY1.wayVulnerabilities IS NOT NULL, CONCAT(tOPTY1.wayVulnerabilities,CHAR(10)),NULL),
								   IIF(sWS1.Pricing_and_Funding_Issues__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Pricing_and_Funding_Issues__c),NULL),
								   IIF(sWS1.TriTech_Weaknesses__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Weaknesses__c),NULL))
							, NULL)	AS What_are_your_Vulnerabilities__c
				  , CONCAT(
							IIF(tOPTY1.cePriorities IS NOT NULL, CONCAT(tOPTY1.cePriorities, CHAR(10)),NULL),
							IIF(sWS1.Executive_Summary__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Executive_Summary__c, CHAR(10)), NULL),
				
							IIF(sCON1.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_1_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON1.Name IS NOT NULL, CONCAT(sCON1.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_1_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_1_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_1_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							),

							IIF(sCON2.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_2_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON2.Name IS NOT NULL, CONCAT(sCON2.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_2_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_2_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_2_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							),

							IIF(sCON3.Name IS NOT NULL 
								OR 
								sWS1.Key_Decision_Maker_3_Title__c IS NOT NULL 
								OR
								sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c IS NOT NULL,
									CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ',
										IIF(sCON3.Name IS NOT NULL, CONCAT(sCON3.Name, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_3_Title__c IS NOT NULL, CONCAT(sWS1.Key_Decision_Maker_3_Title__c, ' '), NULL),
										IIF(sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c IS NOT NULL, CONCAT(CAST(sWS1.Key_Decision_Maker_3_Hot_Buttons_Biases__c AS NVARCHAR(MAX)), ' '), NULL),
										CHAR(10)
										),
								NULL
							)
							) AS Customer_s_Executive_Priorities__c
					, sWS1.CreatedDate AS Win_Strategy_CreatedDate
					, IIF(tOpty1.Description IS NOT NULL OR sWS1.Evaluation_Criteria__c IS NOT NULL OR sWS1.General_Overview_of_Project_Scope__c IS NOT NULL,
						CONCAT(IIF(tOpty1.Description IS NOT NULL, CONCAT(tOpty1.Description, CHAR(10)),NULL),
								IIF(sWS1.Evaluation_Criteria__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.Evaluation_Criteria__c,CHAR(10)),NULL), 
								IIF(sWS1.General_Overview_of_Project_Scope__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.General_Overview_of_Project_Scope__c,CHAR(10)),NULL)),
								NULL) AS Description
			FROM 
			[Tritech_PROD].[dbo].[Win_Strategy__c] sWS1
			  LEFT JOIN cte_sfdc_Opty tOPTY1 ON sWS1.Opportunity__c = tOPTY1.legacyOptyId
			  LEFT JOIN cte_Contact sCON1 ON sWS1.Key_Decision_Maker_1__c = sCON1.Legacy_ID__c
			  LEFT JOIN cte_Contact sCON2 ON sWS1.Key_Decision_Maker_2__c = sCON2.Legacy_ID__c
			  LEFT JOIN cte_Contact sCON3 ON sWS1.Key_Decision_Maker_3__c = sCON3.Legacy_ID__c
)

select * INTO Staging_SB_MapleRoots.dbo.Opportunity_Tritech_PreLoad_3rd from cte_Main 
where RankRank = 3 

-- 3/20 - (2 rows affected)

select * from Opportunity_Tritech_PreLoad_3rd_HF_tcePriorities

USE Staging_SB_MapleRoots

IF EXISTS
( SELECT *
    FROM   information_schema.TABLES
    WHERE  table_name = 'Opportunity_Tritech_Load_3rd' AND
        table_schema = 'dbo' )
DROP TABLE Staging_SB_MapleRoots.[dbo].[Opportunity_Tritech_Load_3rd]; 

SELECT *
INTO Staging_SB_MapleRoots.dbo.Opportunity_Tritech_Load_3rd
FROM Staging_SB_MapleRoots.dbo.Opportunity_Tritech_PreLoad_3rd

select 
ID,
Error,
len(cast(Customers_Competitive_Alternative__c as nvarchar(max))) Customers_Competitive_Alternative__c,
len(cast(Customer_s_Defined_Initiatives__c as nvarchar(max))) Customer_s_Defined_Initiatives__c,
len(cast(Customer_s_Executive_Priorities__c as nvarchar(max))) Customer_s_Executive_Priorities__c,
len(cast(What_are_your_Vulnerabilities__c as nvarchar(max))) What_are_your_Vulnerabilities__c,
len(cast(Deal_Strategy__c as nvarchar(max))) Deal_Strategy__c,
len(cast(What_are_your_Differentiators__c as nvarchar(max))) What_are_your_Differentiators__c
--INTO Staging_SB.dbo.Opportunity_Tritech_Load_1st_Error
FROM Staging_SB_MapleRoots.dbo.Opportunity_Tritech_Load_3rd

EXEC SF_ColCompare 'Update','RT_SUPERION_MAPLEROOTS', 'Opportunity_Tritech_Load_3rd' 

/*****************************************************************************************************************************************************
3/20 and 2/28 -- 60 secs

--- Starting SF_ColCompare V3.6.7
Problems found with Opportunity_Tritech_Load_3rd. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 207]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Opportunity does not contain column RankRank
Salesforce object Opportunity does not contain column Win_Strategy_Id_Orig
Salesforce object Opportunity does not contain column Opty_Id_Orig
Salesforce object Opportunity does not contain column Win_Strategy_CreatedDate

******************************************************************************************************************************************************/


EXEC SF_BulkOps 'Update','RT_SUPERION_MAPLEROOTS', 'Opportunity_Tritech_Load_3rd'

/*****************************************************************************************************************************************************
3/20 - 3 secs
--- Starting SF_BulkOps for Opportunity_Tritech_Load_3rd V3.6.7
15:27:05: Run the DBAmp.exe program.
15:27:05: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:27:05: Updating Salesforce using Opportunity_Tritech_Load_3rd (SQL01 / Staging_SB_MapleRoots) .
15:27:06: DBAmp is using the SQL Native Client.
15:27:06: SOAP Headers: 
15:27:06: Warning: Column 'RankRank' ignored because it does not exist in the Opportunity object.
15:27:06: Warning: Column 'Win_Strategy_Id_Orig' ignored because it does not exist in the Opportunity object.
15:27:06: Warning: Column 'Opty_Id_Orig' ignored because it does not exist in the Opportunity object.
15:27:06: Warning: Column 'Win_Strategy_CreatedDate' ignored because it does not exist in the Opportunity object.
15:27:07: 2 rows read from SQL Table.
15:27:07: 2 rows successfully processed.
15:27:07: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


2/28 -- 60 secs

--- Starting SF_BulkOps for Opportunity_Tritech_Load_3rd V3.6.7
20:35:53: Run the DBAmp.exe program.
20:35:53: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
20:35:53: Updating Salesforce using Opportunity_Tritech_Load_3rd (SQL01 / Staging_SB_MapleRoots) .
20:35:54: DBAmp is using the SQL Native Client.
20:35:54: SOAP Headers: 
20:35:55: Warning: Column 'RankRank' ignored because it does not exist in the Opportunity object.
20:35:55: Warning: Column 'Win_Strategy_Id_Orig' ignored because it does not exist in the Opportunity object.
20:35:55: Warning: Column 'Opty_Id_Orig' ignored because it does not exist in the Opportunity object.
20:35:55: Warning: Column 'Win_Strategy_CreatedDate' ignored because it does not exist in the Opportunity object.
20:35:55: 2 rows read from SQL Table.
20:35:55: 2 rows successfully processed.
20:35:55: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

******************************************************************************************************************************************************/

USE Superion_MAPLESB

EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS',Opportunity, Yes


/**************** VALIDATION AND ERROR CHECKS *******************************/

select 
ID,
Error,
Customer_s_Executive_Priorities__c,
len(cast(Customers_Competitive_Alternative__c as nvarchar(max))) Customers_Competitive_Alternative__c,
len(cast(Customer_s_Defined_Initiatives__c as nvarchar(max))) Customer_s_Defined_Initiatives__c,
len(cast(Customer_s_Executive_Priorities__c as nvarchar(max))) Customer_s_Executive_Priorities__c,
len(cast(What_are_your_Vulnerabilities__c as nvarchar(max))) What_are_your_Vulnerabilities__c,
len(cast(Deal_Strategy__c as nvarchar(max))) Deal_Strategy__c,
len(cast(What_are_your_Differentiators__c as nvarchar(max))) What_are_your_Differentiators__c
--INTO Staging_SB.dbo.Opportunity_Tritech_Load_1st_Error
FROM Staging_SB.dbo.Opportunity_Tritech_Load_3rd

ID					Error	Customers_Competitive_Alternative__c	Customer_s_Defined_Initiatives__c	Customer_s_Executive_Priorities__c	What_are_your_Vulnerabilities__c	Deal_Strategy__c	What_are_your_Differentiators__c
0060v000004Wqv9AAC	NULL	3513									11729								714									114									4603				727

select 
ID,
Customer_s_Executive_Priorities__c,
len(cast(Customers_Competitive_Alternative__c as nvarchar(max))) Customers_Competitive_Alternative__c,
len(cast(Customer_s_Defined_Initiatives__c as nvarchar(max))) Customer_s_Defined_Initiatives__c,
len(cast(Customer_s_Executive_Priorities__c as nvarchar(max))) Customer_s_Executive_Priorities__c,
len(cast(What_are_your_Vulnerabilities__c as nvarchar(max))) What_are_your_Vulnerabilities__c,
len(cast(Deal_Strategy__c as nvarchar(max))) Deal_Strategy__c,
len(cast(What_are_your_Differentiators__c as nvarchar(max))) What_are_your_Differentiators__c
--INTO Staging_SB.dbo.Opportunity_Tritech_Load_1st_Error
FROM MC_SUPERION_FULLSB...Opportunity
where Id = '0060v000004Wqv9AAC'

Before:
ID					Customers_Competitive_Alternative__c	Customer_s_Defined_Initiatives__c	Customer_s_Executive_Priorities__c	What_are_your_Vulnerabilities__c	Deal_Strategy__c	What_are_your_Differentiators__c
0060v000004Wqv9AAC	NULL									7667								433									46									2448				363

After:
ID					Customers_Competitive_Alternative__c	Customer_s_Defined_Initiatives__c	Customer_s_Executive_Priorities__c	What_are_your_Vulnerabilities__c	Deal_Strategy__c	What_are_your_Differentiators__c
0060v000004Wqv9AAC	3513									11729								713									114									4603				727


/************************ HOTFIX for What_are_your_Vulnerabilities__c - 12/15/2018 **********************************/

select Id, count(*) from  
(select Id, error, What_are_your_Vulnerabilities__c
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_1st
UNION
select Id, error, What_are_your_Vulnerabilities__c
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_2nd
UNION
select Id, error, What_are_your_Vulnerabilities__c
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_3rd
) A
group by Id
having count(*) > 1

select Id, What_are_your_Vulnerabilities__c, LoadSequence from  
(select Id, error, What_are_your_Vulnerabilities__c, '1st' LoadSequence
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_1st
UNION
select Id, error, What_are_your_Vulnerabilities__c, '2nd'
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_2nd
UNION
select Id, error, What_are_your_Vulnerabilities__c, '3rd'
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_3rd
) A
where Id in (
'0060v000004jOIJAA2',
'0060v000004jP3aAAE',
'0060v000004jP4YAAU',
'0060v000004jPfzAAE',
'0060v000004jPmKAAU',
'0060v000004jPY8AAM',
'0060v000004jRzyAAE')

WITH
cte_sfdc_Opty AS
(	
	SELECT
		Id AS OptyId
		, Customers_Competitive_Alternative__c AS ccAlternative
		, Customer_s_Defined_Initiatives__c AS cdInitiatives
		, Customer_s_Executive_Priorities__c AS cePriorities
		, What_are_your_Vulnerabilities__c AS wayVulnerabilities
		, Deal_Strategy__c AS dStrategy
		, What_are_your_Differentiators__c AS wayDifferentiators
		, Legacy_Opportunity_ID__c AS legacyOptyId
	FROM Superion_FULLSB.dbo.Opportunity
)

select 
sWS1.TriTech_Weaknesses__c,
tOPTY1.wayVulnerabilities,
tOPTY1.OptyId AS ID,
CAST(NULL AS NVARCHAR(255)) AS Error,
IIF (tOPTY1.wayVulnerabilities IS NOT NULL OR sWS1.TriTech_Weaknesses__c IS NOT NULL ,
		CONCAT(IIF(tOPTY1.wayVulnerabilities IS NOT NULL, CONCAT(tOPTY1.wayVulnerabilities,CHAR(10)),NULL),
				IIF(sWS1.TriTech_Weaknesses__c IS NOT NULL, CONCAT(CAST(sWS1.CreatedDate AS NVARCHAR(10)), ': ', sWS1.TriTech_Weaknesses__c),NULL))
		, NULL)	AS What_are_your_Vulnerabilities__c
INTO Staging_SB.dbo.Opportunity_Tritech_Hotfix_What_are_your_Vulnerabilities__c
from 
Tritech_PROD.dbo.Win_Strategy__c sWS1
LEFT JOIN cte_sfdc_Opty tOPTY1 ON sWS1.Opportunity__c = tOPTY1.legacyOptyId
--where sWS1.Opportunity__c = '0068000001ATOzZAAX'

select * INTO Staging_SB.dbo.Opportunity_Tritech_Hotfix_wayVulnerabilities
from Staging_SB.dbo.Opportunity_Tritech_Hotfix_What_are_your_Vulnerabilities__c

USE Staging_SB

EXEC SF_ColCompare 'Update','MC_SUPERION_FULLSB', 'Opportunity_Tritech_Hotfix_wayVulnerabilities' 

EXEC SF_BulkOps 'Update','MC_SUPERION_FULLSB', 'Opportunity_Tritech_Hotfix_wayVulnerabilities'

----------------------------------------------
select Id, Customer_s_Executive_Priorities__c from Staging_SB.dbo.Opportunity_Tritech_PreLoad_1st

select Id, error, NULL as Customer_s_Executive_Priorities__c INTO Staging_SB.dbo.Opportunity_Tritech_PreLoad_1st_Null_tExecutivePriorities
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_1st

select * from Staging_SB.dbo.Opportunity_Tritech_PreLoad_1st_Null_tExecutivePriorities

select Id, error, NULL as Customer_s_Executive_Priorities__c INTO Staging_SB.dbo.Opportunity_Tritech_PreLoad_2nd_Null_tExecutivePriorities
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_2nd

select Id, error, NULL as Customer_s_Executive_Priorities__c INTO Staging_SB.dbo.Opportunity_Tritech_PreLoad_3rd_Null_tExecutivePriorities
from Staging_SB.dbo.Opportunity_Tritech_PreLoad_3rd

EXEC SF_BulkOps 'Update','MC_SUPERION_FULLSB', 'Opportunity_Tritech_PreLoad_1st_Null_tExecutivePriorities'
EXEC SF_BulkOps 'Update','MC_SUPERION_FULLSB', 'Opportunity_Tritech_PreLoad_2nd_Null_tExecutivePriorities'
EXEC SF_BulkOps 'Update','MC_SUPERION_FULLSB', 'Opportunity_Tritech_PreLoad_3rd_Null_tExecutivePriorities'

drop table Staging_SB.dbo.Opportunity_Tritech_PreLoad_3rd_Null_tExecutivePriorities

0060v000004lMQLAA2

select * from Opportunity_Tritech_PreLoad_3rd_Null_tExecutivePriorities

select * from Staging_SB.dbo.Opportunity_Tritech_PreLoad_3rd_HF_tcePriorities