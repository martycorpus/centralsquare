 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Event__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'MC_Tritech_PROD','BGIntegration__Event__c'

Use SUPERION_MAPLESB
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','BGIntegration__Event__c','Yes'
*/ 

--2018-12-06 Marty Executed for UAT2 LOAD.
 --Exec SF_ColCompare 'Insert','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_Load' 
 --- Starting SF_ColCompare V3.6.9
--Problems found with BGIntegration__Event__c_Tritech_SFDC_Load. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 25]
--- Ending SF_ColCompare. Operation FAILED.
--ErrorDesc
--Salesforce object BGIntegration__Event__c does not contain column BGIntegration__BomgarSession__c_orig
--Salesforce object BGIntegration__Event__c does not contain column CreatedById_orig

--select count(*) from BGIntegration__Event__c_Tritech_SFDC_Load --2,457,573

--Exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_Load'
--bulkapi job id = 7500v000003kEtt

--00:41:05: Job still running.
--00:42:05: Job Complete.
--00:42:05: Retrieving Job Status.
--01:22:39: 2457573 rows read from SQL Table.
--01:22:39: 2457573 rows successfully processed.
--01:22:39: 0 rows failed.
--01:22:39: 2457573 rows marked with current status.
--01:22:50: Percent Failed = 0.000.
----- Ending SF_BulkOps. Operation successful. 02:33:39

Use Staging_SB_MapleRoots;

--Drop table BGIntegration__Event__c_Tritech_SFDC_Preload;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_MAPLESB.dbo.[User] 
where Name like 'Bomgar Site Guest User');

Select

ID = CAST(SPACE(18) as NVARCHAR(18)),
ERROR = CAST(SPACE(255) as NVARCHAR(255)),
Legacy_id__c =bg.Id, 
Legacy_Source_System__c='Tritech',
Migrated_Record__c='True',

BGIntegration__Body__c                    =  bg.BGIntegration__Body__c,

BGIntegration__BomgarSession__c_orig      =  bg.BGIntegration__BomgarSession__c,
BGIntegration__BomgarSession__c           =  bs.Id,

BGIntegration__Destination__c             =  bg.BGIntegration__Destination__c,
BGIntegration__Destination_GS_Number__c   =  bg.BGIntegration__Destination_GS_Number__c,
BGIntegration__Destination_Type__c        =  bg.BGIntegration__Destination_Type__c,
BGIntegration__File_Name__c               =  bg.BGIntegration__File_Name__c,
BGIntegration__File_Size__c               =  bg.BGIntegration__File_Size__c,
BGIntegration__Number__c                  =  bg.BGIntegration__Number__c,
BGIntegration__Performed_By__c            =  bg.BGIntegration__Performed_By__c,
BGIntegration__Performed_By_GS_Number__c  =  bg.BGIntegration__Performed_By_GS_Number__c,
BGIntegration__Performed_By_Type__c       =  bg.BGIntegration__Performed_By_Type__c,
BGIntegration__Timestamp__c               =  bg.BGIntegration__Timestamp__c,

CreatedById_orig                          =  bg.CreatedById,
CreatedById                               =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id),

CreatedDate                               =  bg.CreatedDate,
--Legacy_ID__c                              =  bg.Id,
Name                                      =  bg.[Name]

into BGIntegration__Event__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__Event__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_MAPLESB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_MAPLESB.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

----For Delta Load
--left join Superion_FULLSB.dbo.BGIntegration__Event__c trgt on
--trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

; --(2979134 row(s) affected)

---------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Event__c;--2979134

Select count(*) from BGIntegration__Event__c_Tritech_SFDC_Preload;--2979134

Select Legacy_Id__c,count(*) from Staging_SB_MapleRoots.dbo.BGIntegration__Event__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB_MapleRoots.dbo.BGIntegration__Event__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__Event__c_Tritech_SFDC_Load
from BGIntegration__Event__c_Tritech_SFDC_Preload; --(2979134 row(s) affected)

Select * from Staging_SB_MapleRoots.dbo.BGIntegration__Event__c_Tritech_SFDC_Load 
where  BGIntegration__BomgarSession__c IS NULL;


--Exec SF_ColCompare 'Insert','SL_SUPERION_MAPLESB', 'BGIntegration__Event__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__Event__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Event__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event__c_Tritech_SFDC_Load' (3:33:53)


select *, ntile(3) over(order by Legacy_id__c) series
into BGIntegration__Event__c_Tritech_SFDC_Load_with_Series
from BGIntegration__Event__c_Tritech_SFDC_Load a;--2979134

--Series1
select * into BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_1 from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series
where series=1;--993045

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_1'

--Series2
select * into BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_2 from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series
where series=2;--993045

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_2'

--Series3
select * into BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_3 from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series
where series=3;--993044

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_3'

--Drop table BGIntegration__Event__c_Tritech_SFDC_Load_with_Series;

--Select * into BGIntegration__Event__c_Tritech_SFDC_Load_with_Series
 from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_1;

--Insert into BGIntegration__Event__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_2;

--Insert into BGIntegration__Event__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series_3;

Select count(*) from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series;

--Drop table BGIntegration__Event__c_Tritech_SFDC_Load;

--Select * into BGIntegration__Event__c_Tritech_SFDC_Load
 from BGIntegration__Event__c_Tritech_SFDC_Load_with_Series;

 Select count(*) from BGIntegration__Event__c_Tritech_SFDC_Load;

----------------------------------------------------------------------------------------
--Updating Legacy_Source_System__c,Migrated_Record__c

--Use Staging_SB_MapleRoots;

--Drop table BGIntegration__Event__c_Tritech_SFDC_LoadUpdate

Select 

 Id =bg.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Legacy_id__c
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

into BGIntegration__Event__c_Tritech_SFDC_LoadUpdate
from Staging_SB_MapleRoots.dbo.BGIntegration__Event__c_Tritech_SFDC_Load bg
;--(2284968 row(s) affected)

 --Exec SF_ColCompare 'Update','SL_SUPERION_MAPLESB','BGIntegration__Event__c_Tritech_SFDC_LoadUpdate' 

/*
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_MAPLESB','BGIntegration__Event__c_Tritech_SFDC_LoadUpdate' 

------------------------------------------------------------------------------------------------

Select *
--delete 
from BGIntegration__Event__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --

--insert into BGIntegration__Event__c_Tritech_SFDC_Load
select * from BGIntegration__Event__c_Tritech_SFDC_Load_Delta; 