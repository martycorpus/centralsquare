/*****************************************************************************************************************************************************
REQ #		: REQ-0831
TITLE		: ChatterGroup (CollaborationGroup) Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.2 Chatter Group Members Migration PB.sql
03/01/2019		Ron Tecson			Modified for the MapleRoots Data Load
03/19/2019		Ron Tecson			Loaded MapleRoots and recorded the errors.

DECISIONS:

ERROR/FAILURES:

DATE			ERROR													RESOLUTION
=============== ======================================================= ========================================================================================
03/18/2019		'Error - INACTIVE_OWNER_OR_USER:operation performed		3 Records. 1 Member is inactive in MapleRoots. - Martin James.
						with inactive user [0056A000000m8CI]:--'			
				'Error - REQUIRED_FIELD_MISSING:Required fields are		48 Records. --> 22 Distinct Members/Users don't exists in MapleRoots because they are 
						missing: [MemberId]:MemberId --'					inactive in Tritech and didn't get migrated.

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------
Use Tritech_Prod
Exec SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';
Exec SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroupMember', 'yes';
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes';

Use SUPERION_MAPLESB
Exec SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'User', 'yes';
Exec SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'CollaborationGroup', 'yes';

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroupMember_PreLoad_RT') 
DROP TABLE Staging_SB_MapleRoots.dbo.CollaborationGroupMember_PreLoad_RT;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------

USE Staging_SB_MapleRoots;

SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	Group_Source.[Name] as CollaborationGroupName_Orig,
	[Group_Target].Id as CollaborationGroupId,
	a.MemberID as MemberID_Original,
	[User].ID as MemberId,
	a.CollaborationRole as CollaborationRole,
	a.NotificationFrequency as NotificationFrequency
INTO Staging_SB_MapleRoots.dbo.CollaborationGroupMember_PreLoad_RT
FROM Tritech_Prod.dbo.CollaborationGroupMember a
LEFT OUTER JOIN SUPERION_MAPLESB.dbo.[User] [User] ON a.MemberID = [User].Legacy_Tritech_Id__c							----> MemberID
LEFT OUTER JOIN Tritech_PROD.dbo.[CollaborationGroup] [Group_Source] ON a.CollaborationGroupId = [Group_Source].[ID]	----> Chatter Group
INNER JOIN SUPERION_MAPLESB.dbo.[CollaborationGroup] [Group_Target] ON Group_Source.[Name] = [Group_Target].[Name]

-- 03/01 -- (1130 rows affected)
-- 03/19 -- (1160 rows affected)

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroupMember_Load_RT') 
DROP TABLE Staging_SB_MapleRoots.dbo.CollaborationGroupMember_Load_RT;

SELECT *
INTO Staging_SB_MapleRoots.dbo.CollaborationGroupMember_Load_RT
FROM Staging_SB_MapleRoots.dbo.CollaborationGroupMember_PreLoad_RT

-- 03/01 -- (1130 rows affected)
-- 03/19 -- (1160 rows affected)


---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table CollaborationGroupMember_Load_RT
Add [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

EXEC SF_ColCompare 'insert','RT_SUPERION_MAPLEROOTS', 'CollaborationGroupMember_Load_RT' 

/*****************************************************************************************************************************************************

--- Starting SF_ColCompare V3.6.7
Problems found with CollaborationGroupMember_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 86]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object CollaborationGroupMember does not contain column CollaborationGroupName_Orig
Salesforce object CollaborationGroupMember does not contain column MemberID_Original
Salesforce object CollaborationGroupMember does not contain column Sort

*****************************************************************************************************************************************************/


EXEC SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'RT_SUPERION_MAPLEROOTS', 'CollaborationGroupMember_Load_RT'

/*****************************************************************************************************************************************************
-- 3/19
--- Starting SF_BulkOps for CollaborationGroupMember_Load_RT V3.6.7
12:54:58: Run the DBAmp.exe program.
12:54:58: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
12:54:58: Inserting CollaborationGroupMember_Load_RT (SQL01 / Staging_SB_MapleRoots).
12:54:58: DBAmp is using the SQL Native Client.
12:54:58: Batch size reset to 4000 rows per batch.
12:55:00: Sort column will be used to order input rows.
12:55:00: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
12:55:01: Warning: Column 'CollaborationGroupName_Orig' ignored because it does not exist in the CollaborationGroupMember object.
12:55:01: Warning: Column 'MemberID_Original' ignored because it does not exist in the CollaborationGroupMember object.
12:55:01: Job 7500r000000033aAAA created.
12:55:01: Batch 7510r00000005xVAAQ created with 1160 rows.
12:55:02: Job submitted.
12:55:02: 1160 rows read from SQL Table.
12:55:02: Job still running.
12:55:18: Job still running.
12:56:19: Job Complete.
12:56:19: DBAmp is using the SQL Native Client.
12:56:19: 1109 rows successfully processed.
12:56:19: 51 rows failed.
12:56:19: Errors occurred. See Error column of row and above messages for more information.
12:56:19: Percent Failed = 4.400.
12:56:19: Error: DBAmp.exe was unsuccessful.
12:56:19: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:bulkapi,batchsize(4000) CollaborationGroupMember_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_SUPERION_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 104]
SF_BulkOps Error: 12:54:58: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC12:54:58: Inserting CollaborationGroupMember_Load_RT (SQL01 / Staging_SB_MapleRoots).12:54:58: DBAmp is using the SQL Native Client.12:54:58: Batch size reset to 4000 rows per batch.12:55:00: Sort column will be used to order input rows.12:55:00: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.12:55:01: Warning: Column 'CollaborationGroupName_Orig' ignored because it does not exist in the CollaborationGroupMember object.12:55:01: Warning: Column 'MemberID_Original' ignored because it does not exist in the CollaborationGroupMember object.12:55:01: Job 7500r000000033aAAA created.12:55:01: Batch 7510r00000005xVAAQ created with 1160 rows.12:55:02: Job submitted.12:55:02: 1160 rows read from SQL Table.12:55:02: Job still running.12:55:18: Job still running.12:56:19: Job Complete.12:56:19: DBAmp is using the SQL Native Client.12:56:19: 1109 rows successfully processed.12:56:19: 51 rows failed.12:56:19: Errors occurred. See Error column of row and above messages for more information.

-- 3/1
--- Starting SF_BulkOps for CollaborationGroupMember_Load_RT V3.6.7
05:31:23: Run the DBAmp.exe program.
05:31:23: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
05:31:23: Inserting CollaborationGroupMember_Load_RT (SQL01 / Staging_SB_MapleRoots).
05:31:24: DBAmp is using the SQL Native Client.
05:31:24: Batch size reset to 4000 rows per batch.
05:31:25: Sort column will be used to order input rows.
05:31:25: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
05:31:25: Warning: Column 'CollaborationGroupName_Orig' ignored because it does not exist in the CollaborationGroupMember object.
05:31:25: Warning: Column 'MemberID_Original' ignored because it does not exist in the CollaborationGroupMember object.
05:31:26: Job 7501h000001xCKKAA2 created.
05:31:26: Batch 7511h000001vKKTAA2 created with 1130 rows.
05:31:26: Job submitted.
05:31:26: 1130 rows read from SQL Table.
05:31:27: Job still running.
05:31:42: Job still running.
05:32:43: Job Complete.
05:32:44: DBAmp is using the SQL Native Client.
05:32:45: 1103 rows successfully processed.
05:32:45: 27 rows failed.
05:32:45: Errors occurred. See Error column of row and above messages for more information.
05:32:45: Percent Failed = 2.400.
05:32:45: Error: DBAmp.exe was unsuccessful.
05:32:45: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:bulkapi,batchsize(4000) CollaborationGroupMember_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_SUPERION_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 86]
SF_BulkOps Error: 05:31:23: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC05:31:23: Inserting CollaborationGroupMember_Load_RT (SQL01 / Staging_SB_MapleRoots).05:31:24: DBAmp is using the SQL Native Client.05:31:24: Batch size reset to 4000 rows per batch.05:31:25: Sort column will be used to order input rows.05:31:25: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.05:31:25: Warning: Column 'CollaborationGroupName_Orig' ignored because it does not exist in the CollaborationGroupMember object.05:31:25: Warning: Column 'MemberID_Original' ignored because it does not exist in the CollaborationGroupMember object.05:31:26: Job 7501h000001xCKKAA2 created.05:31:26: Batch 7511h000001vKKTAA2 created with 1130 rows.05:31:26: Job submitted.05:31:26: 1130 rows read from SQL Table.05:31:27: Job still running.05:31:42: Job still running.05:32:43: Job Complete.05:32:44: DBAmp is using the SQL Native Client.05:32:45: 1103 rows successfully processed.05:32:45: 27 rows failed.05:32:45: Errors occurred. See Error column of row and above messages for more information.


*****************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

select ERROR, count(*)
from CollaborationGroupMember_Load_RT
group by ERROR

select * from CollaborationGroupMember_Load_RT where error not like '%success%' and error like '%memberid%'

select * from CollaborationGroupMember_Load_RT where error not like '%success%' and error not like '%memberid%'

select * from CollaborationGroupMember_Load_RT where error like '%success%'

select distinct MemberId_Original from CollaborationGroupMember_Load_RT where error not like '%success%' and error like '%memberid%'

select 
from
Staging_SB_MapleRoots.dbo.CollaborationGroupMember_Load_RT stg1
inner join Tritech_PROD.dbo.[user] src1 ON stg

---------------------------------------------------------------------------------------------------------------
-- Refresh Local Databsae
---------------------------------------------------------------------------------------------------------------

USE SUPERION_MAPLESB

Exec SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'CollaborationGroupMember', 'yes';


---------------------------------------------------------------------------------------------------------------

--drop table USER_Update_inActiveChatterGroupMembers_PB

--Select Distinct
--a.MemberID as ID,
--CAST('' as nvarchar(255)) as error,
--'true' as isactive
--INTO USER_Update_inActiveChatterGroupMembers_PB 
--From CollaborationGroupMember_load_PB a
--WHERE error like '%inactive_owner_or_user%'

--use Superion_FULLSB
--Exec SF_BulkOps 'update:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'USER_Update_inActiveChatterGroupMembers_PB'

--select * from USER_Update_inActiveChatterGroupMembers_PB

--select * from staging_sb.dbo.User_Update_toinActive_PB where error like '%successful%'

--use Staging_SB
--Exec SF_BulkOps 'update:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'User_Update_toinActive_PB'

--Select * INTO CollaborationGroupMember_load_PB_backup
--from CollaborationGroupMember_load_PB


--delete from Superion_FULLSB.dbo.CollaborationGroupMember_load_PB where error like '%success%'

--update USER_Update_inActiveChatterGroupMembers_PB set isActive = '0'