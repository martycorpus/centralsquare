/*****************************************************************************************************************************************************
REQ #		: REQ-0847
DEVELOPER	: RTECSON
CREATED DT  : 11/26/2018
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
11/26/2018		Ron Tecson			Initial
01/03/2019		Ron Tecson			Added the counts per query for the E2E Testing.
02/28/2019		Ron Tecson			Modified for the MapleRoots Data Load.
03/21/2019		Ron Tecson			Loaded data for Mapleroots.

DECISIONS:

PRE-REQUISITES:

1.) Deactivate the Process Builder - Case - Master and Re-activate once the Migration Process is complete.

******************************************************************************************************************************************************/

USE SUPERION_MAPLESB

	EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS',[Case], subset

	EXEC sf_replicate 'RT_SUPERION_MAPLEROOTS','Case'				-- 70 mins

USE Tritech_PROD

	EXEC SF_Refresh 'MC_TRITECH_PROD',Time_Card_WMP__c, Yes 

	select * from Tritech_PROD.dbo.Time_Card_WMP__c 
	--9912 12/5
	--9987 3/21

USE Staging_SB_MapleRoots

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'Case_Tritech_PreLoad' AND
            table_schema = 'dbo' )
  DROP TABLE [Staging_SB_MapleRoots].[dbo].[Case_Tritech_PreLoad]; 

SELECT
	IIF(tC1.Id IS NOT NULL, tC1.Id, 'Missing Case Id') AS ID
	, CAST(NULL AS NVARCHAR(255)) AS Error
	--, STUFF('[' + sTC1.Id + '] ' XML PATH('')), 1, 1, '')
	, sTC1.ticket_WMP__c AS CaseId_Orig
	, SUM(sTC1.Ticket_Duration_WMP__c) as Work_Effort_In_Minutes__c
INTO Staging_SB_MapleRoots.[dbo].[Case_Tritech_PreLoad]
FROM Tritech_PROD.dbo.Time_Card_WMP__c sTC1
LEFT JOIN SUPERION_MAPLESB.dbo.[case] tC1 on sTC1.ticket_WMP__c = tC1.legacy_ID__c
GROUP BY tC1.Id, sTC1.ticket_WMP__c
ORDER BY 3  

-- (5060 rows affected) -- 03/21/2019
-- (5060 rows affected) -- 02/28/2019
-- (5047 rows affected) 01/03/2019
-- 5014 Row 12/5


select count(stc1.id)
FROM Tritech_PROD.dbo.Time_Card_WMP__c sTC1
LEFT JOIN SUPERION_MAPLESB.dbo.[case] tC1 on sTC1.ticket_WMP__c = tC1.legacy_ID__c 
-- 9987 03/21/2019
-- 9987 02/28/2019
-- 9964 01/03/2019
-- 9912 12/4

select * from Staging_SB_MapleRoots.[dbo].[Case_Tritech_PreLoad] where ID <> 'Missing Case Id' order by 4 desc
--CaseId_Orig -- 475 03/20/2019
--CaseId_Orig -- 462 01/03/2019
--CaseId_Orig -- 836

select CaseId_Orig, count(*) from Staging_SB_MapleRoots.[dbo].[Case_Tritech_PreLoad]
group by CaseId_Orig having count(*) > 1

select count(*) from Staging_SB_MapleRoots.[dbo].[Case_Tritech_PreLoad] where ID <> 'Missing Case Id' 
-- 475 03/21/2019
-- 462 01/03/2019
--427

select legacy_id__c, count(*) from Superion_FULLSB.dbo.[Case]
group by legacy_id__c
having count(*) > 1

select * from Superion_FULLSB.dbo.[Case] where legacy_id__c = '5008000000stvqAAAQ'

select * from Tritech_PROD.dbo.[case] where id = '5008000000stvqAAAQ'

select * from Tritech_PROD.dbo.Time_Card_WMP__c where ticket_WMP__c = '5008000000stvqAAAQ'

select 322 + 2880 + 319 + 319 + 66 -- 3906

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'Case_Tritech_Load' AND
            table_schema = 'dbo' )
  DROP TABLE Staging_SB_MapleRoots.[dbo].[Case_Tritech_Load]; 

SELECT * 
INTO Staging_SB_MapleRoots.[dbo].[Case_Tritech_Load]
FROM Staging_SB_MapleRoots.[dbo].[Case_Tritech_PreLoad]
WHERE ID <> 'Missing Case Id'
-- (462 rows affected) -- 01/03/2019 
-- (475 rows affected) -- 02/28/2019
-- (475 rows affected) -- 03/21/2019


EXEC SF_ColCompare 'Update','RT_SUPERION_MAPLEROOTS', 'Case_Tritech_Load' 

/*****************************************************************************************************************************************************

--- Starting SF_ColCompare V3.6.7
Problems found with Case_Tritech_Load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 100]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Case does not contain column CaseId_Orig

*****************************************************************************************************************************************************/

EXEC SF_BulkOps 'Update','RT_SUPERION_MAPLEROOTS', 'Case_Tritech_Load'

/*****************************************************************************************************************************************************

03/21/2019:
--- Starting SF_BulkOps for Case_Tritech_Load V3.6.7
10:52:31: Run the DBAmp.exe program.
10:52:31: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
10:52:31: Updating Salesforce using Case_Tritech_Load (SQL01 / Staging_SB_MapleRoots) .
10:52:31: DBAmp is using the SQL Native Client.
10:52:32: SOAP Headers: 
10:52:32: Warning: Column 'CaseId_Orig' ignored because it does not exist in the Case object.
10:52:53: 475 rows read from SQL Table.
10:52:53: 475 rows successfully processed.
10:52:53: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

02/28/2019:
--- Starting SF_BulkOps for Case_Tritech_Load V3.6.7
00:10:34: Run the DBAmp.exe program.
00:10:34: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
00:10:34: Updating Salesforce using Case_Tritech_Load (SQL01 / Staging_SB_MapleRoots) .
00:10:35: DBAmp is using the SQL Native Client.
00:10:35: SOAP Headers: 
00:10:35: Warning: Column 'CaseId_Orig' ignored because it does not exist in the Case object.
00:10:52: 475 rows read from SQL Table.
00:10:52: 475 rows successfully processed.
00:10:52: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

*****************************************************************************************************************************************************/

SELECT ERROR, count(*) Row_Count FROM [Staging_SB_MapleRoots].[dbo].[Case_Tritech_Load]
GROUP BY ERROR 

USE Superion_MapleSB

	EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS',[Case], Yes

/**************** VALIDATION AND ERROR CHECKS *******************************/

BEFORE:

select Work_Effort_In_Minutes__c from MC_SUPERION_FULLSB...[Case] where Id in (
'5000v000003GCy9AAG',
'5000v000003GJUhAAO',
'5000v000003GI2QAAW',
'5000v000003EYmQAAW',
'5000v000003GKTtAAO')

Work_Effort_In_Minutes__c
0
0
0
0
0

-- SELECT STUFF((SELECT '[' + sTC1.Id + '] ' FROM Tritech_PROD.dbo.Time_Card_WMP__c sTC1  FOR XML PATH('')), 1, 1, '');