 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Sales_Request__c----------
Requirement No  :
Total Records   :  
Scope: Migrate all records that relate to a migrated opportunity.
       Merge two existing recordtypes into one, as per the migration mapping doc.
 */
  /*
Use Tritech_PROD
EXEC SF_Replicate 'MC_Tritech_PROD','Pricing_Request_Form__c'


Use SUPERION_MAPLESB
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Sales_Request__c','Yes'
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','RecordType'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Contact','Yes'
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','User'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Opportunity','Yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Account','Yes'
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','Procurement_Activity__c'
*/ 

declare @recordtypeid NVARCHAR(18) = (Select top 1 Id from SUPERION_MAPLESB.dbo.recordtype 
where name ='Pricing Request' and SobjectType='Sales_Request__c');

declare @defaultuser nvarchar(18)=(select top 1 id from SUPERION_MAPLESB.dbo.[User] 
                                 where name ='Superion API' )
DECLARE @GeneralRequest NVARCHAR(18) = 
(Select top 1 Id from SUPERION_MAPLESB.dbo.recordtype where name ='General Request');

Declare @TriTechPricingRequestForm NVARCHAR(18)=
(Select top 1 Id from Tritech_PROD.dbo.RecordType where Name='TriTech Pricing Request Form' and SobjectType='Pricing_Request_Form__c');

Declare @ZuercherProposalRequestForm NVARCHAR(18)=
(Select top 1 Id from Tritech_PROD.dbo.RecordType where Name='Zuercher Proposal Request Form' and SobjectType='Pricing_Request_Form__c');

Use Staging_SB_MapleRoots;

 --Drop table Sales_Request__c_Tritech_SFDC_PricingRequestForm_Preload

 Select
 
 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Record_ID__c = pricing.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

--,Account__c_orig=pricing.Account__c
--,Account__c=accnt.Id

,Z_Assigned_To__c_orig=Z_Assigned_To__c
,Z_Assigned_To__c_SFID=zassgndusr.Id

,Assigned_CSL__c_orig=pricing.Assigned_CSL__c
,Assigned_CSL__c_SFID=assgndusr.Id

,Assigned_Pricing_Manager__c_orig=pricing.Assigned_Pricing_Manager__c
,Assigned_Pricing_Manager__c_SFID=prcngmngruser.Id

,Assigned_Proposal_Manager__c_orig=pricing.Assigned_Proposal_Manager__c
,Assigned_Proposal_Manager__c_SFID=prpslmngruser.Id

--,Assigned_To__c=IIF(zassgndusr.Id IS NULL,assgndusr.Id,zassgndusr.Id)

,CASE
 WHEN Z_Assigned_To__c IS NOT NULL THEN zassgndusr.Id
 WHEN Assigned_Pricing_Manager__c IS NOT NULL THEN prcngmngruser.Id
 WHEN Assigned_CSL__c IS NOT NULL THEN assgndusr.Id
 ELSE prpslmngruser.Id
 END as Assigned_To__c

,Completed_By__c_orig=Completed_By__c
,Completed_By__c=cmpltdby.Id

,Contact__c_orig=pricing.Contact__c
,Contact__c=cntct.ID

,CreatedById_orig=pricing.CreatedById
,CreatedById=IIF(crtdbyiduser.Id IS NULL,@defaultuser,crtdbyiduser.Id)


,CreatedDate=pricing.CreatedDate
,Date_Completed__c=Date_Completed__c
,Date_of_Request__c=Date_of_Request__c
--,Days_from_Created_to_Complete__c=Days_from_Created_to_Complete__c
,How_is_request_submitted__c=How_is_request_submitted__c
--,Id=Id
--,Pricing_Request_Form__c=pricing.IsDeleted
,Large_Quote_Projected_Value_250K__c=Large_Quote_Projected_Value_250K__c
--,Pricing_Request_Form__c=pricing.LastActivityDate

,LastModifiedById_orig=pricing.LastModifiedById
,LastModifiedById=lstmdfdbyusr.Id

,LastModifiedDate=pricing.LastModifiedDate
--,Pricing_Request_Form__c=pricing.LastReferencedDate
--,Pricing_Request_Form__c=pricing.LastViewedDate
--,Migrating_From__c=pricing.Migrating_From__c
,Legacy_Request_Name__c=pricing.Name
,Needed_by_Date__c=Needed_by_Date__c

,Opportunity__c_orig=pricing.Opportunity__c
,Opportunity__c=oppty.Id

,Z_Other_Instructions__c_orig=Z_Other_Instructions__c
,Other_Details_Special_Instructions__c_orig=pricing.Other_Details_Special_Instructions__c

,Other_Details_Special_Instructions__c=CONCAT
(IIF(pricing.Other_Details_Special_Instructions__c IS NULL,NULL,'Other Details/Special Instructions:'+CAST(pricing.Other_Details_Special_Instructions__c as varchar(max))),CHAR(13)+CHAR(10),
IIF(Z_Other_Instructions__c IS NULL,NULL,'Other Instructions :'+CAST(Z_Other_Instructions__c as varchar(max))))

,Requestor__c_orig=pricing.OwnerId
,Requestor__c= IIF(owneruser.Id IS NULL,@defaultuser,owneruser.Id)

,Proactive_Proposal_Materials_Required__c=Proactive_Proposal_Materials_Required__c

,Procurement_Activity__c_orig=Procurement_Activity_del__c
,Procurement_Activity__c=procurement.Id

--,Product_Family__c=Product_Family__c
--,Product_Migration__c=Product_Migration__c
--,Quick_Summary_Basic_Quote__c=Quick_Summary_Basic_Quote__c
,QUOTE_COMPLETE__c=QUOTE_COMPLETE__c
--,Quote_Completed_By__c=Quote_Completed_By__c
,Quote_Type__c=Quote_Type__c
,Rapid_Implementation__c_orig=Rapid_Implementation__c
,CASE
 WHEN Rapid_Implementation__c='Yes' THEN 'True'
 WHEN Rapid_Implementation__c='No'  THEN 'False'
 END as Rapid_Implementation__c

,RecordTypeId_orig=pricing.RecordTypeId
--,RecordTypeId=recordtype.Id
,CASE 
 WHEN pricing.RecordTypeId=@TriTechPricingRequestForm THEN @recordtypeid
 WHEN pricing.RecordTypeId=@ZuercherProposalRequestForm AND Z_Assigned_To__c IS NOT NULL THEN @GeneralRequest
 WHEN pricing.RecordTypeId=@ZuercherProposalRequestForm AND Z_Assigned_To__c IS NULL AND ISNULL(Z_What_is_Needed__c,'X')<>'Pricing' THEN @GeneralRequest
 ELSE @recordtypeid
 END as RecordTypeId

,Rejection_Reason__c=pricing.Rejection_Reason__c
,Request_Status__c_orig=pricing.Request_Status__c
,Z_Status__c_orig=pricing.Z_Status__c
,Status__c_map=st.Status__c
,Status__c=iif(st.Status__c is null,isnull(pricing.Z_Status__c,pricing.Request_Status__c),st.Status__c)
--,Sales_Team__c=Sales_Team__c
,SUBMIT_PRICING_PROPOSAL_REQUEST__c=SUBMIT_PRICING_REQUEST__c
--,Pricing_Request_Form__c=pricing.SystemModstamp
--,Target_Completion_Date__c=Target_Completion_Date__c

,Letter_Information__c=Z_Letter_Information__c
,Shipment_Notes__c=Z_Notes_Comments__c
,References_Required__c=Z_References_Required__c
--,Status__c=Z_Status__c
,Time_Elapsed__c=Z_Time_Elapsed__c
,What_is_Needed__c=Z_What_is_Needed__c
--,Z_When_is_it_Needed__c=Z_When_is_it_Needed__c

 into Sales_Request__c_Tritech_SFDC_PricingRequestForm_Preload

from Tritech_PROD.dbo.Pricing_Request_Form__c pricing 
inner join SUPERION_MAPLESB.dbo.Opportunity oppty
on oppty.Legacy_Opportunity_ID__c=pricing.Opportunity__c

--Fetching Account__c SFID (Account Lookup)
left join SUPERION_MAPLESB.dbo.Account accnt on
accnt.LegacySFDCAccountId__c=pricing.Account__c

--Fetching Assigned_CSL__c (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] assgndusr on
assgndusr.Legacy_Tritech_Id__c=pricing.Assigned_CSL__c

--Fetching Assigned_Pricing_Manager__c (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] prcngmngruser on
prcngmngruser.Legacy_Tritech_Id__c=pricing.Assigned_Pricing_Manager__c

--Fetching Assigned_Proposal_Manager__c (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] prpslmngruser on
prpslmngruser.Legacy_Tritech_Id__c=pricing.Assigned_Proposal_Manager__c

--Fetching Contact__c (Contact lookup)
left join SUPERION_MAPLESB.dbo.Contact cntct on
cntct.Legacy_Id__c=pricing.Contact__c

--Fetching CreatedById (User lookup)
left join SUPERION_MAPLESB.dbo.[User] crtdbyiduser on
crtdbyiduser.Legacy_Tritech_Id__c=pricing.CreatedById

--Fetching OwnerId (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] owneruser on
owneruser.Legacy_Tritech_Id__c=pricing.OwnerId

--Fetching LastModifiedById (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] lstmdfdbyusr on
lstmdfdbyusr.Legacy_Tritech_Id__c=pricing.LastModifiedById

--Fetching CompletedById (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] cmpltdby on
cmpltdby.Legacy_Tritech_Id__c=pricing.Completed_By__c

--Fetching Procurement_Activity_del__c (Procurement Activity lookup)
left join SUPERION_MAPLESB.dbo.Procurement_Activity__c procurement on
procurement.Legacy_Id__c=pricing.Procurement_Activity_del__c

--Fetching Z_Assigned_To__c (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] zassgndusr on
zassgndusr.Legacy_Tritech_Id__c=pricing.Z_Assigned_To__c

--Fetching RecordTypeId (RecordType Lookup)
left join Tritech_PROD.dbo.RecordType rcrdtype
on rcrdtype.Id=pricing.RecordTypeId
left join SUPERION_MAPLESB.dbo.recordtype recordtype
on recordtype.Name=rcrdtype.Name

left join Staging_SB_MapleRoots.dbo.Tritech_PricingRequestForm_Status st
on st.Request_Status__c=pricing.Request_Status__c AND ISNULL(st.Z_Status__c,'X')=ISNULL(pricing.Z_Status__c,'X')
;

--------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.Pricing_Request_Form__c;--10219

Select count(*) from Staging_SB_MapleRoots.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_Preload;--8719

Select Legacy_Record_ID__c,count(*) from Staging_SB_MapleRoots.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_Preload
group by Legacy_Record_ID__c
having count(*)>1;--0

--Drop table Staging_SB_MapleRoots.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_load;

Select * into 
Staging_SB_MapleRoots.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_load
from Staging_SB_MapleRoots.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_Preload;--8719

Select * from Staging_SB_MapleRoots.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_load ;--8387

--Exec SF_ColCompare 'Insert','SL_SUPERION_MAPLESB', 'Sales_Request__c_Tritech_SFDC_PricingRequestForm_load' 

/*
Salesforce object Sales_Request__c does not contain column Z_Assigned_To__c_orig
Salesforce object Sales_Request__c does not contain column Z_Assigned_To__c_SFID
Salesforce object Sales_Request__c does not contain column Assigned_CSL__c_orig
Salesforce object Sales_Request__c does not contain column Assigned_CSL__c_SFID
Salesforce object Sales_Request__c does not contain column Assigned_Pricing_Manager__c_orig
Salesforce object Sales_Request__c does not contain column Assigned_Pricing_Manager__c_SFID
Salesforce object Sales_Request__c does not contain column Assigned_Proposal_Manager__c_orig
Salesforce object Sales_Request__c does not contain column Assigned_Proposal_Manager__c_SFID
Salesforce object Sales_Request__c does not contain column Completed_By__c_orig
Salesforce object Sales_Request__c does not contain column Contact__c_orig
Salesforce object Sales_Request__c does not contain column CreatedById_orig
Salesforce object Sales_Request__c does not contain column LastModifiedById_orig
Salesforce object Sales_Request__c does not contain column Opportunity__c_orig
Salesforce object Sales_Request__c does not contain column Z_Other_Instructions__c_orig
Salesforce object Sales_Request__c does not contain column Other_Details_Special_Instructions__c_orig
Salesforce object Sales_Request__c does not contain column Requestor__c_orig
Salesforce object Sales_Request__c does not contain column Procurement_Activity__c_orig
Salesforce object Sales_Request__c does not contain column Rapid_Implementation__c_orig
Salesforce object Sales_Request__c does not contain column RecordTypeId_orig
Salesforce object Sales_Request__c does not contain column Request_Status__c_orig
Salesforce object Sales_Request__c does not contain column Z_Status__c_orig
Salesforce object Sales_Request__c does not contain column Status__c_map
*/


alter table Sales_Request__c_Tritech_SFDC_PricingRequestForm_load
alter column Other_Details_Special_Instructions__c nvarchar(max)

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','Sales_Request__c_Tritech_SFDC_PricingRequestForm_load'

-----------------------------------------------------------------------------------------

--drop table Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors
select * 
into Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors
from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load
where error<>'Operation Successful.'
;--24

--drop table Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors_bkp
select * 
into Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors_bkp
from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load
where error<>'Operation Successful.';--24

select  time_elapsed__c
--update a set time_elapsed__c=null
from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors a
where 
error like'%double%';--24

Select * from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors ;--24

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors'

Select *
--delete 
from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load
where error<>'Operation Successful.'; --24

--insert into Sales_Request__c_Tritech_SFDC_PricingRequestForm_load
Select *
from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_errors;--24

Select error,count(*) from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load
group by error;

-----------------------------------------------------------------------------------------

--Shipment_Notes__c Hotfix:

Select 
 Id=ld.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Shipment_Notes__c=ld.Notes__c
,Legacy_Record_ID__c_orig=ld.Legacy_Record_ID__c

--into Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_UpdateShipmentNotes

from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load ld;--8387

--Exec SF_ColCompare 'Update','SL_SUPERION_MAPLESB', 'Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_UpdateShipmentNotes' 

/*
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_MAPLESB','Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_UpdateShipmentNotes'