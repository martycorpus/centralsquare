/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: CommunityUser Migration Script
DEVELOPER	: RTECSON
CREATED DT  : 02/06/2019
DETAIL		: 	
				Selection Criteria:
				a. Migrate all related defects related to a migrated case.

				Discussion: Will migrate to Engineering_Issue__c
				need to Validate with Engineering what their requirements are, and need to also confirm requirements specific to defects NOT related to a case.

DECISION	:
=============
3/18		- For Mapleroots and Production, all Community Users will have a profile of "CentralSquare Community Standard - Staging"
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/06/2019		Ron Tecson			Initial. Copied and modified from 4.2 CommunityUser Migration Script PB.sql.
02/26/2019		Ron Tecson			Modified for MapleRoots Data Load. 
03/18/2019		Ron Tecson			Reload in MapleRoots. Added the log and failures. Ran an update to the main load table after running the process from User_CommunityUser_Load_RT_Error_03182019 table.
03/18/2019		Ron Tecson			Added Refresh of the Local Database after import process. And defaulting the Profile to "CentralSquare Community Standard - Staging".
									

ERROR/FAILURES:

DATE			ERROR													RESOLUTION
=============== ======================================================= ========================================================================================
02/26/2019		Unable to obtain exclusive to this record or 1 records	Re-run the error and set the batchsize to 1
				Limit Exceeded											Asked Maria H. to increase the # of licenses for community users.
				Duplicate Nickname.<br>Another user has already			Fixed the Nickname by CommunityNickName = 'admin1a'
					selected this nickname.<br>Please select another.	

03/18/2019		'Cannot create a portal user without contact'			27 Records. Account related to these failed contacts are out of scope. Thus not migrated the contacts.
				'Duplicate Nickname.<br>Another user has already		Fixed the Nickname by CommunityNickName = 'admin1a'
					selected this nickname.<br>Please select another.'
				Unable to obtain exclusive to this record or 1 records	Re-run the error and set the batchsize to 1

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

-- Start: -- 11:43AM 3/18
Use SUPERION_MAPLESB; 

Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'Profile', 'yes'
Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'Account', 'yes'
Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'Contact', 'yes'
Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'User', 'yes'

USE Tritech_PROD
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Case', 'yes'
-- End: -- 11:46AM 3/18

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUser_PreLoad_RT') 
	DROP TABLE Staging_SB_MapleRoots.dbo.User_CommunityUser_PreLoad_RT;


---------------------------------------------------------------------------------
-- Load Staging Table -- Estimated Query Time: 3:10 for 8128 records
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots;

DECLARE @Default_Profile NVARCHAR(18) = (SELECT [Id]  FROM [SUPERION_MAPLESB].[dbo].[Profile] WHERE NAME  =  'CentralSquare Community Standard - Staging');

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Tritech_Id__c,
		'Tritech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		Concat(a.[Username], '.CSFullSB') as Username,  --update for production
		a.[LastName] as LastName,
		a.[FirstName] as FirstName,
		a.[CompanyName] as CompanyName,
		a.[Division] as Division,
		a.[Department] as Department,
		a.[Title] as Title,
		a.[Street] as Street,
		a.[City] as City,
		a.[State] as State,
		a.[PostalCode] as PostalCode,
		a.[Country] as Country,
		Concat(a.[Email], '.CSFullSB') as Email,  --update for production
		a.[Phone] as Phone,
		a.[Fax] as Fax,
		a.[MobilePhone] as MobilePhone,
		a.[Alias] as Alias,
		Concat(a.[CommunityNickname], '1') as CommunityNickname,  -- Remove the '1' value for production load, only necessary for UAT
		'false' as IsActive,
		a.[TimeZoneSidKey] as TimeZoneSidKey,
		a.[LocaleSidKey] as LocaleSidKey,
		a.[ReceivesInfoEmails] as ReceivesInfoEmails,
		a.[ReceivesAdminInfoEmails] as ReceivesAdminInfoEmails,
		a.[EmailEncodingKey] as EmailEncodingKey,
		TT_Profile.[Name] as ProfileName_Original,
		--Spr_Profile.ID as ProfileID,
		@Default_Profile as ProfileID,
		a.[LanguageLocaleKey] as LanguageLocaleKey,
		a.[EmployeeNumber] as EmployeeNumber,
		a.[UserPermissionsMarketingUser] as UserPermissionsMarketingUser,
		a.[UserPermissionsOfflineUser] as UserPermissionsOfflineUser,
		a.[UserPermissionsCallCenterAutoLogin] as UserPermissionsCallCenterAutoLogin,
		a.[UserPermissionsMobileUser] as UserPermissionsMobileUser,
		a.[UserPermissionsSFContentUser] as UserPermissionsSFContentUser,
		a.[UserPermissionsKnowledgeUser] as UserPermissionsKnowledgeUser,
		a.[UserPermissionsInteractionUser] as UserPermissionsInteractionUser,
		a.[UserPermissionsSupportUser] as UserPermissionsSupportUser,
		a.[ForecastEnabled] as ForecastEnabled,
		a.[UserPreferencesActivityRemindersPopup] as UserPreferencesActivityRemindersPopup,
		a.[UserPreferencesEventRemindersCheckboxDefault] as UserPreferencesEventRemindersCheckboxDefault,
		a.[UserPreferencesTaskRemindersCheckboxDefault] as UserPreferencesTaskRemindersCheckboxDefault,
		a.[UserPreferencesReminderSoundOff] as UserPreferencesReminderSoundOff,
		a.[UserPreferencesApexPagesDeveloperMode] as UserPreferencesApexPagesDeveloperMode,
		a.[UserPreferencesHideCSNGetChatterMobileTask] as UserPreferencesHideCSNGetChatterMobileTask,
		a.[UserPreferencesHideCSNDesktopTask] as UserPreferencesHideCSNDesktopTask,
		a.[UserPreferencesSortFeedByComment] as UserPreferencesSortFeedByComment,
		a.[UserPreferencesLightningExperiencePreferred] as UserPreferencesLightningExperiencePreferred,
		a.[CallCenterId] as CallCenterId,
		a.[Extension] as Extension,
		a.[PortalRole] as PortalRole,
		a.[FederationIdentifier] as FederationIdentifier,
		a.[AboutMe] as AboutMe,
		a.[DigestFrequency] as DigestFrequency,
		Cont.ID as ContactID, 
		a.[Bomgar_Username__c] as Bomgar_Username__c
		INTO Staging_SB_MapleRoots.dbo.User_CommunityUser_PreLoad_RT
		FROM Tritech_PROD.dbo.[User] a
		LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] TT_Profile ON  a.ProfileId = TT_Profile.ID											----> TriTech Profiles
		LEFT OUTER JOIN Superion_MapleSB.dbo.[Profile] Spr_Profile ON Spr_Profile.[Name] =												----> Superion Profile
								CASE TT_Profile.[Name] 
									WHEN 'TriTech Portal Read Only with Tickets' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Read-Only User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Standard User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Manager' THEN 'CentralSquare Community Standard - Dedicated'
									ELSE 'Superion standard user' END
		LEFT OUTER JOIN Superion_MapleSB.dbo.[Contact] Cont ON a.ContactID = Cont.Legacy_ID__c											----> Superion Contact
		WHERE 
		TT_Profile.[Name] IN
					('TriTech Portal Read Only with Tickets',
					 'TriTech Portal Read-Only User',
					 'Overage High Volume Customer Portal User',
					 'Overage Customer Portal Manager Custom',
					 'TriTech Portal Standard User',
					 'TriTech Portal Manager')
		AND
		(
			(	
				a.isActive = 'true'
				AND  -- requires the user to have either logged into their account since 1/1/2018 or the account was created since 1/1/2018
				(
					a.LastLoginDate >= Cast('01/01/2018' as datetime)
					OR
					a.CreatedDate >= Cast('01/01/2018' as datetime)
				)
			)
			OR  -- Includes any Community User that has submitted a case since 1/1/2016
			(
			  a.ContactID IN
			  (
				select distinct contactID from Tritech_PROD.dbo.[CASE] 
				WHERE CreatedDate >= CAST('01/01/2016' as Datetime)
			  )
			)
		)
		Order by Spr_Profile.ID

-- (8473 rows affected) 1 min.

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUser_Load_RT') 
	DROP TABLE Staging_SB_MapleRoots.dbo.User_CommunityUser_Load_RT;

SELECT * 
INTO Staging_SB_MapleRoots.dbo.User_CommunityUser_Load_RT
FROM Staging_SB_MapleRoots.dbo.User_CommunityUser_PreLoad_RT



---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB_MapleRoots.dbo.User_CommunityUser_Load_RT
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Delete preloaded records for a delta load
---------------------------------------------------------------------------------
Delete CommUsr_Load

select * 
FROM User_CommunityUser_Load_RT CommUsr_Load
INNER JOIN Staging_SB_MapleRoots.dbo.[User] ON CommUsr_Load.Legacy_Tritech_Id__c = [User].Legacy_Tritech_Id__c

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

Exec SF_BulkOps 'upsert:batchsize(20)', 'RT_Superion_MAPLEROOTS', 'User_CommunityUser_Load_RT', 'Legacy_Tritech_ID__c'

/*---------------------------------------------------------------------------------

1 hr 17 mins. 3/18

--- Starting SF_BulkOps for User_CommunityUser_Load_RT V3.6.7
15:49:23: Run the DBAmp.exe program.
15:49:23: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:49:23: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .
15:49:23: DBAmp is using the SQL Native Client.
15:49:23: Batch size reset to 20 rows per batch.
15:49:25: SOAP Headers: 
15:49:26: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.
15:49:26: Warning: Column 'Sort' ignored because it does not exist in the User object.
17:06:34: 8473 rows read from SQL Table.
17:06:34: 48 rows failed. See Error column of row for more information.
17:06:34: 8425 rows successfully processed.
17:06:34: Errors occurred. See Error column of row for more information.
17:06:34: Percent Failed = 0.600.
17:06:34: Error: DBAmp.exe was unsuccessful.
17:06:34: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe upsert:batchsize(20) User_CommunityUser_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_MAPLEROOTS"  "Legacy_Tritech_ID__c"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 202]
SF_BulkOps Error: 15:49:23: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC15:49:23: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .15:49:23: DBAmp is using the SQL Native Client.15:49:23: Batch size reset to 20 rows per batch.15:49:25: SOAP Headers: 15:49:26: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.15:49:26: Warning: Column 'Sort' ignored because it does not exist in the User object.17:06:34: 8473 rows read from SQL Table.17:06:34: 48 rows failed. See Error column of row for more information.17:06:34: 8425 rows successfully processed.17:06:34: Errors occurred. See Error column of row for more information.


1 hr 36 mins. 2/26

--- Starting SF_BulkOps for User_CommunityUser_Load_RT V3.6.7
15:52:20: Run the DBAmp.exe program.
15:52:20: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:52:20: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .
15:52:21: DBAmp is using the SQL Native Client.
15:52:21: Batch size reset to 20 rows per batch.
15:52:22: SOAP Headers: 
15:52:22: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.
15:52:22: Warning: Column 'Sort' ignored because it does not exist in the User object.
17:27:59: 8389 rows read from SQL Table.
17:27:59: 3267 rows failed. See Error column of row for more information.
17:27:59: 5122 rows successfully processed.
17:27:59: Errors occurred. See Error column of row for more information.
17:27:59: Percent Failed = 38.900.
17:27:59: Error: DBAmp.exe was unsuccessful.
17:27:59: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe upsert:batchsize(20) User_CommunityUser_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_MAPLEROOTS"  "Legacy_Tritech_ID__c"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 184]
SF_BulkOps Error: 15:52:20: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC15:52:20: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .15:52:21: DBAmp is using the SQL Native Client.15:52:21: Batch size reset to 20 rows per batch.15:52:22: SOAP Headers: 15:52:22: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.15:52:22: Warning: Column 'Sort' ignored because it does not exist in the User object.17:27:59: 8389 rows read from SQL Table.17:27:59: 3267 rows failed. See Error column of row for more information.17:27:59: 5122 rows successfully processed.17:27:59: Errors occurred. See Error column of row for more information.

--------------------------------------------------------------------------------- */

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

SELECT ERROR, count(*) 
FROM User_CommunityUser_Load_RT
GROUP BY ERROR

select * from User_CommunityUser_Load_RT where error like '%success%'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------

Use SUPERION_MAPLESB; 

Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'User', 'yes'

-------------------- Start: FIXED ERROR 3/18/2019 ---------------------------------------

use Staging_SB_MapleRoots

SELECT ERROR, count(*) 
FROM User_CommunityUser_Load_RT
GROUP BY ERROR

select * from User_CommunityUser_Load_RT where error = 'Cannot create a portal user without contact'

select * from User_CommunityUser_Load_RT where error = 'Duplicate Nickname.<br>Another user has already selected this nickname.<br>Please select another.'

select Legacy_Tritech_Id__c, count(*)
from User_CommunityUser_Load_RT
group by Legacy_Tritech_Id__c
having count(*) > 1

select * --INTO User_CommunityUser_Load_RT_Error_03182019
from
User_CommunityUser_Load_RT
where Error <> 'Operation Successful.' and
error <> 'Cannot create a portal user without contact'

update User_CommunityUser_Load_RT_Error_03182019
set CommunityNickName = 'admin1a'
where error = 'Duplicate Nickname.<br>Another user has already selected this nickname.<br>Please select another.'

Exec SF_BulkOps 'INSERT:batchsize(50)','RT_Superion_MAPLEROOTS', 'User_CommunityUser_Load_RT_Error_03182019' 

update T1
set
T1.ID = T2.ID,
T1.Error = T2.Error --'Pre-Process Successfully. Original Errors are available @ User_CommunityUser_Load_RT_Error_03182019 table'
FROM
User_CommunityUser_Load_RT T1
JOIN User_CommunityUser_Load_RT_Error_03182019 T2 ON T1.Legacy_Tritech_Id__c = T2.Legacy_Tritech_Id__c

select * INTO User_CommunityUser_Load_RT_Bkp
from User_CommunityUser_Load_RT

select id, name from RT_Superion_Mapleroots...Profile where Name = 'CentralSquare Community Standard - Staging'

update User_CommunityUser_Load_RT
set ProfileId = '00e0r000000De5tAAC'

Exec SF_BulkOps 'UPDATE','RT_Superion_MAPLEROOTS', 'User_CommunityUser_Load_RT' 


-------------------- End: FIXED ERROR 3/18/2019 ---------------------------------------

select * INTO User_CommunityUser_Error_Load_Duplicate_NickName from User_CommunityUser_Load_RT where error = 'Duplicate Nickname.<br>Another user has already selected this nickname.<br>Please select another.'

'unable to obtain exclusive access to this record or 1 records: 0011h00000Ld3xrAAB'

select * from RT_SUPERION_MAPLEROOTS...Account where Id = '0011h00000Ld3xrAAB'

select * INTO User_CommunityUser_Error_Load_Limit_Exceeded from User_CommunityUser_Load_RT where error not like '%success%' and error = 'limit exceeded'

update User_CommunityUser_Error_Load_Limit_Exceeded
set ProfileID = '00e1h000000QIeGAAW'

Exec SF_BulkOps 'Insert','RT_Superion_MAPLEROOTS','User_CommunityUser_Error_Load_Limit_Exceeded'



select error, count(*) from User_CommunityUser_Error_Load_Limit_Exceeded
group by error

select * INTO User_CommunityUser_Error_Load_Unable_To_Obtain from User_CommunityUser_Load_RT where error not like '%success%' and error like 'unable to obtain%'

update User_CommunityUser_Error_Load_Duplicate_NickName
set CommunityNickName = 'admin1a'

update User_CommunityUser_Error_Load_Unable_To_Obtain
set ProfileID = '00e1h000000QIeGAAW'

Exec SF_BulkOps 'INSERT:batchsize(50)','RT_Superion_MAPLEROOTS', 'User_CommunityUser_Error_Load_Duplicate_NickName' --'User_CommunityUser_Error_Load_Limit_Exceeded' --'User_CommunityUser_Error_Load_Unable_To_Obtain_2'

select error, count(*) from User_CommunityUser_Error_Load_Unable_To_Obtain_2
group by error

drop table User_CommunityUser_Error_Load_Unable_To_Obtain_2

select * INTO User_CommunityUser_Error_Load_Unable_To_Obtain_2 from User_CommunityUser_Error_Load_Unable_To_Obtain where error = 'malformed id'  

update User_CommunityUser_Error_Load_Unable_To_Obtain_2
set ProfileID = '00e6A000000MgNwQAK'            

select * from User_CommunityUser_Error_Load_Unable_To_Obtain_2     

-----------------------------------------

select ProfileId, Error, count(*) from User_CommunityUser_Error_Load_Duplicate_NickName group by ProfileId, Error
select ProfileId, Error, count(*) from User_CommunityUser_Error_Load_Unable_To_Obtain_2 group by ProfileId, Error

select ID, Cast('' as nvarchar(255)) as Error, '00e6A000000MgNwQAK' AS ProfileId 
INTO USER_Update_Profile_RT
FROM ( 
select Id from User_CommunityUser_Error_Load_Unable_To_Obtain 
UNION
select Id from User_CommunityUser_Error_Load_Limit_Exceeded) A
where ID IS NOT NULL AND ID <> ''
-- (3251 rows affected)

select * from USER_Update_Profile_RT where ID = '0051h000001hHrwAAE'

Exec SF_BulkOps 'UPDATE','RT_Superion_MAPLEROOTS', 'USER_Update_Profile_RT' --'User_CommunityUser_Error_Load_Limit_Exceeded' --'User_CommunityUser_Error_Load_Unable_To_Obtain_2'

select * from USER_Update_Profile_RT

---------------------------------------------------------------------------------
-- Refresh into the local database
---------------------------------------------------------------------------------

Exec SF_Refresh 'RT_Superion_FULLSB', 'User_CommunityUser_Load_RT', 'yes'

select * from RT_Superion_FULLSB...[User] where LastName = 'Keller' and FirstName = 'Loretta'

CommunityNickName = 'admin1' --LastName = 'Tassi'

select * from RT_Superion_MAPLEROOTS...[User] where CommunityNickName = 'admin1' --LastName = 'Tassi'

