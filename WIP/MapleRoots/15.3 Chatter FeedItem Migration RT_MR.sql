/*****************************************************************************************************************************************************
REQ #		: REQ-0842
TITLE		: Chatter Feed Item Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/08/2019
DETAIL		: Migrate Chatter Groups from source SFDC Org.
 
				- Selection criteria:
				All Active Groups with activity since 30 September 2018.

				Additional Notes:
				- Support indicates data is not necessary for their use.
				- Require input from Marketing and Sales

				3 Chatter Groups:
				- Company news (drop)
				- Industry News
				- Customer news
				- additional implementation Groups
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.3 Chatter FeedItem Migration PB.sql
02/28/2019		Ron Tecson			Modified for the MapleRoots Data Load.
03/04/2019		Ron Tecson			Reloaded the records in MapleRoots to reference the correct database and linked servers.

DECISIONS:

******************************************************************************************************************************************************/

--------------------------------------------------------------------------------
-- Refresh Data 
--------------------------------------------------------------------------------
USE Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_Prod', 'FeedItem', 'yes';
EXEC SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';

USE SUPERION_MAPLESB
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Case', 'yes';			-- 8 mins
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Opportunity', 'yes';		-- 3 mins
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Contact', 'yes';			-- 2 mins
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Account', 'yes';			-- 1.5 mins
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'CollaborationGroup', 'yes';
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'User', 'yes';
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Task', 'yes';
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'EngineeringIssue__c', 'yes';
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Event', 'yes';
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Procurement_Activity__c', 'yes';
--Exec SF_Refresh 'PB_Superion_FULLSB', 'Write_In__c', 'yes';
--Exec SF_Refresh 'PB_Superion_FULLSB', 'Pricing_Request_Form__c', 'yes';

EXEC SF_Replicate 'RT_SUPERION_MAPLEROOTS', 'Event', 'yes';

-------------------------------------------------------------------------------------
-- Create ChatterParent reference 
-----------------------------------------------------------------------------------
Use Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='map_ChatterParent') 
DROP TABLE Staging_SB_MapleRoots.dbo.map_ChatterParent;

SELECT * INTO Staging_SB_MapleRoots.dbo.map_ChatterParent from
(
		SELECT Legacy_ID__c as LegacyID, ID from SUPERION_MAPLESB.dbo.[Case] where legacy_id__c is not null								----> Case
		UNION
		SELECT Legacy_Opportunity_ID__c as LegacyID, ID from SUPERION_MAPLESB.dbo.Opportunity where Legacy_Opportunity_ID__c is not null ----> Opportunity
		UNION
		SELECT Legacy_ID__c as LegacyID, ID from SUPERION_MAPLESB.dbo.Contact where Legacy_ID__c is not null								----> Contact
		UNION
		SELECT LegacySFDCAccountID__c as LegacyID, ID from SUPERION_MAPLESB.dbo.Account where LegacySFDCAccountID__c is not null			----> Account
		UNION
		SELECT GroupSource.ID as LegacyID, GroupTarget.ID as ID from Tritech_PROD.dbo.CollaborationGroup GroupSource					----> Collaboration Group
				LEFT OUTER JOIN SUPERION_MAPLESB.dbo.CollaborationGroup GroupTarget ON GroupSource.[Name] = GroupTarget.[Name]
				WHERE GroupTarget.ID is NOT NULL
		UNION
		SELECT Legacy_Tritech_Id__c as LegacyID, ID from SUPERION_MAPLESB.dbo.[User] where Legacy_Tritech_Id__c is not null				----> User
		UNION
		---- Write_IN
		--Select Legacy_Id__c as LegacyID, ID from SUPERION_MAPLESB.dbo.Write_In__c where legacy_id__c is not null  and Migrated_Record__c = 'true'
		--UNION
		SELECT Legacy_Id__c as LegacyID, ID from SUPERION_MAPLESB.dbo.Task where Legacy_ID__c is not null								----> Task
		UNION
		SELECT Legacy_CRMId__c as LegacyID, ID from SUPERION_MAPLESB.dbo.EngineeringIssue__c where Legacy_CRMId__c is not null			----> Engineering Issue
		UNION
		---- Pricing
		--Select Legacy_ID__c as LegacyID, ID from SUPERION_MAPLESB.dbo.Pricing_Request_Form__c where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		--UNION
		SELECT a.ID as LegacyID, Oppty.ID as ID from Tritech_PROD.dbo.Demo_Request__c a
			INNER JOIN SUPERION_MAPLESB.dbo.Opportunity Oppty ON a.Opportunity__c = Oppty.Legacy_Opportunity_ID__c						----> Demo Request
		UNION
		--SELECT Legacy_ID__c as LegacyID, ID from SUPERION_MAPLESB.dbo.Event where Legacy_ID__c is not null								----> Event
		SELECT Legacy_ID__c as LegacyID, ID from RT_SUPERION_MAPLEROOTS...Event where Legacy_ID__c is not null
		UNION
		SELECT Legacy_ID__c as Legacyid, ID from SUPERION_MAPLESB.dbo.Procurement_Activity__c where Legacy_id__c is not null				----> Procurement activity
)t

-- 03/04 (1447942 rows affected) -- 5 mins
-- 3/1 (1294220 rows affected) -- 2 mins

--Drop Index ChatterParent_idx on map_ChatterParent

CREATE INDEX ChatterParent_idx
ON [map_ChatterParent] (LegacyID) 


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedItem_PreLoad_RT') 
DROP TABLE Staging_SB_MapleRoots.dbo.FeedItem_PreLoad_RT;


SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.Body, 
	a.CreatedByID as CreatedBy_Orig,
	isNull(CreatedBy.ID, (Select ID from SUPERION_MAPLESB.dbo.[User] where [Name] = 'Superion API')) as CreatedById,
	a.CreatedDate as CreatedDate,
	a.LinkUrl as LinkURL,
	a.IsRichText as IsRichText,
	a.ParentID as Parent_Orig,
	Parent.Id as ParentID,
	--RelatedRecord.ID as RelatedRecordID,
	--ContentFileName as ContentFileName,
	--a.Revision as Revision,
	a.Title as Title,
	a.[Type] as [Type],
	a.ID as Legacy_ID__c
INTO FeedItem_PreLoad_RT
from TriTech_Prod.dbo.FeedItem a
LEFT OUTER JOIN SUPERION_MAPLESB.dbo.[User] CreatedBy ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c			---CreatedBy
INNER JOIN Staging_SB_MapleRoots.dbo.map_ChatterParent Parent ON a.ParentID = Parent.LegacyID								---Parent
WHERE 
	a.[Type] in ('LinkPost', 'TextPost')

-- 03/04 (29012 rows affected)
-- 3/1 (29605 rows affected)

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedItem_Load_RT') 
DROP TABLE Staging_SB_MapleRoots.dbo.FeedItem_Load_RT;

SELECT *
INTO Staging_SB_MapleRoots.dbo.FeedItem_Load_RT
FROM Staging_SB_MapleRoots.dbo.FeedItem_PreLoad_RT
-- 03/04 (29012 rows affected)
-- 3/1 (29605 rows affected)

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB_MapleRoots.dbo.FeedItem_Load_RT
Add [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

EXEC SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'RT_SUPERION_MAPLEROOTS', 'FeedItem_Load_RT'

EXEC SF_BulkOps 'insert:batchsize(1)', 'RT_SUPERION_MAPLEROOTS', 'FeedItem_Activate_Entity_RT'

-- 03/04 - 22 mins

--- Starting SF_BulkOps for FeedItem_Load_RT V3.6.7
17:45:38: Run the DBAmp.exe program.
17:45:38: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:45:38: Inserting FeedItem_Load_RT (SQL01 / Staging_SB_MapleRoots).
17:45:39: DBAmp is using the SQL Native Client.
17:45:39: Batch size reset to 4000 rows per batch.
17:45:39: Sort column will be used to order input rows.
17:45:39: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
17:45:40: Warning: Column 'CreatedBy_Orig' ignored because it does not exist in the FeedItem object.
17:45:40: Warning: Column 'Parent_Orig' ignored because it does not exist in the FeedItem object.
17:45:40: Warning: Column 'Legacy_ID__c' ignored because it does not exist in the FeedItem object.
17:45:41: Job 7501h000001xOhQAAU created.
17:45:41: Batch 7511h000001vYifAAE created with 4000 rows.
17:45:42: Batch 7511h000001vYijAAE created with 4000 rows.
17:45:42: Batch 7511h000001vYioAAE created with 4000 rows.
17:45:43: Batch 7511h000001vYitAAE created with 4000 rows.
17:45:43: Batch 7511h000001vYiyAAE created with 4000 rows.
17:45:44: Batch 7511h000001vYj3AAE created with 4000 rows.
17:45:45: Batch 7511h000001vYj8AAE created with 4000 rows.
17:45:45: Batch 7511h000001vYjDAAU created with 1012 rows.
17:45:45: Job submitted.
17:45:45: 29012 rows read from SQL Table.
17:45:46: Job still running.
17:46:02: Job still running.
17:47:02: Job still running.
17:48:03: Job still running.
17:49:04: Job still running.
17:50:05: Job still running.
17:51:05: Job still running.
17:52:06: Job still running.
17:53:07: Job still running.
17:54:08: Job still running.
17:55:09: Job still running.
17:56:09: Job still running.
17:57:10: Job still running.
17:58:11: Job still running.
17:59:12: Job still running.
18:00:12: Job still running.
18:01:13: Job still running.
18:02:14: Job still running.
18:03:15: Job still running.
18:04:15: Job still running.
18:05:16: Job still running.
18:06:17: Job still running.
18:07:18: Job Complete.
18:07:18: DBAmp is using the SQL Native Client.
18:07:20: 28824 rows successfully processed.
18:07:20: 188 rows failed.
18:07:20: Errors occurred. See Error column of row and above messages for more information.
18:07:21: Percent Failed = 0.600.
18:07:21: Error: DBAmp.exe was unsuccessful.
18:07:21: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:bulkapi,batchsize(4000) FeedItem_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_SUPERION_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 150]
SF_BulkOps Error: 17:45:38: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC17:45:38: Inserting FeedItem_Load_RT (SQL01 / Staging_SB_MapleRoots).17:45:39: DBAmp is using the SQL Native Client.17:45:39: Batch size reset to 4000 rows per batch.17:45:39: Sort column will be used to order input rows.17:45:39: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.17:45:40: Warning: Column 'CreatedBy_Orig' ignored because it does not exist in the FeedItem object.17:45:40: Warning: Column 'Parent_Orig' ignored because it does not exist in the FeedItem object.17:45:40: Warning: Column 'Legacy_ID__c' ignored because it does not exist in the FeedItem object.17:45:41: Job 7501h000001xOhQAAU created.17:45:41: Batch 7511h000001vYifAAE created with 4000 rows.17:45:42: Batch 7511h000001vYijAAE created with 4000 rows.17:45:42: Batch 7511h000001vYioAAE created with 4000 rows.17:45:43: Batch 7511h000001vYitAAE created with 4000 rows.17:45:43: Batch 7511h000001vYiyAAE created with 4000 rows.17:45:44: Batch 7511h000001vYj3AAE created with 4000 rows.17:45:45: Batch 7511h000001vYj8AAE created with 4000 rows.17:45:45: Batch 7511h000001vYjDAAU created with 1012 rows.17:45:45: Job submitted.17:45:45: 29012 rows read from SQL Table.17:45:46: Job still running.17:46:02: Job still running.17:47:02: Job still running.17:48:03: Job still running.17:49:04: Job still running.17:50:05: Job still running.17:51:05: Job still running.17:52:06: Job still running.17:53:07: Job still running.17:54:08: Job still running.17:55:09: Job still running.17:56:09: Job still running.17:57:10: Job still running.17:58:11: Job still running.17:59:12: Job still running.18:00:12: Job still running.18:01:13: Job still running.18:02:14: Job still running.18:03:15: Job still running.18:04:15: Job still running.18:05:16: Job still running.18:06:17: Job still running.18:07:18: Job Complete.18:07:18: DBAmp is using the SQL Native Client.18:07:20: 28824 rows successfully processed.18:07:20: 188 rows failed.18:07:20: Errors occurred. See Er...

--- Starting SF_BulkOps for FeedItem_Load_RT V3.6.7
02:31:16: Run the DBAmp.exe program.
02:31:16: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
02:31:16: Inserting FeedItem_Load_RT (SQL01 / Staging_SB_MapleRoots).
02:31:16: DBAmp is using the SQL Native Client.
02:31:16: Batch size reset to 4000 rows per batch.
02:31:17: Sort column will be used to order input rows.
02:31:17: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
02:31:17: Warning: Column 'CreatedBy_Orig' ignored because it does not exist in the FeedItem object.
02:31:17: Warning: Column 'Parent_Orig' ignored because it does not exist in the FeedItem object.
02:31:17: Warning: Column 'Legacy_ID__c' ignored because it does not exist in the FeedItem object.
02:31:18: Job 7500v000004ejfgAAA created.
02:31:19: Batch 7510v000004D4VeAAK created with 4000 rows.
02:31:19: Batch 7510v000004D4VjAAK created with 4000 rows.
02:31:20: Batch 7510v000004D4VoAAK created with 4000 rows.
02:31:20: Batch 7510v000004D4VaAAK created with 4000 rows.
02:31:21: Batch 7510v000004D4VtAAK created with 4000 rows.
02:31:21: Batch 7510v000004D4VyAAK created with 4000 rows.
02:31:22: Batch 7510v000004D4W3AAK created with 4000 rows.
02:31:22: Batch 7510v000004D4W8AAK created with 1605 rows.
02:31:23: Job submitted.
02:31:23: 29605 rows read from SQL Table.
02:31:23: Job still running.
02:31:39: Job still running.
02:32:40: Job still running.
02:33:41: Job still running.
02:34:42: Job still running.
02:35:42: Job still running.
02:36:43: Job still running.
02:37:44: Job still running.
02:38:45: Job still running.
02:39:45: Job still running.
02:40:46: Job Complete.
02:40:47: DBAmp is using the SQL Native Client.
02:40:49: 6534 rows successfully processed.
02:40:49: 23071 rows failed.
02:40:49: Errors occurred. See Error column of row and above messages for more information.
02:40:49: Percent Failed = 77.900.
02:40:49: Error: DBAmp.exe was unsuccessful.
02:40:49: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:bulkapi,batchsize(4000) FeedItem_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 145]
SF_BulkOps Error: 02:31:16: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC02:31:16: Inserting FeedItem_Load_RT (SQL01 / Staging_SB_MapleRoots).02:31:16: DBAmp is using the SQL Native Client.02:31:16: Batch size reset to 4000 rows per batch.02:31:17: Sort column will be used to order input rows.02:31:17: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.02:31:17: Warning: Column 'CreatedBy_Orig' ignored because it does not exist in the FeedItem object.02:31:17: Warning: Column 'Parent_Orig' ignored because it does not exist in the FeedItem object.02:31:17: Warning: Column 'Legacy_ID__c' ignored because it does not exist in the FeedItem object.02:31:18: Job 7500v000004ejfgAAA created.02:31:19: Batch 7510v000004D4VeAAK created with 4000 rows.02:31:19: Batch 7510v000004D4VjAAK created with 4000 rows.02:31:20: Batch 7510v000004D4VoAAK created with 4000 rows.02:31:20: Batch 7510v000004D4VaAAK created with 4000 rows.02:31:21: Batch 7510v000004D4VtAAK created with 4000 rows.02:31:21: Batch 7510v000004D4VyAAK created with 4000 rows.02:31:22: Batch 7510v000004D4W3AAK created with 4000 rows.02:31:22: Batch 7510v000004D4W8AAK created with 1605 rows.02:31:23: Job submitted.02:31:23: 29605 rows read from SQL Table.02:31:23: Job still running.02:31:39: Job still running.02:32:40: Job still running.02:33:41: Job still running.02:34:42: Job still running.02:35:42: Job still running.02:36:43: Job still running.02:37:44: Job still running.02:38:45: Job still running.02:39:45: Job still running.02:40:46: Job Complete.02:40:47: DBAmp is using the SQL Native Client.02:40:49: 6534 rows successfully processed.02:40:49: 23071 rows failed.02:40:49: Errors occurred. See Error column of row and above messages for more information.


-------------------------------
--Validation
-------------------------------

SELECT ERROR, count(*) 
FROM FeedItem_Load_RT where error not like '%success%'
GROUP BY ERROR

SELECT * FROM FeedItem_Load_RT WHERE error not like '%success%' and error not like '%task%'

SELECT * FROM FeedItem_Load_RT WHERE error not like '%success%' and error not like '%INVALID%' and error not like '%ACTIVATE_ENTITY%' and error not like '%ARCHIVED_GROUP%'

SELECT * FROM FeedItem_Load_RT WHERE error like '%success%' -- 6534

SELECT * FROM FeedItem_Load_RT WHERE error like '%INVALID%' -- INVALID_CROSS_REFERENCE_KEY:Users specified in CreatedById must be active users in this organization.: -- 16094

SELECT * INTO FeedItem_Activate_Entity_RT FROM FeedItem_Load_RT WHERE error like '%ACTIVATE_ENTITY%' -- CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY:Account is not enabled for feeds: -- 6973

SELECT * FROM FeedItem_Load_RT WHERE error like '%ARCHIVED_GROUP%' -- CANNOT_POST_TO_ARCHIVED_GROUP:You can't post to an archived group.: -- 4


---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------

USE SUPERION_MAPLESB

EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'FeedItem', 'yes'; -- 42 mins.

