/*
-- Author     : Shivani Mogullapalli	
-- Date       : 27/11/2018
-- Description: Creating Files for storage of Body Field of Knowledge Article field.
-- Last Modified Date: 
-- Last Modified By : Shivani Mogullapalli
-- Requirement No : 

-----------Storing the Body of Articles as files in the system.----------
*/
--------------------------------- Enabling the Configurations------------------------------
--exec sp_configure 'show advanced options', 1;
--GO
--RECONFIGURE;
--GO
--exec sp_configure 'Ole Automation Procedures', 1;
--GO
--RECONFIGURE;
--GO
----------------------------------------------------------------------------------------------
/* List of the ArticleTypes in Legacy(Tritech_PROD) Database 
-- select distinct Articletype from Tritech_PROD.dbo.KnowledgeArticleVersion
				How_To__kav
				Tech_Advisory__kav
				Defect__kav
				Video__kav
				Ticket_Solutions__kav
				FAQ__kav
				Tech_Tips__kav
				Release_Notes__kav
				User_Manuals__kav
*/


use Staging_SB_MapleRoots
go

-- drop table KnowledgeArticle_FileGeneration_Tritech_Preload
with Article_Description_File as(
select
							 id				= id
							,FileName		= Attachments_How_To__Name__s
							,File__c		= convert(varbinary(max),Attachments_How_To__Body__s,1)
							,Articletype	= ArticleType
from Tritech_PROD.dbo.How_To__kav
									union 
select 
							id				= id
							,FileName       = Attachments_Tech_Advisory__Name__s
							,File__c		= convert(varbinary(max),Attachments_Tech_Advisory__Body__s,1)
							,Articletype	= Articletype
from Tritech_PROD.dbo.Tech_Advisory__kav
									union 
select
							 id				= id
							,FileName		= Attachments_Defects__Name__s
							,File__c		= convert(varbinary(max),Attachments_Defects__Body__s,1)
							,Articletype	= ArticleType
from Tritech_PROD.dbo.Defect__kav
									union 
select
							 id				= id
							,FileName		= Article_Attachments_Video__Name__s
							,File__c		= convert(varbinary(max),Article_Attachments_Video__body__s,1)
							,Articletype	= ArticleType
from Tritech_PROD.dbo.Video__kav 
									union 
select
							 id				= id
							,FileName		= Attachments_Ticket_Solutions__Name__s
							,File__c		= convert(varbinary(max),Attachments_Ticket_Solutions__Body__s,1)
							,Articletype	= ArticleType
from Tritech_PROD.dbo.Ticket_Solutions__kav
									union 
select
							 id				= id
							,FileName		= Attachments_Tech_Tips__Name__s
							,File__c		= convert(varbinary(max),Attachments_Tech_Tips__Body__s,1)
							,Articletype	= ArticleType
from Tritech_PROD.dbo.Tech_Tips__kav
									union 
select
							 id				= id
							,FileName		= Attachment_Release_Notes__Name__s
							,File__c		= convert(varbinary(max),Attachment_Release_Notes__Body__s,1)
							,Articletype	= ArticleType 
from Tritech_PROD.dbo.Release_Notes__kav
									union 
select
							 id				= id
							,FileName		= Attachments_User_Manuals__Name__s
							,File__c		= convert(varbinary(max),Attachments_User_Manuals__Body__s,1)
							,Articletype	= ArticleType
from Tritech_PROD.dbo.User_Manuals__kav
)


select *
into KnowledgeArticle_FileGeneration_Tritech_Preload
from Article_Description_File 
--(5573 row(s) affected)
-- 2 mins

-- drop table KnowledgeArticle_FileGeneration_Tritech_load
select * 
into KnowledgeArticle_FileGeneration_Tritech_load
from KnowledgeArticle_FileGeneration_Tritech_Preload file_Gen
where file_Gen.File__c is not null
-- (3558 row(s) affected)
-- 2:15 mins


-------------------------Cursor to Create Files in the backend System ----------------------------------------------


------------------------------------Knowledge Article File Generation-------------------------------------------------
DECLARE CURSOR_ArticleIds CURSOR FOR (SELECT Id 
											FROM KnowledgeArticle_FileGeneration_Tritech_load 
									)
DECLARE @ArticleId nvarchar(18);

OPEN CURSOR_ArticleIds

FETCH NEXT FROM CURSOR_ArticleIds INTO @ArticleId
WHILE (@@FETCH_STATUS <> -1)
BEGIN
  DECLARE @ImageData varbinary(max);
  DECLARE @Path nvarchar(1024);
  DECLARE @Filename NVARCHAR(max);
  SELECT @Path = 'D:\Articles_Files\';
				  SELECT 
				  @ImageData = File__c 
				  ,@Filename = FileName
						FROM KnowledgeArticle_FileGeneration_Tritech_load
							WHERE 1=1 
							and Id = @ArticleId;
  
  DECLARE @FullPathToOutputFile NVARCHAR(2048);
  SELECT @FullPathToOutputFile = @Path+@Filename;
  print @FullPathToOutputFile
  DECLARE @ObjectToken INT
  
  EXEC sp_OACreate 'ADODB.Stream', @ObjectToken OUTPUT;
  EXEC sp_OASetProperty @ObjectToken, 'Type', 1;
  EXEC sp_OAMethod @ObjectToken, 'Open';
  EXEC sp_OAMethod @ObjectToken, 'Write', NULL, @ImageData;
  EXEC sp_OAMethod @ObjectToken, 'SaveToFile', NULL, @FullPathToOutputFile, 2;
  EXEC sp_OAMethod @ObjectToken, 'Close';
  EXEC sp_OADestroy @ObjectToken;

  FETCH NEXT FROM CURSOR_ArticleIds INTO @ArticleId
END
CLOSE CURSOR_ArticleIds
DEALLOCATE CURSOR_ArticleIds
--------------------------------------------------------------------------------------------------------------------------
-- 2:18 mins 