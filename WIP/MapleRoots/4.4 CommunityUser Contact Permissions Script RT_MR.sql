/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: CommunityUser Contact Community Permissions Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 4.3 CommunityUser Contact Permissions Script PB.sql
02/27/2019		Ron Tecson			Modified for MapleRoots data migration. 

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE SUPERION_MAPLESB
Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'Profile', 'yes'
Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'Contact', 'yes'
Exec SF_Refresh 'RT_Superion_MAPLEROOTS', 'User', 'yes'

USE Tritech_PROD
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Case', 'yes'


---------------------------------------------------------------------------------
-- DROP STAGING TABLE IF EXISTENT 
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Contact_CommunityPermissions_PreLoad_RT') 
	DROP TABLE Staging_SB_MapleRoots.dbo.[Contact_CommunityPermissions_PreLoad_RT];


---------------------------------------------------------------------------------
-- CREATE CONTACT COMMUNITY PERMISSION STAGING TABLE 
---------------------------------------------------------------------------------

SELECT
	a.ContactID as ID,
	CAST('' as nvarchar(255)) as Error,
	CASE SrcProf.[Name] WHEN 'TriTech Portal Read Only with Tickets' THEN 'Case Access Read Only'
						WHEN 'TriTech Portal Standard User' THEN 'Case Access'
						WHEN 'TriTech Portal Manager' THEN 'Delegated Admin Dedicated'
						ELSE '' END as Community_Permissions__c	
INTO Staging_SB_MapleRoots.dbo.Contact_CommunityPermissions_PreLoad_RT
FROM SUPERION_MAPLESB.dbo.[User] a
LEFT OUTER JOIN TriTech_Prod.dbo.[user] SrcUser ON a.Legacy_Tritech_Id__c = SrcUser.ID										----> Source User
LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] SrcProf ON SrcUser.ProfileId = SrcProf.ID										----> Source Profile
WHERE migrated_record__c = 'true'
		AND SrcProf.[Name] in ('TriTech Portal Read Only with Tickets', 'TriTech Portal Standard User', 'TriTech Portal Manager')

-- 02/27 -- 8251 rows affected 

---------------------------------------------------------------------------------
-- DROP LOAD TABLE IF EXIST IN TARGET
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Contact_CommunityPermissions_Load_RT') 
	DROP TABLE Staging_SB_MapleRoots.dbo.Contact_CommunityPermissions_Load_RT;

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
SELECT *
INTO Staging_SB_MapleRoots.dbo.Contact_CommunityPermissions_Load_RT
FROM Staging_SB_MapleRoots.dbo.Contact_CommunityPermissions_PreLoad_RT

ALTER TABLE Staging_SB_MapleRoots.dbo.Contact_CommunityPermissions_Load_RT
ADD [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

EXEC SF_BulkOps 'update:bulkapi,batchsize(800)', 'RT_Superion_MAPLEROOTS', 'Contact_CommunityPermissions_Load_RT'

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------
SELECT * FROM Contact_CommunityPermissions_Load_RT WHERE error not like '%success%'

SELECT error, COUNT(*)
FROM Contact_CommunityPermissions_Load_RT
GROUP BY error

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE SUPERION_MAPLESB

EXEC SF_Refresh 'RT_Superion_MAPLEROOTS', 'Contact', 'yes'