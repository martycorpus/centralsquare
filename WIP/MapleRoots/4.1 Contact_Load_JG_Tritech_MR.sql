  /*
-- Author       : Jagan	
-- Created Date : 20-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Contact----------
Requirement No  : REQ-0369
Total Records   :  
Scope: - All active Contacts
       - Any contact related to a migrated case.
       - Any contact related to an active community user.
       - Associated with an opportunity that has been closed since January 1, 2016 (active or inactive)
       - Associated to an open opportunity (active or inactive)
       - No inactive contacts unless it meets a prior criteria.
       - Source file will contain duplicate data, all data will be migrate in situ and
	     no deduplication or merging of contact records will occur via the migration.
 */

 /*
Use Tritech_PROD
EXEC SF_Replicate 'MC_TRITECH_PROD','Contact'


Use SUPERION_MAPLESB
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','User'
EXEC SF_Replicate 'SL_SUPERION_MAPLESB','Account'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Contact','Yes'
*/ 

Use Staging_SB_MapleRoots;

--Drop table Staging_SB_MapleRoots.dbo.Contact_Tritech_SFDC_Preload;

DECLARE @Default NVARCHAR(18) = (Select top 1 Id from SUPERION_MAPLESB.dbo.[User] 
where Name like 'Superion API');

DECLARE @Owner NVARCHAR(18) = (Select top 1 Id from SUPERION_MAPLESB.dbo.[User] 
where Id='0056A000000GyGbQAK' and Name='Deborah Solorzano')


 
 ;WITH ContactScope As
 (
  -- All active Contacts

  Select distinct Id as ContactID from Tritech_PROD.dbo.Contact where Inactive_Contact__c='false'

  UNION

  -- Associated with an opportunity that has been closed since January 1, 2016 (active or inactive)
  -- Associated to an open opportunity (active or inactive)

 Select distinct Primary_Quote_Contact__c as ContactID from Tritech_PROD.dbo.Opportunity
 where (IsClosed='false' or(IsClosed='true' and CreatedDate>'2016-01-01 00:00:00.0000000'))
 and Primary_Quote_Contact__c is not null


 -- Any contact related to an active community user.

 UNION

 Select distinct ContactId as ContactID from Tritech_PROD.dbo.[User] where IsActive='true' 
 and ContactId is not null  

 -- Any contact related to a migrated case.

 UNION

 Select  distinct ContactId as ContactID from Tritech_PROD.dbo.[Case] ca
 left join Tritech_PROD.dbo.Account acc on 
 ca.AccountId=acc.Id
 where  
 (IsClosed='false') or (ClosedDate>'2016-01-01 00:00:00.0000000') or (ca.CreatedDate>'2016-01-01 00:00:00.0000000') 
 or
 (acc.Name IN ('Tiburon','Milwaukee Police Department WI','City of Baltimore MD','Inform','Dane County WI',
               'City of San Antonio TX','City of Austin TX','California Highway Patrol (CHP) CA',
               'Zuercher','Rapid City Police Department SD','South Dakota Highway Patrol SD'
			   ,'Pennington County Sheriff�s Office SD','Lafourche County Sheriff�s Office LA'
			   ,'Rock Hill Police Department SC','Douglas County Sheriff�s Office GA'))
 or
 ca.Id in(select distinct Case__c from [Tritech_PROD].dbo.BGIntegration__BomgarSession__c where Case__c is not null)
								
),
CaseRelatedContacts AS
(
 Select  distinct ContactId as ContactID from Tritech_PROD.dbo.[Case] ca
 left join Tritech_PROD.dbo.Account acc on 
 ca.AccountId=acc.Id
 where  
 (IsClosed='false') or (ClosedDate>'2016-01-01 00:00:00.0000000') or (ca.CreatedDate>'2016-01-01 00:00:00.0000000') 
 or
 (acc.Name IN ('Tiburon','Milwaukee Police Department WI','City of Baltimore MD','Inform','Dane County WI',
               'City of San Antonio TX','City of Austin TX','California Highway Patrol (CHP) CA',
               'Zuercher','Rapid City Police Department SD','South Dakota Highway Patrol SD'
			   ,'Pennington County Sheriff�s Office SD','Lafourche County Sheriff�s Office LA'
			   ,'Rock Hill Police Department SC','Douglas County Sheriff�s Office GA'))
 or
 ca.Id in(select distinct Case__c from [Tritech_PROD].dbo.BGIntegration__BomgarSession__c where Case__c is not null)
)


 
 Select 
 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,Account_orig=con.AccountId
,AccountId=IIF(accnt.Id IS NULL,'Account Not Found',accnt.Id)

,Alternate_Email__c_orig=con.Alternate_Email__c
,Alternate_Email__c=IIF(con.Alternate_Email__c IS NULL,con.Alternate_Email__c,CONCAT(con.Alternate_Email__c,'.csfullsb'))
,Billing_Contact__c=con.Billing_Contact__c

,CreatedById_orig=con.CreatedById
,CreatedById_sp=IIF(createdbyuser.Id IS NULL,IIF(us.Isactive='false',@Default,'CreatedBy Not Found'),createdbyuser.Id)
,CreatedByID =IIF(createdbyuser.Id IS NULL,@Default,createdbyuser.Id)

,CreatedDate=con.CreatedDate
,TT_Survey_Contact__c=con.Customer_Survey_Contact__c
,Department=IIF(con.Department IS NULL,'Other',con.Department)
,CaseContactId=CaseRelatedContacts.ContactID
,Department1__c=IIF(CaseRelatedContacts.ContactID IS NULL,NULL,'Other')

,Description_orig=con.Description
,Notes__c_orig=con.Notes__c
,Description=CONCAT(con.Description,CHAR(13)+CHAR(10),con.Notes__c)

,Email_orig=con.Email
,Email=IIF(con.Email IS NULL,con.Email,CONCAT(con.Email,'.csfullsb'))

,Executive_Contact__c_orig=Executive_Contact__c
,Marketing_Contact__c_orig=con.Marketing_Contact__c
,Role_WMP__c_orig=con.Role_WMP__c
--,CASE
-- WHEN Executive_Contact__c='true' THEN 'Top Exec'
-- WHEN con.Marketing_Contact__c='true' THEN 'Marketing � Marketing User'
-- END as Role__c
,CASE
 WHEN con.Executive_Contact__c='True'  THEN 'Director/Chief/Owner'
 WHEN con.Marketing_Contact__c='True' THEN 'Marketing Contact'
 WHEN con.Executive_Contact__c='False' AND con.Marketing_Contact__c='False' THEN con.Role_WMP__c
 END as Role__c
,Extension__c=con.Extension__c
,Fax=con.Fax
,FirstName=con.FirstName
,HasOptedOutOfEmail=con.HasOptedOutOfEmail
,HasOptedOutOfFax=con.HasOptedOutOfFax
,HomePhone=con.HomePhone
,Legacy_Id__c=con.Id

,Inactive_Contact__c_orig=con.Inactive_Contact__c
,CASE
 WHEN con.Inactive_Contact__c='true' THEN 'Inactive'
 WHEN con.Inactive_Contact__c='false' THEN 'Active'
 END as Status__c

,Is_Account_a_Client__c_orig=Is_Account_a_Client__c
,CASE
 WHEN Is_Account_a_Client__c='yes' THEN 'true'
 WHEN Is_Account_a_Client__c='no' THEN 'false'
 END as Install_Contact__c

,Last_Verified_PS__c=Last_Referenced_Date_WMP__c
,LastName=con.LastName
,LeadSource_orig=con.LeadSource
,LeadSource=trnsfrmval.TargetLeadSource
,MailingCity=con.MailingCity
,MailingCountry=con.MailingCountry
,MailingPostalCode=con.MailingPostalCode
,MailingState=con.MailingState
,MailingStreet=con.MailingStreet
,MobilePhone=con.MobilePhone
,Phone_2__c=con.OtherPhone

,OwnerId_orig=con.OwnerId
--,OwnerId_sp=IIF(ownerusr.Id IS NULL,IIF(userr.Isactive='false',@Default,'OwnerId Not Found'),ownerusr.Id)
,OwnerId=@Owner
,Phone=con.Phone

,Prefers_Email__c_orig=Prefers_Email__c
,Prefers_Phone__c_orig=Prefers_Phone__c
,CASE
 WHEN Prefers_Email__c='true' THEN 'Email'
 WHEN Prefers_Phone__c='true' THEN 'Phone'
 END as Preferred_Method_of_Contact__c

,PrimaryContact__c=Primary_Contact_WMP__c
,SupportContact__c=Primary_Support_Contact__c
,Reference__c=Reference_WMP__c

,ReportsToId_orig=con.ReportsToId

,Salutation=con.Salutation

,Support_Contact__c_orig=Support_Contact__c
,After_Hours_Contact_WMP__c_orig=con.After_Hours_Contact_WMP__c
,GIS_Contact__c_orig=con.GIS_Contact__c
,IT_Contact__c_orig=con.IT_Contact__c
,MR_Contact__c_orig=con.MR_Contact__c
,NIBRS_Contact__c_orig=con.NIBRS_Contact__c

--,CASE
-- WHEN Support_Contact__c='true' THEN 'Primary Contact'
-- END as Support_Contact_Type__c

,Support_Contact_Type__c=IIF(CaseRelatedContacts.ContactID IS NOT NULL AND (Support_Contact__c='false' and  After_Hours_Contact_WMP__c='false' and GIS_Contact__c='false' and IT_Contact__c='false' and MR_Contact__c='false' and NIBRS_Contact__c ='false'),
'Case Contact',
 STUFF
(
CONCAT
 (
 IIF(IIF(Support_Contact__c='true','Primary Contact',NULL) IS NULL,NULL,CONCAT(';',IIF(Support_Contact__c='true','Primary Contact',NULL))),
 IIF(IIF(After_Hours_Contact_WMP__c='true','After Hours Contact',NULL) IS NULL,NULL,CONCAT(';',IIF(After_Hours_Contact_WMP__c='true','After Hours Contact',NULL))),
 IIF(IIF(GIS_Contact__c='true','GIS Contact',NULL) IS NULL,NULL,CONCAT(';',IIF(GIS_Contact__c='true','GIS Contact',NULL))),
 IIF(IIF(IT_Contact__c='true','System Admin (SA)',NULL) IS NULL,NULL,CONCAT(';',IIF(IT_Contact__c='true','System Admin (SA)',NULL))),
 IIF(IIF(MR_Contact__c='true','Upgrade Contact',NULL) IS NULL,NULL,CONCAT(';',IIF(MR_Contact__c='true','Upgrade Contact',NULL))),
 IIF(IIF(NIBRS_Contact__c='true','NIBRS Contact',NULL) IS NULL,NULL,CONCAT(';',IIF(NIBRS_Contact__c='true','NIBRS Contact',NULL)))
 ),
 1,
 1,
 ''
)
)

,Title=con.Title

 into Contact_Tritech_SFDC_Preload

from Tritech_PROD.dbo.Contact con

Inner join ContactScope cs on
cs.ContactID=con.Id

--left join Superion_FULLSB.dbo.Contact trgtcntct
--on trgtcntct.Legacy_ID__c=con.Id

--Fetching AccountId (Account Lookup)
left join SUPERION_MAPLESB.dbo.Account accnt on
accnt.LegacySFDCAccountId__c=con.AccountId

--Fetching CreatedById (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=con.CreatedById

--Fetching CreatedById status
left join Tritech_PROD.dbo.[User] us on
us.Id=con.CreatedById

--Fetching OwnerId (User Lookup)
left join SUPERION_MAPLESB.dbo.[User] ownerusr 
on ownerusr.Legacy_Tritech_Id__c=con.OwnerId

--Fetching OwnerId status
left join Tritech_PROD.dbo.[User] userr on
userr.Id=con.OwnerId

left join Staging_SB_MapleRoots.dbo.Tritech_map_Contact_LeadSource trnsfrmval
on trnsfrmval.LegacyLeadSource=con.LeadSource

left join CaseRelatedContacts on
CaseRelatedContacts.ContactID=con.Id
;--82410

----------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.Contact;--87346

Select count(*) from Contact_Tritech_SFDC_Preload;--82410

Select Legacy_Id__c,count(*) from Staging_SB_MapleRoots.dbo.Contact_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB_MapleRoots.dbo.Contact_Tritech_SFDC_Load;

Select * into 
Contact_Tritech_SFDC_Load
from Contact_Tritech_SFDC_Preload; --(82410 row(s) affected)

Select * from Contact_Tritech_SFDC_Load
--where AccountId like'%Not%';


--Exec SF_ColCompare 'Insert','SL_SUPERION_MAPLESB', 'Contact_Tritech_SFDC_Load' 

/*
Salesforce object Contact does not contain column Account_orig
Salesforce object Contact does not contain column Alternate_Email__c_orig
Salesforce object Contact does not contain column CreatedById_orig
Salesforce object Contact does not contain column CreatedById_sp
Salesforce object Contact does not contain column CaseContactId
Salesforce object Contact does not contain column Description_orig
Salesforce object Contact does not contain column Notes__c_orig
Salesforce object Contact does not contain column Email_orig
Salesforce object Contact does not contain column Executive_Contact__c_orig
Salesforce object Contact does not contain column Marketing_Contact__c_orig
Salesforce object Contact does not contain column Role_WMP__c_orig
Salesforce object Contact does not contain column Inactive_Contact__c_orig
Salesforce object Contact does not contain column Is_Account_a_Client__c_orig
Salesforce object Contact does not contain column LeadSource_orig
Salesforce object Contact does not contain column OwnerId_orig
Salesforce object Contact does not contain column Prefers_Email__c_orig
Salesforce object Contact does not contain column Prefers_Phone__c_orig
Salesforce object Contact does not contain column ReportsToId_orig
Salesforce object Contact does not contain column Support_Contact__c_orig
Salesforce object Contact does not contain column After_Hours_Contact_WMP__c_orig
Salesforce object Contact does not contain column GIS_Contact__c_orig
Salesforce object Contact does not contain column IT_Contact__c_orig
Salesforce object Contact does not contain column MR_Contact__c_orig
Salesforce object Contact does not contain column NIBRS_Contact__c_orig
Column SupportContact__c is not insertable into the salesforce object Contact
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_MAPLESB','Contact_Tritech_SFDC_Load' (36:53)

----------------------------------------------------------------------------------------
--drop table Contact_Tritech_SFDC_Load_errors
select * 
into Contact_Tritech_SFDC_Load_errors
from Contact_Tritech_SFDC_Load
where error<>'Operation Successful.'--1838
;

--drop table Contact_Tritech_SFDC_Load_errors_bkp
select * 
into Contact_Tritech_SFDC_Load_errors_bkp
from Contact_Tritech_SFDC_Load
where error<>'Operation Successful.'
;--1838

Select Alternate_Email__c,*
--update a set Alternate_Email__c=null
from Contact_Tritech_SFDC_Load_errors a
where  Error like'Alternate%'--2

--Exec SF_BulkOps 'Insert:batchsize(50)','SL_SUPERION_MAPLESB','Contact_Tritech_SFDC_Load_errors'

select * 
--delete
from Contact_Tritech_SFDC_Load
where error<>'Operation Successful.';--1838

--insert into Contact_Tritech_SFDC_Load
select * from Contact_Tritech_SFDC_Load_errors;--1838

---------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

--Updating Ids for fields having lookup relationship on same object.(ReportsToId)

--Drop table Contact_Tritech_SFDC_UpdateReportsToId;

Select 

 Id=b.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))
,ReportsToId_orig=b.ReportsToId_orig
,ReportsToId=iif(a.Id is null,'ReportsToId Not Found',a.id)
,Legacy_id__c_orig=b.Legacy_id__c

into Contact_Tritech_SFDC_UpdateReportsToId

from Staging_SB_MapleRoots.dbo.Contact_Tritech_SFDC_Load b
left join
Staging_SB_MapleRoots.dbo.Contact_Tritech_SFDC_Load a
on a.Legacy_id__c=b.ReportsToId_orig
where b.ReportsToId_orig IS NOT NULL and b.Error='Operation Successful.';--988

--Exec SF_ColCompare 'Update','SL_SUPERION_MAPLESB', 'Contact_Tritech_SFDC_UpdateReportsToId' 

/*
Salesforce object Contact does not contain column ReportsToId_orig
Salesforce object Contact does not contain column Legacy_id__c_orig
*/

Select * from Contact_Tritech_SFDC_UpdateReportsToId;

--Exec SF_BulkOps 'Update','SL_SUPERION_MAPLESB','Contact_Tritech_SFDC_UpdateReportsToId'



------------------------------------------------------------------------------------------------------

