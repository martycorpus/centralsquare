/*****************************************************************************************************************************************************
REQ #		: REQ-0826
TITLE		: Chatter FeedComment Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.4 Chatter FeedComment Migration PB.sql
02/15/2019		Ron Tecson			Added Refreshes, Added the PreLoad before Load Table.
03/02/2019		Ron Tecson			Modified for the MapleRoots Data Load.

ERROR/FAILURES:

DATE			ERROR																		RESOLUTION
===============	===========================================================================	==================================================================================
03/02/2019		REQUIRED_FIELD_MISSING:Required fields are missing: [FeedItemId]:FeedItemId Needed to update the Id column of the load table after re-running the errors from
																							15.3 Chatter FeedItem Migration RT_MR.sql

DECISIONS:

******************************************************************************************************************************************************/

--------------------------------------------------------------------------------
-- Refresh Data 
--------------------------------------------------------------------------------
USE Tritech_Prod

EXEC SF_Refresh 'MC_Tritech_Prod', 'FeedComment', 'yes';
EXEC SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';

USE SUPERION_MAPLESB
EXEC SF_Refresh 'RT_Superion_MAPLEROOTS', 'User', 'yes';

---------------------------------------------------------------------------------
-- Drop Preload table
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedComment_PreLoad_RT') 
	DROP TABLE Staging_SB_MapleRoots.dbo.FeedComment_PreLoad_RT;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.CommentBody as CommentBody,
	a.CommentType as CommentType,
	a.CreatedByID as CreatedBy_Orig,
	isNull(CreatedBy.ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as CreatedById,
	a.CreatedDate as CreatedDate,
	FeedItem.ID as FeedItemID,
	a.IsRichText as IsRichText,
	a.isVerified as IsVerified,
	a.[Status] as [Status],
	a.ID as Legacy_ID__c
--INTO Staging_SB_MapleRoots.dbo.FeedComment_PreLoad_RT
FROM TriTech_Prod.dbo.FeedComment a
LEFT OUTER JOIN SUPERION_MAPLESB.dbo.[User] CreatedBy ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c
LEFT OUTER JOIN Staging_SB_MapleRoots.dbo.FeedItem_Load_RT FeedItem ON a.FeedItemID = FeedItem.Legacy_Id__c						----> FeedItem
WHERE FeedItem.ID IS NOT NULL

select * from Staging_SB_MapleRoots.dbo.FeedItem_Load_RT

select * from SUPERION_MAPLESB.dbo.[Case] where Id = '5000v000003SK1BAAW'

-- 03/04 -- (19789 rows affected)

-- 03/02 -- (20052 rows affected)

---------------------------------------------------------------------------------
-- Drop Load table
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedComment_Load_RT') 
	DROP TABLE Staging_SB_MapleRoots.dbo.FeedComment_Load_RT;

SELECT *
INTO Staging_SB_MapleRoots.dbo.FeedComment_Load_RT
FROM Staging_SB_MapleRoots.dbo.FeedComment_PreLoad_RT

-- 03/04 -- (19789 rows affected)

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE Staging_SB_MapleRoots.dbo.FeedComment_Load_RT
ADD [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots
EXEC SF_BulkOps 'insert:bulkapi,batchsize(1000)', 'RT_Superion_MAPLEROOTS', 'FeedComment_Load_RT'  

-- 03/04 

--- Starting SF_BulkOps for FeedComment_Load_RT V3.6.7
21:56:46: Run the DBAmp.exe program.
21:56:46: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
21:56:46: Inserting FeedComment_Load_RT (SQL01 / Staging_SB_MapleRoots).
21:56:47: DBAmp is using the SQL Native Client.
21:56:47: Batch size reset to 1000 rows per batch.
21:56:47: Sort column will be used to order input rows.
21:56:47: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
21:56:47: Warning: Column 'CreatedBy_Orig' ignored because it does not exist in the FeedComment object.
21:56:47: Warning: Column 'IsVerified' ignored because it not insertable in the FeedComment object.
21:56:47: Warning: Column 'Legacy_ID__c' ignored because it does not exist in the FeedComment object.
21:56:48: Job 7501h000001xPTyAAM created.
21:56:48: Batch 7511h000001vZFxAAM created with 1000 rows.
21:56:49: Batch 7511h000001vZG2AAM created with 1000 rows.
21:56:49: Batch 7511h000001vZG7AAM created with 1000 rows.
21:56:49: Batch 7511h000001vZGCAA2 created with 1000 rows.
21:56:49: Batch 7511h000001vZGHAA2 created with 1000 rows.
21:56:50: Batch 7511h000001vZGMAA2 created with 1000 rows.
21:56:50: Batch 7511h000001vZGRAA2 created with 1000 rows.
21:56:50: Batch 7511h000001vZGWAA2 created with 1000 rows.
21:56:50: Batch 7511h000001vZGbAAM created with 1000 rows.
21:56:50: Batch 7511h000001vZGgAAM created with 1000 rows.
21:56:51: Batch 7511h000001vZGlAAM created with 1000 rows.
21:56:51: Batch 7511h000001vZGqAAM created with 1000 rows.
21:56:51: Batch 7511h000001vZGvAAM created with 1000 rows.
21:56:51: Batch 7511h000001vZH0AAM created with 1000 rows.
21:56:52: Batch 7511h000001vZH5AAM created with 1000 rows.
21:56:52: Batch 7511h000001vZHAAA2 created with 1000 rows.
21:56:52: Batch 7511h000001vZHFAA2 created with 1000 rows.
21:56:52: Batch 7511h000001vZHKAA2 created with 1000 rows.
21:56:52: Batch 7511h000001vZGhAAM created with 1000 rows.
21:56:52: Batch 7511h000001vZHPAA2 created with 789 rows.
21:56:53: Job submitted.
21:56:53: 19789 rows read from SQL Table.
21:56:54: Job still running.
21:57:09: Job still running.
21:58:10: Job still running.
21:59:11: Job still running.
22:00:11: Job still running.
22:01:12: Job still running.
22:02:13: Job still running.
22:03:13: Job still running.
22:04:14: Job still running.
22:05:15: Job still running.
22:06:16: Job still running.
22:07:16: Job still running.
22:08:17: Job still running.
22:09:18: Job still running.
22:10:18: Job Complete.
22:10:19: DBAmp is using the SQL Native Client.
22:10:21: 19756 rows successfully processed.
22:10:21: 33 rows failed.
22:10:21: Errors occurred. See Error column of row and above messages for more information.
22:10:21: Percent Failed = 0.200.
22:10:21: Error: DBAmp.exe was unsuccessful.
22:10:21: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:bulkapi,batchsize(1000) FeedComment_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 91]
SF_BulkOps Error: 21:56:46: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC21:56:46: Inserting FeedComment_Load_RT (SQL01 / Staging_SB_MapleRoots).21:56:47: DBAmp is using the SQL Native Client.21:56:47: Batch size reset to 1000 rows per batch.21:56:47: Sort column will be used to order input rows.21:56:47: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.21:56:47: Warning: Column 'CreatedBy_Orig' ignored because it does not exist in the FeedComment object.21:56:47: Warning: Column 'IsVerified' ignored because it not insertable in the FeedComment object.21:56:47: Warning: Column 'Legacy_ID__c' ignored because it does not exist in the FeedComment object.21:56:48: Job 7501h000001xPTyAAM created.21:56:48: Batch 7511h000001vZFxAAM created with 1000 rows.21:56:49: Batch 7511h000001vZG2AAM created with 1000 rows.21:56:49: Batch 7511h000001vZG7AAM created with 1000 rows.21:56:49: Batch 7511h000001vZGCAA2 created with 1000 rows.21:56:49: Batch 7511h000001vZGHAA2 created with 1000 rows.21:56:50: Batch 7511h000001vZGMAA2 created with 1000 rows.21:56:50: Batch 7511h000001vZGRAA2 created with 1000 rows.21:56:50: Batch 7511h000001vZGWAA2 created with 1000 rows.21:56:50: Batch 7511h000001vZGbAAM created with 1000 rows.21:56:50: Batch 7511h000001vZGgAAM created with 1000 rows.21:56:51: Batch 7511h000001vZGlAAM created with 1000 rows.21:56:51: Batch 7511h000001vZGqAAM created with 1000 rows.21:56:51: Batch 7511h000001vZGvAAM created with 1000 rows.21:56:51: Batch 7511h000001vZH0AAM created with 1000 rows.21:56:52: Batch 7511h000001vZH5AAM created with 1000 rows.21:56:52: Batch 7511h000001vZHAAA2 created with 1000 rows.21:56:52: Batch 7511h000001vZHFAA2 created with 1000 rows.21:56:52: Batch 7511h000001vZHKAA2 created with 1000 rows.21:56:52: Batch 7511h000001vZGhAAM created with 1000 rows.21:56:52: Batch 7511h000001vZHPAA2 created with 789 rows.21:56:53: Job submitted.21:56:53: 19789 rows read from SQL Table.21:56:54: Job still running.21:57:09: Job still running.21:58:10: Job still running...


EXEC SF_BulkOps 'insert', 'RT_Superion_MAPLEROOTS', 'FeedComment_Load_RT'


-------------------------------
-- Validation
-------------------------------
SELECT ERROR, count(*)
FROM Staging_SB_MapleRoots.dbo.FeedComment_Load_RT
GROUP BY ERROR

SELECT distinct error, feeditemid FROM Staging_SB_MapleRoots.dbo.FeedComment_Load_RT
WHERE ERROR not like '%success%'

SELECT * FROM Staging_SB_MapleRoots.dbo.FeedComment_Load_RT
WHERE ERROR not like '%success%'

SELECT * FROM Staging_SB_MapleRoots.dbo.FeedComment_Load_RT
WHERE ERROR = 'The current field definition for "CommentBody" requires a value to be set.'

SELECT * FROM Staging_SB_MapleRoots.dbo.FeedComment_Load_RT
WHERE ERROR not like '%success%'

-------------------------------
-- Validation
-------------------------------
USE SUPERION_MAPLESB
EXEC SF_Refresh 'RT_Superion_MAPLEROOTS', 'FeedComment', 'yes';