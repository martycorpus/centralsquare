/*
-- Author     : Shivani Mogullapalli
-- Date       : 14/02/2019
-- Description: Migrate the Task data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0380
Scope:
Migrate all tasks that relate to a migrated record on a migrated object and migrated record and IS NOT owned by ActOnSync.
Note: Turn off Process flows
*/

/*
Use Tritech_PROD
EXEC SF_ReplicateIAD 'MC_TRITECH_PROD','task' --,'Yes' --30min
EXEC SF_Replicate 'MC_TRITECH_PROD','EmailMessage'--24 mins


Use SUPERION_MAPLESB
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Task','yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Opportunity','yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Contact','yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Account','yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Case','yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Sales_Request__c','yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Procurement_Activity__c','yes'
EXEC SF_Refresh 'SL_SUPERION_MAPLESB','Contact_Reference_Link__c','yes'
*/--10mins
-- select count(*) from tritech_prod.dbo.[Task]
--1691911


use Staging_SB_MapleRoots
go
-- drop table Task_Tritech_SFDC_Preload
 declare @defaultuser nvarchar(18)
 =(select  id from SUPERION_MAPLESB.dbo.[user] where name = 'Superion API')
;With CteWhatData as
(
select 
a.LegacySFDCAccountId__c  Legacy_id_orig,
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Account' as What_parent_object_name 
from SUPERION_MAPLESB.dbo.Account a
where 1=1 
and a.LegacySFDCAccountId__c is not null

						union
select
a.Legacy_Opportunity_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Opportunity' as What_parent_object_name 
from SUPERION_MAPLESB.dbo.Opportunity a
where 1=1
and a.Legacy_Opportunity_ID__c is not null and migrated_record__C='true'

						union 
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Case' as What_parent_object_name 
from SUPERION_MAPLESB.dbo.[case] a
where 1=1
and a.Legacy_ID__c is not null and migrated_record__C='true'
						union 
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from SUPERION_MAPLESB.dbo.Sales_Request__c a
where 1=1
and a.Legacy_Record_ID__c is not null
			union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Procurement_Activity__c' as What_parent_object_name 
from SUPERION_MAPLESB.dbo.Procurement_Activity__c a
where 1=1
and a.Legacy_ID__c is not null
				union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact_Reference_Link__c' as What_parent_object_name 
from SUPERION_MAPLESB.dbo.Contact_Reference_Link__c a
where 1=1
and a.Legacy_ID__c is not null

) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from SUPERION_MAPLESB.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null	 and migrated_record__C='true'			
 )   

SELECT  
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_task.Id
				,Legacy_Source__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_task.[Account_Name__c]
														  --,tr_task.[Account_Name_DE__c]
				,AccountId_orig							= tr_task.[AccountId]
				,AccountId								= Tar_ACcount.ID
				,Activity_Type_For_Reporting__c			= tr_task.[Activity_Type_For_Reporting__c]
				,ActivityDate							= tr_task.[ActivityDate]
														  --,tr_task.[Brand__c]
				,CallDisposition						= tr_task.[CallDisposition]
				,CallDurationInSeconds					= tr_task.[CallDurationInSeconds]
				,CallObject								= tr_task.[CallObject]
				,CallType								= tr_task.[CallType]
														  --,tr_task.[Completed_Date_Time__c]
														  --,tr_task.[Contact_Count__c]
				,CreatedById_orig						= tr_task.[CreatedById]
				,legacy_createdby_name=Legacyuser.name
				,CreatedById_target							= Target_Create.ID
				,CreatedById							= iif(Target_Create.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_task.[CreatedDate]
														 -- ,tr_task.[Customer_Risk_Level__c]
				--,Description_orig							= tr_task.[Description]
														  --,tr_task.[DummyTouchField__c]
				,Id_orig								= tr_task.[Id]
				,Isarchieved__orig = tr_task.[IsArchived]
				,IsClosed_orig=tr_task.[IsClosed]
			    ,IsDeleted_orig=    tr_task.[IsDeleted]
														  --,tr_task.[IsHighPriority]
				,IsRecurrence							= tr_task.[IsRecurrence]
				,IsReminderSet							= tr_task.[IsReminderSet]
														  --,tr_task.[LastModifiedById]
														  --,tr_task.[LastModifiedDate]
				,OwnerId_orig							= tr_task.[OwnerId]
				,legacy_owner_name=Legacyuser1.name
				,OwnerId_target							= Target_Owner.ID
				,OwnerId							    = iif(Target_owner.id is null,@defaultuser
																		--iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,Priority								= tr_task.[Priority]
				,RecurrenceActivityId_orig				= tr_task.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_task.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_task.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_task.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_task.[RecurrenceInstance]
				,RecurrenceInterval						= tr_task.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_task.[RecurrenceMonthOfYear]
				,RecurrenceRegeneratedType				= tr_task.[RecurrenceRegeneratedType]
				,RecurrenceStartDateOnly				= tr_task.[RecurrenceStartDateOnly]
				,RecurrenceTimeZoneSidKey				= tr_task.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_task.[RecurrenceType]
				,ReminderDateTime						= tr_task.[ReminderDateTime]
				,Resolution_Notes__c					= tr_task.[Resolution_Notes__c]
														  --,tr_task.[Solution_s__c]
				,Status									= tr_task.[Status]
				,Subject								= tr_task.[Subject]
														  --,tr_task.[SystemModstamp]
				--,Description							= iif(tr_task.[Task_Notes__c] is not null,concat(tr_task.[Createddate],tr_task.[Task_Notes__c]),null)
				,TaskSubtype							= tr_task.[TaskSubtype]
														 -- ,tr_task.[Time_Elapsed__c]
				,Type									= tr_task.[Type]
														  --,tr_task.[WhatCount]
				,whatId_orig							= tr_task.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
				    
														  --,tr_task.[WhoCount]
				,WhoID_orig								= tr_task.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_task.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_task.[Description]
				,Description_Comments_summary__C_orig = tr_task.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_task.[Task_Notes__c]
				--,[Description]  = concat( iif(tr_task.[Description] is not null,'Description::',''),tr_task.description,CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_task.createddate),''),tr_task.[Comments_Summary__c],CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_task.createddate),''),tr_task.[Task_Notes__c]
				--						  )
				--,DESCRIPTION = IIF(tr_task.Description IS NOT NULL
				--							,IIF(tr_task.tASK_nOTES__c IS NOT NULL, 
				--								CONCAT(tr_task.DESCRIPTION,CHAR(13)+char(10),tr_task.CREATEDDATE,' ',tr_task.task_notes__c),tr_task.description),null)
				,DESCRIPTION=concat(isnull(tr_task.Description,''),
				iif(tr_task.Task_Notes__c IS NOT NULL,concat(CHAR(13)+char(10)+cast(tr_task.CREATEDDATE as nvarchar(40))+' ',
				cast(tr_task.Task_Notes__c as nvarchar(max))),''))
               -- ,tr_email_id=tr_email.id
							into Task_Tritech_SFDC_Preload
  FROM [Tritech_PROD].[dbo].[Task] tr_task
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_task.WhatId
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_task.WhoId
  -----------------------------------------------------------------------------------------------------------------------
    left join SUPERION_MAPLESB.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_task.AccountId
--and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join SUPERION_MAPLESB.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_task.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join SUPERION_MAPLESB.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_task.OwnerID
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_task.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_task.OwnerId
    -----------------------------------------------------------------------------------------------------------------------------
   where 1=1
  and  tr_task.IsDeleted='False' and Legacyuser1.name <>'Act-On Sync'
  and (Wt.parent_id IS NOT NULL or Wo.parent_id IS NOT NULL or Tar_Account.ID IS NOT NULL)
  and (tr_task.IsArchived='false' or Wt.parent_id like '500%')
 ;

  --(272139 row(s) affected) --3 mins

 -- COUNT OF THE TASK RECORDS
select count(*) 
from Task_Tritech_SFDC_Preload
--   272139 row(s) affected)

select count(*) from tritech_prod.dbo.task;--1691911




select -- CHECK FOR THE DUPLICATES.Legacy_ID__c,Count(*) from Task_Tritech_SFDC_Preload
group by Legacy_ID__c
having count(*)>1;--0

--drop table Task_Tritech_SFDC_load
select * 
into Task_Tritech_SFDC_load
from Task_Tritech_SFDC_Preload
where 1=1
 and (whoid is not null or whatid is not null);
 
--(270614 row(s) affected)

select * 
from Task_Tritech_SFDC_load;

-- LOAD TABLE RECORD COUNT
select count(*) 
from Task_Tritech_SFDC_load
--270614

-- check for the duplicates in the final load table.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_load
group by Legacy_ID__c
having count(*)>1


select * from Task_Tritech_SFDC_load where Legacy_id__c='00T80000071RFAJEA4';

--: Run batch program to create task
  
--exec SF_ColCompare 'Insert','SL_SUPERION_MAPLESB', 'Task_Tritech_SFDC_load' 

--check column names
/*

Salesforce object Task does not contain column AccountId_orig
Salesforce object Task does not contain column CreatedById_orig
Salesforce object Task does not contain column legacy_createdby_name
Salesforce object Task does not contain column CreatedById_target
Salesforce object Task does not contain column Id_orig
Salesforce object Task does not contain column Isarchieved__orig
Salesforce object Task does not contain column IsClosed_orig
Salesforce object Task does not contain column IsDeleted_orig
Salesforce object Task does not contain column OwnerId_orig
Salesforce object Task does not contain column legacy_owner_name
Salesforce object Task does not contain column OwnerId_target
Salesforce object Task does not contain column RecurrenceActivityId_orig
Salesforce object Task does not contain column whatId_orig
Salesforce object Task does not contain column whatid_parent_object_name
Salesforce object Task does not contain column whatid_legacy_id_orig
Salesforce object Task does not contain column WhoID_orig
Salesforce object Task does not contain column whoId_parent_object_name
Salesforce object Task does not contain column whoId_legacy_id_orig
Salesforce object Task does not contain column Logged_Wellness_Check__c
Salesforce object Task does not contain column Description__orig
Salesforce object Task does not contain column Description_Comments_summary__C_orig
Salesforce object Task does not contain column Description_Task_notes__C_orig
Column AccountId is not insertable into the salesforce object Task
*/



--drop table Task_Tritech_SFDC_load_with_Series
select *, ntile(5) over(order by whatid,whoid) series
into Task_Tritech_SFDC_load_with_Series
from Task_Tritech_SFDC_load a
--(270614 row(s) affected)
-- 2 mins
-------------------------------------------------------------------------------------------------------

Select Series,Count(*) from Task_Tritech_SFDC_load_with_Series
group by Series

-------------------------------------------------------------------------------------------------------

--Series1
-- drop table Task_Tritech_SFDC_load_with_Series_1
select * into Task_Tritech_SFDC_load_with_Series_1 from Task_Tritech_SFDC_load_with_Series
where series=1;--53918

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_1' ;--70 mins

select error,count(*) from Task_Tritech_SFDC_load_with_Series_1
group by error
;
--drop table Task_Tritech_SFDC_load_with_Series_1_errors
select *
into Task_Tritech_SFDC_load_with_Series_1_errors
from Task_Tritech_SFDC_load_with_Series_1
where error<>'Operation Successful.';
--(4607 row(s) affected)
-- drop table Task_Tritech_SFDC_load_with_Series_1_errors_bkp
select *
into Task_Tritech_SFDC_load_with_Series_1_errors_bkp
from Task_Tritech_SFDC_load_with_Series_1
where error<>'Operation Successful.'

select * from Task_Tritech_SFDC_load_with_Series_1_errors;

--Exec SF_BulkOps 'Insert:batchsize(5)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_1_errors' ;-- 7 :30 mins


--drop table Task_Tritech_SFDC_load_with_Series_1_errors2
select *
into Task_Tritech_SFDC_load_with_Series_1_errors2
from Task_Tritech_SFDC_load_with_Series_1_errors
where error<>'Operation Successful.';

--(6 row(s) affected)

select * 
 -- update a set ActivityDate='2099-01-01 00:00:00.0000000'
 from Task_Tritech_SFDC_load_with_Series_1_errors2 a where error like 'Due Date Only: invalid date:%'
 --(1 row(s) affected)

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_1_errors2' ;--0.04 mins

select *
--delete  
from Task_Tritech_SFDC_load_with_Series_1
where error<>'Operation Successful.';

--insert into Task_Tritech_SFDC_load_with_Series_1
select *
from Task_Tritech_SFDC_load_with_Series_1_errors
where error='Operation Successful.';
--union
--insert into Task_Tritech_SFDC_load_with_Series_1
select *
from Task_Tritech_SFDC_load_with_Series_1_errors2


-------------------------------------------------------------------------------------------------------

--Series2
-- drop table Task_Tritech_SFDC_load_with_Series_2
select * into Task_Tritech_SFDC_load_with_Series_2 from Task_Tritech_SFDC_load_with_Series
where series=2;--52729

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_2' --2 hours

select error,count(*) from Task_Tritech_SFDC_load_with_Series_2
group by error;

-- drop table Task_Tritech_SFDC_load_with_Series_2_errors
select *
into Task_Tritech_SFDC_load_with_Series_2_errors
from Task_Tritech_SFDC_load_with_Series_2
where error<>'Operation Successful.';
--(3984 row(s) affected)

--drop table Task_Tritech_SFDC_load_with_Series_2_errors_bkp
select *
into Task_Tritech_SFDC_load_with_Series_2_errors_bkp
from Task_Tritech_SFDC_load_with_Series_2
where error<>'Operation Successful.'

select * from Task_Tritech_SFDC_load_with_Series_2_errors;

select substring(error,1,20),count(*) from Task_Tritech_SFDC_load_with_Series_2_errors
group by substring(error,1,20);

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_2_errors' ;-- mins

select *
--delete  
from Task_Tritech_SFDC_load_with_Series_2
where error<>'Operation Successful.';--(3984 row(s) affected)

--insert into Task_Tritech_SFDC_load_with_Series_2
select *
from Task_Tritech_SFDC_load_with_Series_2_errors
;--(3984 row(s) affected)


-------------------------------------------------------------------------------------------------------

--Series3
-- drop table Task_Tritech_SFDC_load_with_Series_3
select * into Task_Tritech_SFDC_load_with_Series_3 from Task_Tritech_SFDC_load_with_Series
where series=3

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_3' --2hours

select substring(error,1,20),count(*) from Task_Tritech_SFDC_load_with_Series_3
group by substring(error,1,20);

-- drop table Task_Tritech_SFDC_load_with_Series_3_errors
select *
into Task_Tritech_SFDC_load_with_Series_3_errors
from Task_Tritech_SFDC_load_with_Series_3
where error<>'Operation Successful.';
--(3390 row(s) affected)

-- drop table Task_Tritech_SFDC_load_with_Series_3_errors_bkp
select *
into Task_Tritech_SFDC_load_with_Series_3_errors_bkp
from Task_Tritech_SFDC_load_with_Series_3
where error<>'Operation Successful.'

--(3390 row(s) affected)

select * from Task_Tritech_SFDC_load_with_Series_3_errors;

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_3_errors' ;-- mins

select *
--delete  
from Task_Tritech_SFDC_load_with_Series_3
where error<>'Operation Successful.';--(3390 row(s) affected)


--insert into Task_Tritech_SFDC_load_with_Series_3
select *
from Task_Tritech_SFDC_load_with_Series_3_errors
;--(3390 row(s) affected)


-------------------------------------------------------------------------------------------------------

--Series4
-- drop table Task_Tritech_SFDC_load_with_Series_4
select * into Task_Tritech_SFDC_load_with_Series_4 from Task_Tritech_SFDC_load_with_Series
where series=4

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_4'

select substring(error,1,20),count(*) from Task_Tritech_SFDC_load_with_Series_4
group by substring(error,1,20)

-- drop table Task_Tritech_SFDC_load_with_Series_4_errors
select *
into Task_Tritech_SFDC_load_with_Series_4_errors
from Task_Tritech_SFDC_load_with_Series_4
where error<>'Operation Successful.';
--(2970 row(s) affected)

-- drop table Task_Tritech_SFDC_load_with_Series_4_errors_bkp
select *
into Task_Tritech_SFDC_load_with_Series_4_errors_bkp
from Task_Tritech_SFDC_load_with_Series_4
where error<>'Operation Successful.'
--(2970 row(s) affected)

select * from Task_Tritech_SFDC_load_with_Series_4_errors;

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_4_errors' ;-- mins


select *
--delete  
from Task_Tritech_SFDC_load_with_Series_4
where error<>'Operation Successful.';--(2970 row(s) affected)


--insert into Task_Tritech_SFDC_load_with_Series_4
select *
from Task_Tritech_SFDC_load_with_Series_4_errors
;--(2970 row(s) affected)



-------------------------------------------------------------------------------------------------------

--Series5
-- drop table Task_Tritech_SFDC_load_with_Series_5
select * into Task_Tritech_SFDC_load_with_Series_5 from Task_Tritech_SFDC_load_with_Series
where series=5

--Exec SF_BulkOps 'Insert:batchsize(98)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_5'

select substring(error,1,20),count(*) from Task_Tritech_SFDC_load_with_Series_5
group by  substring(error,1,20)

-- drop table Task_Tritech_SFDC_load_with_Series_5_errors
select *
into Task_Tritech_SFDC_load_with_Series_5_errors
from Task_Tritech_SFDC_load_with_Series_5
where error<>'Operation Successful.';
--(2495 row(s) affected)

-- drop table Task_Tritech_SFDC_load_with_Series_5_errors_bkp
select *
into Task_Tritech_SFDC_load_with_Series_5_errors_bkp
from Task_Tritech_SFDC_load_with_Series_5
where error<>'Operation Successful.'

--(2495 row(s) affected)
select * from Task_Tritech_SFDC_load_with_Series_5_errors;

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_load_with_Series_5_errors' ;-- mins

select *
--delete  
from Task_Tritech_SFDC_load_with_Series_5
where error<>'Operation Successful.';--(2495 row(s) affected)

--insert into Task_Tritech_SFDC_load_with_Series_5
select *
from Task_Tritech_SFDC_load_with_Series_5_errors
;--(2495 row(s) affected)


-------------------------------------------------------------------------------------------------------


select error,count(*) from Task_Tritech_SFDC_load_with_Series_1
group by error;
select error,count(*) from Task_Tritech_SFDC_load_with_Series_2
group by error;
select error,count(*) from Task_Tritech_SFDC_load_with_Series_3
group by error;
select error,count(*) from Task_Tritech_SFDC_load_with_Series_4
group by error;
select error,count(*) from Task_Tritech_SFDC_load_with_Series_5
group by error;


-------------------
select * 
--update a set a.id=b.id,a.error=b.error
from Task_Tritech_SFDC_load a --269589
,(select id,error,legacy_id__C from Task_Tritech_SFDC_load_with_Series_1
union
select id,error,legacy_id__C from Task_Tritech_SFDC_load_with_Series_2
union
select id,error,legacy_id__C from Task_Tritech_SFDC_load_with_Series_3
union
select id,error,legacy_id__C from Task_Tritech_SFDC_load_with_Series_4
union
select id,error,legacy_id__C from Task_Tritech_SFDC_load_with_Series_5
) b
where a.Legacy_id__c=b.Legacy_id__c
;
--(270614 row(s) affected)
-----------------------------------------------

select count(*) from Task_Tritech_SFDC_load 
where error<>'Operation Successful.'

--0 


--drop table Task_Tritech_SFDC_Delete_recurrence_tasks
select a.id,error=cast(space(255) as nvarchar(255)),Legacy_id__c_orig=a.Legacy_id__c 
into Task_Tritech_SFDC_Delete_recurrence_tasks
from SUPERION_MAPLESB.dbo.[task] a
left outer join Task_Tritech_SFDC_load b
on a.id=b.id
where a.Migrated_Record__c='true'
and b.id is null;
--(380 row(s) affected)


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_SUPERION_MAPLESB','Task_Tritech_SFDC_Delete_recurrence_tasks'
-------------------------------------------------------------------------------------------------------

select legacy_id__c,count(*) from Task_Tritech_SFDC_load
group by Legacy_id__c
having count(*)>1



select legacy_id__c ,count(*) from SUPERION_MAPLESB.dbo.task where Migrated_Record__c='true'
group by Legacy_Id__c
having count(*)>1




