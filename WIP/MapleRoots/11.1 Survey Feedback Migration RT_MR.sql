/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Survey Feedback Object Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 11.1 Survey Feedback Migration PB.sql
03/01/2019		Ron Tecson			Modified for the MapleRoots Data Load.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data in the Local Database
---------------------------------------------------------------------------------

USE Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_Prod', 'Account', 'yes' ; -- 1 min
EXEC SF_Refresh 'MC_Tritech_Prod', 'Contact', 'yes' ; -- 1 min

USE SUPERION_MAPLESB
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Contact', 'yes' 
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'RecordType', 'yes' ;
EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Account', 'yes' ;

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
Use Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Survey_Feedback__c_PreLoad_RT') 
DROP TABLE Survey_Feedback__c_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots;

SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.[ID] as Legacy_Id__c,
	'TriTech' as Legacy_Source_System__c,
	'true' as Migrated_Record__c,
	RT.ID as RecordTypeID,
	a.NPS_Score__c as Legacy_NPS_Score__c,
	a.NPS_Survey_Contact__c as NPS_Survey_Contact_Original,
	Cont.ID as Contact2__c,
	a.Survey_Notes__c as Customer_Comments__c,
	a.ID as Account_Original__c,
	Acct.ID as Account__c,
	a.Survey_Date__c as CreatedDate
INTO Survey_Feedback__c_PreLoad_RT
FROM Tritech_PROD.dbo.Account a
LEFT OUTER JOIN SUPERION_MAPLESB.dbo.RecordType RT on 'NPS' = RT.[Name]							----> Record Type
LEFT OUTER JOIN SUPERION_MAPLESB.dbo.Contact Cont ON a.NPS_Survey_Contact__c = Cont.Legacy_ID__c ----> Contact
LEFT OUTER JOIN SUPERION_MAPLESB.dbo.Account Acct ON a.ID = Acct.LegacySFDCAccountId__c			----> Account
WHERE a.NPS_Score__c is not null

-- 3/1 -- (2850 rows affected)
-- 3/19 -- (2850 rows affected)

---------------------------------------------------------------------------------
-- Drop Load tables if exist
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Survey_Feedback__c_Load_RT') 
DROP TABLE Survey_Feedback__c_Load_RT;

SELECT *
INTO Staging_SB_MapleRoots.dbo.Survey_Feedback__c_Load_RT
FROM Staging_SB_MapleRoots.dbo.Survey_Feedback__c_PreLoad_RT

-- 3/1 -- (2850 rows affected)
-- 3/19 -- (2850 rows affected)

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE  Staging_SB_MapleRoots.dbo.Survey_Feedback__c_Load_RT
ADD [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB_MapleRoots

EXEC SF_ColCompare 'INSERT','RT_SUPERION_MAPLEROOTS', 'Survey_Feedback__c_Load_RT' 

/*****************************************************************************************************************************************************

--3/1/2019
--- Starting SF_ColCompare V3.6.7
Problems found with Survey_Feedback__c_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 90]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Survey_Feedback__c does not contain column NPS_Survey_Contact_Original
Salesforce object Survey_Feedback__c does not contain column Account_Original__c
Salesforce object Survey_Feedback__c does not contain column Sort

*****************************************************************************************************************************************************/


EXEC SF_BulkOps 'upsert:bulkapi,batchsize(5000)', 'RT_SUPERION_MAPLEROOTS', 'Survey_Feedback__c_Load_RT', 'Legacy_Id__c';

/*****************************************************************************************************************************************************

-- 3/19
--- Starting SF_BulkOps for Survey_Feedback__c_Load_RT V3.6.7
16:49:52: Run the DBAmp.exe program.
16:49:52: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
16:49:52: Upserting Salesforce using Survey_Feedback__c_Load_RT (SQL01 / Staging_SB_MapleRoots) .
16:49:53: DBAmp is using the SQL Native Client.
16:49:53: Batch size reset to 5000 rows per batch.
16:49:54: Sort column will be used to order input rows.
16:49:54: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
16:49:55: Warning: Column 'NPS_Survey_Contact_Original' ignored because it does not exist in the Survey_Feedback__c object.
16:49:55: Warning: Column 'Account_Original__c' ignored because it does not exist in the Survey_Feedback__c object.
16:49:55: Job 7500r000000034iAAA created.
16:49:56: Batch 7510r00000005zCAAQ created with 2850 rows.
16:49:56: Job submitted.
16:49:56: 2850 rows read from SQL Table.
16:49:56: Job still running.
16:50:11: Job still running.
16:51:11: Job Complete.
16:51:12: DBAmp is using the SQL Native Client.
16:51:12: 2848 rows successfully processed.
16:51:12: 2 rows failed.
16:51:12: Errors occurred. See Error column of row and above messages for more information.
16:51:12: Percent Failed = 0.100.
16:51:12: Error: DBAmp.exe was unsuccessful.
16:51:12: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe upsert:bulkapi,batchsize(5000) Survey_Feedback__c_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_SUPERION_MAPLEROOTS"  "Legacy_Id__c"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 109]
SF_BulkOps Error: 16:49:52: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC16:49:52: Upserting Salesforce using Survey_Feedback__c_Load_RT (SQL01 / Staging_SB_MapleRoots) .16:49:53: DBAmp is using the SQL Native Client.16:49:53: Batch size reset to 5000 rows per batch.16:49:54: Sort column will be used to order input rows.16:49:54: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.16:49:55: Warning: Column 'NPS_Survey_Contact_Original' ignored because it does not exist in the Survey_Feedback__c object.16:49:55: Warning: Column 'Account_Original__c' ignored because it does not exist in the Survey_Feedback__c object.16:49:55: Job 7500r000000034iAAA created.16:49:56: Batch 7510r00000005zCAAQ created with 2850 rows.16:49:56: Job submitted.16:49:56: 2850 rows read from SQL Table.16:49:56: Job still running.16:50:11: Job still running.16:51:11: Job Complete.16:51:12: DBAmp is using the SQL Native Client.16:51:12: 2848 rows successfully processed.16:51:12: 2 rows failed.16:51:12: Errors occurred. See Error column of row and above messages for more information.


--3/1/2019
--- Starting SF_BulkOps for Survey_Feedback__c_Load_RT V3.6.7
05:55:36: Run the DBAmp.exe program.
05:55:36: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LL
05:55:36: Upserting Salesforce using Survey_Feedback__c_Load_RT (SQL01 / Staging_SB_MapleRoots) .
05:55:36: DBAmp is using the SQL Native Client.
05:55:36: Batch size reset to 5000 rows per batch.
05:55:37: Sort column will be used to order input rows.
05:55:37: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
05:55:37: Warning: Column 'NPS_Survey_Contact_Original' ignored because it does not exist in the Survey_Feedback__c object.
05:55:37: Warning: Column 'Account_Original__c' ignored because it does not exist in the Survey_Feedback__c object.
05:55:37: Job 7501h000001xCNsAAM created.
05:55:37: Batch 7511h000001vKMKAA2 created with 2850 rows.
05:55:37: Job submitted.
05:55:37: 2850 rows read from SQL Table.
05:55:37: Job still running.
05:55:52: Job still running.
05:56:52: Job Complete.
05:56:53: DBAmp is using the SQL Native Client.
05:56:53: 2850 rows successfully processed.
05:56:53: 0 rows failed.
05:56:53: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


*****************************************************************************************************************************************************/


---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------
SELECT ERROR, count(*)
FROM Survey_Feedback__c_Load_RT
GROUP BY ERROR

select * from Survey_Feedback__c_Load_RT where error not like '%success%'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE SUPERION_MAPLESB

EXEC SF_Refresh 'RT_SUPERION_MAPLEROOTS', 'Survey_Feedback__c', 'yes'
