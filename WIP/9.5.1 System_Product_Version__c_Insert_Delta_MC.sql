-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 9.5.1 System_Product_Version__c_Insert_Delta_MC.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--- Copyright Apps Associates 2018
--- Created Date: Feb 1, 2019
--- Last Updated: 
--- Change Log: 
--- 
--- 
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-02-xx Executed in Superion FullSbx.
-- (xxx row(s) affected)
/*

*/
---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_PROD;

Exec SF_Refresh 'MC_TRITECH_PROD','Hardware_Software__c','Yes'

Use Superion_FULLSB;

Exec SF_Refresh 'MC_SUPERION_FULLSB','CUSTOMER_ASSET__C','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Registered_Product__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Environment__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Version__c','Yes'


---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_SB;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_Delta_MC') 
DROP TABLE Staging_SB.dbo.System_Product_Version__c_Insert_Delta_MC;

---------------------------------------------------------------------------------
-- Create System_Product_Version__c Staging Load Table
---------------------------------------------------------------------------------

--Cte
WITH CTE_SUP_Environment As
(
SELECT Id, Name, Type__c, Account__c from Superion_FULLSB.dbo.Environment__c
),
CTE_Zuercher_Customer_assets as
(
SELECT t1.id as Zuercher_Registered_Product_Id,
       t1.name as Zuercher_Registered_Product_Name,
       t2.id   AS CustomerAsset__c,
       t2.name AS CustomerAsset_Name,
       t2.product_group__c as CUSTOMER_ASSET_Product_Group,
       p2.product_group__c,
       p2.product_line__c,
       t1.Account__c,
       a.Name as accountname
FROM   superion_fullsb.dbo.REGISTERED_PRODUCT__C t1
        LEFT JOIN superion_fullsb.dbo.PRODUCT2 p2
        ON p2.id = t1.product__c
       join superion_fullsb.dbo.CUSTOMER_ASSET__C t2
         ON t2.id = t1.customerasset__c
       left join  superion_fullsb.dbo.account a on a.Id = t1.Account__c 
WHERE  t2.product_group__c = 'Zuercher' 
),
Cte_version as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, t1.ProductLine__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_FULLSB.dbo.version__c t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
),
Cte_version_MajorRelease as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, 
t1.ProductLine__c, t1.LegacyMajorReleaseVersion__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_FULLSB.dbo.version__c t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
and t1.LegacyMajorReleaseVersion__c is not null
and t1.legacyttzsource__c = 'Version reconciliation' and t1.LegacyMajorReleaseVersion__c = '1.1'
),
Cte_version_2 as
(
SELECT Row_number( )
         over (
           PARTITION BY t1.versionnumber__c
           ORDER BY (CASE WHEN t1.legacyttzsource__c = 'CS Import File' THEN 1 WHEN t1.legacyttzsource__c = 'Hardware Software' THEN 2 ELSE 3 END) ) AS "RANK",
       t1.id,
       t1.ReplaceexistingwiththisVersion__c,
       t2.name as ReplaceexistingwiththisVersion_Name,       
       t1.name,
       t1.productline__c,
       t1.legacyttzversion__c,
       t1.legacyttzproductgroup__c,
       t1.product_group__c,
       t1.versionnumber__c,
       t1.legacyttzsource__c,
       t1.current__c
FROM   superion_fullsb.dbo.VERSION__C t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
WHERE  t1.legacyttzproductgroup__c = 'IMC'  OR
       t1.product_group__c = 'IMC' 
),
CTE_TT_HS -- TT Hardware Software CTE
AS
(
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          t3.id                      AS account__c,
          t3.name                    AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.cim_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.cim_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.cim_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.cim_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.cim_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.fbr_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.fbr_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.fbr_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.fbr_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.fbr_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.imc_version__c  AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMC_Version__c '
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.imc_version__c IS NOT NULL
UNION
SELECT    t1.id                   AS tt_legacy_id_hs,
          t1.name                 AS tt_hs_name,
          t1.account_name__c      AS tt_legacysfdcaccountid__c,
          t3.id                   AS account__c,
          t3.name                 AS account_name,
          t1.inform_me_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Inform_Me_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.inform_me_version__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.iq_version__c   AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IQ_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.iq_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.jail_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Jail_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.jail_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.rms_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.rms_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.rms_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.ttms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'TTMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.ttms_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_cad_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_CAD_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_cad_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_fbr_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_FBR_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fbr_software_version__c IS NOT NULL
UNION
SELECT    t1.id                          AS tt_legacy_id_hs,
          t1.name                        AS tt_hs_name,
          t1.account_name__c             AS tt_legacysfdcaccountid__c,
          t3.id                          AS account__c,
          t3.name                        AS account_name,
          t1.va_fire_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Fire_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fire_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_inform_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Inform_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_inform_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_mobile_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Mobile_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_mobile_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_rms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_RMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_rms_software_version__c IS NOT NULL
UNION
SELECT    t1.id               AS tt_legacy_id_hs,
          t1.name             AS tt_hs_name,
          t1.account_name__c  AS tt_legacysfdcaccountid__c,
          t3.id               AS account__c,
          t3.name             AS account_name,
          t1.visicad_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_patch__c IS NOT NULL
UNION
SELECT    t1.id                 AS tt_legacy_id_hs,
          t1.name               AS tt_hs_name,
          t1.account_name__c    AS tt_legacysfdcaccountid__c,
          t3.id                 AS account__c,
          t3.name               AS account_name,
          t1.visicad_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_production_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Production_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_production_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_mobile_test_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_mobile_test_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_training_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Training_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_training_version__c IS NOT NULL
UNION
SELECT    t1.id                        AS tt_legacy_id_hs,
          t1.name                      AS tt_hs_name,
          t1.account_name__c           AS tt_legacysfdcaccountid__c,
          t3.id                        AS account__c,
          t3.name                      AS account_name,
          t1.visinet_mobile_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_version__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_test_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_test_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_version__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.visinet_training_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_training_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.x911_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'X911_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.x911_software_version__c IS NOT NULL
)
SELECT DISTINCT Cast (null as nchar(18)) as ID,
                Cast (null as nvarchar(255)) as Error,
                COALESCE( regprd.id, 'ID NOT FOUND' )   AS Registered_Product__c,
                COALESCE( regprd.NAME, 'REG PROD NOT FOUND' )       AS [Registered Product Name],
                COALESCE( COALESCE(cte_env.id,cte_env2.id), 'ID NOT FOUND' )       AS Environment__c,
                COALESCE( coalesce(cte_env.NAME,cte_env2.name), 'ENVIRONMENT NOT FOUND' )                                                                                                                                                 AS [Environment Name],
                CASE 
                   WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
                   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id )
                   WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id) is not null then COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id)
                   else 'Id not found'
                   end as Version__c,
               -- COALESCE( COALESCE( COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ), COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) ), 'ID NOT FOUND' )              AS Version__c,
                --COALESCE(cte_vers.Id,'ID NOT FOUND') AS 	Version__c,
                --COALESCE( COALESCE( COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ), COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) ), 'VERSION NOT FOUND' ) AS [VERSION NAME],
                CASE 
                   WHEN  COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
                   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) 
                   when COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL then COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) 
                   else 'VERSION NOT FOUND'
                end as [VERSION_NAME],
                cte_tt_hs.*,
                CASE
                  WHEN cte_vers.replaceexistingwiththisversion__c IS NOT NULL  OR
                       cte_vers_2.replaceexistingwiththisversion__c IS NOT NULL THEN 'True'
                  ELSE 'False'
                END AS Replace_Existing_Version      
INTO   System_Product_Version__c_Insert_Delta_MC 				                                                                                                                                                                                      
FROM   CTE_TT_HS cte_tt_hs
       LEFT JOIN superion_fullsb.dbo.REGISTERED_PRODUCT__C regprd
                 LEFT JOIN superion_fullsb.dbo.PRODUCT2 p2
                        ON p2.id = regprd.product__c
              ON regprd.account__c = CTE_TT_HS.account__c AND
                 p2.product_group__c = CTE_TT_HS.productgroup AND
                 p2.product_line__c = CTE_TT_HS.productline
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env
              ON cte_env.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env.type__c AND
                 cte_env.name like '%' + CTE_TT_HS.productgroup + '%'
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env2
              ON cte_env2.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env2.type__c AND
                 cte_env2.name like '%vision%' AND
                 CTE_TT_HS.productgroup = 'Inform'
       LEFT JOIN CTE_VERSION cte_vers on cte_vers.ProductLine__c = cte_tt_hs.productline and cte_vers.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%'
       LEFT JOIN Cte_version_2 cte_vers_2 
              ON coalesce(cte_vers_2.LegacyTTZProductGroup__c, cte_vers_2.Product_Group__c) =  cte_tt_hs.productgroup AND
                 cte_vers_2.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%' AND
                 cte_vers_2.[RANK] =  1
       LEFT JOIN Cte_version_MajorRelease cte_mrv on cte_tt_hs.[Version]  = cte_mrv.LegacyMajorReleaseVersion__c
       --where cte_tt_hs.[version] in ('IQ 5.6','IQ 5.6.1') --(27437 row(s) affected)


ALTER TABLE System_Product_Version__c_Insert_Delta_MC ADD Ident INT Identity(1,1);

select * from System_Product_Version__c_Insert_Delta_MC
where Registered_Product__c = 'a9a0v0000008W7NAAU'

select * from System_Product_Version__c_Insert_MC
where Registered_Product__c = 'a9a0v0000008W7NAAU'


-- select 27437 - 27156 = 281

select t2.id, t1.ID, T2.Registered_Product__c AS LOADED_Registered_Product__c, T1.Registered_Product__c,
T2.Version__c AS LOADED_Version__c, T1.Version__c,
T2.Version__c AS LOADED_Environment__c, T1.Environment__c
 from System_Product_Version__c_Insert_Delta_MC t1
left join System_Product_Version__c_Insert_MC t2
on t2.Registered_Product__c = t1.Registered_Product__c 
and coalesce(t2.Version__c,'x') = coalesce(t1.Version__c,'x') 
and coalesce(t2.Environment__c,'x')  = coalesce(t1.Environment__c,'x')
where 
t1.Registered_Product__c = 'a9a0v0000008W7NAAU'
and 
T1.Registered_Product__c <>  'id not found'
and
t2.id is null
order by Registered_Product__c


select * from 
(
SELECT Y.* , X.*
FROM 
(SELECT COUNT(*) AS COUNTOF, Registered_Product__c
FROM System_Product_Version__c_Insert_Delta_MC
GROUP BY Registered_Product__c) X
LEFT JOIN
(
SELECT COUNT(*) AS COUNTOF, Registered_Product__c
FROM System_Product_Version__c_Insert_MC
GROUP BY Registered_Product__c
) Y ON Y.Registered_Product__c = X.Registered_Product__c
WHERE Y.COUNTOF <> COALESCE(X.COUNTOF,0)
)z
where 


a9a0v0000004Ka6AAE