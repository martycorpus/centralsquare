-------------------------------------------------------------------------------
--- ProblemCode__c Object Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2019
--- Created Date: January 2, 2019
--- Last Updated: January 2, 2019 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 1. 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'Product_Sub_Module__c', 'yes' ;

Use Superion_FUllSB
--Exec SF_Refresh 'PB_Superion_FullSB', 'Contact', 'yes'
--Exec SF_Refresh 'PB_Superion_FullSB', 'RecordType', 'yes' ;

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ProblemCode__c_Load_PB') 
DROP TABLE ProblemCode__c_Load_PB;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
Use Staging_SB;


Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_CRMID__c,
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		a.[Name] as [Name],
		CASE a.Active__c WHEN 'true' THEN 'false'
						 WHEN 'false' THEN 'true'
						 Else '' END as Active__c,
		a.Product__c as Product_Original,
		Prod.ID as Product__c,
		a.Product_Sub_Module_Code__c as ProblemCodeName__c

		--INTO ProblemCode__c_Load_PB
		FROM Tritech_PROD.dbo.Product_Sub_Module__c a
		-- Product2
		LEFT OUTER JOIN Superion_FULLSB.dbo.Product2 Prod ON a.Product__c = Prod.Legacy_SF_ID__c and Prod.Acronym_List__c = 'legacy ttz' 
		where 
		Prod.ID is not null
		

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table  Staging_SB.dbo.ProblemCode__c_Load_PB
Add [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
Use Staging_SB
Exec SF_BulkOps 'upsert:bulkapi,batchsize(5000)', 'PB_Superion_FullSB', 'ProblemCode__c_Load_PB', 'Legacy_Id__c';


--select * from ProblemCode__c_Load_PB where error not like '%success%'
