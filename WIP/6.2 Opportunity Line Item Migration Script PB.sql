-------------------------------------------------------------------------------
--- Opportunity Line Item Migration Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 4 December 2018
--- Last Updated: 4 December 2018 - PAB
--- Change Log: 
---
--- Prerequisites:
---	
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='OpportunityLineItem_Load_PB') 
	DROP TABLE Staging_SB.dbo.[OpportunityLineItem_Load_PB];


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Tritech_PROD
Exec SF_Replicate 'PB_Tritech_Prod', 'User'
--Exec SF_Replicate 'PB_Tritech_Prod', 'Account'
Exec SF_Refresh 'PB_Tritech_Prod', 'Opportunity', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'OpportunityLineItem', 'yes'

USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Opportunity', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'PricebookEntry', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Product2', 'yes'

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.ID as Legacy_ID__c,
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		isNull(a.Charge_Type__c, '') as ChargeType__c,
		isNULL(CAST(a.Cost__c as varchar(19)), '') as Cost__c,
		--a.[CreatedByID] as CreatedByID_Original,
		--isNull(CreatedBy.ID, (Select top 1 ID from Superion_FULLSB.dbo.[user] where [Name] = 'Superion API')) as [CreatedByID],
		--a.CreatedDate as CreatedDate,
		isNull(a.[Description], '') as [Description],
		isNull(Cast(a.Discount as varchar(18)), '') as Discount,
		isNull(Cast(a.Extended_List__c as varchar(18)), '') as Extended_List__c,
		isNull(Cast(a.Extended_Net_Price__c as varchar(18)), '') as Extended_Net_Price__c,
		--a.Extended_Price__c as ExtendedPrice__c,  -- formula field
		isNull(a.Functional_Area__c, '') as Functional_Area__c,
		--isNULL(CAST(a.Gross_Margin__c as varchar(19)), '') as Gross_margin__c,  -- formula
		isNull(Cast(a.List_Price_Per_Unit__c as varchar(18)), '')  as List_Price_Per_Unit__c,
		isNull(Cast(a.Maintenance_Amount__c as varchar(18)), '') as Maintenance_Amount__c,
		isNull(Cast(a.Modified_Quantity__c as Varchar(18)), '') as Modified_Quantity__c,
		a.Net_Price__c as Net_Price__c,
		Oppty.ID as Opportunity_Original,
		Target_Oppty.ID as OpportunityId,
		a.PricebookEntryID as PricebookEntry_Orig,
		PBE.ID as PricebookEntryId, 
		isNull(a.Prod_Category__c, '') as Prod_Category__c,
		isNull(a.Prod_Type__c, '') as Prod_Type__c,
		a.Product2id as Product_Original,
		Prod.ID  as Product2ID, 
		a.Quantity as Quantity,
		isNull(Convert(varchar(19), a.ServiceDate, 120), '') as ServiceDate,
		isNull(a.Support_Level__c, '') as Support_Level__c,
		isNull(Cast(a.T_E_Fee__c as varchar(18)), '') as T_E_Fee__c,
		--a.TotalPrice as TotalPrice,
		isNull(a.Type__c, '') as Type__c,
		a.UnitPrice as UnitPrice,
			----Change_Order_Difference__c  -- N/A
			----Converted_to_Asset__c  -- N/A
			----GuidedSelling__customfield__c  -- N/A
			----IsDeleted  -- N/A
			----LastModifiedById  -- N/A
			----LastModifiedDate  -- N/A
			----ListPrice  -- N/A
			----Name  -- N/A
			----ProductCode  -- N/A
			----SortOrder  -- N/A
			----SystemModstamp  -- N/A
		'' as blank
		INTO OpportunityLineItem_Load_PB
		FROM Tritech_PROD.dbo.OpportunityLineItem a
		---- TriTech Opportunity
		LEFT OUTER JOIN TriTech_Prod.dbo.Opportunity Oppty on a.OpportunityID = Oppty.ID
		-- Superion Opportunity
		LEFT OUTER JOIN Superion_FULLSB.dbo.Opportunity Target_Oppty ON Oppty.ID = Target_Oppty.Legacy_Opportunity_ID__c		
		---- Created By
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON a.[CreatedByID] = [CreatedBy].Legacy_Tritech_Id__c
		---- Product
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Product2] Prod ON a.Product2id = Prod.Legacy_SF_ID__c and Prod.Acronym_List__c = 'Legacy TTZ'
		---- Pricebook
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Pricebook2] PB ON PB.[Name] = 'Legacy Pricebook'
		---- Pricebook Entry
		LEFT OUTER JOIN Superion_FULLSB.dbo.[PricebookEntry] PBE ON PBE.Product2Id = Prod.ID
						AND PBE.Pricebook2Id = PB.ID
			
		WHERE 
		(Oppty.IsClosed = 'true' AND Oppty.CloseDate >= Cast('1/1/2016' as datetime))



---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.OpportunityLineItem_Load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'upsert:bulkapi,batchsize(2000)', 'PB_Superion_FULLSB', 'OpportunityLineItem_Load_PB', 'Legacy_ID__c'


--select * from OpportunityLineItem_Load_PB where error not like '%success%' 
