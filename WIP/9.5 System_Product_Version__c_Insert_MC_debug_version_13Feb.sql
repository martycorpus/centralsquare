/*-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 9.5 System_Product_Version__c_Insert_MC.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--- Copyright Apps Associates 2018
--- Created Date: Jan 18, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-01-21 Added version matching using product group only (per Maria).
--             Added ReplaceexistingwiththisVersion__c logic.
--             Added logic for version matching using LegacyMajorReleaseVersion__c.
--             Added logic for Environment Matching due to change in the Environment Name and deduping logic.
--  2019-01-29 Fix syntax error on Cte_version_MajorRelease line 151. (extra string 5.10.1 not needed, causing execution error).
--  2019-02-04 Wipe and Reload.
--  2019-02-05 Wipe and Reload.
--  2019-02-12 Wipe and Reload.
--- Prerequisites for Script to execute
---  
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-02-12 Executed in Superion FullSbx.

-- WIPE:
-- select * into System_Product_Version__c_Insert_MC_delete_12Feb from System_Product_Version__c_Insert_MC where error = 'Operation Successful.'
-- (7134 row(s) affected)
-- exec SF_Bulkops 'Delete','MC_SUPERION_FULLSB','System_Product_Version__c_Insert_MC_delete_12Feb'
--16:19:37: Warning: Column 'productline' ignored because it does not exist in the System_Product_Version__c object.
--16:19:37: Warning: Column 'environmenttype' ignored because it does not exist in the System_Product_Version__c object.
--16:19:37: Warning: Column 'Replace_Existing_Version' ignored because it does not exist in the System_Product_Version__c object.
--16:19:37: Warning: Column 'Ident' ignored because it does not exist in the System_Product_Version__c object.
--16:20:43: 7134 rows read from SQL Table.
--16:20:43: 7134 rows successfully processed.
--16:20:43: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.




-- RELOAD: (18944 additional SPV records). -- select 26078 - 7134


-- exec SF_ColCompare 'Insert','MC_SUPERION_FULLSB','System_Product_Version__c_Insert_MC'
/*
--- Starting SF_BulkOps for System_Product_Version__c_Insert_MC V3.6.9
18:14:14: Run the DBAmp.exe program.
18:14:14: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:14:14: Inserting System_Product_Version__c_Insert_MC (SQL01 / Staging_SB).
18:14:14: DBAmp is using the SQL Native Client.
18:14:15: SOAP Headers: 
18:14:15: Warning: Column 'Registered Product Name' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'Environment Name' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'VERSION_NAME' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'tt_legacy_id_hs' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'tt_hs_name' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'tt_legacysfdcaccountid__c' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'account__c' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'account_name' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'version' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'sourcefieldname' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'productgroup' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'productline' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'environmenttype' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'Replace_Existing_Version' ignored because it does not exist in the System_Product_Version__c object.
18:14:15: Warning: Column 'Ident' ignored because it does not exist in the System_Product_Version__c object.
18:17:36: 38781 rows read from SQL Table.
18:17:36: 12703 rows failed. See Error column of row for more information.
18:17:36: 26078 rows successfully processed.
18:17:36: Errors occurred. See Error column of row for more information.
18:17:36: Percent Failed = 32.800.
18:17:36: Error: DBAmp.exe was unsuccessful.
18:17:36: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert System_Product_Version__c_Insert_MC "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 69]
SF_BulkOps Error: 18:14:14: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC18:14:14: Inserting System_Product_Version__c_Insert_MC (SQL01 / Staging_SB).18:14:14: DBAmp is using the SQL Native Client.18:14:15: SOAP Headers: 18:14:15: Warning: Column 'Registered Product Name' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'Environment Name' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'VERSION_NAME' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'tt_legacy_id_hs' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'tt_hs_name' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'tt_legacysfdcaccountid__c' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'account__c' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'account_name' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'version' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'sourcefieldname' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'productgroup' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'productline' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'environmenttype' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'Replace_Existing_Version' ignored because it does not exist in the System_Product_Version__c object.18:14:15: Warning: Column 'Ident' ignored because it does not exist in the System_Product_Version__c object.18:17:36: 38781 rows read from SQL Table....

*/
---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_PROD;

Exec SF_Refresh 'MC_TRITECH_PROD','Hardware_Software__c','Yes'

Use Superion_FULLSB;

Exec SF_Refresh 'MC_SUPERION_FULLSB','CUSTOMER_ASSET__C','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Registered_Product__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Environment__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Version__c','Yes'


---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_SB;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_MC') 
DROP TABLE Staging_SB.dbo.System_Product_Version__c_Insert_MC;

---------------------------------------------------------------------------------
-- Create System_Product_Version__c Staging Load Table
---------------------------------------------------------------------------------

*/

--Cte
WITH CTE_SUP_Environment As
(
SELECT Id, Name, Type__c, Account__c from Superion_FULLSB.dbo.Environment__c
),
CTE_Zuercher_Customer_assets as
(
SELECT t1.id as Zuercher_Registered_Product_Id,
       t1.name as Zuercher_Registered_Product_Name,
       t2.id   AS CustomerAsset__c,
       t2.name AS CustomerAsset_Name,
       t2.product_group__c as CUSTOMER_ASSET_Product_Group,
       p2.product_group__c,
       p2.product_line__c,
       t1.Account__c,
       a.Name as accountname
FROM   superion_fullsb.dbo.REGISTERED_PRODUCT__C t1
        LEFT JOIN superion_fullsb.dbo.PRODUCT2 p2
        ON p2.id = t1.product__c
       join superion_fullsb.dbo.CUSTOMER_ASSET__C t2
         ON t2.id = t1.customerasset__c
       left join  superion_fullsb.dbo.account a on a.Id = t1.Account__c 
WHERE  t2.product_group__c = 'Zuercher' 
),
Cte_version as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, t1.ProductLine__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_FULLSB.dbo.version__c t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
),
Cte_version_MajorRelease as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, 
t1.ProductLine__c, t1.LegacyMajorReleaseVersion__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_FULLSB.dbo.version__c t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
and t1.LegacyMajorReleaseVersion__c is not null
and t1.legacyttzsource__c = 'Version reconciliation' and t1.LegacyMajorReleaseVersion__c = '1.1'
),
Cte_version_2 as
(
SELECT Row_number( )
         over (
           PARTITION BY t1.versionnumber__c
           ORDER BY (CASE WHEN t1.legacyttzsource__c = 'CS Import File' THEN 1 WHEN t1.legacyttzsource__c = 'Hardware Software' THEN 2 ELSE 3 END) ) AS "RANK",
       t1.id,
       t1.ReplaceexistingwiththisVersion__c,
       t2.name as ReplaceexistingwiththisVersion_Name,       
       t1.name,
       t1.productline__c,
       t1.legacyttzversion__c,
       t1.legacyttzproductgroup__c,
       t1.product_group__c,
       t1.versionnumber__c,
       t1.legacyttzsource__c,
       t1.current__c
FROM   superion_fullsb.dbo.VERSION__C t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
WHERE  t1.legacyttzproductgroup__c = 'IMC'  OR
       t1.product_group__c = 'IMC' 
),
CTE_TT_HS -- TT Hardware Software CTE
AS
(
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          t3.id                      AS account__c,
          t3.name                    AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.cim_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.cim_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.cim_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.cim_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.cim_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.fbr_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.fbr_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.fbr_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.fbr_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.fbr_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.imc_version__c  AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMC_Version__c '
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.imc_version__c IS NOT NULL
UNION
SELECT    t1.id                   AS tt_legacy_id_hs,
          t1.name                 AS tt_hs_name,
          t1.account_name__c      AS tt_legacysfdcaccountid__c,
          t3.id                   AS account__c,
          t3.name                 AS account_name,
          t1.inform_me_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Inform_Me_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.inform_me_version__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.iq_version__c   AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IQ_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.iq_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.jail_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Jail_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.jail_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.rms_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.rms_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.rms_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.ttms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'TTMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.ttms_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_cad_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_CAD_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_cad_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_fbr_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_FBR_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fbr_software_version__c IS NOT NULL
UNION
SELECT    t1.id                          AS tt_legacy_id_hs,
          t1.name                        AS tt_hs_name,
          t1.account_name__c             AS tt_legacysfdcaccountid__c,
          t3.id                          AS account__c,
          t3.name                        AS account_name,
          t1.va_fire_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Fire_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fire_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_inform_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Inform_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_inform_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_mobile_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Mobile_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_mobile_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_rms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_RMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_rms_software_version__c IS NOT NULL
UNION
SELECT    t1.id               AS tt_legacy_id_hs,
          t1.name             AS tt_hs_name,
          t1.account_name__c  AS tt_legacysfdcaccountid__c,
          t3.id               AS account__c,
          t3.name             AS account_name,
          t1.visicad_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_patch__c IS NOT NULL
UNION
SELECT    t1.id                 AS tt_legacy_id_hs,
          t1.name               AS tt_hs_name,
          t1.account_name__c    AS tt_legacysfdcaccountid__c,
          t3.id                 AS account__c,
          t3.name               AS account_name,
          t1.visicad_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_production_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Production_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_production_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_mobile_test_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_mobile_test_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_training_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Training_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_training_version__c IS NOT NULL
UNION
SELECT    t1.id                        AS tt_legacy_id_hs,
          t1.name                      AS tt_hs_name,
          t1.account_name__c           AS tt_legacysfdcaccountid__c,
          t3.id                        AS account__c,
          t3.name                      AS account_name,
          t1.visinet_mobile_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_version__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_test_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_test_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_version__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.visinet_training_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_training_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.x911_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'X911_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.x911_software_version__c IS NOT NULL
)
SELECT DISTINCT Cast (null as nchar(18)) as ID,
                Cast (null as nvarchar(255)) as Error,
                COALESCE( regprd.id, 'ID NOT FOUND' )   AS Registered_Product__c,
                COALESCE( regprd.NAME, 'REG PROD NOT FOUND' )       AS [Registered Product Name],
                COALESCE( COALESCE(cte_env.id,cte_env2.id), 'ID NOT FOUND' )       AS Environment__c,
                COALESCE( coalesce(cte_env.NAME,cte_env2.name), 'ENVIRONMENT NOT FOUND' )                                                                                                                                                 AS [Environment Name],
                --CASE 
                --   WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
                --   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id )
                --   WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id) is not null then COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id)
                --   else 'Id not found'
                --   end as Version__c,

                CASE 
                   WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
                  -- WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id )
                   WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id) is not null then COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id)
                   else 'Id not found'
                   end as Version__c,


               -- COALESCE( COALESCE( COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ), COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) ), 'ID NOT FOUND' )              AS Version__c,
                --COALESCE(cte_vers.Id,'ID NOT FOUND') AS 	Version__c,
                --COALESCE( COALESCE( COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ), COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) ), 'VERSION NOT FOUND' ) AS [VERSION NAME],
                --CASE 
                --   WHEN  COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
                --   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) 
                --   when COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL then COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) 
                --   else 'VERSION NOT FOUND'
                --end as [VERSION_NAME],


				                CASE 
                   WHEN  COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
                   --WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) 
                   when COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL then COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) 
                   else 'VERSION NOT FOUND'
                end as [VERSION_NAME],

                cte_tt_hs.*,
                CASE
                  WHEN cte_vers.replaceexistingwiththisversion__c IS NOT NULL   
				  --OR
      --                 cte_vers_2.replaceexistingwiththisversion__c IS NOT NULL 
	  THEN 'True'
                  ELSE 'False'
                END AS Replace_Existing_Version    
				,   cte_tt_hs.[Version] as [cte_tt_hs.Version]
				, cte_vers.LegacyTTZVersion__c as [cte_vers.LegacyTTZVersion__c]
--INTO   System_Product_Version__c_Insert_MC 				                                                                                                                                                                                      
FROM   CTE_TT_HS cte_tt_hs
       LEFT JOIN superion_fullsb.dbo.REGISTERED_PRODUCT__C regprd
                 LEFT JOIN superion_fullsb.dbo.PRODUCT2 p2
                        ON p2.id = regprd.product__c
              ON regprd.account__c = CTE_TT_HS.account__c AND
                 p2.product_group__c = CTE_TT_HS.productgroup AND
                 p2.product_line__c = CTE_TT_HS.productline
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env
              ON cte_env.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env.type__c AND
                 cte_env.name like '%' + CTE_TT_HS.productgroup + '%'
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env2
              ON cte_env2.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env2.type__c AND
                 cte_env2.name like '%visi%' AND --maybe change to '%visi%'
                 CTE_TT_HS.productgroup = 'Inform'
      LEFT JOIN CTE_VERSION cte_vers on cte_vers.ProductLine__c = cte_tt_hs.productline and cte_vers.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%'
	          --LEFT JOIN CTE_VERSION cte_vers on cte_vers.ProductLine__c = cte_tt_hs.productline and cte_vers.LegacyTTZVersion__c =  cte_tt_hs.[Version]
     --  LEFT JOIN Cte_version_2 cte_vers_2 --possible remove this section
     --         ON coalesce(cte_vers_2.LegacyTTZProductGroup__c, cte_vers_2.Product_Group__c) =  cte_tt_hs.productgroup AND
     --            cte_vers_2.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%' AND
     --            cte_vers_2.[RANK] =  1 --if we dont find an exact match: leave the version field as null 
				 ----we need to make sure that there is only one spv record per environment
       LEFT JOIN Cte_version_MajorRelease cte_mrv on cte_tt_hs.[Version]  = cte_mrv.LegacyMajorReleaseVersion__c
	   where 
	   --cte_tt_hs.[version] in  ('5.8 SP19')
	   --AND
	    regprd.account__c = '0010v00000IkM10AAF'
       --where cte_tt_hs.[version] in ('IQ 5.6','IQ 5.6.1') --(38781 row(s) affected 2/12


--ALTER TABLE System_Product_Version__c_Insert_MC ADD Ident INT Identity(1,1);

--select * from System_Product_Version__c_Insert_MC
-- where Registered_Product__c = 'a9a0v0000008VFHAA2'


-- select * from System_Product_Version__c_Insert_MC
-- where Registered_Product__c
-- = 'a9a0v0000008VFHAA2'