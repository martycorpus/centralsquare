/*
-- Author     : Shivani Mogullapalli
-- Date       : 06/11/2018
-- Description: Migrate the Case data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0022
Scope:
All Case Comments related to a migrated case will be migrated to the Superion instance.
Migrate the case comments as Chatter comments to case
*/

--use Tritech_Prod
--EXEC SF_Refresh 'MC_Tritech_PROD','CaseComment','yes'

select count(*) from Tritech_PROD.dbo.CaseComment
--2424529


use staging_sb
go 
-- drop table feedItem_Tritech_SFDC_CaseComment_Preload


declare @defaultuser nvarchar(18)
 select @defaultuser = id from Superion_FULLSB.dbo.[user] where name = 'Superion API'
SELECT 
         ID											 =   CAST(SPACE(18) as NVARCHAR(18))
		,ERROR										 =   CAST(SPACE(255) as NVARCHAR(255))
		,Body										 =	 Tr_Cmt.[CommentBody]
		,CreatedbyId_orig							 =	 Tr_Cmt.[CreatedById]
		,CreatedById								 =   iif(Target_Create.Id is null,@defaultuser,Target_Create.Id)
		,CreatedDate								 =	 Tr_Cmt.[CreatedDate]
		,Id_orig									 =   Tr_Cmt.[Id]
														  --,Tr_Cmt.[IsDeleted]
		,IsNotificationSelected						 = Tr_Cmt.[IsNotificationSelected]
		,Visibility								 = iif(Tr_Cmt.[IsPublished] ='True' ,'AllUsers','InternalUsers')
		,IsPublished_orig							=tr_Cmt.IsPublished
														  --,Tr_Cmt.[LastModifiedById]
														  --,Tr_Cmt.[LastModifiedDate]
				,Parentid_orig						=   Tr_Cmt.[ParentId]
				,Parentid							=   Target_Parent.ID
														  --,Tr_Cmt.[SystemModstamp]
				into feedItem_Tritech_SFDC_CaseComment_preload
  FROM [Tritech_PROD].[dbo].[CaseComment] Tr_Cmt
  inner join Superion_FULLSB.dbo.[case] Target_Parent
  on Target_Parent.Legacy_ID__c=Tr_cmt.ParentId
  left join Superion_FULLSB.dbo.[User] Target_Create
  on Target_create.Legacy_Tritech_Id__c=Tr_Cmt.CreatedById
--(895023 row(s) affected)



select * from feedItem_Tritech_SFDC_CaseComment_preload where id_orig='00a8000000qGM0IAAW'

select count(*) from
feedItem_Tritech_SFDC_CaseComment_preload

select Legacy_ID__c,* from Superion_FULLSB.dbo.[case] where Legacy_ID__c='50080000017cdVxAAI'
group by Legacy_ID__c
having count(*)>1

select id_orig, count(*)
from feedItem_Tritech_SFDC_CaseComment_preload
group by id_orig 
having count(*)>1

-- drop table feedItem_Tritech_SFDC_CaseComment_load
select * 
into feedITem_Tritech_SFDC_CaseComment_load
from feedItem_Tritech_SFDC_CaseComment_preload

-- (895023 row(s) affected)

select * from feedITem_Tritech_SFDC_CaseComment_load

-- insert Casecomment records as Chatter comment.

-- EXEC SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'feedITem_Tritech_SFDC_CaseComment_load' 

-- EXEC SF_BulkOps 'Insert','SL_SUPERION_FULLSB','feedItem_Tritech_SFDC_CaseComment_load' 

/*
Salesforce object feedITem does not contain column CreatedbyId_orig
Salesforce object feedITem does not contain column Id_orig
Salesforce object feedITem does not contain column IsNotificationSelected
Salesforce object feedITem does not contain column IsPublished_orig
Salesforce object feedITem does not contain column Parentid_orig
*/

select error, count(*)
from feedItem_Tritech_SFDC_CaseComment_load
group by error 

select * from feedItem_Tritech_SFDC_CaseComment_load 
where error <>'Operation Successful.'

select error, count(*)
from feedItem_Tritech_SFDC_CaseComment_load
group by error 

-- drop table feedItem_Tritech_SFDC_CaseComment_load_errors
select *
into feedItem_Tritech_SFDC_CaseComment_load_errors
from feedItem_Tritech_SFDC_CaseComment_load
where error <>'Operation Successful.' and error not like'Required%'
-- (1626 row(s) affected)


-- drop table feedItem_Tritech_SFDC_CaseComment_load_errors_bkp
select *
into feedItem_Tritech_SFDC_CaseComment_load_errors_bkp
from feedItem_Tritech_SFDC_CaseComment_load
where error <>'Operation Successful.'
-- (1626 row(s) affected)


-- EXEC SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','feedItem_Tritech_SFDC_CaseComment_load_errors' 


select error, count(*) from feedItem_Tritech_SFDC_CaseComment_load
group by error 


-- drop table feedItem_Tritech_SFDC_CaseComment_load_errors1
select *
into feedItem_Tritech_SFDC_CaseComment_load_errors1
from feedItem_Tritech_SFDC_CaseComment_load_errors
where error <>'Operation Successful.'

-- (5 row(s) affected)

-- EXEC SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','feedItem_Tritech_SFDC_CaseComment_load_errors1' 

select error, count(*) from feedItem_Tritech_SFDC_CaseComment_load_errors1
group by error 
having count(*)>1

select *
-- delete 
from feedItem_Tritech_SFDC_CaseComment_load where error <>'Operation Successful.'

--(1626 row(s) affected)

-- insert into  feedItem_Tritech_SFDC_CaseComment_load
select * from feedItem_Tritech_SFDC_CaseComment_load_errors 
-- (998 row(s) affected)

-- insert into  feedItem_Tritech_SFDC_CaseComment_load
select * from feedItem_Tritech_SFDC_CaseComment_load_errors_bkp where error like 'Required fields%'
--(628 row(s) affected)

-- insert into  feedItem_Tritech_SFDC_CaseComment_load
select * from feedItem_Tritech_SFDC_CaseComment_load_errors1 where error like 'Required fields%'






