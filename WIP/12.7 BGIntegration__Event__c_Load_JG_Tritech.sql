 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Event__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'SL_Tritech_PROD','BGIntegration__Event__c'

Use Superion_FULLSB
EXEC SF_Refresh 'SL_Superion_FULLSB','User','Yes'
EXEC SF_Refresh 'SL_Superion_FULLSB','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_Superion_FULLSB','BGIntegration__Event__c','Yes'
*/ 

--2018-12-06 Marty Executed for UAT2 LOAD.
 --Exec SF_ColCompare 'Insert','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_Load' 
 --- Starting SF_ColCompare V3.6.9
--Problems found with BGIntegration__Event__c_Tritech_SFDC_Load. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 25]
--- Ending SF_ColCompare. Operation FAILED.
--ErrorDesc
--Salesforce object BGIntegration__Event__c does not contain column BGIntegration__BomgarSession__c_orig
--Salesforce object BGIntegration__Event__c does not contain column CreatedById_orig

--select count(*) from BGIntegration__Event__c_Tritech_SFDC_Load --2,457,573

--Exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_Load'
--bulkapi job id = 7500v000003kEtt

--00:41:05: Job still running.
--00:42:05: Job Complete.
--00:42:05: Retrieving Job Status.
--01:22:39: 2457573 rows read from SQL Table.
--01:22:39: 2457573 rows successfully processed.
--01:22:39: 0 rows failed.
--01:22:39: 2457573 rows marked with current status.
--01:22:50: Percent Failed = 0.000.
----- Ending SF_BulkOps. Operation successful. 02:33:39

Use Staging_SB;

--Drop table BGIntegration__Event__c_Tritech_SFDC_Preload;

Select

ID = CAST(SPACE(18) as NVARCHAR(18)),
ERROR = CAST(SPACE(255) as NVARCHAR(255)),
Legacy_id__c =bg.Id, 
Legacy_Source_System__c='Tritech',
Migrated_Record__c='True',

BGIntegration__Body__c                    =  bg.BGIntegration__Body__c,

BGIntegration__BomgarSession__c_orig      =  bg.BGIntegration__BomgarSession__c,
BGIntegration__BomgarSession__c           =  bs.Id,

BGIntegration__Destination__c             =  bg.BGIntegration__Destination__c,
BGIntegration__Destination_GS_Number__c   =  bg.BGIntegration__Destination_GS_Number__c,
BGIntegration__Destination_Type__c        =  bg.BGIntegration__Destination_Type__c,
BGIntegration__File_Name__c               =  bg.BGIntegration__File_Name__c,
BGIntegration__File_Size__c               =  bg.BGIntegration__File_Size__c,
BGIntegration__Number__c                  =  bg.BGIntegration__Number__c,
BGIntegration__Performed_By__c            =  bg.BGIntegration__Performed_By__c,
BGIntegration__Performed_By_GS_Number__c  =  bg.BGIntegration__Performed_By_GS_Number__c,
BGIntegration__Performed_By_Type__c       =  bg.BGIntegration__Performed_By_Type__c,
BGIntegration__Timestamp__c               =  bg.BGIntegration__Timestamp__c,

CreatedById_orig                          =  bg.CreatedById,
CreatedById                               =  createdbyuser.Id,

CreatedDate                               =  bg.CreatedDate,
--Legacy_ID__c                              =  bg.Id,
Name                                      =  bg.[Name]

into BGIntegration__Event__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__Event__c bg

--Fetching CreatedById(UserLookup)
left join Superion_FULLSB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join Superion_FULLSB.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

----For Delta Load
--left join Superion_FULLSB.dbo.BGIntegration__Event__c trgt on
--trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

; --(2565657 row(s) affected)

---------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Event__c;--2565657

Select count(*) from BGIntegration__Event__c_Tritech_SFDC_Preload;--2565657

Select Legacy_Id__c,count(*) from Staging_SB.dbo.BGIntegration__Event__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB.dbo.BGIntegration__Event__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__Event__c_Tritech_SFDC_Load
from BGIntegration__Event__c_Tritech_SFDC_Preload; --(2565657 row(s) affected)

Select * from Staging_SB.dbo.BGIntegration__Event__c_Tritech_SFDC_Load 
where  BGIntegration__BomgarSession__c IS NULL;


--Exec SF_ColCompare 'Insert','SL_Superion_FullSB', 'BGIntegration__Event__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__Event__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Event__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_Load'

----------------------------------------------------------------------------------------
--Updating Legacy_Source_System__c,Migrated_Record__c

--Use Staging_SB;

--Drop table BGIntegration__Event__c_Tritech_SFDC_LoadUpdate

Select 

 Id =bg.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Legacy_id__c
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

into BGIntegration__Event__c_Tritech_SFDC_LoadUpdate
from Staging_SB.dbo.BGIntegration__Event__c_Tritech_SFDC_Load bg
;--(2284968 row(s) affected)

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_LoadUpdate' 

/*
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_LoadUpdate' 

------------------------------------------------------------------------------------------------

Select *
--delete 
from BGIntegration__Event__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --

--insert into BGIntegration__Event__c_Tritech_SFDC_Load
select * from BGIntegration__Event__c_Tritech_SFDC_Load_Delta; 