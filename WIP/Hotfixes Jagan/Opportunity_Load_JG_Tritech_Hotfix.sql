--Updating Price_Book__c

--Drop table Opportunity_Tritech_SFDC_UpdatePriceBook;

--DECLARE @LegacyPricebookId NVARCHAR(18) = (Select top 1 Id from Superion_FULLSB.dbo.Pricebook2
--where Name like 'Legacy Pricebook');

Select 

 Id=a.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Price_Book__c_prev=a.Price_Book__c
,Price_Book__c='Legacy Pricebook'
,Charge_Types__c_prev=tar.Charge_Types__c
,Charge_Types__c=opp.Charge_Types__c
,Solution_Family__c='Public Safety & Justice'
--,Pricebook2Id=@LegacyPricebookId

into Opportunity_Tritech_SFDC_UpdatePriceBook 

from Staging_SB.dbo.Opportunity_Tritech_SFDC_Load a

inner join Tritech_PROD.dbo.Opportunity opp
on opp.Id=a.Legacy_Opportunity_ID__c

inner join Superion_FULLSB.dbo.Opportunity tar
on tar.Legacy_Opportunity_ID__c=opp.Id

where a.Error='Operation Successful.';--18660

Select * from Opportunity_Tritech_SFDC_UpdatePriceBook;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdatePriceBook' 

/*
Salesforce object Opportunity does not contain column Price_Book__c_prev
Salesforce object Opportunity does not contain column Charge_Types__c_prev
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdatePriceBook'

----------------------------------------------------------------------------------

Select 
 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Pricebook2Id_prev=a.Pricebook2Id
,Pricebook2Id='01s0v000000i8UEAAY'
,Legacy_Opportunity_ID__c_orig=a.Legacy_Opportunity_ID__c

into Opportunity_Tritech_SFDC_UpdatePriceBook2Id

from Staging_SB.dbo.Opportunity_Tritech_SFDC_Load a

Select * from Opportunity_Tritech_SFDC_UpdatePriceBook2Id;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdatePriceBook2Id' 

/*
Salesforce object Opportunity does not contain column Pricebook2Id_prev
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdatePriceBook2Id'

------------------------------------------------------------------------
--Select Legacy_Id__c from Staging_SB.dbo.Contact_Tritech_SFDC_Preload where Legacy_Id__c IN
--(
Select 
 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Products_Required__c=a.Products_Required__c
,Pre_Paid_Maintenance__c=a.Pre_Paid_Maintenance__c
,Z_Special_Maintenance_Terms__c=a.Z_Special_Maintenance_Terms__c
,Primary_Quote_Contact__c_orig=lgcyopty.Primary_Quote_Contact__c
,Primary_Quote_Contact__c=prmryquotecntct.Id
,Legacy_Opportunity_ID__c_orig=a.Legacy_Opportunity_ID__c
,Z_Forecast_Category__c_orig=lgcyopty.Z_Forecast_Category__c
,CASE 
 WHEN lgcyopty.Z_Forecast_Category__c='Best Case' THEN 'Likely'
 WHEN lgcyopty.Z_Forecast_Category__c='Commit' THEN 'Worst'
 WHEN lgcyopty.Z_Forecast_Category__c='Pipeline' THEN 'Best'
 ELSE lgcyopty.Z_Forecast_Category__c
 END as Mgr_Commit__c

--into Opportunity_Tritech_SFDC_UpdateNewFields

from Staging_SB.dbo.Opportunity_Tritech_SFDC_Load a

inner join Tritech_PROD.dbo.Opportunity lgcyopty on
lgcyopty.Id=a.Legacy_Opportunity_ID__c

--Fetching Primary_Quote_Contact__c (Contact Lookup)
left join Superion_FULLSB.dbo.Contact prmryquotecntct on
prmryquotecntct.Legacy_id__c=lgcyopty.Primary_Quote_Contact__c

--where prmryquotecntct.Id IS NULL and lgcyopty.Primary_Quote_Contact__c IS NOT NULL;
--)
--18985
Select * from Opportunity_Tritech_SFDC_UpdateNewFields;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateNewFields' 

/*
Salesforce object Opportunity does not contain column Products_Required__c
Salesforce object Opportunity does not contain column Pre_Paid_Maintenance__c
Salesforce object Opportunity does not contain column Z_Special_Maintenance_Terms__c
Salesforce object Opportunity does not contain column Primary_Quote_Contact__c_orig
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID__c_orig
Salesforce object Opportunity does not contain column Z_Forecast_Category__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateNewFields'

select * 
into Opportunity_Tritech_SFDC_UpdateNewFieldserrors
from Opportunity_Tritech_SFDC_UpdateNewFields
where error<>'Operation Successful.';--3856

select * 
into Opportunity_Tritech_SFDC_UpdateNewFieldserrors_bkp
from Opportunity_Tritech_SFDC_UpdateNewFields
where error<>'Operation Successful.';--3856

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateNewFieldserrors'

--delete 
from Opportunity_Tritech_SFDC_UpdateNewFields
where error<>'Operation Successful.'; --3856

--insert into Opportunity_Tritech_SFDC_UpdateNewFields
select * from Opportunity_Tritech_SFDC_UpdateNewFieldserrors;--3856

------------------------------------------------------------------------------------
-------------------------------------------------OPPORTUNITY HOTFIX---------------------------------------------------------
select id, error,
PrePaid_Maintenance__c =	Pre_Paid_Maintenance__c
,Special_Maintenance_Terms__c = Z_Special_Maintenance_Terms__c
--into Opportunity_Tritech_SFDC_load_Update_hotfix
from Opportunity_Tritech_SFDC_load
where error='Operation Successful.'
and( Pre_Paid_Maintenance__c is not null or Z_Special_Maintenance_Terms__c is not null)

--Exec SF_BulkOps 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_load_Update_hotfix' 
-------------------------------------------------------------------------------------------------------------------------------



-------------------------------------------------------------------------------------------

Select

 Id=ld.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))  
,Winning_Competitor__c_orig
,Winning_SW_Vendor_Lookup__c_orig=Winning_SW_Vendor_Lookup__c
,Winning_SW_Vendor_Lookup__c=comp.[Sandbox Id]

--into Opportunity_Tritech_SFDC_UpdateWinningCompetitor

 from Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld

 left join Staging_SB.dbo.MissingCompetitors comp
 on ld.Winning_Competitor__c_orig=comp.[Competitor Name]

 where Winning_Competitor__c_orig is not null and Winning_SW_Vendor_Lookup__c is null;

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateWinningCompetitor' 

/*
Salesforce object Opportunity does not contain column Winning_Competitor__c_orig
Salesforce object Opportunity does not contain column Winning_SW_Vendor_Lookup__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateWinningCompetitor'

 ------------------------------------------------------------------------------------------------------------

 Select  
 Id=ld.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))  
,Incumbent_to_be_Replaced__c_orig
,Incumbent_Software_Vendor__c_orig=Incumbent_Software_Vendor__c
,Incumbent_Software_Vendor__c=comp.[Sandbox Id]

--into Opportunity_Tritech_SFDC_UpdateIncumbentSoftwareVendor

 from Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld

 left join Staging_SB.dbo.MissingCompetitors comp
 on cast(ld.Incumbent_to_be_Replaced__c_orig as nvarchar(max))=comp.[Competitor Name]

 where Incumbent_to_be_Replaced__c_orig is not null and Incumbent_Software_Vendor__c is null;

  --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateIncumbentSoftwareVendor' 

/*
Salesforce object Opportunity does not contain column Incumbent_to_be_Replaced__c_orig
Salesforce object Opportunity does not contain column Incumbent_Software_Vendor__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateIncumbentSoftwareVendor'

------------------------------------------------------------------------------

Use Staging_SB;

Select 
 Id = ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Product_s_Required__c_sup=sup.Product_s_Required__c
,Product_s_Required__c=opp.Products_Required__c
,Z_Forecast_Category__c_orig=opp.Z_Forecast_Category__c
,StageName_orig=opp.StageName
,Deal_Forecast__c_sup=sup.Deal_Forecast__c
,CASE 
 WHEN opp.Z_Forecast_Category__c='Commit' THEN 'Commit'
 WHEN opp.Z_Forecast_Category__c='Best' THEN 'Likely'
 WHEN opp.Z_Forecast_Category__c='Pipeline' THEN 'Best'
 WHEN opp.Z_Forecast_Category__c='Omitted' THEN 'Omit'
 WHEN opp.Z_Forecast_Category__c='Best Case' THEN 'Likely'
 WHEN opp.Z_Forecast_Category__c='Best Deal' THEN 'Likely'
 WHEN opp.Z_Forecast_Category__c='Closed' THEN 'Commit'
 WHEN opp.Z_Forecast_Category__c IS NULL THEN IIF(opp.StageName='Closed Won','Commit',NULL)
 WHEN opp.Z_Forecast_Category__c IS NULL THEN IIF(opp.StageName='Closed Lost','Omit',NULL)
 ELSE opp.Z_Forecast_Category__c
 END as Deal_Forecast__c
,Legacy_Opportunity_ID__c_orig=opp.Id

into Opportunity_Tritech_SFDC_UpdateNewFields2

from Tritech_PROD.dbo.Opportunity opp
inner join Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld
on opp.Id=ld.Legacy_Opportunity_ID__c
left join Superion_FULLSB.dbo.Opportunity sup
on sup.Legacy_Opportunity_ID__c=opp.Id
where ld.ERROR='Operation Successful.';

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateNewFields2' 

/*
Salesforce object Opportunity does not contain column Product_s_Required__c_sup
Salesforce object Opportunity does not contain column Z_Forecast_Category__c_orig
Salesforce object Opportunity does not contain column StageName_orig
Salesforce object Opportunity does not contain column Deal_Forecast__c_sup
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateNewFields2'

select * 
into Opportunity_Tritech_SFDC_UpdateNewFields2errors
from Opportunity_Tritech_SFDC_UpdateNewFields2
where error<>'Operation Successful.';--49

select * 
into Opportunity_Tritech_SFDC_UpdateNewFields2errors_bkp
from Opportunity_Tritech_SFDC_UpdateNewFields2
where error<>'Operation Successful.';--49

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateNewFields2errors'

--1 error entity is deleted.

--delete 
from Opportunity_Tritech_SFDC_UpdateNewFields2
where error<>'Operation Successful.'; --49

--insert into Opportunity_Tritech_SFDC_UpdateNewFields2
select * from Opportunity_Tritech_SFDC_UpdateNewFields2errors;--49

-------------------------------------------------------------------------------------

select oppload.id, oppload.error,Targetopp.probability as Probability_Superion,Targetopp.isclosed as isclosed_superion

,Reason_Tritech_lost__c_orig= opp.reason_Tritech_lost__c

,tt_Reason_Tritech_lost__C_Tritech_orig = map.tt_Reason_Tritech_lost__C

,Primary_win_Loss_reason__c = map.superion_Primary_Win_loss_reason__c

,Target_Win_loss_comments__c = oppload.Win_Loss_Comments__c

,Superion_Win_loss_comments__c = Targetopp.Win_Loss_Comments__c

,superion_win_loss_comments__c_orig = map.superion_win_loss_comments__c

,Comments_Reason_TriTech_Lost__c_orig = opp.Comments_Reason_TriTech_Lost__c

,Win_Loss_Comments__c= iif(map.superion_win_loss_comments__c ='Comments_Reason_TriTech_Lost__c',opp.Comments_Reason_TriTech_Lost__c,

                  iif(map.superion_win_loss_Comments__c is not null,map.superion_win_loss_comments__c,oppload.win_loss_Comments__c))

,targetopp.StageName as superion_Stagename

,oppload.StageName as load_Stagename

,Stagename = iif(map.superion_Stagename is not null, map.superion_Stagename , oppload.stagename)

,Return_to_Marketing_Reason__c = map.superion_Return_to_Marketing_Reason__c

,Superion_Return_to_Marketing_Reason__c =  targetopp.Return_to_Marketing_Reason__c

--into Opportunity_Tritech_SFDC_load_hotfix

from tritech_prod.dbo.opportunity opp

inner join Opportunity_Tritech_SFDC_load oppload

on opp.id=oppload.Legacy_Opportunity_ID__c

left join Tritech_Opportunity_Primary_Win_loss_reason_Mapping map

on map.TT_Reason_Tritech_lost__c= opp.Reason_TriTech_Lost__c

inner join Superion_FULLSB.dbo.Opportunity Targetopp

on targetopp.Legacy_Opportunity_ID__c= opp.id

-------------------------------------------------------------------------------------------------

--Drop table Opportunity_Tritech_SFDC_UpdateOrder_Type__c

Update  OpportunityTypeMapping
set [TT Value]='Add-On/Extension' where [TT Value]='Add On/Extension'

Select

 Id=ld.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255)) 
,Deal_Type__c_orig=opp.Deal_Type__c
,Mapping_TT_Value=map.[TT Value]
,Order_Type__c_sup=tar.Order_Type__c
,Order_Type__c=map.[CS Value] 
,SolutionCategory__c_sup=tar.SolutionCategory__c
,SolutionCategory__c=IIF(opp.Deal_Type__c='911 System','911',tar.SolutionCategory__c)


into Opportunity_Tritech_SFDC_UpdateOrder_Type

 from Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld

 left join Tritech_PROD.dbo.Opportunity opp
 on opp.Id=ld.Legacy_Opportunity_ID__c

 left join Staging_SB.dbo.Tritech_OpportunityTypeMapping map
 on map.[TT Value]=opp.Deal_Type__c and map.[Field API]='Deal_Type__c'

 left join Superion_FULLSB.dbo.Opportunity tar
 on tar.id=ld.id

 where ld.ERROR='Operation Successful.' 

;

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateOrder_Type' 

/*
Salesforce object Opportunity does not contain column Deal_Type__c_orig
Salesforce object Opportunity does not contain column Mapping_TT_Value
Salesforce object Opportunity does not contain column Order_Type__c_sup
Salesforce object Opportunity does not contain column SolutionCategory__c_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateOrder_Type'

Select * into Opportunity_Tritech_SFDC_UpdateOrder_Type_Errors
from Opportunity_Tritech_SFDC_UpdateOrder_Type
where error<>'Operation Successful.'--2232

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateOrder_Type_Errors'

--1 error Entity is deleted.

----------------------------------------------------------------------------

Select 
 Id = ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Z_Forecast_Category__c_orig=opp.Z_Forecast_Category__c
,StageName_orig=opp.StageName
,Deal_Forecast__c_sup=sup.Deal_Forecast__c
,CASE 
 WHEN opp.StageName='Booked' THEN 'Commit'
 WHEN opp.StageName='Lost'THEN 'Omit'
 END as Deal_Forecast__c
,Legacy_Opportunity_ID__c_orig=opp.Id

into Opportunity_Tritech_SFDC_UpdateDealForecastField

from Tritech_PROD.dbo.Opportunity opp
inner join Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld
on opp.Id=ld.Legacy_Opportunity_ID__c
left join Superion_FULLSB.dbo.Opportunity sup
on sup.Legacy_Opportunity_ID__c=opp.Id
where ld.ERROR='Operation Successful.'
and opp.Z_Forecast_Category__c IS NULL
and opp.StageName IN ('Booked','Lost');--5751

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateDealForecastField' 

/*
Salesforce object Opportunity does not contain column Z_Forecast_Category__c_orig
Salesforce object Opportunity does not contain column StageName_orig
Salesforce object Opportunity does not contain column Deal_Forecast__c_sup
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateDealForecastField'

------------------------------------------------------------------------------------------

Select
 
 Id=sup.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Deal_Forecast__c_sup=sup.Deal_Forecast__c
,Dealcommitupsidestatus__c_sup=sup.Dealcommitupsidestatus__c
,StageName_sup=sup.StageName
,IsWon_sup=sup.IsWon
,IsClosed_sup=sup.IsClosed
,CASE
 WHEN sup.IsWon='True' THEN 'Commit'
 WHEN sup.IsClosed='True' AND sup.IsWon='False' THEN 'Omit'
 WHEN sup.IsClosed='False' and sup.Dealcommitupsidestatus__c='Upside' THEN 'Best'
 WHEN sup.IsClosed='False' and sup.Dealcommitupsidestatus__c='Expected' THEN 'Likely'
 WHEN sup.IsClosed='False' and sup.Dealcommitupsidestatus__c='Pipeline' THEN 'Pipeline'
 WHEN sup.IsClosed='False' and sup.Dealcommitupsidestatus__c='No Deal' THEN 'Omit'
 ELSE 'Best'
 END as Deal_Forecast__c

 into Opportunity_Tritech_SFDC_UpdateDealForecastNonMigratedRecords

from Superion_FULLSB.dbo.Opportunity sup
where Migrated_Record__c='False';

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateDealForecastNonMigratedRecords' 

/*
Salesforce object Opportunity does not contain column Deal_Forecast__c_sup
Salesforce object Opportunity does not contain column Dealcommitupsidestatus__c_sup
Salesforce object Opportunity does not contain column StageName_sup
Salesforce object Opportunity does not contain column IsWon_sup
Salesforce object Opportunity does not contain column IsClosed_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateDealForecastNonMigratedRecords'

Select * into Opportunity_Tritech_SFDC_UpdateDealForecastNonMigratedRecords_Errors
from Opportunity_Tritech_SFDC_UpdateDealForecastNonMigratedRecords where error<>'Operation Successful.'--105

--Exec SF_BulkOps 'Update:batchsize(1)','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateDealForecastNonMigratedRecords_Errors'

-------------------------------------------------------------------------------------
--Drop table Opportunity_Tritech_SFDC_UpdateDealForecastNew30Jan19
Use Staging_SB;

Select 
 Id = ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Z_Forecast_Category__c_orig=opp.Z_Forecast_Category__c
,StageName_orig=opp.StageName
,StageName_sup=sup.StageName
,IsClosed_orig=opp.IsClosed
,IsClosed_sup=sup.IsClosed
,IsWon_orig=opp.IsWon
,IsWon_sup=sup.IsWon
,Deal_Forecast__c_sup=sup.Deal_Forecast__c
,CASE 
 WHEN opp.Z_Forecast_Category__c='Commit' THEN 'Commit'
-- WHEN opp.Z_Forecast_Category__c='Best' THEN 'Likely'
 WHEN opp.Z_Forecast_Category__c='Pipeline' THEN 'Best'
 WHEN opp.Z_Forecast_Category__c='Omitted' THEN 'Omit'
 WHEN opp.Z_Forecast_Category__c='Best Case' THEN 'Likely'
-- WHEN opp.Z_Forecast_Category__c='Best Deal' THEN 'Likely'
-- WHEN opp.Z_Forecast_Category__c='Closed' THEN 'Commit'
 WHEN opp.Z_Forecast_Category__c='Closed'  and opp.isWon='True' THEN 'Commit'
 WHEN opp.Z_Forecast_Category__c='Closed'  and opp.isWon='False' THEN 'Omit'
 WHEN opp.Z_Forecast_Category__c IS NULL AND opp.StageName='Booked' THEN 'Commit'
 WHEN opp.Z_Forecast_Category__c IS NULL AND opp.StageName='Lost' THEN 'Omit'
 WHEN opp.Z_Forecast_Category__c IS NULL AND opp.StageName='Closed Lost - No Bid' THEN 'Omit'
 WHEN opp.Z_Forecast_Category__c IS NULL AND opp.StageName='Closed Lost - Other' THEN 'Omit'
 WHEN opp.Z_Forecast_Category__c IS NULL AND opp.StageName='Disqualified' THEN 'Omit'
 WHEN opp.Z_Forecast_Category__c IS NULL AND opp.IsClosed='False' THEN 'Best'
 WHEN opp.IsClosed='True' AND opp.IsWon='True' THEN 'Commit'
 WHEN opp.IsClosed='True' AND opp.IsWon='False' THEN 'Omit'
 END as Deal_Forecast__c
,Legacy_Opportunity_ID__c_orig=opp.Id

into Opportunity_Tritech_SFDC_UpdateDealForecastNew30Jan19

from Tritech_PROD.dbo.Opportunity opp
inner join Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld
on opp.Id=ld.Legacy_Opportunity_ID__c
left join Superion_FULLSB.dbo.Opportunity sup
on sup.Legacy_Opportunity_ID__c=opp.Id
where ld.ERROR='Operation Successful.';

Select * from Opportunity_Tritech_SFDC_UpdateDealForecastNew30Jan19
where Deal_Forecast__c IS NULL;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateDealForecastNew30Jan19' 

/*
Salesforce object Opportunity does not contain column Z_Forecast_Category__c_orig
Salesforce object Opportunity does not contain column StageName_orig
Salesforce object Opportunity does not contain column StageName_sup
Salesforce object Opportunity does not contain column IsClosed_orig
Salesforce object Opportunity does not contain column IsClosed_sup
Salesforce object Opportunity does not contain column IsWon_orig
Salesforce object Opportunity does not contain column IsWon_sup
Salesforce object Opportunity does not contain column Deal_Forecast__c_sup
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateDealForecastNew30Jan19'

-----------------------------------------------------------------------

--Drop table Opportunity_Tritech_SFDC_UpdateDescription5Feb19;

Select 

 Id=ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Opportunity_ID=ld.Legacy_Opportunity_ID__c
,Business_Issues_WMP__c_orig=ld.Customer_s_Defined_Initiatives__c
,Customer_s_Defined_Initiatives_sup=tar.Customer_s_Defined_Initiatives__c
,Description_Sup=tar.Description
,Description_orig=ld.Description
,CASE 
 WHEN ld.Customer_s_Defined_Initiatives__c IS NOT NULL AND ld.Description IS NOT NULL THEN CONCAT(ld.Description,CHAR(13)+CHAR(10),CHAR(13)+CHAR(10),'Business Issues:',ld.Customer_s_Defined_Initiatives__c)
 WHEN ld.Customer_s_Defined_Initiatives__c IS NOT NULL AND ld.Description IS NULL THEN CONCAT('Business Issues:',ld.Customer_s_Defined_Initiatives__c)
 END as Description  

 --into Opportunity_Tritech_SFDC_UpdateDescription5Feb19

from 
Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld

inner join Superion_FULLSB.dbo.Opportunity tar
on tar.Id=ld.Id

where error='Operation Successful.'
and ld.Customer_s_Defined_Initiatives__c IS NOT NULL;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateDescription5Feb19' 

/*
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID
Salesforce object Opportunity does not contain column Business_Issues_WMP__c_orig
Salesforce object Opportunity does not contain column Customer_s_Defined_Initiatives_sup
Salesforce object Opportunity does not contain column Description_Sup
Salesforce object Opportunity does not contain column Description_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateDescription5Feb19'

---------------------------------------------------------------------------

Select 

 Id=ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,VP_Notes__c=opp.VP_Notes__c

from Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld

left join Tritech_PROD.dbo.Opportunity opp
on opp.Id=ld.Legacy_Opportunity_ID__c

where opp.VP_Notes__c IS NOT NULL;

Select VP_Notes__c from Tritech_PROD.dbo.Opportunity
where VP_Notes__c is not null;

---------------------------------------------------------------------

Select 

 Id=ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Opportunity_ID=ld.Legacy_Opportunity_ID__c
,Total_Contract_Value_Forecast__c_sup=tar.Total_Contract_Value_Forecast__c
,Total_Contract_Value_Forecast__c=opp.Actual_Opportunity_Value__c
  

 into Opportunity_Tritech_SFDC_UpdateTCVForecast11Feb19

from 
Staging_SB.dbo.Opportunity_Tritech_SFDC_Load ld

inner join Superion_FULLSB.dbo.Opportunity tar
on tar.Id=ld.Id

inner join Tritech_PROD.dbo.Opportunity opp
on opp.Id=ld.Legacy_Opportunity_ID__c

where error='Operation Successful.';--18981

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Opportunity_Tritech_SFDC_UpdateTCVForecast11Feb19' 

/*
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID
Salesforce object Opportunity does not contain column Total_Contract_Value_Forecast__c_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Opportunity_Tritech_SFDC_UpdateTCVForecast11Feb19'