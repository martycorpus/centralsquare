--TT_Survey_Contact__c Hotfix

Select 
 Id=a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,TT_Survey_Contact__c=a.Customer_Survey_Contact__c
,Legacy_id__c_orig=a.Legacy_Id__c

into Contact_Tritech_SFDC_UpdateTTSurveyContact

from Staging_SB.dbo.Contact_Tritech_SFDC_Load a;--81704

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Contact_Tritech_SFDC_UpdateTTSurveyContact'
/*
Salesforce object Contact does not contain column Legacy_id__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Contact_Tritech_SFDC_UpdateTTSurveyContact'

----------------------------------------------------------------------------------------

--Support_Contact_Type__c Hotfix

Select 
 
 Id=ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Id__c_orig=a.Legacy_Id__c
,After_Hours_Contact_WMP__c_orig=After_Hours_Contact_WMP__c
,GIS_Contact__c_orig=GIS_Contact__c
,IT_Contact__c_orig=IT_Contact__c
,MR_Contact__c_orig=MR_Contact__c
,NIBRS_Contact__c_orig=NIBRS_Contact__c
,Department_tarval=ld.Department
,Department=IIF(ld.Department IS NULL,'Other',ld.Department)
,Support_Contact_Type__c=STUFF
(
CONCAT
 (
 IIF(After_Hours_Contact_WMP__c_pickval IS NULL,NULL,CONCAT(';',After_Hours_Contact_WMP__c_pickval)),
 IIF(GIS_Contact__c_pickval IS NULL,NULL,CONCAT(';',GIS_Contact__c_pickval)),
 IIF(IT_Contact__c_pickval IS NULL,NULL,CONCAT(';',IT_Contact__c_pickval)),
 IIF(MR_Contact__c_pickval IS NULL,NULL,CONCAT(';',MR_Contact__c_pickval)),
 IIF(NIBRS_Contact__c_pickval IS NULL,NULL,CONCAT(';',NIBRS_Contact__c_pickval))
 ),
 1,
 1,
 ''
)
 

 --into Contact_Tritech_SFDC_UpdateSupportContactType
 
from Contact_Tritech_SFDC_UpdateSupport_Contact_Type__cIntermediate a
inner join Staging_SB.dbo.Contact_Tritech_SFDC_Load ld
on ld.Legacy_Id__c=a.Legacy_Id__c
where ld.ERROR='Operation Successful.'
and (After_Hours_Contact_WMP__c_pickval IS NOT NULL 
  or GIS_Contact__c_pickval IS NOT NULL 
  or IT_Contact__c_pickval IS NOT NULL
  or MR_Contact__c_pickval IS NOT NULL
  or NIBRS_Contact__c_pickval IS NOT NULL
  or ld.Department IS NULL
)--76220


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Contact_Tritech_SFDC_UpdateSupportContactType'

/*
Salesforce object Contact does not contain column Legacy_Id__c_orig
Salesforce object Contact does not contain column After_Hours_Contact_WMP__c_orig
Salesforce object Contact does not contain column GIS_Contact__c_orig
Salesforce object Contact does not contain column IT_Contact__c_orig
Salesforce object Contact does not contain column MR_Contact__c_orig
Salesforce object Contact does not contain column NIBRS_Contact__c_orig
Salesforce object Contact does not contain column Department_tarval
*/

Select * from Contact_Tritech_SFDC_UpdateSupportContactType;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Contact_Tritech_SFDC_UpdateSupportContactType'

----------------------------------------------------------------------------------------------

Select 
 Support_Contact_Type__c_upd=typ.Support_Contact_Type__c
,Support_Contact_Type__c_load=ld.Support_Contact_Type__c
,ID=ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,CASE When typ.Support_Contact_Type__c IS NULL THEN 'Primary Contact'
ELSE CONCAT(typ.Support_Contact_Type__c,';','Primary Contact')
END as Support_Contact_Type__c

into Contact_Tritech_SFDC_UpdateTTSurveyContact2

from Contact_Tritech_SFDC_UpdateSupportContactType typ
inner join Contact_Tritech_SFDC_Load ld
on ld.Id=typ.Id
and ld.Support_Contact_Type__c like 'P%';--800 rows

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Contact_Tritech_SFDC_UpdateTTSurveyContact2'
/*
Salesforce object Contact does not contain column Support_Contact_Type__c_upd
Salesforce object Contact does not contain column Support_Contact_Type__c_load
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Contact_Tritech_SFDC_UpdateTTSurveyContact2'

--------------------------------------------------------------------------------------------------

Select LeadSource from Tritech_PROD.dbo.Contact;

Select 
 Id=a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,LeadSource_orig=leg.LeadSource
,LeadSource=trnsfrmval.TargetLeadSource
,Legacy_id__c_orig=a.Legacy_Id__c

into Contact_Tritech_SFDC_UpdateLeadSource

from Tritech_PROD.dbo.Contact leg

inner join  Staging_SB.dbo.Contact_Tritech_SFDC_Load a
on a.Legacy_Id__c=leg.Id

left join Staging_SB.dbo.Tritech_map_Contact_LeadSource trnsfrmval
on trnsfrmval.LegacyLeadSource=leg.LeadSource

where leg.LeadSource is not null;--12037

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Contact_Tritech_SFDC_UpdateLeadSource'
/*
Salesforce object Contact does not contain column LeadSource_orig
Salesforce object Contact does not contain column Legacy_id__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Contact_Tritech_SFDC_UpdateLeadSource'

-----------------------------------------------------------------------------------------------

WITH ContactScope As
(
Select distinct Contactid from Staging_SB.dbo.case_Tritech_SFDC_load 
)

Select  

 Id=ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c_orig=ld.Legacy_Id__c
,Department1__c_sup=supcon.Department1__c
,Department1__c='Other'
,Support_Contact_Type__c_sup=supcon.Support_Contact_Type__c
,Support_Contact_Type__c=IIF(supcon.Support_Contact_Type__c IS NULL,'Case Contact',supcon.Support_Contact_Type__c)

into Contact_Tritech_SFDC_UpdateDepartmentSupportContactType

from Staging_SB.dbo.Contact_Tritech_SFDC_Load ld

inner join ContactScope on
ContactScope.Contactid=ld.Id

left join Superion_FULLSB.dbo.Contact supcon
on supcon.Id=ld.Id;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Contact_Tritech_SFDC_UpdateDepartmentSupportContactType'

/*
Salesforce object Contact does not contain column Legacy_id__c_orig
Salesforce object Contact does not contain column Department1__c_sup
Salesforce object Contact does not contain column Support_Contact_Type__c_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Contact_Tritech_SFDC_UpdateDepartmentSupportContactType'

---------------------------------------------------------------------------------------------------

Select 

 Id=a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Executive_Contact__c_orig=a.Executive_Contact__c_orig
,Executive_Contact__c_tt=leg.Executive_Contact__c
,Marketing_Contact__c_tt=leg.Marketing_Contact__c
,Marketing_Contact__c_orig=a.Marketing_Contact__c_orig
,CASE
 WHEN a.Executive_Contact__c_orig='True'  THEN 'Director/Chief/Owner'
 WHEN  a.Marketing_Contact__c_orig='True' THEN 'Marketing Contact'
 WHEN a.Executive_Contact__c_orig='False' AND a.Marketing_Contact__c_orig='False' THEN leg.Role_WMP__c
 END as Role__c
,Role_WMP__c_tt=leg.Role_WMP__c
,Role_sup=tar.Role__c
,LegacyId=a.Legacy_Id__c

into Contact_Tritech_SFDC_UpdateRole11Feb19

from Staging_SB.dbo.Contact_Tritech_SFDC_Load a
inner join Tritech_PROD.dbo.Contact leg
on leg.Id=a.Legacy_Id__c
left join Superion_FULLSB.dbo.Contact tar
on tar.Id=a.Id;--

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Contact_Tritech_SFDC_UpdateRole11Feb19'
/*
Salesforce object Contact does not contain column Executive_Contact__c_orig
Salesforce object Contact does not contain column Executive_Contact__c_tt
Salesforce object Contact does not contain column Marketing_Contact__c_tt
Salesforce object Contact does not contain column Marketing_Contact__c_orig
Salesforce object Contact does not contain column Role_WMP__c_tt
Salesforce object Contact does not contain column Role_sup
Salesforce object Contact does not contain column LegacyId
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Contact_Tritech_SFDC_UpdateRole11Feb19'

------------------------------------------------------------------------

DECLARE @Owner NVARCHAR(18) = (Select top 1 Id from Superion_FULLSB.dbo.[User] 
where Id='0056A000000GyGbQAK' and Name='Deborah Solorzano')

Select 

 Id=a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,OwnerId_sup=tar.OwnerId
,OwnerId=@Owner
,LegacyId=a.Legacy_Id__c

--into Contact_Tritech_SFDC_UpdateOwner8Feb19

from Staging_SB.dbo.Contact_Tritech_SFDC_Load a
inner join Tritech_PROD.dbo.Contact leg
on leg.Id=a.Legacy_Id__c
left join Superion_FULLSB.dbo.Contact tar
on tar.Id=a.Id;--81680

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Contact_Tritech_SFDC_UpdateOwner8Feb19'
/*
Salesforce object Contact does not contain column OwnerId_sup
Salesforce object Contact does not contain column LegacyId
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Contact_Tritech_SFDC_UpdateOwner8Feb19'


