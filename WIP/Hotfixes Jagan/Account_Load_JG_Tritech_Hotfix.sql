--Fire_RMS_Current_SW_Vendor__c hotfix

--Drop table Account_Tritech_SFDC_UpdateFire_RMS_Current_SW_Vendor__c

Select 
 Id=a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Fire_RMS_Current_SW_Vendor__c=a.Fire_RMS_Current_SW_Vendor__c
,Legacy_id__c_orig=a.LegacySFDCAccountId__c

 into  Account_Tritech_SFDC_UpdateFireRMSCurrentSWVendor

from Staging_SB.dbo.Account_Tritech_SFDC_Load a;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateFireRMSCurrentSWVendor' 

/*
Salesforce object Account does not contain column Legacy_id__c_orig
*/

Select * from Account_Tritech_SFDC_UpdateFireRMSCurrentSWVendor;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateFireRMSCurrentSWVendor'

Select * into Account_Tritech_SFDC_UpdateFireRMSCurrentSWVendor_errors
from Account_Tritech_SFDC_UpdateFireRMSCurrentSWVendor where
error<>'Operation Successful.'--200

--Exec SF_BulkOps 'Update:batchsize(1)','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateFireRMSCurrentSWVendor_errors'

----------------------------------------------------------------------------------------------------------

-------------------------------------------------ACCOUNT HOTFIX-----------------------------------------------------------
select b.id
, b.error
, client__c = a.client__c
 ,Customer_Success_Liaison__c_orig = a.Customer_Success_Liaison__c
 ,d.client__c as client__C_orig
 , d.AAM_PSJ__c as customer_Success_liaison__c_orig1
,AAM_PSJ__c= c.name
--into Account_Tritech_SFDC_load_Update_hotfix
from Tritech_prod.dbo.Account a
inner join Account_Tritech_SFDC_load b
on b.LegacySFDCAccountId__c= a.id
left join Tritech_prod.dbo.[user] c
on c.id = a.Customer_Success_Liaison__c
left join Superion_Fullsb.dbo.Account d
on b.id=d.id
and b.error='Operation Successful.'
where a.client__c ='True' or c.name is not null
--(4926 row(s) affected)

select * from Account_Tritech_SFDC_load_Update_hotfix
--Exec SF_BulkOps 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_load_Update_hotfix_errors' 


select Error, count(*) from Account_Tritech_SFDC_load_Update_hotfix_errors
group by error

select * into Account_Tritech_SFDC_load_Update_hotfix_errors
from Account_Tritech_SFDC_load_Update_hotfix 
where error <>'Operation Successful.'
-- Errors because of Restricted Picklist.
----------------------------------------------------------------------------------------

--IQ_Account_Owner__c,X911_Account_Owner__c hotfix

--Drop table Account_Tritech_SFDC_UpdateX911_Account_OwnerIQ_Account_Owner

Select 
 Id=a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,AE_IQ__c_supval=supval.AE_IQ__c
,IQ_Account_Owner__c_orig=leg.IQ_Account_Owner__c
,AE_IQ__c=iqusr.[Name]
,AE_911__c_supval=supval.AE_911__c
,X911_Account_Owner__c_orig=leg.X911_Account_Owner__c
,AE_911__c=x911usr.[Name]
,Legacy_id__c_orig=a.LegacySFDCAccountId__c

-- into  Account_Tritech_SFDC_UpdateX911_Account_OwnerIQ_Account_Owner
--Select count(*)
from Staging_SB.dbo.Account_Tritech_SFDC_Load a

left join Tritech_PROD.dbo.Account leg
on leg.Id=a.LegacySFDCAccountId__c

left join Superion_FULLSB.dbo.[User] iqusr
on iqusr.Legacy_Tritech_Id__c=leg.IQ_Account_Owner__c

left join Superion_FULLSB.dbo.[User] x911usr
on x911usr.Legacy_Tritech_Id__c=leg.X911_Account_Owner__c

left join Superion_FULLSB.dbo.Account supval
on supval.Id=a.ID

where leg.X911_Account_Owner__c is not null or leg.IQ_Account_Owner__c is not null;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateX911_Account_OwnerIQ_Account_Owner' 

/*
Salesforce object Account does not contain column AE_IQ__c_supval
Salesforce object Account does not contain column IQ_Account_Owner__c_orig
Salesforce object Account does not contain column AE_911__c_supval
Salesforce object Account does not contain column X911_Account_Owner__c_orig
Salesforce object Account does not contain column Legacy_id__c_orig
*/

Select * from Account_Tritech_SFDC_UpdateX911_Account_OwnerIQ_Account_Owner;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateX911_Account_OwnerIQ_Account_Owner'

-----------------------------------------------------------------------------------------

--MergedAccounts CADVendor Hotfix

Select 
 ID = sup.Id
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,CAD_Vendor__c_sup=sup.CurrentCADSWVendor__c
,CAD_Vendor__c_tt=leg.CAD_Vendor__c
,CAD_Vendor__c_tt_id=cadvendor.SandboxID
,CurrentCADSWVendor__c=IIF(sup.CurrentCADSWVendor__c IS NULL,cadvendor.SandboxID,sup.CurrentCADSWVendor__c)
 
 into Account_Tritech_SFDC_UpdateMergeAccountCADVendor

from Superion_FULLSB.dbo.Account sup
 
inner join Account_Tritech_SFDC_LatestUpdateMergingAccounts mrgacnt
on mrgacnt.Id=sup.Id

left join Tritech_PROD.dbo.Account leg
on leg.Id=sup.LegacySFDCAccountId__c

--Fetching CAD_Vendor__cId (Competitor Lookup)
left join Staging_SB.dbo.map_Competitor_Vendor cadvendor on 
cadvendor.TriTech_Competitor=leg.CAD_Vendor__c

 where sup.CurrentCADSWVendor__c is null and leg.CAD_Vendor__c is not null;--40

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateMergeAccountCADVendor' 

/*
Salesforce object Account does not contain column CAD_Vendor__c_sup
Salesforce object Account does not contain column CAD_Vendor__c_tt
Salesforce object Account does not contain column CAD_Vendor__c_tt_id
*/

Select * from Account_Tritech_SFDC_UpdateMergeAccountCADVendor;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateMergeAccountCADVendor'

--------------------------------------------------------------------------------------

Select 
 ID = sup.Id
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Last_SW_Purchase_Date_CAD__c_sup=sup.Last_SW_Purchase_Date_CAD__c
,Last_SW_Purchase_Date_CAD__c_tt=leg.CAD_Installed_Year__c
,Last_SW_Purchase_Date_CAD__c=IIF(sup.Last_SW_Purchase_Date_CAD__c is null,leg.CAD_Installed_Year__c,sup.Last_SW_Purchase_Date_CAD__c)
,Alarm_Mgmt_Current_SW_Vendor__c_sup=sup.Alarm_Mgmt_Current_SW_Vendor__c
,Alarm_Mgmt_Current_SW_Vendor__c_tt=leg.Alarm_Management_Vendor__c
,Alarm_Mgmt_Current_SW_Vendor__c_tt_id=alrmvendor.SandboxID
,Alarm_Mgmt_Current_SW_Vendor__c=IIF(sup.Alarm_Mgmt_Current_SW_Vendor__c is null,alrmvendor.SandboxID,sup.Alarm_Mgmt_Current_SW_Vendor__c)
 
 into Account_Tritech_SFDC_UpdateMergeAccountAlarmVendorLastSWPurchaseDate

from Superion_FULLSB.dbo.Account sup
 
inner join Account_Tritech_SFDC_LatestUpdateMergingAccounts mrgacnt
on mrgacnt.Id=sup.Id

left join Tritech_PROD.dbo.Account leg
on leg.Id=sup.LegacySFDCAccountId__c

--Fetching Alarm_Mgmt_Current_SW_Vendor__cId (Competitor Lookup)
left join Staging_SB.dbo.map_Competitor_Vendor alrmvendor on 
alrmvendor.TriTech_Competitor=leg.Alarm_Management_Vendor__c

 where (sup.Last_SW_Purchase_Date_CAD__c is null and leg.CAD_Installed_Year__c is not null)
 or (sup.Alarm_Mgmt_Current_SW_Vendor__c is null and leg.Alarm_Management_Vendor__c is not null);--40

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateMergeAccountAlarmVendorLastSWPurchaseDate' 

/*
Salesforce object Account does not contain column Last_SW_Purchase_Date_CAD__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_CAD__c_tt
Salesforce object Account does not contain column Alarm_Mgmt_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Alarm_Mgmt_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column Alarm_Mgmt_Current_SW_Vendor__c_tt_id
*/

Select * from Account_Tritech_SFDC_UpdateMergeAccountAlarmVendorLastSWPurchaseDate;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateMergeAccountAlarmVendorLastSWPurchaseDate'

--------------------------------------------------------------------------------------------------

--Drop table Account_Tritech_SFDC_UpdateCompetitors

Select  
 Id=ld.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))  

,CAD_Vendor__c_orig
,CurrentCADSWVendor__c_orig=ld.CurrentCADSWVendor__c
,CurrentCADSWVendor__c_sup=sup.CurrentCADSWVendor__c
,CurrentCADSWVendor__c=compCAD.[Sandbox Id]

,Jail_Vendor__c_orig
,Current_Justice_SW_Vendor__c_orig=ld.Current_Justice_SW_Vendor__c
,Current_Justice_SW_Vendor__c_sup=sup.Current_Justice_SW_Vendor__c
,Current_Justice_SW_Vendor__c=compJUS.[Sandbox Id]

,Mobile_Vendor__c_orig
,Current_Mobile_SW_Vendor__c_orig=ld.Current_Mobile_SW_Vendor__c
,Current_Mobile_SW_Vendor__c_sup=sup.Current_Mobile_SW_Vendor__c
,Current_Mobile_SW_Vendor__c=compMOB.[Sandbox Id]

,Billing_Vendor__c_orig
,Billing_Current_SW_Vendor__c_orig=ld.Billing_Current_SW_Vendor__c
,Billing_Current_SW_Vendor__c_sup=sup.Billing_Current_SW_Vendor__c
,Billing_Current_SW_Vendor__c=compBILL.[Sandbox Id]

,ePCR_Current_SW_Vendor__c_orig
,ePCR_Current_SW_Vendor__c_orig_id=ld.ePCR_Current_SW_Vendor__c
,ePCR_Current_SW_Vendor__c_sup=sup.ePCR_Current_SW_Vendor__c
,ePCR_Current_SW_Vendor__c=compEPCR.[Sandbox Id]

,FBR_Current_SW_Vendor__c_orig
,FBR_Current_SW_Vendor__c_orig_id=ld.FBR_Current_SW_Vendor__c
,FBR_Current_SW_Vendor__c_sup=sup.FBR_Current_SW_Vendor__c
,FBR_Current_SW_Vendor__c=compFBR.[Sandbox Id]

,Fire_RMS_Current_SW_Vendor__c_orig
,Fire_RMS_Current_SW_Vendor__c_orig_id=ld.Fire_RMS_Current_SW_Vendor__c
,Fire_RMS_Current_SW_Vendor__c_sup=sup.Fire_RMS_Current_SW_Vendor__c
,Fire_RMS_Current_SW_Vendor__c=compFIRE.[Sandbox Id]

,Mapping_Current_SW_Vendor__c_orig
,Mapping_Current_SW_Vendor__c_orig_id=ld.Mapping_Current_SW_Vendor__c
,Mapping_Current_SW_Vendor__c_sup=sup.Mapping_Current_SW_Vendor__c
,Mapping_Current_SW_Vendor__c=compMAP.[Sandbox Id]

,Law_RMS_Current_SW_Vendor__c_orig
,Law_RMS_Current_SW_Vendor__c_orig_id=ld.Law_RMS_Current_SW_Vendor__c
,Law_RMS_Current_SW_Vendor__c_sup=sup.Law_RMS_Current_SW_Vendor__c
,Law_RMS_Current_SW_Vendor__c=compLAW.[Sandbox Id]

,X911_Current_SW_Vendor__c_orig
,X911_Current_SW_Vendor__c_orig_id=ld.X911_Current_SW_Vendor__c
,X911_Current_SW_Vendor__c_sup=sup.X911_Current_SW_Vendor__c
,X911_Current_SW_Vendor__c=compX911.[Sandbox Id]

into Account_Tritech_SFDC_UpdateCompetitors

 from Staging_SB.dbo.Account_Tritech_SFDC_Load ld

 left join Superion_FULLSB.dbo.Account sup
 on sup.Id=ld.LegacySFDCAccountId__c

 left join Staging_SB.dbo.TotalCompetitors compCAD
 on cast(ld.CAD_Vendor__c_orig as nvarchar(max))=compCAD.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compJUS
 on cast(ld.Jail_Vendor__c_orig as nvarchar(max))=compJUS.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compMOB
 on cast(ld.Mobile_Vendor__c_orig as nvarchar(max))=compMOB.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compBILL
 on cast(ld.Billing_Vendor__c_orig as nvarchar(max))=compBILL.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compEPCR
 on cast(ld.ePCR_Current_SW_Vendor__c_orig as nvarchar(max))=compEPCR.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compFBR
 on cast(ld.FBR_Current_SW_Vendor__c_orig as nvarchar(max))=compFBR.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compFIRE
 on cast(ld.Fire_RMS_Current_SW_Vendor__c_orig as nvarchar(max))=compFIRE.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compMAP
 on cast(ld.Mapping_Current_SW_Vendor__c_orig as nvarchar(max))=compMAP.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compLAW
 on cast(ld.Law_RMS_Current_SW_Vendor__c_orig as nvarchar(max))=compLAW.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compX911
 on cast(ld.X911_Current_SW_Vendor__c_orig as nvarchar(max))=compX911.CompetitorName

 where (CAD_Vendor__c_orig is not null and ld.CurrentCADSWVendor__c is null)
    or (Jail_Vendor__c_orig is not null and ld.Current_Justice_SW_Vendor__c is null )
	or (Mobile_Vendor__c_orig is not null and ld.Current_Mobile_SW_Vendor__c is null )
	or (Billing_Vendor__c_orig is not null and ld.Billing_Current_SW_Vendor__c is null )
	or (ePCR_Current_SW_Vendor__c_orig is not null and ld.ePCR_Current_SW_Vendor__c is null)
	or (FBR_Current_SW_Vendor__c_orig is not null and ld.FBR_Current_SW_Vendor__c is null)
	or (Fire_RMS_Current_SW_Vendor__c_orig is not null and ld.Fire_RMS_Current_SW_Vendor__c is null)
	or (Mapping_Current_SW_Vendor__c_orig is not null and ld.Mapping_Current_SW_Vendor__c is null)
	or (Law_RMS_Current_SW_Vendor__c_orig is not null and ld.Law_RMS_Current_SW_Vendor__c is null)
	or (X911_Current_SW_Vendor__c_orig is not null and ld.X911_Current_SW_Vendor__c is null);--1564

  --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateCompetitors' 

/*
Salesforce object Account does not contain column CAD_Vendor__c_orig
Salesforce object Account does not contain column CurrentCADSWVendor__c_orig
Salesforce object Account does not contain column CurrentCADSWVendor__c_sup
Salesforce object Account does not contain column Jail_Vendor__c_orig
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_orig
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_sup
Salesforce object Account does not contain column Mobile_Vendor__c_orig
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_orig
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_sup
Salesforce object Account does not contain column Billing_Vendor__c_orig
Salesforce object Account does not contain column Billing_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Billing_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_orig_id
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_orig_id
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_orig_id
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_orig_id
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_orig_id
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_orig_id
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateCompetitors'

--------------------------------------------------------------------------------------------

Select  
 Id=mrgacc.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))  

,X911_Current_SW_Vendor__c_sup=supacc.X911_Current_SW_Vendor__c
,X911_Current_SW_Vendor__c_tt=pre.X911_Current_SW_Vendor__c_orig
,X911_Current_SW_Vendor__c_tt_id=pre.X911_Current_SW_Vendor__c
,X911_Current_SW_Vendor__c=compX911.[Sandbox Id]

,Billing_Current_SW_Vendor__c_sup=supacc.Billing_Current_SW_Vendor__c
,Billing_Current_SW_Vendor__c_tt=pre.Billing_Vendor__c_orig
,Billing_Current_SW_Vendor__c_tt_id=pre.Billing_Current_SW_Vendor__c
,Billing_Current_SW_Vendor__c=compBILL.[Sandbox Id]

,ePCR_Current_SW_Vendor__c_sup=supacc.ePCR_Current_SW_Vendor__c
,ePCR_Current_SW_Vendor__c_tt=pre.ePCR_Current_SW_Vendor__c_orig
,ePCR_Current_SW_Vendor__c_tt_id=pre.ePCR_Current_SW_Vendor__c
,ePCR_Current_SW_Vendor__c=compEPCR.[Sandbox Id]

,FBR_Current_SW_Vendor__c_sup=supacc.FBR_Current_SW_Vendor__c
,FBR_Current_SW_Vendor__c_tt=pre.ePCR_Current_SW_Vendor__c_orig
,FBR_Current_SW_Vendor__c_tt_id=pre.FBR_Current_SW_Vendor__c
,FBR_Current_SW_Vendor__c=compFBR.[Sandbox Id]

,Fire_RMS_Current_SW_Vendor__c_sup=supacc.Fire_RMS_Current_SW_Vendor__c
,Fire_RMS_Current_SW_Vendor__c_tt=pre.Fire_RMS_Current_SW_Vendor__c_orig
,Fire_RMS_Current_SW_Vendor__c_tt_id=pre.Fire_RMS_Current_SW_Vendor__c
,Fire_RMS_Current_SW_Vendor__c=compFIRE.[Sandbox Id]

,Law_RMS_Current_SW_Vendor__c_sup=supacc.Law_RMS_Current_SW_Vendor__c
,Law_RMS_Current_SW_Vendor__c_tt=pre.Law_RMS_Current_SW_Vendor__c_orig
,Law_RMS_Current_SW_Vendor__c_tt_id=pre.Law_RMS_Current_SW_Vendor__c
,Law_RMS_Current_SW_Vendor__c=compLAW.[Sandbox Id]

,Mapping_Current_SW_Vendor__c_sup=supacc.Mapping_Current_SW_Vendor__c
,Mapping_Current_SW_Vendor__c_tt=pre.Mapping_Current_SW_Vendor__c_orig
,Mapping_Current_SW_Vendor__c_tt_id=pre.Mapping_Current_SW_Vendor__c
,Mapping_Current_SW_Vendor__c=compMAP.[Sandbox Id]

,CurrentCADSWVendor__c_sup=supacc.CurrentCADSWVendor__c
,CurrentCADSWVendor__c_tt=pre.CAD_Vendor__c_orig
,CurrentCADSWVendor__c_tt_id=pre.CurrentCADSWVendor__c
,CurrentCADSWVendor__c=IIF(supacc.CurrentCADSWVendor__c IS NULL,compCAD.[Sandbox Id],supacc.CurrentCADSWVendor__c)

,Current_Mobile_SW_Vendor__c_sup=supacc.Current_Mobile_SW_Vendor__c
,Current_Mobile_SW_Vendor__c_tt_id=pre.Current_Mobile_SW_Vendor__c
,Current_Mobile_SW_Vendor__c_tt=pre.Mobile_Vendor__c_orig
,Current_Mobile_SW_Vendor__c=compMOB.[Sandbox Id]

,Current_Justice_SW_Vendor__c_sup=supacc.Current_Justice_SW_Vendor__c
,Current_Justice_SW_Vendor__c_tt_id=pre.Current_Justice_SW_Vendor__c
,Current_Justice_SW_Vendor__c_tt=pre.Jail_Vendor__c_orig
,Current_Justice_SW_Vendor__c=compJUS.[Sandbox Id]

into Account_Tritech_SFDC_UpdateMergeAccountCompetitors

 from 
 Staging_SB.dbo.Account_Tritech_SFDC_UpdateMergingAccounts mrgacc

 left join Staging_SB.dbo.Account_Tritech_SFDC_Preload pre
 on mrgacc.LegacySFDCAccountId__c=pre.LegacySFDCAccountId__c

 left join Superion_FULLSB.dbo.Account supacc
 on supacc.LegacySFDCAccountId__c=mrgacc.LegacySFDCAccountId__c

 left join Staging_SB.dbo.TotalCompetitors compX911
 on cast(pre.X911_Current_SW_Vendor__c_orig as nvarchar(max))=compX911.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compBILL
 on cast(pre.Billing_Vendor__c_orig as nvarchar(max))=compBILL.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compEPCR
 on cast(pre.ePCR_Current_SW_Vendor__c_orig as nvarchar(max))=compEPCR.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compFBR
 on cast(pre.FBR_Current_SW_Vendor__c_orig as nvarchar(max))=compFBR.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compFIRE
 on cast(pre.Fire_RMS_Current_SW_Vendor__c_orig as nvarchar(max))=compFIRE.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compLAW
 on cast(pre.Law_RMS_Current_SW_Vendor__c_orig as nvarchar(max))=compLAW.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compMAP
 on cast(pre.Mapping_Current_SW_Vendor__c_orig as nvarchar(max))=compMAP.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compCAD
 on cast(pre.CAD_Vendor__c_orig as nvarchar(max))=compCAD.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compMOB
 on cast(pre.Mobile_Vendor__c_orig as nvarchar(max))=compMOB.CompetitorName

 left join Staging_SB.dbo.TotalCompetitors compJUS
 on cast(pre.Jail_Vendor__c_orig as nvarchar(max))=compJUS.CompetitorName

  where (X911_Current_SW_Vendor__c_orig is not null and pre.X911_Current_SW_Vendor__c is null)
     or (Billing_Vendor__c_orig is not null and pre.Billing_Current_SW_Vendor__c is null )
	 or (ePCR_Current_SW_Vendor__c_orig is not null and pre.ePCR_Current_SW_Vendor__c is null)
	 or (FBR_Current_SW_Vendor__c_orig is not null and pre.FBR_Current_SW_Vendor__c is null)
	 or (Fire_RMS_Current_SW_Vendor__c_orig is not null and pre.Fire_RMS_Current_SW_Vendor__c is null)
	 or (Law_RMS_Current_SW_Vendor__c_orig is not null and pre.Law_RMS_Current_SW_Vendor__c is null)
	 or (Mapping_Current_SW_Vendor__c_orig is not null and pre.Mapping_Current_SW_Vendor__c is null)
	 or (CAD_Vendor__c_orig is not null and pre.CurrentCADSWVendor__c is null)
	 or (Mobile_Vendor__c_orig is not null and pre.Current_Mobile_SW_Vendor__c is null )
	 or (Jail_Vendor__c_orig is not null and pre.Current_Justice_SW_Vendor__c is null );--55

	 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateMergeAccountCompetitors' 

/*
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_tt_id
Salesforce object Account does not contain column Billing_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Billing_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column Billing_Current_SW_Vendor__c_tt_id
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_tt_id
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_tt_id
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_tt_id
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_tt_id
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_tt
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_tt_id
Salesforce object Account does not contain column CurrentCADSWVendor__c_sup
Salesforce object Account does not contain column CurrentCADSWVendor__c_tt
Salesforce object Account does not contain column CurrentCADSWVendor__c_tt_id
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_sup
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_tt_id
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_tt
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_sup
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_tt_id
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_tt
*/

     --Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateMergeAccountCompetitors'

	 -------------------------------------------------------------------------------

--Account_Executive_Fin__c,Installed_PA__c Hotfix (OwnerId dependent)

Select 

 Id=ld.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c_orig=ld.LegacySFDCAccountId__c
,OwnerId_leg=acc.OwnerId
,OwnerName_leg=usr.[Name]
,Client__c_leg=acc.Client__c
,Installed_PA__c_sup=supacc.Installed_PA__c
,Installed_PA__c=IIF(acc.Client__c='True',usr.Name,NULL)
,Account_Executive_Fin__c_sup=supacc.Account_Executive_Fin__c
,Account_Executive_Fin__c=IIF(acc.Client__c='False',usr.Name,NULL)

--into Account_Tritech_SFDC_UpdateInstalled_PAAccount_Executive_Fin

from Staging_SB.dbo.Account_Tritech_SFDC_Load ld

left join Tritech_PROD.dbo.Account acc
on acc.Id=ld.LegacySFDCAccountId__c 

left join Tritech_PROD.dbo.[User] usr
on usr.Id=acc.OwnerId

left join Superion_FULLSB.dbo.Account supacc
on supacc.Id=ld.Id

where error='Operation Successful.';

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateInstalled_PAAccount_Executive_Fin' 

/*
Salesforce object Account does not contain column Legacy_id__c_orig
Salesforce object Account does not contain column OwnerId_leg
Salesforce object Account does not contain column OwnerName_leg
Salesforce object Account does not contain column Client__c_leg
Salesforce object Account does not contain column Installed_PA__c_sup
Salesforce object Account does not contain column Account_Executive_Fin__c_sup
*/

Select * from Account_Tritech_SFDC_UpdateInstalled_PAAccount_Executive_Fin;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateInstalled_PAAccount_Executive_Fin'

--------------------------------------------------------------------------------------------

--Planning_to_replace_911_provider__c,Planning_to_replace_CAD_provider__c,Planning_to_replace_Jail_provider__c,Planning_to_replace_RMS_provider__c Hotfix

Select 
 Id=supacc.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Id=supacc.LegacySFDCAccountId__c
,Planning_to_replace_911_provider__c_sup=supacc.Planning_to_replace_911_provider__c
,Planning_to_replace_911_provider__c=acc.Planning_to_replace_911_provider__c
,Planning_to_replace_CAD_provider__c_sup=supacc.Planning_to_replace_CAD_provider__c
,Planning_to_replace_CAD_provider__c=acc.Planning_to_replace_CAD_provider__c
,Planning_to_replace_Jail_provider__c_sup=supacc.Planning_to_replace_Jail_provider__c
,Planning_to_replace_Jail_provider__c=acc.Planning_to_replace_Jail_provider__c
,Planning_to_replace_RMS_provider__c_sup=supacc.Planning_to_replace_RMS_provider__c
,Planning_to_replace_RMS_provider__c=acc.Planning_to_replace_RMS_provider__c

into Account_Tritech_SFDC_UpdatePlanningFields4Feb19

from Superion_FULLSB.dbo.Account supacc
inner join Tritech_PROD.dbo.Account acc
on acc.Id=supacc.LegacySFDCAccountId__c

where acc.Planning_to_replace_911_provider__c IS NOT NULL or acc.Planning_to_replace_CAD_provider__c IS NOT NULL
or acc.Planning_to_replace_Jail_provider__c IS NOT NULL or acc.Planning_to_replace_RMS_provider__c IS NOT NULL ;

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdatePlanningFields4Feb19' 

/*
Salesforce object Account does not contain column Legacy_Id
Salesforce object Account does not contain column Planning_to_replace_911_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_RMS_provider__c_sup
*/

Select * from Account_Tritech_SFDC_UpdatePlanningFields4Feb19;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdatePlanningFields4Feb19'

-------------------------------------------------------------------------------------

Select 

 Id=acc.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))

,Fire_Paid_Per_Call__c_sup=tar.Fire_Paid_Per_Call__c
,Fire_Paid_Per_Call__c=leg.Fire_Fighter_Paid_Per_Call__c

,Secure_Folder_ID__c_sup=tar.Secure_Folder_ID__c
,Secure_Folder_ID__c_leg=leg.Secure_Folder_ID__c
,Secure_Folder_ID__c=ISNULL(tar.Secure_Folder_ID__c,leg.Secure_Folder_ID__c)

,Sensitive_Account_WMP__C_leg=leg.Sensitive_Account_WMP__c
,Sensitive_Account_WMP__C_sup=tar.SupportNotes__c
,SupportNotes__c=CONCAT(tar.SupportNotes__c,leg.Sensitive_Account_WMP__c)

,Installed_PA__c_sup=tar.Installed_PA__c
,Client__c_sup=tar.Client__c
,OwnerID_leg=leg.OwnerId
,Installed_PA__c=IIF(tar.Installed_PA__c IS NULL AND tar.Client__c='true',ownrname.Name,tar.Installed_PA__c)

,Account_Executive_Fin__c_sup=tar.Account_Executive_Fin__c
,Client__c_sup=tar.Client__c
,OwnerID_leg=leg.OwnerId
,Account_Executive_Fin__c=IIF(tar.Account_Executive_Fin__c IS NULL AND tar.Client__c='true',ownrname.Name,tar.Account_Executive_Fin__c)

,AAM_PSJ__c_sup=tar.AAM_PSJ__c
,Customer_Success_Liaison__c_leg=leg.Customer_Success_Liaison__c
,Customer_Success_Liaison__c_tar=cslname.Name
,AAM_PSJ__c=ISNULL(tar.AAM_PSJ__c,cslname.Name)

,IQ_Account_Owner__c_leg=leg.IQ_Account_Owner__c
,AE_IQ__c=iqname.[Name]

,X911_Account_Owner__c_leg=leg.X911_Account_Owner__c
,AE_911__c=x911name.[Name]

into Account_Tritech_SFDC_UpdateMergeAccountMissingFields8Feb19

from Staging_SB.dbo.Account_Tritech_SFDC_UpdateMergingAccounts acc
inner join Superion_FULLSB.dbo.Account tar
on tar.Id=acc.Id
left join Tritech_PROD.dbo.Account leg
on leg.Id=tar.LegacySFDCAccountId__c
left join Tritech_PROD.dbo.[User] cslname 
on cslname.Id=leg.Customer_Success_Liaison__c
left join Tritech_PROD.dbo.[User] ownrname
on ownrname.Id=leg.OwnerId
left join Tritech_PROD.dbo.[User] iqname 
on iqname.Id=leg.IQ_Account_Owner__c
left join Tritech_PROD.dbo.[User] x911name 
on x911name.Id=leg.X911_Account_Owner__c;

--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateMergeAccountMissingFields8Feb19' 

/*

*/

Select * from Account_Tritech_SFDC_UpdateMergeAccountMissingFields8Feb19;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateMergeAccountMissingFields8Feb19'

----------------------------------------------------------------------------------------------

--Name Hotfix

Select

 ID = sup.Id
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Name_stg=ld.Name
,Name_sup=sup.Name
,Name_tt=ld.Name_orig
,Name=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ld.Name,'Department of Public Safety','DPS'),'Sheriff''s Department','Sheriff')

,'Sheriff Department','Sheriff'),'Police Department','Police'),'Sheriff''s Dept.','Sheriff'),'Sheriff Dept.','Sheriff'),'Police Dept.','Police')

,'Sheriff''s Office','Sheriff'),'Sheriff Office','Sheriff')

into Account_Tritech_SFDC_UpdateName11Feb19_Intermediate

from Staging_SB.dbo.Account_Tritech_SFDC_Preload ld
inner join Superion_FULLSB.dbo.Account sup
on sup.LegacySFDCAccountId__c=ld.LegacySFDCAccountId__c
where 1=1 ;--26353

Select * into Account_Tritech_SFDC_UpdateName11Feb19
from Account_Tritech_SFDC_UpdateName11Feb19_Intermediate
where Name_Sup<>Name;--14317

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Account_Tritech_SFDC_UpdateName11Feb19' 

/*
Salesforce object Account does not contain column Name_stg
Salesforce object Account does not contain column Name_sup
Salesforce object Account does not contain column Name_tt
*/

Select * from Account_Tritech_SFDC_UpdateName11Feb19;

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Account_Tritech_SFDC_UpdateName11Feb19'



