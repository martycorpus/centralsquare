--Attendee_s__c Hotfix

Select 
 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Attendee_s__c=a.Attendee_s__c
--,Sharpies_Needed__c =a.Sharpies_Needed__c
--,Demo_Dates__c=a.Demo_Dates__c
--,Demo_Start_Time_Zone__c=a.Demo_Start_Time_Zone__c
--,Will_AV_Company_Setup__c=a.Will_AV_Company_Setup__c
,Legacy_Record_ID__c_orig =a.Legacy_Record_ID__c

into Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateAttendee

from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_load a;

Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateAttendee;


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateAttendee' 

/*
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateAttendee'

---------------------------------------------------------------------------------------------------------

--Sharpies_Needed__c Hotfix

Select 
 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
--,Attendee_s__c=a.Attendee_s__c
,Sharpies_Needed__c =a.Sharpies_Needed__c
--,Demo_Dates__c=a.Demo_Dates__c
--,Demo_Start_Time_Zone__c=a.Demo_Start_Time_Zone__c
--,Will_AV_Company_Setup__c=a.Will_AV_Company_Setup__c
,Legacy_Record_ID__c_orig =a.Legacy_Record_ID__c

into Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateSharpiesNeeded

from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_load a;

Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateSharpiesNeeded;


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateSharpiesNeeded' 

/*
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateSharpiesNeeded'

-----------------------------------------------------------------------------------------------------

Use Staging_SB;

Select 
 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
--,Demo_Date_Options__c=a.Demo_Dates__c
,Demo_Time_Zone__c=a.Demo_Start_Time_Zone__c
,Will_AV_Company_setup_or_just_drop_off__c=a.Will_AV_Company_Setup__c
,Legacy_Record_ID__c_orig =a.Legacy_Record_ID__c

into Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateNewFields

from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_load a;

Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateNewFields;


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateNewFields' 

/*
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateNewFields'

-- 1 error entity is deleted.
---------------------------------------------------------------------------------------------

Use Staging_SB;

Select 
 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Demo_Date_Options__c_sup=tar.Demo_Date_Options__c
,Demo_Date_Options__c=a.Demo_Dates__c
,Legacy_Record_ID__c_orig =a.Legacy_Record_ID__c

into Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateDemoDate

from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_load a

left join Superion_FULLSB.dbo.Sales_Request__c tar
on tar.Id=a.Id;

Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateDemoDate;


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateDemoDate' 

/*
Salesforce object Sales_Request__c does not contain column Demo_Date_Options__c_sup
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_UpdateDemoDate'