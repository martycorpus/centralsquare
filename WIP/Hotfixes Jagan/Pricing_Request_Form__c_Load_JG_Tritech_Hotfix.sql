--Status__C Hotfix

Select 
 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Request_Status__c_orig=leg.Request_Status__c
,Z_Status__c_orig=leg.Z_Status__c
,Status__c=IIF(leg.Z_Status__c IS NULL,leg.Request_Status__c,leg.Z_Status__c)
,Status__c_sup=tar.Status__c
,Legacy_Record_ID__c_orig =a.Legacy_Record_ID__c
,RecordType_sup=tar.RecordTypeId
,RecordTypeId='0120v000000JPB4AAO'

into Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateStatus

from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_load a

left join Tritech_PROD.dbo.Pricing_Request_Form__c leg
on leg.Id=a.Legacy_Record_ID__c

left join Superion_FULLSB.dbo.Sales_Request__c tar
on tar.Id=a.Id

where  Z_Status__c IS NOT NULL;--7391

Select * from Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateStatus;


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateStatus' 

/*
Salesforce object Sales_Request__c does not contain column Request_Status__c_orig
Salesforce object Sales_Request__c does not contain column Z_Status__c_orig
Salesforce object Sales_Request__c does not contain column Status__c_sup
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
Salesforce object Sales_Request__c does not contain column RecordType_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateStatus'

--10 errors.Entity is deleted.

----------------------------------------------------------------------------

--DECLARE @GeneralRequest NVARCHAR(18) = 
--(Select top 1 Id from Superion_FULLSB.dbo.recordtype where name ='General Request' and SobjectType='Sales_Request__c');

--declare @recordtypeid NVARCHAR(18) = (Select top 1 Id from Superion_FULLSB.dbo.recordtype 
--where name ='Pricing Request' and SobjectType='Sales_Request__c');

--Select 

-- Id = a.Id
--,Error=CAST(SPACE(255) as NVARCHAR(255))
--,Z_Assigned_To__c_orig=a.Z_Assigned_To__c_orig
--,What_is_Needed__c_orig=a.What_is_Needed__c
--,RecordTypeId_sup=tar.RecordTypeId
--,CASE 
-- WHEN Z_Assigned_To__c_orig IS NOT NULL and ISNULL(a.What_is_Needed__c,'X')<>'Pricing' THEN @GeneralRequest
-- ELSE @recordtypeid
-- END as RecordTypeId

--into Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeId

--from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_load a

--inner join Superion_FULLSB.dbo.Sales_Request__c tar
--on tar.Id=a.Id;--8156

--Select * from Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeId;


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeId' 

/*
Salesforce object Sales_Request__c does not contain column Z_Assigned_To__c_orig
Salesforce object Sales_Request__c does not contain column What_is_Needed__c_orig
Salesforce object Sales_Request__c does not contain column RecordTypeId_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeId'

--------------------------------------------------------------------------

--DECLARE @GeneralRequest NVARCHAR(18) = 
--(Select top 1 Id from Superion_FULLSB.dbo.recordtype where name ='General Request' and SobjectType='Sales_Request__c');

--declare @recordtypeid NVARCHAR(18) = (Select top 1 Id from Superion_FULLSB.dbo.recordtype 
--where name ='Pricing Request' and SobjectType='Sales_Request__c');

--Select 

-- Id = a.Id
--,Error=CAST(SPACE(255) as NVARCHAR(255))
--,Z_Assigned_To__c_orig=a.Z_Assigned_To__c_orig
--,What_is_Needed__c_orig=a.What_is_Needed__c
--,RecordTypeId_sup=tar.RecordTypeId
--,CASE 
-- WHEN Z_Assigned_To__c_orig IS NOT NULL THEN @GeneralRequest
-- WHEN Z_Assigned_To__c_orig IS NULL AND ISNULL(a.What_is_Needed__c,'X')<>'Pricing' THEN @GeneralRequest
-- ELSE @recordtypeid
-- END as RecordTypeId

--into Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField

--from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_load a

--inner join Superion_FULLSB.dbo.Sales_Request__c tar
--on tar.Id=a.Id;--8156

--Select * from Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField;


----Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField' 

--/*
--Salesforce object Sales_Request__c does not contain column Z_Assigned_To__c_orig
--Salesforce object Sales_Request__c does not contain column What_is_Needed__c_orig
--Salesforce object Sales_Request__c does not contain column RecordTypeId_sup
--*/

----Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField'

------------------------------------------------------------------------------------------------

DECLARE @GeneralRequest NVARCHAR(18) = 
(Select top 1 Id from Superion_FULLSB.dbo.recordtype where name ='General Request' and SobjectType='Sales_Request__c');

declare @recordtypeid NVARCHAR(18) = (Select top 1 Id from Superion_FULLSB.dbo.recordtype 
where name ='Pricing Request' and SobjectType='Sales_Request__c');

Declare @TriTechPricingRequestForm NVARCHAR(18)=
(Select top 1 Id from Tritech_PROD.dbo.RecordType where Name='TriTech Pricing Request Form' and SobjectType='Pricing_Request_Form__c');

Declare @ZuercherProposalRequestForm NVARCHAR(18)=
(Select top 1 Id from Tritech_PROD.dbo.RecordType where Name='Zuercher Proposal Request Form' and SobjectType='Pricing_Request_Form__c');

Select 

 Id = a.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Record_ID=a.Legacy_Record_ID__c
,RecordTypeId_orig=a.RecordTypeId_orig
,RecordTypeName=rt.Name
,Z_Assigned_To__c_orig=a.Z_Assigned_To__c_orig
,What_is_Needed__c_orig=a.What_is_Needed__c
,RecordTypeId_sup=tar.RecordTypeId
,CASE 
 WHEN RecordTypeId_orig=@TriTechPricingRequestForm THEN @recordtypeid
 WHEN RecordTypeId_orig=@ZuercherProposalRequestForm AND Z_Assigned_To__c_orig IS NOT NULL THEN @GeneralRequest
 WHEN RecordTypeId_orig=@ZuercherProposalRequestForm AND Z_Assigned_To__c_orig IS NULL AND ISNULL(a.What_is_Needed__c,'X')<>'Pricing' THEN @GeneralRequest
 ELSE @recordtypeid
 END as RecordTypeId

into Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField5Feb19

from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_PricingRequestForm_load a

inner join Superion_FULLSB.dbo.Sales_Request__c tar
on tar.Id=a.Id
left join Tritech_PROD.dbo.RecordType rt
on rt.Id=a.RecordTypeId_orig;--8156

Select * from Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField5Feb19;


--Exec SF_ColCompare 'Update','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField5Feb19' 

/*
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID
Salesforce object Sales_Request__c does not contain column RecordTypeId_orig
Salesforce object Sales_Request__c does not contain column RecordTypeName
Salesforce object Sales_Request__c does not contain column Z_Assigned_To__c_orig
Salesforce object Sales_Request__c does not contain column What_is_Needed__c_orig
Salesforce object Sales_Request__c does not contain column RecordTypeId_sup
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDCPricingRequestForm_UpdateRecordTypeIdField5Feb19'