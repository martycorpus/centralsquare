
/*
-- Author     : Sree
-- Date       : 11-20-2018
-- Description: Migrate the User data from Tritech Prod to Superion Salesforce.

Requirement Number:REQ-0841
Scope: 

Selection Criteria: 
a. Migrate 'active' Salesforce licensed Users
b. Do not migrate any other license type Users

-- inactive users, who are not migrated and are the owner or creator of a record, will have the owning user be set as a specific user ie (Central Square API)/

EN 10/24: Scope needs to be revisited by track leads
*/
---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
/*
USE Tritech_PROD
Exec SF_Refresh 'SL_TRITECH_PROD', 'User','Yes'
Exec SF_Replicate 'SL_TRITECH_PROD', 'Profile' --,'Yes'

USE Superion_FULLSB
Exec SF_Refresh 'SL_Superion_FULLSB', 'User' ,'Yes'
Exec SF_Replicate 'SL_Superion_FULLSB', 'Profile'-- ,'Yes'

select Legacy_Tritech_Id__c,count(*) from Superion_FULLSB.dbo.[user]
group by Legacy_Tritech_Id__c
having count(*)>1

*/
---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;
/*
select name,username,email,isactive,* from SUPERION_MAPLESB.dbo.[user]
where usertype='Standard' --and email like '%@Tritech.com%'
and name like 'Christine Gossman'


select a.id,b.id,c.id, a.username,b.username,c.username,a.name,b.name,c.name,a.email,b.email,c.email,a.isactive,b.isactive,c.isactive
,b.usertype,c.usertype
 from tritech_prod.dbo.[user] a
left outer join SUPERION_MAPLESB.dbo.[user] b
on a.email=replace(b.email,'.invalid','') and b.usertype='standard'
left outer join SUPERION_MAPLESB.dbo.[user] c
on a.name=c.name and c.usertype='standard'
where a.isactive='true' and a.usertype='standard'
and (b.id is not null or c.id is not null ) --and (b.usertype<>'standard' or c.usertype<>'standard')



select name,count(*) from SUPERION_MAPLESB.dbo.[user]
where usertype='Standard'
group by name
having count(*)>1;
*/
--drop table User_Tritech_SFDC_preload_test

declare @defaultprofile nvarchar(18)=(select top 1 id from Superion_FULLSB.dbo.[Profile] Spr_Profile
                                       where  name ='Superion standard user');
declare @CustomerSupportMgrRole nvarchar(18)=(select  id from Superion_FULLSB.dbo.[UserRole] Spr_role
                                       where  name ='Customer Support Mgr');
declare @CustomerSupportAgentRole nvarchar(18)=(select  id from Superion_FULLSB.dbo.[UserRole] Spr_role
                                       where  name ='Customer Support Agent');

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Tritech_Id__c,
		'Tritech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		concat(substring(a.username,1,charindex('@',a.username)-1),'centralsquare.com.superion.boss') as Username,
		a.[Username] as Username_orig,
		a.name ,
		a.[LastName] as LastName,
		isNull(a.[FirstName], '') as FirstName,
		isNull(a.[CompanyName], '') as CompanyName,
		a.CreatedDate CreatedDate_orig,
		isNull(a.[Division], '') as Division,
		isNull(a.[Department], '') as Department,
		isNull(a.[Title], '') as Title,
		isNull(a.[Street], '') as Street,
		isNull(a.[City], '') as City,
		isNull(a.[State], '') as State,
		isNull(a.[PostalCode], '') as PostalCode,
		isNull(a.[Country], '') as Country,
		--Concat(a.[Email], '.CSFullSB') as Email,
		replace(replace(a.email,'@Zuerchertech.com','@centralsquare.com'),'@Tritech.com','@centralsquare.com') as Email,
		a.[Email] as Email_orig,
		isNull(a.[Phone], '') as Phone,
		isNull(a.[Fax], '') as Fax,
		isNull(a.[MobilePhone], '') as MobilePhone,
		a.[Alias] as Alias,
		Concat(a.[CommunityNickname], '1') as CommunityNickname,
		a.[CommunityNickname] as CommunityNickname_orig,
		a.IsActive as IsActive,
		a.IsActive  isactive_orig,
		a.[TimeZoneSidKey] as TimeZoneSidKey,
		a.[LocaleSidKey] as LocaleSidKey,
		a.[ReceivesInfoEmails] as ReceivesInfoEmails,
		a.[ReceivesAdminInfoEmails] as ReceivesAdminInfoEmails,
		a.[EmailEncodingKey] as EmailEncodingKey,
		--@defaultprofile as ProfileID,
		--Src_Prof.[Name] as SourcProfile_Original,
      Trg_Prof.[Name] as TargetProfile_Original,
         Trg_Prof.ID as ProfileID,
isNull(Map_Prof.[Service Cloud User], 'false') as UserPermissionsSupportUser,
isNull(Map_Prof.[Service Cloud User], 'false') as UserPermissionsKnowledgeUser,

		a.ProfileId ProfileID_orig,
		TT_Profile.name tritech_profile_name,
		a.UserRoleId as UserRoleId_orig,
		case when Trg_Prof.name='Support Manager' then @CustomerSupportMgrRole
		     when Trg_Prof.name='Support Rep' then @CustomerSupportAgentRole
			 else null
			 end as UserRoleId,
		a.UserType ,
		a.[LanguageLocaleKey] as LanguageLocaleKey,
		isNull(a.[EmployeeNumber], '') as EmployeeNumber,
		isNull(a.[UserPermissionsMarketingUser], '') as UserPermissionsMarketingUser,
		isNull(a.[UserPermissionsOfflineUser], '') as UserPermissionsOfflineUser,
		isNull(a.[UserPermissionsCallCenterAutoLogin], '') as UserPermissionsCallCenterAutoLogin,
		isNull(a.[UserPermissionsMobileUser], '') as UserPermissionsMobileUser,
		isNull(a.[UserPermissionsSFContentUser], '') as UserPermissionsSFContentUser,
		--isNull(a.[UserPermissionsKnowledgeUser], '') as UserPermissionsKnowledgeUser,
		isNUll(a.[UserPermissionsInteractionUser], '') as UserPermissionsInteractionUser,
		--isNull(a.[UserPermissionsSupportUser], '') as UserPermissionsSupportUser,
		a.[ForecastEnabled] as ForecastEnabled,
		a.[UserPreferencesActivityRemindersPopup] as UserPreferencesActivityRemindersPopup,
		a.[UserPreferencesEventRemindersCheckboxDefault] as UserPreferencesEventRemindersCheckboxDefault,
		a.[UserPreferencesTaskRemindersCheckboxDefault] as UserPreferencesTaskRemindersCheckboxDefault,
		a.[UserPreferencesReminderSoundOff] as UserPreferencesReminderSoundOff,
		a.[UserPreferencesApexPagesDeveloperMode] as UserPreferencesApexPagesDeveloperMode,
		a.[UserPreferencesHideCSNGetChatterMobileTask] as UserPreferencesHideCSNGetChatterMobileTask,
		a.[UserPreferencesHideCSNDesktopTask] as UserPreferencesHideCSNDesktopTask,
		a.[UserPreferencesSortFeedByComment] as UserPreferencesSortFeedByComment,
		a.[UserPreferencesLightningExperiencePreferred] as UserPreferencesLightningExperiencePreferred,
		isNull(a.[CallCenterId], '') as CallCenterId,
		isNull(a.[Extension], '') as Extension,
		isNull(a.[PortalRole], '') as PortalRole,
		isNull(a.[FederationIdentifier], '') as FederationIdentifier,
		isNull(a.[AboutMe], '') as AboutMe,
		a.[DigestFrequency] as DigestFrequency,
		isNull(a.[Bomgar_Username__c], '') as Bomgar_Username__c,
		a.Managerid as Managerid_orig,
		sp_user.id sp_userid,
		sp_user.name sp_name,
		sp_user.username sp_username,
		sp_user.email sp_email,
		sp_user.Legacy_Tritech_Id__c  Legacy_Tritech_Id__c_sp,
		sp_user.Legacy_Source_System__c Legacy_Source_System__c_sp,
		sp_user1.id sp_existing_id
		INTO User_Tritech_SFDC_preload
		FROM Tritech_PROD.dbo.[User] a
		-- TriTech Profiles
		LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] TT_Profile 
		ON  a.ProfileId = TT_Profile.ID
		left outer join [Superion_FULLSB].[dbo].[user] sp_user
	    on sp_user.Email=a.Email or sp_user.email=a.email+'.CSFullSB'
	left outer join [Superion_FULLSB].[dbo].[user] sp_user1
	    on sp_user1.Legacy_Tritech_Id__c=a.id 
	-- Map Profile
LEFT OUTER JOIN Staging_SB.dbo.map_Profile Map_Prof 
ON TT_Profile.[Name] = Map_Prof.[Tritech Profile Name]
-- Target Profile
LEFT OUTER JOIN Superion_FULLSB.dbo.[Profile] Trg_Prof 
ON Map_Prof.[Superion Profile Name] = Trg_Prof.[Name]

			WHERE    1=1 --a.isActive = 'true'
		-- Following code is to prevent migration of User records that were precreated
			/*AND a.[Email] NOT IN
				(	Select SPR.email
					FROM Superion_FULLSB.dbo.[User] SPR
					WHERE Migrated_Record__c = 'false'
				)*/
		-- Eliminates Community Users licensed Users from being migrated as part of this script, to be migrated separately later
		/*AND TT_Profile.[Name] Not IN
					('TriTech Portal Read Only with Tickets',
					 'TriTech Portal Read-Only User',
					 'Overage High Volume Customer Portal User',
					 'Overage Customer Portal Manager Custom',
					 'TriTech Portal Standard User',
					 'TriTech Portal Manager')
					 */--(15796 row(s) affected)

	select * from 	User_Tritech_SFDC_preload;--15796
	
	select count(*) from [Tritech_PROD].[dbo].[user];	--15668
	
	 select Legacy_Tritech_Id__c,count(*) from User_Tritech_SFDC_preload
	 group by Legacy_Tritech_Id__c
	 having count(*)>1;--76

	 select * from User_Tritech_SFDC_preload
	 where UserType='Standard'
	 where legacy_tritech_id__C='0058000000EIqwYAAT'


	 select distinct usertype
	 --select *
	 	 from User_Tritech_SFDC_preload
	 where usertype='Standard'
	 and isactive_orig='true';

	 select * from Tritech_PROD.dbo.[user]
	 where email='scott.key@tritech.com';

	 select usertype,isactive_orig,count(*) from User_Tritech_SFDC_preload
	 group by usertype,isactive_orig
	 having count(*)>1;
	 

	 --Drop table User_Tritech_SFDC_load
	 select *
	 into User_Tritech_SFDC_load
	 from User_Tritech_SFDC_preload a
	 where usertype='Standard'
	 and isactive_orig='true'
	 --and sp_userid is null
	 and  sp_existing_id is null;--5

	 select Legacy_Tritech_Id__c,count(*) from User_Tritech_SFDC_load
	 group by Legacy_Tritech_Id__c
	 having count(*)>1;--0


	 --drop table User_Tritech_SFDC_load_final
	  select *
	 into User_Tritech_SFDC_load_final
	 from User_Tritech_SFDC_load a
	 where  sp_userid is null;--5
	 


	 
	 select * from User_Tritech_SFDC_load_final;




---------------------------------------------------------------------------------
-- Insert records
---------------------------------------------------------------------------------

--exec SF_ColCompare 'Insert','SL_Superion_FULLSB', 'User_Tritech_SFDC_load_final' 

--check column names

/*
Salesforce object User does not contain column Username_orig
Salesforce object User does not contain column CreatedDate_orig
Salesforce object User does not contain column Email_orig
Salesforce object User does not contain column CommunityNickname_orig
Salesforce object User does not contain column isactive_orig
Salesforce object User does not contain column TargetProfile_Original
Salesforce object User does not contain column ProfileID_orig
Salesforce object User does not contain column tritech_profile_name
Salesforce object User does not contain column Managerid_orig
Salesforce object User does not contain column sp_userid
Salesforce object User does not contain column sp_name
Salesforce object User does not contain column sp_username
Salesforce object User does not contain column sp_email
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp
Salesforce object User does not contain column Legacy_Source_System__c_sp
Salesforce object User does not contain column sp_existing_id
Column name is not insertable into the salesforce object User
Column UserType is not insertable into the salesforce object User
*/


--Exec SF_BulkOps 'Insert:batchsize(50)','SL_Superion_FULLSB','User_Tritech_SFDC_load_final'



select * from User_Tritech_SFDC_load_final
 where error not like '%success%'
 ;

 select error,count(*) from User_Tritech_SFDC_load_final
 group by error;

---------------------------------------------------------------------------------
-- Populate External ID for precreated Users
-- Only users were a matching email exists on a SINGLE record will be populated
-- User records whose email exists on multiple records will NOT be populated and must be manually resolved
---------------------------------------------------------------------------------

USE Staging_SB
--drop table User_Tritech_SFDC_Update_existing_User
;with CteArticlemgrPerSet as
(select  b.AssigneeId from Superion_FULLSB.dbo.PermissionSet a
  ,Superion_FULLSB.dbo.PermissionSetAssignment b
  ,Superion_FULLSB.dbo.[User] c
  where a.id=b.PermissionSetId
  and b.AssigneeId=c.id
  and a.Label='Article Manager'
   )
, CTeDupeemails as (
select Legacy_Tritech_Id__c ,count(*) rec_count 
from User_Tritech_SFDC_load
group by Legacy_Tritech_Id__c
having count(*)>1
)
Select 
	SPR.ID as ID, 
	stg.Error,
	stg.Legacy_Tritech_Id__c as Legacy_Tritech_Id__c,
	spr.UserPermissionsKnowledgeUser as UserPermissionsKnowledgeUser_sup,
	stg.UserPermissionsKnowledgeUser as UserPermissionsKnowledgeUser_stg,
	iif(ap.AssigneeId is not null,'true',stg.UserPermissionsKnowledgeUser) as UserPermissionsKnowledgeUser,
	stg.Legacy_Source_System__c,
	,Duplicate_number = ROW_NUMBER() OVER (PARTITION BY stg.Legacy_Tritech_Id__c
order by iif(spr.name=stg.name,1,2) ),
spr.name spr_name,stg.name tr_name,stg.Legacy_Tritech_Id__c_sp,stg.Email_orig,spr.email spr_email,sp_userid,
dupe.Legacy_Tritech_Id__c dupe_Legacy_Tritech_Id__c
		INTO User_Tritech_SFDC_Update_existing_User
	FROM Superion_FULLSB.dbo.[User] SPR
	INNER JOIN User_Tritech_SFDC_load stg
	on spr.id=stg.sp_userid
	left outer join CTeDupeemails dupe
	on spr.Legacy_Tritech_Id__c=dupe.Legacy_Tritech_Id__c	
	left outer join CteArticlemgrPerSet ap
	on spr.id=ap.AssigneeId
	where 1=1
	   --and dupe.Legacy_Tritech_Id__c is not null
	    and spr.Legacy_Tritech_Id__c is null
		AND SPR.Migrated_Record__c = 'false'  -- ensures that only pre-existing User records are included
		;--0

		--drop table User_Tritech_SFDC_Update_existing_User_final
		select * 
		INTO User_Tritech_SFDC_Update_existing_User_final
		from User_Tritech_SFDC_Update_existing_User
		where Duplicate_number=1;

		select id,count(*) from User_Tritech_SFDC_Update_existing_User_final
		group by id
		having count(*)>1;

		select * from User_Tritech_SFDC_Update_existing_User_final;

		---------------------------------------------------------------------------------
-- Update records
---------------------------------------------------------------------------------

--exec SF_ColCompare 'Update','SL_Superion_FULLSB', 'User_Tritech_SFDC_Update_existing_User_final' 

--check column names
/*
Salesforce object User does not contain column Duplicate_number
Salesforce object User does not contain column spr_name
Salesforce object User does not contain column tr_name
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp
Salesforce object User does not contain column Email_orig
Salesforce object User does not contain column spr_email
Salesforce object User does not contain column sp_userid
Salesforce object User does not contain column dupe_Legacy_Tritech_Id__c
*/

--Exec SF_BulkOps 'Update:batchsize(1)','SL_Superion_FULLSB','User_Tritech_SFDC_Update_existing_User_final'



select * from User_Tritech_SFDC_Update_existing_User_final
 where error not like '%success%'
 ;

 select error,count(*) from User_Tritech_SFDC_Update_existing_User_final
 group by error;

 	---------------------------------------------------------------------------------
-- Update Manager records
---------------------------------------------------------------------------------
/*
USE Superion_FULLSB
Exec SF_Replicate 'SL_Superion_FULLSB', 'User' --,'Yes'
*/

Use Staging_SB

--drop table User_Tritech_SFDC_Manager_update
select
tr_user.id
,error=cast(space(255) as nvarchar(255))
,Managerid=iif(sp_mgr.id is  null,'Manager Not Found',sp_mgr.id)
,managerid_orig=tr_user.ManagerId_orig
,tr_name=tr_user.name
,tr_email=tr_user.email
,tr_username=tr_user.Username
,sp_mgr_name=sp_mgr.name
,sp_mgr_email=sp_mgr.email
,sp_mgr_username=sp_mgr.Username
,Legacy_Source_System__c_sp_mgr=sp_mgr.Legacy_Source_System__c
,Legacy_Tritech_Id__c_sp_mgr=sp_mgr.Legacy_Tritech_Id__c
,tr_user_id=tr_user.id
into User_Tritech_SFDC_Manager_update
 from User_Tritech_SFDC_load_final tr_user
left outer join [Superion_FULLSB].dbo.[User] sp_mgr
on tr_user.ManagerId_orig=sp_mgr.Legacy_Tritech_Id__c
where tr_user.error='Operation Successful.'
and tr_user.ManagerId_orig is not null
;--2

select * from User_Tritech_SFDC_Manager_update;


--Exec SF_ColCompare 'update', 'SL_Superion_FULLSB', 'User_Tritech_SFDC_Manager_update'

/*
Salesforce object User does not contain column managerid_orig
Salesforce object User does not contain column tr_name
Salesforce object User does not contain column tr_email
Salesforce object User does not contain column tr_username
Salesforce object User does not contain column sp_mgr_name
Salesforce object User does not contain column sp_mgr_email
Salesforce object User does not contain column sp_mgr_username
Salesforce object User does not contain column Legacy_Source_System__c_sp_mgr
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp_mgr
Salesforce object User does not contain column tr_user_id
*/

--Exec SF_BulkOps 'update:batchsize(1)', 'SL_Superion_FULLSB', 'User_Tritech_SFDC_Manager_update'


select error,count(*) from User_Tritech_SFDC_Manager_update
group by error



---UAT1 Hotfix....
/*
--drop table User_Tritech_Manager_update_SL
select
sp_user.id
,error=cast(space(255) as nvarchar(255))
,mangerid_sp=sp_user.ManagerId
,Managerid=iif(sp_mgr.id is  null,'Manager Not Found',sp_mgr.id)
,sp_name=sp_user.name
,sp_email=sp_user.email
,sp_username=sp_user.Username
,sp_mgr_name=sp_mgr.name
,sp_mgr_email=sp_user.email
,sp_mgr_username=sp_user.Username
,Legacy_Source_System__c_sp=sp_user.Legacy_Source_System__c
,Legacy_Tritech_Id__c_sp=sp_user.Legacy_Tritech_Id__c
,tr_user_id=tr_usr.id
,tr_managerid=tr_usr.ManagerId
into User_Tritech_Manager_update_SL
 from [Superion_FULLSB].dbo.[User] sp_user
left outer join [Tritech_PROD].dbo.[User] tr_usr
on sp_user.Legacy_Tritech_Id__c=tr_usr.id
left outer join [Superion_FULLSB].dbo.[User] sp_mgr
on tr_usr.ManagerId=sp_mgr.Legacy_Tritech_Id__c
where sp_user.Legacy_Tritech_Id__c is not null
and tr_usr.id is not null and tr_usr.ManagerId is not null
and sp_user.ManagerId is  null;--500

select * from User_Tritech_Manager_update_SL;


--Exec SF_ColCompare 'update', 'SL_Superion_FULLSB', 'User_Tritech_Manager_update_SL'

/*
Salesforce object User does not contain column mangerid_sp
Salesforce object User does not contain column sp_name
Salesforce object User does not contain column sp_email
Salesforce object User does not contain column sp_username
Salesforce object User does not contain column sp_mgr_name
Salesforce object User does not contain column sp_mgr_email
Salesforce object User does not contain column sp_mgr_username
Salesforce object User does not contain column Legacy_Source_System__c_sp
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp
Salesforce object User does not contain column tr_user_id
Salesforce object User does not contain column tr_managerid

*/

--Exec SF_BulkOps 'update:batchsize(20)', 'SL_Superion_FULLSB', 'User_Tritech_Manager_update_SL'

select error,count(*) from Staging_SB.dbo.User_Tritech_Manager_update_SL
group by error
*/
*/

--------------------------------------------------------------------------------------------
--------------------- Assigning Article Manger Permission set for given users----------------
--------------------------------------------------------------------------------------------

--select * from Superion_FULLSB.dbo.PermissionSetAssignment b;

--drop table PermissionSetAssignment_Superion_Article_mgr_assignment_preload

declare @ArticleMgrPerSet nvarchar(18)=(select top 1 id from Superion_FULLSB.dbo.PermissionSet
                                       where Label='Article Manager');
select 
ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,assigneeId=a.id
,PermissionSetId=@ArticleMgrPerSet
, a.name user_name,a.Legacy_Tritech_Id__c,a.UserPermissionsSupportUser,a.UserPermissionsKnowledgeUser
,Duplicate_number = ROW_NUMBER() OVER (PARTITION BY a.name
order by iif(a.Legacy_Tritech_Id__c is not null,1,2), a.UserPermissionsKnowledgeUser desc,a.UserPermissionsSupportUser desc)
,b.id persetassingmentid_sup
,a.Migrated_Record__c Migrated_Record__c_sp
into PermissionSetAssignment_Superion_Article_mgr_assignment_preload
from Superion_FULLSB.dbo.[User] a
left outer join Superion_FULLSB.dbo.PermissionSetAssignment b
on a.id=b.AssigneeId and b.PermissionSetId=@ArticleMgrPerSet
  where 1=1 --  and b.id is null
    and a.name in 
  (
  
'Paul Trewin'
,'Kelly Adams'
,'Samantha O''Brien'
,'Ty Kaiser'
,'Renae Livingston'
,'Jesse Blommer'
,'Shane Birch'
,'Pete Avila'
,'Kristi Miller'
,'Ellen Manning'
  );

  select * from PermissionSetAssignment_Superion_Article_mgr_assignment_preload;

  --drop table PermissionSetAssignment_Superion_Article_mgr_assignment_load

  select * 
  into PermissionSetAssignment_Superion_Article_mgr_assignment_load
  from 
  PermissionSetAssignment_Superion_Article_mgr_assignment_preload
  where 1=1
  and Duplicate_number=1
  ;

  select assigneeid,count(*) from PermissionSetAssignment_Superion_Article_mgr_assignment_load
  group by assigneeid
  having count(*)>1;

  select * from PermissionSetAssignment_Superion_Article_mgr_assignment_load;

--Exec SF_ColCompare 'Insert', 'SL_Superion_FULLSB', 'PermissionSetAssignment_Superion_Article_mgr_assignment_load'


--Exec SF_BulkOps 'Insert:batchsize(50)', 'SL_Superion_FULLSB', 'PermissionSetAssignment_Superion_Article_mgr_assignment_load'


select * from PermissionSetAssignment_Superion_Article_mgr_assignment_load
where error<>'Operation Successful.'
;

  select error,count(*) from PermissionSetAssignment_Superion_Article_mgr_assignment_load
  group by error;


