-------------------------------------------------------------------------------
--- Content (Attachment) Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 27, 2018
--- Last Updated: November 27, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'Attachment', 'yes' ;

Use Superion_FUllSB
Exec SF_Refresh 'PB_Superion_FullSB', 'User', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Account', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Contact', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Opportunity', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Environment__c', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'EngineeringIssue__c', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Sales_Request__c', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Case', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Task', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Event', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Procurement_Activity__c', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'EmailMessage', 'yes';

---------------------------------------------------------------------------------
-- Drop Map_NoteParentID_PB
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Map_ContentParentID') 
DROP TABLE Staging_SB.dbo.Map_ContentParentID;


---------------------------------------------------------------------------------
-- Load Map_ContentParentID_PB
---------------------------------------------------------------------------------
Use Staging_SB;

Select Distinct * 
INTO MAP_ContentParentID
From
(	
		-- Account
		select LegacySFDCAccountID__c as LegacyID, ID from Superion_FULLSB.dbo.Account where LegacySFDCAccountID__c is not null
		UNION
		-- Contact
		Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Contact where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Opportunity
		Select Legacy_Opportunity_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Opportunity where Legacy_Opportunity_ID__c is not null and Migrated_Record__c = 'true'
		--UNION
		-- Environment
		--select LegacyIDValue__c as LegacyID, ID from Environment__c  -- validate and uncomment once object is coded.
		UNION
		-- Engineering Issue
		Select Legacy_CRMId__c as LegacyID, ID from Superion_FULLSB.dbo.EngineeringIssue__c where Legacy_CRMId__c is not null 
		UNION
		-- Win Strategy
		Select a.ID as LegacyID, Oppty.ID from TriTech_Prod.dbo.Win_Strategy__c a
		INNER JOIN Superion_FULLSB.dbo.Opportunity Oppty on a.Opportunity__c = Oppty.Legacy_Opportunity_ID__c
		UNION
		-- CASE
		Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.[Case] where legacy_id__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Task
		Select Legacy_Id__c as LegacyID, ID from Superion_FULLSB.dbo.Task where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Event
		Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Event where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		UNION
		--Procurement activity
		Select Legacy_ID__c as Legacyid, ID from Superion_FULLSB.dbo.Procurement_Activity__c where Legacy_id__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Email Message
		Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.EmailMessage where legacy_id__c is not null and Migrated_Record__c = 'true'
		UNION
		-- Sales Request
		Select Legacy_Record_Id__c as LegacyID, ID from Superion_FULLSB.dbo.Sales_Request__c where legacy_Record_id__c is not null
)t


CREATE INDEX ContentParentMapIdx
ON MAP_ContentParentID (LegacyID)


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ContentVersion_LoadAttachments_PB') 
DROP TABLE Staging_SB.dbo.ContentVersion_LoadAttachments_PB;


---------------------------------------------------------------------------------
-- Load ContentNote Staging Table
---------------------------------------------------------------------------------
Use Staging_SB;

Select Distinct
		CAST('' AS nchar(18)) as [ID], 
		CAST('' as nvarchar(255)) as Error, 
		a.ID as Legacy_Source_ID__c,
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		a.[Name] as Title, 
		CAST(isNull([Body], '') as varbinary(max)) as VersionData,
		CAST('c:\temp\' + a.[Name] as nvarchar(500)) as PathOnClient,
		'C' as Origin,
		a.[CreatedDate] as CreatedDate
		--a.OwnerID as OwnerID_Orig,
		--isNull([OwnerID].ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as OwnerID,
		--a.CreatedById as CreatedBy_Orig,
		--isNull([CreatedBy].ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as CreatedByID
		INTO Staging_SB.dbo.ContentVersion_LoadAttachments_PB
		From TriTech_Prod.dbo.Attachment a
		-- eliminates records that were not migrated to new system based on ParentID
		INNER JOIN Staging_SB.dbo.MAP_ContentParentID map ON a.ParentID = map.LegacyID -- and substring(LegacyID, 1, 3) in ('006','500')
		---- OwnerID
		--LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [OwnerID] ON a.OwnerId = OwnerID.Legacy_Tritech_Id__c
		---- CreatedBy
		--LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [CreatedBy] ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c 



--------------------------------------------------------------------------------
-- Upload ContentNote
--------------------------------------------------------------------------------
Exec sf_bulkops 'Insert:batchsize(5)','PB_Superion_FULLSB','ContentVersion_LoadAttachments_PB'

--Select * from ContentVersion_LoadAttachments_PB where error not like '%Success%' 

-----------
-- NOTE FOR PROCESSING 2ND AND SUBSEQUENT RUNS
-- PROCESS THE CREATION OF THE CONTENTVERSION TABLE AS INDICATED TWO STEPS BELOW
-- POST CREATION OF THE CONTENTVERSION TABLE, EXECUTE THE DELETE STATEMENT ONE STEP BELOW
-- THEN RE-EXECUTE THE INSERT STATEMENT ONE STEP ABOVE TO LOAD ONLY DELTA (FAILED) RECORDS
-----------


----- Only execute following statement when running multiple / subsequent runs of ContentVersion
Delete PB
	From ContentVersion_LoadAttachments_PB PB
	Inner Join superion_fullsb.dbo.ContentVersion PB1 on PB.Legacy_Source_ID__c = PB1.Legacy_Source_ID__c
	
	--select count(*) From ContentVersion_LoadAttachments_PB 

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Content Document Link
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Refresh ContentVersion table, in order to obtain ContentDocumentID
--------------------------------------------------------------------------------
Use Superion_FULLSB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ContentVersion') 
DROP TABLE ContentVersion;


Select * 
INTO Superion_FullSB.dbo.ContentVersion
FROM
       OPENQUERY
       (PB_Superion_FullSB, 'Select ID, ContentDocumentID, Legacy_Source_ID__c, ownerid from ContentVersion')


CREATE INDEX ContentVersionIdx
ON ContentVersion (Legacy_Source_ID__c)

--------------------------------------------------------------------------------
-- Drop Document Link Staging Table
--------------------------------------------------------------------------------

Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ContentDocumentLink_LoadAttachments_PB') 
DROP TABLE Staging_SB.dbo.ContentDocumentLink_LoadAttachments_PB;


--------------------------------------------------------------------------------
-- Create Content Document Link Staging Table
--------------------------------------------------------------------------------
USE Staging_SB

Select Distinct
	CAST('' as nvarchar(18)) as ID,
	CAST('' as nvarchar(255)) as error,
	a.ContentDocumentID as ContentDocumentID,
	map.ID as LinkedEntityID,
	CASE Substring(map.ID, 1, 3) WHEN '02s' THEN 'V'  --- set on EmailMessage object to prevent editing by user
								 ELSE 'I' END as ShareType,
	'AllUsers' as Visibility
	INTO ContentDocumentLink_LoadAttachments_PB
	From Superion_FULLSB.dbo.ContentVersion a
	INNER JOIN ContentVersion_LoadAttachments_PB VersionLoad ON a.ID = VersionLoad.ID and VersionLoad.Error like '%success%'  -- Only allows linking of records in the ContentVersion_LoadAttachments table that loaded successfully
	LEFT OUTER JOIN Tritech_PROD.dbo.Attachment attch ON a.Legacy_Source_ID__c = Attch.ID
	LEFT OUTER JOIN Staging_sb.dbo.MAP_ContentParentID map ON attch.ParentID = map.LegacyID



Delete PB
	From ContentDocumentLink_LoadAttachments_PB PB
	Inner Join ContentDocumentLink_LoadAttachments_PB_Success PB1 on PB.ContentDocumentID = PB1.ContentDocumentID
	AND PB.LinkedEntityID = PB1.LinkedEntityID
  AND PB1.Error like '%success%'
	



--------------------------------------------------------------------------------
-- Upload ContentNote
--------------------------------------------------------------------------------
Exec sf_bulkops 'Insert','PB_Superion_FULLSB','ContentDocumentLink_LoadAttachments_PB'

--Select * from ContentDocumentLink_LoadAttachments_PB where error not like '%Success%'



