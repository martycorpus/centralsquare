---  BGIntegration__Event_Data__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Event_Data__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Event_Data__c')


)t


Alter table BGIntegration__Event_Data__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Event_Data__c_HardDelete'

--------------------------------------------------------------------------------------------------------------------

---  BGIntegration__Customer__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Customer__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Customer__c')


)t


Alter table BGIntegration__Customer__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Customer__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------

---  BGIntegration__Customer_Exit_Survey_Response__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Customer_Exit_Survey_Response__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Customer_Exit_Survey_Response__c')


)t


Alter table BGIntegration__Customer_Exit_Survey_Response__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Customer_Exit_Survey_Response__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------


---  BGIntegration__Recording__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Recording__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Recording__c')


)t


Alter table BGIntegration__Recording__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Recording__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------

---  BGIntegration__Representative__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Representative__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Representative__c')


)t


Alter table BGIntegration__Representative__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Representative__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------


---  BGIntegration__System_Information_Data__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__System_Information_Data__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__System_Information_Data__c')


)t


Alter table BGIntegration__System_Information_Data__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__System_Information_Data__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------



---  BGIntegration__System_Information__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__System_Information__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__System_Information__c')


)t


Alter table BGIntegration__System_Information__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__System_Information__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------



---  BGIntegration__Team__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Team__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Team__c')


)t


Alter table BGIntegration__Team__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Team__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------



---  BGIntegration__Event__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Event__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Event__c')


)t


Alter table BGIntegration__Event__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Event__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------


---  BGIntegration__Custom_Attribute__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__Custom_Attribute__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__Custom_Attribute__c')


)t


Alter table BGIntegration__Custom_Attribute__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__Custom_Attribute__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------


---  BGIntegration__BomgarSessionReady__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__BomgarSessionReady__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__BomgarSessionReady__c')


)t


Alter table BGIntegration__BomgarSessionReady__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__BomgarSessionReady__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------


---  BGIntegration__BomgarSession__c
Select 
ID as ID,
CAST('' as nvarchar(255)) as error
INTO BGIntegration__BomgarSession__c_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID from BGIntegration__BomgarSession__c')


)t


Alter table BGIntegration__BomgarSession__c_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'BGIntegration__BomgarSession__c_HardDelete'

----------------------------------------------------------------------------------------------------------------------
