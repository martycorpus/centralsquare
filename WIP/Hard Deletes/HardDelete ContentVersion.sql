Select 
t.ContentDocumentID as ID,
CAST('' as nvarchar(255)) as error,
t.Legacy_Source_ID__c
INTO ContentDocument_HardDelete
FROM
(
	Select * 
	FROM
		   OPENQUERY
		   (PB_Superion_FullSB, 'Select ID, ContentDocumentID, Legacy_Source_ID__c, ownerid from ContentVersion')


)t


Alter table ContentDocument_HardDelete
Add [Sort] int identity (1,1)

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'ContentDocument_HardDelete'