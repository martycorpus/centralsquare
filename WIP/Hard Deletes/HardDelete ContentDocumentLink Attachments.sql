Select Distinct
ID as ID,
CAST('' as nvarchar(255)) as error
INTO ContentDocumentLink_Delete
FROM
(
Select ID from ContentDocumentLink_LoadAttachments_PB_Success where error like '%success%'
UNION
Select ID from ContentDocumentLink_LoadAttachments_PB where error like '%success%'
UNION
Select ID from ContentDocumentLink_LoadAttachments_PB_1 where error like '%success%'
) t

EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'ContentDocumentLink_Delete'

select * from ContentDocumentLink_Delete where error not like '%success%'