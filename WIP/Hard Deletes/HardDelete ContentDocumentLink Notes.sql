Select Distinct
ID as ID,
CAST('' as nvarchar(255)) as error
INTO ContentDocumentLink_DeleteNotes
FROM
(
select ID from Staging_sb.dbo.ContentDocumentLink_LoadNotes_PB where error like '%Success%'
UNION
select ID from Staging_sb.dbo.ContentDocumentLink_LoadNotes_PB1 where error like '%Success%'
)t


EXEC SF_BulkOps 'harddelete:bulkapi', 'PB_Superion_FULLSB', 'ContentDocumentLink_DeleteNotes'

select * from ContentDocumentLink_DeleteNotes where error not like '%success%'