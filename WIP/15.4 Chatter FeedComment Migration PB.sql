-------------------------------------------------------------------------------
--- Chatter FeedComment Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 28, 2018
--- Last Updated: November 28, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Replicate Data 
--------------------------------------------------------------------------------
Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'FeedComment', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'CollaborationGroup', 'yes';


Use Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes';




---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
Drop Table Staging_SB.dbo.FeedComment_Load_PB

USE Staging_SB

Select
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.CommentBody as CommentBody,
	a.CommentType as CommentType,
	a.CreatedByID as CreatedBy_Orig,
	isNull(CreatedBy.ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as CreatedById,
	a.CreatedDate as CreatedDate,
	FeedItem.ID as FeedItemID,
	a.IsRichText as IsRichText,
	a.isVerified as IsVerified,
	isNull(a.[Status], '') as [Status],
	a.ID as Legacy_ID__c
	INTO Staging_SB.dbo.FeedComment_Load_PB
	from TriTech_Prod.dbo.FeedComment a
	---CreatedBy
	LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c
	---FeedItem
	LEFT OUTER JOIN Staging_SB.dbo.FeedItem_load_PB FeedItem ON a.FeedItemID = FeedItem.Legacy_Id__c
	WHERE FeedItem.ID IS NOT NULL


---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.FeedComment_load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Staging_SB
Exec SF_BulkOps 'insert:bulkapi,batchsize(1000)', 'PB_Superion_FULLSB', 'FeedComment_load_PB'

-------------------------------
-------------------------------

--select * from FeedComment_Load_PB where error not like '%success%'