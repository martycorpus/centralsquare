
/*
-- Author     : Shivani Mogullapalli
-- Date       : 24/11/2018
-- Description: Migrate the KnowledgeArticle Datacategories data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-

Scope: Load the Datacategories based on the given DataCategories Mapping.

-- drop table DataCategories_KB_Tritech_SFDC_Preload
*/
select 
							DatacategoryName		= api.Article_Category_API
							,DataCategoryGroupName	= 'Products'
							,parentid				= a.id
							,DataCategoryLabel		= c.newArticleCategory
					into DataCategories_KB_Tritech_SFDC_Preload
from KnowledgeArticle_Tritech_SFDC_load a
,Tritech_KB_Article_Data_Categories c
,DataCategory_API_New_Category api
where a.ArticleNumber_orig=c.ArticleNumber 
and c.NewArticleCategory = api.Article_Category_Label
and a.id like 'ka%'


--select * from Tritech_KB_Article_Data_Categories
-----------------------------------------------------------------------------------------------------------------------
-- (6321 row(s) affected)

select count(*) from DataCategories_KB_Tritech_SFDC_Preload
-- 6321

select * from DataCategories_KB_Tritech_SFDC_Preload
-- drop table DataCategories_KB_Tritech_SFDC_load
select * 
into DataCategories_KB_Tritech_SFDC_load 
from DataCategories_KB_Tritech_SFDC_Preload
-- (6321 row(s) affected)


select count(*) from DataCategories_KB_Tritech_SFDC_load
-- (6321 row(s) affected)

select * from DataCategories_KB_Tritech_SFDC_load
Select distinct
   A.parentid,
        stuff((
        select ';' + B.DataCategoryLabel
        from DataCategories_KB_Tritech_SFDC_load B
        WHERE A.parentid=B.parentid
        for xml path('')),1,1,'')
   From DataCategories_KB_Tritech_SFDC_load A
   where parentid!=''


----------------------------------------------------------------------------------------------------------------------------
select * from DataCategories_KB_Tritech_SFDC_Preload


Select distinct
   A.parentid,
        stuff((
        select ';' + c.article_category_api
        from DataCategories_KB_Tritech_SFDC_load B
		left join DataCategory_API_New_Category c
		on b.datacategoryLabel=c.article_category_label
        WHERE A.parentid=B.parentid
        for xml path('')),1,1,'') as Product__c
   From DataCategories_KB_Tritech_SFDC_load A
   where parentid!=''

   select * from DataCategories_KB_Tritech_SFDC_load

   select * from Tritech_KB_Article_Data_Categories

   select kav.id,kav.KnowledgeArticleId ,
   product__c=stuff((
        select ';' + c.article_category_api
        from DataCategories_KB_Tritech_SFDC_load B
		left join DataCategory_API_New_Category c
		on b.datacategoryLabel=c.article_category_label
        WHERE A.parentid=B.parentid
        for xml path('')),1,1,'')
   from tritech_prod.dbo.KnowledgeArticleVersion kav
   inner join Tritech_KB_Article_Data_Categories datacat
   on kav.KnowledgeArticleId=datacat.knowledgearticleid