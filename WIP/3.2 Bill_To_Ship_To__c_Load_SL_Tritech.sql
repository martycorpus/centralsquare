
/*
-- Author     : Sree
-- Date       : 11-23-2018
-- Description: Migrate the User data from Tritech (excel file data) to Superion Salesforce.

Requirement Number:REQ-0347
Scope: 

Selection Criteria: 
- Addresses will be provided via a SQL Database table made available to the database team.
- Addresses will logically relate to an account
- The data extract will be provided from Great Plains (GPDB), and will contain duplicate addresses.
- No deduplication of addresses will be performed.
---
Prerequisities:
Turn off Trigger: 

alter table [TRITECH_CUSTOMER_B2S2]
add   [phone1]    nvarchar(100);

update [TRITECH_CUSTOMER_B2S2]
set [phone1]=convert(nvarchar(100),convert(bigint, convert ( float, [Phone 1]) ))




*/
---------------------------------------------------------------------------------
-- Drop staging tables 

---------------------------------------------------------------------------------

USE Staging_SB

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
/*
USE Tritech_PROD
Exec SF_Refresh 'MC_TRITECH_PROD', 'User','Yes'
Exec SF_Refresh 'MC_TRITECH_PROD', 'Account' ,'Yes'

USE Superion_FULLSB
Exec SF_Replicate 'SL_Superion_FULLSB', 'Bill_To_Ship_To__c' --,'Yes'
Exec SF_Refresh 'SL_Superion_FULLSB', 'Account' ,'Yes'
select * from Bill_To_Ship_To__c
where        Migrated_Record__c='true';--10068
*/
---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

--drop table Bill_To_Ship_To__c_Tritech_Excel_preload
select ID= CAST(SPACE(18) as NVARCHAR(18))
,ERROR= CAST(SPACE(255) as NVARCHAR(255))
,Migrated_Record__c ='true'
,Legacy_Source_System__c='Tritech'

      , [GP Company ID Orig]=trt.[GP Company ID] 
	  , Address_Code__c=trt.[Address Code]+'-'+trt.[GP Company ID]
	   --contactenate the Address Code column + '-' + GP Company Id
      ,Account_orig=trt.[Customer Number]
	  ,customernumber_before=trt.customernumber_before
	  ,tr_accountid=tr_act.id
	   ,Account__C=iif(sp_act.id is null,'Account Not Found',sp_act.id)
	  ,Account__C_sp=sp_act.id
     -- ,[Customer Name]
     -- ,[Hold]
     -- ,[Inactive]
	  ,Legacy_Address_ID__c=trt.[Customer Number]+': '+trt.[Address Code]
	  --concate Customer Number : Address code for unique value
      ,[Address Code orig] =trt.[Address Code]
      ,Attention_To__c=trt.[Contact Person]
      ,Street_1__c=trt.[Address 1]
	  ,Street_2__c=CONCAT( trt.[Address 2] ,char(13),char(10), trt.[Address 3])
      ,Street_2_orig=trt.[Address 2]
      ,[Address 3 Orig]=trt.[Address 3]
      ,City__c=trt.[City]
      ,State__c=trt.[State]
	  --,State2__c=trt.[State] --descripted SL 12/17
      ,Country__c=trt.[Country]
	   ,Country__c_orig=trt.[Country]
      ,Zip_Postal_Code__c=trt.[Zip]
      ,Phone__c_orig=trt.[Phone 1]
	   ,Phone1_orig=trt.[Phone1]
	  ,Phone__c=iif(len(trt.[Phone1])>5 and CHARINDEX('0000',trt.[Phone1],len(trt.[Phone1])-4)<>0,left(trt.[Phone1],len(trt.[Phone1])-4),trt.[Phone1])  --last 4 is extension, drop last 4
      --,[Phone 2]
      --,[Phone 3]
      --,[Fax]
      --,[Accounts Receivable Account Number]
      --,[Comment1]
      --,[Comment2]
      --,[Customer Class]
      ,Email__c=trt.[Email] --SL 2/20
      --,[Payment Terms ID]
      --,[PriceLevel]
	--  ,[Address Code orig]=trt.[Address Code]
	
	 /* ,Primary__C= Case when trt.[Primary Billto Address Code]=trt.[Address Code] then 'true'
	                    when trt.[Primary Shipto Address Code]=trt.[Address Code] then 'true'
						else 'false' end
						*/--old logic SL 12/17
, Primary__C= trt.[is primary address?] -- SL 12/17
      ,[Primary Billto Address Code Orig]=trt.[Primary Billto Address Code]
	 
	  ---For a Bill To address, for each customer,
	  -- when [address code] column = [Primary Billto Address Code] column ,
	  -- then mark this address record as Bill_To__c = 'true';
	 -- ,Bill_To__c=iif(trt.[Primary Billto Address Code]=trt.[Address Code],'true','false')--old logic

	  , Bill_To__c=trt.[is primary bill to address?] -- SL 12/17
     
	  ,[Primary Shipto Address Code Orig]=trt.[Primary Shipto Address Code]
	  --,Ship_to__c=iif(trt.[Primary Shipto Address Code]=trt.[Address Code]  ,'true','false') 	     --when [primaryshipto address code] = [address code] THEN 'true' --old logic
		 
		 ,Ship_to__c=[is primary ship to address?] -- SL 12/17
		 
		 --[Move to Netsuite] mark primary bill to and primary ship to = 'yes'
		/* ,Move_to_Accounting_system__c= Case when trt.[Primary Billto Address Code]=trt.[Address Code] then 'true'
	                    when trt.[Primary Shipto Address Code]=trt.[Address Code] then 'true'
						else 'false' end
		 --If  "transformed" primary__c for  bill to or "transformed" primary__c for ship to = 'true'  then = 'true'
						*/
		--New changes for Move_to_Accounting_system__c 3/11 SL
		--(If  "transformed" primary__c for  bill to or "transformed" primary__c for ship to = 'true') AND Adress 1 column is not blank or null then = 'true'.
		,Move_to_Accounting_system__c= Case when cast(trt.[is primary address?] as nvarchar(100))='1' and isnull(trt.[Address 1],' ')<>' ' then 'true'
	                    			else 'false' end

	 
		
      --,[Statement Address Code]
      --,[Statement Name]
      --,[Sales Territory]
      --,[Salesperson ID]
      --,[Tax Exempt 1]
      --,[Tax Exempt 2]
      --,[Tax Registration Number]
      ,Taxable__c_orig=[Tax Schedule ID] --CASE [Tax Schedule ID] = 'EXEMPT' then 'TRUE' ELSE FALSE.
	  ,Taxable__c=iif(trt.[Tax Schedule ID] ='EXEMPT','false','true')
	,Ownerid=sp_act.ownerid --OwnerID	populate with the owner of the Account
	,DeliveryPreference_orig=trt.[Delivery Preference]  --SL 2/20
	,InvoiceDeliveryMethod__c=iif(trt.[Delivery Preference]=1,'Email','Print') --SL 2/20--SL 3/12 corrected logic
	into Bill_To_Ship_To__c_Tritech_Excel_preload_fix
	--select tr_act.id,*  
  FROM [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2] trt
    left outer join [Tritech_PROD].[dbo].[Account] tr_act
  on   trt.[Customer Number]=  tr_act.EMS_Customer_Number_WMP__c
  left outer join [Superion_FULLSB].dbo.[Account] sp_act
  on tr_act.id=sp_act.LegacySFDCAccountId__c --and sp_act.Legacy_Source_System__c='Tritech'
  ;--10284

  select * from Bill_To_Ship_To__c_Tritech_Excel_preload
  where account_orig in ('CA297','OH175');

  select * 
   from [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2];

 

  

  select  [Primary Billto Address Code],[Address Code],[Primary Shipto Address Code],* from [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2]
  where [Customer Number]='CA146'
  order by 1
  

  select count(*) from Bill_To_Ship_To__c_Tritech_Excel_preload;--10284

  select count(*) from [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2];--10282

  select name,* from [Tritech_PROD].dbo.[Account]
  where EMS_Customer_Number_WMP__c='CA297';


  select EMS_Customer_Number_WMP__c,count(*) from [Tritech_PROD].dbo.[Account]
  --where EMS_Customer_Number_WMP__c like 'AK%'
  group by EMS_Customer_Number_WMP__c
  having count(*)>1;--8

  ;with cteDupe as
  (
  select EMS_Customer_Number_WMP__c,count(*) rec from [Tritech_PROD].dbo.[Account]
  --where EMS_Customer_Number_WMP__c like 'AK%'
  group by EMS_Customer_Number_WMP__c
  having count(*)>1
  )
select * from [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2] a
inner join cteDupe b
on a.[Customer Number]=b.EMS_Customer_Number_WMP__c;

--CA297
--OH175

  select account__C ,primary__C,ship_to__C, count(*) 
  --select *
   from Bill_To_Ship_To__c_Tritech_Excel_preload a
   where Primary__C=1 and Ship_to__c=1
  group by Account__C,primary__C,ship_to__C
  having count(*)>1;--144

   select account__C ,primary__C,Bill_To__c, count(*) 
   from Bill_To_Ship_To__c_Tritech_Excel_preload a
   where Primary__C=1 and Bill_To__c=1
  group by Account__C,primary__C,Bill_To__c
  having count(*)>1;--201

  select * from Bill_To_Ship_To__c_Tritech_Excel_preload
  --where account__C='Account Not Found';--1102
  where Bill_To__c=0 and Ship_to__c=0;--2415
 -- where Account__C='0010v00000HUm7nAAD'

  select * from Bill_To_Ship_To__c_Tritech_Excel_preload
  where account__C='Account Not Found'--1102
  and Bill_To__c=0 and Ship_to__c=0;--1280
  

  --drop table  Bill_To_Ship_To__c_Tritech_Excel_load;

  select * 
  into   Bill_To_Ship_To__c_Tritech_Excel_load
  from Bill_To_Ship_To__c_Tritech_Excel_preload
  where 1=1
 -- and Account__C is not null
  ;--10284

  select * from Bill_To_Ship_To__c_Tritech_Excel_load;--10284

 
  ---------------------------------------------------------------------------------
-- Insert records
---------------------------------------------------------------------------------
-- SL 12/17
--inactivated B2S2 trigger...
--email deliverability = 'No Access'

--exec SF_ColCompare 'Insert','SL_Superion_FULLSB', 'Bill_To_Ship_To__c_Tritech_Excel_load' 

--check column names

/*
Salesforce object Bill_To_Ship_To__c does not contain column GP Company ID Orig
Salesforce object Bill_To_Ship_To__c does not contain column Account_orig
Salesforce object Bill_To_Ship_To__c does not contain column tr_accountid
Salesforce object Bill_To_Ship_To__c does not contain column Account__C_sp
Salesforce object Bill_To_Ship_To__c does not contain column Address Code orig
Salesforce object Bill_To_Ship_To__c does not contain column Street_2_orig
Salesforce object Bill_To_Ship_To__c does not contain column Address 3 Orig
Salesforce object Bill_To_Ship_To__c does not contain column Country__c_orig
Salesforce object Bill_To_Ship_To__c does not contain column Phone__c_orig
Salesforce object Bill_To_Ship_To__c does not contain column Phone1_orig
Salesforce object Bill_To_Ship_To__c does not contain column Primary Billto Address Code Orig
Salesforce object Bill_To_Ship_To__c does not contain column Primary Shipto Address Code Orig
Salesforce object Bill_To_Ship_To__c does not contain column Taxable__c_orig
Salesforce object Bill_To_Ship_To__c does not contain column DeliveryPreference_orig
*/

--Exec SF_BulkOps 'Insert:batchsize(50)','SL_Superion_FULLSB','Bill_To_Ship_To__c_Tritech_Excel_load'

select error,count(*) from Bill_To_Ship_To__c_Tritech_Excel_load
group by error;

--update Bill_To_Ship_To__c_Tritech_Excel_load
set country__C='United States'
where country__C='USA';--10230

select * from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.' ;

select * from Bill_To_Ship_To__c_Tritech_Excel_load
where error<>'Operation Successful.' and Account__C='Account Not Found'--3762


select * from Bill_To_Ship_To__c_Tritech_Excel_load
where error<>'Operation Successful.' --and Account__C<>'Account Not Found'

select left(error,10),count(*) from Bill_To_Ship_To__c_Tritech_Excel_load
where error<>'Operation Successful.' and Account__C<>'Account Not Found'--3762
group by left(error,10);

select distinct error from Bill_To_Ship_To__c_Tritech_Excel_load
where error<>'Operation Successful.' and Account__C<>'Account Not Found';--3762


--------------
--drop table Bill_To_Ship_To__c_Tritech_Excel_load_errors
select *
into Bill_To_Ship_To__c_Tritech_Excel_load_errors
 from Bill_To_Ship_To__c_Tritech_Excel_load
where error<>'Operation Successful.'
;--1142

--drop table Bill_To_Ship_To__c_Tritech_Excel_load_errors_bkp
select *
into Bill_To_Ship_To__c_Tritech_Excel_load_errors_bkp
 from Bill_To_Ship_To__c_Tritech_Excel_load
where error<>'Operation Successful.'
;--1142

--update a set a.[customer Number]=b.tr_accountid
--select b.*, a.* 
from [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2] a
inner join [TRITECH_NON_SFDC].[dbo].tritech_B2s2_Account_fix b
on a.[customer Number]=b.Account_orig;--25

;with CteAccount as
(
select distinct a.account_orig,b.tr_accountid
 from Bill_To_Ship_To__c_Tritech_Excel_load a
 inner join [TRITECH_NON_SFDC].[dbo].tritech_B2s2_Account_fix b
on a.Account_orig=b.Account_orig
where account__C='Account Not Found'
)

select a.* 
into Bill_To_Ship_To__c_Tritech_Excel_load_Account_fix
from Bill_To_Ship_To__c_Tritech_Excel_preload_fix a
inner join [TRITECH_NON_SFDC].[dbo].tritech_B2s2_Account_fix b
on a.customernumber_before=b.Account_orig;--25


 select *
 from Bill_To_Ship_To__c_Tritech_Excel_load
where error<>'Operation Successful.'
