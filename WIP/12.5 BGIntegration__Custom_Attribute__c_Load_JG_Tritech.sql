/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Custom_Attribute__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'SL_Tritech_PROD','BGIntegration__Custom_Attribute__c'

Use Superion_FULLSB
EXEC SF_Refresh 'SL_Superion_FULLSB','User','Yes'
EXEC SF_Refresh 'SL_Superion_FULLSB','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__Custom_Attribute__c'
*/ 

Use Staging_SB;

--Drop table BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload;

Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,BGIntegration__BomgarSession__c_orig  =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c  =  bs.Id

,BGIntegration__Code__c           =  bg.BGIntegration__Code__c
,BGIntegration__Value__c          =  bg.BGIntegration__Value__c

,CreatedById_orig                 =  bg.CreatedById
,CreatedById                      =  createdbyuser.Id

,CreatedDate                      =  bg.CreatedDate
--,Legacy_ID__c                     =  Id
,Name                             =  bg.Name

into BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload

from 
Tritech_PROD.dbo.BGIntegration__Custom_Attribute__c bg

--Fetching CreatedById(UserLookup)
left join Superion_FULLSB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join Superion_FULLSB.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

--left join Superion_FULLSB.dbo.BGIntegration__Custom_Attribute__c trgt 
--on trgt.Legacy_Id__c = bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(97792 row(s) affected)

----------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Custom_Attribute__c;--97792

Select count(*) from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload;--97792

Select Legacy_Id__c,count(*) from Staging_SB.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load
from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload; --(97792 row(s) affected)

Select * from Staging_SB.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load;


--Exec SF_ColCompare_test 'Insert','SL_Superion_FullSB', 'BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__Custom_Attribute__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Custom_Attribute__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load'

----------------------------------------------------------------------------------------

Select *
--delete 
from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --37

--insert into BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load
select * from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Load_Delta; --483