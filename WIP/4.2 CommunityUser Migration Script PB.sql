-------------------------------------------------------------------------------
--- CommunityUser Migration Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 20 November 2018
--- Last Updated: 21 November 2018 - PAB
--- Change Log: 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUserLoad_PB') 
	DROP TABLE Staging_SB.dbo.[User_CommunityUserLoad_PB];


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Profile', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Account', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Contact', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes'

USE Tritech_PROD
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Case', 'yes'

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Tritech_Id__c,
		'Tritech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		Concat(a.[Username], '.CSFullSB') as Username,  --update for production
		a.[LastName] as LastName,
		isNull(a.[FirstName], '') as FirstName,
		isNull(a.[CompanyName], '') as CompanyName,
		isNull(a.[Division], '') as Division,
		isNull(a.[Department], '') as Department,
		isNull(a.[Title], '') as Title,
		isNull(a.[Street], '') as Street,
		isNull(a.[City], '') as City,
		isNull(a.[State], '') as State,
		isNull(a.[PostalCode], '') as PostalCode,
		isNull(a.[Country], '') as Country,
		Concat(a.[Email], '.CSFullSB') as Email,  --update for production
		isNull(a.[Phone], '') as Phone,
		isNull(a.[Fax], '') as Fax,
		isNull(a.[MobilePhone], '') as MobilePhone,
		a.[Alias] as Alias,
		Concat(a.[CommunityNickname], '1') as CommunityNickname,  -- Remove the '1' value for production load, only necessary for UAT
		'false' as IsActive,
		a.[TimeZoneSidKey] as TimeZoneSidKey,
		a.[LocaleSidKey] as LocaleSidKey,
		a.[ReceivesInfoEmails] as ReceivesInfoEmails,
		a.[ReceivesAdminInfoEmails] as ReceivesAdminInfoEmails,
		a.[EmailEncodingKey] as EmailEncodingKey,
		TT_Profile.[Name] as ProfileName_Original,
		Spr_Profile.ID as ProfileID,
		a.[LanguageLocaleKey] as LanguageLocaleKey,
		isNull(a.[EmployeeNumber], '') as EmployeeNumber,
		isNull(a.[UserPermissionsMarketingUser], '') as UserPermissionsMarketingUser,
		isNull(a.[UserPermissionsOfflineUser], '') as UserPermissionsOfflineUser,
		isNull(a.[UserPermissionsCallCenterAutoLogin], '') as UserPermissionsCallCenterAutoLogin,
		isNull(a.[UserPermissionsMobileUser], '') as UserPermissionsMobileUser,
		isNull(a.[UserPermissionsSFContentUser], '') as UserPermissionsSFContentUser,
		isNull(a.[UserPermissionsKnowledgeUser], '') as UserPermissionsKnowledgeUser,
		isNUll(a.[UserPermissionsInteractionUser], '') as UserPermissionsInteractionUser,
		isNull(a.[UserPermissionsSupportUser], '') as UserPermissionsSupportUser,
		a.[ForecastEnabled] as ForecastEnabled,
		a.[UserPreferencesActivityRemindersPopup] as UserPreferencesActivityRemindersPopup,
		a.[UserPreferencesEventRemindersCheckboxDefault] as UserPreferencesEventRemindersCheckboxDefault,
		a.[UserPreferencesTaskRemindersCheckboxDefault] as UserPreferencesTaskRemindersCheckboxDefault,
		a.[UserPreferencesReminderSoundOff] as UserPreferencesReminderSoundOff,
		a.[UserPreferencesApexPagesDeveloperMode] as UserPreferencesApexPagesDeveloperMode,
		a.[UserPreferencesHideCSNGetChatterMobileTask] as UserPreferencesHideCSNGetChatterMobileTask,
		a.[UserPreferencesHideCSNDesktopTask] as UserPreferencesHideCSNDesktopTask,
		a.[UserPreferencesSortFeedByComment] as UserPreferencesSortFeedByComment,
		a.[UserPreferencesLightningExperiencePreferred] as UserPreferencesLightningExperiencePreferred,
		isNull(a.[CallCenterId], '') as CallCenterId,
		isNull(a.[Extension], '') as Extension,
		isNull(a.[PortalRole], '') as PortalRole,
		isNull(a.[FederationIdentifier], '') as FederationIdentifier,
		isNull(a.[AboutMe], '') as AboutMe,
		a.[DigestFrequency] as DigestFrequency,
		isNull(Cont.ID, '') as ContactID, 
		isNull(a.[Bomgar_Username__c], '') as Bomgar_Username__c
		INTO Staging_SB.dbo.User_CommunityUserLoad_PB
		FROM Tritech_PROD.dbo.[User] a
		-- TriTech Profiles
		LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] TT_Profile ON  a.ProfileId = TT_Profile.ID
		-- Superion Profile
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Profile] Spr_Profile ON Spr_Profile.[Name] = 
								CASE TT_Profile.[Name] 
									WHEN 'TriTech Portal Read Only with Tickets' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Read-Only User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Standard User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Manager' THEN 'CentralSquare Community Standard - Dedicated'
									ELSE 'Superion standard user' END
		-- Contact
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Contact] Cont ON a.ContactID = Cont.Legacy_ID__c
		
		
		WHERE 
		TT_Profile.[Name] IN
					('TriTech Portal Read Only with Tickets',
					 'TriTech Portal Read-Only User',
					 'Overage High Volume Customer Portal User',
					 'Overage Customer Portal Manager Custom',
					 'TriTech Portal Standard User',
					 'TriTech Portal Manager')
		AND
		(
			(	
				a.isActive = 'true'
				AND  -- requires the user to have either logged into their account since 1/1/2018 or the account was created since 1/1/2018
				(
					a.LastLoginDate >= Cast('01/01/2018' as datetime)
					OR
					a.CreatedDate >= Cast('01/01/2018' as datetime)
				)
			)
			OR  -- Includes any Community User that has submitted a case since 1/1/2016
			(
			  a.ContactID IN
			  (
				select distinct contactID from Tritech_PROD.dbo.[CASE] 
				WHERE CreatedDate >= CAST('01/01/2016' as Datetime)
			  )
			)
		)
		Order by Spr_Profile.ID

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.user_CommunityUserLoad_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Delete preloaded records for a delta load
---------------------------------------------------------------------------------
Delete CommUsr_Load
FROM User_CommunityUserLoad_PB CommUsr_Load
INNER JOIN Superion_FULLSB.dbo.[User] ON CommUsr_Load.Legacy_Tritech_Id__c = [User].Legacy_Tritech_Id__c

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'upsert:batchsize(20)', 'PB_Superion_FULLSB', 'User_CommunityUserLoad_PB', 'Legacy_Tritech_ID__c'


--select * from User_CommunityUserLoad_PB where error not like '%success%'

