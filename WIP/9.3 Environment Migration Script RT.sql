/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Migrate Environment__c - Source System to SFDC
DEVELOPER	: RTECSON, MCORPUS, and PBOWEN
CREATED DT  : 01/15/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
12/18/2018		Patrick Bowen		Initial
01/15/2019		Ron Tecson			Modified to new specs. 
01/30/2019		Ron Tecson			Clean-up the script and uncomment "Environment__c_PreLoad_RT_1st, etc." and commented out "Environment__c_PreLoad_RT_1st_Revised_Name_1, etc."
01/31/2019		Ron Tecson			Additional Clean-Up. Kept the original script on OneNote.
									Merged the script for the creation of the Zuercher Environments
									Modified the script for the creation of Accounts with No Tritech Hardware and Software records.
01/31/2019		Ron Tecson			Added a condition tProd1.Product_Group__c <> 'Zuercher'.
02/05/2019		Ron Tecson			Cleaned-up the script and used the proper staging and load tables. Reloaded the Target.
02/12/2019		Ron Tecson			Wipe and Reload. Include the log.
02/13/2019		Ron Tecson			Commented out WHERE a.Name NOT IN ('HS-102676', 'HS-102674', 'HS-120167', 'HS-119845') and 
									Commented out AND tAccount.Client__c = 'true' 
02/14/2019		Ron Tecson			Added Replace(tAccount.Name, CHAR(39), '') to remove the single quote for the Environment Name, Per Gabe. 
02/15/2019		Ron Tecson			Executed the script and placed the logs.
02/19/2019		Ron Tecson			Executed the script and placed the logs. Separated the script for the creation of the Zuercher Products. Removed and saved it 9.3.2.
02/26/2019		Ron Tecson			Wipe and Reload
03/04/2019		Ron Tecson			Wipe and Reload
03/12/2019      Ron Tecson			Wipe and Reload. For the next load, Replaced 'HS-119845' with 'HS-119844' to be excluded in the where clause.
03/26/2019		Ron Tecson			Wipe and Reload. Execution Time: 22 mins.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_Prod', 'Hardware_Software__c', 'subset';
EXEC SF_Refresh 'MC_Tritech_Prod', 'RecordType', 'subset';
EXEC SF_Refresh 'MC_Tritech_Prod', 'Account', 'subset';

USE Superion_FULLSB
EXEC SF_Refresh 'RT_Superion_FullSB', 'Account', 'subset';					-- 10 mins
EXEC SF_Refresh 'RT_Superion_FullSB', 'Registered_Product__c', 'subset';
EXEC SF_Refresh 'RT_Superion_FullSB', 'Product2', 'subset';
EXEC SF_Refresh 'RT_Superion_FullSB', 'CUSTOMER_ASSET__C', 'subset';		-- 8 mins.

drop table Staging_SB.dbo.Environment__c_For_Deletion

select id, CAST('' AS nvarchar(255)) AS error
INTO Staging_SB.dbo.Environment__c_For_Deletion
from RT_Superion_FULLSB...Environment__c where Migrated_Record__c = 'true'

USE Staging_SB

EXEC sf_bulkops 'DELETE','RT_Superion_FULLSB','Environment__c_For_Deletion'

select * from Environment__c_For_Deletion

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_PreLoad_RT_1st
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_RT_1st') 
DROP TABLE Staging_SB.dbo.Environment__c_PreLoad_RT_1st;

---------------------------------------------------------------------------------
-- Load Environment Preload Table: Environment__c_PreLoad_RT_1st
---------------------------------------------------------------------------------

USE Staging_SB;

WITH 
cte_Environment_Type AS
	(
			SELECT 
				ID AS Legacy_ID__c, 
				'Prod' AS Environment_Type
			FROM Tritech_PROD.dbo.Hardware_Software__c 

			UNION

			SELECT 
			ID AS Legacy_ID__c, 
			'Test' AS Environment_Type 
			FROM Tritech_PROD.dbo.Hardware_Software__c 
			WHERE 
			(
				isnull(VisiNet_Test_System_Version__c, '') <> ''
				OR isNull(VisiNet_Mobile_Test_Version__c, '') <> ''
				OR isnull(FBR_Test_Software_Version__c, '') <> ''
				OR isNULL(TTMS_Test_Software_Version__c, '') <> ''
				OR isNull(CIM_Test_Software_Version__c, '') <> ''
				OR isNull(RMS_Test_Software_Version__c, '') <> ''
				OR isNull(RMS_Web_Test_Software_Version__c, '') <> ''
				OR isNull(Tib_DEV_Build__c, '') <> ''
			)

			UNION

			SELECT 
				a.ID AS Legacy_ID__c, 
				'Train' AS Environment_Type 
			FROM Tritech_PROD.dbo.Hardware_Software__c a
			WHERE 
			(
				isnull(VisiNet_Training_System_Version__c, '') <> ''
				OR isNull(VisiNet_Mobile_Training_Version__c, '') <> ''
				OR isnull(FBR_Training_Software_Version__c, '') <> ''
				OR isNULL(TTMS_Training_Software_Version__c, '') <> ''
				OR isNull(CIM_Training_Software_Version__c, '') <> ''
				OR isNull(RMS_Training_Software_Version__c, '') <> ''
				OR isNull(RMS_Web_Training_Software_Version__c, '') <> ''
				OR isNull(Tib_TRN_Build__c, '') <> ''
			)
	)

SELECT 
	CAST('' AS NVARCHAR(255)) AS ID,
	CAST('' AS nvarchar(255)) AS error,
	a.ID AS Legacy_SystemID__c,
	'TriTech' AS Legacy_Source_System__c,
	'true' AS Migrated_Record__c,
	sRecType.[Name] AS RecordType_Orig,
	tAccount.[Name] AS AccountName,
	SUBSTRING(
		CASE	 WHEN sRecType.[Name] = '911 Hardware and Software' THEN CONCAT(EnvType.Environment_Type, '-911-', tAccount.[Name])
				 WHEN sRecType.[Name] = 'IMC Hardware and Software' THEN CONCAT(EnvType.Environment_Type, '-IMC-', tAccount.[Name])
				 WHEN sRecType.[Name] = 'Inform Hardware and Software' THEN CONCAT(EnvType.Environment_Type, '-Inform-', tAccount.[Name])
				 WHEN sRecType.[Name] = 'Respond Hardware and Software' THEN CONCAT(EnvType.Environment_Type, '-Respond-', tAccount.[Name])
				 WHEN sRecType.[Name] = 'Tiburon Hardware and Software' THEN CONCAT(EnvType.Environment_Type, '-Tiburon-', tAccount.[Name])
				 WHEN sRecType.[Name] = 'VisionAIR Hardware and Software' AND
																	(a.VA_CAD_Product__c is NULL OR
																	 a.VA_Mobile_Software_Version__c IS NULL)	THEN CONCAT(EnvType.Environment_Type, '-Inform-', tAccount.[Name])
				WHEN sRecType.[Name] = 'VisionAIR Hardware and Software' THEN CONCAT(EnvType.Environment_Type, '-Vision-', tAccount.[Name])
				WHEN sRecType.[Name] = 'Impact Hardware and Software' THEN CONCAT(EnvType.Environment_Type, '-Impact-', tAccount.[Name])
		ELSE 'MISSING NAME'
		END, 1, 80) AS [Name],
	tAccount.ID AS Account__c,
	'Yes' AS Active__c,
	CASE	EnvType.Environment_Type 
			WHEN 'Prod' THEN 'Production'
			WHEN 'Train' THEN 'Training'
			WHEN 'Test' THEN 'Test'
			ELSE NULL 
	END AS Type__c,
	CASE tAccount.Bomgar_Approved_Client__c WHEN 'True' THEN 'Bomgar' ELSE '' END AS Connection_Type__c,
	CONCAT(sRecType.[Name], ': ', a.[NAME]) AS [Description__c],
	CONCAT('Secure Folder ID: ', sAccount.Secure_Folder_ID__c) AS Notes__c,
	a.[NAME] as HS_ID,
	EnvType.Environment_Type
INTO Environment__c_PreLoad_RT_1st
FROM Tritech_Prod.dbo.Hardware_Software__c a
	LEFT OUTER JOIN Superion_FullSB.dbo.Account tAccount ON a.Account_Name__c = tAccount.LegacySFDCAccountId__c -----> Account
	LEFT OUTER JOIN Tritech_Prod.dbo.Account sAccount ON tAccount.LegacySFDCAccountId__c = sAccount.ID			-----> Src Account
	LEFT OUTER JOIN TriTech_Prod.dbo.RecordType sRecType ON a.RecordTypeId = sRecType.ID						-----> RecordType
	INNER JOIN cte_Environment_Type EnvType ON a.ID = EnvType.Legacy_ID__c										-----> Environment Type assignment
WHERE a.Name NOT IN ('HS-119844') --('HS-102676', 'HS-102674', 'HS-120167', 'HS-119845')
ORDER BY tAccount.Id

-- 2/12 -- (4975 rows affected)
-- 2/15 -- (4977 rows affected)
-- 2/18 -- (4976 rows affected)
-- 2/26 -- (4976 rows affected)
-- 3/03 -- (4980 rows affected)
-- 3/12 -- (4985 rows affected)
-- 3/15 -- (4988 rows affected)
-- 3/26 -- (4988 rows affected)

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_PreLoad_RT_2nd
---------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_RT_2nd') 
DROP TABLE Staging_SB.dbo.Environment__c_PreLoad_RT_2nd;

---------------------------------------------------------------------------------
-- Load Environment Preload Table: Environment__c_PreLoad_RT_1st
---------------------------------------------------------------------------------

SELECT 
	ID, 
	error, 
	Legacy_Source_System__c, 
	Migrated_Record__c, 
	Account__c, 
	CONCAT(Name, ' - ', AccountName) as AccountName, 
	[NAME], 
	Active__c, 
	Type__c, 
	Description__c = 'Hardware and Software: ' + STUFF
		(
			( 	-- You only want to combine rows for a single ID here:
				SELECT ', ' + HS_ID
				
				FROM Environment__c_PreLoad_RT_1st AS T2 
				-- You only want to combine rows for a single ID here:
				WHERE T2.Account__c = T1.Account__c and T1.Type__c = T2.Type__c
				ORDER BY Type__c
				FOR XML PATH (''), TYPE
			).value('.', 'varchar(max)')
		, 1, 1, ''), 
	LegacySystemIds = STUFF
		(
			( 	-- You only want to combine rows for a single ID here:
				SELECT ', ' + Legacy_SystemID__c + '-' + Environment_Type
				FROM Environment__c_PreLoad_RT_1st AS T2 
				----FROM Environment__c_PreLoad_RT_1st_Revised_Name_1 as T2
				WHERE T2.Account__c = T1.Account__c and T1.Type__c = T2.Type__c
				ORDER BY Type__c
				FOR XML PATH (''), TYPE
			).value('.', 'varchar(max)')
		, 1, 1, ''),
	RecordTypes = STUFF
		(
			( 	-- You only want to combine rows for a single ID here:
				SELECT ', ' + RecordType_Orig
				FROM Environment__c_PreLoad_RT_1st AS T2 --> Ron
				WHERE T2.Account__c = T1.Account__c and T1.Type__c = T2.Type__c
				ORDER BY Type__c
				FOR XML PATH (''), TYPE
			).value('.', 'varchar(max)')
		, 1, 1, ''), 
	connection_type__c, 
	Notes__c
INTO Staging_SB.dbo.Environment__c_PreLoad_RT_2nd
FROM Staging_SB.dbo.Environment__c_PreLoad_RT_1st AS T1
WHERE Account__c IS NOT NULL
GROUP BY ID, error, Legacy_Source_System__c, Migrated_Record__c, Account__c, AccountName, Name, Active__c, Type__c,connection_type__c, Notes__c

-- 2/12 (4787 rows affected)
-- 2/15 (4788 rows affected)
-- 2/18 (4788 rows affected)
-- 2/26 (4788 rows affected)
-- 3/03 (4791 rows affected)
-- 3/12 (4789 rows affected)
-- 3/15 (4792 rows affected)
-- 3/26 (4791 rows affected)

select * from Environment__c_PreLoad_RT_1st where Account__c is null

select * from Staging_SB.dbo.Environment__c_PreLoad_RT_2nd order by Account__c

select Name, count(*) from Environment__c_PreLoad_RT_2nd 
group by Name
having count(*) > 1

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_PreLoad_RT_3rd
---------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_RT_3rd') 
DROP TABLE Staging_SB.dbo.Environment__c_PreLoad_RT_3rd;

---------------------------------------------------------------------------------
-- Load Environment Preload Table: Environment__c_PreLoad_RT_3rd
---------------------------------------------------------------------------------

WITH

	cte_TTZ_MergedAccount AS
		(SELECT id,
			 NAME,
			  LegacySFDCAccountId__c AS Legacy_TT_Accountid,
			  Client__c
		FROM   superion_fullsb.dbo.ACCOUNT
		WHERE  migrated_record__c = 'True' AND
			 legacy_source_system__c = 'Tritech'
		UNION
		SELECT ffSAccnt.[18-digit sup account id] AS ID,
			ffSAccnt.[sup account name]        AS NAME,
			ffSAccnt.[18-digit TT Account ID]  AS Legacy_TT_Accountid,
			tAccntCte.Client__c
		FROM   TT_SUPERION_ACCOUNT_MERGE ffSAccnt
		JOIN Superion_FULLSB.dbo.Account tAccntCte ON tAccntCte.Id = ffSAccnt.[18-digit sup account id]
		WHERE  [merge?] = 'Yes'
		)

	/* Used for Zuercher Environment 
	, cte_Environment AS 
		(
		)
	*/

SELECT * 
INTO Staging_SB.dbo.Environment__c_PreLoad_RT_3rd
FROM
	(
	SELECT 
		ID
		, error
		, Legacy_Source_System__c
		, Migrated_Record__c
		, Account__c
		, AccountName
		, Replace(Name, CHAR(39), '') Name
		, Active__c
		, Type__c
		, CAST(Description__c AS NVARCHAR(MAX)) AS Description__c
		, CAST(LTRIM(RTRIM(IIF(CHARINDEX(',',LegacySystemIds) > 1, SUBSTRING(LegacySystemIds,1, CHARINDEX(',',LegacySystemIds)-1),LegacySystemIds))) AS NVARCHAR(MAX)) AS Legacy_SystemID__c
		, CAST(LegacySystemIds AS NVARCHAR(MAX)) AS LegacySystemIds
		, IIF(CHARINDEX(',',RecordTypes) > 1, SUBSTRING(RecordTypes,1, CHARINDEX(',',RecordTypes)-1),RecordTypes) AS Winning_RecordType 
		, RecordTypes
		, connection_type__c
		, Notes__c
	FROM Staging_SB.dbo.Environment__c_PreLoad_RT_2nd

/*********************************** Commented Out per Maria 3/14
	UNION

	/*Create Environment records for Accounts with out Hardware and Software TriTech Records*/
	SELECT DISTINCT 
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' AS Legacy_Source_System__c,
		'true' AS Migrated_Record__c,
		tAccount.Id AS Account__c,
		tAccount.Name AS AccountName,
		--SUBSTRING (Concat('Prod-',tAccount.Name),1, 80) AS [Name], 
		SUBSTRING (Concat('Prod-',Replace(tAccount.Name, CHAR(39), '')),1, 80) AS [Name],
		'Yes' AS Active__c,
		'Production' AS Type__c,
		'Automatically created because no Hardware Software records existed in Tritech source' AS Description__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
	FROM cte_TTZ_MergedAccount tAccount
		LEFT OUTER JOIN Superion_FULLSB.dbo.Registered_Product__c tRP1 ON tRP1.Account__c = tAccount.Id 
		LEFT OUTER JOIN Superion_FULLSB.dbo.product2 tProd1 ON tProd1.Id = tRP1.Product__c and tProd1.TTZ_Product__c = 'true' and tProd1.Product_Group__c <> 'Zuercher'
		LEFT OUTER JOIN Tritech_Prod.dbo.Hardware_Software__c a ON a.Account_Name__c = tAccount.Legacy_TT_Accountid
	WHERE
		a.Id IS NULL 
		-- AND tAccount.Client__c = 'true' 
***************************************** Commented Out per Maria 3/14 */

	/*
	UNION

	/*Create Environment records for Zuercher Records*/
	SELECT DISTINCT
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		Account__c,
		AccountName,
		Replace(Name, CHAR(39), '') AS [Name],
		'Yes' AS Active__c,
		IIF(Environment_Type = 'Prod', 'Production', Environment_Type) AS Type__c,
		'Zuercher' + '-' + Product_Line__c as Description__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
	FROM cte_Environment
	*/
) A
order by Account__c

-- 2/12 -- (7229 rows affected)
-- 2/18 -- (26845 rows affected)
-- 2/26 -- (26844 rows affected)
-- 3/03 -- (26847 rows affected)
-- 3/12 -- (26826 rows affected)
-- 3/15 -- (4792 rows affected)
-- 3/26 -- (4791 rows affected)

select Legacy_SystemID__c, count(*) from Staging_SB.dbo.Environment__c_PreLoad_RT_3rd
group by Legacy_SystemID__c
having count(*) > 1

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_Load_RT
---------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_Load_RT') 
DROP TABLE Staging_SB.dbo.Environment__c_Load_RT;

--------------------------------------------------------------------------------
-- Load Environment Preload Table: Environment__c_Load_RT
--------------------------------------------------------------------------------
SELECT * 
INTO Staging_sb.dbo.Environment__c_Load_RT 
FROM Staging_sb.dbo.Environment__c_PreLoad_RT_3rd

-- 2/15 -- (26953 rows affected)
-- 2/18 -- (26845 rows affected)
-- 2/26 -- (26844 rows affected)
-- 3/03 -- (26847 rows affected)
-- 3/12 -- (26826 rows affected)
-- 3/15 -- (4792 rows affected)
-- 3/26 -- (4791 rows affected)

--------------------------------------------------------------------------------
-- Migrate Data Into SFDC Environment__c
--------------------------------------------------------------------------------
USE Staging_SB

EXEC SF_ColCompare 'INSERT','RT_Superion_FULLSB','Environment__c_Load_RT'

/****************************************************************************************

--- Starting SF_ColCompare V3.6.9
Problems found with Environment__c_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 414]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment__c does not contain column AccountName
Salesforce object Environment__c does not contain column LegacySystemIds
Salesforce object Environment__c does not contain column Winning_RecordType
Salesforce object Environment__c does not contain column RecordTypes

****************************************************************************************/

EXEC sf_bulkops 'INSERT','RT_Superion_FULLSB','Environment__c_Load_RT'

/****************************************************************************************

3/26:
--- Starting SF_BulkOps for Environment__c_Load_RT V3.6.9
15:42:16: Run the DBAmp.exe program.
15:42:16: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:42:16: Inserting Environment__c_Load_RT (SQL01 / Staging_SB).
15:42:17: DBAmp is using the SQL Native Client.
15:42:17: SOAP Headers: 
15:42:17: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
15:42:17: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
15:42:17: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
15:42:17: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
15:42:54: 4791 rows read from SQL Table.
15:42:54: 4791 rows successfully processed.
15:42:54: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


3/20:
--- Starting SF_BulkOps for Environment__c_Load_RT V3.6.9
13:29:29: Run the DBAmp.exe program.
13:29:29: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
13:29:29: Inserting Environment__c_Load_RT (SQL01 / Staging_SB).
13:29:29: DBAmp is using the SQL Native Client.
13:29:31: SOAP Headers: 
13:29:31: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
13:29:31: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
13:29:31: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
13:29:31: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
13:30:27: 4791 rows read from SQL Table.
13:30:27: 4791 rows successfully processed.
13:30:27: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

3/15:
--- Starting SF_BulkOps for Environment__c_Load_RT V3.6.9
17:26:11: Run the DBAmp.exe program.
17:26:11: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:26:11: Inserting Environment__c_Load_RT (SQL01 / Staging_SB).
17:26:12: DBAmp is using the SQL Native Client.
17:26:13: SOAP Headers: 
17:26:13: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
17:26:13: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
17:26:13: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
17:26:13: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:26:59: 4792 rows read from SQL Table.
17:26:59: 4792 rows successfully processed.
17:26:59: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

3/12:

--- Starting SF_BulkOps for Environment__c_Load_RT V3.6.9
17:19:48: Run the DBAmp.exe program.
17:19:48: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:19:48: Inserting Environment__c_Load_RT (SQL01 / Staging_SB).
17:19:48: DBAmp is using the SQL Native Client.
17:19:50: SOAP Headers: 
17:19:50: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
17:19:50: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
17:19:50: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
17:19:50: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:24:29: 26826 rows read from SQL Table.
17:24:29: 26826 rows successfully processed.
17:24:30: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


2/26:

--- Starting SF_BulkOps for Environment__c_Load_RT V3.6.9
17:04:25: Run the DBAmp.exe program.
17:04:25: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:04:25: Inserting Environment__c_Load_RT (SQL01 / Staging_SB).
17:04:26: DBAmp is using the SQL Native Client.
17:04:26: SOAP Headers: 
17:04:26: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
17:04:26: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
17:04:26: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
17:04:26: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:08:05: 26844 rows read from SQL Table.
17:08:05: 26844 rows successfully processed.
17:08:05: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


--- Starting SF_BulkOps for Environment__c_Load_RT V3.6.9
16:56:26: Run the DBAmp.exe program.
16:56:26: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
16:56:26: Inserting Environment__c_Load_RT (SQL01 / Staging_SB).
16:56:27: DBAmp is using the SQL Native Client.
16:56:29: SOAP Headers: 
16:56:29: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
16:56:29: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
16:56:29: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
16:56:29: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:00:40: 26953 rows read from SQL Table.
17:00:40: 26953 rows successfully processed.
17:00:40: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

****************************************************************************************/

--------------------------------------------------------------------------------
-- Validation
--------------------------------------------------------------------------------

SELECT error, count(*) 
FROM Staging_sb.dbo.Environment__c_Load_RT 
GROUP BY error

select * from Staging_sb.dbo.Environment__c_Load_RT 

USE Superion_FULLSB

EXEC SF_Refresh 'RT_Superion_FullSB', 'Environment__c', 'yes';

---------------------------- SECTION: Address errors.

--Select * from Environment__c_Load_PB where error not like '%Success%'

Your attempt to delete Prod-IMC-Newton Police Department, NH could not be completed because it is associated with the following cases.: 
02023704 (Prod-IMC-Newton Police Department, NH)
, 02023705 (Prod-IMC-Newton Police Department, NH)

select * INTO Environment__c_Load_RT_Error1 from Environment__c_Load_RT
where error = 'unable to obtain exclusive access to this record or 195 records: 0010v00000IkMFXAA3,0010v00000IkMExAAN,0010v00000IkMEyAAN,0010v00000IkLUKAA3,0010v00000IkMEzAAN,0010v00000IkLUDAA3,0010v00000IkMEuAAN,0010v00000IkM5fAAF,0010v00000IkMfwAAF,0010v00000IkMEvAAN'
--and account__c = '0010v00000IkMFXAA3'

Exec sf_bulkops 'INSERT','PB_Superion_FULLSB','Environment__c_Load_RT_Error1'

select * from Environment__c_Load_RT_Error2 from Environment__c_Load_RT
where
error = 'unable to obtain exclusive access to this record or 196 records: 0010v00000IkNzMAAV,0010v00000IkNzNAAV,0016A00000AMs9vQAD,0010v00000IkLUGAA3,0010v00000IkN92AAF,0010v00000IkOKEAA3,0010v00000IkNZ6AAN,0010v00000IkLV4AAN,0010v00000IkNzOAAV,0010v00000IkNYPAA3'

Exec sf_bulkops 'INSERT','PB_Superion_FULLSB','Environment__c_Load_RT_Error2'

update m1
set m1.ID = t1.ID, 
m1.error = t1.error
from Environment__c_Load_RT m1
inner join Environment__c_Load_RT_Error1 t1 on m1.Legacy_SystemID__c = t1.Legacy_SystemID__c

update m1
set m1.ID = t1.ID, 
m1.error = t1.error
from Environment__c_Load_RT m1
inner join Environment__c_Load_RT_Error2 t1 on m1.Legacy_SystemID__c = t1.Legacy_SystemID__c


WITH Environment_Type 
AS
(
		select 
		ID as Legacy_ID__c, 
		'Prod' as Environment_Type
		from Tritech_PROD.dbo.Hardware_Software__c 

		UNION

		select 
		ID as Legacy_ID__c, 
		'Test' as Environment_Type 
		from Tritech_PROD.dbo.Hardware_Software__c 
		where 
		(
		isnull(VisiNet_Test_System_Version__c, '') <> ''
		OR isNull(VisiNet_Mobile_Test_Version__c, '') <> ''
		OR isnull(FBR_Test_Software_Version__c, '') <> ''
		OR isNULL(TTMS_Test_Software_Version__c, '') <> ''
		OR isNull(CIM_Test_Software_Version__c, '') <> ''
		OR isNull(RMS_Test_Software_Version__c, '') <> ''
		OR isNull(RMS_Web_Test_Software_Version__c, '') <> ''
		OR isNull(Tib_DEV_Build__c, '') <> ''
		)

		UNION

		select 
		a.ID as Legacy_ID__c, 
		'Train' as Environment_Type 
		from Tritech_PROD.dbo.Hardware_Software__c a
		where 
		(
		isnull(VisiNet_Training_System_Version__c, '') <> ''
		OR isNull(VisiNet_Mobile_Training_Version__c, '') <> ''
		OR isnull(FBR_Training_Software_Version__c, '') <> ''
		OR isNULL(TTMS_Training_Software_Version__c, '') <> ''
		OR isNull(CIM_Training_Software_Version__c, '') <> ''
		OR isNull(RMS_Training_Software_Version__c, '') <> ''
		OR isNull(RMS_Web_Training_Software_Version__c, '') <> ''
		OR isNull(Tib_TRN_Build__c, '') <> ''
		)
)

select tAccount.LegacySFDCAccountId__c,  a.RecordTypeId , sRecType.ID, a.ID , EnvType.Legacy_ID__c
	From Tritech_Prod.dbo.Hardware_Software__c a
-- Account
INNER JOIN Superion_FullSB.dbo.Account tAccount ON a.Account_Name__c = tAccount.LegacySFDCAccountId__c
LEFT OUTER JOIN TriTech_Prod.dbo.RecordType sRecType ON a.RecordTypeId = sRecType.ID
-- Environment Type assignment
INNER JOIN Environment_Type EnvType ON a.ID = EnvType.Legacy_ID__c
--where acct.id = '0010v00000IkKAuAAN'
--order by tAccount.Id
where a.Account_Name__c = '0018000001PZcc3AAD'

/******************************* LOG ***************************************

-- 02/05/2019
--- Starting SF_BulkOps for Environment__c_Load_RT V3.6.9
02:45:28: Run the DBAmp.exe program.
02:45:28: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
02:45:28: Inserting Environment__c_Load_RT (SQL01 / Staging_SB).
02:45:28: DBAmp is using the SQL Native Client.
02:45:29: SOAP Headers: 
02:45:29: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
02:45:29: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
02:45:29: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
02:45:29: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
02:46:29: 7227 rows read from SQL Table.
02:46:29: 7227 rows successfully processed.
02:46:29: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

******************************* LOG ***************************************/

drop table Environment__c_For_Deletion

select id, CAST('' AS NVARCHAR(255)) AS error  INTO Environment__c_For_Deletion from RT_Superion_FULLSB...Environment__c where Migrated_Record__c = 'true'

EXEC sf_bulkops 'DELETE','RT_Superion_FULLSB','Environment__c_For_Deletion'