/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: CommunityUser Migration Script
DEVELOPER	: RTECSON
CREATED DT  : 02/06/2019
DETAIL		: 	
				Selection Criteria:
				a. Migrate all related defects related to a migrated case.

				Discussion: Will migrate to Engineering_Issue__c
				need to Validate with Engineering what their requirements are, and need to also confirm requirements specific to defects NOT related to a case.
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/06/2019		Ron Tecson			Initial. Copied and modified from 4.2 CommunityUser Migration Script PB.sql.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Superion_FULLSB
Exec SF_Refresh 'RT_Superion_FULLSB', 'Profile', 'yes'
Exec SF_Refresh 'RT_Superion_FULLSB', 'Account', 'yes'
Exec SF_Refresh 'RT_Superion_FULLSB', 'Contact', 'yes'
Exec SF_Refresh 'RT_Superion_FULLSB', 'User', 'yes'

USE Tritech_PROD
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Case', 'yes'

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUser_PreLoad_RT') 
	DROP TABLE Staging_SB.dbo.User_CommunityUser_PreLoad_RT;


---------------------------------------------------------------------------------
-- Load Staging Table -- Estimated Query Time: 3:10 for 8128 records
---------------------------------------------------------------------------------
USE Staging_SB;

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Tritech_Id__c,
		'Tritech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		Concat(a.[Username], '.CSFullSB') as Username,  --update for production
		a.[LastName] as LastName,
		a.[FirstName] as FirstName,
		a.[CompanyName] as CompanyName,
		a.[Division] as Division,
		a.[Department] as Department,
		a.[Title] as Title,
		a.[Street] as Street,
		a.[City] as City,
		a.[State] as State,
		a.[PostalCode] as PostalCode,
		a.[Country] as Country,
		Concat(a.[Email], '.CSFullSB') as Email,  --update for production
		a.[Phone] as Phone,
		a.[Fax] as Fax,
		a.[MobilePhone] as MobilePhone,
		a.[Alias] as Alias,
		Concat(a.[CommunityNickname], '1') as CommunityNickname,  -- Remove the '1' value for production load, only necessary for UAT
		'false' as IsActive,
		a.[TimeZoneSidKey] as TimeZoneSidKey,
		a.[LocaleSidKey] as LocaleSidKey,
		a.[ReceivesInfoEmails] as ReceivesInfoEmails,
		a.[ReceivesAdminInfoEmails] as ReceivesAdminInfoEmails,
		a.[EmailEncodingKey] as EmailEncodingKey,
		TT_Profile.[Name] as ProfileName_Original,
		Spr_Profile.ID as ProfileID,
		a.[LanguageLocaleKey] as LanguageLocaleKey,
		a.[EmployeeNumber] as EmployeeNumber,
		a.[UserPermissionsMarketingUser] as UserPermissionsMarketingUser,
		a.[UserPermissionsOfflineUser] as UserPermissionsOfflineUser,
		a.[UserPermissionsCallCenterAutoLogin] as UserPermissionsCallCenterAutoLogin,
		a.[UserPermissionsMobileUser] as UserPermissionsMobileUser,
		a.[UserPermissionsSFContentUser] as UserPermissionsSFContentUser,
		a.[UserPermissionsKnowledgeUser] as UserPermissionsKnowledgeUser,
		a.[UserPermissionsInteractionUser] as UserPermissionsInteractionUser,
		a.[UserPermissionsSupportUser] as UserPermissionsSupportUser,
		a.[ForecastEnabled] as ForecastEnabled,
		a.[UserPreferencesActivityRemindersPopup] as UserPreferencesActivityRemindersPopup,
		a.[UserPreferencesEventRemindersCheckboxDefault] as UserPreferencesEventRemindersCheckboxDefault,
		a.[UserPreferencesTaskRemindersCheckboxDefault] as UserPreferencesTaskRemindersCheckboxDefault,
		a.[UserPreferencesReminderSoundOff] as UserPreferencesReminderSoundOff,
		a.[UserPreferencesApexPagesDeveloperMode] as UserPreferencesApexPagesDeveloperMode,
		a.[UserPreferencesHideCSNGetChatterMobileTask] as UserPreferencesHideCSNGetChatterMobileTask,
		a.[UserPreferencesHideCSNDesktopTask] as UserPreferencesHideCSNDesktopTask,
		a.[UserPreferencesSortFeedByComment] as UserPreferencesSortFeedByComment,
		a.[UserPreferencesLightningExperiencePreferred] as UserPreferencesLightningExperiencePreferred,
		a.[CallCenterId] as CallCenterId,
		a.[Extension] as Extension,
		a.[PortalRole] as PortalRole,
		a.[FederationIdentifier] as FederationIdentifier,
		a.[AboutMe] as AboutMe,
		a.[DigestFrequency] as DigestFrequency,
		Cont.ID as ContactID, 
		a.[Bomgar_Username__c] as Bomgar_Username__c
		--INTO Staging_SB.dbo.User_CommunityUser_PreLoad_RT
		FROM Tritech_PROD.dbo.[User] a
		LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] TT_Profile ON  a.ProfileId = TT_Profile.ID											----> TriTech Profiles
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Profile] Spr_Profile ON Spr_Profile.[Name] =												----> Superion Profile
								CASE TT_Profile.[Name] 
									WHEN 'TriTech Portal Read Only with Tickets' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Read-Only User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Standard User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Manager' THEN 'CentralSquare Community Standard - Dedicated'
									ELSE 'Superion standard user' END
		LEFT OUTER JOIN Superion_FULLSB.dbo.[Contact] Cont ON a.ContactID = Cont.Legacy_ID__c											----> Superion Contact
		WHERE 
		TT_Profile.[Name] IN
					('TriTech Portal Read Only with Tickets',
					 'TriTech Portal Read-Only User',
					 'Overage High Volume Customer Portal User',
					 'Overage Customer Portal Manager Custom',
					 'TriTech Portal Standard User',
					 'TriTech Portal Manager')
		AND
		(
			(	
				a.isActive = 'true'
				AND  -- requires the user to have either logged into their account since 1/1/2018 or the account was created since 1/1/2018
				(
					a.LastLoginDate >= Cast('01/01/2018' as datetime)
					OR
					a.CreatedDate >= Cast('01/01/2018' as datetime)
				)
			)
			OR  -- Includes any Community User that has submitted a case since 1/1/2016
			(
			  a.ContactID IN
			  (
				select distinct contactID from Tritech_PROD.dbo.[CASE] 
				WHERE CreatedDate >= CAST('01/01/2016' as Datetime)
			  )
			)
		)
		Order by Spr_Profile.ID

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUser_PreLoad_RT') 
	DROP TABLE Staging_SB.dbo.User_CommunityUser_PreLoad_RT;

SELECT * 
INTO Staging_SB.dbo.User_CommunityUser_Load_RT
FROM Staging_SB.dbo.User_CommunityUser_PreLoad_RT

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.User_CommunityUser_Load_RT
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Delete preloaded records for a delta load
---------------------------------------------------------------------------------
Delete CommUsr_Load
FROM User_CommunityUser_Load_RT CommUsr_Load
INNER JOIN Superion_FULLSB.dbo.[User] ON CommUsr_Load.Legacy_Tritech_Id__c = [User].Legacy_Tritech_Id__c

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB

Exec SF_BulkOps 'upsert:batchsize(20)', 'RT_Superion_FULLSB', 'User_CommunityUser_Load_RT', 'Legacy_Tritech_ID__c'

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

SELECT ERROR, count(*) 
FROM User_CommunityUser_Load_RT
GROUP BY ERROR

select * from User_CommunityUserLoad_RT where error not like '%success%'

---------------------------------------------------------------------------------
-- Refresh into the local database
---------------------------------------------------------------------------------

Exec SF_Refresh 'RT_Superion_FULLSB', 'User_CommunityUser_Load_RT', 'yes'