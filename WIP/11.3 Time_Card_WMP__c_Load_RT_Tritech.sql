/*****************************************************************************************************************************************************
REQ #		: REQ-0847
DEVELOPER	: RTECSON
CREATED DT  : 11/26/2018
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
11/26/2018		Ron Tecson			Initial
01/03/2019		Ron Tecson			Added the counts per query for the E2E Testing.

DECISIONS:

PRE-REQUISITES:

1.) Deactivate the Process Builder - Case - Master and Re-activate once the Migration Process is complete.

******************************************************************************************************************************************************/

USE Superion_FULLSB

	EXEC SF_Refresh 'MC_SUPERION_FULLSB',[Case], Yes

	EXEC sf_replicate 'MC_SUPERION_FULLSB','Case'

USE Tritech_PROD

	EXEC SF_Refresh 'MC_TRITECH_PROD',Time_Card_WMP__c, Yes 

	select * from Tritech_PROD.dbo.Time_Card_WMP__c --9912 12/5

USE Staging_SB

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'Case_Tritech_PreLoad' AND
            table_schema = 'dbo' )
  DROP TABLE [STAGING_SB].[dbo].[Case_Tritech_PreLoad]; 

SELECT
	IIF(tC1.Id IS NOT NULL, tC1.Id, 'Missing Case Id') AS ID
	, CAST(NULL AS NVARCHAR(255)) AS Error
	--, STUFF('[' + sTC1.Id + '] ' XML PATH('')), 1, 1, '')
	, sTC1.ticket_WMP__c AS CaseId_Orig
	, SUM(sTC1.Ticket_Duration_WMP__c) as Work_Effort_In_Minutes__c
INTO [STAGING_SB].[dbo].[Case_Tritech_PreLoad]
FROM Tritech_PROD.dbo.Time_Card_WMP__c sTC1
LEFT JOIN Superion_FULLSB.dbo.[case] tC1 on sTC1.ticket_WMP__c = tC1.legacy_ID__c
GROUP BY tC1.Id, sTC1.ticket_WMP__c
ORDER BY 3  
-- (5047 rows affected) 01/03/2019
-- 5014 Row 12/5

select count(stc1.id)
FROM Tritech_PROD.dbo.Time_Card_WMP__c sTC1
LEFT JOIN Superion_FULLSB.dbo.[case] tC1 on sTC1.ticket_WMP__c = tC1.legacy_ID__c 
-- 9964 01/03/2019
-- 9912 12/4

select * from [STAGING_SB].[dbo].[Case_Tritech_PreLoad] where ID <> 'Missing Case Id' order by 4 desc
--CaseId_Orig -- 462 01/03/2019
--CaseId_Orig -- 836

select CaseId_Orig, count(*) from [STAGING_SB].[dbo].[Case_Tritech_PreLoad]
group by CaseId_Orig having count(*) > 1

select count(*) from [STAGING_SB].[dbo].[Case_Tritech_PreLoad] where ID <> 'Missing Case Id' 
-- 462 01/03/2019
--427

select legacy_id__c, count(*) from Superion_FULLSB.dbo.[Case]
group by legacy_id__c
having count(*) > 1

select * from Superion_FULLSB.dbo.[Case] where legacy_id__c = '5008000000stvqAAAQ'

select * from Tritech_PROD.dbo.[case] where id = '5008000000stvqAAAQ'

select * from Tritech_PROD.dbo.Time_Card_WMP__c where ticket_WMP__c = '5008000000stvqAAAQ'

select 322 + 2880 + 319 + 319 + 66 -- 3906

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'Case_Tritech_Load' AND
            table_schema = 'dbo' )
  DROP TABLE [STAGING_SB].[dbo].[Case_Tritech_Load]; 

SELECT * 
INTO [STAGING_SB].[dbo].[Case_Tritech_Load]
FROM [STAGING_SB].[dbo].[Case_Tritech_PreLoad]
WHERE ID <> 'Missing Case Id'
-- (462 rows affected) -- 01/03/2019 


EXEC SF_ColCompare 'Update','MC_SUPERION_FULLSB', 'Case_Tritech_Load' 

EXEC SF_BulkOps 'Update','MC_SUPERION_FULLSB', 'Case_Tritech_Load'

SELECT ERROR, count(*) Row_Count FROM [STAGING_SB].[dbo].[Case_Tritech_Load]
GROUP BY ERROR 

USE Superion_FULLSB

	EXEC SF_Refresh 'MC_SUPERION_FULLSB',[Case], Yes

/**************** VALIDATION AND ERROR CHECKS *******************************/

BEFORE:

select Work_Effort_In_Minutes__c from MC_SUPERION_FULLSB...[Case] where Id in (
'5000v000003GCy9AAG',
'5000v000003GJUhAAO',
'5000v000003GI2QAAW',
'5000v000003EYmQAAW',
'5000v000003GKTtAAO')

Work_Effort_In_Minutes__c
0
0
0
0
0

-- SELECT STUFF((SELECT '[' + sTC1.Id + '] ' FROM Tritech_PROD.dbo.Time_Card_WMP__c sTC1  FOR XML PATH('')), 1, 1, '');