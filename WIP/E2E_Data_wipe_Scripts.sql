select id,error=cast(space(255) as nvarchar(255)) 
  into Opportunity_Tritech_delete_E2E
  from [Superion_FULLSB].dbo.[Opportunity]
  where Migrated_Record__c='true'
  --and IsClosed='false'

  select * from Opportunity_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Opportunity_Tritech_delete_E2E'

select error,count(*) from Opportunity_Tritech_delete_E2E
  group by error;
  

  select id,error=cast(space(255) as nvarchar(255))
,accountid_sup=accountid 
,ntile(6) over(order by accountid) series
  into Case_Tritech_delete_E2E_series
   from [Superion_FULLSB].dbo.[case]
  where Migrated_Record__c='true'
  and IsClosed='False';--287313

   select * from Case_Tritech_delete_E2E_series;
 --Series1

select * into Case_Tritech_delete_E2E_series_1 from Case_Tritech_delete_E2E_series
where series=1

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Case_Tritech_delete_E2E_series_1'

select * from Case_Tritech_delete_E2E_series_1
where error<>'Operation Successful.'; 

 --Series2

select * into Case_Tritech_delete_E2E_series_2 from Case_Tritech_delete_E2E_series
where series=2

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Case_Tritech_delete_E2E_series_2'

select * from Case_Tritech_delete_E2E_series_2
where error<>'Operation Successful.'; 

--Series3

select * into Case_Tritech_delete_E2E_series_3 from Case_Tritech_delete_E2E_series
where series=3

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Case_Tritech_delete_E2E_series_3'

select * from Case_Tritech_delete_E2E_series_3
where error<>'Operation Successful.'; 


 --Series4

select * into Case_Tritech_delete_E2E_series_4 from Case_Tritech_delete_E2E_series
where series=4

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Case_Tritech_delete_E2E_series_4'

select * from Case_Tritech_delete_E2E_series_4
where error<>'Operation Successful.'; 

 --Series5

select * into Case_Tritech_delete_E2E_series_5 from Case_Tritech_delete_E2E_series
where series=5

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Case_Tritech_delete_E2E_series_5'

select * from Case_Tritech_delete_E2E_series_5
where error<>'Operation Successful.'; 

--Series6

select * into Case_Tritech_delete_E2E_series_6 from Case_Tritech_delete_E2E_series
where series=6

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Case_Tritech_delete_E2E_series_6'

select * from Case_Tritech_delete_E2E_series_6
where error<>'Operation Successful.'; 
----------------
select error,count(*) from Case_Tritech_delete_E2E_series_1
  group by error;
  select error,count(*) from Case_Tritech_delete_E2E_series_2
  group by error;
   select error,count(*) from Case_Tritech_delete_E2E_series_3
  group by error;
select error,count(*) from Case_Tritech_delete_E2E_series_4
  group by error;
  select error,count(*) from Case_Tritech_delete_E2E_series_5
  group by error;
   select error,count(*) from Case_Tritech_delete_E2E_series_6
  group by error;




-----------
--Survey_Feedback__c 

select  id,error=cast(space(255) as nvarchar(255)) 
  into Survey_Feedback__c_Tritech_delete_E2E
   from [Superion_FULLSB].dbo.Survey_Feedback__c
where Migrated_Record__c='true';--2851

select * from Survey_Feedback__c_Tritech_delete_E2E;

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Survey_Feedback__c_Tritech_delete_E2E'

select error,count(*) from Survey_Feedback__c_Tritech_delete_E2E
  group by error;

  --- -----------
--Contact_Reference_Link__c 

select  id,error=cast(space(255) as nvarchar(255)) 
  into Contact_Reference_Link__c_Tritech_delete_E2E
   from [Superion_FULLSB].dbo.Contact_Reference_Link__c
where Migrated_Record__c='true';--150

select * from Contact_Reference_Link__c_Tritech_delete_E2E;

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Contact_Reference_Link__c_Tritech_delete_E2E'
--Contact 
select a.id,error=cast(space(255) as nvarchar(255)) 
  into Contact_Tritech_delete_E2E
   from Superion_FULLSB.dbo.Contact a
  ,Account_Tritech_delete_E2E_errors b
  where a.accountid=b.id
  and b.error <>'Operation Successful.' and b.error <>'entity is deleted';--5245

  select * from Contact_Tritech_delete_E2E;

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Contact_Tritech_delete_E2E'



--Account wiping

select id,error=cast(space(255) as nvarchar(255)) 
  into Account_Tritech_delete_E2E
  from [Superion_FULLSB].dbo.[Account]
  where Migrated_Record__c='true'
;--23655

  select * from Account_Tritech_delete_E2E
  where id='0016A00000brKwDQAU'


--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Account_Tritech_delete_E2E'

select * 
into Account_Tritech_delete_E2E_errors
from Account_Tritech_delete_E2E
where error <>'Operation Successful.';--3480

select * from Account_Tritech_delete_E2E_errors;


--Exec SF_BulkOps 'Delete:batchsize(49)','SL_Superion_FullSB','Account_Tritech_delete_E2E_errors'


select * 
into Account_Tritech_delete_E2E_errors1
from Account_Tritech_delete_E2E_errors
where error <>'Operation Successful.' and error<>'entity is deleted';--737

select * from Account_Tritech_delete_E2E_errors1;


--Exec SF_BulkOps 'Delete:batchsize(10)','SL_Superion_FullSB','Account_Tritech_delete_E2E_errors1'



 select a.id,error=cast(space(255) as nvarchar(255)) 
  into Opportunity_Tritech_delete_E2E_test_data
  --into Superion_test_opportunities
   from [Superion_FULLSB].dbo.Opportunity a
  inner join Account_Tritech_delete_E2E_errors b
  on a.accountid=b.id
  where a.Migrated_Record__c='false';

  select * from Opportunity_Tritech_delete_E2E_test_data;

  --Exec SF_BulkOps 'Delete:batchsize(1)','SL_Superion_FullSB','Opportunity_Tritech_delete_E2E_test_data'




  select a.id,error=cast(space(255) as nvarchar(255)) 
  into Bill_To_Ship_To__c_Tritech_delete_E2E
    from [Superion_FULLSB].dbo.Bill_To_Ship_To__c a
  where Migrated_Record__c='true';--10065

    select * from Bill_To_Ship_To__c_Tritech_delete_E2E;

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Bill_To_Ship_To__c_Tritech_delete_E2E'

--Chatter Group
select a.id,error=cast(space(255) as nvarchar(255)) 
  into CollaborationGroup_Tritech_delete_E2E
  from Superion_FULLSB.dbo.CollaborationGroup a
,CollaborationGroup_load_PB b
where a.id=b.id;--20


 select * from CollaborationGroup_Tritech_delete_E2E;

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','CollaborationGroup_Tritech_delete_E2E'

--Chatter Group Member
select a.id,error=cast(space(255) as nvarchar(255)) 
  into CollaborationGroupMember_Tritech_delete_E2E
  from Superion_FULLSB.dbo.CollaborationGroupMember a
,CollaborationGroupMember_load_PB b
where a.id=b.id;--2


 select * from CollaborationGroupMember_Tritech_delete_E2E;

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','CollaborationGroupMember_Tritech_delete_E2E'



select a.id,error=cast(space(255) as nvarchar(255)) 
  into Contact_Tritech_delete_E2E_Merge_account
  from Superion_FULLSB.dbo.Contact a
where Migrated_Record__c='True';--10095

 select * from Contact_Tritech_delete_E2E_Merge_account;

--Exec SF_BulkOps 'Delete:batchsize(90)','SL_Superion_FullSB','Contact_Tritech_delete_E2E_Merge_account'

select * 
into Contact_Tritech_delete_E2E_Merge_account_errors
from Contact_Tritech_delete_E2E_Merge_account
where error<>'Operation Successful.'

select * from Contact_Tritech_delete_E2E_Merge_account_errors;

--Exec SF_BulkOps 'Delete:batchsize(1)','SL_Superion_FullSB','Contact_Tritech_delete_E2E_Merge_account_errors'


--EngineeringIssue__c

select a.id,error=cast(space(255) as nvarchar(255)) 
  into EngineeringIssue__c_Tritech_delete_E2E
from [Superion_FULLSB].dbo.EngineeringIssue__c a
where migrated_record__C='true';--13360

select * from EngineeringIssue__c_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(99)','SL_Superion_FullSB','EngineeringIssue__c_Tritech_delete_E2E'


--Event

select a.id,error=cast(space(255) as nvarchar(255)) 
  into Event_Tritech_delete_E2E
from [Superion_FULLSB].dbo.Event a
where migrated_record__C='true';--52

select * from Event_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(10)','SL_Superion_FullSB','Event_Tritech_delete_E2E'

--Task

select a.id,error=cast(space(255) as nvarchar(255)) 
  into Task_Tritech_delete_E2E
from [Superion_FULLSB].dbo.task a
where migrated_record__C='true';--907

select * from Task_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_Superion_FullSB','Task_Tritech_delete_E2E'

select error,count(*) from Task_Tritech_delete_E2E
group by error;

--EmailMessage

select a.id,error=cast(space(255) as nvarchar(255)) 
  into EmailMessage_Tritech_delete_E2E
from [Superion_FULLSB].dbo.EmailMessage a
where migrated_record__C='true';--3624

select * from EmailMessage_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_Superion_FullSB','EmailMessage_Tritech_delete_E2E'

select error,count(*) from EmailMessage_Tritech_delete_E2E
group by error;

--Feeditem

select a.id,error=cast(space(255) as nvarchar(255)) 
  into FeedItem_Tritech_delete_E2E
from Superion_FULLSB.dbo.FeedItem a
inner join Staging_SB.dbo.FeedItem_load_PB ld
on ld.ID=a.Id;--1844

select * from FeedItem_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_Superion_FullSB','FeedItem_Tritech_delete_E2E'

select error,count(*) from FeedItem_Tritech_delete_E2E
group by error;

--Environment__c

select a.id,error=cast(space(255) as nvarchar(255)) 
  into Environment__c_Tritech_delete_E2E
from [Superion_FULLSB].dbo.Environment__c a
where migrated_record__C='true';--502

select * from Environment__c_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(99)','SL_Superion_FullSB','Environment__c_Tritech_delete_E2E'



--FeedComment

select a.id,error=cast(space(255) as nvarchar(255)) 
  into FeedComment_Tritech_delete_E2E
from Superion_FULLSB.dbo.FeedComment a
inner join Staging_SB.dbo.FeedComment_load_PB ld
on ld.ID=a.Id;--830

select * from FeedComment_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_Superion_FullSB','FeedComment_Tritech_delete_E2E'

select error,count(*) from FeedComment_Tritech_delete_E2E
group by error;

---PricebookEntry

select id,error=cast(space(255) as nvarchar(255)),legacy_id__C_orig=legacy_id__c
into pricebookEntry_Tritech_delete_E2E
 from Superion_FULLSB.dbo.pricebookEntry
where migrated_record__c='true'
and legacy_id__c like '%:%';--4340


select * from pricebookEntry_Tritech_delete_E2E;


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_Superion_FullSB','pricebookEntry_Tritech_delete_E2E'


select * 
--into pricebookEntry_Tritech_delete_E2E_errors
from pricebookEntry_Tritech_delete_E2E
where error='Operation Successful.';

select * from pricebookEntry_Tritech_delete_E2E_errors;

select distinct pricebook2id from pricebookEntry_Tritech_delete_E2E_errors a
,[Superion_FULLSB].dbo.[pricebookEntry] b
where a.id=b.id

--Exec SF_BulkOps 'Delete:batchsize(1)','SL_Superion_FullSB','pricebookEntry_Tritech_delete_E2E_errors'


 select a.id,LegacySFDCAccountId__c_orig=a.LegacySFDCAccountId__c, LegacySFDCAccountId__c=null,error=cast(space(255) as nvarchar(255))
  --into Account_Update_superion_LegacySFDCAccountId_null
from [Superion_FULLSB].dbo.Account a
,Account_Update_Tritech_Merge b
  where 1=1 --and  Migrated_Record__c='true'
  and a.LegacySFDCAccountId__c=b.LegacySFDCAccountId__c
  and a.id=b.id
  ;--2689
  --2693

  select * from Account_Update_superion_LegacySFDCAccountId_null
 ;



  --Exec SF_BulkOps 'Update:batchsize(50)','SL_Superion_FullSB','Account_Update_superion_LegacySFDCAccountId_null'





