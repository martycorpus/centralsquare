/*
-- Author       : Shivani Mogullapalli
-- Created Date : 22/1/2019
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Opportunity----------
Requirement No  : 
Total Records   :  
Scope: Create Superion opportunites as Procurement Activity records.
 */

 /*
Use Superion_FULLSB
EXEC SF_Replicate 'SL_Superion_FULLSB','Opportunity'
EXEC SF_Refresh 'SL_Superion_FULLSB','Procurement_Activity__C','yes'
*/ 
 --select count(*) from Superion_FULLSB.dbo.Opportunity where Legacy_Source_System__c is null
 --select count(*) from sl_Superion_FULLSB...Opportunity where Legacy_Source_System__c<>'Tritech'
 --19655


Use Staging_SB;

 --Drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload;

 Select  
								 ID = CAST(SPACE(18) as NVARCHAR(18))
								,ERROR = CAST(SPACE(255) as NVARCHAR(255))
								,Migrated_Record__c                      ='True'  
								,RFPStatus__c_orig = RFPStatus__c
								,ROW_NUMBER() OVER(partition by opportunity__c order by iif(isnull(b.isactive,'false')='true',1,2)) as rownum
								,Proposal_Stage__c = iif(RFPStatus__c='Under Review' ,'Bid Review',RFPStatus__c)
								,Procurement_Type__c = RFP_Type__c
								,Proposal_Manager__c_orig = Proposal_Specialist__c
								,Proposal_manager__c = mgrusr.id
								,Request_Received__c = Receipt_Date__c
								,Response_Due_Date__c = Due_Date__c
								,Letter_of_Intent_Completed__c_orig = Letter_of_Intent_Completed__c
								,Letter_of_Intent_Required__c = iif(Letter_of_Intent_Completed__c='yes',1,0)
								,Letter_of_Intent_Due_Date__c = Letter_of_Intent__c
								,Deadline_for_Vendor_Questions__c = Question_Deadline__c
								,Bidder_s_Conference_Date__c = Date_of_Bidder_s_Conference__c
								,Bidder_s_Conference__c_orig = Bidder_s_Conference__c
								,Bidder_s_Conference_Required__c = iif(Bidder_s_Conference__c ='yes',1,0)
								,Bid_Bond_Required__c_orig = Bid_Bond_Required__c
								,Bid_Bond_Required__c = iif(Bid_Bond_Required__c='yes',1,0)
								,Proposal_Manager_Notes__c = RFP_Summary__c
								,Response_Ship_Date__c = Date_Shipped__c
								,Overall_Fit__c = Fit_Analysis__c
								,CAD_Fit__c = replace(CAD__c,'%','')
								,RMS_Fit__c = replace(RMS__c,'%','')
								,JMS_Fit__c = replace(JMS__c,'%','')
								,Mobile_Fit__c = replace(Mobile__c,'%','')
								,If_Consultant_Other__c = Consultant_Company__c
								,Common_Partners__c = Third_Party_Vendors__c
								,Opportunity__c =opp.id
								,Name = opp.name
--into Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload
 from Superion_FULLSB.dbo.Opportunity opp
 left join Superion_FULLSB.dbo.[User] mgrusr 
 on mgrusr.name=opp.Proposal_Specialist__c  
where 1=1
and isnull(opp.Legacy_Source_System__c,'X')<>'Tritech'
and Opp.RFPStatus__c is not null


--(1261 row(s) affected)


select distinct * from Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload

-- drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load
select * 
into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
from Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload where rownum=1

--(1261 row(s) affected)

--Exec SF_ColCompare 'Insert','SL_Superion_FullSB', 'Procurement_Activity__c_Opportunity_Tritech_SFDC_load' 

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load'
/*
Salesforce object Procurement_Activity__c does not contain column RFPStatus__c_orig
Salesforce object Procurement_Activity__c does not contain column Proposal_Manager__c_orig
Salesforce object Procurement_Activity__c does not contain column Due_Date__c
Salesforce object Procurement_Activity__c does not contain column Letter_of_Intent_Completed__c_orig
Salesforce object Procurement_Activity__c does not contain column Bidder_s_Conference__c_orig
Salesforce object Procurement_Activity__c does not contain column Bid_Bond_Required__c_orig
*/


select count(*) 
from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 
where error ='Operation successful.'
-- 434


select * 
into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 
where error<>'Operation Successful.'
--(827 row(s) affected)


select * 
into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors_bkp
from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 
where error<>'Operation Successful.'
--(827 row(s) affected)

select cad_fit__c,rms_fit__c,jms_fit__c,mobile_fit__c
/*update a set a.CAD_Fit__c = replace(a.CAD_Fit__c,'%','')
								,a.RMS_Fit__c = replace(a.RMS_Fit__c,'%','')
								,a.JMS_Fit__c = replace(a.JMS_Fit__c,'%','')
								,a.Mobile_Fit__c = replace(a.Mobile_Fit__c,'%','')*/
								from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors  a
								--(827 row(s) affected)


								
select Proposal_manager_notes__C
--update a set proposal_manager_notes__c =null
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors a where len(proposal_manager_notes__c)>255
 --37

 select if_consultant_other__c
--update a set if_consultant_other__c =null
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors a where len(if_consultant_other__c)>12
 --92


 select name
--update a set name =substring(name,1,80)
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors a where len(name)>80

 --(2 row(s) affected)

 --Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors'


 select error, count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors 
 group by error

 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'
 --(31 row(s) affected)

 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1_bkp
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'
 --(31 row(s) affected)

 
 --Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1'


 select error, count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 
 group by error

 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1
 where error <>'Operation Successful.'
 --(14 row(s) affected)

 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2_bkp
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1
 where error <>'Operation Successful.'
 --(14 row(s) affected)

 select *
 -- update a set a.cad_fit__c=null
  from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2 a where cad_fit__c LIKE '[A-Za-z]%'
  --(4 row(s) affected)
  select * 
  -- update a set a.RMS_fit__c=null
  from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2 a where RMS_fit__c LIKE '[A-Za-z]%'
  --(4 row(s) affected)
   select * 
   -- update a set a.JMS_fit__c=null
   from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2 a where JMS_fit__c LIKE '[A-Za-z]%'
   --(6 row(s) affected)
    select * 
	-- update a set a.Mobile_fit__c=null
	from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2 a where Mobile_fit__c LIKE '[A-Za-z]%'
	--(2 row(s) affected)

 --Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2'

 select * into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors3
  from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2
 where error<>'Operation Successful.'

  select * into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors3_bkp
  from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2
 where error<>'Operation Successful.'


 select *
 --update a set a.Mobile_fit__c=null
	from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors3 a

	--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors3'

 select *
 --delete 
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load where error<>'Operation Successful.'

 --(827 row(s) affected)

 insert into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors where error='Operation Successful.'
 --(796 row(s) affected)

 insert into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 where error='Operation Successful.'
 --(17 row(s) affected)

 insert into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2 where error='Operation Successful.'
 --(12 row(s) affected)

 insert into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors3 where error='Operation Successful.'
 --(2 row(s) affected)

 select count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 


 select id, error ,Migrated_Record__c                      ='True'
 --into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_hotfix
  from Procurement_Activity__c_Opportunity_Tritech_SFDC_load where error='Operation Successful.'

  --Exec SF_ColCompare 'Update','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_hotfix'


  --Exec SF_BulkOps 'Update','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_hotfix'


  select a.id, a.error,Response_due_Date__c= c.due_Date__c, Deadline_for_Vendor_Questions__c=c.Question_Deadline__c
  into  Procurement_Activity__c_Opportunity_Tritech_SFDC_load_hotfix3
   from Procurement_Activity__c_Opportunity_Tritech_SFDC_load a 
   inner join Superion_fullsb.dbo.procurement_Activity__c b 
   on a.id=b.id 
   inner join Superion_FULLSB.dbo.opportunity c 
   on a.Opportunity__c= c.id
   where a.error='Operation Successful.'

   --Exec SF_BulkOps 'Update','SL_Superion_FullSB','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_hotfix3'







