/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Migrate Environment__c - Source System to SFDC
DEVELOPER	: RTECSON, MCORPUS, and PBOWEN
CREATED DT  : 01/31/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
01/31/2019		Ron Tecson			Script for the creation of the Zuercher Environments.
02/26/2019		Ron Tecson			Wipe and Reload
03/04/2019		Ron Tecson			Wipe and Reload
03/12/2019		Ron Tecson			Wipe and Reload. Added a Substring to the Name. The length of the Name target field is 80.
03/15/2019		Ron Tecson			Wipe and Reload
03/26/2019		Ron Tecson			Wipe and Reload. Execution Duration: 6 mins.
									
DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------
USE Superion_FULLSB
EXEC SF_Refresh 'RT_Superion_FullSB', 'Account', 'SUBSET';
EXEC SF_Refresh 'RT_Superion_FullSB', 'Registered_Product__c', 'subset';
EXEC SF_Refresh 'RT_Superion_FullSB', 'Product2', 'SUBSET';
EXEC SF_Refresh 'RT_Superion_FullSB', 'CUSTOMER_ASSET__C', 'SUBSET';

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_For_Zuercher_Products_PreLoad_RT
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_For_Zuercher_Products_PreLoad_RT_1st') 
DROP TABLE Staging_SB.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st;

---------------------------------------------------------------------------------
-- Environment for Zuercher Products
---------------------------------------------------------------------------------

SELECT *
INTO Staging_SB.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st
FROM
(
		SELECT DISTINCT
			'Prod-Zuercher-' + a.Name as Name,
			t1.id as Zuercher_Registered_Product_Id,
			--Concat('Prod', '-', p2.product_line__c, '-', a.name) as Name,
			t1.name as Zuercher_Registered_Product_Name,
			t2.id   AS CustomerAsset__c,
			t2.name AS CustomerAsset_Name,
			t2.product_group__c as CUSTOMER_ASSET_Product_Group,
			p2.product_group__c,
			p2.product_line__c,
			t1.Account__c,
			a.Name as accountname,
			'Prod' AS Environment_Type
		FROM Superion_FULLSB.dbo.REGISTERED_PRODUCT__C t1
			LEFT JOIN Superion_FULLSB.dbo.PRODUCT2 p2 ON p2.id = t1.product__c
			JOIN Superion_FULLSB.dbo.CUSTOMER_ASSET__C t2 ON t2.id = t1.customerasset__c
			LEFT jOIN  Superion_FULLSB.dbo.account a on a.Id = t1.Account__c 
		WHERE t2.product_group__c = 'Zuercher' and t1.name not like '%test%' and t1.name not like '%train%'
		UNION
		SELECT 
			'Test-Zuercher-' + a.Name as Name,
			t1.id as Zuercher_Registered_Product_Id,
			t1.name as Zuercher_Registered_Product_Name,
			t2.id   AS CustomerAsset__c,
			t2.name AS CustomerAsset_Name,
			t2.product_group__c as CUSTOMER_ASSET_Product_Group,
			p2.product_group__c,
			p2.product_line__c,
			t1.Account__c,
			a.Name as accountname,
			'Test' AS Environment_Type
		FROM Superion_FULLSB.dbo.REGISTERED_PRODUCT__C t1
			LEFT JOIN Superion_FULLSB.dbo.PRODUCT2 p2 ON p2.id = t1.product__c
			JOIN Superion_FULLSB.dbo.CUSTOMER_ASSET__C t2 ON t2.id = t1.customerasset__c
			LEFT JOIN Superion_FULLSB.dbo.account a ON a.Id = t1.Account__c 
		WHERE  t2.product_group__c = 'Zuercher' and t1.name like '%test%' 
		UNION		
		SELECT 
			'Train-Zuercher-' + a.Name as Name,
			t1.id as Zuercher_Registered_Product_Id,
			t1.name as Zuercher_Registered_Product_Name,
			t2.id   AS CustomerAsset__c,
			t2.name AS CustomerAsset_Name,
			t2.product_group__c as CUSTOMER_ASSET_Product_Group,
			p2.product_group__c,
			p2.product_line__c,
			t1.Account__c,
			a.Name as accountname,
			'Training' AS Environment_Type
		FROM Superion_FULLSB.dbo.REGISTERED_PRODUCT__C t1
			LEFT JOIN Superion_FULLSB.dbo.PRODUCT2 p2 ON p2.id = t1.product__c
			JOIN Superion_FULLSB.dbo.CUSTOMER_ASSET__C t2 ON t2.id = t1.customerasset__c
			LEFT JOIN Superion_FULLSB.dbo.account a on a.Id = t1.Account__c 
		WHERE t2.product_group__c = 'Zuercher' and t1.name like '%train%'
		)
A

-- 3/12 -- (314 rows affected)
-- 3/15 -- (491 rows affected)
-- 3/26 -- (2575 rows affected)

select distinct name from staging_sb.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_For_Zuercher_Products_PreLoad_RT
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_For_Zuercher_Products_PreLoad_RT_2nd') 
DROP TABLE Staging_SB.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_2nd;

/*Create Environment records for Zuercher Records*/
SELECT DISTINCT
	CAST('' AS NVARCHAR(255)) AS ID,
	CAST('' AS NVARCHAR(255)) AS error,	
	'TriTech' as Legacy_Source_System__c,
	'true' as Migrated_Record__c,
	Account__c,
	AccountName,
	Replace(Name, CHAR(39), '') AS [Name],
	'Yes' AS Active__c,
	IIF(Environment_Type = 'Prod', 'Production', Environment_Type) AS Type__c,
	--'Zuercher' + '-' + Product_Line__c as Description__c,
	Description__c = 'Registered Prods Id: ' + 
			
			STUFF
	(
		( 	-- You only want to combine rows for a single ID here:
			SELECT ', ' + Zuercher_Registered_Product_Id
				
			FROM Staging_SB.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st AS T2 
			-- You only want to combine rows for a single ID here:
			WHERE T2.Account__c = T1.Account__c --and T1.Name = T2.Name
			ORDER BY T2.Name
			FOR XML PATH (''), TYPE
		).value('.', 'varchar(max)')
	, 1, 1, '') ,
	NULL AS Legacy_SystemID__c,
	NULL AS LegacySystemIds,	
	NULL AS Winning_RecordType,
	NULL AS RecordTypes,
	NULL AS connection_type__c,
	NULL AS Notes__c
INTO Staging_SB.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_2nd
FROM Staging_SB.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st T1
GROUP BY Account__c, AccountName, Name, Environment_Type

-- 3/12 -- (104 rows affected)
-- 3/15 -- (105 rows affected)
-- 3/26 -- (529 rows affected)

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_For_Zuercher_Products_PreLoad_RT
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_For_Zuercher_Products_Load_RT') 
DROP TABLE Staging_SB.dbo.Environment__c_For_Zuercher_Products_Load_RT;

SELECT
	ID,
	error,	
	Legacy_Source_System__c,
	Migrated_Record__c,
	Account__c,
	AccountName,
	SUBSTRING([Name], 1, 80) AS [Name],
	IIF(Len([Name]) > 80, [Name], NULL) AS Environment_Name_Orig,
	Active__c,
	Type__c,
	CAST(SUBSTRING(Description__c, 1, 255) AS NVARCHAR(255)) as Description__c,
	NULL AS Legacy_SystemID__c,
	NULL AS LegacySystemIds,	
	NULL AS Winning_RecordType,
	NULL AS RecordTypes,
	NULL AS connection_type__c,
	NULL AS Notes__c
INTO Staging_SB.dbo.Environment__c_For_Zuercher_Products_Load_RT
FROM Environment__c_For_Zuercher_Products_PreLoad_RT_2nd

-- 3/15 -- (105 rows affected)
-- 3/26 -- (529 rows affected)


--------------------------------------------------------------------------------
-- Migrate Data Into SFDC Environment__c
--------------------------------------------------------------------------------
USE Staging_SB

EXEC SF_ColCompare 'INSERT','RT_Superion_FULLSB','Environment__c_For_Zuercher_Products_Load_RT'

/****************************************************************************************
--- Starting SF_ColCompare V3.6.9
Problems found with Environment__c_For_Zuercher_Products_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 176]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment__c does not contain column AccountName
Salesforce object Environment__c does not contain column LegacySystemIds
Salesforce object Environment__c does not contain column Winning_RecordType
Salesforce object Environment__c does not contain column RecordTypes

****************************************************************************************/

EXEC sf_bulkops 'INSERT','RT_Superion_FULLSB','Environment__c_For_Zuercher_Products_Load_RT'

/****************************************************************************************

3/26:
--- Starting SF_BulkOps for Environment__c_For_Zuercher_Products_Load_RT V3.6.9
15:46:33: Run the DBAmp.exe program.
15:46:33: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:46:33: Inserting Environment__c_For_Zuercher_Products_Load_RT (SQL01 / Staging_SB).
15:46:34: DBAmp is using the SQL Native Client.
15:46:34: SOAP Headers: 
15:46:34: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
15:46:34: Warning: Column 'Environment_Name_Orig' ignored because it does not exist in the Environment__c object.
15:46:34: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
15:46:34: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
15:46:34: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
15:46:38: 529 rows read from SQL Table.
15:46:38: 529 rows successfully processed.
15:46:38: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


3/15
--- Starting SF_BulkOps for Environment__c_For_Zuercher_Products_Load_RT V3.6.9
17:32:59: Run the DBAmp.exe program.
17:32:59: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:32:59: Inserting Environment__c_For_Zuercher_Products_Load_RT (SQL01 / Staging_SB).
17:32:59: DBAmp is using the SQL Native Client.
17:33:00: SOAP Headers: 
17:33:00: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
17:33:00: Warning: Column 'Environment_Name_Orig' ignored because it does not exist in the Environment__c object.
17:33:00: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
17:33:00: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
17:33:00: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:33:01: 105 rows read from SQL Table.
17:33:01: 105 rows successfully processed.
17:33:01: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

--- Starting SF_BulkOps for Environment__c_For_Zuercher_Products_Load_RT V3.6.9
17:28:36: Run the DBAmp.exe program.
17:28:36: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:28:36: Inserting Environment__c_For_Zuercher_Products_Load_RT (SQL01 / Staging_SB).
17:28:37: DBAmp is using the SQL Native Client.
17:28:37: SOAP Headers: 
17:28:37: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
17:28:37: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
17:28:37: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
17:28:37: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:28:38: 104 rows read from SQL Table.
17:28:38: 1 rows failed. See Error column of row for more information.
17:28:38: 103 rows successfully processed.
17:28:38: Errors occurred. See Error column of row for more information.
17:28:38: Percent Failed = 1.000.
17:28:38: Error: DBAmp.exe was unsuccessful.
17:28:38: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Environment__c_For_Zuercher_Products_Load_RT "SQL01"  "Staging_SB"  "RT_Superion_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 197]
SF_BulkOps Error: 17:28:36: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC17:28:36: Inserting Environment__c_For_Zuercher_Products_Load_RT (SQL01 / Staging_SB).17:28:37: DBAmp is using the SQL Native Client.17:28:37: SOAP Headers: 17:28:37: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.17:28:37: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.17:28:37: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.17:28:37: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.17:28:38: 104 rows read from SQL Table.17:28:38: 1 rows failed. See Error column of row for more information.17:28:38: 103 rows successfully processed.17:28:38: Errors occurred. See Error column of row for more information.


****************************************************************************************/

--------------------------------------------------------------------------------
-- Validation
--------------------------------------------------------------------------------

SELECT error, count(*) 
FROM Staging_sb.dbo.Environment__c_For_Zuercher_Products_Load_RT 
GROUP BY error

select Len(Name), name from Staging_sb.dbo.Environment__c_For_Zuercher_Products_Load_RT where error <> 'Operation Successful.'

USE Superion_FULLSB

EXEC SF_Refresh 'RT_Superion_FullSB', 'Environment__c', 'subset';


---------------------------------------------------------------------------------------------
-- Environment Name - 3/12. Per Gabe, Truncate.

Prod-Zuercher-Charleston County School District Security and Emergency Management, SC

use Staging_SB

select * INTO Environment__c_Hotfix_Length_RT from Staging_sb.dbo.Environment__c_For_Zuercher_Products_Load_RT where error <> 'Operation Successful.'

update Environment__c_Hotfix_Length_RT
set Name = substring(Name, 1, 80)

select * from Environment__c_Hotfix_Length_RT

EXEC sf_bulkops 'INSERT','RT_Superion_FULLSB','Environment__c_Hotfix_Length_RT'