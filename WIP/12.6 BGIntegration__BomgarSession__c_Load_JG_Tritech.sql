 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__BomgarSession__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'SL_Tritech_PROD','BGIntegration__BomgarSession__c'

Use Superion_FULLSB
EXEC SF_Replicate 'SL_Superion_FULLSB','User'
EXEC SF_Replicate 'SL_Superion_FULLSB','Case'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__Customer__c'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__Representative__c'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__Team__c'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__BomgarSession__c'
*/ 

Use Staging_SB;

--Drop table BGIntegration__BomgarSession__c_Tritech_SFDC_Preload;
 
 Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__Appliance__c                      =  bg.BGIntegration__Appliance__c
,BGIntegration__Chat_Transcript_Download_URL__c   =  bg.BGIntegration__Chat_Transcript_Download_URL__c
,BGIntegration__Chat_Transcript_View_URL__c       =  bg.BGIntegration__Chat_Transcript_View_URL__c
,BGIntegration__Duration__c                       =  bg.BGIntegration__Duration__c
,BGIntegration__End_Time__c                       =  bg.BGIntegration__End_Time__c
,BGIntegration__External_Key__c                   =  bg.BGIntegration__External_Key__c
,BGIntegration__File_Delete_Count__c              =  bg.BGIntegration__File_Delete_Count__c
,BGIntegration__File_Move_Count__c                =  bg.BGIntegration__File_Move_Count__c
,BGIntegration__File_Transfer_Count__c            =  bg.BGIntegration__File_Transfer_Count__c
,BGIntegration__Jumpoint__c                       =  bg.BGIntegration__Jumpoint__c
,BGIntegration__Jumpoint_ID__c                    =  bg.BGIntegration__Jumpoint_ID__c
,BGIntegration__LSEQ__c                           =  bg.BGIntegration__LSEQ__c
,BGIntegration__LSID__c                           =  bg.BGIntegration__LSID__c

,BGIntegration__Primary_Customer__c_orig          =  bg.BGIntegration__Primary_Customer__c
--,BGIntegration__Primary_Customer__c               =  customer.Id

,BGIntegration__Primary_Representative__c_orig    =  bg.BGIntegration__Primary_Representative__c
--,BGIntegration__Primary_Representative__c         =  representative.Id

,BGIntegration__Primary_Team__c_orig              =  bg.BGIntegration__Primary_Team__c
--,BGIntegration__Primary_Team__c                   =  team.Id

,BGIntegration__Public_Site__c                    =  bg.BGIntegration__Public_Site__c
,BGIntegration__Public_Site_ID__c                 =  bg.BGIntegration__Public_Site_ID__c
,BGIntegration__Recording_Download_URL__c         =  bg.BGIntegration__Recording_Download_URL__c
,BGIntegration__Recording_View_URL__c             =  bg.BGIntegration__Recording_View_URL__c
,BGIntegration__Session_Type__c                   =  bg.BGIntegration__Session_Type__c
,BGIntegration__Start_Time__c                     =  bg.BGIntegration__Start_Time__c

,Case__c_orig                                     =  bg.Case__c
,Case__c                                          =  ca.Id

,CreatedById_orig                                 =  bg.CreatedById
,CreatedById                                      =  createdbyuser.Id

,CreatedDate                                      =  bg.CreatedDate
--,Legacy_ID__c                                     =  Id
,Name                                             =  bg.[Name]

,OwnerId_orig                                     =  bg.OwnerId
,OwnerId                                          =  owneruser.Id

into BGIntegration__BomgarSession__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__BomgarSession__c bg

--Fetching CreatedById(UserLookup)
left join Superion_FULLSB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching OwnerId(UserLookup)
left join Superion_FULLSB.dbo.[User] owneruser 
on owneruser.Legacy_Tritech_Id__c=bg.OwnerId

----Fetching PrimaryCustomerId (BGIntegration__Customer__c Lookup)
--left join Superion_FULLSB.dbo.BGIntegration__Customer__c customer
--on customer.Legacy_Id__c=bg.BGIntegration__Primary_Customer__c

----Fetching PrimaryRepresentativeId (BGIntegration__Representative__c Lookup)
--left join Superion_FULLSB.dbo.BGIntegration__Representative__c representative
--on representative.Legacy_Id__c=bg.BGIntegration__Primary_Representative__c

----Fetching PrimaryTeamId (BGIntegration__Team__c Lookup)
--left join Superion_FULLSB.dbo.BGIntegration__Team__c team
--on team.Legacy_Id__c=bg.BGIntegration__Primary_Team__c

--Fetching CaseNumber (To avoid duplicates)
left join Tritech_PROD.dbo.[Case] cn
on  cn.Id=bg.Case__c

--Fetching CaseId (Case Lookup)
left join Superion_FULLSB.dbo.[case] ca
on ca.Legacy_Id__c=bg.Case__c and cn.CaseNumber=ca.legacy_number__c

----For Delta Load
--left join Superion_FULLSB.dbo.BGIntegration__BomgarSession__c trgt
--on trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;(108314 row(s) affected)

----------------------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__BomgarSession__c;--108314

Select count(*) from BGIntegration__BomgarSession__c_Tritech_SFDC_Preload;--108314

Select Legacy_Id__c,count(*) from Staging_SB.dbo.BGIntegration__BomgarSession__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB.dbo.BGIntegration__BomgarSession__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__BomgarSession__c_Tritech_SFDC_Load
from BGIntegration__BomgarSession__c_Tritech_SFDC_Preload; --(108314 row(s) affected)

Select * from Staging_SB.dbo.BGIntegration__BomgarSession__c_Tritech_SFDC_Load 
where OwnerId is NULL or CreatedById is null;

--Exec SF_ColCompare 'Insert','SL_Superion_FullSB','BGIntegration__BomgarSession__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__BomgarSession__c does not contain column BGIntegration__Primary_Customer__c_orig
Salesforce object BGIntegration__BomgarSession__c does not contain column BGIntegration__Primary_Representative__c_orig
Salesforce object BGIntegration__BomgarSession__c does not contain column BGIntegration__Primary_Team__c_orig
Salesforce object BGIntegration__BomgarSession__c does not contain column Case__c_orig
Salesforce object BGIntegration__BomgarSession__c does not contain column CreatedById_orig
Salesforce object BGIntegration__BomgarSession__c does not contain column OwnerId_orig
*/

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','BGIntegration__BomgarSession__c_Tritech_SFDC_Load'


--drop table BGIntegration__BomgarSession__c_Tritech_SFDC_Load_errors
select * 
into BGIntegration__BomgarSession__c_Tritech_SFDC_Load_errors
from BGIntegration__BomgarSession__c_Tritech_SFDC_Load
where error<>'Operation Successful.'
;--

--drop table BGIntegration__BomgarSession__c_Tritech_SFDC_Load_errors_bkp
select * 
into BGIntegration__BomgarSession__c_Tritech_SFDC_Load_errors_bkp
from BGIntegration__BomgarSession__c_Tritech_SFDC_Load
where error<>'Operation Successful.'
;--

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','BGIntegration__BomgarSession__c_Tritech_SFDC_Load_errors'

select * 
--delete
from BGIntegration__BomgarSession__c_Tritech_SFDC_Load
where error<>'Operation Successful.';--

--insert into BGIntegration__BomgarSession__c_Tritech_SFDC_Load
select * from BGIntegration__BomgarSession__c_Tritech_SFDC_Load_errors;--

----------------------------------------------------------------------------------------

--Updating BGIntegration__Customer__c,BGIntegration__Representative__c,BGIntegration__Team__c (Lookup fields)

--Use Staging_SB;

--Drop table BGIntegration__BomgarSession__c_Tritech_SFDC_LoadUpdate

Select 

 Id =bg.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Legacy_id__c
,BGIntegration__Primary_Customer__c_prev=bg.BGIntegration__Primary_Customer__c_orig
,BGIntegration__Primary_Customer__c=customer.Id
,BGIntegration__Primary_Representative__c_prev=BGIntegration__Primary_Representative__c_orig
,BGIntegration__Primary_Representative__c=representative.Id
,BGIntegration__Primary_Team__c_prev=BGIntegration__Primary_Team__c_orig
,BGIntegration__Primary_Team__c=team.Id

into BGIntegration__BomgarSession__c_Tritech_SFDC_LoadUpdate
from Staging_SB.dbo.BGIntegration__BomgarSession__c_Tritech_SFDC_Load bg

--Fetching PrimaryCustomerId (BGIntegration__Customer__c Lookup)
left join Superion_FULLSB.dbo.BGIntegration__Customer__c customer
on customer.Legacy_Id__c=bg.BGIntegration__Primary_Customer__c_orig

--Fetching PrimaryRepresentativeId (BGIntegration__Representative__c Lookup)
left join Superion_FULLSB.dbo.BGIntegration__Representative__c representative
on representative.Legacy_Id__c=bg.BGIntegration__Primary_Representative__c_orig

--Fetching PrimaryTeamId (BGIntegration__Team__c Lookup)
left join Superion_FULLSB.dbo.BGIntegration__Team__c team
on team.Legacy_Id__c=bg.BGIntegration__Primary_Team__c_orig;--(108314 row(s) affected)

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB','BGIntegration__BomgarSession__c_Tritech_SFDC_LoadUpdate' 

/*
Salesforce object BGIntegration__BomgarSession__c does not contain column BGIntegration__Primary_Customer__c_prev
Salesforce object BGIntegration__BomgarSession__c does not contain column BGIntegration__Primary_Representative__c_prev
Salesforce object BGIntegration__BomgarSession__c does not contain column BGIntegration__Primary_Team__c_prev
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','BGIntegration__BomgarSession__c_Tritech_SFDC_LoadUpdate'

---------------------------------------------------------------------

Select *
--delete 
from BGIntegration__BomgarSession__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --

--insert into BGIntegration__BomgarSession__c_Tritech_SFDC_Load
select * from BGIntegration__BomgarSession__c_Tritech_SFDC_Load_Delta; --479 

---------------------------------------------------------------------------------

--Deleting 200 BGIntegration__BomgarSession__c records
--Backup Table (BGIntegration__BomgarSession__c_Delete200_bkp)

----Exec SF_BulkOps 'Delete','SL_Superion_FullSB','BGIntegration__BomgarSession__c_Delete200'

