/*
-- Author     : Marty Corpus	
-- Date       : 11/6/2018
-- Description: Migrate Requirement__c data from the Tritech Prod org to the Superion org (References, API: Contact_Reference_Link__c)
Requirement Number:REQ-0846
Scope:
Migrate all records that relate to a migrated opportunity.

2018-11-06 Missing 2 custome fields - config needed.
           1. [Reference_Document_Event__c]
		   2. [If_Other_Please_List_Document_Event__c]
2018-11-10 Scripted Missing fields.
           Opp load table pending - waiting for Patrick to load for UAT1
2018-11-12 Executed for UAT1 Load.
2018-12-04 Executed for UAT2 Load.
2018-12-18 Fix join to get account id.
2019-01-02 MartyC. Executed for E2E load.
2019-03-28 Moved to Production.

*/


USE [Tritech_PROD]

EXEC Sf_refresh 'MC_TRITECH_PROD','References__c','Yes';
--- Starting SF_Refresh for References__c V3.6.7
--04:00:26: Using Schema Error Action of yes
--04:00:26: Using last run time of 2019-01-03 03:28:00
--04:00:26: Identified 0 updated/inserted rows.
--04:00:26: Identified 0 deleted rows.
--- Ending SF_Refresh. Operation successful.


EXEC SF_Refresh 'MC_TRITECH_PROD','user','yes'

use Superion_FULLSB;

EXEC SF_Refresh 'MC_SUPERION_FULLSB','Opportunity','yes'
--- Starting SF_Refresh for Opportunity V3.6.7
--03:59:25: Using Schema Error Action of yes
--03:59:26: Using last run time of 2019-01-02 18:18:00
--03:59:29: Identified 5 updated/inserted rows.
--03:59:30: Identified 0 deleted rows.
--03:59:30: Adding updated/inserted rows into Opportunity
--- Ending SF_Refresh. Operation successful.

EXEC SF_Refresh 'MC_SUPERION_FULLSB','User','yes'

--- Starting SF_Refresh for User V3.6.7
--03:59:50: Using Schema Error Action of yes
--03:59:52: Using last run time of 2019-01-03 00:15:00
--03:59:52: Identified 0 updated/inserted rows.
--03:59:52: Identified 0 deleted rows.
--- Ending SF_Refresh. Operation successful.




USE [Staging_SB]



go

-- drop table Contact_Reference_Link__c_Load_MC_Tritech

-- User: 0056A000000lfqkQAA	Superion API Fullsb

SELECT Cast( NULL AS NCHAR(18))         AS Id,
       Cast( NULL AS NVARCHAR(255))     AS Error,
       rf.[id]                          AS Legacy_Id__c,
       'true'                           AS Migrated_Record__c,
       'Tritech'                        AS Legacy_Source_System__c,
       coalesce(u.id,'0056A000000lfqkQAA')                            AS Ownerid,
	   tu.name + ' | ' + tu.Isactive       as CreatedbyIdName_orig,
	   rf.Account__c                    AS Account_orig ,
       coalesce(a.id,'ID NOT FOUND')    AS Account__c, --12/18
       coalesce(opp.id,'ID NOT FOUND')  AS Opportunity__c, --11/9 waiting on Partick's opp load.
       rf.[reference_document_event__c] AS [Reference_Document_Event__c], --11/9
       rf.[if_other_please_list_document_event__c] as [If_Other_Please_List_Document_Event__c], --11/9
       rf.[createdbyid]                 [CreatedById_orig],
       rf.[createddate]                 AS [CreatedDate_orig],
       rf.[isdeleted]                   as [isdeleted_orig]  ,
       rf.[lastactivitydate]            AS [LastActivityDate_orig],
       rf.[lastmodifiedbyid]            AS [LastModifiedById_orig],
       rf.[lastmodifieddate]            AS [LastModifiedDate_orig],
       rf.[name]                        AS [Name_orig],
       rf.[opportunity__c]              AS [Opportunity_orig],
       rf.[systemmodstamp]              AS [SystemModstamp_orig]
INTO   Contact_Reference_Link__c_Load_MC_Tritech
FROM   [Tritech_PROD].[dbo].[REFERENCES__C] rf
       LEFT JOIN [Tritech_PROD].[dbo].[user] tu on tu.id = rf.CreatedById
       LEFT JOIN Superion_FULLSB.dbo.[Opportunity] opp
              ON opp.legacy_opportunity_id__c = rf.opportunity__c --11/9 waiting on Partick's opp load.
       LEFT JOIN Superion_FULLSB.dbo.[USER] u
              ON u.[Legacy_Tritech_Id__c] = rf.createdbyid
       LEFT JOIN [MC_SUPERION_FULLSB]...[Account] a --12/18
              ON a.legacysfdcaccountid__c = rf.account__c; --(150 row(s) affected) 11/12
			  --(162 row(s) affected)  1/2 E2E MC
  


select  * from Contact_Reference_Link__c_Load_MC_Tritech



--checks:

--check for dups: select Legacy_Id__c, count(*) from Contact_Reference_Link__c_Load_MC_Tritech group by Legacy_Id__c having count(*) > 1 --1/3 E2E MC 0 records
--checK for account ids not found: select * from Contact_Reference_Link__c_Load_MC_Tritech where account__c is null or account__c = 'ID NOT FOUND'  --1/3 E2E MC MC 0 records
--check for   oppty ids not found: select * from Contact_Reference_Link__c_Load_MC_Tritech where Opportunity__c is null or account__c = 'ID NOT FOUND' --1/3 E2E MC 0 records
--check count of legacy source table: select count(*) from [Tritech_PROD].[dbo].[REFERENCES__C] --162 1/3 E2E MC
--check piclist values: select distinct Reference_Document_Event__c from Contact_Reference_Link__c_Load_MC_Tritech;
/*Demo
Other
Proactive Proposal
Reference Letter
RFX Response
*/

-- exec sf_colcompare 'Insert','MC_SUPERION_FULLSB','Contact_Reference_Link__c_Load_MC_Tritech'
/*
--- Starting SF_ColCompare V3.6.9
Problems found with Contact_Reference_Link__c_Load_MC_Tritech. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 104]
--- Ending SF_ColCompare. Operation FAILED.
Salesforce object Contact_Reference_Link__c does not contain column CreatedbyIdName_orig
Salesforce object Contact_Reference_Link__c does not contain column Account_orig
Salesforce object Contact_Reference_Link__c does not contain column CreatedById_orig
Salesforce object Contact_Reference_Link__c does not contain column CreatedDate_orig
Salesforce object Contact_Reference_Link__c does not contain column isdeleted_orig
Salesforce object Contact_Reference_Link__c does not contain column LastActivityDate_orig
Salesforce object Contact_Reference_Link__c does not contain column LastModifiedById_orig
Salesforce object Contact_Reference_Link__c does not contain column LastModifiedDate_orig
Salesforce object Contact_Reference_Link__c does not contain column Name_orig
Salesforce object Contact_Reference_Link__c does not contain column Opportunity_orig
Salesforce object Contact_Reference_Link__c does not contain column SystemModstamp_orig

*/

-- alter table Contact_Reference_Link__c_Load_MC_Tritech add ident int identity(1,1);
select * from Contact_Reference_Link__c_Load_MC_Tritech;



 -- exec SF_BulkOps 'Insert','MC_SUPERION_FULLSB','Contact_Reference_Link__c_Load_MC_Tritech';

 /*
--- Starting SF_BulkOps for Contact_Reference_Link__c_Load_MC_Tritech V3.6.9
--- Starting SF_BulkOps for Contact_Reference_Link__c_Load_MC_Tritech V3.6.9
04:11:44: Run the DBAmp.exe program.
04:11:44: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
04:11:44: Inserting Contact_Reference_Link__c_Load_MC_Tritech (SQL01 / Staging_SB).
04:11:45: DBAmp is using the SQL Native Client.
04:11:45: SOAP Headers: 
04:11:45: Warning: Column 'CreatedbyIdName_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'Account_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'CreatedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'CreatedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'isdeleted_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'LastActivityDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'LastModifiedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'LastModifiedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'Name_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'Opportunity_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'SystemModstamp_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:45: Warning: Column 'ident' ignored because it does not exist in the Contact_Reference_Link__c object.
04:11:46: 162 rows read from SQL Table.
04:11:46: 162 rows successfully processed.
04:11:46: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


*/