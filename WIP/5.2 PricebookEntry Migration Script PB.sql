-------------------------------------------------------------------------------
--- Pricebook Entry Migration Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 12 December 2018
--- Last Updated: 12 December 2018 - PAB
--- Change Log: 
---
--- Prerequisites:
---	
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Tritech_PROD
Exec SF_Refresh 'PB_Tritech_Prod', 'Product2', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Pricebook2', 'yes'

USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Product2', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Pricebook2', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'PricebookEntry', 'yes'


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='PricebookEntry_StandardPB_Load_PB') 
	DROP TABLE Staging_SB.dbo.PricebookEntry_StandardPB_Load_PB;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		'true' migrated_record__c,
		'TriTech' as Legacy_Source_System__c,
		Concat(a.ID, ':', pb.id) as Legacy_ID__c,
		PB.ID as Pricebook2ID,
		a.ID as Product2ID,
		--'0.00' as Unit_Cost__c,
		'0.00' as UnitPrice,
		'true' as isActive,
		'false' as UseStandardPrice
		INTO PricebookEntry_StandardPB_Load_PB
		FROM Superion_FULLSB.dbo.Product2 a
		LEFT OUTER JOIN Superion_FullSB.dbo.Pricebook2 PB on PB.[Name] = 'Standard Price Book'
		WHERE Acronym_List__c = 'legacy ttz'
		order by a.[Name]



---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'upsert', 'PB_Superion_FULLSB', 'PricebookEntry_StandardPB_Load_PB', 'Legacy_ID__c'


--select * from PricebookEntry_StandardPB_Load_PB where error not like '%success%' 


---------------------------------------------------------------------------------
-- Drop Legacy staging tables 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='PricebookEntry_LegacyPB_Load_PB') 
	DROP TABLE Staging_SB.dbo.PricebookEntry_LegacyPB_Load_PB;



---------------------------------------------------------------------------------
-- Load Legacy Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		'true' migrated_record__c,
		'TriTech' as Legacy_Source_System__c,
		Concat(a.ID, ':', pb.id) as Legacy_ID__c,
		PB.ID as Pricebook2ID,
		a.ID as Product2ID,
		--'0.00' as Unit_Cost__c,
		'0.00' as UnitPrice,
		'true' as isActive,
		'false' as UseStandardPrice
		INTO PricebookEntry_LegacyPB_Load_PB
		FROM Superion_FULLSB.dbo.Product2 a
		LEFT OUTER JOIN Superion_FullSB.dbo.Pricebook2 PB on PB.[Name] = 'Legacy PriceBook'
		WHERE Acronym_List__c = 'legacy ttz'
		order by a.[Name]



---------------------------------------------------------------------------------
-- Insert/Update records for Legacy Pricebook
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'upsert', 'PB_Superion_FULLSB', 'PricebookEntry_LegacyPB_Load_PB', 'Legacy_ID__c'

-- select * from PricebookEntry_LegacyPB_Load_PB where error not like '%success%'