-------------------------------------------------------------------------------
--- Survey Feedback Object Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: December 6, 2018
--- Last Updated: December 6, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 1. 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Survey_Feedback__c_Load_PB') 
DROP TABLE Survey_Feedback__c_Load_PB;


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'Account', 'yes' ;
Exec SF_Refresh 'PB_Tritech_Prod', 'Contact', 'yes' ;

Use Superion_FUllSB
Exec SF_Refresh 'PB_Superion_FullSB', 'Contact', 'yes'
Exec SF_Refresh 'PB_Superion_FullSB', 'RecordType', 'yes' ;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
Use Staging_SB;


Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Id__c,
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		RT.ID as RecordTypeID,
		a.NPS_Score__c as Legacy_NPS_Score__c,
		a.NPS_Survey_Contact__c as NPS_Survey_Contact_Original,
		isNull(Cont.ID, '') as Contact2__c,
		isNull(a.Survey_Notes__c, '') as Customer_Comments__c,
		a.ID as Account_Original__c,
		Acct.ID as Account__c,
		a.Survey_Date__c as CreatedDate
		INTO Survey_Feedback__c_Load_PB
		FROM Tritech_PROD.dbo.Account a
		-- Record Type
		LEFT OUTER JOIN Superion_FULLSB.dbo.RecordType RT on 'NPS' = RT.[Name]
		-- Contact
		LEFT OUTER JOIN Superion_FULLSB.dbo.Contact Cont ON a.NPS_Survey_Contact__c = Cont.Legacy_ID__c
		-- Account
		LEFT OUTER JOIN Superion_FULLSB.dbo.Account Acct ON a.ID = Acct.LegacySFDCAccountId__c
		Where a.NPS_Score__c is not null

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table  Staging_SB.dbo.Survey_Feedback__c_Load_PB
Add [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
Use Staging_SB
Exec SF_BulkOps 'upsert:bulkapi,batchsize(5000)', 'PB_Superion_FullSB', 'Survey_Feedback__c_Load_PB', 'Legacy_Id__c';


--select * from Account_Load_PB where error not like '%success%'
