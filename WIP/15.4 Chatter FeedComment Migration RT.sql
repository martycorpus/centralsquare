/*****************************************************************************************************************************************************
REQ #		: REQ-0826
TITLE		: Chatter FeedComment Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.4 Chatter FeedComment Migration PB.sql
02/15/2019		Ron Tecson			Added Refreshes, Added the PreLoad before Load Table.

DECISIONS:

******************************************************************************************************************************************************/

--------------------------------------------------------------------------------
-- Refresh Data 
--------------------------------------------------------------------------------
USE Tritech_Prod

EXEC SF_Refresh 'MC_Tritech_Prod', 'FeedComment', 'yes';
EXEC SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';

USE Superion_FULLSB
EXEC SF_Refresh 'RT_Superion_FULLSB', 'User', 'yes';

---------------------------------------------------------------------------------
-- Drop Preload table
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedComment_PreLoad_RT') 
	DROP TABLE Staging_SB.dbo.FeedComment_PreLoad_RT;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB

SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.CommentBody as CommentBody,
	a.CommentType as CommentType,
	a.CreatedByID as CreatedBy_Orig,
	isNull(CreatedBy.ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as CreatedById,
	a.CreatedDate as CreatedDate,
	FeedItem.ID as FeedItemID,
	a.IsRichText as IsRichText,
	a.isVerified as IsVerified,
	a.[Status] as [Status],
	a.ID as Legacy_ID__c
INTO Staging_SB.dbo.FeedComment_PreLoad_RT
FROM TriTech_Prod.dbo.FeedComment a
LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c
LEFT OUTER JOIN Staging_SB.dbo.FeedItem_load_PB FeedItem ON a.FeedItemID = FeedItem.Legacy_Id__c						----> FeedItem
WHERE FeedItem.ID IS NOT NULL

---------------------------------------------------------------------------------
-- Drop Load table
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedComment_Load_RT') 
	DROP TABLE Staging_SB.dbo.FeedComment_Load_RT;

SELECT *
INTO Staging_SB.dbo.FeedComment_Load_RT
FROM Staging_SB.dbo.FeedComment_PreLoad_RT

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE Staging_SB.dbo.FeedComment_Load_RT
ADD [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
EXEC SF_BulkOps 'insert:bulkapi,batchsize(1000)', 'RT_Superion_FULLSB', 'FeedComment_Load_RT'

-------------------------------
-- Validation
-------------------------------
SELECT ERROR, count(*)
FROM Staging_SB.dbo.FeedComment_Load_RT
GROUP BY ERROR

SELECT * FROM Staging_SB.dbo.FeedComment_Load_RT
WHERE ERROR not like '%success%'