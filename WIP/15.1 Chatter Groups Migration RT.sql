/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: ChatterGroup (CollaborationGroup) - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.1 Chatter Groups Migration PB.sql

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------
Use Tritech_Prod

Exec SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroup_PreLoad_RT') 
DROP TABLE Staging_SB.dbo.CollaborationGroup_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB;

Select
	CAST('' as nchar(18)) as [ID],
	CAST('' as nvarchar(255)) as Error,
	a.Name as Name,
	a.CollaborationType as CollaborationType,
	a.[Description] as [Description],
	a.InformationTitle as InformationTitle,
	a.InformationBody as InformationBody,
	a.CanHaveGuests as CanHaveGuests,
	a.IsArchived as IsArchived,
	a.IsAutoArchiveDisabled as IsAutoArchiveDisabled,
	a.IsBroadcast as IsBroadCast
INTO Staging_SB.dbo.CollaborationGroup_PreLoad_RT
FROM TriTech_Prod.dbo.CollaborationGroup a
WHERE a.IsArchived = 'false'
	AND a.MemberCount > 0
	AND a.LastFeedModifiedDate >= CAST('09/30/2018' as Datetime)

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
USE Staging_SB;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroup_Load_RT') 
DROP TABLE Staging_SB.dbo.CollaborationGroup_Load_RT;

SELECT *
INTO Staging_SB.dbo.CollaborationGroup_Load_RT
FROM Staging_SB.dbo.CollaborationGroup_PreLoad_RT

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE Staging_SB.dbo.CollaborationGroup_load_RT
ADD [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Staging_SB
Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'RT_Superion_FULLSB', 'CollaborationGroup_load_RT'


---------------------------------------------------------------------------------
-- Refresh Data to the local database
---------------------------------------------------------------------------------
USE Superion_FULLSB;

EXEC SF_Refresh 'RT_Superion_FULLSB', 'CollaborationGroup', 'yes';