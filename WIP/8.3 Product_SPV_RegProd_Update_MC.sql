-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 8.3 Product_SPV_RegProd_Update_MC.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--- Copyright Apps Associates 2018
--- Created Date: Jan 24, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-01-31: Added addtional fields.
--- Added matching on Product Name.
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------
/*
Notes:
This script will update the migrated TT cases to products using these matching logic.

1.	Look for a match based on Product Code (current logic)
2.	Look for match based on Product Name
3.	Populate Product in Target based on mapping from Shane (See CASE_PRODUCT_MAPPING_TRITECH table).

*/

-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-02-01 Executed in Superion FullSbx.
-- ((287280 row(s) affected)row(s) affected)

-- EXEC SF_ColCompare 'Update','MC_SUPERION_FULLSB','Case_Update_Product_MC'
/*
--- Starting SF_ColCompare V3.6.9
Problems found with Case_Update_Product_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 29]
--- Ending SF_ColCompare. Operation FAILED.

ErrorDesc
Salesforce object Case does not contain column Product_Name
Salesforce object Case does not contain column Match_Type
Salesforce object Case does not contain column Case_ProductID_Pre_Update
Salesforce object Case does not contain column Case_ProductName_Pre_Update
Salesforce object Case does not contain column Case_ProductAcronymList_Pre_Update
Salesforce object Case does not contain column Sup_Case_ID
Salesforce object Case does not contain column TT_TicketNumber
Salesforce object Case does not contain column TT_Case_productId
Salesforce object Case does not contain column TT_ProductCode
Salesforce object Case does not contain column TT_ProductName
*/

-- EXEC SF_Bulkops 'Update:bulkapi,batchsize(10000)','MC_SUPERION_FULLSB','Case_Update_Product_MC'

/*
--- Starting SF_BulkOps for Case_Update_Product_MC V3.6.9
20:28:01: Run the DBAmp.exe program.
20:28:01: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
20:28:01: Updating Salesforce using Case_Update_Product_MC (SQL01 / Staging_SB) .
20:28:02: DBAmp is using the SQL Native Client.
20:28:02: Batch size reset to 10000 rows per batch.
20:28:03: Warning: BulkAPI operations without a Sort column could perform slowly.
20:28:03: Batch size reset to 10000 rows per batch.
20:28:03: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
20:28:04: Warning: Column 'Product_Name' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'Match_Type' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'Case_ProductID_Pre_Update' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'Case_ProductName_Pre_Update' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'Case_ProductAcronymList_Pre_Update' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'Sup_Case_ID' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'TT_TicketNumber' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'TT_Case_productId' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'TT_ProductCode' ignored because it does not exist in the Case object.
20:28:04: Warning: Column 'TT_ProductName' ignored because it does not exist in the Case object.
20:28:19: Batch 7510v0000040kqiAAA created with 10000 rows.
20:28:32: Batch 7510v0000040kqnAAA created with 10000 rows.
20:28:45: Batch 7510v0000040kqsAAA created with 10000 rows.
20:28:58: Batch 7510v0000040kflAAA created with 10000 rows.
20:29:11: Batch 7510v0000040kqxAAA created with 10000 rows.
20:29:23: Batch 7510v0000040kjsAAA created with 10000 rows.
20:29:36: Batch 7510v0000040kr7AAA created with 10000 rows.
20:29:49: Batch 7510v0000040krCAAQ created with 10000 rows.
20:30:02: Batch 7510v0000040krHAAQ created with 10000 rows.
20:30:15: Batch 7510v0000040krMAAQ created with 10000 rows.
20:30:28: Batch 7510v0000040krRAAQ created with 10000 rows.
20:30:41: Batch 7510v0000040krWAAQ created with 10000 rows.
20:30:54: Batch 7510v0000040kqKAAQ created with 10000 rows.
20:31:07: Batch 7510v0000040krqAAA created with 10000 rows.
20:31:20: Batch 7510v0000040ks5AAA created with 10000 rows.
20:31:33: Batch 7510v0000040kstAAA created with 10000 rows.
20:31:46: Batch 7510v0000040ksqAAA created with 10000 rows.
20:31:59: Batch 7510v0000040ktSAAQ created with 10000 rows.
20:32:12: Batch 7510v0000040kpWAAQ created with 10000 rows.
20:32:25: Batch 7510v0000040ku7AAA created with 10000 rows.
20:32:38: Batch 7510v0000040kukAAA created with 10000 rows.
20:32:51: Batch 7510v0000040kuzAAA created with 10000 rows.
20:33:04: Batch 7510v0000040kvOAAQ created with 10000 rows.
20:33:17: Batch 7510v0000040kwHAAQ created with 10000 rows.
20:33:31: Batch 7510v0000040kwvAAA created with 10000 rows.
20:33:45: Batch 7510v0000040ksrAAA created with 10000 rows.
20:33:58: Batch 7510v0000040kwnAAA created with 10000 rows.
20:34:12: Batch 7510v0000040kvKAAQ created with 10000 rows.
20:34:21: Batch 7510v0000040kzCAAQ created with 7280 rows.
20:34:22: 287280 rows read from SQL Table.
20:34:22: 287280 rows processed.
21:35:22: Error: Polling timeout reached. Unable to retrieve results from salesforce.
21:35:22: Check the Salesforce Application Setup / Monitoring / Bulk Data Load Jobs for final job disposition.
21:35:23: Percent Failed = 100.000.
21:35:23: Error: DBAmp.exe was unsuccessful.
21:35:23: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe update:bulkapi,batchsize(10000) Case_Update_Product_MC "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 49]
SF_BulkOps Error: 20:28:01: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC20:28:01: Updating Salesforce using Case_Update_Product_MC (SQL01 / Staging_SB) .20:28:02: DBAmp is using the SQL Native Client.20:28:02: Batch size reset to 10000 rows per batch.20:28:03: Warning: BulkAPI operations without a Sort column could perform slowly.20:28:03: Batch size reset to 10000 rows per batch.20:28:03: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.20:28:04: Warning: Column 'Product_Name' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'Match_Type' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'Case_ProductID_Pre_Update' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'Case_ProductName_Pre_Update' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'Case_ProductAcronymList_Pre_Update' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'Sup_Case_ID' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'TT_TicketNumber' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'TT_Case_productId' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'TT_ProductCode' ignored because it does not exist in the Case object.20:28:04: Warning: Column 'TT_ProductName' ignored because it does not exist in the Case object.20:28:19: Batch 7510v0000040kqiAAA created with 10000 rows.20:28:32: Batch 7510v0000040kqnAAA created with 10000 rows.20:28:45: Batch 7510v0000040kqsAAA created with 10000 rows.20:28:58: Batch 7510v0000040kflAAA created with 10000 rows.20:29:11: Batch 7510v0000040kqxAAA created with 10000 rows.20:29:23: Batch 7510v0000040kjsAAA created with 10000 rows.20:29:36: Batch 7510v0000040kr7AAA created with 10000 rows.20:29:49: Batch 7510v0000040krCAAQ created with 10000 rows.20:30:02: Batch 7510v0000040krHAAQ created with 10000 rows.20:30:15: Batch 7510v0000040krMAAQ...


--EXEC SF_Bulkops 'Status','MC_SUPERION_FULLSB','Case_Update_Product_MC'
00:13:39: Retrieving Job Status.
00:18:32: 287280 rows read from SQL Table.
00:18:32: 274995 rows successfully processed.
00:18:32: 12285 rows failed.
00:18:32: 274995 rows marked with current status.
00:18:32: Errors occurred. See Error column of row and above messages for more information.
00:18:34: Percent Failed = 4.300.


select error, count(*)
from Case_Update_Product_MC
group by error

Environment: id value of incorrect type: ID NOT FOUND	1
Operation Successful.	5210

select * from Case_Update_Product_MC
where error not like  '%Operation Successful.'
*/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_PROD;

Exec SF_Refresh 'MC_TRITECH_PROD','Hardware_Software__c','Yes'

Use Superion_FULLSB;

exec sf_refresh 'MC_SUPERION_FULLSB','CASE','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','CUSTOMER_ASSET__C','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Registered_Product__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Environment__c','Yes'

exec sf_refresh 'MC_SUPERION_FULLSB','System_product_Version__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Version__c','Yes'

exec sf_refresh 'MC_SUPERION_FULLSB','Product2','Yes'
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_SB;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' 
      AND TABLE_NAME='Case_Update_Product_MC') 
DROP TABLE Staging_SB.dbo.Case_Update_Product_MC;

Use Staging_sb --exec time: <1 min.
;WITH cte_legacy_product_code AS
(
SELECT t.id,
       s.value                  AS legacy_product_code__c,
       t.legacy_product_code__c AS legacy_product_code_string,
	   t.Name
FROM   superion_fullsb.dbo.[PRODUCT2] t
       CROSS apply String_split( t.legacy_product_code__c, ',' ) s
WHERE  t.legacy_product_code__c IS NOT NULL 
),
cte_prod_name as
(
SELECT Row_number( )
         OVER (
           partition BY t2.NAME
           ORDER BY (CASE WHEN t2.acronym_list__c = 'Legacy TTZ' THEN 10 ELSE 1 END) ) AS "RANK",
       t2.id as Product2Id_Sup,
       t2.NAME,
       t3.id Product2Id_TT,
       t2.acronym_list__c
FROM   superion_fullsb.dbo.[PRODUCT2] t2
       JOIN tritech_prod.dbo.[PRODUCT2] t3
         ON t3.NAME = t2.NAME 
),
cte_registered_product as
(SELECT p2.product_group__c,
       p2.product_line__c,
       p2.id as Registered_product_ProductId,
       p2.name,
       p2.productcode,
       rp.name as Registered_Product_Name,
       rp.Account__c
FROM   superion_fullsb.dbo.REGISTERED_PRODUCT__C rp
       join superion_fullsb.dbo.PRODUCT2 p2
         ON p2.id = rp.product__c 
),
cte_case_prod_mapping as
(
SELECT p2.id                    AS TT_ProductId,
       sp2.id                   AS Sup_ProductId,
       sp2.[name]               AS Sup_prod2_name,
       cpmt.[tt/z product name] AS [Mapping TT/Z Product Name],
       cpmt.[target product]    AS [Mapping Target Product],
       cpmt.[product code]      AS [Mapping Product Code]
FROM   dbo.CASE_PRODUCT_MAPPING_TRITECH cpmt
       LEFT JOIN tritech_prod.dbo.PRODUCT2 p2
              ON p2.NAME = cpmt.[tt/z product name]
       LEFT JOIN superion_fullsb.dbo.PRODUCT2 sp2
              ON sp2.productcode = cpmt.[product code]
WHERE  cpmt.[product code] IS NOT NULL
)
SELECT t1.Id               AS Id,
       cast(null as nvarchar(255)) AS Error,
	   Coalesce(COALEsCE(t4.id,COALESCE(cte_pn.Product2Id_Sup,cptm.Sup_ProductId)),'ID NOT FOUND') AS ProductId,
	   Coalesce(COALEsCE(t4.Name,COALESCE(cte_pn.Name,cptm.Sup_prod2_name)),'PRODUCT NOT FOUND') AS Product_Name,
	   CASE WHEN t4.id IS NOT NULL THEN 'Legacy Product Code'
	        WHEN cte_pn.Product2Id_Sup IS NOT NULL THEN 'Product Name'
			WHEN cptm.Sup_ProductId IS NOT NULL THEN 'Mapping File from Shane'
		    ELSE 'MAPPING NOT FOUND'
       END                 AS Match_Type,
	   t1.ProductId        AS Case_ProductID_Pre_Update,
	   t3.[Name]           AS Case_ProductName_Pre_Update,
	   t3.Acronym_List__c  AS Case_ProductAcronymList_Pre_Update,
       t1.id               AS Sup_Case_ID,
       t1.legacy_number__c AS TT_TicketNumber,
       t1.legacy_id__c,
       ttp.id              AS TT_Case_productId,
       ttp.productcode     AS TT_ProductCode,
       ttp.NAME            AS TT_ProductName
       --t4.id               AS Matched_ProductID,
       --t4.legacy_product_code_string,
       --cptm.Sup_ProductId  AS Matched_ProductId_from_Shane,
       --cptm.[Mapping Product Code] as Matched_ProductId_from_Shane_productcode,
       --cte_pn.Product2Id_Sup as Matched_By_product_name
 INTO Case_Update_Product_MC   
FROM   superion_fullsb.dbo.[CASE] t1
       LEFT JOIN tritech_prod.dbo.[CASE] t2
              ON t2.id = t1.legacy_id__c
       LEFT JOIN tritech_prod.dbo.[PRODUCT2] ttp
              ON ttp.id = t2.productid
       LEFT JOIN superion_fullsb.dbo.[PRODUCT2] t3
              ON t3.id = t1.productid
       LEFT JOIN cte_legacy_product_code t4
              ON t4.legacy_product_code__c = ttp.productcode 
       LEFT JOIN cte_registered_product cte_rp 
              ON cte_rp.productcode = ttp.productcode AND
                 t2.AccountId = cte_rp.Account__c
       LEFT JOIN cte_prod_name cte_pn on cte_pn.Product2Id_TT = ttp.id   and  cte_pn.[Rank] = 1  
       LEFT JOIN cte_case_prod_mapping cptm on cptm.TT_ProductId = ttp.id
WHERE  t1.legacy_source_system__c = 'Tritech' AND
       t1.migrated_record__c = 'true' AND
       t2.productid IS NOT NULL;

