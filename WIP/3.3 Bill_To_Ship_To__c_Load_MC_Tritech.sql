/*
-- Author     : Marty Corpus
-- Date       : 01/4/2019
-- Description: Migrate custom account address fields Tritech Org to Superion Salesforce BilltoShipto object for Non Customer Accounts (prospects).
Requirement Number:REQ-0347
--Change History: SL 2/4: Added CTE and LEFT JOIN to execute the accounts that are already loaded as a part of 3.2 Bill_To_Ship_To__c_Load_SL_Tritech.sql
2019-03-10 MartyC. Added Legacy_Source_System__c = 'Tritech', Migrated_Record__c = 'true'


-------------------------------------------------------------------------------
-- Execution Log:
2019-03-10 MartyC. Wipe and Reload.

DATA WIPE 3/10:

SELECT * INTO Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC_Delete_10Mar FROM Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC WHERE Error = 'Operation Successful.'
--(21336 row(s) affected) 3/10
Exec sf_bulkops 'Delete','MC_SUPERION_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC_Delete_10Mar'
13:43:09: Warning: Column 'Ident' ignored because it does not exist in the Bill_To_Ship_To__c object.
13:47:56: 21336 rows read from SQL Table.
13:47:56: 2759 rows failed. See Error column of row for more information.
13:47:56: 18577 rows successfully processed.
13:47:56: Errors occurred. See Error column of row for more information.
13:47:56: Percent Failed = 12.900.
13:47:56: Error: DBAmp.exe was unsuccessful.
13:47:56: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe delete Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC_Delete_10Mar "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.

select distinct error from Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC_Delete_10Mar
Operation Successful.
invalid cross reference id

select * from Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC_Delete_10Mar where error = 'invalid cross reference id'

SELECT * INTO Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC_Delete_10Mar FROM Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC WHERE Error = 'Operation Successful.'
--(21328 row(s) affected) 3/10
Exec sf_bulkops 'Delete','MC_SUPERION_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC_Delete_10Mar'
13:56:52: Warning: Column 'Account_Name_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
13:56:52: Warning: Column 'Ident' ignored because it does not exist in the Bill_To_Ship_To__c object.
14:00:46: 21328 rows read from SQL Table.
14:00:46: 2758 rows failed. See Error column of row for more information.
14:00:46: 18570 rows successfully processed.
14:00:46: Errors occurred. See Error column of row for more information.
14:00:46: Percent Failed = 12.900.
14:00:46: Error: DBAmp.exe was unsuccessful.
14:00:46: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe delete Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC_Delete_10Mar "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.

BILL TO's LOAD:
--checks:
-- SELECT * FROM Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC WHERE Account__c = 'ID NOT FOUND' --59
-- select Account_ID_TT_orig , count(*) from Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC group by Account_ID_TT_orig having count(*) > 1 (0 row(s) affected)


-- Exec SF_ColCompare 'Insert','MC_SUPERION_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC' 
--- Starting SF_ColCompare V3.6.9
Problems found with Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 54]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object Bill_To_Ship_To__c does not contain column Account_ID_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account_Name_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account__C_Existing_B2

-- Exec SF_Bulkops 'Insert','MC_SUPERION_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC'
--- Starting SF_BulkOps for Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC V3.6.9
17:41:15: Run the DBAmp.exe program.
17:41:15: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:41:15: Inserting Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC (SQL01 / Staging_SB).
17:41:16: DBAmp is using the SQL Native Client.
17:41:16: SOAP Headers: 
17:41:16: Warning: Column 'Account_ID_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
17:41:16: Warning: Column 'Account_Name_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
17:41:16: Warning: Column 'Account__C_Existing_B2' ignored because it does not exist in the Bill_To_Ship_To__c object.
17:43:27: 19216 rows read from SQL Table.
17:43:27: 83 rows failed. See Error column of row for more information.
17:43:27: 19133 rows successfully processed.
17:43:27: Errors occurred. See Error column of row for more information.
17:43:27: Percent Failed = 0.400.
17:43:27: Error: DBAmp.exe was unsuccessful.
17:43:27: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.

select * from Bill_To_Ship_To__c_Tritech_Excel_load where account__c = 
'0016A00000MddacQAB'

select * from 

SHIP TO's LOAD:
--checks:
-- SELECT * FROM Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC WHERE Account__c = 'ID NOT FOUND'
-- select Account_ID_TT_orig , count(*) from Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC group by Account_ID_TT_orig having count(*) > 1

-- Exec SF_ColCompare 'Insert','MC_SUPERION_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC'
--- Starting SF_ColCompare V3.6.9
Problems found with Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 93]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object Bill_To_Ship_To__c does not contain column Account_ID_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account_Name_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account__C_Existing_s2

-- Exec SF_Bulkops 'Insert','MC_SUPERION_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC'
--- Starting SF_BulkOps for Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC V3.6.9
18:08:22: Run the DBAmp.exe program.
18:08:22: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:08:22: Inserting Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC (SQL01 / Staging_SB).
18:08:22: DBAmp is using the SQL Native Client.
18:08:23: SOAP Headers: 
18:08:23: Warning: Column 'Account_ID_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
18:08:23: Warning: Column 'Account_Name_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
18:08:23: Warning: Column 'Account__C_Existing_s2' ignored because it does not exist in the Bill_To_Ship_To__c object.
18:10:37: 19226 rows read from SQL Table.
18:10:37: 184 rows failed. See Error column of row for more information.
18:10:37: 19042 rows successfully processed.
18:10:37: Errors occurred. See Error column of row for more information.
18:10:37: Percent Failed = 1.000.
18:10:37: Error: DBAmp.exe was unsuccessful.
18:10:37: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC "SQL01"  "Staging_SB"  "MC_SUPERION_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.




*/
--use [Superion_FULLSB]

exec SF_Replicate 'MC_SUPERION_FULLSB','Bill_To_Ship_To__c'

--create B2 address records.

--DROP TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC

Use Staging_SB;

;With CteGP_B2 as
(
select Distinct Account__C,Bill_To__c from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.' and Bill_To__c=1
) --Added CTE by SL on 2/4 
SELECT Cast( NULL AS NCHAR(18))                  AS id,
       Cast( NULL AS NVARCHAR(255))              AS Error,
       COALESCE(t2.id,'ID NOT FOUND')            AS Account__c,
       t1.id                                     AS Account_ID_TT_orig,
       'true'                                    AS Bill_To__c,
       t1.NAME                                   AS Account_Name_TT_orig,
       t1.mailing_billing_street_wmp__c          AS Street_1__c,
       t1.mailing_billing_city_wmp__c            AS City__c,
       t1.mailing_billing_country_wmp__c         AS Country__c,
       t1.mailing_billing_state_wmp__c           AS State__c,
       t1.mailing_billing_zip_postal_code_wmp__c AS Zip_Postal_Code__c,
	   b2.Account__C  as Account__C_Existing_B2,
	   Legacy_Source_System__c = 'Tritech',
	   Migrated_Record__c = 'true'
INTO Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC
FROM   [Tritech_PROD].[dbo].ACCOUNT t1
       LEFT JOIN MC_SUPERION_FULLSB...ACCOUNT t2
              ON t2.legacysfdcaccountid__c = t1.id
       LEFT JOIN CteGP_B2 b2
	   on t2.id=b2.Account__C --Added by SL on 2/4 
WHERE  t1.Client__c = 'false' 
and b2.Account__C is  null --Added by SL on 2/4 

; --00:01:05


--18723
--2824

select * from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.'

--(21460 row(s) affected) 1/4 MC E2E.

ALTER TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC ADD Ident int Identity(1,1);




--create S2 address records.

--DROP TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC
;With CteGP_S2 as
(
select distinct Account__C,Ship_to__c from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.' and Ship_to__c=1
) --Added CTE by SL on 2/4 
SELECT Cast( NULL AS NCHAR(18))       AS id,
       Cast( NULL AS NVARCHAR(255))   AS Error,
       COALESCE(t2.id,'ID NOT FOUND') AS Account__c,
       t1.id                          AS Account_ID_TT_orig,
       t1.NAME                        AS Account_Name_TT_orig,
       'true'                         AS Ship_To__c,
       shipping_street_wmp__c         AS Street_1__c,
       shipping_city_wmp__c           AS City__c,
       shipping_country_wmp__c        AS Country__c,
       shipping_state_wmp__c          AS State__c,
       shipping_zip_postal_code__c    AS Zip_Postal_Code__c,
	   s2.Account__C  as Account__C_Existing_s2,
	   Legacy_Source_System__c = 'Tritech',
	   Migrated_Record__c = 'true'
INTO Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC
FROM   [Tritech_PROD].[dbo].ACCOUNT t1
       LEFT JOIN MC_SUPERION_FULLSB...ACCOUNT t2
              ON t2.legacysfdcaccountid__c = t1.id
	   LEFT JOIN CteGP_S2 s2
	   on t2.id=s2.Account__C --Added by SL on 2/4 
WHERE  t1.client__c = 'false' 
and s2.Account__C is null  --Added by SL on 2/4 

; --(21460 row(s) affected) 1/4 E2E MC.

ALTER TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC ADD Ident int Identity(1,1);


