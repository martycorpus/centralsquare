-------------------------------------------------------------------------------
--- CommunityUser Contact Community Permissions Script
--- Developed for Central Square
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: 26 November 2018
--- Last Updated: 28 November 2018 - PAB
--- Change Log: 
--- 
--- Prerequisites: 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

USE Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'Profile', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'Contact', 'yes'
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes'

USE Tritech_PROD
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'PB_Tritech_Prod', 'Case', 'yes'


---------------------------------------------------------------------------------
-- DROP STAGING TABLE IF EXISTENT 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Contact_CommunityPermissions_LOAD_PB') 
	DROP TABLE Staging_SB.dbo.[Contact_CommunityPermissions_LOAD_PB];


---------------------------------------------------------------------------------
-- CREATE CONTACT COMMUNITY PERMISSION STAGING TABLE 
---------------------------------------------------------------------------------

Select
	a.ContactID as ID,
	Cast('' as nvarchar(255)) as Error,
	CASE SrcProf.[Name] WHEN 'TriTech Portal Read Only with Tickets' THEN 'Case Access Read Only'
						WHEN 'TriTech Portal Standard User' THEN 'Case Access'
						WHEN 'TriTech Portal Manager' THEN 'Delegated Admin Dedicated'
						ELSE '' END as Community_Permissions__c	
	INTO Contact_CommunityPermissions_Load_PB
	FROM Superion_FullSB.dbo.[User] a
	-- Source User
	LEFT OUTER JOIN TriTech_Prod.dbo.[user] SrcUser ON a.Legacy_Tritech_Id__c = SrcUser.ID
	-- Source Profile
	LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] SrcProf ON SrcUser.ProfileId = SrcProf.ID
		WHERE migrated_record__c = 'true'
	and SrcProf.[Name] in ('TriTech Portal Read Only with Tickets', 'TriTech Portal Standard User', 'TriTech Portal Manager')


---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.Contact_CommunityPermissions_Load_PB
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(800)', 'PB_Superion_FULLSB', 'Contact_CommunityPermissions_Load_PB'


--select * from Contact_CommunityPermissions_Load_PB where error not like '%success%'
