/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Script for the Environment Customer Web Portal
DEVELOPER	: RTECSON and MCORPUS
CREATED DT  : 02/14/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
03/15/2019		Ron Tecson			Initial for Customer Web Portal
03/26/2019		Ron Tecson			Reloaded data in Full Sandbox. Duration time: 15 mins.

DECISIONS:

Pre-requisite:
	- Need to run the SPV Process to populate the load table Environment__c_PreLoad_Customer_Web_Portal_RT.

******************************************************************************************************************************************************/


---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

Use Superion_FULLSB
Exec SF_Refresh 'RT_SUPERION_FULLSB', 'Account', 'subset';

---------------------------------------------------------------------------------
-- Drop Environment__c_PreLoad_Non_Zuercher_RT
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_Customer_Web_Portal_RT') 
DROP TABLE Staging_SB.dbo.Environment__c_PreLoad_Customer_Web_Portal_RT;

select distinct 
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		registered_product_name, -- RegProd_ProductGroup, 
		registered_product_accountid, -- RegProd_AccountId, 
		--SUBSTRING(CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + registered_product_name + '-' + a.name,1,80) AS Name,
		SUBSTRING('PROD' + '-' + registered_product_name + '-' + a.name,1,80) AS Name,
		registered_product_accountid as Account__c,
		'Automatically created because no Hardware Software records existed in Tritech source'  as description__c,
       --       CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production'
       --             WHEN EnvironmentType = 'Train' Then 'Training'
       --             WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c,
		a.Name as AccountName,
		'Yes' AS Active__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
INTO Staging_SB.dbo.Environment__c_PreLoad_Customer_Web_Portal_RT
FROM System_Product_Version__c_Insert_Webportal_MC_Load s
	JOIN superion_fullsb.dbo.ACCOUNT a ON a.id = s.registered_product_accountid --regprod_accountid
WHERE  environment__c = 'ID NOT FOUND'

-- 03/12 -- (208 rows affected)
-- 03/15 -- (1380 rows affected)
-- 03/26 -- (2059 rows affected)

select * FROM System_Product_Version__c_Insert_Webportal_MC_Load s WHERE  environment__c = 'ID NOT FOUND'

---------------------------------------------------------------------------------
-- Drop Environment__c_Load_Customer_Web_Portal_RT
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_Load_Customer_Web_Portal_RT') 
DROP TABLE Staging_SB.dbo.Environment__c_Load_Customer_Web_Portal_RT;

select * INTO Staging_SB.dbo.Environment__c_Load_Customer_Web_Portal_RT
from Staging_SB.dbo.Environment__c_PreLoad_Customer_Web_Portal_RT

EXEC SF_ColCompare 'INSERT','RT_Superion_FULLSB','Environment__c_Load_Customer_Web_Portal_RT'

/************************************** ColCompare L O G **************************************
--- Starting SF_ColCompare V3.6.9
Problems found with Environment__c_Load_Customer_Web_Portal_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 79]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment__c does not contain column registered_product_name
Salesforce object Environment__c does not contain column registered_product_accountid
Salesforce object Environment__c does not contain column AccountName
Salesforce object Environment__c does not contain column LegacySystemIds
Salesforce object Environment__c does not contain column Winning_RecordType
Salesforce object Environment__c does not contain column RecordTypes

************************************** ColCompare L O G **************************************/

EXEC sf_bulkops 'INSERT','RT_Superion_FULLSB','Environment__c_Load_Customer_Web_Portal_RT'

/************************************** INSERT L O G **************************************

03/26/2019
--- Starting SF_BulkOps for Environment__c_Load_Customer_Web_Portal_RT V3.6.9
16:31:29: Run the DBAmp.exe program.
16:31:29: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
16:31:29: Inserting Environment__c_Load_Customer_Web_Portal_RT (SQL01 / Staging_SB).
16:31:30: DBAmp is using the SQL Native Client.
16:31:30: SOAP Headers: 
16:31:30: Warning: Column 'registered_product_name' ignored because it does not exist in the Environment__c object.
16:31:30: Warning: Column 'registered_product_accountid' ignored because it does not exist in the Environment__c object.
16:31:30: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
16:31:30: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
16:31:30: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
16:31:30: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
16:31:43: 2059 rows read from SQL Table.
16:31:43: 2059 rows successfully processed.
16:31:43: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


03/15/2019
--- Starting SF_BulkOps for Environment__c_Load_Customer_Web_Portal_RT V3.6.9
18:57:00: Run the DBAmp.exe program.
18:57:00: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:57:00: Inserting Environment__c_Load_Customer_Web_Portal_RT (SQL01 / Staging_SB).
18:57:00: DBAmp is using the SQL Native Client.
18:57:01: SOAP Headers: 
18:57:01: Warning: Column 'registered_product_name' ignored because it does not exist in the Environment__c object.
18:57:01: Warning: Column 'registered_product_accountid' ignored because it does not exist in the Environment__c object.
18:57:01: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
18:57:01: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
18:57:01: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
18:57:01: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
18:57:09: 1380 rows read from SQL Table.
18:57:09: 1380 rows successfully processed.
18:57:09: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


************************************** INSERT L O G **************************************/

------------------------------------------------------------------------------------
-- VALIDATION
------------------------------------------------------------------------------------

select error, count(*) from Environment__c_Load_Customer_Web_Portal_RT
group by error

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE Superion_FULLSB

EXEC SF_Refresh 'RT_Superion_FullSB', 'Environment__c', 'subset';

select LEN(Name), * from Environment__c_Load_Non_Zuercher_RT where error <> 'Operation Successful.'

select * from System_Product_Version__c_Insert_WebPortal_MC
where error <> 'Operation Successful.'

SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD
