-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 9.5 System_Product_Version__c_Insert_MC.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--- Copyright Apps Associates 2018
--- Created Date: Jan 18, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-01-21 Added version matching using product group only (per Maria).
--             Added ReplaceexistingwiththisVersion__c logic.
--             Added logic for version matching using LegacyMajorReleaseVersion__c.
--             Added logic for Environment Matching due to change in the Environment Name and deduping logic.
--  2019-01-29 Fix syntax error on Cte_version_MajorRelease line 151. (extra string 5.10.1 not needed, causing execution error).
--  2019-02-04 Wipe and Reload.
--  2019-02-05 Wipe and Reload.
--  2019-02-12 Wipe and Reload.
--  2019-02-13 Fixes from data validation.
--    1. Added new unions to CTE_TT_HS for additionals Product groups: Impact, tiburon, zuercher, Respond, MetroAlert, LETG, PSSI
--    2. Additional changes: 
--       a] we need to make sure that there is only one spv record per environment pre gabe.
--       b] Introduced a preload table to enforce this new requirement.
--  2019-02-14 Created CTE_Registered_Product
--  2019-02-15 Wipe and Reload.
--- Prerequisites for Script to execute
---  
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-02-15 Executed in Superion FullSbx.

-- WIPE:
-- select * into System_Product_Version__c_Insert_MC_delete_15Feb from System_Product_Version__c_Insert_MC where error = 'Operation Successful.'
-- (26078 row(s) affected)
-- exec SF_Bulkops 'Delete','MC_SUPERION_FULLSB','System_Product_Version__c_Insert_MC_delete_15Feb'
--16:26:27: Warning: Column 'environmenttype' ignored because it does not exist in the System_Product_Version__c object.
--16:26:27: Warning: Column 'Replace_Existing_Version' ignored because it does not exist in the System_Product_Version__c object.
--16:26:27: Warning: Column 'Ident' ignored because it does not exist in the System_Product_Version__c object.
--16:30:12: 26078 rows read from SQL Table.
--16:30:12: 26078 rows successfully processed.
--16:30:12: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.




-- RELOAD: (

-- COLCOMPARE:
-- exec SF_ColCompare 'Insert','MC_SUPERION_FULLSB','System_Product_Version__c_Insert_MC_load'
/*
--- Starting SF_ColCompare V3.6.9
Problems found with System_Product_Version__c_Insert_MC_load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 50]
--- Ending SF_ColCompare. Operation FAILED.
Salesforce object System_Product_Version__c does not contain column Registered Product Name
Salesforce object System_Product_Version__c does not contain column Environment Name
Salesforce object System_Product_Version__c does not contain column VERSION_NAME
Salesforce object System_Product_Version__c does not contain column tt_legacy_id_hs
Salesforce object System_Product_Version__c does not contain column tt_hs_name
Salesforce object System_Product_Version__c does not contain column tt_legacysfdcaccountid__c
Salesforce object System_Product_Version__c does not contain column account__c
Salesforce object System_Product_Version__c does not contain column account_name
Salesforce object System_Product_Version__c does not contain column version
Salesforce object System_Product_Version__c does not contain column sourcefieldname
Salesforce object System_Product_Version__c does not contain column productgroup
Salesforce object System_Product_Version__c does not contain column productline
Salesforce object System_Product_Version__c does not contain column environmenttype
Salesforce object System_Product_Version__c does not contain column Replace_Existing_Version
Salesforce object System_Product_Version__c does not contain column cte_vers.LegacyTTZVersion__c
Salesforce object System_Product_Version__c does not contain column cte_tt_hs.[Version
Salesforce object System_Product_Version__c does not contain column cte_mrv.LegacyMajorReleaseVersion__c
Salesforce object System_Product_Version__c does not contain column RegProd_ProductGroup
Salesforce object System_Product_Version__c does not contain column RegProd_ProductLine
Salesforce object System_Product_Version__c does not contain column RegProd_AccountId
Salesforce object System_Product_Version__c does not contain column rank
*/

--INSERT:
-- exec SF_Bulkops 'Insert','MC_SUPERION_FULLSB','System_Product_Version__c_Insert_MC_load'
/*
--- Starting SF_BulkOps for System_Product_Version__c_Insert_MC_load V3.6.9
18:05:21: Run the DBAmp.exe program.
18:05:21: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:05:21: Inserting System_Product_Version__c_Insert_MC_load (SQL01 / Staging_SB).
18:05:21: DBAmp is using the SQL Native Client.
18:05:22: SOAP Headers: 
18:05:22: Warning: Column 'Registered Product Name' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'Environment Name' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'VERSION_NAME' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'tt_legacy_id_hs' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'tt_hs_name' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'tt_legacysfdcaccountid__c' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'account__c' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'account_name' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'version' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'sourcefieldname' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'productgroup' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'productline' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'environmenttype' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'Replace_Existing_Version' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'cte_vers' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'cte_tt_hs' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'cte_mrv' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'RegProd_ProductLine' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the System_Product_Version__c object.
18:05:22: Warning: Column 'rank' ignored because it does not exist in the System_Product_Version__c object.
18:05:37: 2172 rows read from SQL Table.
18:05:37: 2172 rows successfully processed.
18:05:37: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.
*/
---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_PROD;

Exec SF_Refresh 'MC_TRITECH_PROD','Hardware_Software__c','Yes'

Use Superion_FULLSB;

Exec SF_Refresh 'MC_SUPERION_FULLSB','CUSTOMER_ASSET__C','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Registered_Product__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Environment__c','Yes'

Exec SF_Refresh 'MC_SUPERION_FULLSB','Version__c','Yes'


---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_SB;

--IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_MC') 
--DROP TABLE Staging_SB.dbo.System_Product_Version__c_Insert_MC;


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_MC_preload') 
DROP TABLE Staging_SB.dbo.System_Product_Version__c_Insert_MC_preload; --2/15

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_MC_load') 
DROP TABLE Staging_SB.dbo.System_Product_Version__c_Insert_MC_load; --2/15





---------------------------------------------------------------------------------
-- Create System_Product_Version__c Staging Load Table
---------------------------------------------------------------------------------

--Cte
WITH CTE_SUP_Environment As
(
SELECT Id, Name, Type__c, Account__c, [description__c] from Superion_FULLSB.dbo.Environment__c --where account__c = '0010v00000IkOIrAAN'
),
CTE_Zuercher_Customer_assets as
(
SELECT t1.id as Zuercher_Registered_Product_Id,
       t1.name as Zuercher_Registered_Product_Name,
       t2.id   AS CustomerAsset__c,
       t2.name AS CustomerAsset_Name,
       t2.product_group__c as CUSTOMER_ASSET_Product_Group,
       p2.product_group__c,
       p2.product_line__c,
       t1.Account__c,
       a.Name as accountname
FROM   superion_fullsb.dbo.REGISTERED_PRODUCT__C t1
        LEFT JOIN superion_fullsb.dbo.PRODUCT2 p2
        ON p2.id = t1.product__c
       join superion_fullsb.dbo.CUSTOMER_ASSET__C t2
         ON t2.id = t1.customerasset__c
       left join  superion_fullsb.dbo.account a on a.Id = t1.Account__c 
WHERE  t2.product_group__c = 'Zuercher' 
),
Cte_version as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, t1.ProductLine__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_FULLSB.dbo.version__c t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
),
Cte_version_MajorRelease as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, 
t1.ProductLine__c, t1.LegacyMajorReleaseVersion__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_FULLSB.dbo.version__c t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
and t1.LegacyMajorReleaseVersion__c is not null
and t1.legacyttzsource__c = 'Version reconciliation' and t1.LegacyMajorReleaseVersion__c = '1.1'
),
Cte_version_2 as
(
SELECT Row_number( )
         over (
           PARTITION BY t1.versionnumber__c
           ORDER BY (CASE WHEN t1.legacyttzsource__c = 'CS Import File' THEN 1 WHEN t1.legacyttzsource__c = 'Hardware Software' THEN 2 ELSE 3 END) ) AS "RANK",
       t1.id,
       t1.ReplaceexistingwiththisVersion__c,
       t2.name as ReplaceexistingwiththisVersion_Name,       
       t1.name,
       t1.productline__c,
       t1.legacyttzversion__c,
       t1.legacyttzproductgroup__c,
       t1.product_group__c,
       t1.versionnumber__c,
       t1.legacyttzsource__c,
       t1.current__c
FROM   superion_fullsb.dbo.VERSION__C t1
left join Superion_FULLSB.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
WHERE  t1.legacyttzproductgroup__c = 'IMC'  OR
       t1.product_group__c = 'IMC' 
),
CTE_TT_HS -- TT Hardware Software CTE
AS
(
--new 2/13
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Public_Safety_Records_Mgmt_Software__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Public_Safety_Records_Mgmt_Software__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_VCAD_Visual_Computer_Aided_Dispatch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_VCAD_Visual_Computer_Aided_Dispatch__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Comm_Server__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Comm_Server__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Advanced_Mobile_Online__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Advanced_Mobile_Online__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_PRD_Build__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_PRD_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_PRD_Build_Date__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_PRD_Build_Date__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_TRN_Build__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_TRN_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_TRN_Build_Date__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_TRN_Build_Date__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_DEV_Build__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_DEV_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_DEV_Build_Date__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_DEV_Build_Date__c IS NOT NULL
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'Zuercher' as productgroup,
          'CAD'  asproductline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%Zuercher%') t1
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'Respond' as productgroup,
          'Billing'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%respond%') t1
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'MetroAlert' as productgroup, 	
          'RMS'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%Metro%') t1 
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'LETG' as productgroup, 	 
          'CAD'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%LETG%') t1 
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'PSSI' as productgroup, 	 
          'CAD'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%PSSI%') t1  --end new 2/13
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.imc_version__c  AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMC_Version__c '
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.imc_version__c IS NOT NULL
UNION
SELECT    t1.id                   AS tt_legacy_id_hs,
          t1.name                 AS tt_hs_name,
          t1.account_name__c      AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.inform_me_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Inform_Me_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.inform_me_version__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.iq_version__c   AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IQ_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.iq_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.jail_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Jail_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.jail_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.rms_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.rms_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.rms_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.rms_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.rms_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.rms_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.ttms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'TTMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.ttms_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_cad_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_CAD_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_cad_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_fbr_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_FBR_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_fbr_software_version__c IS NOT NULL
UNION
SELECT    t1.id                          AS tt_legacy_id_hs,
          t1.name                        AS tt_hs_name,
          t1.account_name__c             AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_fire_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Fire_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_fire_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_inform_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Inform_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_inform_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_mobile_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Mobile_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_mobile_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_rms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_RMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_rms_software_version__c IS NOT NULL
UNION
SELECT    t1.id               AS tt_legacy_id_hs,
          t1.name             AS tt_hs_name,
          t1.account_name__c  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visicad_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visicad_patch__c IS NOT NULL
UNION
SELECT    t1.id                 AS tt_legacy_id_hs,
          t1.name               AS tt_hs_name,
          t1.account_name__c    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visicad_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visicad_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_production_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Production_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_production_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_test_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_test_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_test_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_test_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_training_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Training_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_training_version__c IS NOT NULL
UNION
SELECT    t1.id                        AS tt_legacy_id_hs,
          t1.name                      AS tt_hs_name,
          t1.account_name__c           AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_version__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_test_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_test_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_test_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_test_system_version__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_training_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_training_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_training_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_training_system_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.x911_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'X911_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.x911_software_version__c IS NOT NULL
),
CTE_Registered_Product  as --2/14
(SELECT rp.id as ID,
        rp.Name as Name,
		rp.account__c as account__c,
		pr.id as Product2Id,
		pr.product_group__c as Product_Group,
		pr.product_line__c as Product_Line
from superion_fullsb.dbo.Registered_Product__c rp
left join superion_fullsb.dbo.product2 pr on pr.id = rp.product__c
join superion_fullsb.dbo.Customer_Asset__c cass  on rp.customerasset__c = cass.id
where 
rp.name <> 'Customer Web Portal'
and 
cass.product_group__c in
(
'Inform','IQ','PSSI','Tiburon','ETI','Impact','LETG','MetroAlert','IMC','Respond','Zuercher','VisiNet','VisionAIR'
)
and cass.Eligible_for_Registered_Product__c = 'true'
)
SELECT DISTINCT Cast (null as nchar(18)) as ID,
                Cast (null as nvarchar(255)) as Error,
                COALESCE( regprd.id, 'ID NOT FOUND' )   AS Registered_Product__c,
                COALESCE( regprd.NAME, 'REG PROD NOT FOUND' )       AS [Registered Product Name],
                COALESCE( COALESCE(cte_env.id,coalesce(cte_env2.id,cte_env3.Id)), 'ID NOT FOUND' )       AS Environment__c,
                COALESCE( coalesce(cte_env.NAME,coalesce(cte_env2.name,cte_env3.Name)), 'ENVIRONMENT NOT FOUND' )     AS [Environment Name],
                CASE 
                   WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id) is not null then COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id)
                   WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
                   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id )
                   else 'Id not found'
                   end as Version__c,
               CASE 
                   when COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL then COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) 
                   WHEN  COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
                   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) 
                else 'VERSION NOT FOUND'
                end as [VERSION_NAME],
                cte_tt_hs.*,
                CASE
                  WHEN cte_vers.replaceexistingwiththisversion__c IS NOT NULL  OR
                       cte_vers_2.replaceexistingwiththisversion__c IS NOT NULL THEN 'True'
                  ELSE 'False'
                END AS Replace_Existing_Version  ,
				cte_vers.LegacyTTZVersion__c as [cte_vers.LegacyTTZVersion__c] ,
				 cte_tt_hs.[Version]   as [cte_tt_hs.[Version]   ,
				 cte_mrv.LegacyMajorReleaseVersion__c as [cte_mrv.LegacyMajorReleaseVersion__c],
				 regprd.Product_Group as RegProd_ProductGroup,
				 regprd.Product_Line as RegProd_ProductLine,
				 regprd.account__c as RegProd_AccountId
INTO   System_Product_Version__c_Insert_MC_preload_debug_dup_env		--drop table 	System_Product_Version__c_Insert_MC_preload_debug_dup_env	                                                                                                                                                                                      
FROM   CTE_Registered_Product regprd
       left join CTE_TT_HS cte_tt_hs
              ON regprd.account__c = CTE_TT_HS.account__c AND
                 regprd.product_group = CTE_TT_HS.productgroup AND
                 regprd.product_line = CTE_TT_HS.productline
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env
              ON cte_env.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env.type__c AND
                 cte_env.name like '%' + CTE_TT_HS.productgroup + '%'
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env2
              ON cte_env2.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env2.type__c AND
                 cte_env2.name like '%visi%' AND --2/13
                 CTE_TT_HS.productgroup = 'Inform'
       LEFT JOIN Cte_version_MajorRelease cte_mrv on cte_tt_hs.[Version]  = cte_mrv.LegacyMajorReleaseVersion__c
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env3 --TRY match on account id and default env
              ON cte_env3.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env3.type__c AND
				 cte_env3.Description__c like '%Automatically%'
       LEFT JOIN CTE_VERSION cte_vers on cte_vers.ProductLine__c = cte_tt_hs.productline and cte_vers.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%'
       LEFT JOIN Cte_version_2 cte_vers_2 
              ON coalesce(cte_vers_2.LegacyTTZProductGroup__c, cte_vers_2.Product_Group__c) =  cte_tt_hs.productgroup AND
                 cte_vers_2.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%' AND
                 cte_vers_2.[RANK] =  1
 --where regprd.account__c = '0010v00000IkOIrAAN'

       --(29065 row(s) affected) 2/15



--ALTER TABLE System_Product_Version__c_Insert_MC ADD Ident INT Identity(1,1);

SELECT *
INTO   System_Product_Version__c_Insert_MC_load_dup_env --drop table System_Product_Version__c_Insert_MC_load_dup_env
FROM   ( SELECT *,
                Row_number( )
                  OVER (
                    partition BY environment__c, registered_product__c, account__c
                    ORDER BY CASE WHEN version = 'id not found' THEN 0 ELSE Len(version) END DESC) [rank]
         FROM   System_Product_Version__c_Insert_MC_preload_debug_dup_env
         WHERE
         registered_product__c <> 'ID NOT FOUND' ) x
WHERE  x.[rank] = 1  --(2172 row(s) affected) 2/15


select * from System_Product_Version__c_Insert_MC_load_dup_env where environment__c = 'ID NOT FOUND'; --1310

SELECT rp.id as ID,
        rp.Name as Name,
		rp.account__c as account__c,
		pr.id as Product2Id,
		pr.product_group__c as Product_Group,
		pr.product_line__c as Product_Line
from superion_fullsb.dbo.Registered_Product__c rp
left join superion_fullsb.dbo.product2 pr on pr.id = rp.product__c
join superion_fullsb.dbo.Customer_Asset__c cass  on rp.customerasset__c = cass.id
where 
rp.name <> 'Customer Web Portal'
and 
cass.product_group__c in
(
'Inform','IQ','PSSI','Tiburon','ETI','Impact','LETG','MetroAlert','IMC','Respond','Zuercher','VisiNet','VisionAIR'
)
and cass.Eligible_for_Registered_Product__c = 'true'
and rp.id = 'a9a0v0000008XOgAAM'

---Null out versions
select * from System_Product_Version__c_Insert_MC_load
where version__c = 'ID NOT FOUND'

UPdate System_Product_Version__c_Insert_MC_load
set Version__c = null
where version__c = 'ID NOT FOUND' --(1577 row(s) affected)


--- update environments part 1
Update t1
set environment__c = t2.id, [Environment Name] = t2.Name
from System_Product_Version__c_Insert_MC_load_dup_env t1
join Staging_SB.dbo.Environment__c t2 on t1.RegProd_AccountId = t2.Account__c and 
     t2.name like '%' + t1.RegProd_ProductGroup + '%' and t2.type__c = 'Production'
and t2.id not in (select id from Environment__c_Load_Non_Zuercher_RT)
where t1. environment__c = 'ID NOT FOUND' 


select t2.id, t2.name, t1.* 
from System_Product_Version__c_Insert_MC_load_dup_env t1
join Staging_SB.dbo.Environment__c t2 on t1.RegProd_AccountId = t2.Account__c and 
     t2.name like '%' + t1.RegProd_ProductGroup + '%' and t2.type__c = 'Production'
and t2.id not in (select id from Environment__c_Load_Non_Zuercher_RT)
where t1. environment__c = 'ID NOT FOUND' 
and t2.name like '%gates volunteer%'


update t1
set environment__c = t2.id, [Environment Name] = t2.Name
from System_Product_Version__c_Insert_MC_load t1 --1310
join Staging_SB.dbo.Environment__c_Load_Non_Zuercher_RT t2 on t1.RegProd_AccountId = t2.Account__c and t2.RegProd_ProductGroup = t1.RegProd_ProductGroup
where t1. environment__c = 'ID NOT FOUND' --1310


select * from Staging_SB.dbo.Environment__c
where name like '%prod%'
--checks:
select * from System_Product_Version__c_Insert_MC_load_dup_env where version__c = 'ID NOT FOUND';

select * from System_Product_Version__c_Insert_MC_load_dup_env where environment__c = 'ID NOT FOUND'
and  RegProd_AccountId in
(


select Account__c  from Superion_FULLSB.dbo.Environment__c
group by Account__c, Name
having count(*) > 1
)


select * from System_Product_Version__c_Insert_MC_load_dup_env where Registered_Product__c = 'ID NOT FOUND';
select environment__c, Registered_Product__c , count(*) from System_Product_Version__c_Insert_MC_load_dup_env group by environment__c, Registered_Product__c having count(*) > 1


--------------------------- dev querries---------------------------


select * from System_Product_Version__c_Insert_MC_load_dup_env
where RegProd_AccountId = '0010v00000IkOIrAAN'

'0010v00000IkOIrAAN' 
select * from  Environment__c_Load_Non_Zuercher_RT --rons addtional env load table.

select t2.id, t2.name, t1.*
from System_Product_Version__c_Insert_MC_load t1 --1310
join Staging_SB.dbo.Environment__c_Load_Non_Zuercher_RT t2 on t1.RegProd_AccountId = t2.Account__c and t2.RegProd_ProductGroup = t1.RegProd_ProductGroup
where t1. environment__c = 'ID NOT FOUND' --1310


select distinct Registered_Product__c from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_PRELOAD

select * from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_load where
RegProd_AccountId = '0010v00000IkLUyAAN' --Rochester Police, NH

select * from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_PRELOAD
where Registered_Product__c = 'a9a0v0000008XOgAAM'

select * from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD
where Registered_Product__c = 'a9a0v0000008XOgAAM'

select * from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD  where [Environment Name] like '%rochester%'
select * from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_PRELOAD  where [Environment Name] like '%rochester%'

--create environments 
select distinct RegProd_ProductGroup, RegProd_AccountId, 'PROD-'+RegProd_ProductGroup+'-'+a.name as Name, RegProd_AccountId as account__c, 
'Automatically created because no Hardware Software records existed in Tritech source'  as description__c,
type__c = 'Production'
 from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD s
 join Superion_FULLSB.dbo.Account a on a.id = s.RegProd_AccountId
 where Environment__c = 'ID NOT FOUND'
 --and a.name like '%Rochester%'



 1. Wipe Env, EPLA 
 2. Ron load Env, EPLA 
 3. Marty Wipe SPV 
 4. Marty Load SPV
 5. Ron create Env for SPVs with missing ENvs
 6. Marty Load SPVs that errored out due to missing env.
 7. refresh the [TT_Superion_Account_Merge]

 select * from staging_sb.[dbo].[TT_Superion_Account_Merge]


 select * into [TT_Superion_Account_Merge_15Feb] from staging_sb.[dbo].[TT_Superion_Account_Merge]
-- (1557 row(s) affected)

delete from staging_sb.[dbo].[TT_Superion_Account_Merge]
where apps_comment = 'Additional Merges from Deborah 2/13  - Customers'








 FROM cte_TTZ_MergedAccount tAccount
        LEFT OUTER JOIN Superion_FULLSB.dbo.Registered_Product__c tRP1 ON tRP1.Account__c = tAccount.Id
        LEFT OUTER JOIN Superion_FULLSB.dbo.product2 tProd1 ON tProd1.Id = tRP1.Product__c and tProd1.TTZ_Product__c = 'true' and tProd1.Product_Group__c <> 'Zuercher'
        LEFT OUTER JOIN Tritech_Prod.dbo.Hardware_Software__c a ON a.Account_Name__c = tAccount.Legacy_TT_Accountid


select distinct Registered_Product__c from [dbo].[System_Product_Version__c_Insert_WebPortal_MC]

select registered_product__c


select distinct Registered_Product__c from [dbo].[System_Product_Version__c_Insert_Zuercher_MC]

select * from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_load where Registered_Product__c in (select id from superion_fullsb.dbo.registered_product__c where account__c = '0010v00000IkO7AAAV')

select count(*) from SYSTEM_PRODUCT_VERSION__C_INSERT_MC_PRELOAD --41176

System_Product_Version__c_Insert_MC_load

select account__c, *  from System_Product_Version__c_Insert_MC_preload
where productgroup = 'Respond'
and Environment__c <> 'ID NOT FOUND'
and Registered_Product__c = 'a9a0v0000004Kb1AAE'

select account__c, *  from System_Product_Version__c_Insert_MC_load
where productgroup = 'Respond'
and Environment__c <> 'ID NOT FOUND'
and  Registered_Product__c <> 'ID NOT FOUND'
and Registered_Product__c = 'a9a0v0000004Kb1AAE'

select distinct Registered_Product__c, environment__c
from System_Product_Version__c_Insert_MC_preload
where productgroup = 'Respond'
and Environment__c <> 'ID NOT FOUND'
and Registered_Product__c <> 'ID NOT FOUND'


select x.*, y.Environment__c
from 
(
select distinct Registered_Product__c, environment__c
from System_Product_Version__c_Insert_MC_preload
where productgroup = 'Respond'
and Environment__c <> 'ID NOT FOUND'
) x 
left outer join 
(
select *  from System_Product_Version__c_Insert_MC_load
where productgroup = 'Respond'
and Environment__c <> 'ID NOT FOUND'
) y on y.Registered_Product__c = x.Registered_Product__c and y.Environment__c = x.Environment__c


select account__c, *  from System_Product_Version__c_Insert_MC_preload
where productgroup = 'LETG'
and Environment__c = 'ID NOT FOUND'


select account__c, *  from System_Product_Version__c_Insert_MC_preload
where productgroup = 'MetroAlert'
and Environment__c = 'ID NOT FOUND'


Impact, tiburon, zuercher, MetroAlert,  PSSI


select registered_product__c, Environment__c, account__c, count(*) from System_Product_Version__c_Insert_MC_preload
where Registered_Product__c <> 'ID NOT FOUND'
and Version__c <> 'Id not found'
group by registered_product__c, Environment__c, account__c
having count(*) > 1



select * from System_Product_Version__c_Insert_MC_preload
where Environment__c = 'a960v0000000nTiAAI' and Registered_Product__c = 'a9a0v0000008VpQAAU'
and version = [cte_tt_hs.[Version]


= '0010v00000IkM10AAF'

select *
from 
(

select * ,
         row_number() over (partition by Environment__c, Registered_Product__c , account__c order by case when version = 'id not found' then 0 else len(version) end desc) [rank]
         from System_Product_Version__c_Insert_MC_preload
where
-- account__c = '0010v00000IkM10AAF'
--and
 Registered_Product__c <> 'ID NOT FOUND') x
 where x.[rank] = 1
 and account__c = '0010v00000IkM10AAF'


 select distinct Environment__c, Registered_Product__c, account__c
 from System_Product_Version__c_Insert_MC_preload
 where  Registered_Product__c <> 'ID NOT FOUND'
 and Environment__c <> 'id not found'
