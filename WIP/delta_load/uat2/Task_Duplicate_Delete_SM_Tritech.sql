

-- duplicate count
select legacy_id__c , count(*) from Superion_FULLSB.dbo.Task
where Migrated_Record__c='True'
group by Legacy_Id__c
having count(*)>1
-- 25 records.

-- total records. 378
-- drop table Task_Tritech_SFDC_Load_Duplicate_Delete1
; with maindupe as(
select id,legacy_id__C,whoid, whatid, accountid ,IsRecurrence,RecurrenceActivityId
from Superion_FULLSB.dbo.Task 
where Legacy_Id__c in (select legacy_id__c from Superion_FULLSB.dbo.Task
						where Migrated_Record__c='True'
						group by Legacy_Id__c
						having count(*)>1) 
					)
select m.id,m.whoid, m.whatid,m.Legacy_Id__c,m.IsRecurrence
into Task_Tritech_SFDC_Load_Duplicate_Delete1 
from maindupe m
inner join Task_Tritech_SFDC_Load t
on m.id = t.id 
-- (25 row(s) affected)
-- drop table Task_Tritech_Duplicate_Records_Delete
; with maindupe as(
select id,legacy_id__C,whoid, whatid, accountid ,IsRecurrence,RecurrenceActivityId
from Superion_FULLSB.dbo.Task 
where Legacy_Id__c in (select legacy_id__c from Superion_FULLSB.dbo.Task
						where Migrated_Record__c='True'
						group by Legacy_Id__c
						having count(*)>1) 
					)
select m.id,error= CAST(SPACE(255) as NVARCHAR(255))
into Task_Tritech_Duplicate_Records_Delete
from maindupe m  
where m.id not in (select id from Task_Tritech_SFDC_Load_Duplicate_Delete1)
-- (353 row(s) affected)
-------------------------------------------------------------------------------------------
-- Total of 353 records should be deleted.
--: Run batch program to create Event
  
--exec SF_ColCompare 'Delete','SL_SUPERION_FULLSB', 'Task_Tritech_Duplicate_Records_Delete' 

--check column names
  
--Exec SF_BulkOps 'Delete','SL_SUPERION_FULLSB','Task_Tritech_Duplicate_Records_Delete'

-- again it would recurr -- then what is the solution 
-- should those 25 records recuurence should be changed from true to false.
