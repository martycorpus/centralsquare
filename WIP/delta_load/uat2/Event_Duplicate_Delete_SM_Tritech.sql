
-- duplicate count
select legacy_id__c , count(*) from Superion_FULLSB.dbo.Event
where Migrated_Record__c='True'
group by Legacy_Id__c
having count(*)>1
-- 28 records.

-- total records. 338
-- drop table Event_Tritech_SFDC_Load_Duplicate_Delete1
; with maindupe as(
select id,legacy_id__C,whoid, whatid, accountid ,IsRecurrence,RecurrenceActivityId
from Superion_FULLSB.dbo.Event
where Legacy_Id__c in (select legacy_id__c from Superion_FULLSB.dbo.Event
						where Migrated_Record__c='True'
						group by Legacy_Id__c
						having count(*)>1) 
					)
select m.id,m.whoid, m.whatid,m.Legacy_Id__c,m.IsRecurrence
into Event_Tritech_SFDC_Load_Duplicate_Delete1 
from maindupe m
inner join Event_Tritech_SFDC_Load t
on m.id = t.id 
-- (28 row(s) affected)

-- drop table Event_Tritech_Duplicate_Records_Delete
; with maindupe as(
select id,legacy_id__C,whoid, whatid, accountid ,IsRecurrence,RecurrenceActivityId
from Superion_FULLSB.dbo.Event
where Legacy_Id__c in (select legacy_id__c from Superion_FULLSB.dbo.Event
						where Migrated_Record__c='True'
						group by Legacy_Id__c
						having count(*)>1) 
					)
select m.id,error= CAST(SPACE(255) as NVARCHAR(255))
into Event_Tritech_Duplicate_Records_Delete
from maindupe m  
where m.id not in (select id from Event_Tritech_SFDC_Load_Duplicate_Delete1)
 --(310 row(s) affected)
-------------------------------------------------------------------------------------------

-- Total of 310 records should be deleted.
--: Run batch program to create Event
  
--exec SF_ColCompare 'Delete','SL_SUPERION_FULLSB', 'Event_Tritech_Duplicate_Records_Delete' 

--check column names
  
--Exec SF_BulkOps 'Delete','SL_SUPERION_FULLSB','Event_Tritech_Duplicate_Records_Delete'

-- again it would recurr -- then what is the solution 
-- should those 28 records recuurence should be changed from true to false.
