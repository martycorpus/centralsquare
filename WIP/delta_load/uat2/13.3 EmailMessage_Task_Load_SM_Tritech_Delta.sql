/*
-- Author     : Shivani Mogullapalli
-- Date       :07/12/2018
-- Description: Migrate the EmailMessage from the Archieved Tasks.
Requirement Number:
Scope:
Migrate all the EmailMessage where relatedtoid is in the RelatedToId.
*/
/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_TRITECH_PROD','EmailMEssage','Yes'

EXEC SF_ReplicateIAD 'MC_TRITECH_PROD','task' 


Use Superion_FULLSB
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Task','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Opportunity','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Account','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Write_In__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Pricing_Request_Form__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Procurement_Activity__C','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Demo_Request__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact_reference_Link__c','yes'


*/
-- select count(*) from tritech_prod.dbo.[Task]
--1651965


use staging_sb
go
-- drop table EmailMessage_Tritech_SFDC_Preload_task_sl
declare @defaultuser varchar(18)
select @defaultuser = id  from [Superion_FULLSB].dbo.[user]
							where name like 'Superion API'
;With CteWhatData as
(
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Sales_Request__c a
,Tritech_PROD.dbo.Demo_Request__c b
where 1=1
and a.Legacy_Record_ID__c is not null
and a.legacy_record_id__C=b.id
 )  
 ,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from Superion_FULLSB.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null				
 )   
 

SELECT		
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_email.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
				,ActivityID_orig						= tr_email.[ActivityId]
				
				,BccAddress_orig						 = tr_email.[BccAddress]
				,BCcAddress								 = replace(cast([BccAddress] as nvarchar(max)),'@','@dummy.')
				,CCAddress_orig							 = tr_email.[CcAddress]
				,CcAddress								 =	replace(cast([CcAddress] as nvarchar(max)),'@','@dummy.')
				,CreatedById_orig						= tr_email.[CreatedById]
				,CreatedById							= iif(Tar_CreateID.id is null, 
																		iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Tar_createID.id )
				,CreatedDate							= tr_email.[CreatedDate]
				,FromAddress_orig						= tr_email.[FromAddress]
				,FromAddress							= replace([FromAddress],'@','@dummy.')
				,FromName								= tr_email.[FromName]
														  --,tr_email.[HasAttachment]
				,Headers								= tr_email.[Headers]
				,HTMLBody								= tr_email.[HtmlBody]
				,id_orig								= tr_email.[Id]
														--n  ,tr_email.[Incoming]
														-- n  ,tr_email.[IsClientManaged]
														  --,tr_email.[IsDeleted]
				,IsExternallyVisible					= tr_email.[IsExternallyVisible]
														  --,tr_email.[LastModifiedById]
														  --,tr_email.[LastModifiedDate]
				,MessageDate							= tr_email.[MessageDate]
														 --n ,tr_email.[MessageIdentifier]
						,ParentID_orig					=	tr_email.[ParentId]
						--,ParentID						=  Target_case.id
				,RelateddtoId_orig						= tr_email.[RelatedToId]
				,Relatedtoid							= Act.Parent_id
				,Relatedtoid_Legacy_Source_System__c	= act.Legacy_Source_System
				,What_parent_object_name				= act.what_parent_object_name
														  --n ,tr_email.[ReplyToEmailMessageId]
				,Status									= tr_email.[Status]
				,Subject								= tr_email.[Subject]
														  --,tr_email.[SystemModstamp]
				,TextBody								= tr_email.[TextBody]
														  -- n,tr_email.[ThreadIdentifier]
				,ToAddress_orig							= tr_email.[ToAddress]
				,ToAddress								= replace(cast([ToAddress] as nvarchar(max)),'@','@dummy.')
														  --n ,tr_email.[ValidatedFromAddress]
				,Isdeleted_orig							= tr_task.IsDeleted
				,IsArchived								= tr_task.IsArchived
				,Task_ownerid_orig							= Tr_task.OwnerId
				,Task_ownerid								= Tar_owner.id
				,Task_whoid_orig							= Tr_Task.WhoId
				,Task_whoid									= Whoid.Parent_id
				,Task_whoparent_object_name					= whoid.Who_parent_object_name
				,Task_legacy_source_system					= whoid.Legacy_Source_System

						 into EmailMessage_Tritech_SFDC_Preload_task_sl
 FROM [Tritech_PROD].[dbo].[EmailMessage] tr_email
 left join Superion_FULLSB.dbo.[User] Tar_CreateID
  on Tar_CreateID.Legacy_Tritech_Id__c=tr_email.CreatedById
  and Tar_CreateId.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------
  left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_email.createdbyId
  ------------------------------------------------------------------------------------
  inner join Tritech_Prod.dbo.task Tr_Task
  on tr_task.id = tr_email.ActivityId and tr_task.IsDeleted='false'
  -------------------------------------------------------------------------------------
  inner join ctewhatdata act 
  on act.Legacy_id_orig =Tr_email.RelatedToId
  --and act.Legacy_Source_System='Tritech'
  ----------------------------------------------------------------------------------------
  left join ctewhodata Whoid
  on whoid.Legacy_id_orig = tr_task.WhoId
  and act.Legacy_Source_System='Tritech'
  ----------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Tar_owner
  on Tar_owner.Legacy_Tritech_Id__c=Tr_task.OwnerId
  and Tar_owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------
 where 1=1 and 
 tr_Email.RelatedToId is not null and tr_email.ParentId is null  
 
 
 --(21831 row(s) affected)

 select * from EmailMessage_Tritech_SFDC_Preload_task_sl

 -- drop table EmailMessage_Tritech_SFDC_load_task_sl
 select  *
 into EmailMessage_Tritech_SFDC_load_task_sl
 from EmailMessage_Tritech_SFDC_Preload_task_sl tr_task
 where 1=1 and tr_task.IsArchived='True';--428
 
 --(13087 row(s) affected)

 select * from EmailMessage_Tritech_SFDC_load_task_sl;

 -- check for the duplicates in the final load table.
select Legacy_ID__c,Count(*) from EmailMessage_Tritech_SFDC_load_task_sl
group by Legacy_ID__c
having count(*)>1


select * from EmailMessage_Tritech_SFDC_load_task_sl;

--: Run batch program to create task
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'EmailMessage_Tritech_SFDC_load_task_sl' 

--check column names
  
--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','EmailMessage_Tritech_SFDC_load_task_sl'
/*
Salesforce object EmailMessage does not contain column ActivityID_orig
Salesforce object EmailMessage does not contain column BccAddress_orig
Salesforce object EmailMessage does not contain column CCAddress_orig
Salesforce object EmailMessage does not contain column CreatedById_orig
Salesforce object EmailMessage does not contain column FromAddress_orig
Salesforce object EmailMessage does not contain column id_orig
Salesforce object EmailMessage does not contain column ParentID_orig
Salesforce object EmailMessage does not contain column RelateddtoId_orig
Salesforce object EmailMessage does not contain column Relatedtoid_Legacy_Source_System__c
Salesforce object EmailMessage does not contain column What_parent_object_name
Salesforce object EmailMessage does not contain column ToAddress_orig
Salesforce object EmailMessage does not contain column Isdeleted_orig
Salesforce object EmailMessage does not contain column IsArchived
Salesforce object EmailMessage does not contain column Task_ownerid_orig
Salesforce object EmailMessage does not contain column Task_ownerid
Salesforce object EmailMessage does not contain column Task_whoid_orig
Salesforce object EmailMessage does not contain column Task_whoid
Salesforce object EmailMessage does not contain column Task_whoparent_object_name
Salesforce object EmailMessage does not contain column Task_legacy_source_system
*/

select error,count(*) from EmailMessage_Tritech_SFDC_load_task_sl
group by error

select * from EmailMessage_Tritech_SFDC_load_task_sl


select count(*) from EmailMessage_Tritech_SFDC_load_task_sl 
where error<>'Operation Successful.'

/*
Use Superion_FULLSB
EXEC SF_Refresh 'SL_SUPERION_FULLSB','EmailMessage','yes'
*/
use Staging_SB
go
-- drop table Task_tritech_SFDC_Preload_Update_whoid_sl

select 
					 Id								= em.ActivityId
					,Id_orig						= loadtk.ActivityID_orig			
					,Error							= loadtk.error	
					,OwnerID						= loadtk.Task_ownerid
					,Ownnerid_orig					= loadtk.Task_ownerid_orig
					,WhoId							= loadtk.Task_whoid
					,whoid_orig						= loadtk.Task_whoid_orig
		into Task_tritech_SFDC_Preload_Update_whoid_sl
from Superion_FULLSB.dbo.EmailMessage em 
inner join EmailMessage_Tritech_SFDC_load_task_sl loadtk
on em.id = loadtk.id;--428

-- (13086 row(s) affected)

select * from Task_tritech_SFDC_Preload_Update_whoid_sl 

select count(*) from Task_tritech_SFDC_Preload_Update_whoid_sl 

-- check duplicates.
select id, count(*) from Task_tritech_SFDC_Preload_Update_whoid_sl
group by id 
having count(*)>1

-- drop table Task_tritech_SFDC_load_Update_whoid_sl
select * 
into Task_tritech_SFDC_load_Update_whoid_sl
from Task_tritech_SFDC_Preload_Update_whoid_sl 
where 1=1 

select * from Task_tritech_SFDC_load_Update_whoid_sl

-- (13086 row(s) affected)

--: Run batch program to create task
  
--exec SF_ColCompare 'Update','SL_SUPERION_FULLSB', 'Task_tritech_SFDC_load_Update_whoid_sl' 

--check column names
  
--Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_FULLSB','Task_tritech_SFDC_load_Update_whoid_sl'

/*Salesforce object Task does not contain column Id_orig
Salesforce object Task does not contain column Ownnerid_orig
Salesforce object Task does not contain column whoid_orig
*/


select error,count(*) from Task_tritech_SFDC_load_Update_whoid_sl
group by error

select * from Task_tritech_SFDC_load_Update_whoid_sl


select *
into Task_tritech_SFDC_load_Update_whoid_sl_errors
from Task_tritech_SFDC_load_Update_whoid_sl 
where error<>'Operation Successful.';


--Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_FULLSB','Task_tritech_SFDC_load_Update_whoid_sl_errors'
