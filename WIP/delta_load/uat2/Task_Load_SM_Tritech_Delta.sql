

use staging_sb
go
 declare @defaultuser nvarchar(18)
 select @defaultuser = id from Superion_FULLSB.dbo.[user] where name = 'Superion API'
;With CteWhatData as
(
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Sales_Request__c a
inner join Tritech_PROD.dbo.Demo_Request__c b
on a.legacy_record_id__c = b.id
where 1=1
and a.Legacy_Record_ID__c is not null
			) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from Superion_FULLSB.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null				
 )   

SELECT  
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_task.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_task.[Account_Name__c]
														  --,tr_task.[Account_Name_DE__c]
				,AccountId_orig							= tr_task.[AccountId]
				,AccountId								= iif(Tar_Account.Id is null or tr_task.[AccountId] is null, null,Tar_ACcount.ID)
				,Activity_Type_For_Reporting__c			= tr_task.[Activity_Type_For_Reporting__c]
				,ActivityDate							= tr_task.[ActivityDate]
														  --,tr_task.[Brand__c]
				,CallDisposition						= tr_task.[CallDisposition]
				,CallDurationInSeconds					= tr_task.[CallDurationInSeconds]
				,CallObject								= tr_task.[CallObject]
				,CallType								= tr_task.[CallType]
														  --,tr_task.[Completed_Date_Time__c]
														  --,tr_task.[Contact_Count__c]
				,CreatedById_orig						= tr_task.[CreatedById]
				,CreatedById_target							= Target_Create.ID
				,CreatedById							= iif(Target_Create.id is null, 
																		iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_task.[CreatedDate]
														 -- ,tr_task.[Customer_Risk_Level__c]
				--,Description_orig							= tr_task.[Description]
														  --,tr_task.[DummyTouchField__c]
				,Id_orig								= tr_task.[Id]
														  --,tr_task.[IsArchived]
														  --,tr_task.[IsClosed]
														  --,tr_task.[IsDeleted]
														  --,tr_task.[IsHighPriority]
				,IsRecurrence							= tr_task.[IsRecurrence]
				,IsReminderSet							= tr_task.[IsReminderSet]
														  --,tr_task.[LastModifiedById]
														  --,tr_task.[LastModifiedDate]
				,OwnerId_orig							= tr_task.[OwnerId]
				,OwnerId_target							= Target_Owner.ID
				,OwnerId							    = iif(Target_owner.id is null,
																		iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,Priority								= tr_task.[Priority]
				,RecurrenceActivityId_orig				= tr_task.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_task.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_task.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_task.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_task.[RecurrenceInstance]
				,RecurrenceInterval						= tr_task.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_task.[RecurrenceMonthOfYear]
				,RecurrenceRegeneratedType				= tr_task.[RecurrenceRegeneratedType]
				,RecurrenceStartDateOnly				= tr_task.[RecurrenceStartDateOnly]
				,RecurrenceTimeZoneSidKey				= tr_task.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_task.[RecurrenceType]
				,ReminderDateTime						= tr_task.[ReminderDateTime]
				,Resolution_Notes__c					= tr_task.[Resolution_Notes__c]
														  --,tr_task.[Solution_s__c]
				,Status									= tr_task.[Status]
				,Subject								= tr_task.[Subject]
														  --,tr_task.[SystemModstamp]
				--,Description							= iif(tr_task.[Task_Notes__c] is not null,concat(tr_task.[Createddate],tr_task.[Task_Notes__c]),null)
				,TaskSubtype							= tr_task.[TaskSubtype]
														 -- ,tr_task.[Time_Elapsed__c]
				,Type									= tr_task.[Type]
														  --,tr_task.[WhatCount]
				,whatId_orig							= tr_task.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
				    
														  --,tr_task.[WhoCount]
				,WhoID_orig								= tr_task.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_task.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_task.[Description]
				,Description_Comments_summary__C_orig = tr_task.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_task.[Task_Notes__c]
				,[Description]  = concat( iif(tr_task.[Description] is not null,'Description::',''),tr_task.description,CHAR(13)+CHAR(10),
										  iif(tr_task.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_task.createddate),''),tr_task.[Comments_Summary__c],CHAR(13)+CHAR(10),
										  iif(tr_task.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_task.createddate),''),tr_task.[Task_Notes__c]
										  )

						into Task_Tritech_SFDC_Preload_Demo_Request
  FROM [Tritech_PROD].[dbo].[Task] tr_task
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_task.WhatId
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_task.WhoId
  -----------------------------------------------------------------------------------------------------------------------
    left join Superion_FULLSB.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_task.AccountId
and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_task.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_task.OwnerID
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_task.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_task.OwnerId
  -----------------------------------------------------------------------------------------------------------------------------
  where 1=1
  and tr_task.IsArchived='False' and tr_task.IsDeleted='False'
  and (Wt.parent_id IS NOT NULL or Wo.parent_id IS NOT NULL or Tar_Account.ID IS NOT NULL)

  --(124846 row(s) affected)
  select * from Task_Tritech_SFDC_load

  --drop table Task_Tritech_SFDC_load_demo_Request
select * 
into Task_Tritech_SFDC_load_demo_Request
from Task_Tritech_SFDC_Preload_demo_Request
where 1=1
 and (whoid is not null or whatid is not null) 
 and whatid_parent_object_name like 'Sales%'

 --(438 row(s) affected)
 -- drop table Task_Tritech_SFDC_load_demo_Request_update
 select id= b.id,whatid=a.whatid,legacy_id__c= a.Legacy_id__c,a.error
 into Task_Tritech_SFDC_load_demo_Request_update 
 from Task_Tritech_SFDC_load_demo_Request a 
 inner join Superion_FULLSB.dbo.Task b 
 on a.Legacy_id__c = b.Legacy_Id__c
 -- (432 row(s) affected)


 -- drop table Task_Tritech_SFDC_load_demo_Request_insert
 

 select * into Task_Tritech_SFDC_load_demo_Request_insert 
 from Task_Tritech_SFDC_load_demo_Request 
 where Legacy_id__c not in (select Legacy_id__c from Task_Tritech_SFDC_load_demo_Request_update)

 --(6 row(s) affected)

 
--: Run batch program to create task
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'Task_Tritech_SFDC_load_demo_Request_insert' 

--check column names
  
--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_demo_Request_insert'


--Exec SF_BulkOps 'Update','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_demo_Request_update'