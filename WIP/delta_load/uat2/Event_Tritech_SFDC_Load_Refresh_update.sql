-- drop table Event_Tritech_Update_for_Refresh 
select b.*
-- into Event_Tritech_Update_for_Refresh
 from event_SM_13122018 a
inner join event b
on a.id = b.id
and a.lastmodifieddate <> b.LastModifiedDate


use staging_sb
go
 declare @defaultuser nvarchar(18)
 select @defaultuser = id from Superion_FULLSB.dbo.[user] where name = 'Superion API'
 --print @defaultuser
;With CteWhatData as
(
select 
a.LegacySFDCAccountId__c  Legacy_id_orig,
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Account' as What_parent_object_name 
from Superion_FULLSB.dbo.Account a
where 1=1 
and a.LegacySFDCAccountId__c is not null

						union
select
a.Legacy_Opportunity_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Opportunity' as What_parent_object_name 
from Superion_FULLSB.dbo.Opportunity a
where 1=1
and a.Legacy_Opportunity_ID__c is not null

						union 
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Case' as What_parent_object_name 
from Superion_FULLSB.dbo.[case] a
where 1=1
and a.Legacy_ID__c is not null

			union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Procurement_Activity__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Procurement_Activity__c a
where 1=1
and a.Legacy_ID__c is not null
			union
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Sales_Request__c a
where 1=1
and a.Legacy_Record_ID__c is not null
				union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact_Reference_Link__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Contact_Reference_Link__c a
where 1=1
and a.Legacy_ID__c is not null

) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from Superion_FULLSB.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null				
 )   

SELECT  
				--ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_event.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_event.[Account_Name__c]
														  --,tr_event.[Account_Name_DE__c]
				,AccountID_orig						    = tr_event.[AccountId]
				,AccountId								= iif(Tar_Account.Id is null or tr_event.[AccountId] is null, 'Account not found',Tar_ACcount.ID)
				,Activity_Type_For_Reporting__c			= tr_event.[Activity_Type_For_Reporting__c]
														  ,ActivityDate = tr_event.[ActivityDate]
														  ,ActivityDateTime = tr_event.[ActivityDateTime]
														--  ,tr_event.[Brand__c]
				--,Description							 = concat( tr_event.createddate ,': ',tr_event.[Comments_Summary__c])
														  --,tr_event.[Completed_Date_Time__c]
														  --,tr_event.[Contact_Count__c]
				,CreatedbyId_orig						= tr_event.[CreatedById]
				,CreatedbyId_target							= Target_Create.id
				,CreatedById							= iif(Target_Create.id is null, 
																		iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_event.[CreatedDate]
														 -- ,tr_event.[Customer_Risk_Level__c]
				--,Description							= tr_event.[Description]
														 -- ,tr_event.[DummyTouchField__c]
				,DurationInMinutes						= tr_event.[DurationInMinutes]
				,EndDateTime							= tr_event.[EndDateTime]
				,EventSubtype							= tr_event.[EventSubtype]
														  --,tr_event.[GroupEventType]
				,ID_orig								= tr_event.[Id]
				,IsAllDayEvent							= tr_event.[IsAllDayEvent]
														  --,tr_event.[IsArchived]
														  --,tr_event.[IsChild]
														  --,tr_event.[IsDeleted]
														  --,tr_event.[IsGroupEvent]
				,IsPrivate								= tr_event.[IsPrivate]
				,IsRecurrence							= tr_event.[IsRecurrence]
				,IsReminderSet							= tr_event.[IsReminderSet]
														  --,tr_event.[LastModifiedById]
														  --,tr_event.[LastModifiedDate]
				,Location								= tr_event.[Location]
				,OwnerId_orig							= tr_event.[OwnerId]
				,ownerId_target								= Target_owner.ID
				,OwnerId							    = iif(Target_owner.id is null,
																		iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,RecurrenceActivityID_orig				= tr_event.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_event.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_event.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_event.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_event.[RecurrenceInstance]
				,RecurrenceInterval						= tr_event.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_event.[RecurrenceMonthOfYear]
				,RecurrenceStartDateTime				= tr_event.[RecurrenceStartDateTime]
				,RecurrenceTimeZoneSidKey				= tr_event.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_event.[RecurrenceType]
				,ReminderDateTime						= tr_event.[ReminderDateTime]
				,Resolution_Notes__c					= tr_event.[Resolution_Notes__c]
				,ShowAs									= tr_event.[ShowAs]
														  --,tr_event.[Solution_s__c]
				,StartDateTime							= tr_event.[StartDateTime]
				,Subject								= tr_event.[Subject]
														  --,tr_event.[SystemModstamp]
				--,Description							= tr_event.[Task_Notes__c]
														 -- ,tr_event.[Time_Elapsed__c]
				,Type									= tr_event.[Type]
														  --,tr_event.[WhatCount]
				,whatID_orig							= tr_event.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
														  --,tr_event.[WhoCount]
				,whoid_orig								= tr_event.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_event.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_event.[Description]
				,Description_Comments_summary__C_orig = tr_event.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_event.[Task_Notes__c]
				,[Description]  = concat( iif(tr_event.[Description] is not null,'Description::',''),tr_event.description,CHAR(13)+CHAR(10),
										  iif(tr_event.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_event.createddate),''),tr_event.[Comments_Summary__c],CHAR(13)+CHAR(10),
										  iif(tr_event.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_event.createddate),''),tr_event.[Task_Notes__c]
										  )
				    into Event_Tritech_SFDC_Preload_Refresh
  FROM Event_Tritech_Update_for_Refresh tr_event 
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_event.WhatId
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_event.WhoId
  ------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_event.AccountId
and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_event.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_event.OwnerId
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_event.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_event.OwnerId
  -----------------------------------------------------------------------------------------------------------------------------
  where 1=1
  and (Wt.parent_id IS NOT NULL or Wo.parent_id IS NOT NULL or Tar_Account.ID IS NOT NULL)

  select * from Event_Tritech_SFDC_Preload_Refresh


  -- drop table Event_Tritech_SFDC_Load_Update_Refresh
  select a.id,b.*
into Event_Tritech_SFDC_Load_Update_Refresh
 from event_Tritech_SFDC_load a
inner join Event_Tritech_SFDC_Preload_Refresh b 
on a.Legacy_id__c=b.Legacy_id__c

--(6 row(s) affected)

--Exec SF_BulkOps 'Update','SL_SUPERION_FULLSB','Event_Tritech_SFDC_Load_Update_Refresh'