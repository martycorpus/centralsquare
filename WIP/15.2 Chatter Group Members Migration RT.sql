/*****************************************************************************************************************************************************
REQ #		: REQ-0831
TITLE		: ChatterGroup (CollaborationGroup) Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.2 Chatter Group Members Migration PB.sql

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------
Use Tritech_Prod
Exec SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';
Exec SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroupMember', 'yes';
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes';

Use Superion_FULLSB
Exec SF_Refresh 'RT_Superion_FULLSB', 'User', 'yes';
Exec SF_Refresh 'RT_Superion_FULLSB', 'CollaborationGroup', 'yes';

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroupMember_PreLoad_RT') 
DROP TABLE Staging_SB.dbo.CollaborationGroupMember_PreLoad_RT;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------

USE Staging_SB;

SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	Group_Source.[Name] as CollaborationGroupName_Orig,
	[Group_Target].Id as CollaborationGroupId,
	a.MemberID as MemberID_Original,
	[User].ID as MemberId,
	a.CollaborationRole as CollaborationRole,
	a.NotificationFrequency as NotificationFrequency
INTO Staging_SB.dbo.CollaborationGroupMember_PreLoad_RT
FROM Tritech_Prod.dbo.CollaborationGroupMember a
LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [User] ON a.MemberID = [User].Legacy_Tritech_Id__c							----> MemberID
LEFT OUTER JOIN Tritech_PROD.dbo.[CollaborationGroup] [Group_Source] ON a.CollaborationGroupId = [Group_Source].[ID]	----> Chatter Group
INNER JOIN Superion_FULLSB.dbo.[CollaborationGroup] [Group_Target] ON Group_Source.[Name] = [Group_Target].[Name]

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroupMember_Load_RT') 
DROP TABLE Staging_SB.dbo.CollaborationGroupMember_Load_RT;

SELECT *
INTO Staging_SB.dbo.CollaborationGroupMember_Load_RT
FROM Staging_SB.dbo.CollaborationGroupMember_PreLoad_RT

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table CollaborationGroupMember_load_PB
Add [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB

Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'CollaborationGroupMember_load_PB'

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

select ERROR, count(*)
from CollaborationGroupMember_load_PB
group by ERROR

select * from CollaborationGroupMember_load_PB where error not like '%success%' and error like '%memberid%'

select * from CollaborationGroupMember_load_PB where error like '%success%'

---------------------------------------------------------------------------------------------------------------
-- Refresh Local Databsae
---------------------------------------------------------------------------------------------------------------

USE Superion_FULLSB

Exec SF_Refresh 'RT_Superion_FULLSB', 'CollaborationGroupMember', 'yes';


---------------------------------------------------------------------------------------------------------------

--drop table USER_Update_inActiveChatterGroupMembers_PB

--Select Distinct
--a.MemberID as ID,
--CAST('' as nvarchar(255)) as error,
--'true' as isactive
--INTO USER_Update_inActiveChatterGroupMembers_PB 
--From CollaborationGroupMember_load_PB a
--WHERE error like '%inactive_owner_or_user%'

--use Superion_FULLSB
--Exec SF_BulkOps 'update:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'USER_Update_inActiveChatterGroupMembers_PB'

--select * from USER_Update_inActiveChatterGroupMembers_PB

--select * from staging_sb.dbo.User_Update_toinActive_PB where error like '%successful%'

--use Staging_SB
--Exec SF_BulkOps 'update:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'User_Update_toinActive_PB'

--Select * INTO CollaborationGroupMember_load_PB_backup
--from CollaborationGroupMember_load_PB


--delete from Superion_FULLSB.dbo.CollaborationGroupMember_load_PB where error like '%success%'

--update USER_Update_inActiveChatterGroupMembers_PB set isActive = '0'