-- Use Tritech_PROD;

-- exec SF_Refresh en_Tritech_Prod,ServiceContract,'yes';


-- use Superion_Production;

-- exec sf_refresh 'MC_superion_prod','Registered_Product__c','Yes';

-- exec sf_refresh 'MC_superion_prod','Customer_Asset__c','Yes';

-- exec sf_refresh 'MC_superion_prod','System_Product_Version__c','Yes';


USE staging_sb;

DROP TABLE REGISTERED_PRODUCT__C_ADDITIONAL_CWP;
drop table REGISTERED_PRODUCT__C_ADDITIONAL_CWP_donot_create;
drop table REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create;

;
WITH cte_tt_svc_contract
     AS ( SELECT tts.id,
                 tts.accountid AS TT_Accountid,
                 ta.NAME       AS TTAcocuntName,
                 sa.id         AS Accountid_SUP,
                 tts.status,
                 tts.NAME,
                 tts.product_family_wmp__c,
                 tts.contract_type__c,
                 ttm.[tt product family],
                 ttm.[tt contract type],
                 ttm.productid,
				 ttm.[CentralSquare Product Group],
				 ttm.[CentralSquare Product Line],
                 p2.NAME       AS ttm_productname
          FROM   tritech_prod.dbo.SERVICECONTRACT tts
                 LEFT JOIN staging_sb.dbo.[TT SERVICE CONTRACT MAPPING] ttm 
                        ON ttm.[tt product family] = tts.product_family_wmp__c AND
                           ttm.[tt contract type] = tts.contract_type__c
                 LEFT JOIN MC_superion_fullsb...ACCOUNT sa
                        ON sa.legacysfdcaccountid__c = tts.accountid
                 LEFT JOIN tritech_prod.dbo.ACCOUNT ta
                        ON ta.id = tts.accountid
                 LEFT JOIN MC_superion_fullsb...PRODUCT2 p2
                        ON p2.id = ttm.productid
          WHERE  status <> 'Expired' -- 'Expired'
         )
SELECT
--sca.*, cte.*
Cast( NULL AS NCHAR(18))     AS Id,
Cast( NULL AS NVARCHAR(255)) AS Error,
cte.productid                AS Product__c,
cte.ttm_productname          AS NAME,
sca.id                       AS CustomerAsset__c,
sca.account__c               AS Account__c,
'True'                       AS Needs_Review__c,
cast('True' as nvarchar(5))  AS Create_Registered_Product_Load_Record,
cast(null as nchar(18))      as Existing_Sup_Registered_Product_id,
cte.product_family_wmp__c    AS TT_ServiceContract_ProdFamily,
cte.contract_type__c         AS TT_ServiceContract_ContractType,
sca.NAME                     AS SUP_Customer_Asset_Name,
cte.id                       AS TT_ServiceContractID,
cte.status                   AS TT_ServiceContract_Status,
cte.ttacocuntname            AS TT_AccountName,
cte.[CentralSquare Product Group],
cte.[CentralSquare Product Line]
INTO   REGISTERED_PRODUCT__C_ADDITIONAL_CWP
FROM   MC_superion_fullsb...CUSTOMER_ASSET__C sca
       JOIN cte_tt_svc_contract cte
         ON cte.accountid_sup = sca.account__c
WHERE  sca.product__c = '01t6A000000GyHWQA0'; --6117 00:02:00 4/7 MC

--exclude registered products to create based on account id and product id (already existing).

Update t1
set Create_Registered_Product_Load_Record = 'False',  Existing_Sup_Registered_Product_id = t2.id
from REGISTERED_PRODUCT__C_ADDITIONAL_CWP t1
join (select distinct id, Account__c, Product__c from MC_superion_fullsb...Registered_Product__c) t2 on t2.Product__c = t1.Product__c and t1.Account__c = t2.Account__c;
--(1789 row(s) affected) oo:02:22 4/7

--exclude registered products to create based on account id, product line, and product group (already existing).
Update t1
set Create_Registered_Product_Load_Record = 'False',  Existing_Sup_Registered_Product_id = t2.id
from REGISTERED_PRODUCT__C_ADDITIONAL_CWP t1
join (select t2.Product_Line__c,t2.Product_Group__c , t1.id, t1.Account__c  from MC_superion_fullsb...Registered_Product__c t1
join MC_superion_fullsb...product2 t2 on t2.id = t1.product__c) t2 on t2.Product_Line__c = t1.[CentralSquare Product Line] and t1.[CentralSquare Product Group] = t2.Product_Group__c
     and t2.Account__c = t1.Account__c
where t1.Create_Registered_Product_Load_Record = 'True'; --(1360 row(s) affected) 4/7

--exclude registered products to create based on if a customer has a substitute RP already manually created by Support or Finance group.
select t1.* from MC_superion_fullsb...Registered_Product__c t1
join tritech_prod.dbo.ServiceContract ts on ts.id = t1.Legacy_SFDC_ID__c and t1.Needs_Review__c = 'true'

Update t1
set Create_Registered_Product_Load_Record = 'False',  Existing_Sup_Registered_Product_id = 'Already Created.'
from REGISTERED_PRODUCT__C_ADDITIONAL_CWP t1
join (select  distinct rpc.account__c from MC_superion_fullsb...Registered_Product__c rpc
join tritech_prod.dbo.ServiceContract ts on ts.id = rpc.Legacy_SFDC_ID__c and rpc.Needs_Review__c = 'true') t2 on  t2.Account__c = t1.Account__c
where t1.Create_Registered_Product_Load_Record = 'True'; 



SELECT *
into REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create
FROM   staging_sb.dbo.REGISTERED_PRODUCT__C_ADDITIONAL_CWP
where Create_Registered_Product_Load_Record = 'True';


SELECT *
into REGISTERED_PRODUCT__C_ADDITIONAL_CWP_donot_create
FROM   staging_sb.dbo.REGISTERED_PRODUCT__C_ADDITIONAL_CWP
where Create_Registered_Product_Load_Record = 'false';

SELECT *
FROM   staging_sb.dbo.REGISTERED_PRODUCT__C_ADDITIONAL_CWP
where Create_Registered_Product_Load_Record = 'True';

select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_donot_create

select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create

exec SF_ColCompare  'Insert','MC_superion_fullsb','REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create';
--- Starting SF_ColCompare V3.7.7
--Problems found with REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 122]
--- Ending SF_ColCompare. Operation FAILED.
--Salesforce object REGISTERED_PRODUCT__C does not contain column Create_Registered_Product_Load_Record
--Salesforce object REGISTERED_PRODUCT__C does not contain column Existing_Sup_Registered_Product_id
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_ServiceContract_ProdFamily
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_ServiceContract_ContractType
--Salesforce object REGISTERED_PRODUCT__C does not contain column SUP_Customer_Asset_Name
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_ServiceContractID
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_ServiceContract_Status
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_AccountName
--Salesforce object REGISTERED_PRODUCT__C does not contain column CentralSquare Product Group
--Salesforce object REGISTERED_PRODUCT__C does not contain column CentralSquare Product Line

exec SF_BulkOps 'Insert','MC_superion_fullsb','REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create';
--- Starting SF_BulkOps for REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create V3.7.7
14:36:52: Run the DBAmp.exe program.
14:36:52: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
14:36:52: Inserting REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create (SQL01 / Staging_SB).
14:36:53: DBAmp is using the SQL Native Client.
14:36:54: SOAP Headers: 
14:36:54: Warning: Column 'Create_Registered_Product_Load_Record' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'Existing_Sup_Registered_Product_id' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'TT_ServiceContract_ProdFamily' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'TT_ServiceContract_ContractType' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'SUP_Customer_Asset_Name' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'TT_ServiceContractID' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'TT_ServiceContract_Status' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'TT_AccountName' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'CentralSquare Product Group' ignored because it does not exist in the Registered_Product__c object.
14:36:54: Warning: Column 'CentralSquare Product Line' ignored because it does not exist in the Registered_Product__c object.
14:37:18: 2968 rows read from SQL Table.
14:37:18: 2968 rows successfully processed.
14:37:18: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.



select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create

------Create SPVs For Substitute RP's
drop table System_Product_Version__c_Insert_substitute_ADDITIONAL_CWP_create;

SELECT Cast( NULL AS NCHAR(18))     AS Id,
       Cast( NULL AS NVARCHAR(255)) AS Error,
	   t1.id as Reqistered_Product__c,
	   Cast( NULL AS NCHAR(18))     AS Environment__c,
	   Cast( NULL AS NVARCHAR(255)) AS Environment_Name,
	   Cast( NULL AS NVARCHAR(255)) AS Environment_match_type,
	   t1.[CentralSquare Product Group] ,
	   t1.Account__c
 INTO System_Product_Version__c_Insert_substitute_ADDITIONAL_CWP_create
FROM REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create t1;--(2968 row(s) affected) 4/7


--find an environment based on account__c and CentralSquare Product Group (col c in the mapping).
UPDATE rp
SET    environment__c = se.id, Environment_Name =  se.Name, Environment_match_type = 'Account and CS Prod group'
FROM   SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE rp
       JOIN ( SELECT *
              FROM   MC_superion_fullsb...ENVIRONMENT__C
              WHERE  migrated_record__c = 'true' AND
                     legacy_source_system__c = 'Tritech' AND
                     NAME LIKE 'PROD%' ) se
         ON se.account__c = rp.account__c AND
            se.[name] LIKE '%' + rp.[centralsquare product group] + '%' ; --(1416 row(s) affected) 4/7


--for the records that did not find an environment based on account__c and CentralSquare Product Group (col c in the mapping), 
--default to the CWP environment for that Account.

UPDATE rp
SET    environment__c =spv.Environment__c, Environment_Name =  spv.Name, Environment_match_type = 'CWP default.'
FROM   SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE rp
join (
select rp.account__c, rp.Id as RP_Id, spv.Environment__c, e.Name  from MC_superion_fullsb...SYSTEM_PRODUCT_VERSION__C spv
join MC_superion_fullsb...registered_product__c rp on rp.id = spv.registered_product__c
join MC_superion_fullsb...Environment__c e on e.Id = spv.Environment__c 
where Product_Name__c like '%customer web%') spv on spv.Account__c = rp.Account__c 
where rp.Environment__c is null --(1546 row(s) affected) 02:25


select * from SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE
Required fields are missing: [Registered_Product__c]
Reqistered_Product__c

exec SF_BulkOps 'Insert','MC_superion_fullsb','SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE';

--- Starting SF_BulkOps for SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE V3.7.7
--18:46:56: Run the DBAmp.exe program.
--18:46:56: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
--18:46:56: Inserting SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE (SQL01 / Staging_SB).
--18:46:56: DBAmp is using the SQL Native Client.
--18:46:56: SOAP Headers: 
--18:46:57: Warning: Column 'Reqistered_Product__c' ignored because it does not exist in the System_Product_Version__c object.
--18:46:57: Warning: Column 'Environment_Name' ignored because it does not exist in the System_Product_Version__c object.
--18:46:57: Warning: Column 'Environment_match_type' ignored because it does not exist in the System_Product_Version__c object.
--18:46:57: Warning: Column 'CentralSquare Product Group' ignored because it does not exist in the System_Product_Version__c object.
--18:46:57: Warning: Column 'Account__c' ignored because it does not exist in the System_Product_Version__c object.
--18:47:18: 2968 rows read from SQL Table.
--18:47:18: 2968 rows successfully processed.
--18:47:18: Percent Failed = 0.000.
----- Ending SF_BulkOps. Operation successful.

alter table SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE add  Registered_Product__c nchar(15)

alter table SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE alter column Registered_Product__c nchar(18)

select * from SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE

update SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE set Registered_Product__c = Reqistered_Product__c

select * from SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE
---dev
/*

select sca.*
FROM   superion_production.dbo.CUSTOMER_ASSET__C sca
WHERE  sca.product__c = '01t6A000000GyHWQA0';

select * from product2 where id = '01t6A000000GyHWQA0' --CentralSquare Customer Web Portal


select * from 

select * from superion_production.dbo.SYSTEM_PRODUCT_VERSION__C spv
join superion_production.dbo.registered_product__c rp on rp.id = spv.registered_product__c
where Product_Name__c like '%customer web%'



--find an environment based on account__c and CentralSquare Product Group (col c in the mapping).

select se.id, se.name, rp.[CentralSquare Product Group] ,  rp.* from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create rp 
join 
(
select * from Superion_Production.dbo.Environment__c
where Migrated_Record__c = 'true' and Legacy_Source_System__c = 'Tritech'
and name like 'PROD%') se on se.Account__c = rp.Account__c and 
se.[name] like '%'+ rp.[CentralSquare Product Group] + '%'
select dbo.fnSFDC18([Product ID 15]) , [Product ID 15], *
from [TT Service Contract Mapping]

select * from [TT SERVICE CONTRACT MAPPING]


select t2.Product_Line__c,t2.Product_Group__c , t1.id  from Superion_Production.dbo.Registered_Product__c t1
join Superion_Production.dbo.product2 t2 on t2.id = t1.product__c


[CentralSquare Product Group]	[CentralSquare Product Line]
Respond	ePCR
ePCR	Respond ePCR Base

update [TT Service Contract Mapping]
set [ProductID] = dbo.fnSFDC18([Product ID 15])
where [Product ID 15] like '01%'

select tts.AccountId, tts.status, tts.name, tts.Product_Family_WMP__c, tts.Contract_Type__c
from tritech_prod.dbo.ServiceContract tts
join staging_sb.dbo.[TT Service Contract Mapping] ttm on ttm.[TT Product Family] = tts.Product_Family_WMP__c and
      ttm.[TT Contract Type] = tts.Contract_Type__c
where Product_Family_WMP__c  = 'ems' AND Contract_Type__c  = 'Respond CAD'
AND STATUS <> 'Expired'

select id, name from product2 where name like '%customer web%'

y yall -- support mentioned one additional dimension on the service contract that could help us generate more precise registered products 
-- it's called "Contract Type" -- here's a TT prod report with that dimension added 
-- we would still provide a CS product ID for each combination of family/contract type 
-- any concerns on your end? If so, I won't show it to the business on this call  https://na70.salesforce.com/00O1E000006ZqWU



;WITH cte_customer_asset_sup as
(select * from Superion_Production.customer_asset__c )
SELECT tts.id,
       tts.accountid AS TT_Accountid,
       ta.NAME       AS TTAcocuntName,
       sa.id         AS Accountid_SUP,
       tts.status    AS TT_SupportContact_Status,   
       tts.NAME      AS TT_SupportContact_Name,
       tts.product_family_wmp__c AS TT_SupportContact_product_family_wmp,
       tts.contract_type__c  as TT_SupportContact_contract_type,
       ttm.[tt product family],
       ttm.[tt contract type],
       ttm.productid as Mapping_contract_type
FROM   tritech_prod.dbo.SERVICECONTRACT tts
       LEFT JOIN staging_sb.dbo.[TT SERVICE CONTRACT MAPPING] ttm
              ON ttm.[tt product family] = tts.product_family_wmp__c AND
                 ttm.[tt contract type] = tts.contract_type__c
       LEFT JOIN superion_production.dbo.ACCOUNT sa
              ON sa.legacysfdcaccountid__c = tts.accountid
       LEFT JOIN tritech_prod.dbo.ACCOUNT ta
              ON ta.id = tts.accountid
WHERE  status <> 'Expired' -- 'Expired'

select Product_Family_WMP__c, Contract_Type__c, count(*) from 
(
select tts.id, tts.AccountId as TT_Accountid, ta.name as TTAcocuntName, sa.id as Accountid_SUP, tts.status, tts.name, tts.Product_Family_WMP__c, tts.Contract_Type__c, ttm.[TT Product Family], ttm.[TT Contract Type],ttm.productid
from tritech_prod.dbo.ServiceContract tts
left join staging_sb.dbo.[TT Service Contract Mapping] ttm on ttm.[TT Product Family] = tts.Product_Family_WMP__c and
      ttm.[TT Contract Type] = tts.Contract_Type__c
left join Superion_Production.dbo.Account sa on sa.LegacySFDCAccountId__c = tts.AccountId
left join Tritech_PROD.dbo.account ta on ta.id = tts.AccountId
where status <>  'Expired' -- 'Expired'
and sa.id is null
--and ttm.[TT Product Family] is null 
--and ttm.[TT Contract Type] is null
) x
group by Product_Family_WMP__c, Contract_Type__c
order by Product_Family_WMP__c, Contract_Type__c
*/