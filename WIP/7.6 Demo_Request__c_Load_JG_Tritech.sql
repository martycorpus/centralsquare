  /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Demo_Request__c----------
Requirement No  :
Total Records   :  
Scope: Migrate all records related to a migrated opportunity 
 */
  /*
Use Tritech_PROD
EXEC SF_Replicate 'SL_Tritech_PROD','Demo_Request__c'



Use Superion_FULLSB
EXEC SF_Replicate 'SL_Superion_FULLSB','RecordType','Yes'
EXEC SF_Replicate 'SL_Superion_FULLSB','Account','Yes'
EXEC SF_Replicate 'SL_Superion_FULLSB','User','Yes'
EXEC SF_Replicate 'SL_Superion_FULLSB','Opportunity','Yes'
*/

Use Staging_SB;

--Drop table Sales_Request__c_Tritech_SFDC_DemoRequest_Preload;

DECLARE @RecordTypeId NVARCHAR(18) = 
(Select top 1 Id from Superion_FULLSB.dbo.recordtype where name ='Demo Request');

declare @defaultuser nvarchar(18)=(select top 1 id from Superion_FULLSB.dbo.[User] 
                                 where name ='Superion API' );

DECLARE @GeneralRequest NVARCHAR(18) = 
(Select top 1 Id from Superion_FULLSB.dbo.recordtype where name ='General Request');


 Select
 
 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Record_ID__c =demo.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,Additional_Agency_1__c_orig=Additional_Agency_1__c
,Additional_Agency_1__c=accnt1.Id

,Additional_Agency_2__c_orig=Additional_Agency_2__c
,Additional_Agency_2__c=accnt2.Id

,Additional_Agency_3__c_orig=Additional_Agency_3__c
,Additional_Agency_3__c=accnt3.Id

,Attendee_s__c=CONCAT(IIF(user1.Name IS NULL,NULL,IIF(Additional_Attendee_1_Role__c IS NULL,user1.Name,CONCAT(user1.Name,',',Additional_Attendee_1_Role__c,CHAR(13)+CHAR(10)))),
                      IIF(user2.Name IS NULL,NULL,IIF(Additional_Attendee_2_Role__c IS NULL,user2.Name,CONCAT(user2.Name,',',Additional_Attendee_2_Role__c,CHAR(13)+CHAR(10)))),
					  IIF(user3.Name IS NULL,NULL,IIF(Additional_Attendee_3_Role__c IS NULL,user3.Name,CONCAT(user3.Name,',',Additional_Attendee_3_Role__c,CHAR(13)+CHAR(10)))),       
					  IIF(user4.Name IS NULL,NULL,IIF(Additional_Attendee_4_Role__c IS NULL,user4.Name,CONCAT(user4.Name,',',Additional_Attendee_4_Role__c,CHAR(13)+CHAR(10)))),
					  IIF(user5.Name IS NULL,NULL,IIF(Additional_Attendee_5_Role__c IS NULL,user5.Name,CONCAT(user5.Name,',',Additional_Attendee_5_Role__c,CHAR(13)+CHAR(10)))),
					  IIF(user6.Name IS NULL,NULL,IIF(Additional_Attendee_6_Role__c IS NULL,user6.Name,CONCAT(user6.Name,',',Additional_Attendee_6_Role__c,CHAR(13)+CHAR(10))))
                     )

,Additional_Attendee_1__c_orig=Additional_Attendee_1__c
,Additional_Attendee_1__c_SFID=user1.Id

,Additional_Attendee_1_Role__c_orig=Additional_Attendee_1_Role__c

,Additional_Attendee_2__c_orig=Additional_Attendee_2__c
,Additional_Attendee_2__c_SFID=user2.Id

,Additional_Attendee_2_Role__c_orig=Additional_Attendee_2_Role__c

,Additional_Attendee_3__c_orig=Additional_Attendee_3__c
,Additional_Attendee_3__c_SFID=user3.Id

,Additional_Attendee_3_Role__c_orig=Additional_Attendee_3_Role__c

,Additional_Attendee_4__c_orig=Additional_Attendee_4__c
,Additional_Attendee_4__c_SFID=user4.Id

,Additional_Attendee_4_Role__c_orig=Additional_Attendee_4_Role__c

,Additional_Attendee_5__c_orig=Additional_Attendee_5__c
,Additional_Attendee_5__c_SFID=user5.Id

,Additional_Attendee_5_Role__c_orig=Additional_Attendee_5_Role__c

,Additional_Attendee_6__c_orig=Additional_Attendee_6__c
,Additional_Attendee_6__c_SFID=user6.Id

,Additional_Attendee_6_Role__c_orig=Additional_Attendee_6_Role__c

,Additional_Information_Required__c=Additional_Information_Required__c
,Agency_Types_Involved__c=Agency_Types_Involved__c
,Are_Additional_Agencies_Involved__c=Are_Additional_Agencies_Involved__c
,AV_Needed_Date__c=AV_Needed_Date__c
,AV_Needed_Time__c=AV_Needed_Time__c
,AV_Needed_Time_Zone__c=AV_Needed_Time_Zone__c
,AV_Pickup_Date__c=AV_Pickup_Date__c
,AV_Pickup_Time__c=AV_Pickup_Time__c
,AV_Pickup_Time_Zone__c=AV_Pickup_Time_Zone__c
,AV_REQUEST_COMPLETE__c=AV_REQUEST_COMPLETE__c
,Building_Access_Information__c=Building_Access_Information__c
,Building_Access_Information_for_Caterer__c=Building_Access_Information_for_Caterer__c
,Building_access_information_for_Vendor__c=Building_access_information_for_Vendor__c
--,CAD__c=demo.CAD__c
--,Catering_Needs__c=Catering_Needs__c
,Catering_Request_Complete__c=Catering_Request_Complete__c
--,Client_Organization_Name__c=Client_Organization_Name__c

,CreatedById_orig=demo.CreatedById
,CreatedById=IIF(crtdbyusr.Id IS NULL,@defaultuser,crtdbyusr.Id)

,CreatedDate=demo.CreatedDate
--,Current_Opportunity_Stage__c=Current_Opportunity_Stage__c
--,Current_Opportunity_Type__c=Current_Opportunity_Type__c
,Custom_Forms_From_Agency__c=Custom_Forms_From_Agency__c
,Demo_Booklets__c=Demo_Booklets__c
,Demo_Booklets_Quantity__c=Demo_Booklets_Quantity__c
,Legacy_Demo_Contact__c=Demo_Contact__c
,Legacy_Demo_Contact_Phone__c=Demo_Contact_Phone__c
,Demo_Date_Options__c=Demo_Dates__c
,Demo_End_Date__c=Demo_End_Date__c
,Demo_End_Time__c=Demo_End_Time__c
,Demo_End_Time_Zone__c=Demo_End_Time_Zone__c
,Demo_Kit__c=Demo_Kit__c
,Demo_Location__c=Demo_Location__c
,Demo_Notes_Attached__c=Demo_Notes_Attached__c
,Demo_Objective_Project_Background__c=Demo_Objective__c
,DEMO_REQUEST_COMPLETE__c=DEMO_REQUEST_COMPLETE__c
,Demo_Set_up_Date__c=Demo_Set_up_Date__c
,Demo_Set_up_Time__c=Demo_Set_up_Time__c
,Demo_Set_up_Time_Zone__c=Demo_Set_up_Time_Zone__c
,Demo_Start_Date__c=Demo_Start_Date__c
,Demo_Start_Time__c=Demo_Start_Time__c
,Demo_Time_Zone__c=Demo_Start_Time_Zone__c
,Demo_System_Assigned__c=Demo_System_Assigned__c
,Demo_Team__c=Demo_Team__c
,Demo_Type__c=Demo_Type__c
--,Demo_wiki__c=Demo_wiki__c
,Discovery_Results_Attached__c=Discovery_Results_Attached__c
,Dress_Code_Expected__c=Dress_Code_Expected__c
,Drop_Off_Address__c=Drop_Off_Address__c
--,DummyTouchField__c=DummyTouchField__c
--,Fire_Records__c=Fire_Records__c
,Food_Drinks_Needed__c=Food_Drinks_Needed__c
--,GIS_Analyst_Required__c=GIS_Analyst_Required__c
--,Id=Id
,If_Other_Please_List__c=If_Other_Please_List__c
,Internet_Availability__c=Internet_Availability__c
--,IQ_and_Analytics__c=IQ_and_Analytics__c
--,IsDeleted=demo.IsDeleted
--,Jail_Records__c=Jail_Records__c
,Key_Areas_of_the_SW_to_Highlight__c=Key_Areas_of_the_SW_to_Highlight__c
,Laptop_s__c=Laptop_s__c
--,LastActivityDate_orig=demo.LastActivityDate

--,LastModifiedById_orig=demo.LastModifiedById

--,LastModifiedDate_orig=demo.LastModifiedDate
--,LastReferencedDate=demo.LastReferencedDate
--,LastViewedDate=demo.LastViewedDate
,Logistics_Team_Hotel__c=Logistics_Team_Hotel__c
--,Manager_Email__c=Manager_Email__c
--,Manager_Email_Formula__c=Manager_Email_Formula__c
,Marketing_Shipment_Location__c =Marketing_Shipment_Location__c 
,Marketing_Needs_Quantity__c=Marketing_Needs_Quantity__c
,MARKETING_REQUEST_COMPLETE__c=MARKETING_REQUEST_COMPLETE__c
,Materials__c=Materials__c
--,Mobile__c=demo.Mobile__c
,Legacy_Request_Name__c=demo.Name
,Nameplate_Cards_Needed__c=Nameplate_Cards_Needed__c
,Nearest_Airport__c=Nearest_Airport__c
,Notes__c=demo.Notes__c
,Number_of_Laptops_Needed__c=Number_of_Laptops_Needed__c
,Number_of_Monitors_Needed__c=Number_of_Monitors_Needed__c
,Number_of_Projectors_Needed__c=Number_of_Projectors_Needed__c
,Number_of_Screens_Needed__c=Number_of_Screens_Needed__c
,On_Site_Discovery_Date__c=On_Site_Discovery_Date__c

,Opportunity__c_orig=Opportunity__c
,Opportunity__c=oppty.Id

--,Other_3rd_Party__c=Other_3rd_Party__c
,Other_Equipment_Needed__c=Other_Equipment_Needed__c
,Other_Staff_Attending__c=Other_ZT_People_Attending__c
,Performed_On_Site_Discovery__c=Performed_On_Site_Discovery__c
,Peripheral_devices_needed__c=Peripheral_devices_needed__c
,Permission_to_Record_Demo__c=Permission_to_Record_Demo__c
,Pickup_Address__c=Pickup_Address__c
,Products_to_be_Demo_d__c=Products_to_be_Demo_d__c
,Projector_Stands_Needed__c=Projector_Stands_Needed__c
--,Records_Management__c=Records_Management__c

,RecordTypeId_orig=demo.RecordTypeId
,RecordTypeId=IIF(Demo_Booklets__c IS NOT NULL or Demo_Booklets_Quantity__c IS NOT NULL,@GeneralRequest,@RecordTypeId)

--,Requestor_Mobile_Phone__c=Requestor_Mobile_Phone__c
,Room_Description__c_orig=Room_Description__c
,CASE
 WHEN Room_Description__c='Large (room for more than 2 screens)' THEN 'Large (Room for more than 2 screens)'
 WHEN Room_Description__c='Medium (room for 2 screens)' THEN 'Medium (Room for 2 screens)'
 WHEN Room_Description__c='Small (Room for 1 screen)' THEN 'Small (Room for 1 screen)'
 WHEN Room_Description__c='Very Small (No screens)' THEN 'Very Small (No screens)'
 END as Room_Description__c

,Room_Dimensions__c=Room_Dimensions__c
--,Sales_Calendar__c=Sales_Calendar__c
,Screen_Size__c=Screen_Size__c
,Sharpies_Needed__c=Sharpies_Needed__c
,Shipping_Instructions_If_needed__c=Shipping_Instructions_If_needed__c
,Solution_Architect_Needed__c=Solution_Architect_Needed__c
,Solution_Architect_s__c=Solution_Architect_s__c
,State_database_needed__c=State_database_needed__c
,Status__c=Status__c
,Swag_Demo_Team_Will_Bring__c=Swag__c
--,SystemModstamp=demo.SystemModstamp
,Tablecloth_s_Needed__c=Tablecloth_s_Needed__c
,Theme_Theme_Points__c=Theme_Theme_Points__c
,Third_Party_Vendor__c=Third_Party_Vendor__c
,Total_Number_of_Attendees__c=Total_Number_of_Attendees__c
--,Travel__c=Travel__c
,Logo_Stand_s_Needed__c=TriTech_Logo_Stand_Needed__c

,Requestor__c_orig=TriTech_Requestor__c
,Requestor__c=crtdbyusr.Id

,Who_is_bringing_projectors__c=Who_is_bringing_projectors__c
,Who_is_bringing_screens__c=Who_is_bringing_screens__c
,Will_AV_Company_setup_or_just_drop_off__c=Will_AV_Company_Setup__c
,Will_AV_Company_tear_down_screens__c=Will_AV_Company_tear_down_screens__c
--,X911__c=X911__c


-- into Sales_Request__c_Tritech_SFDC_DemoRequest_Preload
--Select count(*) 
from Tritech_PROD.dbo.Demo_Request__c demo
inner join Superion_FULLSB.dbo.Opportunity oppty
on oppty.Legacy_Opportunity_ID__c=demo.Opportunity__c

--Fetching Additional_Agency_1__c (Account Lookup)
left join Superion_FULLSB.dbo.Account accnt1 on
accnt1.LegacySFDCAccountId__c=demo.Additional_Agency_1__c

--Fetching Additional_Agency_2__c (Account Lookup)
left join Superion_FULLSB.dbo.Account accnt2 on
accnt2.LegacySFDCAccountId__c=demo.Additional_Agency_2__c

--Fetching Additional_Agency_3__c (Account Lookup)
left join Superion_FULLSB.dbo.Account accnt3 on
accnt3.LegacySFDCAccountId__c=demo.Additional_Agency_3__c

--Fetching Additional_Attendee_1__c (User Lookup)
left join Superion_FULLSB.dbo.[User] usertar1 on
usertar1.Legacy_Tritech_Id__c=demo.Additional_Attendee_1__c

--Fetching Additional_Attendee_1_Name ( Legacy User Lookup)
left join Tritech_PROD.dbo.[User] user1 on
user1.Id=demo.Additional_Attendee_1__c


--Fetching Additional_Attendee_2__c (User Lookup)
left join Superion_FULLSB.dbo.[User] usertar2 on
usertar2.Legacy_Tritech_Id__c=demo.Additional_Attendee_2__c

--Fetching Additional_Attendee_2_Name ( Legacy User Lookup)
left join Tritech_PROD.dbo.[User] user2 on
user2.Id=demo.Additional_Attendee_2__c

--Fetching Additional_Attendee_3__c (User Lookup)
left join Superion_FULLSB.dbo.[User] usertar3 on
usertar3.Legacy_Tritech_Id__c=demo.Additional_Attendee_3__c

--Fetching Additional_Attendee_3_Name ( Legacy User Lookup)
left join Tritech_PROD.dbo.[User] user3 on
user3.Id=demo.Additional_Attendee_3__c

--Fetching Additional_Attendee_4__c (User Lookup)
left join Superion_FULLSB.dbo.[User] usertar4 on
usertar4.Legacy_Tritech_Id__c=demo.Additional_Attendee_4__c

--Fetching Additional_Attendee_4_Name ( Legacy User Lookup)
left join Tritech_PROD.dbo.[User] user4 on
user4.Id=demo.Additional_Attendee_4__c

--Fetching Additional_Attendee_5__c (User Lookup)
left join Superion_FULLSB.dbo.[User] usertar5 on
usertar5.Legacy_Tritech_Id__c=demo.Additional_Attendee_5__c

--Fetching Additional_Attendee_5_Name ( Legacy User Lookup)
left join Tritech_PROD.dbo.[User] user5 on
user5.Id=demo.Additional_Attendee_5__c

--Fetching Additional_Attendee_6__c (User Lookup)
left join Superion_FULLSB.dbo.[User] usertar6 on
usertar6.Legacy_Tritech_Id__c=demo.Additional_Attendee_6__c

--Fetching Additional_Attendee_6_Name ( Legacy User Lookup)
left join Tritech_PROD.dbo.[User] user6 on
user6.Id=demo.Additional_Attendee_6__c


--Fetching CreatedById (User Lookup)
left join Superion_FULLSB.dbo.[User] crtdbyusr on
crtdbyusr.Legacy_Tritech_Id__c =demo.CreatedById
; 

--------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.Demo_Request__c;--1521

Select count(*) from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_Preload;--1516

Select * from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_Preload;

Select Legacy_Record_ID__c,count(*) from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_Preload
group by Legacy_Record_ID__c
having count(*)>1;--0

--Drop table Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_load;

Select * into 
Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_load
from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_Preload;

Select * from Staging_SB.dbo.Sales_Request__c_Tritech_SFDC_DemoRequest_load;--1516

--Exec SF_ColCompare 'Insert','SL_Superion_FullSB', 'Sales_Request__c_Tritech_SFDC_DemoRequest_load' 

/*
Salesforce object Sales_Request__c does not contain column Additional_Agency_1__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Agency_2__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Agency_3__c_orig
Salesforce object Sales_Request__c does not contain column Attendee_s__c  --Hidden
Salesforce object Sales_Request__c does not contain column Additional_Attendee_1__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_1__c_SFID
Salesforce object Sales_Request__c does not contain column Additional_Attendee_1_Role__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_2__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_2__c_SFID
Salesforce object Sales_Request__c does not contain column Additional_Attendee_2_Role__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_3__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_3__c_SFID
Salesforce object Sales_Request__c does not contain column Additional_Attendee_3_Role__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_4__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_4__c_SFID
Salesforce object Sales_Request__c does not contain column Additional_Attendee_4_Role__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_5__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_5__c_SFID
Salesforce object Sales_Request__c does not contain column Additional_Attendee_5_Role__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_6__c_orig
Salesforce object Sales_Request__c does not contain column Additional_Attendee_6__c_SFID
Salesforce object Sales_Request__c does not contain column Additional_Attendee_6_Role__c_orig
Salesforce object Sales_Request__c does not contain column CreatedById_orig
Salesforce object Sales_Request__c does not contain column Demo_Dates__c  --Field Not Found
Salesforce object Sales_Request__c does not contain column Demo_Start_Time_Zone__c  --Field Not Found
Salesforce object Sales_Request__c does not contain column Opportunity__c_orig
Salesforce object Sales_Request__c does not contain column RecordTypeId_orig
Salesforce object Sales_Request__c does not contain column Room_Description__c_orig
Salesforce object Sales_Request__c does not contain column Sharpies_Needed__c  --Hidden
Salesforce object Sales_Request__c does not contain column Requestor__c_orig
Salesforce object Sales_Request__c does not contain column Will_AV_Company_Setup__c  --Field Not Found
*/

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_load'

--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'
;--

--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.';--

select  Demo_Booklets_Quantity__c 
--update a set Demo_Booklets_Quantity__c =NULL
from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors a
where error like'%double%';--


Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors ;--1373

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors'

Select *
--delete 
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'; --

--insert into Sales_Request__c_Tritech_SFDC_DemoRequest_load
Select *
from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors;--

-------------------------------------------------------------------------------------
--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors2
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors2
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'
;--

--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp2
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp2
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.';--

Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors2 ;--

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors2'

Select *
--delete 
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'; --

--insert into Sales_Request__c_Tritech_SFDC_DemoRequest_load
Select *
from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors2;--

------------------------------------------------------------------------------------
--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors3
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors3
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'
;--

--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp3
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp3
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.';--

Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors3 ;--

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors3'

Select *
--delete 
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'; --

--insert into Sales_Request__c_Tritech_SFDC_DemoRequest_load
Select *
from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors3;--

--------------------------------------------------------------------------------
--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors4
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors4
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'
;--

--drop table Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp4
select * 
into Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors_bkp4
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.';--

Select * from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors4 ;--

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors4'

Select *
--delete 
from Sales_Request__c_Tritech_SFDC_DemoRequest_load
where error<>'Operation Successful.'; --

--insert into Sales_Request__c_Tritech_SFDC_DemoRequest_load
Select *
from Sales_Request__c_Tritech_SFDC_DemoRequest_load_errors4;--

-----------------------------------------------------------------------------------------

