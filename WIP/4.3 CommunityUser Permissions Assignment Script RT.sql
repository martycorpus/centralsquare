/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: CommunityUser Permission Migration Script - Source System to SFDC
DEVELOPER	: RTECSON
CREATED DT  : 02/06/2019
DETAIL		: 	
										
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/06/2019		Ron Tecson			Initial. Copied and modified from 4.3 CommunityUser Permissions Assignment Script PB.sql
02/07/2019		Ron Tecson			Corrected the refreshes of the local database.


DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE Superion_FULLSB
Exec SF_Refresh 'RT_Superion_FULLSB', 'Profile', 'yes'
Exec SF_Refresh 'RT_Superion_FULLSB', 'Contact', 'yes'
Exec SF_Refresh 'RT_Superion_FULLSB', 'User', 'yes'
Exec SF_Refresh 'RT_Superion_FullSB', 'PermissionSet', 'yes';
Exec SF_Refresh 'RT_Superion_FullSB', 'PermissionSetAssignment', 'yes';

USE Tritech_PROD
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Case', 'yes'


---------------------------------------------------------------------------------
-- DROP STAGING TABLE IF EXISTENT 
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='PermissionSetAssignment_PreLoad_RT') 
	DROP TABLE Staging_SB.dbo.[PermissionSetAssignment_PreLoad_RT];


---------------------------------------------------------------------------------
-- CREATE PERMISSION SET ASSIGNMENT STAGING TABLE 
---------------------------------------------------------------------------------

SELECT
	CAST('' as nvarchar(18)) as ID,
	CAST('' as nvarchar(255)) as Error,
	a.Legacy_Tritech_ID__c as AssigneeID_Original,
	a.ID as AssigneeID,
	SrcProf.[Name] as PermissionSetID_original,
	Perm.ID as PermissionSetID
	INTO PermissionSetAssignment_PreLoad_RT
	FROM Superion_FullSB.dbo.[User] a
	LEFT OUTER JOIN TriTech_Prod.dbo.[user] SrcUser ON a.Legacy_Tritech_Id__c = SrcUser.ID																							----> Source User
	LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] SrcProf ON SrcUser.ProfileId = SrcProf.ID																							----> Source Profile
	LEFT OUTER JOIN Superion_FULLSB.dbo.[PermissionSet] Perm ON Perm.[Name] =																										----> Permission Set
																			CASE SrcProf.[Name] WHEN 'TriTech Portal Read Only with Tickets' THEN 'Community_Case_Access_Readonly'
																								WHEN 'TriTech Portal Standard User' THEN 'Community_Case_Access'
																								WHEN 'TriTech Portal Manager' THEN 'Community_Delegated_Admin_Dedicated'
																								ELSE '' END
	WHERE migrated_record__c = 'true'
	and SrcProf.[Name] in ('TriTech Portal Read Only with Tickets', 'TriTech Portal Standard User', 'TriTech Portal Manager')

---------------------------------------------------------------------------------
-- Set related community users to 'Active'
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='USER_Update_toActive_RT') 
	DROP TABLE Staging_SB.dbo.[USER_Update_toActive_RT];

Select AssigneeID as ID,
CAST('' as nvarchar(255)) as error,
'true' as isActive
INTO Staging_SB.dbo.USER_Update_toActive_RT
From Staging_SB.dbo.PermissionSetAssignment_PreLoad_RT

Alter table Staging_SB.dbo.USER_Update_toActive_RT
Add [Sort] int identity (1,1)

USE Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(800)', 'RT_Superion_FULLSB', 'USER_Update_toActive_RT'

--Select * from User_Update_toActive_PB where error not like '%success%'

---------------------------------------------------------------------------------
-- Drop Load Table if it exists
---------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='PermissionSetAssignment_Load_RT') 
	DROP TABLE Staging_SB.dbo.PermissionSetAssignment_Load_RT;

SELECT *
INTO Staging_SB.dbo.PermissionSetAssignment_Load_RT
FROM PermissionSetAssignment_PreLoad_RT

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE Staging_SB.dbo.PermissionSetAssignment_Load_RT
ADD [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB

EXEC SF_BulkOps 'insert:bulkapi,batchsize(800)', 'RT_Superion_FULLSB', 'PermissionSetAssignment_Load_RT';

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

SELECT ERROR, count(*) 
FROM PermissionSetAssignment_Load_RT
GROUP BY ERROR

SELECT * FROM  PermissionSetAssignment_Load_RT WHERE error NOT LIKE '%success%'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE Superion_FULLSB

Exec SF_Refresh 'RT_Superion_FullSB', 'PermissionSetAssignment', 'yes';

---------------------------------------------------------------------------------
-- Set related community users to 'inActive'
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='USER_Update_toInActive_RT') 
	DROP TABLE Staging_SB.dbo.[USER_Update_toInActive_RT];

Select AssigneeID as ID,
CAST('' as nvarchar(255)) as error,
'false' as isActive
INTO Staging_SB.dbo.USER_Update_toInActive_RT
From Staging_SB.dbo.PermissionSetAssignment_Load_RT

Alter table Staging_SB.dbo.User_Update_toInActive_RT
Add [Sort] int identity (1,1)

USE Staging_SB
Exec SF_BulkOps 'update:bulkapi,batchsize(800)', 'RT_Superion_FULLSB', 'User_Update_toInActive_RT'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE Superion_FULLSB

Exec SF_Refresh 'RT_Superion_FullSB', 'User', 'yes';

--Select * from User_Update_toActive_PB where error not like '%success%'