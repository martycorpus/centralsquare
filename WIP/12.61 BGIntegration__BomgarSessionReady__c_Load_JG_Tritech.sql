/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__BomgarSessionReady__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'SL_Tritech_PROD','BGIntegration__BomgarSessionReady__c'

Use Superion_FULLSB
EXEC SF_Refresh 'SL_Superion_FULLSB','User','Yes'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__BomgarSessionReady__c','Yes'
*/ 

Use Staging_SB;

--Drop table BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload;

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__CustomerExitSurveyReady__c  =  BGIntegration__CustomerExitSurveyReady__c
,BGIntegration__LSID__c                     =  BGIntegration__LSID__c
,BGIntegration__RepExitSurveyReady__c       =  BGIntegration__RepExitSurveyReady__c
,BGIntegration__SessionReady__c             =  BGIntegration__SessionReady__c

,CreatedById_orig                           =  bg.CreatedById
,CreatedById                                =  createdbyuser.Id

,CreatedDate                                =  bg.CreatedDate
--,Legacy_ID__c                               =  Id
,Name                                       =  bg.[Name]

,OwnerId_orig                               =  bg.OwnerId
,OwnerId                                    =  owneruser.Id

 into BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__BomgarSessionReady__c bg 

--Fetching CreatedById(UserLookup)
left join Superion_FULLSB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching OwnerId(UserLookup)
left join Superion_FULLSB.dbo.[User] owneruser 
on owneruser.Legacy_Tritech_Id__c=bg.OwnerId

;--(108315 row(s) affected)

----------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__BomgarSessionReady__c;--108315

Select count(*) from BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload;--108315

Select Legacy_Id__c,count(*) from Staging_SB.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Load
from BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload; --(108315 row(s) affected)

Select * from Staging_SB.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Load;


--Exec SF_ColCompare 'Insert','SL_Superion_FullSB', 'BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column CreatedById_orig
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column OwnerId_orig
*/

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Load'

----------------------------------------------------------------------------------------
