/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		:  Script
DEVELOPER	: RTECSON, MCORPUS, and PBOWEN
CREATED DT  : 02/14/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/14/2019		Ron Tecson			Create Environment records for Non-Zuercher products.
02/19/2019		Ron Tecson			Execute a delta load (11 Records). Modified the main SQL since Marty provided a new script.
02/26/2019		Ron Tecson			Wipe and reload.
03/04/2019		Ron Tecson			Wipe and Reload
03/12/2019		Ron Tecson			Wipe and Reload.
03/15/2019		Ron Tecson			Wipe and Reload.
03/26/2019		Ron Tecson			Wipe and Reload.

DECISIONS:

Pre-requisite:
	- Need to run the SPV Process to populate the load table SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD.

******************************************************************************************************************************************************/


---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

Use Superion_FULLSB
Exec SF_Refresh 'RT_SUPERION_FULLSB', 'Account', 'subset';

---------------------------------------------------------------------------------
-- Drop Environment__c_PreLoad_Non_Zuercher_RT
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_Non_Zuercher_RT') 
DROP TABLE Staging_SB.dbo.Environment__c_PreLoad_Non_Zuercher_RT;

select distinct 
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		RegProd_ProductGroup, 
		RegProd_AccountId, 
		SUBSTRING(CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + regprod_productgroup + '-' + a.name,1,80) AS Name,
		RegProd_AccountId as Account__c,
		'Automatically created because no Hardware Software records existed in Tritech source'  as description__c,
              CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production'
                    WHEN EnvironmentType = 'Train' Then 'Training'
                    WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c,
		a.Name as AccountName,
		'Yes' AS Active__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
INTO Staging_SB.dbo.Environment__c_PreLoad_Non_Zuercher_RT
FROM SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD s
	JOIN superion_fullsb.dbo.ACCOUNT a ON a.id = s.regprod_accountid
WHERE  environment__c = 'ID NOT FOUND'

-- 03/12 -- (208 rows affected)
-- 03/15 -- (1098 rows affected)
-- 03/26 -- (1388 rows affected)

---------------------------------------------------------------------------------
-- Drop Environment__c_Load_Non_Zuercher_RT
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_Load_Non_Zuercher_RT') 
DROP TABLE Staging_SB.dbo.Environment__c_Load_Non_Zuercher_RT;

select * INTO Staging_SB.dbo.Environment__c_Load_Non_Zuercher_RT
from Staging_SB.dbo.Environment__c_PreLoad_Non_Zuercher_RT

EXEC SF_ColCompare 'INSERT','RT_Superion_FULLSB','Environment__c_Load_Non_Zuercher_RT'

/************************************** ColCompare L O G **************************************
--- Starting SF_ColCompare V3.6.9
Problems found with Environment__c_Load_Non_Zuercher_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 27]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment__c does not contain column RegProd_ProductGroup
Salesforce object Environment__c does not contain column RegProd_AccountId
Salesforce object Environment__c does not contain column AccountName
Salesforce object Environment__c does not contain column LegacySystemIds
Salesforce object Environment__c does not contain column Winning_RecordType
Salesforce object Environment__c does not contain column RecordTypes
************************************** ColCompare L O G **************************************/

EXEC sf_bulkops 'INSERT','RT_Superion_FULLSB','Environment__c_Load_Non_Zuercher_RT'

/************************************** INSERT L O G **************************************

03/26/2019
--- Starting SF_BulkOps for Environment__c_Load_Non_Zuercher_RT V3.6.9
16:01:38: Run the DBAmp.exe program.
16:01:38: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
16:01:38: Inserting Environment__c_Load_Non_Zuercher_RT (SQL01 / Staging_SB).
16:01:38: DBAmp is using the SQL Native Client.
16:01:39: SOAP Headers: 
16:01:39: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the Environment__c object.
16:01:39: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the Environment__c object.
16:01:39: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
16:01:39: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
16:01:39: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
16:01:39: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
16:01:49: 1388 rows read from SQL Table.
16:01:49: 1388 rows successfully processed.
16:01:49: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

03/15/2019
--- Starting SF_BulkOps for Environment__c_Load_Non_Zuercher_RT V3.6.9
17:50:13: Run the DBAmp.exe program.
17:50:13: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:50:13: Inserting Environment__c_Load_Non_Zuercher_RT (SQL01 / Staging_SB).
17:50:14: DBAmp is using the SQL Native Client.
17:50:14: SOAP Headers: 
17:50:14: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the Environment__c object.
17:50:14: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the Environment__c object.
17:50:14: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
17:50:14: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
17:50:14: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
17:50:14: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:50:21: 1098 rows read from SQL Table.
17:50:21: 1 rows failed. See Error column of row for more information.
17:50:21: 1097 rows successfully processed.
17:50:21: Errors occurred. See Error column of row for more information.
17:50:21: Percent Failed = 0.100.
17:50:21: Error: DBAmp.exe was unsuccessful.
17:50:21: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Environment__c_Load_Non_Zuercher_RT "SQL01"  "Staging_SB"  "RT_Superion_FULLSB"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 92]
SF_BulkOps Error: 17:50:13: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC17:50:13: Inserting Environment__c_Load_Non_Zuercher_RT (SQL01 / Staging_SB).17:50:14: DBAmp is using the SQL Native Client.17:50:14: SOAP Headers: 17:50:14: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the Environment__c object.17:50:14: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the Environment__c object.17:50:14: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.17:50:14: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.17:50:14: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.17:50:14: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.17:50:21: 1098 rows read from SQL Table.17:50:21: 1 rows failed. See Error column of row for more information.17:50:21: 1097 rows successfully processed.17:50:21: Errors occurred. See Error column of row for more information.



03/12/2019

--- Starting SF_BulkOps for Environment__c_Load_Non_Zuercher_RT V3.6.9
17:59:10: Run the DBAmp.exe program.
17:59:10: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:59:10: Inserting Environment__c_Load_Non_Zuercher_RT (SQL01 / Staging_SB).
17:59:10: DBAmp is using the SQL Native Client.
17:59:10: SOAP Headers: 
17:59:11: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the Environment__c object.
17:59:11: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the Environment__c object.
17:59:11: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
17:59:11: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
17:59:11: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
17:59:11: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
17:59:12: 208 rows read from SQL Table.
17:59:12: 208 rows successfully processed.
17:59:12: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

************************************** INSERT L O G **************************************/

------------------------------------------------------------------------------------
-- VALIDATION
------------------------------------------------------------------------------------

select error, count(*) from Environment__c_Load_Non_Zuercher_RT
group by error

select LEN(Name), * from Environment__c_Load_Non_Zuercher_RT where error <> 'Operation Successful.'

select * from System_Product_Version__c_Insert_WebPortal_MC
where error <> 'Operation Successful.'

SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD

select * from Environment__c_Load_Non_Zuercher_RT

--*****************************************************************************************************

--create environments
SELECT DISTINCT 
	regprod_productgroup, --2/19
    regprod_accountid,
    CASE 
		WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD'
		ELSE Upper(environmenttype)
	END + '-' + regprod_productgroup + '-' + a.name AS Name,
    regprod_accountid AS account__c,
    'Automatically created because no Hardware Software records existed in Tritech source' AS description__c,
    CASE 
		WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production'
        WHEN EnvironmentType = 'Train' Then 'Training'
        WHEN EnvironmentType = 'Test' Then 'Test' 
	END as Type__c
FROM   System_Product_Version__c_Insert_MC_load s
      join superion_fullsb.dbo.ACCOUNT a
        ON a.id = s.regprod_accountid
WHERE  environment__c = 'ID NOT FOUND'

-- Delta Feb 19 Load
select distinct 
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		RegProd_ProductGroup, 
		RegProd_AccountId, 
		CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + regprod_productgroup + '-' + a.name AS Name,
		RegProd_AccountId as Account__c,
		'Automatically created because no Hardware Software records existed in Tritech source'  as description__c,
              CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production'
                    WHEN EnvironmentType = 'Train' Then 'Training'
                    WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c,
		a.Name as AccountName,
		'Yes' AS Active__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
INTO Staging_SB.dbo.Environment__c_PreLoad_Non_Zuercher_RT_feb19debug
FROM   System_Product_Version__c_Insert_MC_load_feb19debug s
     join superion_fullsb.dbo.ACCOUNT a
       ON a.id = s.regprod_accountid
WHERE  environment__c = 'ID NOT FOUND'

EXEC sf_bulkops 'INSERT','RT_Superion_FULLSB','Environment__c_PreLoad_Non_Zuercher_RT_feb19debug'