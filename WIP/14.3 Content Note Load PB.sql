-------------------------------------------------------------------------------
--- Content Note Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 25, 2018
--- Last Updated: November 25, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'Note', 'yes' ;
Exec SF_Refresh 'PB_Tritech_Prod', 'Demo_Request__c', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'Win_Strategy__c', 'yes';

Use Superion_FUllSB
Exec SF_Refresh 'PB_Superion_FullSB', 'User', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Account', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Contact', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Opportunity', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Environment__c', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'Sales_Request__c', 'yes';
Exec SF_Refresh 'PB_Superion_FullSB', 'EngineeringIssue__c', 'yes';


---------------------------------------------------------------------------------
-- Drop Map_NoteParentID_PB
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Map_NoteParentID') 
DROP TABLE Staging_SB.dbo.Map_NoteParentID;


---------------------------------------------------------------------------------
-- Load Map_NoteParentID
---------------------------------------------------------------------------------
Use Staging_SB;

Select * 
INTO MAP_NoteParentID
From
(	
	-- Account
	select LegacySFDCAccountID__c as LegacyID, ID from Superion_FULLSB.dbo.Account where LegacySFDCAccountID__c is not null
	UNION
	-- Contact
	Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Contact where Legacy_ID__c is not null
	UNION
	-- Opportunity
	Select Legacy_Opportunity_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Opportunity where Legacy_Opportunity_ID__c is not null
	--UNION
	-- Environment
	--select LegacyIDValue__c as LegacyID, ID from Environment__c  -- validate and uncomment once object is coded.
	UNION
	-- Engineering Issue
	Select Legacy_CRMId__c as LegacyID, ID from Superion_FULLSB.dbo.EngineeringIssue__c where Legacy_CRMId__c is not null
	UNION
	-- Demo Request
	Select a.ID as LegacyID, Oppty.ID as ID from Tritech_PROD.dbo.Demo_Request__c a
	INNER JOIN Superion_FULLSB.dbo.Opportunity Oppty ON a.Opportunity__c = Oppty.Legacy_Opportunity_ID__c
	UNION
	-- Win Strategy
	Select a.ID as LegacyID, Oppty.ID from TriTech_Prod.dbo.Win_Strategy__c a
	INNER JOIN Superion_FULLSB.dbo.Opportunity Oppty on a.Opportunity__c = Oppty.Legacy_Opportunity_ID__c
	UNION
	-- Sales_Request__c
	Select Legacy_Record_Id__c as LegacyID, ID from Superion_FULLSB.dbo.Sales_Request__c where legacy_Record_id__c is not null
)t



---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ContentNote_Load_PB') 
DROP TABLE Staging_SB.dbo.ContentNote_Load_PB;


---------------------------------------------------------------------------------
-- Load ContentNote Staging Table
---------------------------------------------------------------------------------
Use Staging_SB;

Select 
		CAST('' AS nchar(18)) as [ID], 
		CAST('' as nvarchar(255)) as Error, 
		a.ID as LegacyID_Original,
		Concat(CAST(a.title as varchar(230)), ':', a.ID) as Title,
		CAST(dbo.EscapeContentNote(isNull([Body], '')) as varbinary(max)) as Content,
		a.[CreatedDate] as CreatedDate,
		a.OwnerID as OwnerID_Orig,
		isNull([OwnerID].ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as OwnerID,
		a.CreatedById as CreatedBy_Orig,
		isNull([CreatedBy].ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as CreatedByID
		INTO Staging_SB.dbo.ContentNote_Load_PB
		From TriTech_Prod.dbo.Note a
		-- eliminates records that were not migrated to new system based on ParentID
		INNER JOIN Staging_SB.dbo.MAP_NoteParentID map ON a.ParentID = map.LegacyID
		-- OwnerID
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [OwnerID] ON a.OwnerId = OwnerID.Legacy_Tritech_Id__c
		-- CreatedBy
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [CreatedBy] ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c 


--Delete PB
--	From ContentNote_Load_PB PB
--	Inner Join ContentNote_Load_PB1 PB1 on PB.LegacyID_Original = PB1.LegacyID_Original
--	where PB1.error like '%success%'


--------------------------------------------------------------------------------
-- Upload ContentNote
--------------------------------------------------------------------------------
Exec sf_bulkops 'Insert:batchsize(50)','PB_Superion_FULLSB','ContentNote_Load_PB'

--Select * from ContentNote_Load where error not like '%Success%'

--Select * 
--INTO ContentNote_Load_PB
--FROM
--(
--	Select * from ContentNote_Load_PB1 where error like '%success%'
--	UNION
--	Select * from ContentNote_Load_PB2 where error like '%success%'
--)t




--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Content Document Link
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- Drop Document Link Staging Table
--------------------------------------------------------------------------------

Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ContentDocumentLink_LoadNotes_PB') 
DROP TABLE Staging_SB.dbo.ContentDocumentLink_LoadNotes_PB;


--Delete PB
--	From ContentDocumentLink_LoadNotes_PB PB
--	Inner Join ContentDocumentLink_LoadNotes_PB1 PB1 on PB.ContentDocumentID = PB1.ContentDocumentID
--				AND PB.LinkedEntityID = PB1.LinkedEntityID
--	where PB1.error like '%success%'

--------------------------------------------------------------------------------
-- Create Content Document Link Staging Table
--------------------------------------------------------------------------------

Select 
	CAST('' as nvarchar(18)) as ID,
	CAST('' as nvarchar(255)) as error,
	a.ID as ContentDocumentID,
	Note.ParentID as ParentID_Original,
	map.ID as LinkedEntityID,
	'I' as ShareType,
	'AllUsers' as Visibility
	INTO ContentDocumentLink_LoadNotes_PB
	From ContentNote_Load_PB a
	LEFT OUTER JOIN Tritech_PROD.dbo.Note Note ON a.LegacyID_Original = Note.ID
	LEFT OUTER JOIN Staging_sb.dbo.MAP_NoteParentID map ON Note.ParentID = map.LegacyID


--------------------------------------------------------------------------------
-- Upload ContentNote
--------------------------------------------------------------------------------
Exec sf_bulkops 'Insert','PB_Superion_FULLSB','ContentDocumentLink_LoadNotes_PB'

--Select * from ContentDocumentLink_LoadNotes_PB where error not like '%Success%'



--------------------------------------------------------------------------------
-- Strip Title
--------------------------------------------------------------------------------

-- This statement is to strip the last 18 characters from the Title field, which is holding the legacy id.
Select 
ID,
Cast('' as nvarchar(255)) as error,
substring(Title, 1, len(Title) - 19) as Title
Into ContentNote_Update_PB
from ContentNote_load_PB

Exec sf_bulkops 'Update','PB_Superion_FULLSB','ContentNote_Update_PB'

--Select * from ContentNote_Update_PB where error not like '%success%'





