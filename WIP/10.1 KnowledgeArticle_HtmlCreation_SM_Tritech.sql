1. generate rich text area files first.
2. Create the file files 
3. Store them based on the the 3 Sectioning of Article types as not more than 5 mb can be loaded through import articles.
-- Article types 
--How_To__kav
--Tech_Advisory__kav
--Defect__kav
--Video__kav
--Ticket_Solutions__kav
--FAQ__kav
--Tech_Tips__kav
--Release_Notes__kav
--User_Manuals__kav
step 1 :

use Staging_SB
go
-- drop table ArticleHtmlGeneration_Tritech_Preload
with ArticlehtmlFile as(
select id,Description__c=cast(Article_Body_How_To__c as nvarchar(max)) ,Articletype
from Tritech_PROD.dbo.How_To__kav
union 
select id,Description__c=cast(Tech_Advisory_Article_Body__c as nvarchar(max)),Articletype
from Tritech_PROD.dbo.Tech_Advisory__kav
union 
select id,Description__c=cast(Article_body_Defects__c as nvarchar(max)),Articletype
from Tritech_PROD.dbo.Defect__kav
union 
select id
,Description__c= cast(concat(iif(Article_body_Video__c is null, null,Article_body_video__C ),CHAR(13)+CHAR(10),
									 iif(Video_link__c is null, null,'Video Link:'+Video_Link__C ))as nvarchar(max))
,Articletype
from Tritech_PROD.dbo.Video__kav 
union 
select id,Description__c=cast(Article_Body_Ticket_Solutions__c as nvarchar(max)) ,Articletype
from Tritech_PROD.dbo.Ticket_Solutions__kav
union 
select id
,Description__c=cast(concat(IIF(Question__C is null, null,concat('<u><b>QUESTION: </b></u><br>',Question__c)),CHAR(13)+CHAR(10), 
								IIF(Answer__c is null, null,concat('<u><b>ANSWER: </b></u><br>',Answer__c))) as nvarchar(max))
,Articletype
from Tritech_PROD.dbo.FAQ__kav
union 
select id,Description__c=cast(Article_Body_Tech_Tips__c as nvarchar(max)) ,Articletype
from Tritech_PROD.dbo.Tech_Tips__kav
union 
select id,Description__c=cast(Article_Body_Release_Notes__C as nvarchar(max)),Articletype 
from Tritech_PROD.dbo.Release_Notes__kav
union 
select id,Description__c=cast(Article_Body_User_Manuals__C as nvarchar(max)),Articletype
from Tritech_PROD.dbo.User_Manuals__kav
)
select *
--into ArticleHtmlGeneration_Tritech_Preload
from ArticlehtmlFile
-- (5654 row(s) affected)

-- for duplicate check.
select id ,count(*)
from ArticleHtmlGeneration_Tritech_Preload
group by id 
having count(*)>1

select articletype ,count(*)
from ArticleHtmlGeneration_Tritech_Preload
group by articletype;


-- drop table ArticleHtmlGeneration_Tritech_load
select 
id
,Description_orig = Description__c
,Description__c
,Articletype
into ArticleHtmlGeneration_Tritech_load
from ArticleHtmlGeneration_Tritech_Preload
--where articletype='Defect__kav';
-- (5654 row(s) affected)

-- select * from ArticleHtmlGeneration_Tritech_load

--alter table ArticleHtmlGeneration_Tritech_load add Rowno [smallint] identity (1, 1);


--EXEC master.dbo.sp_configure 'show advanced options', 1
--RECONFIGURE
--EXEC master.dbo.sp_configure 'xp_cmdshell', 1
--RECONFIGURE
DECLARE  @First				[smallint]
		,@Last				[smallint]
		,@DocumentNumber	[varchar](256)
		,@SQLCommand		[nvarchar](max)
		,@str				nvarchar(max)

SELECT @First = MIN([RowNo]) FROM Staging_sb.dbo.ArticleHtmlGeneration_Tritech_load
SELECT @Last =  MAX([RowNo]) FROM staging_sb.dbo.ArticleHtmlGeneration_Tritech_load
--SELECT @Last = 1

WHILE @First <= @Last
BEGIN
	SELECT @DocumentNumber = id FROM Staging_sb.dbo.ArticleHtmlGeneration_Tritech_load WHERE	[RowNo] = @First		
    SET @SQLCommand = 'EXEC xp_cmdshell ''bcp "SELECT Description__c FROM [Staging_sb].dbo.ArticleHtmlGeneration_Tritech_load WHERE [Id] = ''''' 
                    + @DocumentNumber + '''''" queryout "D:\Articles\' + @DocumentNumber + '.htm" -T -c -t,''' 
     

	
	--PRINT @SQLCommand
	EXEC (@SQLCommand)
	WAITFOR DELAY '00:00:05' 
	SET @First = @First + 1
END