/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Chatter Feed Item Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/08/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.3 Chatter FeedItem Migration PB.sql

DECISIONS:

******************************************************************************************************************************************************/

--------------------------------------------------------------------------------
-- Refresh Data 
--------------------------------------------------------------------------------
USE Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_Prod', 'FeedItem', 'yes';
EXEC SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';

USE Superion_FULLSB
EXEC SF_Refresh 'RT_Superion_FULLSB', 'Case', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'Opportunity', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'Contact', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'Account', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'CollaborationGroup', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'User', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'Task', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'EngineeringIssue__c', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'Event', 'yes';
EXEC SF_Refresh 'RT_Superion_FULLSB', 'Procurement_Activity__c', 'yes';
--Exec SF_Refresh 'PB_Superion_FULLSB', 'Write_In__c', 'yes';
--Exec SF_Refresh 'PB_Superion_FULLSB', 'Pricing_Request_Form__c', 'yes';


-------------------------------------------------------------------------------------
-- Create ChatterParent reference 
-----------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='map_ChatterParent') 
DROP TABLE Staging_SB.dbo.map_ChatterParent;

SELECT * INTO STAGING_SB.dbo.map_ChatterParent from
(
		SELECT Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.[Case] where legacy_id__c is not null								----> Case
		UNION
		SELECT Legacy_Opportunity_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Opportunity where Legacy_Opportunity_ID__c is not null ----> Opportunity
		UNION
		SELECT Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Contact where Legacy_ID__c is not null								----> Contact
		UNION
		SELECT LegacySFDCAccountID__c as LegacyID, ID from Superion_FULLSB.dbo.Account where LegacySFDCAccountID__c is not null			----> Account
		UNION
		SELECT GroupSource.ID as LegacyID, GroupTarget.ID as ID from Tritech_PROD.dbo.CollaborationGroup GroupSource					----> Collaboration Group
				LEFT OUTER JOIN Superion_FULLSB.dbo.CollaborationGroup GroupTarget ON GroupSource.[Name] = GroupTarget.[Name]
				WHERE GroupTarget.ID is NOT NULL
		UNION
		SELECT Legacy_Tritech_Id__c as LegacyID, ID from Superion_FULLSB.dbo.[User] where Legacy_Tritech_Id__c is not null				----> User
		UNION
		---- Write_IN
		--Select Legacy_Id__c as LegacyID, ID from Superion_FULLSB.dbo.Write_In__c where legacy_id__c is not null  and Migrated_Record__c = 'true'
		--UNION
		SELECT Legacy_Id__c as LegacyID, ID from Superion_FULLSB.dbo.Task where Legacy_ID__c is not null								----> Task
		UNION
		SELECT Legacy_CRMId__c as LegacyID, ID from Superion_FULLSB.dbo.EngineeringIssue__c where Legacy_CRMId__c is not null			----> Engineering Issue
		UNION
		---- Pricing
		--Select Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Pricing_Request_Form__c where Legacy_ID__c is not null and Migrated_Record__c = 'true'
		--UNION
		SELECT a.ID as LegacyID, Oppty.ID as ID from Tritech_PROD.dbo.Demo_Request__c a
			INNER JOIN Superion_FULLSB.dbo.Opportunity Oppty ON a.Opportunity__c = Oppty.Legacy_Opportunity_ID__c						----> Demo Request
		UNION
		SELECT Legacy_ID__c as LegacyID, ID from Superion_FULLSB.dbo.Event where Legacy_ID__c is not null								----> Event
		UNION
		SELECT Legacy_ID__c as Legacyid, ID from Superion_FULLSB.dbo.Procurement_Activity__c where Legacy_id__c is not null				----> Procurement activity
)t

--Drop Index ChatterParent_idx on map_ChatterParent

CREATE INDEX ChatterParent_idx
ON [map_ChatterParent] (LegacyID) 


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedItem_PreLoad_RT') 
DROP TABLE Staging_SB.dbo.FeedItem_PreLoad_RT;


SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.Body, 
	a.CreatedByID as CreatedBy_Orig,
	isNull(CreatedBy.ID, (Select ID from Superion_FULLSB.dbo.[User] where [Name] = 'Superion API')) as CreatedById,
	a.CreatedDate as CreatedDate,
	a.LinkUrl as LinkURL,
	a.IsRichText as IsRichText,
	a.ParentID as Parent_Orig,
	Parent.Id as ParentID,
	--RelatedRecord.ID as RelatedRecordID,
	--ContentFileName as ContentFileName,
	--a.Revision as Revision,
	a.Title as Title,
	a.[Type] as [Type],
	a.ID as Legacy_ID__c
INTO FeedItem_PreLoad_RT
from TriTech_Prod.dbo.FeedItem a
LEFT OUTER JOIN Superion_FULLSB.dbo.[User] CreatedBy ON a.CreatedById = CreatedBy.Legacy_Tritech_Id__c			---CreatedBy
INNER JOIN Staging_SB.dbo.map_ChatterParent Parent ON a.ParentID = Parent.LegacyID								---Parent
WHERE 
	a.[Type] in ('LinkPost', 'TextPost')



---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='FeedItem_Load_RT') 
DROP TABLE Staging_SB.dbo.FeedItem_Load_RT;

SELECT *
INTO Staging_SB.dbo.FeedItem_Load_RT
FROM Staging_SB.dbo.FeedItem_PreLoad_RT

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_SB.dbo.FeedItem_Load_RT
Add [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_SB

EXEC SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'RT_Superion_FULLSB', 'FeedItem_Load_RT'

-------------------------------
--Validation
-------------------------------

SELECT ERROR, count(*) 
FROM FeedItem_Load_RT
GROUP BY ERROR

SELECT * FROM FeedItem_Load_RT WHERE error not like '%success%' and error not like '%task%'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------

USE Superion_FULLSB

EXEC SF_Refresh 'RT_Superion_FULLSB', 'FeedItem', 'yes';
