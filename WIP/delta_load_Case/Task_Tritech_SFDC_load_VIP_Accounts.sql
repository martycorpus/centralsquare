/*
Use Tritech_PROD
EXEC SF_RefreshIAD 'EN_TRITECH_PROD','task' --,'Yes' --30min
EXEC SF_Refresh 'EN_TRITECH_PROD','EmailMessage','yes'--24 mins


Use superion_maplesb
EXEC SF_Refresh 'sl_superion_maplesb ','Task','yes'
EXEC SF_Refresh 'sl_superion_maplesb ','Opportunity','yes'
EXEC SF_Refresh 'sl_superion_maplesb ','Contact','yes'
EXEC SF_Refresh 'sl_superion_maplesb ','Account','yes'
EXEC SF_Refresh 'sl_superion_maplesb ','Case','yes'
EXEC SF_Refresh 'sl_superion_maplesb ','Sales_Request__c','yes'
EXEC SF_Refresh 'sl_superion_maplesb ','Procurement_Activity__c','yes'
EXEC SF_Refresh 'sl_superion_maplesb ','Contact_Reference_Link__c','yes'
*/--10mins
-- select count(*) from tritech_prod.dbo.[Task]
--1694717


use staging_sb_mapleroots 
go
-- drop table Task_Tritech_SFDC_Preload_VIP_Account
 declare @defaultuser nvarchar(18)
 =(select  id from superion_maplesb.dbo.[user] where name = 'Superion API')
;With CteWhatData as
(
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Case' as What_parent_object_name 
from superion_maplesb.dbo.[case] a
inner join Case_tritech_SFDC_load_VIP_Account b 
on a.id=b.id 
where 1=1
and a.Legacy_ID__c is not null and a.migrated_record__C='true'
						) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from superion_maplesb.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null	 and migrated_record__C='true'			
 )   

SELECT  
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_task.Id
				,Legacy_Source__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_task.[Account_Name__c]
														  --,tr_task.[Account_Name_DE__c]
				,AccountId_orig							= tr_task.[AccountId]
				,AccountId								= Tar_ACcount.ID
				,Activity_Type_For_Reporting__c			= tr_task.[Activity_Type_For_Reporting__c]
				,ActivityDate							= tr_task.[ActivityDate]
														  --,tr_task.[Brand__c]
				,CallDisposition						= tr_task.[CallDisposition]
				,CallDurationInSeconds					= tr_task.[CallDurationInSeconds]
				,CallObject								= tr_task.[CallObject]
				,CallType								= tr_task.[CallType]
														  --,tr_task.[Completed_Date_Time__c]
														  --,tr_task.[Contact_Count__c]
				,CreatedById_orig						= tr_task.[CreatedById]
				,legacy_createdby_name=Legacyuser.name
				,CreatedById_target							= Target_Create.ID
				,CreatedById							= iif(Target_Create.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_task.[CreatedDate]
														 -- ,tr_task.[Customer_Risk_Level__c]
				--,Description_orig							= tr_task.[Description]
														  --,tr_task.[DummyTouchField__c]
				,Id_orig								= tr_task.[Id]
				,Isarchieved__orig = tr_task.[IsArchived]
				,IsClosed_orig=tr_task.[IsClosed]
			    ,IsDeleted_orig=    tr_task.[IsDeleted]
														  --,tr_task.[IsHighPriority]
				,IsRecurrence							= tr_task.[IsRecurrence]
				,IsReminderSet							= tr_task.[IsReminderSet]
														  --,tr_task.[LastModifiedById]
														  --,tr_task.[LastModifiedDate]
				,OwnerId_orig							= tr_task.[OwnerId]
				,legacy_owner_name=Legacyuser1.name
				,OwnerId_target							= Target_Owner.ID
				,OwnerId							    = iif(Target_owner.id is null,@defaultuser
																		--iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,Priority								= tr_task.[Priority]
				,RecurrenceActivityId_orig				= tr_task.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_task.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_task.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_task.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_task.[RecurrenceInstance]
				,RecurrenceInterval						= tr_task.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_task.[RecurrenceMonthOfYear]
				,RecurrenceRegeneratedType				= tr_task.[RecurrenceRegeneratedType]
				,RecurrenceStartDateOnly				= tr_task.[RecurrenceStartDateOnly]
				,RecurrenceTimeZoneSidKey				= tr_task.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_task.[RecurrenceType]
				,ReminderDateTime						= tr_task.[ReminderDateTime]
				,Resolution_Notes__c					= tr_task.[Resolution_Notes__c]
														  --,tr_task.[Solution_s__c]
				,Status									= tr_task.[Status]
				,Subject								= tr_task.[Subject]
														  --,tr_task.[SystemModstamp]
				--,Description							= iif(tr_task.[Task_Notes__c] is not null,concat(tr_task.[Createddate],tr_task.[Task_Notes__c]),null)
				,TaskSubtype							= tr_task.[TaskSubtype]
														 -- ,tr_task.[Time_Elapsed__c]
				,Type									= tr_task.[Type]
														  --,tr_task.[WhatCount]
				,whatId_orig							= tr_task.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
				    
														  --,tr_task.[WhoCount]
				,WhoID_orig								= tr_task.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_task.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_task.[Description]
				,Description_Comments_summary__C_orig = tr_task.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_task.[Task_Notes__c]
				--,[Description]  = concat( iif(tr_task.[Description] is not null,'Description::',''),tr_task.description,CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_task.createddate),''),tr_task.[Comments_Summary__c],CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_task.createddate),''),tr_task.[Task_Notes__c]
				--						  )
				--,DESCRIPTION = IIF(tr_task.Description IS NOT NULL
				--							,IIF(tr_task.tASK_nOTES__c IS NOT NULL, 
				--								CONCAT(tr_task.DESCRIPTION,CHAR(13)+char(10),tr_task.CREATEDDATE,' ',tr_task.task_notes__c),tr_task.description),null)
				,DESCRIPTION=concat(isnull(tr_task.Description,''),
				iif(tr_task.Task_Notes__c IS NOT NULL,concat(CHAR(13)+char(10)+cast(tr_task.CREATEDDATE as nvarchar(40))+' ',
				cast(tr_task.Task_Notes__c as nvarchar(max))),''))
               -- ,tr_email_id=tr_email.id
			   ,superion_taskId= t.id
			   ,superion_legacyId = t.legacy_id__c
			   ,superion_whatid = t.whatid
			   ,superion_whoid = t.whoid
							into Task_Tritech_SFDC_Preload_VIP_Account
  FROM [Tritech_PROD].[dbo].[Task] tr_task
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_task.WhatId
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_task.WhoId
  -----------------------------------------------------------------------------------------------------------------------
    left join superion_maplesb.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_task.AccountId
--and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join superion_maplesb.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_task.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join superion_maplesb.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_task.OwnerID
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_task.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_task.OwnerId
    -----------------------------------------------------------------------------------------------------------------------------
	left join SUPERION_MAPLESB.dbo.task t 
	on t.Legacy_Id__c = tr_task.id
	----------------------------------
   where 1=1
  and  tr_task.IsDeleted='False' and Legacyuser1.name <>'Act-On Sync'
  and (Wt.parent_id IS NOT NULL)
  and (tr_task.IsArchived='false' or Wt.parent_id like '500%')
 ;

  --(3155 row(s) affected)

 -- COUNT OF THE TASK RECORDS
select count(*) 
from Task_Tritech_SFDC_Preload_VIP_Account
--   3155 row(s) affected)

select count(*) from tritech_prod.dbo.task;--1694717



-- CHECK FOR THE DUPLICATES.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_Preload_VIP_Account
group by Legacy_ID__c
having count(*)>1;--0
--Insert
--drop table Task_Tritech_SFDC_load_VIP_Account
select * 
into Task_Tritech_SFDC_load_VIP_Account
from Task_Tritech_SFDC_Preload_VIP_Account a 

where 1=1
 and (whoid is not null or whatid is not null) and a.superion_taskId is null
 
 --(3154 row(s) affected)

--Update

--drop table Task_Tritech_SFDC_load_VIP_Account_update
select a.superion_taskId as id , a.error, a.whatid 
,a.superion_whatid as Previous_whatid
,a.superion_whoid as Previous_whoid
,a.whoid as current_whoid
,a.Legacy_id__c as current_legacy_id__c

into Task_Tritech_SFDC_load_VIP_Account_update
from Task_Tritech_SFDC_Preload_VIP_Account a 

where 1=1
 and (whoid is not null or whatid is not null) and a.superion_taskId is not null
 
 --(1 row(s) affected)

select * 
from Task_Tritech_SFDC_load_VIP_Account;

-- LOAD TABLE RECORD COUNT
select count(*) 
from Task_Tritech_SFDC_load_VIP_Account
--271967

-- check for the duplicates in the final load table.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_load_VIP_Account
group by Legacy_ID__c
having count(*)>1


select * from Task_Tritech_SFDC_load_VIP_Account;

--: Run batch program to create task
  
--exec SF_ColCompare 'Insert','sl_superion_maplesb ', 'Task_Tritech_SFDC_load_VIP_Account' 

--check column names
/*

Salesforce object Task does not contain column AccountId_orig
Salesforce object Task does not contain column CreatedById_orig
Salesforce object Task does not contain column legacy_createdby_name
Salesforce object Task does not contain column CreatedById_target
Salesforce object Task does not contain column Id_orig
Salesforce object Task does not contain column Isarchieved__orig
Salesforce object Task does not contain column IsClosed_orig
Salesforce object Task does not contain column IsDeleted_orig
Salesforce object Task does not contain column OwnerId_orig
Salesforce object Task does not contain column legacy_owner_name
Salesforce object Task does not contain column OwnerId_target
Salesforce object Task does not contain column RecurrenceActivityId_orig
Salesforce object Task does not contain column whatId_orig
Salesforce object Task does not contain column whatid_parent_object_name
Salesforce object Task does not contain column whatid_legacy_id_orig
Salesforce object Task does not contain column WhoID_orig
Salesforce object Task does not contain column whoId_parent_object_name
Salesforce object Task does not contain column whoId_legacy_id_orig
Salesforce object Task does not contain column Logged_Wellness_Check__c
Salesforce object Task does not contain column Description__orig
Salesforce object Task does not contain column Description_Comments_summary__C_orig
Salesforce object Task does not contain column Description_Task_notes__C_orig
Salesforce object Task does not contain column superion_taskId
Salesforce object Task does not contain column superion_legacyId
Salesforce object Task does not contain column superion_whatid
Salesforce object Task does not contain column superion_whoid
Column AccountId is not insertable into the salesforce object Task
*/

--exec SF_Bulkops 'Insert','sl_superion_maplesb ', 'Task_Tritech_SFDC_load_VIP_Account' 

--exec SF_Bulkops 'Update','sl_superion_maplesb ', 'Task_Tritech_SFDC_load_VIP_Account_update' 
