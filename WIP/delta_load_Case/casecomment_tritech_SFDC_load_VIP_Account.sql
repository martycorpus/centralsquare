

--use Tritech_Prod
--EXEC SF_Refresh 'EN_Tritech_PROD','CaseComment','yes'
-- execution time : 1 min 

select count(*) from Tritech_PROD.dbo.CaseComment
--2484746




use staging_sb_mapleroots 
go 
-- drop table feedItem_Tritech_SFDC_CaseComment_Preload_VIP_Account


declare @defaultuser nvarchar(18)
 select @defaultuser = id from superion_maplesb.dbo.[user] where name = 'Superion API'
SELECT 
         ID											 =   CAST(SPACE(18) as NVARCHAR(18))
		,ERROR										 =   CAST(SPACE(255) as NVARCHAR(255))
		,Body										 =	 Tr_Cmt.[CommentBody]
		,CreatedbyId_orig							 =	 Tr_Cmt.[CreatedById]
		,CreatedById								 =   iif(Target_Create.Id is null,@defaultuser,Target_Create.Id)
		,CreatedDate								 =	 Tr_Cmt.[CreatedDate]
		,Id_orig									 =   Tr_Cmt.[Id]
														  --,Tr_Cmt.[IsDeleted]
		,IsNotificationSelected						 = Tr_Cmt.[IsNotificationSelected]
		,Visibility								 = iif(Tr_Cmt.[IsPublished] ='True' ,'AllUsers','InternalUsers')
		,IsPublished_orig							=tr_Cmt.IsPublished
														  --,Tr_Cmt.[LastModifiedById]
														  --,Tr_Cmt.[LastModifiedDate]
				,Parentid_orig						=   Tr_Cmt.[ParentId]
				,Parentid							=   Target_Parent.ID
														  --,Tr_Cmt.[SystemModstamp]
				into feedItem_Tritech_SFDC_CaseComment_preload_VIP_Account
  FROM [Tritech_PROD].[dbo].[CaseComment] Tr_Cmt
  inner join staging_sb_mapleroots.dbo.case_Tritech_SFDC_load_VIP_Account Target_PArent
  on Target_Parent.Legacy_ID__c=Tr_cmt.ParentId
  left join superion_maplesb.dbo.[User] Target_Create
  on Target_create.Legacy_Tritech_Id__c=Tr_Cmt.CreatedById
  where 1=1 and Target_PArent.error='Operation Successful.'
--(20078 row(s) affected)
--



select count(*) from
feedItem_Tritech_SFDC_CaseComment_preload_VIP_Account

--952614

select id_orig, count(*)
from feedItem_Tritech_SFDC_CaseComment_preload_VIP_Account
group by id_orig 
having count(*)>1

-- drop table feedItem_Tritech_SFDC_CaseComment_load_VIP_Account
select * 
into feedITem_Tritech_SFDC_CaseComment_load_VIP_Account
from feedItem_Tritech_SFDC_CaseComment_preload_VIP_Account

--(20078 row(s) affected)

select * from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account

-- insert Casecomment records as Chatter comment.

-- EXEC SF_ColCompare 'Insert','sl_superion_maplesb ', 'feedITem_Tritech_SFDC_CaseComment_load_VIP_Account' 

-- EXEC SF_BulkOps 'Insert','sl_superion_maplesb ','feedITem_Tritech_SFDC_CaseComment_load_VIP_Account' 

/*
Salesforce object feedITem does not contain column CreatedbyId_orig
Salesforce object feedITem does not contain column Id_orig
Salesforce object feedITem does not contain column IsNotificationSelected
Salesforce object feedITem does not contain column IsPublished_orig
Salesforce object feedITem does not contain column Parentid_orig
*/

select error, count(*)
from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account
group by error 

select * from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account 
where error <>'Operation Successful.'

select error, count(*)
from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account
group by error 

-- drop table feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors
select * 
into feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors
from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account
where error<>'Operation Successful.' and error<>'Required fields are missing: [Body]'

-- drop table feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors_bkp
select * 
into feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors_bkp
from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account
where error<>'Operation Successful.' and error<>'Required fields are missing: [Body]'

-- EXEC SF_BulkOps 'Insert:batchsize(1)','sl_superion_maplesb ','feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors' 


-- drop table feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors1
select * 
into feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors1
from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors
where error<>'Operation Successful.'

-- EXEC SF_BulkOps 'Insert:batchsize(1)','sl_superion_maplesb ','feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors1'


select * 
-- delete 
from  feedITem_Tritech_SFDC_CaseComment_load_VIP_Account 
where error<>'Operation Successful.' and error<>'Required fields are missing: [Body]'
--(200 row(s) affected)


--insert into feedITem_Tritech_SFDC_CaseComment_load_VIP_Account
select * from feedITem_Tritech_SFDC_CaseComment_load_VIP_Account_errors
--(200 row(s) affected)
