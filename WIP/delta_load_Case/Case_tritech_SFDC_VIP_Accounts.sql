
/* 
Script for the test of the VIP Accounts related Cases 
*/
/*
Use superion_maplesb
EXEC SF_Refresh 'sl_superion_mapleSb ','Account','yes'
EXEC SF_Refresh 'sl_superion_mapleSb ','User','yes'
EXEC SF_Refresh 'sl_superion_mapleSb ','Contact','yes'
EXEC SF_refresh 'sl_superion_mapleSb ','EngineeringIssue__c','yes'
EXEC SF_Refresh 'sl_superion_mapleSb ','ProblemCode__c','yes'
EXEC SF_Refresh 'sl_superion_mapleSb ','Product2','yes'
EXEC SF_Refresh 'sl_superion_mapleSb ','Version__c','yes'
EXEC SF_Refresh 'sl_superion_mapleSb ','Case','yes'


use Tritech_Prod
EXEC Sf_refresh 'EN_Tritech_PROD','Case','yes'
EXEC Sf_refresh 'EN_Tritech_PROD','Product2','yes'
EXEC Sf_refresh 'EN_Tritech_PROD','Product_Sub_Module__c','yes'
EXEC Sf_refresh 'EN_Tritech_PROD','user','yes'
EXEC Sf_refresh 'EN_Tritech_PROD','Potential_Defect__c','yes'
EXEC Sf_refresh 'EN_Tritech_PROD','BGIntegration__BomgarSession__c','yes'

*/

use staging_sb_mapleroots 
go

-- drop table Case_Tritech_SFDC_Preload_VIP_Account

--DECLARE @DefaultVersionID NVARCHAR(18) = (Select top 1 Id from superion_maplesb.dbo.Version__C where id='a9o0v0000004Qx1AAE');
declare @defaultuser nvarchar(18) = (select id from superion_maplesb.dbo.[user] where name = 'Superion API')
declare @supportrecordtypeid nvarchar(18) = (select id from superion_maplesb.dbo.RecordType where SobjectType='case' and name='Support')
SELECT 
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				--N/A										  ,[Account_Support_Team__c]
				,Legacy_id__c                            = tr_case.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
				,AccountID_orig							 = tr_case.[AccountId]
				,AccountID                               = iif(Tar_Account.id is null and tr_case.[AccountId] is not null,'Account Not Found',Tar_Account.id)
				,AccountName_tr = tr_act.name
															  --,tr_case.[Actual_Time_for_Upgrade_in_hours__c]
															  --,tr_case.[After_Hours_Upgrade__c]
				,Installation_start_Date__C				 = tr_case.[Anticipated_Delivery_Date__c]
															  --,tr_case.[Anticipated_Go_Live_Date_CAD__c]
															  --,tr_case.[Anticipated_Go_Live_Date_IQ__c]
															  --,tr_case.[Anticipated_Go_Live_Date_Jail__c]
															  --,tr_case.[Anticipated_Go_Live_Date_Mobile__c]
															  --,tr_case.[Anticipated_Go_Live_Date_RMS__c]
				--,Installation_Start_Date__c				=	tr_case.[Anticpated_Delivery_Time__c]
															  --,tr_case.[Applicable_Sales_Order_Amount__c]
															  --,tr_case.[Assigned_CAD_BA_Email__c]
															  --,tr_case.[Associated_JIRA__c]
															  --,tr_case.[BA_Services_Tasks__c]
															  --,tr_case.[Back_Out_Plan__c]
															  --,tr_case.[Bad_Debt__c]
															  --,tr_case.[Bad_Debt_Indicator__c]
															  --,tr_case.[Billable__c]
															  --,tr_case.[Billable_Hours__c]
				 ,Black_Box_Log_Collected__c			=	tr_case.[Black_Box_Log_Collected__c]
															  --,tr_case.[Blitz__c]
															  --,tr_case.[CAD_BA_Email__c]
				,Legacy_Number__c						=	tr_case.[CaseNumber]
															  --,tr_case.[CCB_MOL_Engineering_Ready__c]
															  --,tr_case.[CDP_Tier_2_Contact__c]
															  --,tr_case.[CDP_Tier_2_Group__c]
															  --,tr_case.[Choose_applicable_product__c]
															  --,tr_case.[Choose_applicable_product_family__c]
															  --,tr_case.[CIS_Rehost_Task__c]
															  --,tr_case.[CIS_Task_List__c]
		--,Client_Steps_to_Recreate_Issue__c_Description	=	tr_case.[Client_Steps_to_Recreate_Issue__c]
		,Closed_Incidents_Reason_WMP__c_orig = tr_case.Closed_Incidents_Reason_WMP__c
		,Closed_Incidents_Reason_WMP__c_target = picklistincident.[Target_Picklist_Mapping]
		,Closed_Reason__c_orig = tr_Case.Closed_Reason__c
		,Closed_Reason__c_target = picklistclosed.[Target_Picklist_Mapping]
		
				,ResolutionType__c							= CASE
																	WHEN rectype.name='Zuercher Customer Support Ticket' THEN picklistclosed.[Target_Picklist_Mapping]
																	WHEN rectype.name='Customer Support Ticket' THEN picklistincident.[Target_Picklist_Mapping]
																	ELSE NULL
															  END
															  --,tr_case.[Closed_Reason__c]
				,ClosedDate								=	tr_case.[ClosedDate]
				--,NextStep__c							= tr_case.[Comments]/*follow up needed*/
															  --,tr_case.[Contact_Extension__c]
															  --,tr_case.[Contact_Other_Phone__c]
															  --,tr_case.[Contact_Work_Mobile__c]
															  --,tr_case.[ContactEmail]
															  --,tr_case.[ContactFax]
				,Contactid_orig							=	tr_case.[ContactId]
				,Contactid								=   Tar_contact.Id
				,Contactid_sr							=   iif(Tar_contact.Id is null and tr_case.[ContactId] is not null,'Contact Not Found',Tar_Contact.id)
															  --,tr_case.[ContactMobile]
															  --,tr_case.[ContactPhone]
			    ,CreatedById_orig						=	tr_case.[CreatedById]
				,CreatedById_target							=   Tar_CreateID.ID
				,CreatedById_sp							= iif(Tar_CreateID.id is null, 
																		iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Tar_CreateID.id )
				,CreatedbyId							= iif(Tar_CreateId.id is null, @defaultuser, Tar_CreateID.id)

				,CreatedDate							=	tr_case.[CreatedDate]
			    ,SlaExitDate							=	tr_case.[Customer_Contact_Date_Time_WMP__c]
				--,[Resolution Notes]					=	tr_case.[Customer_Initiated_Closure_Notes__c]
				,Severity__c_Customer_Priority_WMP__c	=	tr_case.[Customer_Priority_WMP__c]
			--	,Severity__c_orig	                    =    tr_case.severtiy__c
				,sourceRecordtype                       =    rectype.name
			--	,Severity__c   = iif(rectype.name='Zuercher Customer Support Ticket' , tr_case.[Customer_Priority_WMP__c],tr_case.Severity__c) 
			    ,Severity__c  = case tr_case.[Customer_Priority_WMP__c]
									when 'P1 - System Down' then '1 - Urgent'
									when 'P2' then '2 - Critical'
									when 'P3' then '3 - Non Critical'
									when 'P4' then '4 - Minor'
									when 'P5' then '4 - Minor'
								end
				,External_Case_Reference__c				=	tr_case.[Customer_Reference_Number__c]
															  --,tr_case.[Customer_State__c]
															  --,tr_case.[Data_Transfer__c]
															  --,tr_case.[Days_over_SLA_High_Priority__c]
															  --,tr_case.[Days_over_SLA_Low_Priority__c]
															  --,tr_case.[Defect_Comments__c]
				,Defect_Number__c_case			=	tr_case.[Defect_Number__c]
				,Potential_Defect__c_case=tr_case.[Potential_Defect__c]
				,Potent_defect_number__orig					=   potent.Defect_Ticket_ID__c
				,Parent_Engineering_Issue__c				=   Target_Eng.Id
				,Potential_Defect_id_orig                   =   Potent.ID
															  --,tr_case.[Defect_Status__c]
				,VSOIssue__c							    =   tr_case.[Defect_TFS_Work_Item__c]
															  --,tr_case.[DefectNumber__c]
															  --,tr_case.[DefectStatus__c]
				,Description_ORIG							=	tr_case.[Description]
															  --,tr_case.[Due_Date_Time__c]
															  --,tr_case.[Email_Added_Date_Time_Update__c]
															  --,tr_case.[Email_Sender__c]
															  --,tr_case.[Email_Sent__c]
															  --,tr_case.[Email_Template__c]
															  --,tr_case.[Email_Thread__c]
															  --,tr_case.[EMS_Customer_Number__c]
															  --,tr_case.[Engineering_Intervention_Required__c]
															  --,tr_case.[Entered_CCB_Review__c]
															  --,tr_case.[Entered_Engineering_Defect__c]
															  --,tr_case.[Environment_Type__c]
				,Error_Message__c						=	tr_case.[Error_Message__c]
				,IsEscalated							=	tr_case.[Escalation__c]
															  --,tr_case.[Estimated_Back_Out_Duration_in_hours__c]
				,Estimated_Time_for_Upgrade_in_hours__c =	tr_case.[Estimated_Time_for_Upgrade_in_hours__c]
															  --,tr_case.[Event_Frequency__c]
															  --,tr_case.[External_ID__c]
															  --,tr_case.[FBR_BA_Email__c]
															  --,tr_case.[First_Call_Resolution__c]
				,Go_live_Blocker__c						=   tr_case.[Gating__c]
															  --,tr_case.[GIS_Internal_Work_Queue__c]
															  --,tr_case.[GIS_Progress_Notes__c]
															  --,tr_case.[GIS_Services_Requested_Due_Date__c]
															  --,tr_case.[GIS_Services_Work_Type__c]
															  --,tr_case.[HasCommentsUnreadByOwner]
															  --,tr_case.[HasSelfServiceComments]
				,ID_orig								=	tr_case.[Id]
															  --,tr_case.[Impact_Time_Difference_1__c]
															  --,tr_case.[Impact_Time_Difference_2__c]
															  --,tr_case.[Implementation_Plan__c]
															  --,tr_case.[Install_Name__c]
															  --,tr_case.[Integration_Products__c]
				,Internal_Ranking__C_orig				=	tr_Case.[Internal_Ranking__c]
				,Internal_Work_Activities__c_orig		=	tr_case.[Internal_Work_Activities__c]
				,nextstep__c=iif(tr_case.[Internal_Ranking__c] is not null,concat('Internal Ranking :',tr_case.[Internal_Ranking__c]),null)
				-- add new line and handle null 
				--,NextStep__c							=concat( iif(tr_case.[Internal_Ranking__c] is not null,'Internal Ranking : ',''),tr_case.[Internal_Ranking__c],CHAR(13)+CHAR(10),
				--											 iif(tr_case.[Internal_Work_Activities__c] is not null,'Internal Work Activities : ',''),tr_case.[Internal_Work_Activities__c],CHAR(13)+CHAR(10),
				--											  iif(tr_case.[Comments] is not null,'Internal Comments : ',''),tr_case.[Comments],CHAR(13)+CHAR(10),
				--											  iif(tr_case.[Upgrade_Note__c] is not null,'Upgrade Notes : s',''),tr_case.[Upgrade_Note__c])
															  --,tr_case.[IsClosed]
															  --,tr_case.[isClosed__c]
															  --,tr_case.[IsDeleted]
				--,External_Case_reference__c				=    tr_case.[Jira_DN__c]
				,Known_Issue_Confirmed__c_orig     =  tr_case.[Known_Issue_Confirmed__c]
				 ,KnownIssueStatus__c              = iif(tr_case.[Known_Issue_Confirmed__c] ='True','Approved',
				                                              iif(tr_case.Known_Issues_List__c='True','Submitted',null))
				,KnownIssueDescription__c					= iif(tr_case.[Known_Issue_Confirmed__c] ='True',tr_case.[Known_Issue_Description__c],null)
															  --,tr_case.[Known_Issues_List__c]
				,NewProductIntroductionNPI__c				=  tr_case.[LA_Case__c]
															  --,tr_case.[Last_Feature_Request_Saved__c]
															  --,tr_case.[Last_Modified_GIS_Progress_Notes__c]
				,Legacy_Last_Modified_By__c_orig				= tr_case.[LastModifiedById]
				,Legacy_Last_Modified_By__c                     = LastMod.Name
															  --,tr_case.[LastModifiedDate]
															  --,tr_case.[LastReferencedDate]
															  --,tr_case.[LastViewedDate]
															  --,tr_case.[Legacy_Closed_By__c]
															  --,tr_case.[Legacy_Contact__c]
															  --,tr_case.[Legacy_Date_Closed__c]
															  --,tr_case.[Legacy_Date_Opened__c]
															  --,tr_case.[Legacy_Defect_Association__c]
															  --,tr_case.[Legacy_Defect_ID__c]
															  --,tr_case.[Legacy_Fixed_in_Version__c]
															  --,tr_case.[Legacy_Keyword_s__c]
															  --,tr_case.[Legacy_Master_Related_Ticket_s__c]
															  --,tr_case.[Legacy_Ticket_ID__c]
															  --,tr_case.[Legacy_Ticket_Priority__c]
															  --,tr_case.[Legacy_Ticket_Status__c]
															  --,tr_case.[Legacy_Version__c]
				,Log_Output__c							=	tr_case.[Log_Output__c]
															  --,tr_case.[Mac_Hours_Utilized__c]
				, ParentID_orig							=	tr_case.[Master_Related_Ticket__c]
				,Master_Related_Ticket__c_orig			=	tr_case.[Master_Related_Ticket__c]
															  --,tr_case.[Media_Shipped__c]
															  --,tr_case.[Milestone_Date_DOLF__c]
															  --,tr_case.[Milestone_Date_Hardware_Installation__c]
															  --,tr_case.[Milestone_Date_System_FAT_Testing__c]
															  --,tr_case.[Milestone_Date_System_Orientation__c]
															  --,tr_case.[Mobile_BA_Email__c]
															  --,tr_case.[Modified_after_Close__c]
															  --,tr_case.[Not_Reproducible_in_Version__c]
															  --,tr_case.[NPI_Gating__c]
															  --,tr_case.[NPI_Release_Measurement__c]
															  --,tr_case.[NPI_Requirements_Measurement__c]
				 ,Occurred_After_Upgrade__c				=	tr_case.[Occured_After_Upgrade_WMP__c]
															  --,tr_case.[Occurrence__c]
															  --,tr_case.[of_Attendees_Per_Class__c]
															  --,tr_case.[of_Days_for_Class__c]
															  --,tr_case.[On_Site_MAC_Hours_Utilized__c]
															  --,tr_case.[Operating_System__c]
				,Origin_orig 								=	tr_case.[Origin]
				,Origin									= picklistorigin.[Target_Picklist_Mapping]

															  --,tr_case.[Original_Customer__c]
				,OwnerID_orig							=	tr_case.[OwnerId]
				,OwnerID_Target								=   Tar_Owner.Id
				,OwnerId_sp							    = iif(Tar_Owner.id is null,
																		iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Tar_Owner.id)
				,OwnerId								= iif(Tar_owner.id is null, @defaultuser, Tar_owner.id)
															 -- ,Owner= map to users table
				,Hotfix__c								=	tr_case.[Patch_Requested__c]
				,Patch_Requested_Date_Time__c_orig		=	tr_case.[Patch_Requested_Date_Time__c]
				--,Patch_Requested_Date_Time__c           =   Eng.Patch_Requested_Date_Time__c
				,VersionNeededIn__c						=	tr_case.[Patch_Requested_Version__c]
				,Patch_Status__c_orig						=	tr_case.[Patch_Status__c]
				--,Patch_Status__c						=	eng.[Patch_Status__c]
															  --,tr_case.[Pending_Roadmap_Planning_Reason__c]
															  --,tr_case.[Planned_Release__c]
															  --,tr_case.[PM_1_Email__c]
															  --,tr_case.[PM_2_Email__c]
															  --,tr_case.[PNC_Ticket__c]
															  --,tr_case.[PO_Received__c]
															  --,tr_case.[Portal_Product_Family__c]
				--,Parent_Engineering_issue__c_orig		=  tr_case.[Potential_Defect__c]
				--,Parent_Engineering_issue__c            =  Eng.Id
															  --,tr_case.[Pre_check_Received__c]
				,Priority								=	CASE
																	WHEN tr_case.[Priority]='P1 - System Down' THEN '1 - Urgent'
																	WHEN tr_case.[Priority]='P2' THEN '2 - Critical'
																	WHEN tr_case.[Priority]='P3' THEN '3 - Non Critical'
																	WHEN tr_case.[Priority]='P4' THEN '4 - Minor'
																	WHEN tr_case.[Priority]='P5' THEN '4 - Minor'
																	ELSE tr_Case.[Priority]
															END
				,Product_Family_WMP__c_orig		 =	tr_case.[Product_Family_WMP__c]
				,Product_Group__c_orig			 = tr_case.[Product_Group__c]
				,Product_Sub_Module__c_orig				=	tr_case.[Product_Sub_Module__c]
				,ProblemCode__C_orig                    = Problem.Legacy_CRMId__c
				,ProblemCode__c							= Problem.id
				,ProductID_orig							=	tr_case.[ProductId]
				,ProductID								=   prodt2.id
				,Reason_orig                            = tr_Case.RCA__c											  
				--,Reason									= iif(tr_case.[RCA__c] ='true','Root Cause Analysis',null)
															  --,tr_case.[Reason]
															  --,tr_case.[Record_Numbers__c]
				,Recordtypeid_orig						=	tr_case.[RecordTypeId]
				--,Recordtypeid                           =   Target_Record.id
				,legacy_recordtypeid					= tr_case.Recordtypeid
				,LegacyWorkOrderType__c                = tr_case.Work_order_type__c
				,work_order_type__c_mapping				= recordtypemap.work_order_type__c
				,Superion_Case_recordtype_orig			= recordtypemap.[superion case record type]
				,Recordtype_Name						= suprectype.name
                ,recordtypeid							= iif(rectype.name <>'Work order Ticket',@supportrecordtypeid,suprectype.id)
															 
															  --,tr_case.[Released_In__c]
				,Reported_Major_Version_WMP__c_orig						=	tr_case.[Reported_Major_Version_WMP__c]
															  --,tr_case.[Reported_Minor_Version__c]
															  --,tr_case.[Reported_Patch_WMP__c]
				,Reproducible_WMP__c_Description		=	tr_case.[Reproducible_WMP__c]-- check the tranformation rules.
															  --,tr_case.[Request_Priority__c]
				--,Resolution__c							=	concat(tr_case.[Resolution_Notes_WMP__c],CHAR(13)+CHAR(10),tr_case.[Customer_Initiated_Closure_Notes__c] pakka'Customer Initiated Closure Notes')
				--,Resolution__c						=		tr_case.[Z_Public_Resolution_Notes__c] == Zuercher Customer Support Ticket
				,Resolution__C							= concat( iif(tr_case.[Customer_Initiated_Closure_Notes__c] is not null,'Customer Initiated Closure Notes : ',''),tr_case.[Customer_Initiated_Closure_Notes__c],CHAR(13)+CHAR(10),
															iif(rectype.name='Zuercher Customer Support Ticket', 
														iif(tr_case.[Z_Public_Resolution_Notes__c] is not null,concat('Public Resolution Notes : ',tr_case.[Z_Public_Resolution_Notes__c]),''),
														iif(tr_case.[Resolution_Notes_WMP__c] is not null,concat('Resolution Notes WMP : ',Resolution_Notes_WMP__c),'')))
															  --,tr_case.[Resolution_Time__c]
				,ResolutionProvided__c					=	tr_case.[Resolved_Date__c]
															  --,tr_case.[Resolved_Time__c]
															  --,tr_case.[Response_Time__c]
															  --,tr_case.[Response_Time_min__c]
															  --,tr_case.[RMS_BA_Email__c]
															  --,tr_case.[ROID__c]
															  --,tr_case.[Sales_Order_Number__c]
															  --,tr_case.[SE_Assistance_Needed__c]
															  --,tr_case.[SE_Contact__c]
															  --,tr_case.[SE_Status__c]
															  --,tr_case.[SE_Task_List__c]
															  --,tr_case.[Secure_Folder_Path__c]
															  --,tr_case.[Send_Email__c]
															  --,tr_case.[Send_Email_DateTime__c]
															  --,tr_case.[Sensitive_Customer__c]
															  --,tr_case.[Sensitive_Customer_Indicator__c]
															  --,tr_case.[Service_Contract_WMP__c]
															  --,tr_case.[Severity__c]
				,Short_Issue_Summary_WMP__c_Description		=	tr_case.[Short_Issue_Summary_WMP__c]-- check the transformation rules.
				--tr_case.Short_Issue_Summary_WMP__c  tr_case.[Description]
				,[Description]  = concat( iif(tr_case.Short_Issue_Summary_WMP__c is not null,'Short Issue Summary WMP : ',''),tr_case.Short_Issue_Summary_WMP__c,CHAR(13)+CHAR(10),
										  iif(tr_case.[Description] is not null,'Description : ',''),tr_case.description,CHAR(13)+CHAR(10),
										  iif(tr_case.Client_Steps_to_Recreate_Issue__c is not null,'Customer Stepts to Replicate : ',''),tr_case.Client_Steps_to_Recreate_Issue__c,CHAR(13)+CHAR(10),
										  iif(tr_case.Reproducible_WMP__c is not null,'Client Reproducible : ',''),tr_case.Reproducible_WMP__c,CHAR(13)+CHAR(10),
										  iif(tr_case.[Z_Impacted_User_s_Login_s__c] is not null,'Impacted User(s) & Login(s) : ',''),tr_Case.Z_Impacted_User_s_Login_s__c)
															  --,tr_case.[SQL_Installation__c]
															  --,tr_case.[SQL_Upgrade__c]
				,Status_orig						=   tr_case.[Status]
				,Status_target						= pickliststatus.[Target_Picklist_Mapping]
				,Z_Issue_Type__c_orig               =  tr_case.[Z_Issue_Type__c]
				,status								= iif(tr_case.[Z_Issue_Type__c]='Feature','Closed',pickliststatus.[Target_Picklist_Mapping])
				,TroubleshootingSteps__c			=	concat(tr_case.[TriTech_Reproducable__c],CHAR(13)+CHAR(10),tr_case.[Steps_to_Recreate_Issue_WMP__c])
				,Subject							=	tr_case.[Subject]
															  --,tr_case.[SuppliedCompany]
															  --,tr_case.[SuppliedEmail]
															  --,tr_case.[SuppliedName]
															  --,tr_case.[SuppliedPhone]
															  --,tr_case.[SystemModstamp]
															  --,tr_case.[Target_Patch_Release_Date__c]
															  --,tr_case.[Tiburon_Group_Name__c]
															  --,tr_case.[Tiburon_Task_List__c]
															  --,tr_case.[Ticket_Age__c]
															  --,tr_case.[Ticket_Comment_Added_Date_Time_Update__c]
				,Reason_Ticket_issue_Category_orig					=	tr_case.[Ticket_Issue_Category__c]
				,Ticket_issue_Category_Target						= picklistticket.[Target_Picklist_Mapping]
				,RCA__c_orig                                        = tr_Case.RCA__c
				,Reason										= iif(tr_case.[RCA__c] ='true','Root Cause Analysis',picklistticket.[Target_Picklist_Mapping])
				,Type										= picklistticket.case_type
															  --,tr_case.[Ticket_Long_ID__c]
															  --,tr_case.[Ticket_Owner_Email__c]
															  --,tr_case.[Ticket_Product__c]
															  --,tr_case.[TicketURL__c]
															  --,tr_case.[Tier_3_Audit__c]
															  --,tr_case.[Tier_3_Audit_Reason__c]
															  --,tr_case.[Tier_3_Contact__c]
															  --,tr_case.[Tier_3_Status__c]
															  --,tr_case.[Tier_3_Team__c]
															  --,tr_case.[Tier_3_Triage_Order__c]
															  --,tr_case.[Time_Card_Duration_WMP__c]
															  --,tr_case.[Time_of_Incident__c]
															  --,tr_case.[Time_On_Call__c]
															  --,tr_case.[Time_on_Call_Reporting__c]
															  --,tr_case.[Training_End_Date__c]
															  --,tr_case.[Training_Location__c]
															  --,tr_case.[Training_Start_Date__c]
				,TroubleshootingSteps__c_orig			=		tr_case.[TriTech_Reproducable__c]
															  --,tr_case.[Type]
															  --,tr_case.[Upgrade_Cancellation_List__c]
				,Alternate_Contact__C_orig				=  tr_case.[Upgrade_Contact__c]
				,Alternate_Contact__C					= upgradecon.id
															  --,tr_case.[Upgrade_Contact_Email__c]
															  --,tr_case.[Upgrade_Contact_Mobile__c]
															  --,tr_case.[Upgrade_Contact_Other_Phone__c]
															  --,tr_case.[Upgrade_Contact_Phone__c]
				,Upgrade_Downtime_in_hours__c		=		tr_case.[Upgrade_Downtime_in_hours__c]
				,Upgrade_End_Version__c_orig		=		tr_case.[Upgrade_End_Version__c]
															  --,tr_case.[Upgrade_End_Version_Patch__c]
															  --,tr_case.[Upgrade_Justification__c]
				,NextStep__c_Upgrade_Note_Orig		=		tr_case.[Upgrade_Note__c]
															  --,tr_case.[Upgrade_Request__c]
				,UpgradeStandbyList__c               =      tr_case.[Upgrade_Standby_List__c]
				,Upgrade_Start_Version__c_orig		 =      tr_case.[Upgrade_Start_Version__c]
				
															  --,tr_case.[Upgrade_Start_Version_Patch__c]
															  --,tr_case.[Upgrade_Survey_Score__c]
															  --,tr_case.[Upgrade_Time_Differential__c]
				,Installation_Type__c				=		tr_case.[Upgrade_Type__c]
				,Sub_status__c						=		iif(tr_case.[Uprade_Confirmed__c]='True','Upgrade Confirmed',null)
															  --,tr_case.[User_Acceptance_Test_Plan__c]
				,UUID_of_Impacted_User_s__c_orig	=		tr_case.[UUID_of_Impacted_User_s__c]
				,Technical_Description__c			=		concat(tr_case.[UUID_of_Impacted_User_s__c],CHAR(13)+CHAR(10),tr_case.[Z_Development_Description__c])
															  --,tr_case.[Work_Around_Impact_to_Operation__c]
				,Work_order_type__c_orig			=		tr_case.[Work_Order_Type__c]
															  --,tr_case.[Workflow_Fired__c]
															  --,tr_case.[Z_After_Hours_Call__c]
															  --,tr_case.[Z_Build_Revision__c]
															  --,tr_case.[Z_Component_s__c]
				,Z_Development_Description__c_orig	=		tr_case.[Z_Development_Description__c]
															  --,tr_case.[Z_Fixed_in_Release__c]
															  --,tr_case.[Z_GIS_Maintenance_Hours__c]
				,Z_Impacted_User_s_Login_s__c_Description	= tr_case.[Z_Impacted_User_s_Login_s__c]-- transformation rules check.
				,Scope__c							=		iif(tr_case.[Z_Impacts_All_Users__c] ='Yes','1 - Affects All',null)-- follow up needed
				,Superion_legacy_id__c = c.legacy_id__c
				,Superion_case_id = c.id
				
					into Case_Tritech_SFDC_Preload_VIP_Account											  
FROM [Tritech_PROD].[dbo].[Case] tr_case
left join [Tritech_PROD].dbo.[account] tr_act
on tr_case.accountid=tr_act.id
---------------------------------------------------------------------------------
left join superion_maplesb.dbo.Account Tar_Account
on Tar_Account.LegacySFDCAccountId__c=Tr_case.AccountId
--and Tar_Account.Legacy_Source_System__c='Tritech'
----------------------------------------------------------------------------------
left join superion_maplesb.dbo.contact Tar_contact 
on Tar_contact.Legacy_ID__c=tr_case.ContactId
and Tar_contact.Legacy_Source_System__c='Tritech'
----------------------------------------------------------------------------------
left join superion_maplesb.dbo.[User] Tar_Owner
on Tar_Owner.Legacy_Tritech_Id__c=tr_case.OwnerId
and Tar_Owner.Legacy_Source_System__c='Tritech'
----------------------------------------------------------------------------------
left join superion_maplesb.dbo.[User] Tar_CreateID
on Tar_CreateID.Legacy_Tritech_Id__c=tr_case.CreatedById
and Tar_CreateId.Legacy_Source_System__c='Tritech'
----------------------------------------------------------------------------------
left join Tritech_PROD.dbo.Potential_Defect__c Potent
on potent.id=tr_case.Potential_Defect__c
left join superion_maplesb.dbo.EngineeringIssue__c Target_Eng
on Target_Eng.Legacy_CRMId__c = Potent.id
-----------------------------------------------------------------------------------
left join superion_maplesb.dbo.contact upgradecon
on upgradecon.Legacy_ID__c = tr_Case.Upgrade_Contact__c
and upgradecon.Legacy_Source_System__c='Tritech'
-----------------------------------------------------------------------------------
--left join superion_maplesb.dbo.EngineeringIssue__c Eng
--on Eng.Legacy_CRMId__c=Tr_case.Potential_Defect__c
------------------------------------------------------------------------------------
left join Tritech_PROD.dbo.[User] LastMod
on LastMod.id=tr_case.LastModifiedById
------------------------------------------------------------------------------------
left join Tritech_PROD.dbo.Product_Sub_Module__c Prod
on Prod.id = tr_case.Product_Sub_Module__c
left join superion_maplesb.dbo.ProblemCode__c problem
on problem.[Legacy_CRMId__c]=prod.id
------------------------------------------------------------------------------------
left join superion_maplesb.dbo.Product2 prodt2
on prodt2.Legacy_SF_ID__c=tr_case.ProductId
 and prodt2.Acronym_List__c = 'legacy ttz'
------------------------------------------------------------------------------------
left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_case.createdbyId
  ------------------------------------------------------------------------------------
  left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_case.createdbyId
  ------------------------------------------------------------------------------------
  left join Tritech_PROD.dbo.RecordType rectype
  on rectype.id = tr_case.RecordTypeId
  -------------------------------------------------------------------------------------
 left join case_Recordtype_Mapping_Tritech recordtypemap 
on tr_case.Work_Order_Type__c=recordtypemap.work_order_type__c
and recordtypemap.recordtypeid=tr_case.RecordTypeId
----------------------------------------------------------------------------------------
left join superion_maplesb.dbo.RecordType suprectype 
on suprectype.Name=recordtypemap.[superion case record type]

-----------------------------------------------------------------------------------------------------
  --left join superion_maplesb.dbo.Version__C vers
  --on vers.Legacy_VersionId__c = tr_case.
  -------------------------------------------------------------------------------------
  left join Case_Picklist_Mapping_Superion picklistorigin
  on picklistorigin.[Tritech_Active_Picklist_Values] = tr_case.Origin
  and picklistorigin.[API_Name]='Origin'
  -------------------------------------------------------------------------------------
  left join Case_Picklist_Mapping_Superion pickliststatus
  on pickliststatus.[Tritech_Active_Picklist_Values] = tr_case.[Status]
  and pickliststatus.[API_Name]='Status'
  -------------------------------------------------------------------------------------
  left join Case_Picklist_Mapping_Superion picklistclosed
  on picklistclosed.[Tritech_Active_Picklist_Values] = tr_case.Closed_Reason__c
  and picklistclosed.[API_Name]='Closed_Reason__c'
  ---------------------------------------------------------------------------------------
   left join Case_Picklist_Mapping_Superion picklistincident
  on picklistincident.[Tritech_Active_Picklist_Values] = tr_case.Closed_Incidents_Reason_WMP__c
  and picklistincident.[API_Name]='Closed_Incidents_Reason_WMP__c'
  ----------------------------------------------------------------------------------------
   left join Case_Picklist_Mapping_Superion picklistticket
  on picklistticket.[Tritech_Active_Picklist_Values] = tr_case.Ticket_Issue_Category__c
  and picklistticket.[API_Name]='Ticket_Issue_Category__c'
  -------------------------------------------------------------------------------------------
  left join superion_maplesb.dbo.[case] c
  on c.legacy_id__c = tr_case.id
  -----------------------------------------------------
where 1=1
and  tr_act.name in (
					'Tiburon'
					,'Milwaukee Police Department WI' 
					--,'City of Baltimore MD'
					,'Baltimore MD'
					,'Inform'
					,'Dane County WI'
					--,'City of San Antonio TX'
					,'San Antonio TX'
					--,'City of Austin TX'
					,'Austin TX'
					,'California Highway Patrol (CHP) CA'
					,'Zuercher'
					,'Rapid City Police Department SD' 
					,'South Dakota Highway Patrol SD' 
					,'Pennington County Sheriff''s Office SD' 
					--,'Lafourche County Sheriff''s Office LA'
					,'Lafourche Parish Sheriff''s Office LA'
					,'Rock Hill Police Department SC'
					,'Douglas County Sheriff''s Office GA'
				)

and isnull(recordtypemap.[superion case record type],'x') <>'DO NOT IMPORT' 


--(22635 row(s) affected)
.

--select count(*) from Case_Tritech_SFDC_Preload_VIP_Account a where 1=1 and 
--isnull(Superion_Case_recordtype_orig,'x') = 'DO NOT IMPORT' -- 49 
--isnull(a.ResolutionType__c,'x') = 'Exclude case from import'-- 25


select count(*) from Case_Tritech_SFDC_Preload_VIP_Account a 
left join Case_Tritech_SFDC_load b
on a.legacy_id__c = b.Legacy_id__c
where b.Legacy_id__c is null and isnull(a.ResolutionType__c,'x') <>'Exclude case from import' -- 5789 records.



-- drop table Case_Tritech_SFDC_load_VIP_Account
select a.* 
into Case_Tritech_SFDC_load_VIP_Account
from Case_Tritech_SFDC_Preload_VIP_Account a 
where 1=1 and Superion_case_id is null 
and isnull(a.ResolutionType__c,'x') <>'Exclude case from import'
 -- (5976 row(s) affected)

  
  select * from case_tritech_SFDC_load_VIP_Account

  select accountid,accountname_tr ,count(*) from case_tritech_SFDC_load_VIP_Account
  group by accountid,accountname_tr

--exec SF_ColCompare 'Insert','sl_superion_maplesb ', 'Case_Tritech_SFDC_load_VIP_Account' 
/*
Salesforce object Case does not contain column AccountID_orig
Salesforce object Case does not contain column AccountName_tr
Salesforce object Case does not contain column Closed_Incidents_Reason_WMP__c_orig
Salesforce object Case does not contain column Closed_Incidents_Reason_WMP__c_target
Salesforce object Case does not contain column Closed_Reason__c_orig
Salesforce object Case does not contain column Closed_Reason__c_target
Salesforce object Case does not contain column Contactid_orig
Salesforce object Case does not contain column Contactid_sr
Salesforce object Case does not contain column CreatedById_orig
Salesforce object Case does not contain column CreatedById_target
Salesforce object Case does not contain column CreatedById_sp
Salesforce object Case does not contain column Severity__c_Customer_Priority_WMP__c
Salesforce object Case does not contain column sourceRecordtype
Salesforce object Case does not contain column Defect_Number__c_case
Salesforce object Case does not contain column Potential_Defect__c_case
Salesforce object Case does not contain column Potent_defect_number__orig
Salesforce object Case does not contain column Potential_Defect_id_orig
Salesforce object Case does not contain column Description_ORIG
Salesforce object Case does not contain column ID_orig
Salesforce object Case does not contain column Internal_Ranking__C_orig
Salesforce object Case does not contain column Internal_Work_Activities__c_orig
Salesforce object Case does not contain column Known_Issue_Confirmed__c_orig
Salesforce object Case does not contain column KnownIssueStatus__c
Salesforce object Case does not contain column KnownIssueDescription__c
Salesforce object Case does not contain column Legacy_Last_Modified_By__c_orig
Salesforce object Case does not contain column ParentID_orig
Salesforce object Case does not contain column Master_Related_Ticket__c_orig
Salesforce object Case does not contain column Origin_orig
Salesforce object Case does not contain column OwnerID_orig
Salesforce object Case does not contain column OwnerID_Target
Salesforce object Case does not contain column OwnerId_sp
Salesforce object Case does not contain column Patch_Requested_Date_Time__c_orig
Salesforce object Case does not contain column VersionNeededIn__c
Salesforce object Case does not contain column Patch_Status__c_orig
Salesforce object Case does not contain column Product_Family_WMP__c_orig
Salesforce object Case does not contain column Product_Group__c_orig
Salesforce object Case does not contain column Product_Sub_Module__c_orig
Salesforce object Case does not contain column ProblemCode__C_orig
Salesforce object Case does not contain column ProductID_orig
Salesforce object Case does not contain column Reason_orig
Salesforce object Case does not contain column Recordtypeid_orig
Salesforce object Case does not contain column legacy_recordtypeid
Salesforce object Case does not contain column work_order_type__c_mapping
Salesforce object Case does not contain column Superion_Case_recordtype_orig
Salesforce object Case does not contain column Recordtype_Name
Salesforce object Case does not contain column Reported_Major_Version_WMP__c_orig
Salesforce object Case does not contain column Reproducible_WMP__c_Description
Salesforce object Case does not contain column Short_Issue_Summary_WMP__c_Description
Salesforce object Case does not contain column Status_orig
Salesforce object Case does not contain column Status_target
Salesforce object Case does not contain column Z_Issue_Type__c_orig
Salesforce object Case does not contain column Reason_Ticket_issue_Category_orig
Salesforce object Case does not contain column Ticket_issue_Category_Target
Salesforce object Case does not contain column RCA__c_orig
Salesforce object Case does not contain column TroubleshootingSteps__c_orig
Salesforce object Case does not contain column Alternate_Contact__C_orig
Salesforce object Case does not contain column Upgrade_End_Version__c_orig
Salesforce object Case does not contain column NextStep__c_Upgrade_Note_Orig
Salesforce object Case does not contain column Upgrade_Start_Version__c_orig
Salesforce object Case does not contain column UUID_of_Impacted_User_s__c_orig
Salesforce object Case does not contain column Work_order_type__c_orig
Salesforce object Case does not contain column Z_Development_Description__c_orig
Salesforce object Case does not contain column Z_Impacted_User_s_Login_s__c_Description
Salesforce object Case does not contain column Superion_legacy_id__c
Salesforce object Case does not contain column Superion_case_id
Column SlaExitDate is not insertable into the salesforce object Case
Column VSOIssue__c is not insertable into the salesforce object Case
*/

--exec SF_BulkOps 'Insert','sl_superion_maplesb ', 'Case_Tritech_SFDC_load_VIP_Account' 

select error, count(*) from Case_Tritech_SFDC_load_VIP_Account 
group by error

-- drop table Case_Tritech_SFDC_load_VIP_Account_errors 
select * 
into Case_Tritech_SFDC_load_VIP_Account_errors
from Case_Tritech_SFDC_load_VIP_Account 
where error<>'Operation Successful.'
--(380 row(s) affected)

-- drop table Case_Tritech_SFDC_load_VIP_Account_errors_bkp
select * 
into Case_Tritech_SFDC_load_VIP_Account_errors_bkp
from Case_Tritech_SFDC_load_VIP_Account 
where error<>'Operation Successful.'
--(380 row(s) affected)


select error, len(External_Case_Reference__c)
--update a set a.External_Case_Reference__c = null 
from Case_Tritech_SFDC_load_VIP_Account_errors a where error like 'Value too large max length:12%' or len(External_Case_Reference__c)>12
--(96 row(s) affected)



--exec SF_BulkOps 'Insert','sl_superion_maplesb ', 'Case_Tritech_SFDC_load_VIP_Account_errors' 

select error, count(*) from Case_Tritech_SFDC_load_VIP_Account_errors 
group by error

select *
--delete 
from Case_Tritech_SFDC_load_VIP_Account
where error<>'Operation Successful.'
--- 380 records.

--insert into Case_Tritech_SFDC_load_VIP_Account
select * from Case_Tritech_SFDC_load_VIP_Account_errors
--(380 row(s) affected)


--------------------------------------------------------------------------------------------------------
/* Creating_Internal_Work_Activities__c as Feed item */
-- drop table feedItem_Tritech_SFDC_Case_preload_VIP_Account
DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from superion_maplesb.dbo.[User] 
where Name like 'Superion API');

SELECT 
         ID											 =   CAST(SPACE(18) as NVARCHAR(18))
		,ERROR										 =   CAST(SPACE(255) as NVARCHAR(255))
		,Body										 =	concat('Internal Work Activities: ', Tr_Cmt.Internal_Work_Activities__c_orig)
		,Tr_Cmt.Internal_Work_Activities__c_orig
		,CreatedbyId								 =	 @DefaultUser
		--,CreatedDate								 =	 Tr_Cmt.[CreatedDate]
		,caseId_orig									 =   Tr_Cmt.[ID_orig]
														  --,Tr_Cmt.[IsDeleted]
		--,IsNotificationSelected						 = Tr_Cmt.[IsNotificationSelected]
		,Visibility								 = 'InternalUsers'
		--,IsPublished_orig							=tr_Cmt.IsPublished
														  --,Tr_Cmt.[LastModifiedById]
														  --,Tr_Cmt.[LastModifiedDate]
				,Parentid					=   Tr_Cmt.Id
														  --,Tr_Cmt.[SystemModstamp]
				into feedItem_Tritech_SFDC_Case_preload_VIP_Account
  FROM case_Tritech_SFDC_load_VIP_Account tr_Cmt 
  where tr_cmt.Internal_Work_Activities__c_orig is not null
  --(10 row(s) affected)

  -- drop table feedItem_Tritech_SFDC_Case_load_VIP_Account
select * 
into feedITem_Tritech_SFDC_Case_load_VIP_Account
from feedItem_Tritech_SFDC_Case_preload_VIP_ACcount
where Parentid<>'' or parentid<> null

--(6 row(s) affected)

select * from feedITem_Tritech_SFDC_Case_load_VIP_Account

-- insert Casecomment records as Chatter comment.

-- EXEC SF_ColCompare 'Insert','sl_superion_maplesb ', 'feedITem_Tritech_SFDC_Case_load_VIP_Account' 

-- EXEC SF_BulkOps 'Insert','sl_superion_maplesb ','feedItem_Tritech_SFDC_Case_load_VIP_Account' 

/*
Salesforce object feedITem does not contain column Internal_Work_Activities__c_orig
Salesforce object feedITem does not contain column caseId_orig
*/

select error, count(*) from feedItem_Tritech_SFDC_Case_load_VIP_Account 
group by error

------------------------------------------------------------------------------------------------------------------------------------

/* Creating Upgrade_Note__c as Feed item */
-- drop table feedItem_Tritech_SFDC_Case_Upgrade_preload_VIP_Account
DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from superion_maplesb.dbo.[User] 
where Name like 'Superion API');

SELECT 
         ID											 =   CAST(SPACE(18) as NVARCHAR(18))
		,ERROR										 =   CAST(SPACE(255) as NVARCHAR(255))
		,Body										 =	 'Upgrade Note: '+Tr_Cmt.NextStep__c_Upgrade_Note_Orig
		,Tr_Cmt.NextStep__c_Upgrade_Note_Orig
		,CreatedbyId								 =	@DefaultUser-- Tr_Cmt.[CreatedById]
		--,CreatedDate								 =	 Tr_Cmt.[CreatedDate]
		,caseId_orig									 =   Tr_Cmt.[ID_orig]
														  --,Tr_Cmt.[IsDeleted]
		--,IsNotificationSelected						 = Tr_Cmt.[IsNotificationSelected]
		,Visibility								 = 'InternalUsers'
		--,IsPublished_orig							=tr_Cmt.IsPublished
														  --,Tr_Cmt.[LastModifiedById]
														  --,Tr_Cmt.[LastModifiedDate]
				,Parentid					=   Tr_Cmt.Id
														  --,Tr_Cmt.[SystemModstamp]
				into feedItem_Tritech_SFDC_Case_Upgrade_preload_VIP_Account
  FROM case_Tritech_SFDC_load_VIP_Account tr_Cmt 
  where tr_cmt.NextStep__c_Upgrade_Note_Orig is not null and error='Operation Successful.'

  --(10 row(s) affected)

  -- drop table feedItem_Tritech_SFDC_Case_Upgrade_load_VIP_Account
select * 
into feedItem_Tritech_SFDC_Case_Upgrade_load_VIP_Account
from feedItem_Tritech_SFDC_Case_Upgrade_preload_VIP_Account
where Parentid<>'' or parentid<> null

--(10 row(s) affected)

select * from feedItem_Tritech_SFDC_Case_Upgrade_load_VIP_Account

-- insert Casecomment records as Chatter comment.

-- EXEC SF_ColCompare 'Insert','sl_superion_maplesb ', 'feedItem_Tritech_SFDC_Case_Upgrade_load_VIP_Account' 

-- EXEC SF_BulkOps 'Insert','sl_superion_maplesb ','feedItem_Tritech_SFDC_Case_Upgrade_load_VIP_Account' 

/*
Salesforce object feedItem does not contain column NextStep__c_Upgrade_Note_Orig
Salesforce object feedItem does not contain column caseId_orig
*/
-----------------------------------------------------------------------------------------------------------------------------------------------

select error, count(*) from feedItem_Tritech_SFDC_Case_Upgrade_load_VIP_Account 
group by error


--use superion_maplesb
--EXEC SF_Refresh 'sl_superion_mapleSb ','Case','yes'
use staging_sb_mapleroots
go
--drop table Case_Tritech_SFDC_load_parent_update_VIP_Account
select 
 Id=a.id
,Legacy_id__c_orig=a.Legacy_id__c
,ERROR= CAST(SPACE(255) as NVARCHAR(255))
,ParentID_orig=a.ParentID_orig
,ParentId=iif(b.Id is null,'Target ParentId is not Found',b.Id)
into Case_Tritech_SFDC_load_parent_update_VIP_Account 
from Case_Tritech_SFDC_load_VIP_Account  a
left join superion_maplesb.dbo.[case] b
on a.ParentID_orig=b.Legacy_id__c
where 1=1
and a.error='Operation Successful.'
and a.ParentID_orig is not null;

--(160 row(s) affected)

select * from Case_Tritech_SFDC_load_parent_update_VIP_Account where error like 'parent case%'

select * from Case_Tritech_SFDC_Preload_VIP_Account where Legacy_id__c='5008000000ONt1OAAT'


select* from Tritech_PROD.dbo.[case] where id ='5008000000ONt1OAAT'

select name,* from Tritech_prod.dbo.Account where id='0018000000rdEmIAAU'
-- update parent id 
--Exec SF_ColCompare 'Update','sl_superion_maplesb ','Case_Tritech_SFDC_load_parent_update_VIP_Account'


--Exec SF_BulkOps 'Update','sl_superion_maplesb ','Case_Tritech_SFDC_load_parent_update_VIP_Account'

/*
Salesforce object Case does not contain column Legacy_id__c_orig
Salesforce object Case does not contain column ParentID_orig*/



select error, count(*) from Case_Tritech_SFDC_load_parent_update_VIP_Account 
group by error;

--This case is parented by a case that is also its child.	199
--Operation Successful.	7697

select count(*),error from Case_Tritech_SFDC_load
group by error
--1694	Account ID: id value of incorrect type: Account Not Found
--312148	Operation Successful.







