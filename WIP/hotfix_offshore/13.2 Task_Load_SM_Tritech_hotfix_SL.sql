/*
-- Author     : Shivani Mogullapalli
-- Date       : 06/11/2018
-- Description: Migrate the Case data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0380
Scope:
Migrate all tasks that relate to a migrated record on a migrated object and migrated record and IS NOT owned by ActOnSync.

*/

/*












Use Tritech_PROD
EXEC SF_RefreshIAD 'MC_TRITECH_PROD','task' --,'Yes'
EXEC SF_Refresh 'MC_TRITECH_PROD','EmailMessage' --,'Yes'

Use Superion_FULLSB
EXEC SF_Replicate 'SL_SUPERION_FULLSB','Task','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Opportunity','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Account','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Case','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Sales_Request__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Procurement_Activity__c','yes'
EXEC SF_Refresh 'SL_SUPERION_FULLSB','Contact_Reference_Link__c','yes'
*/
-- select count(*) from tritech_prod.dbo.[Task]
--1663557

-- drop table Task_Tritech_SFDC_Preload_delta
use staging_sb
go
 declare @defaultuser nvarchar(18)
 select @defaultuser = id from Superion_FULLSB.dbo.[user] where name = 'Superion API'
;With CteWhatData as
(
select 
a.LegacySFDCAccountId__c  Legacy_id_orig,
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Account' as What_parent_object_name 
from Superion_FULLSB.dbo.Account a
where 1=1 
and a.LegacySFDCAccountId__c is not null

						union
select
a.Legacy_Opportunity_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Opportunity' as What_parent_object_name 
from Superion_FULLSB.dbo.Opportunity a
where 1=1
and a.Legacy_Opportunity_ID__c is not null

						union 
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Case' as What_parent_object_name 
from Superion_FULLSB.dbo.[case] a
where 1=1
and a.Legacy_ID__c is not null
						union 
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Sales_Request__c a
where 1=1
and a.Legacy_Record_ID__c is not null
			union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Procurement_Activity__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Procurement_Activity__c a
where 1=1
and a.Legacy_ID__c is not null
				union
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact_Reference_Link__c' as What_parent_object_name 
from Superion_FULLSB.dbo.Contact_Reference_Link__c a
where 1=1
and a.Legacy_ID__c is not null

) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from Superion_FULLSB.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null				
 )   

SELECT  
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_task.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_task.[Account_Name__c]
														  --,tr_task.[Account_Name_DE__c]
				,AccountId_orig							= tr_task.[AccountId]
				,AccountId								= iif(Tar_Account.Id is null or tr_task.[AccountId] is null, null,Tar_ACcount.ID)
				,Activity_Type_For_Reporting__c			= tr_task.[Activity_Type_For_Reporting__c]
				,ActivityDate							= tr_task.[ActivityDate]
														  --,tr_task.[Brand__c]
				,CallDisposition						= tr_task.[CallDisposition]
				,CallDurationInSeconds					= tr_task.[CallDurationInSeconds]
				,CallObject								= tr_task.[CallObject]
				,CallType								= tr_task.[CallType]
														  --,tr_task.[Completed_Date_Time__c]
														  --,tr_task.[Contact_Count__c]
				,CreatedById_orig						= tr_task.[CreatedById]
				,Legacy_createdby_name =Legacyuser.name 
				,CreatedById_target							= Target_Create.ID
				,CreatedById							= iif(Target_Create.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_task.[CreatedDate]
														 -- ,tr_task.[Customer_Risk_Level__c]
				--,Description_orig							= tr_task.[Description]
														  --,tr_task.[DummyTouchField__c]
				,Id_orig								= tr_task.[Id]
				,Isarchieved__orig = tr_task.[IsArchived]
				,IsClosed_orig=tr_task.[IsClosed]
			    ,IsDeleted_orig=    tr_task.[IsDeleted]
														  --,tr_task.[IsHighPriority]
				,IsRecurrence							= tr_task.[IsRecurrence]
				,IsReminderSet							= tr_task.[IsReminderSet]
														  --,tr_task.[LastModifiedById]
														  --,tr_task.[LastModifiedDate]
				,OwnerId_orig							= tr_task.[OwnerId]
				,Legacy_owner_name =Legacyuser1.name 
				,OwnerId_target							= Target_Owner.ID
				,OwnerId							    = iif(Target_owner.id is null,@defaultuser
																		--iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,Priority								= tr_task.[Priority]
				,RecurrenceActivityId_orig				= tr_task.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_task.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_task.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_task.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_task.[RecurrenceInstance]
				,RecurrenceInterval						= tr_task.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_task.[RecurrenceMonthOfYear]
				,RecurrenceRegeneratedType				= tr_task.[RecurrenceRegeneratedType]
				,RecurrenceStartDateOnly				= tr_task.[RecurrenceStartDateOnly]
				,RecurrenceTimeZoneSidKey				= tr_task.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_task.[RecurrenceType]
				,ReminderDateTime						= tr_task.[ReminderDateTime]
				,Resolution_Notes__c					= tr_task.[Resolution_Notes__c]
														  --,tr_task.[Solution_s__c]
				,Status									= tr_task.[Status]
				,Subject								= tr_task.[Subject]
														  --,tr_task.[SystemModstamp]
				--,Description							= iif(tr_task.[Task_Notes__c] is not null,concat(tr_task.[Createddate],tr_task.[Task_Notes__c]),null)
				,TaskSubtype							= tr_task.[TaskSubtype]
														 -- ,tr_task.[Time_Elapsed__c]
				,Type									= tr_task.[Type]
														  --,tr_task.[WhatCount]
				,whatId_orig							= tr_task.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
				    
														  --,tr_task.[WhoCount]
				,WhoID_orig								= tr_task.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_task.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_task.[Description]
				,Description_Comments_summary__C_orig = tr_task.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_task.[Task_Notes__c]
				--,[Description]  = concat( iif(tr_task.[Description] is not null,'Description::',''),tr_task.description,CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_task.createddate),''),tr_task.[Comments_Summary__c],CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_task.createddate),''),tr_task.[Task_Notes__c]
				--						  )
				--,DESCRIPTION = IIF(tr_task.Description IS NOT NULL
				--							,IIF(tr_task.tASK_nOTES__c IS NOT NULL, 
				--								CONCAT(tr_task.DESCRIPTION,CHAR(13)+char(10),tr_task.CREATEDDATE,' ',tr_task.task_notes__c),tr_task.description),null)
				,DESCRIPTION=concat(isnull(tr_task.Description,''),
				iif(tr_task.tASK_nOTES__c IS NOT NULL,concat(CHAR(13)+char(10)+cast(tr_task.CREATEDDATE as nvarchar(40))+' ',
				cast(tr_task.tASK_nOTES__c as nvarchar(max))),''))
             ,tr_email_id=tr_email.id

						into Task_Tritech_SFDC_Preload_delta
  FROM [Tritech_PROD].[dbo].[Task] tr_task
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_task.WhatId
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_task.WhoId
  -----------------------------------------------------------------------------------------------------------------------
    left join Superion_FULLSB.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_task.AccountId
--and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_task.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join Superion_FULLSB.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_task.OwnerID
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_task.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_task.OwnerId
 -----------------------------------------------------------------------------------------------------------------------------
 left join Tritech_Prod.dbo.[EmailMessage] tr_email
  on tr_task.id = tr_email.ActivityId
    -----------------------------------------------------------------------------------------------------------------------------
  
  where 1=1
  and  tr_task.IsDeleted='False' and Legacyuser1.name <>'Act-On Sync'
  and (Wt.parent_id IS NOT NULL or Wo.parent_id IS NOT NULL or Tar_Account.ID IS NOT NULL)
  and (tr_task.IsArchived='false' or Wt.parent_id like '500%')
  and tr_email.id is null
  

  --(119965 row(s) affected)
 -- COUNT OF THE TASK RECORDS
select count(*) 
from Task_Tritech_SFDC_Preload_delta
--   404909 row(s) affected)


-- CHECK FOR THE DUPLICATES.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_Preload_delta
group by Legacy_ID__c
having count(*)>1

--drop table Task_Tritech_SFDC_load_delta
select * 
into Task_Tritech_SFDC_load_delta
from Task_Tritech_SFDC_Preload_delta
where 1=1
 and (whoid is not null or whatid is not null)
  and Legacy_id__c not in (select Legacy_id__c from task_Tritech_SFDC_load
 where error='Operation Successful.')
 ;--47730

 


select * 
from Task_Tritech_SFDC_load_delta
where Description_Task_notes__C_orig is not null

-- LOAD TABLE RECORD COUNT
select count(*) 
from Task_Tritech_SFDC_load_delta
--349019

-- check for the duplicates in the final load table.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_load_delta
group by Legacy_ID__c
having count(*)>1


select * from task_Tritech_SFDC_load_delta
where accountid_orig like '0018000001O2ntuAAB'


select ownerid,IsArchived,Subject,whatid,whoid,* from [Tritech_PROD].dbo.[task]
where accountid='0018000001O2ntuAAB'


select ownerid,IsArchived,Subject,whatid,whoid,* from [Tritech_PROD].dbo.[event]
where accountid='0018000001O2ntuAAB'


--: Run batch program to create task
  
--exec SF_ColCompare 'Insert','SL_SUPERION_FULLSB', 'Task_Tritech_SFDC_load_delta' 

--check column names
  
--Exec SF_BulkOps 'Insert','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_delta'
/*
Salesforce object Task does not contain column Legacy_Source_System__c
Salesforce object Task does not contain column AccountId_orig
Salesforce object Task does not contain column CreatedById_orig
Salesforce object Task does not contain column Legacy_createdby_name
Salesforce object Task does not contain column CreatedById_target
Salesforce object Task does not contain column Id_orig
Salesforce object Task does not contain column Isarchieved__orig
Salesforce object Task does not contain column IsClosed_orig
Salesforce object Task does not contain column IsDeleted_orig
Salesforce object Task does not contain column OwnerId_orig
Salesforce object Task does not contain column Legacy_owner_name
Salesforce object Task does not contain column OwnerId_target
Salesforce object Task does not contain column RecurrenceActivityId_orig
Salesforce object Task does not contain column whatId_orig
Salesforce object Task does not contain column whatid_parent_object_name
Salesforce object Task does not contain column whatid_legacy_id_orig
Salesforce object Task does not contain column WhoID_orig
Salesforce object Task does not contain column whoId_parent_object_name
Salesforce object Task does not contain column whoId_legacy_id_orig
Salesforce object Task does not contain column Logged_Wellness_Check__c
Salesforce object Task does not contain column Description__orig
Salesforce object Task does not contain column Description_Comments_summary__C_orig
Salesforce object Task does not contain column Description_Task_notes__C_orig
Column AccountId is not insertable into the salesforce object Task
*/

select error,count(*) from Task_Tritech_SFDC_load_delta
group by error


select count(*) from Task_Tritech_SFDC_load_delta 
where error<>'Operation Successful.'



-- drop table Task_Tritech_SFDC_load_delta_errors
select *
into Task_Tritech_SFDC_load_delta_errors
from Task_Tritech_SFDC_load_delta 
where error <>'Operation Successful.'
--(36000 row(s) affected)

-- drop table Task_Tritech_SFDC_load_delta_errors_bkp
select *
into Task_Tritech_SFDC_load_delta_errors_bkp
from Task_Tritech_SFDC_load_delta 
where error <>'Operation Successful.'




 --Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_delta_errors'


 select error , count(*) from Task_Tritech_SFDC_load_delta_errors
 group by error;

 select * from Task_Tritech_SFDC_load_delta_errors

 select *
 --delete 
 from Task_Tritech_SFDC_load_delta where error<>'Operation Successful.')

 --(1401 row(s) affected)

 -- insert into Task_Tritech_SFDC_load_delta
 select * from Task_Tritech_SFDC_load_delta_errors where error='Operation Successful.'
 --(1401 row(s) affected)

 select error , count(*) from Task_Tritech_SFDC_load_delta
 group by error;

 -- drop table Task_Tritech_SFDC_load_errors1
 select *
 into Task_Tritech_SFDC_load_errors1
 from Task_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'

 --(5 row(s) affected)


 -- drop table Task_Tritech_SFDC_load_errors1_bkp
 select *
 into Task_Tritech_SFDC_load_errors1_bkp
 from Task_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'

 --(5 row(s) affected)


 -- --Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_FULLSB','Task_Tritech_SFDC_load_errors1'

  select error , count(*) from Task_Tritech_SFDC_load_errors1
 group by error;


 select *
 --delete 
 from Task_Tritech_SFDC_load where Legacy_id__c in
 (select Legacy_id__c from Task_Tritech_SFDC_load_errors1 where error='Operation Successful.')

 -- (5 row(s) affected)

 -- insert into Task_Tritech_SFDC_Load
 select * from Task_Tritech_SFDC_load_errors1 where error='Operation Successful.'

 --
--(5 row(s) affected)


select error, count(*) from Task_Tritech_SFDC_Load 
group by error

--141396

select legacy_id__c,count(*) from Task_Tritech_SFDC_load
group by Legacy_id__c
having count(*)>1



select legacy_id__c ,count(*) from Superion_FULLSB.dbo.task
group by Legacy_Id__c
having count(*)>1




