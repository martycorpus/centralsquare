

select distinct CreatedById --,OwnerId 
from tritech_prod.dbo.BGIntegration__Customer_Exit_Survey_Response__c


--00580000009URIDAA4

select a.id,error,b.EMS_Customer_Number_WMP__c,b.name accountname_orig,LegacySFDCAccountId__c_orig=LegacySFDCAccountId__c 
into Account_Tritech_SFDC_Delete_SL_1Mar2019
from Account_Tritech_SFDC_Load a
,tritech_prod.dbo.Account b
where a.LegacySFDCAccountId__c=b.id
and b.EMS_Customer_Number_WMP__c='Internal';--21

select * from Account_Tritech_SFDC_Delete_SL_1Mar2019;

Your attempt to delete P, SI could not be completed because it is associated with the following cases.: 01158290, 01272737 Your attempt to delete P, SI could not be completed because it is associated with the following portal users.: april.herbertpssi@tr

--Exec SF_BulkOps 'Delete','SL_SUPERION_MAPLESB','Account_Tritech_SFDC_Delete_SL_1Mar2019' --(25:36)

select a.id,error=cast(space(255) as nvarchar(255)),a.Legacy_id__c Legacy_id__c_orig,a.accountid,a.isclosed 
into Case_Tritech_SFDC_Delete_SL_1Mar2019
from SUPERION_MAPLESB.dbo.[case] a
,Account_Tritech_SFDC_Delete_SL_1Mar2019 b
where a.accountid=b.id;--6057

select * from Case_Tritech_SFDC_Delete_SL_1Mar2019;

--Exec SF_BulkOps 'Delete','SL_SUPERION_MAPLESB','Case_Tritech_SFDC_Delete_SL_1Mar2019' --(25:36)


select count(*) from tritech_prod.dbo.[case] a
,tritech_prod.dbo.Account b
where a.accountid=b.id
and b.EMS_Customer_Number_WMP__c='Internal';--21

select * from Account_Tritech_SFDC_Delete_SL_1Mar2019;


select a.id,error=cast(space(255) as nvarchar(255))
,a.contactid contactid_sup,contactid=null,c.Legacy_ID__c Legacy_ID__c_contact
,a.Legacy_id__c Legacy_id__c_orig,a.accountid accountid_sup,a.isclosed isclosed_sup
into Case_Tritech_SFDC_Update_SL_1Mar2019
from SUPERION_MAPLESB.dbo.[case] a
,SUPERION_MAPLESB.dbo.contact c
,Account_Tritech_SFDC_Delete_SL_1Mar2019 b
where c.accountid=b.id
and a.contactid=c.id
;--3383

select * from Case_Tritech_SFDC_Update_SL_1Mar2019;

--Exec SF_colcompare 'Update','SL_SUPERION_MAPLESB','Case_Tritech_SFDC_Update_SL_1Mar2019' --(25:36)

--Exec SF_BulkOps 'Update','SL_SUPERION_MAPLESB','Case_Tritech_SFDC_Update_SL_1Mar2019' --(25:36)
--drop table Opportunity_Tritech_SFDC_Delete_SL_1Mar2019
select a.id,error=cast(space(255) as nvarchar(255)),a.Legacy_Opportunity_ID__c Legacy_Opportunity_ID__c_orig,a.accountid,a.stagename stagename_sup
,isclosed isclosed_sup,iswon iswon_sup 
into Opportunity_Tritech_SFDC_Delete_SL_1Mar2019
--select *  
from SUPERION_MAPLESB.dbo.[Opportunity] a
,Account_Tritech_SFDC_Delete_SL_1Mar2019 b
where a.accountid=b.id;--106


NULL

select * from Opportunity_Tritech_SFDC_Delete_SL_1Mar2019;

--Exec SF_BulkOps 'Delete','SL_SUPERION_MAPLESB','Opportunity_Tritech_SFDC_Delete_SL_1Mar2019' --(25:36)

--drop table User_Tritech_SFDC_Update_SL_1Mar2019
select a.id,error=cast(space(255) as nvarchar(255))
,a.contactid contactid_sup,contactid=null,c.Legacy_ID__c Legacy_ID__c_contact
,a.Legacy_Tritech_Id__c Legacy_id__c_orig,c.accountid accountid_sup
into User_Tritech_SFDC_Update_SL_1Mar2019
--select distinct c.accountid
from SUPERION_MAPLESB.dbo.[User] a
,SUPERION_MAPLESB.dbo.contact c
,Account_Tritech_SFDC_Delete_SL_1Mar2019 b
where c.accountid=b.id
and a.contactid=c.id
;--27


select * from User_Tritech_SFDC_Update_SL_1Mar2019;

--Exec SF_BulkOps 'Update','SL_SUPERION_MAPLESB','User_Tritech_SFDC_Update_SL_1Mar2019' --(25:36)

You can't create a contact for this user because the org doesn't have the necessary permissions. Contact Salesforce Customer Support for help.

