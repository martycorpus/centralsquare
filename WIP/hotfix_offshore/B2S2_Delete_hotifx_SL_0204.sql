--use Superion_FULLSB
--exec SF_Replicate 'SL_SUPERION_FULLSB','Bill_To_Ship_To__c'

Use Staging_SB

--drop table Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_Hotfix_delete_SL_0204
;With CteGP_B2 as
(
select Distinct Account__C,Bill_To__c from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.' and Bill_To__c=1
) --Added CTE by SL on 2/4 
select  a.id,error=cast(space(255) as nvarchar(255)),a.Account__c account__C_Sp,a.Bill_To__c Bill_To__c_sp
into Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_Hotfix_delete_SL_0204
from Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC a
inner join CteGP_B2 b2
on a.account__C=b2.account__C
inner join Superion_FULLSB.dbo.Bill_To_Ship_To__c sp
on a.id=sp.id
where a.error='Operation Successful.'
;--2759

select * from Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_Hotfix_delete_SL_0204


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_Superion_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_Hotfix_delete_SL_0204'


--drop table Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_Hotfix_delete_SL_0204
;With CteGP_S2 as
(
select Distinct Account__C,Ship_to__c from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.' and Ship_to__c=1
) --Added CTE by SL on 2/4 
select  a.id,error=cast(space(255) as nvarchar(255)),a.Account__c account__C_Sp,a.Ship_To__c Ship_To__c_sp
into Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_Hotfix_delete_SL_0204
from Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC a
inner join CteGP_S2 s2
on a.account__C=s2.account__C
inner join Superion_FULLSB.dbo.Bill_To_Ship_To__c sp
on a.id=sp.id
where a.error='Operation Successful.'
;--2758

select * from Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_Hotfix_delete_SL_0204;


--Exec SF_BulkOps 'Delete:batchsize(50)','SL_Superion_FULLSB','Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_Hotfix_delete_SL_0204'