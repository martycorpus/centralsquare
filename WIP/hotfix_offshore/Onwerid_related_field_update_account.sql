--use Superion_FULLSB
--exec SF_Refresh 'SL_SUPERION_FULLSB','Account','Yes'

--drop table Account_Tritech_SFDC_hotfix_Fullsb_27feb_SL
select a.id,error=cast(space(255) as nvarchar(255)),
 Account_Executive_Fin__c_prev=Account_Executive_Fin__c,Installed_PA__c_pre=Installed_PA__c
 ,Account_Executive_del_del__c=Account_Executive_Fin__c
 ,Account_Executive_Install_PSJ__c=Installed_PA__c
 ,Account_Executive_Fin__c=null
 ,Installed_PA__c=null
 into Account_Tritech_SFDC_hotfix_Fullsb_27feb_SL
  from Superion_FULLSB.dbo.Account  a
  --,Tritech_PROD.dbo.Account b
  where 1=1-- a.LegacySFDCAccountId__c=b.id
  and Migrated_Record__c='true';--25552

   --Exec SF_ColCompare 'Update','SL_SUPERION_FULLSB', 'Account_Tritech_SFDC_hotfix_Fullsb_27feb_SL' 

  --Exec SF_BulkOps 'Update:batchsize(70)','SL_SUPERION_FULLSB','Account_Tritech_SFDC_hotfix_Fullsb_27feb_SL'

  select error,count(*) from Account_Tritech_SFDC_Delete_SL_1Mar2019
  group by error
 
 -------------------------------------------
 ---------------------------------------------------
 /****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [TriTech_Competitor]
      ,[Superion_Competitor]
      ,[SandboxID]
      ,[ProductionID]
  FROM [Staging_SB_MapleRoots].[dbo].[map_Competitor_Vendor]

  select name,* from [SUPERION_MAPLESB].dbo.competitor__C
  where id like 'a0J6A000002L8x2%'


  select a.*,b.id,b.name
  from [Staging_SB_MapleRoots].[dbo].[Tritech_competitor_lookup_mapping] a
  left outer join [SUPERION_MAPLESB].dbo.competitor__C b
  on a.[CompetitorNameSandbox]=b.name
  where b.id is null and  [SandboxId_full] is not null



  select a.*,b.id,b.name
  --update a set SandboxId=b.id
  from [Staging_SB_MapleRoots].[dbo].[Tritech_competitor_lookup_mapping] a
  left outer join [SUPERION_MAPLESB].dbo.competitor__C b
  on a.[CompetitorNameSandbox]=b.name
  --where b.id is null and  [SandboxId_full] is not null

  select a.*,b.id,b.name,c.id,c.name
  --update a set SandboxId=c.id
  from [Staging_SB_MapleRoots].[dbo].[Tritech_competitor_lookup_mapping] a
  left outer join [SUPERION_MAPLESB].dbo.competitor__C b
  on a.[CompetitorNameSandbox]=b.name
  left outer join [SUPERION_MAPLESB].dbo.competitor__C c
 -- on a.productioId=b.id
 on a.productionId COLLATE Latin1_General_CS_AS=substring(c.id,1,15)   COLLATE Latin1_General_CS_AS
  where b.id is null and  [SandboxId_full] is not null


  select name,* from Superion_FULLSB.dbo.Competitor__c
  where id='a0J6A000000wkWlUAI'

  select name,count(*) from [SUPERION_MAPLESB].dbo.competitor__C
  group by name
  having count(*)>1;

  select *
  --into SUPERION_MAPLESB.dbo.Account_bkp_25fb2019_SL
   from SUPERION_MAPLESB.dbo.Account;


   select iswon, id,Comments_Reason_TriTech_Lost__c,Comments_Reason_TriTech_Won__c,Reason_TriTech_Lost__c,CASE 
 WHEN Reason_TriTech_Lost__c='No Bid' THEN 'No Bid'
 WHEN Reason_TriTech_Lost__c='Other' THEN opp.Comments_Reason_TriTech_Lost__c
 ELSE CONCAT(IIF(Comments_Reason_TriTech_Lost__c IS NULL,NULL,CONCAT('Comments_Reason_TriTech_Lost:',Comments_Reason_TriTech_Lost__c)),CHAR(13)+CHAR(10),
                      IIF(Comments_Reason_TriTech_Won__c IS NULL,NULL,CONCAT('Comments_Reason_TriTech_Won__c:',Comments_Reason_TriTech_Won__c)))
 END as Win_Loss_Comments__c
 ,CASE 
 WHEN Reason_TriTech_Lost__c='No Bid' THEN 'No Bid'
 WHEN Reason_TriTech_Lost__c='Other' THEN opp.Comments_Reason_TriTech_Lost__c
 when Comments_Reason_TriTech_Lost__c is not null and Comments_Reason_TriTech_Won__c is not null then concat('Comments - Reason TriTech Lost: '+cast(Comments_Reason_TriTech_Lost__c as nvarchar(max)),CHAR(13)+CHAR(10),
                      'Comments - Reason TriTech Won: '+cast(Comments_Reason_TriTech_Won__c as nvarchar(max)))
else Coalesce(Comments_Reason_TriTech_Lost__c , Comments_Reason_TriTech_Won__c )
 END as Win_Loss_Comments__c
 --select distinct Reason_TriTech_Lost__c
 from tritech_prod.dbo.Opportunity opp
 where (Comments_Reason_TriTech_Lost__c is not null and Comments_Reason_TriTech_Won__c is not null)
 and Reason_TriTech_Lost__c='Other (See Details)'

 select contactid,* from User_CommunityUser_Load_RT where error not like '%success%';

 select distinct Forecasting_Category__c,Deal_Forecast__c--,error 
 from opportunity_tritech_Sfdc_load
 where error<>'Operation Successful.' and error like '%bad%';


 select distinct Deal_Forecast__c from Tritech_PROD.dbo.Opportunity

 select id,error,
 Account_Executive_Fin__c_prev=Account_Executive_Fin__c,Installed_PA__c_pre=Installed_PA__c
 ,Account_Executive_del_del__c=Account_Executive_Fin__c
 ,Account_Executive_Install_PSJ__c=Installed_PA__c
 ,Account_Executive_Fin__c=null
 ,Installed_PA__c=null
 into Account_Tritech_SFDC_hotfix_27feb_SL
  from Account_Tritech_SFDC_Load
  where error='Operation Successful.'
  and 


  select * from Account_Tritech_SFDC_hotfix_27feb_SL;

  --Exec SF_ColCompare 'Update','SL_SUPERION_MAPLESB', 'Account_Tritech_SFDC_hotfix_27feb_SL' 

  --Exec SF_BulkOps 'Update:batchsize(70)','SL_SUPERION_MAPLESB','Account_Tritech_SFDC_hotfix_27feb_SL'
  
   (25:36)


 select *,Forecasting_Category__c from staging_sb.dbo.opportunity_tritech_Sfdc_load
 where error<>'Operation Successful.' and error like '%bad%'

 You can't create a contact for this user because the org doesn't have the necessary permissions. 
 Contact Salesforce Customer Support for help.


 Comments - Reason TriTech Lost: Used funds to clean-up billing database instead
Comments - Reason TriTech Won: Saw functionality demonstrated on IA User group and saw how it could simplify his billing process.

 Comments - Reason TriTech Lost: Site demo was scheduled with this client to take place in March. Due to a short list demo the resources/equipment was taken from the first look demo. New dates were provided by PreSales, however resources were never assigned.




  AIM Software & Services (RAM Software Systems)
