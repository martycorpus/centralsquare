/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Team__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Replicate 'SL_Tritech_PROD','BGIntegration__Team__c'

Use Superion_FULLSB
EXEC SF_Refresh 'SL_Superion_FULLSB','User','Yes'
EXEC SF_Refresh 'SL_Superion_FULLSB','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Replicate 'SL_Superion_FULLSB','BGIntegration__Team__c','Yes'
*/ 

Use Staging_SB;

--Drop table BGIntegration__Team__c_Tritech_SFDC_Preload;

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__BomgarSession__c_orig  =  BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c  =  bs.Id

,BGIntegration__Primary__c        =  BGIntegration__Primary__c
,BGIntegration__Team_ID__c        =  BGIntegration__Team_ID__c

,CreatedById_orig                 =  bg.CreatedById
,CreatedById                      =  createdbyuser.Id

,CreatedDate                      =  bg.CreatedDate
--,LegacyID__c                      =  Id
,Name                             =  bg.[Name]

into BGIntegration__Team__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.BGIntegration__Team__c bg

--Fetching CreatedById(UserLookup)
left join Superion_FULLSB.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join Superion_FULLSB.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

;--(279 row(s) affected)

-------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Team__c;--280

Select count(*) from BGIntegration__Team__c_Tritech_SFDC_Preload;--280

Select Legacy_Id__c,count(*) from Staging_SB.dbo.BGIntegration__Team__c_Tritech_SFDC_Preload
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_SB.dbo.BGIntegration__Team__c_Tritech_SFDC_Load;

Select * into 
BGIntegration__Team__c_Tritech_SFDC_Load
from BGIntegration__Team__c_Tritech_SFDC_Preload; --280

Select * from Staging_SB.dbo.BGIntegration__Team__c_Tritech_SFDC_Load 
where CreatedById is null;


--Exec SF_ColCompare 'Insert','SL_Superion_FullSB', 'BGIntegration__Team__c_Tritech_SFDC_Load' 

/*
Salesforce object BGIntegration__Team__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Team__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_Superion_FullSB','BGIntegration__Team__c_Tritech_SFDC_Load'

----------------------------------------------------------------------------------------

--Updating Legacy_Source_System__c,Migrated_Record__c

--Use Staging_SB;

--Drop table BGIntegration__Team__c_Tritech_SFDC_LoadUpdate

Select 

 Id =bg.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Legacy_id__c
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

into BGIntegration__Team__c_Tritech_SFDC_LoadUpdate
from Staging_SB.dbo.BGIntegration__Team__c_Tritech_SFDC_Load bg
;--(278 row(s) affected)

 --Exec SF_ColCompare 'Update','SL_Superion_FullSB','BGIntegration__Team__c_Tritech_SFDC_LoadUpdate' 

/*
*/

--Exec SF_BulkOps 'Update','SL_Superion_FullSB','BGIntegration__Team__c_Tritech_SFDC_LoadUpdate' 