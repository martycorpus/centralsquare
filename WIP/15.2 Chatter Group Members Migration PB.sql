-------------------------------------------------------------------------------
--- ChatterGroup (CollaborationGroup) Migration Script
--- Developed for CentralSquare
--- Developed by Patrick Bowen
--- Copyright Apps Associates 2018
--- Created Date: November 28, 2018
--- Last Updated: November 28, 2018 - PAB
--- Change Log: 
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_Prod
Exec SF_Refresh 'PB_Tritech_Prod', 'CollaborationGroup', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'CollaborationGroupMember', 'yes';
Exec SF_Refresh 'PB_Tritech_Prod', 'User', 'yes';

Use Superion_FULLSB
Exec SF_Refresh 'PB_Superion_FULLSB', 'User', 'yes';
Exec SF_Refresh 'PB_Superion_FULLSB', 'CollaborationGroup', 'yes';

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_SB

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroupMember_Load_PB') 
DROP TABLE Staging_SB.dbo.CollaborationGroupMember_Load_PB;


---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------

USE Staging_SB

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		Group_Source.[Name] as CollaborationGroupName_Orig,
		[Group_Target].Id as CollaborationGroupId,
		a.MemberID as MemberID_Original,
		[User].ID as MemberId,
		a.CollaborationRole as CollaborationRole,
		a.NotificationFrequency as NotificationFrequency
		INTO CollaborationGroupMember_Load_PB
		From Tritech_Prod.dbo.CollaborationGroupMember a
		--- MemberID
		LEFT OUTER JOIN Superion_FULLSB.dbo.[User] [User] ON a.MemberID = [User].Legacy_Tritech_Id__c
		-- Chatter Group
		LEFT OUTER JOIN Tritech_PROD.dbo.[CollaborationGroup] [Group_Source] ON a.CollaborationGroupId = [Group_Source].[ID]
		INNER JOIN Superion_FULLSB.dbo.[CollaborationGroup] [Group_Target] ON Group_Source.[Name] = [Group_Target].[Name]



---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performan
---------------------------------------------------------------------------------
Alter table CollaborationGroupMember_load_PB
Add [Sort] int identity (1,1)



---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Staging_SB
Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'CollaborationGroupMember_load_PB'

--select * from CollaborationGroupMember_load_PB where error not like '%success%' and error like '%memberid%'

--select * from CollaborationGroupMember_load_PB where error like '%success%'

---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------

--drop table USER_Update_inActiveChatterGroupMembers_PB

--Select Distinct
--a.MemberID as ID,
--CAST('' as nvarchar(255)) as error,
--'true' as isactive
--INTO USER_Update_inActiveChatterGroupMembers_PB 
--From CollaborationGroupMember_load_PB a
--WHERE error like '%inactive_owner_or_user%'

--use Superion_FULLSB
--Exec SF_BulkOps 'update:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'USER_Update_inActiveChatterGroupMembers_PB'

--select * from USER_Update_inActiveChatterGroupMembers_PB

--select * from staging_sb.dbo.User_Update_toinActive_PB where error like '%successful%'

--use Staging_SB
--Exec SF_BulkOps 'update:bulkapi,batchsize(4000)', 'PB_Superion_FULLSB', 'User_Update_toinActive_PB'

--Select * INTO CollaborationGroupMember_load_PB_backup
--from CollaborationGroupMember_load_PB


--delete from Superion_FULLSB.dbo.CollaborationGroupMember_load_PB where error like '%success%'

--update USER_Update_inActiveChatterGroupMembers_PB set isActive = '0'