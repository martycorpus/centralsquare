/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Script for the Environment Customer Web Portal
DEVELOPER	: RTECSON and MCORPUS
CREATED DT  : 02/14/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
03/15/2019		Ron Tecson			Initial for Customer Web Portal
03/26/2019		Ron Tecson			Reloaded data in Full Sandbox. Duration time: 15 mins.
03/28/2019		Ron Tecson			Repointed to Production.
03/30/2019		Ron Tecson			Executed in Production. Total Record Count: 820 with no errors.

DECISIONS:

Pre-requisite:
	- Need to run the SPV Process to populate the load table Environment__c_PreLoad_Customer_Web_Portal_RT.

******************************************************************************************************************************************************/


---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

Use Superion_Production
Exec SF_Refresh 'RT_Superion_PROD', 'Account', 'subset';

---------------------------------------------------------------------------------
-- Drop Environment__c_PreLoad_Non_Zuercher_RT
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_Customer_Web_Portal_RT') 
DROP TABLE Staging_PROD.dbo.Environment__c_PreLoad_Customer_Web_Portal_RT;

select distinct 
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		registered_product_name, -- RegProd_ProductGroup, 
		registered_product_accountid, -- RegProd_AccountId, 
		--SUBSTRING(CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + registered_product_name + '-' + a.name,1,80) AS Name,
		SUBSTRING('PROD' + '-' + registered_product_name + '-' + a.name,1,80) AS Name,
		registered_product_accountid as Account__c,
		'Automatically created because no Hardware Software records existed in Tritech source'  as description__c,
       --       CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production'
       --             WHEN EnvironmentType = 'Train' Then 'Training'
       --             WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c,
		a.Name as AccountName,
		'Yes' AS Active__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
INTO Staging_PROD.dbo.Environment__c_PreLoad_Customer_Web_Portal_RT
FROM System_Product_Version__c_Insert_Webportal_MC_Load s
	JOIN Superion_Production.dbo.ACCOUNT a ON a.id = s.registered_product_accountid --regprod_accountid
WHERE  environment__c = 'ID NOT FOUND'

-- 03/12 -- (208 rows affected)
-- 03/15 -- (1380 rows affected)
-- 03/26 -- (2059 rows affected)
-- 03/30 -- Production: (820 rows affected)

select * FROM System_Product_Version__c_Insert_Webportal_MC_Load s WHERE  environment__c = 'ID NOT FOUND'

---------------------------------------------------------------------------------
-- Drop Environment__c_Load_Customer_Web_Portal_RT
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_Load_Customer_Web_Portal_RT') 
DROP TABLE Staging_PROD.dbo.Environment__c_Load_Customer_Web_Portal_RT;

select * INTO Staging_PROD.dbo.Environment__c_Load_Customer_Web_Portal_RT
from Staging_PROD.dbo.Environment__c_PreLoad_Customer_Web_Portal_RT

EXEC SF_ColCompare 'INSERT','RT_Superion_PROD','Environment__c_Load_Customer_Web_Portal_RT'

/************************************** ColCompare L O G **************************************
--- Starting SF_ColCompare V3.6.7
Problems found with Environment__c_Load_Customer_Web_Portal_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 80]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment__c does not contain column registered_product_name
Salesforce object Environment__c does not contain column registered_product_accountid
Salesforce object Environment__c does not contain column AccountName
Salesforce object Environment__c does not contain column LegacySystemIds
Salesforce object Environment__c does not contain column Winning_RecordType
Salesforce object Environment__c does not contain column RecordTypes

************************************** ColCompare L O G **************************************/

EXEC sf_bulkops 'INSERT','RT_Superion_PROD','Environment__c_Load_Customer_Web_Portal_RT'

/************************************** INSERT L O G **************************************

03/30/2019

--- Starting SF_BulkOps for Environment__c_Load_Customer_Web_Portal_RT V3.6.7
15:47:35: Run the DBAmp.exe program.
15:47:35: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:47:35: Inserting Environment__c_Load_Customer_Web_Portal_RT (SQL01 / Staging_PROD).
15:47:35: DBAmp is using the SQL Native Client.
15:47:36: SOAP Headers: 
15:47:36: Warning: Column 'registered_product_name' ignored because it does not exist in the Environment__c object.
15:47:36: Warning: Column 'registered_product_accountid' ignored because it does not exist in the Environment__c object.
15:47:36: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
15:47:36: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
15:47:36: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
15:47:36: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
15:47:39: 820 rows read from SQL Table.
15:47:39: 820 rows successfully processed.
15:47:39: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

************************************** INSERT L O G **************************************/

------------------------------------------------------------------------------------
-- VALIDATION
------------------------------------------------------------------------------------

select error, count(*) from Environment__c_Load_Customer_Web_Portal_RT
group by error

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE Superion_Production

EXEC SF_Refresh 'RT_Superion_PROD', 'Environment__c', 'subset';

select LEN(Name), * from Environment__c_Load_Non_Zuercher_RT where error <> 'Operation Successful.'

select * from System_Product_Version__c_Insert_WebPortal_MC
where error <> 'Operation Successful.'

SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD
