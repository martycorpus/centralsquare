--Update the community users permission set for go live based on the permission set mapping in confluence.
--2019-03-31 MartyC. Created.


/*-------------------------------------------------------
Execution Log:

COLCOMPARE
-- Exec Sf_colcompare 'UPDATE','MC_SUPERION_PROD','PermissionSetAssignment_Update_MC';
--- Starting SF_ColCompare V3.6.7
Problems found with PermissionSetAssignment_Update_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 8]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object PermissionSetAssignment does not contain column AssigneeID_Original
Salesforce object PermissionSetAssignment does not contain column PermissionSetID_original
Salesforce object PermissionSetAssignment does not contain column Superion_PermissionSetName
Column AssigneeID is not updateable in the salesforce object PermissionSetAssignment
Column PermissionSetID is not updateable in the salesforce object PermissionSetAssignment

INSERT:
-- Exec Sf_Bulkops 'Insert','MC_SUPERION_PROD','PermissionSetAssignment_Update_MC';
--- Starting SF_BulkOps for PermissionSetAssignment_Update_MC V3.6.7
01:24:02: Run the DBAmp.exe program.
01:24:02: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
01:24:02: Inserting PermissionSetAssignment_Update_MC (SQL01 / Staging_PROD).
01:24:03: DBAmp is using the SQL Native Client.
01:24:03: SOAP Headers: 
01:24:03: Warning: Column 'AssigneeID_Original' ignored because it does not exist in the PermissionSetAssignment object.
01:24:03: Warning: Column 'PermissionSetID_original' ignored because it does not exist in the PermissionSetAssignment object.
01:24:03: Warning: Column 'Superion_PermissionSetName' ignored because it does not exist in the PermissionSetAssignment object.
01:30:12: 8354 rows read from SQL Table.
01:30:12: 8354 rows failed. See Error column of row for more information.
01:30:12: 0 rows successfully processed.
01:30:12: Errors occurred. See Error column of row for more information.
01:30:12: Percent Failed = 100.000.
01:30:12: Error: DBAmp.exe was unsuccessful.
01:30:12: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert PermissionSetAssignment_Update_MC "SQL01"  "Staging_PROD"  "MC_SUPERION_PROD"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 21]
SF_BulkOps Error: 01:24:02: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC01:24:02: Inserting PermissionSetAssignment_Update_MC (SQL01 / Staging_PROD).01:24:03: DBAmp is using the SQL Native Client.01:24:03: SOAP Headers: 01:24:03: Warning: Column 'AssigneeID_Original' ignored because it does not exist in the PermissionSetAssignment object.01:24:03: Warning: Column 'PermissionSetID_original' ignored because it does not exist in the PermissionSetAssignment object.01:24:03: Warning: Column 'Superion_PermissionSetName' ignored because it does not exist in the PermissionSetAssignment object.01:30:12: 8354 rows read from SQL Table.01:30:12: 8354 rows failed. See Error column of row for more information.01:30:12: 0 rows successfully processed.01:30:12: Errors occurred. See Error column of row for more information.




*/

use Staging_PROD;


-- drop table PermissionSetAssignment_Update_MC


SELECT
	CAST('' as nvarchar(18)) as ID,
	CAST('' as nvarchar(255)) as Error,
	a.Legacy_Tritech_ID__c as AssigneeID_Original,
	a.ID as AssigneeID,
	SrcProf.[Name] as PermissionSetID_original,
	Perm.ID as PermissionSetID,
	Perm.Name as Superion_PermissionSetName
	INTO PermissionSetAssignment_Update_MC
	FROM User_CommunityUser_Load_RT a
	LEFT OUTER JOIN TriTech_Prod.dbo.[user] SrcUser ON a.Legacy_Tritech_Id__c = SrcUser.Id																							----> Source User
	LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] SrcProf ON SrcUser.ProfileId = SrcProf.ID																							----> Source Profile
	LEFT OUTER JOIN SUPERION_Production.dbo.[PermissionSet] Perm ON Perm.[Name] =																										----> Permission Set
																			CASE SrcProf.[Name] WHEN 'TriTech Portal Read-Only with Tickets' THEN 'Community_Case_Access_Readonly'
																			                    WHEN 'TriTech Portal Read Only with Tickets' THEN 'Community_Case_Access_Readonly'
																								WHEN 'TriTech Portal Read-Only User' THEN 'Community_Case_Access_Readonly'
																								WHEN 'TriTech Portal Standard User' THEN 'Community_Case_Access'
																								WHEN 'TriTech Portal Manager' THEN 'Community_Delegated_Admin_Dedicated'
																								ELSE '' END
	WHERE a.error = 'Operation Successful.'  --(8354 row(s) affected) 3/30 MC PROD.




--CHECKS: 
	--select id, name, * from SUPERION_Production.dbo.[PermissionSet]
	--where name like '%community%';

	SELECT * FROM PermissionSetAssignment_Update_MC WHERE AssigneeID is null;

	SELECT * FROM PermissionSetAssignment_Update_MC WHERE PermissionSetID is null; 

	select * from PermissionSetAssignment_Update_MC;

	SELECT * FROM PermissionSetAssignment_Update_MC where AssigneeID = '0052G000009INzTQAW'

select * from Contact_CommunityPermissions_Load_RT where id = '0032G00002CKkKzQAL'

select * from mc_superion_prod...[user] where id = '0052G000009INzTQAW'