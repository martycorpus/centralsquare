
/*
-- Author     : Sree
-- Date       : 11-20-2018
-- Description: Migrate the User data from Tritech Prod to Superion Salesforce.

Requirement Number:REQ-0841
Scope: 

Selection Criteria: 
a. Migrate 'active' Salesforce licensed Users
b. Do not migrate any other license type Users

-- inactive users, who are not migrated and are the owner or creator of a record, will have the owning user be set as a specific user ie (Central Square API)/

EN 10/24: Scope needs to be revisited by track leads

Modification Comments:
2/21 - RT. Added "@" on the UserName field. ('@centralsquare.com.superion.boss').
	 - RT. Executed the Users (Insert and Updates). Added the logs.

*/
---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_PROD

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
/*
USE Tritech_PROD
Exec SF_Refresh 'MC_TRITECH_PROD', 'User','Yes' 
Exec SF_Replicate 'MC_TRITECH_PROD', 'Profile' --,'Yes'
Exec SF_Replicate 'MC_TRITECH_PROD', 'UserRole'

USE Superion_Production
Exec SF_Refresh 'SL_SUPERION_PROD', 'User' ,'Yes' --1mins
Exec SF_Replicate 'SL_SUPERION_PROD', 'Profile'-- ,'Yes'
Exec SF_Replicate 'SL_SUPERION_PROD', 'UserRole'-- ,'Yes'
Exec SF_Replicate 'SL_SUPERION_PROD','PermissionSetAssignment'
Exec SF_Replicate 'SL_SUPERION_PROD','PermissionSet'

select Legacy_Tritech_Id__c,count(*) from Superion_Production.dbo.[user]
group by Legacy_Tritech_Id__c
having count(*)>1;--46326

*/
---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_PROD;
/*
select name,username,email,isactive,* from Superion_Production.dbo.[user]
where usertype='Standard' and email like '%@appsassociates.com%'
;--6

select name,username,email,isactive,* from tritech_prod.dbo.[user]
where usertype='Standard' and email like '%@appsassociates.com%'
;--5


select a.id,b.id,c.id, a.username,b.username,c.username,a.name,b.name,c.name,a.email,b.email,c.email,a.isactive,b.isactive,c.isactive
,b.usertype,c.usertype
 from tritech_prod.dbo.[user] a
left outer join Superion_Production.dbo.[user] b
on a.email=replace(b.email,'.invalid','') and b.usertype='standard'
left outer join Superion_Production.dbo.[user] c
on a.name=c.name and c.usertype='standard'
where a.isactive='true' and a.usertype='standard'
and (b.id is not null or c.id is not null ) and a.email not like '%@appsassociates.com%'
order by a.name;--138

select name,username,email,isactive from [tritech_prod].dbo.[user]
where username='david.bateman@tritech.prod'

select name,count(*) from Superion_Production.dbo.[user]
where usertype='Standard'
group by name
having count(*)>1;--12
*/
--drop table User_Tritech_SFDC_preload

declare @defaultprofile nvarchar(18)=(select  id from Superion_Production.dbo.[Profile] Spr_Profile
                                       where  name like 'CentralSquare Standard User');
declare @CustomerSupportMgrRole nvarchar(18)=(select  id from Superion_Production.dbo.[UserRole] Spr_role
                                       where  name ='Customer Support Mgr');
declare @CustomerSupportAgentRole nvarchar(18)=(select  id from Superion_Production.dbo.[UserRole] Spr_role
                                       where  name ='Customer Support Agent');

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Tritech_Id__c,
		'Tritech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		concat(substring(a.username,1,charindex('@',a.username)-1),'@centralsquare.com') as Username,
		a.[Username] as Username_orig,
		a.name ,
		a.[LastName] as LastName,
		isNull(a.[FirstName], '') as FirstName,
		isNull(a.[CompanyName], '') as CompanyName,
		a.CreatedDate CreatedDate_orig,
		isNull(a.[Division], '') as Division,
		isNull(a.[Department], '') as Department,
		isNull(a.[Title], '') as Title,
		isNull(a.[Street], '') as Street,
		isNull(a.[City], '') as City,
		isNull(a.[State], '') as State,
		isNull(a.[PostalCode], '') as PostalCode,
		isNull(a.[Country], '') as Country,
		concat(replace(replace(a.email,'@Zuerchertech.com','@centralsquare.com'),'@Tritech.com','@centralsquare.com'),'.prod') as Email,
		a.[Email] as Email_orig,
		isNull(a.[Phone], '') as Phone,
		isNull(a.[Fax], '') as Fax,
		isNull(a.[MobilePhone], '') as MobilePhone,
		a.[Alias] as Alias,
		Concat(a.[CommunityNickname], '1') as CommunityNickname,
		a.[CommunityNickname] as CommunityNickname_orig,
		a.IsActive as IsActive,
		a.IsActive  isactive_orig,
		a.[TimeZoneSidKey] as TimeZoneSidKey,
		a.[LocaleSidKey] as LocaleSidKey,
		a.[ReceivesInfoEmails] as ReceivesInfoEmails,
		a.[ReceivesAdminInfoEmails] as ReceivesAdminInfoEmails,
		a.[EmailEncodingKey] as EmailEncodingKey,
		
isNull(Map_Prof.[Service Cloud User], 'false') as UserPermissionsSupportUser,
isNull(Map_Prof.[Service Cloud User], 'false') as UserPermissionsKnowledgeUser,

		a.ProfileId ProfileID_orig,
		Map_Prof.[Superion Profile Name] Mapped_superionProfileName,
		Trg_Prof.[Name] as SuperionProfile_name,
		Trg_Prof.ID as ProfileID_sup,
         isnull(Trg_Prof.ID,@defaultprofile) as ProfileID,
		TT_Profile.name tritech_profile_name,
		--------------------------------------
		a.UserRoleId as UserRoleId_orig,
		case when Trg_Prof.name='Support Manager' then @CustomerSupportMgrRole
		     when Trg_Prof.name='Support Rep' then @CustomerSupportAgentRole
			 else sp_role.id
			 end as UserRoleId,
			 tt_role.name tt_role,
			 role_map.tritech_role ,
			 role_map.superion_role,
			 role_map.notes role_map_notes,
			 sp_role.name sp_rolename,
   ---------------------------------------------------------

		a.UserType ,
		a.[LanguageLocaleKey] as LanguageLocaleKey,
		isNull(a.[EmployeeNumber], '') as EmployeeNumber,
		isNull(a.[UserPermissionsMarketingUser], '') as UserPermissionsMarketingUser,
		isNull(a.[UserPermissionsOfflineUser], '') as UserPermissionsOfflineUser,
		isNull(a.[UserPermissionsCallCenterAutoLogin], '') as UserPermissionsCallCenterAutoLogin,
		isNull(a.[UserPermissionsMobileUser], '') as UserPermissionsMobileUser,
		isNull(a.[UserPermissionsSFContentUser], '') as UserPermissionsSFContentUser,
		--isNull(a.[UserPermissionsKnowledgeUser], '') as UserPermissionsKnowledgeUser,
		isNUll(a.[UserPermissionsInteractionUser], '') as UserPermissionsInteractionUser,
		--isNull(a.[UserPermissionsSupportUser], '') as UserPermissionsSupportUser,
		a.[ForecastEnabled] as ForecastEnabled,
		a.[UserPreferencesActivityRemindersPopup] as UserPreferencesActivityRemindersPopup,
		a.[UserPreferencesEventRemindersCheckboxDefault] as UserPreferencesEventRemindersCheckboxDefault,
		a.[UserPreferencesTaskRemindersCheckboxDefault] as UserPreferencesTaskRemindersCheckboxDefault,
		a.[UserPreferencesReminderSoundOff] as UserPreferencesReminderSoundOff,
		a.[UserPreferencesApexPagesDeveloperMode] as UserPreferencesApexPagesDeveloperMode,
		a.[UserPreferencesHideCSNGetChatterMobileTask] as UserPreferencesHideCSNGetChatterMobileTask,
		a.[UserPreferencesHideCSNDesktopTask] as UserPreferencesHideCSNDesktopTask,
		a.[UserPreferencesSortFeedByComment] as UserPreferencesSortFeedByComment,
		a.[UserPreferencesLightningExperiencePreferred] as UserPreferencesLightningExperiencePreferred,
		isNull(a.[CallCenterId], '') as CallCenterId,
		isNull(a.[Extension], '') as Extension,
		isNull(a.[PortalRole], '') as PortalRole,
		a.[FederationIdentifier]as FederationIdentifier_orig,
		concat(replace(replace(a.email,'@Zuerchertech.com','@centralsquare.com'),'@Tritech.com','@centralsquare.com'),'.prod') as FederationIdentifier,
		isNull(a.[AboutMe], '') as AboutMe,
		a.[DigestFrequency] as DigestFrequency,
		isNull(a.[Bomgar_Username__c], '') as Bomgar_Username__c,
		a.Managerid as Managerid_orig,
		sp_user.id sp_userid,
		sp_user.name sp_name,
		sp_user.username sp_username,
		sp_user.email sp_email,
		sp_user.isactive sp_isactive,
		sp_user.Legacy_Tritech_Id__c  Legacy_Tritech_Id__c_sp,
		sp_user.Legacy_Source_System__c Legacy_Source_System__c_sp,
		---------------------------
		sp_user_email.id sp_userid1,
		sp_user_email.name sp_name1,
		sp_user_email.username sp_username1,
		sp_user_email.email sp_email1,
		sp_user.isactive sp_isactive1,
		sp_user_email.Legacy_Tritech_Id__c  Legacy_Tritech_Id__c_sp1,
		sp_user_email.Legacy_Source_System__c Legacy_Source_System__c_sp1,
		
		-----------------
	    sp_user1.id sp_existing_id,
		--------------------
		usr_exclude.TT_userid excluded_userid,
		usr_exclude.comments excluded_user_comments
		-------------------
		INTO User_Tritech_SFDC_preload
		FROM Tritech_PROD.dbo.[User] a
		------------------------------------------------------
		left outer join [Superion_Production].[dbo].[user] sp_user
	    on sp_user.name=a.name and sp_user.Usertype='Standard' and sp_user.id<>'0056A000000m8EgQAI'
		-------------------------------------------------------------
		left outer join [Superion_Production].[dbo].[user] sp_user_email
	    on  sp_user_email.email=a.email 
		 and sp_user_email.Usertype='Standard' and sp_user_email.id<>'0056A000000m8EgQAI'
	---------------------------------------------------------------------
	left outer join [Superion_Production].[dbo].[user] sp_user1
	    on sp_user1.Legacy_Tritech_Id__c=a.id 
	-------------------------------------------------------------------
	-- TriTech Profiles
		LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] TT_Profile 
		ON  a.ProfileId = TT_Profile.ID
    ---------------------------------------------------------------------
		-- Map Profile
LEFT OUTER JOIN .dbo.map_Profile Map_Prof 
ON TT_Profile.[Name] = Map_Prof.[Tritech Profile Name]
--------------------------------------------------------------
-- Target Profile
LEFT OUTER JOIN Superion_Production.dbo.[Profile] Trg_Prof 
ON Map_Prof.[Superion Profile Name] = Trg_Prof.[Name]
-------------------------------------------------------
-- Tritech Role
LEFT OUTER JOIN Tritech_PROD.dbo.[userRole] tt_role 
ON tt_role.id = a.UserRoleId
-------------------------------------------------------
-- Role mapping for Sales team
LEFT OUTER JOIN Tritech_Sales_Role_Mapping role_map 
ON role_map.tritech_Role = tt_role.[Name]
-------------------------------------------------------
-- Superion Role
LEFT OUTER JOIN Superion_Production.dbo.[userRole] sp_role 
ON sp_role.name =role_map.superion_role
-------------------------------------------------------
--Exclude user list
Left outer join tritech_user_exclude_list usr_exclude
ON a.id =usr_exclude.TT_userid
-------------------------------------------------
			WHERE    1=1 
			and a.isActive = 'true'
			and a.usertype='Standard' and a.email not like '%@appsassociates.com%'
			;--560

		
		
	select * from 	User_Tritech_SFDC_preload;--560

	select 560-27
	
	select * from tritech_user_exclude_list;--28

	select a.excluded_user_comments,b.id tt_userid,b.name tt_name,b.username tt_username,b.email tt_email,sp_userid,sp_userid1
		 from User_Tritech_SFDC_preload a
		 inner join [Tritech_PROD].dbo.[user] b
		 on a.Legacy_Tritech_Id__c=b.id
		 where a.excluded_userid is not null;--27

	----Counts----

	select count(*) from 	User_Tritech_SFDC_preload;--560
	
	select count(*) from [Tritech_PROD].[dbo].[user];	--16405
	
	select count(*) from [Tritech_PROD].[dbo].[user]
	where isactive='true' and usertype='standard'
	;	--565

	select count(*) from Superion_Production.[dbo].[user];--46326
-----------------------------------------------------------------------
	
	 select Legacy_Tritech_Id__c,count(*) from User_Tritech_SFDC_preload
	 group by Legacy_Tritech_Id__c
	 having count(*)>1;--0

	 select sp_userid,* from User_Tritech_SFDC_preload
	-- where UserType='Standard'
	 where legacy_tritech_id__C='0051E00000F4MuCQAV';--deborah.solorzano@tt.centralsquare.com

	 select id,name,email,isactive,username from [Superion_Production].dbo.[user]
	 where name like 'Deborah Solorzano'


	 select distinct usertype
	 --select *
	 	 from User_Tritech_SFDC_preload
	 where usertype='Standard'
	 and isactive_orig='true';

	 select * from Tritech_PROD.dbo.[user]
	 where email='scott.key@tritech.com';--4

	 select usertype,isactive_orig,count(*) from User_Tritech_SFDC_preload
	 group by usertype,isactive_orig
	 having count(*)>1;
	 
	 

	 --Drop table User_Tritech_SFDC_load  --this table will have both newly created as well as updated records
	 	 select *
	 into User_Tritech_SFDC_load
	 from User_Tritech_SFDC_preload a
	 where usertype='Standard'
	 and isactive_orig='true'
	 and sp_existing_id is null and excluded_userid is null
	 order by Name
	 ;--533

	 select name,sp_name1,* from User_Tritech_SFDC_load
	 where 1=1
	  and ( sp_userid is null and sp_userid1 is  null);--397


	 select Legacy_Tritech_Id__c,count(*) from User_Tritech_SFDC_load
	 group by Legacy_Tritech_Id__c
	 having count(*)>1;--0

	

	 select sp_userid1,sp_userid, * from User_Tritech_SFDC_load 
	 where Legacy_Tritech_Id__c='0058000000F4B8TAAV'
	 order by Name;


	 select b.id tt_userid,b.name tt_name,b.username tt_username,b.email tt_email,c.id superion_id,c.isactive superion_isactive,c.name superion_name,c.email superion_email,c.username superion_username
		 from User_Tritech_SFDC_load a
		 inner join [Tritech_PROD].dbo.[user] b
		 on a.Legacy_Tritech_Id__c=b.id
		  left outer join Superion_Production.dbo.[user] c
		 on isnull(a.sp_userid,sp_userid1)=c.id
	 where 1=1
	  and ( sp_userid is not null or sp_userid1 is not null)
	  or a.username in ('alexander.russell@centralsquare.com'
,'sara.wise@centralsquare.com'
,'jennifer.gerrietts@centralsquare.com')
	  ;--136
	

	 --drop table User_Tritech_SFDC_load_final   --this is for Insert
	  select *
	 into User_Tritech_SFDC_load_final
	 from User_Tritech_SFDC_load a
	 where 1=1
	  and ( sp_userid is null and sp_userid1 is null);--397

	   select Legacy_Tritech_Id__c,count(*) from User_Tritech_SFDC_load_final
	 group by Legacy_Tritech_Id__c
	 having count(*)>1;--0

	 select distinct ProfileID,superionProfile_name,tritech_profile_name, tt_role,role_map_notes
	 --select *
	  from User_Tritech_SFDC_load_final
	 where userroleid is  null;--54

	 select *
	 --select distinct country
	  from User_Tritech_SFDC_load_final
	 where profileid is  null;--0

	  
	 select isactive,email,username,FederationIdentifier,Country,State,* from User_Tritech_SFDC_load_final;
---------------------------------------------------------------------------------
-- Insert records
---------------------------------------------------------------------------------

--exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'User_Tritech_SFDC_load_final' 

--check column names

/*
Salesforce object User does not contain column Username_orig
Salesforce object User does not contain column CreatedDate_orig
Salesforce object User does not contain column Email_orig
Salesforce object User does not contain column CommunityNickname_orig
Salesforce object User does not contain column isactive_orig
Salesforce object User does not contain column ProfileID_orig
Salesforce object User does not contain column Mapped_superionProfileName
Salesforce object User does not contain column SuperionProfile_name
Salesforce object User does not contain column ProfileID_sup
Salesforce object User does not contain column tritech_profile_name
Salesforce object User does not contain column UserRoleId_orig
Salesforce object User does not contain column tt_role
Salesforce object User does not contain column tritech_role
Salesforce object User does not contain column superion_role
Salesforce object User does not contain column role_map_notes
Salesforce object User does not contain column sp_rolename
Salesforce object User does not contain column FederationIdentifier_orig
Salesforce object User does not contain column Managerid_orig
Salesforce object User does not contain column sp_userid
Salesforce object User does not contain column sp_name
Salesforce object User does not contain column sp_username
Salesforce object User does not contain column sp_email
Salesforce object User does not contain column sp_isactive
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp
Salesforce object User does not contain column Legacy_Source_System__c_sp
Salesforce object User does not contain column sp_userid1
Salesforce object User does not contain column sp_name1
Salesforce object User does not contain column sp_username1
Salesforce object User does not contain column sp_email1
Salesforce object User does not contain column sp_isactive1
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp1
Salesforce object User does not contain column Legacy_Source_System__c_sp1
Salesforce object User does not contain column sp_existing_id
Salesforce object User does not contain column excluded_userid
Salesforce object User does not contain column excluded_user_comments
Column name is not insertable into the salesforce object User
Column UserType is not insertable into the salesforce object User
*/


--Exec SF_BulkOps 'Insert:batchsize(50)','SL_SUPERION_PROD','User_Tritech_SFDC_load_final'
--3 mins


select email,username,username_orig,name, * from User_Tritech_SFDC_load_final
 where error not like '%success%'
 ;

select * from User_Tritech_SFDC_load_final
 where error  like '%success%'

 select error,count(*) from User_Tritech_SFDC_load_final
 group by error;

---------------------------------------------------------------------------------
-- Populate External ID for precreated Users
-- Only users were a matching email exists on a SINGLE record will be populated
-- User records whose email exists on multiple records will NOT be populated and must be manually resolved
---------------------------------------------------------------------------------

USE Staging_PROD
--drop table User_Tritech_SFDC_Update_existing_User
;with CteArticlemgrPerSet as
(select  b.AssigneeId from Superion_Production.dbo.PermissionSet a
  ,Superion_Production.dbo.PermissionSetAssignment b
  ,Superion_Production.dbo.[User] c
  where a.id=b.PermissionSetId
  and b.AssigneeId=c.id
  and a.Label='Article Manager'
   )
, CTeDupeemails as (
select Legacy_Tritech_Id__c ,count(*) rec_count 
from User_Tritech_SFDC_load
group by Legacy_Tritech_Id__c
having count(*)>1
)
,ExistingUsers as (select a.id,a.username from [Superion_Production].dbo.[user] a
,User_Tritech_SFDC_load b
where a.IsActive='true'
and a.username=b.username and (b.sp_userid is null and b.sp_userid1 is null)
)
Select 
	SPR.ID as ID, 
	stg.Error,
	stg.Legacy_Tritech_Id__c as Legacy_Tritech_Id__c,
	spr.UserPermissionsKnowledgeUser as UserPermissionsKnowledgeUser_sup,
	stg.UserPermissionsKnowledgeUser as UserPermissionsKnowledgeUser_stg,
	ap.AssigneeId AssigneeId_sup,
	iif(ap.AssigneeId is not null  ,'true',spr.UserPermissionsKnowledgeUser) as UserPermissionsKnowledgeUser,
	stg.Legacy_Source_System__c,
	Duplicate_number = ROW_NUMBER() OVER (PARTITION BY stg.Legacy_Tritech_Id__c
order by iif(spr.name=stg.name,1,2) ),spr.username spr_username,
spr.name spr_name,stg.name tr_name,stg.Legacy_Tritech_Id__c_sp,stg.Email_orig,spr.email spr_email,sp_userid,sp_userid1,
dupe.Legacy_Tritech_Id__c dupe_Legacy_Tritech_Id__c,stg.username tr_username
--eu.username existing_username,
--eu.id existing_sp_id
		INTO User_Tritech_SFDC_Update_existing_User
	FROM Superion_Production.dbo.[User] SPR
	inner  JOIN User_Tritech_SFDC_load stg
	on spr.id=isnull(stg.sp_userid,stg.sp_userid1) or (stg.username in ('alexander.russell@centralsquare.com'
,'sara.wise@centralsquare.com'
,'jennifer.gerrietts@centralsquare.com') and stg.username=spr.username)
	left outer join CTeDupeemails dupe
	on spr.Legacy_Tritech_Id__c=dupe.Legacy_Tritech_Id__c	
	left outer join CteArticlemgrPerSet ap
	on spr.id=ap.AssigneeId
--	left outer join ExistingUsers eu
	--on SPR.id=eu.id
	where 1=1
	   --and dupe.Legacy_Tritech_Id__c is not null
	    and spr.Legacy_Tritech_Id__c is null
		AND SPR.Migrated_Record__c = 'false'  -- ensures that only pre-existing User records are included
		
		;--139  --136+3

		select * from User_Tritech_SFDC_Update_existing_User
		where Legacy_Tritech_Id__c  is null;--='0051E00000F4MuCQAV'
		where Duplicate_number>1;

		select * from User_Tritech_SFDC_Update_existing_User
		where spr_username in ('alexander.russell@centralsquare.com'
,'sara.wise@centralsquare.com'
,'jennifer.gerrietts@centralsquare.com')

		-------------------------------Update--------------------

		--drop table User_Tritech_SFDC_Update_existing_User_final
		select * 
		INTO User_Tritech_SFDC_Update_existing_User_final
		from User_Tritech_SFDC_Update_existing_User
		where Duplicate_number=1;--139

		select id,count(*) from User_Tritech_SFDC_Update_existing_User_final
		group by id
		having count(*)>1;

		select * from User_Tritech_SFDC_Update_existing_User_final
	--	where AssigneeId_sup is not null;--1
		where UserPermissionsKnowledgeUser_sup='false' and UserPermissionsKnowledgeUser='true'
		

		---------------------------------------------------------------------------------
-- Update records
---------------------------------------------------------------------------------

--exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'User_Tritech_SFDC_Update_existing_User_final' 

--check column names
/*
Salesforce object User does not contain column UserPermissionsKnowledgeUser_sup
Salesforce object User does not contain column UserPermissionsKnowledgeUser_stg
Salesforce object User does not contain column AssigneeId_sup
Salesforce object User does not contain column Duplicate_number
Salesforce object User does not contain column spr_username
Salesforce object User does not contain column spr_name
Salesforce object User does not contain column tr_name
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp
Salesforce object User does not contain column Email_orig
Salesforce object User does not contain column spr_email
Salesforce object User does not contain column sp_userid
Salesforce object User does not contain column sp_userid1
Salesforce object User does not contain column dupe_Legacy_Tritech_Id__c
Salesforce object User does not contain column tr_username
*/

--Exec SF_BulkOps 'Update:batchsize(10)','SL_SUPERION_PROD','User_Tritech_SFDC_Update_existing_User_final'



select * from User_Tritech_SFDC_Update_existing_User_final
 where error not like '%success%'
 ;

 select error,count(*) from User_Tritech_SFDC_Update_existing_User_final
 group by error;

 	---------------------------------------------------------------------------------
-- Update Manager records
---------------------------------------------------------------------------------
/*
USE Superion_Production
Exec SF_Refresh 'SL_SUPERION_PROD', 'User' --,'Yes'
*/

Use Staging_PROD

--drop table User_Tritech_SFDC_Manager_update
select
tr_user.id
,error=cast(space(255) as nvarchar(255))
,Legacy_Tritech_Id__c_sp_user=tr_user.Legacy_Tritech_Id__c
,Managerid=iif(sp_mgr.id is  null,'Manager Not Found',sp_mgr.id)
,managerid_orig=tr_user.ManagerId_orig
,tr_name=tr_user.name
,tr_email=tr_user.email
,tr_username=tr_user.Username
,sp_mgr_name=sp_mgr.name
,sp_mgr_email=sp_mgr.email
,sp_mgr_username=sp_mgr.Username
,Legacy_Source_System__c_sp_mgr=sp_mgr.Legacy_Source_System__c
,Legacy_Tritech_Id__c_sp_mgr=sp_mgr.Legacy_Tritech_Id__c
,tr_user_id=tr_user.id
into User_Tritech_SFDC_Manager_update
 from User_Tritech_SFDC_load_final tr_user
left outer join [Superion_Production].dbo.[User] sp_mgr
on tr_user.ManagerId_orig=sp_mgr.Legacy_Tritech_Id__c
where tr_user.error='Operation Successful.'
and tr_user.ManagerId_orig is not null
;--372

select * from User_Tritech_SFDC_Manager_update;


--Exec SF_ColCompare 'update', 'SL_SUPERION_PROD', 'User_Tritech_SFDC_Manager_update'

/*
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp_user
Salesforce object User does not contain column managerid_orig
Salesforce object User does not contain column tr_name
Salesforce object User does not contain column tr_email
Salesforce object User does not contain column tr_username
Salesforce object User does not contain column sp_mgr_name
Salesforce object User does not contain column sp_mgr_email
Salesforce object User does not contain column sp_mgr_username
Salesforce object User does not contain column Legacy_Source_System__c_sp_mgr
Salesforce object User does not contain column Legacy_Tritech_Id__c_sp_mgr
Salesforce object User does not contain column tr_user_id
*/

--Exec SF_BulkOps 'update:batchsize(10)', 'SL_SUPERION_PROD', 'User_Tritech_SFDC_Manager_update'


select error,count(*) from User_Tritech_SFDC_Manager_update
group by error;--43 failed




--------------------------------------------------------------------------------------------
--------------------- Assigning Article Manger Permission set for given users----------------
--------------------------------------------------------------------------------------------

--select * from Superion_Production.dbo.PermissionSetAssignment b;

--drop table PermissionSetAssignment_Superion_Article_mgr_assignment_preload

declare @ArticleMgrPerSet nvarchar(18)=(select top 1 id from Superion_Production.dbo.PermissionSet
                                       where Label='Article Manager');
select 
ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,assigneeId=a.id
,PermissionSetId=@ArticleMgrPerSet
,a.isactive isactive_sup
, a.name user_name,a.Legacy_Tritech_Id__c,a.UserPermissionsSupportUser UserPermissionsSupportUser_sup,
a.UserPermissionsKnowledgeUser UserPermissionsKnowledgeUser_sup
,Duplicate_number = ROW_NUMBER() OVER (PARTITION BY a.name
order by iif(a.Legacy_Tritech_Id__c is not null,1,2), a.UserPermissionsKnowledgeUser desc,a.UserPermissionsSupportUser desc)
,b.id persetassingmentid_sup
,a.Migrated_Record__c Migrated_Record__c_sp
into PermissionSetAssignment_Superion_Article_mgr_assignment_preload
from Superion_Production.dbo.[User] a
left outer join Superion_Production.dbo.PermissionSetAssignment b
on a.id=b.AssigneeId and b.PermissionSetId=@ArticleMgrPerSet
  where 1=1 --  and b.id is null
    and a.name in 
  (
  
'Paul Trewin'
,'Kelly Adams'
,'Samantha O''Brien'
,'Ty Kaiser'
,'Renae Livingston'
,'Jesse Blommer'
--,'Shane Birch'
,'Pete Avila'
,'Kristi Miller'
,'Ellen Manning'
  );

  select * from PermissionSetAssignment_Superion_Article_mgr_assignment_preload;

  --drop table PermissionSetAssignment_Superion_Article_mgr_assignment_load

  select * 
  into PermissionSetAssignment_Superion_Article_mgr_assignment_load
  from 
  PermissionSetAssignment_Superion_Article_mgr_assignment_preload
  where 1=1
  and Duplicate_number=1
  ;--8

  select assigneeid,count(*) from PermissionSetAssignment_Superion_Article_mgr_assignment_load
  group by assigneeid
  having count(*)>1;

  select * from PermissionSetAssignment_Superion_Article_mgr_assignment_load;

--Exec SF_ColCompare 'Insert', 'SL_SUPERION_PROD', 'PermissionSetAssignment_Superion_Article_mgr_assignment_load'

/*
Salesforce object PermissionSetAssignment does not contain column isactive_sup
Salesforce object PermissionSetAssignment does not contain column user_name
Salesforce object PermissionSetAssignment does not contain column Legacy_Tritech_Id__c
Salesforce object PermissionSetAssignment does not contain column UserPermissionsSupportUser_sup
Salesforce object PermissionSetAssignment does not contain column UserPermissionsKnowledgeUser_sup
Salesforce object PermissionSetAssignment does not contain column Duplicate_number
Salesforce object PermissionSetAssignment does not contain column persetassingmentid_sup
Salesforce object PermissionSetAssignment does not contain column Migrated_Record__c_sp
*/

--Exec SF_BulkOps 'Insert:batchsize(50)', 'SL_SUPERION_PROD', 'PermissionSetAssignment_Superion_Article_mgr_assignment_load'


select * from PermissionSetAssignment_Superion_Article_mgr_assignment_load
where error<>'Operation Successful.'
;

  select error,count(*) from PermissionSetAssignment_Superion_Article_mgr_assignment_load
  group by error;


