 /*
-- Author       : Jagan	
-- Created Date : 05-Feb-19
-- Modified Date: 
-- Modified By  :   
-- Description  : Migrate from OpportunityContactRole to Key_Players_Influencers__c.

-----------Tritech Key_Players_Influencers__c----------
Requirement No  : REQ-0912
Total Records   :  
Scope: Migrate all records that relate to a migrated opportunity record.
 */

 /*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','OpportunityContactRole','yes'


Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','Contact','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Opportunity','Yes'
*/ 

Use Staging_PROD;

--Drop table Key_Players_Influencers__c_Tritech_SFDC_Preload

DECLARE @Default NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'CentralSquare API');

Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,legacy_id__c=ocr.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
,Name_orig=ocr.ContactId
,Name__c=IIF(con.Id IS NULL,'Contact Not Found',con.Id)
,Opportunity_orig=ocr.OpportunityId
,Opportunity__c=opp.Id
,Role_orig=ocr.Role
,CASE
 WHEN ocr.Role='Coach' THEN 'Coach'
 WHEN ocr.Role='Business User' THEN 'User'
 WHEN ocr.Role='Consultant' THEN 'Consultant'
 WHEN ocr.Role='Decision Maker' THEN 'Decision Maker'
 WHEN ocr.Role='Influencer' THEN 'Decision Influencer'
 WHEN ocr.Role='Technical Buyer' THEn 'Technical Buyer'
 WHEN ocr.Role IS NULL THEN NULL
 ELSE 'Other'
 END as Formal_Role__c
,CreatedDate=ocr.CreatedDate
,CreatedById_orig=ocr.CreatedById
,CreatedById=IIF(usr.Id IS NULL,@Default,usr.Id)
,InfluenceLevel__c ='Influencer Base'

 into Key_Players_Influencers__c_Tritech_SFDC_Preload

from Tritech_PROD.dbo.OpportunityContactRole ocr
inner join SUPERION_PRODUCTION.dbo.Opportunity opp
on opp.Legacy_Opportunity_ID__c=ocr.OpportunityId
left join SUPERION_PRODUCTION.dbo.Contact con
on con.Legacy_ID__c=ocr.ContactId
left join SUPERION_PRODUCTION.dbo.[User] usr
on ocr.CreatedById=usr.Legacy_Tritech_Id__c;--(4351 row(s) affected)

--------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.OpportunityContactRole;--5108

Select count(*) from Key_Players_Influencers__c_Tritech_SFDC_Preload;--4351

Select LegacySFDCOpportunityContactRoleId__c,count(*) from Staging_PROD.dbo.Key_Players_Influencers__c_Tritech_SFDC_Preload
group by LegacySFDCOpportunityContactRoleId__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.Key_Players_Influencers__c_Tritech_SFDC_Load;

Select * into 
Key_Players_Influencers__c_Tritech_SFDC_Load
from Key_Players_Influencers__c_Tritech_SFDC_Preload; --(4351 row(s) affected)

Select * from Staging_PROD.dbo.Key_Players_Influencers__c_Tritech_SFDC_Load;--4351


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'Key_Players_Influencers__c_Tritech_SFDC_Load' 

/*
Salesforce object Key_Players_Influencers__c does not contain column LegacySFDCOpportunityContactRoleId__c
Salesforce object Key_Players_Influencers__c does not contain column Legacy_Source_System__c
Salesforce object Key_Players_Influencers__c does not contain column Migrated_Record__c
Salesforce object Key_Players_Influencers__c does not contain column Name_orig
Salesforce object Key_Players_Influencers__c does not contain column Opportunity_orig
Salesforce object Key_Players_Influencers__c does not contain column Role_orig
Salesforce object Key_Players_Influencers__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_PROD','Key_Players_Influencers__c_Tritech_SFDC_Load'

---------------------------------------------------------------------------------------------------------------

--drop table Key_Players_Influencers__c_Tritech_SFDC_Load_errors
select * 
into Key_Players_Influencers__c_Tritech_SFDC_Load_errors
from Key_Players_Influencers__c_Tritech_SFDC_Load
where error<>'Operation Successful.'
;--501

--drop table Key_Players_Influencers__c_Tritech_SFDC_Load_errors_bkp
select * 
into Key_Players_Influencers__c_Tritech_SFDC_Load_errors_bkp
from Key_Players_Influencers__c_Tritech_SFDC_Load
where error<>'Operation Successful.';--501


Select * from Key_Players_Influencers__c_Tritech_SFDC_Load_errors_bkp ;

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_PROD','Key_Players_Influencers__c_Tritech_SFDC_Load_errors'

Select *
--delete 
from Key_Players_Influencers__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --

--insert into Key_Players_Influencers__c_Tritech_SFDC_Load
Select *
from Key_Players_Influencers__c_Tritech_SFDC_Load_errors;--


select error, count(*) from Key_Players_Influencers__c_Tritech_SFDC_Load
group by error

select id, error,legacy_id__c =LegacySFDCOpportunityContactRoleId__c, Legacy_Source_System__c,Migrated_Record__c
into Key_Players_Influencers__c_Tritech_SFDC_Load_hotfix 
from Key_Players_Influencers__c_Tritech_SFDC_Load where error='Operation Successful.'
 --(4262 row(s) affected)

 select * from Key_Players_Influencers__c_Tritech_SFDC_Load_hotfix

 --Exec SF_Bulkops 'Update:batchsize(1)','SL_SUPERION_PROD', 'Key_Players_Influencers__c_Tritech_SFDC_Load_hotfix' 

 select a.id,a.error,b.procurement_type__c,b.proposal_stage__c,b.proposal_manager_notes__c proposal_manager_notes__c_orig,
left(b.proposal_manager_notes__c,254) proposal_manager_notes__c
into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_err_hotfix_SL
from Procurement_Activity__c_Opportunity_Tritech_SFDC_load a
,Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors_bkp b
where a.opportunity__C=b.opportunity__C
;--706

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD ','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_err_hotfix_SL'