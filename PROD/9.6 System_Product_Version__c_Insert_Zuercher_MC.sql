-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 9.6 System_Product_Version__c_Insert_Zuercher_MC.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--- Copyright Apps Associates 2018
--- Created Date: Jan 22, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-01-21 Created.
--  2019-02-05 Wipe and Reload.
--  2019-02-12 Wipe and Reload.
--  2019-02-15 Wipe and Reload.
--  2019-03-28 Move to Production.
---
--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-03-xx Executed in Superion PROD.

-- WIPE:
-- select * into System_Product_Version__c_delete_Zuercher_MC_18Feb from System_Product_Version__c_Insert_Zuercher_MC where error = 'Operation Successful.'
-- (0 row(s) affected)


-- Use Staging_PROD;
-- EXEC SF_ColCompare 'Insert','MC_SUPERION_PROD','System_Product_Version__c_Insert_Zuercher_MC'
/*
--- Starting SF_ColCompare V3.6.9
Problems found with System_Product_Version__c_Insert_Zuercher_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 30]
--- Ending SF_ColCompare. Operation FAILED.



ErrorDesc
Salesforce object System_Product_Version__c does not contain column Environment_Name
Salesforce object System_Product_Version__c does not contain column Environment_Description
Salesforce object System_Product_Version__c does not contain column Environment_Type
Salesforce object System_Product_Version__c does not contain column Zuercher_Registered_Product_Name
Salesforce object System_Product_Version__c does not contain column Version_Name
Salesforce object System_Product_Version__c does not contain column Account_Id
Salesforce object System_Product_Version__c does not contain column CUSTOMER_ASSET_Product_Group
Salesforce object System_Product_Version__c does not contain column product_group
Salesforce object System_Product_Version__c does not contain column product_line
*/
-- Use Staging_PROD;
-- EXEC SF_Bulkops 'Insert','MC_SUPERION_PROD','System_Product_Version__c_Insert_Zuercher_MC'

/*
--- Starting SF_BulkOps for System_Product_Version__c_Insert_Zuercher_MC V3.6.9
18:26:17: Run the DBAmp.exe program.
18:26:17: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:26:17: Inserting System_Product_Version__c_Insert_Zuercher_MC (SQL01 / Staging_PROD).
18:26:17: DBAmp is using the SQL Native Client.
18:26:18: SOAP Headers: 
18:26:18: Warning: Column 'Environment_Name' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'Environment_Description' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'Environment_Type' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'Zuercher_Registered_Product_Name' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'Version_Name' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'Account_Id' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'CUSTOMER_ASSET_Product_Group' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'product_group' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'product_line' ignored because it does not exist in the System_Product_Version__c object.
18:26:18: Warning: Column 'Ident' ignored because it does not exist in the System_Product_Version__c object.
18:26:19: 138 rows read from SQL Table.
18:26:19: 138 rows successfully processed.
18:26:19: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

*/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_PROD;

Exec SF_Refresh 'MC_TRITECH_PROD','Hardware_Software__c','Yes'

Use Superion_Production;

Exec SF_Refresh 'MC_SUPERION_PROD','CUSTOMER_ASSET__C','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Registered_Product__c','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Environment__c','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Version__c','Yes'


---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_Zuercher_MC') 
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_Zuercher_MC;

Use Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_Zuercher_MC_preload') 
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_Zuercher_MC_preload;



With CTE_Zuercher_Customer_assets as
(
SELECT t1.id as Zuercher_Registered_Product_Id,
       t1.name as Zuercher_Registered_Product_Name,
       t2.id   AS CustomerAsset__c,
       t2.name AS CustomerAsset_Name,
       t2.product_group__c as CUSTOMER_ASSET_Product_Group,
       p2.product_group__c,
       p2.product_line__c,
       t1.Account__c,
       a.Name as accountname
FROM   Superion_Production.dbo.REGISTERED_PRODUCT__C t1
        LEFT JOIN Superion_Production.dbo.PRODUCT2 p2
        ON p2.id = t1.product__c
       join Superion_Production.dbo.CUSTOMER_ASSET__C t2
         ON t2.id = t1.customerasset__c
       left join  Superion_Production.dbo.account a on a.Id = t1.Account__c 
WHERE  t2.product_group__c = 'Zuercher' 
),
CTE_Z_Env as
(
SELECT * FROM Superion_Production.dbo.ENVIRONMENT__C WHERE NAME LIKE '%Zuercher%'
) 
SELECT distinct  CAST(NULL AS NCHAR(18)) AS Id,
       CAST(NULL AS NVARCHAR(255)) AS Error,
	   coalesce(CTE_Z_Env.Id,cte_env_test.Id)  as Environment__c,
	   coalesce(CTE_Z_Env.Name,cte_env_test.Name) as Environment_Name,
	   coalesce(CTE_Z_Env.Description__c,cte_env_test.Description__c ) as Environment_Description,
	   coalesce(CTE_Z_Env.Type__c,cte_env_test.Type__c)as Environment_Type,
	   cte_ca.Zuercher_Registered_Product_Id as Registered_Product__c,
	   cte_ca.Zuercher_Registered_Product_Name,
	   (select id from MC_SUPERION_PROD...[Version__c] where name = 'PSJ-Zuercher vALL') as Version__c,
	   'PSJ-Zuercher vALL' as Version_Name,
	   cte_ca.Account__c as Account_Id,
	   cte_ca.CUSTOMER_ASSET_Product_Group,
	   cte_ca.product_group__c as product_group,
	   cte_ca.product_line__c AS product_line
into System_Product_Version__c_Insert_Zuercher_MC_preload
FROM CTE_Zuercher_Customer_assets cte_ca 
left join CTE_Z_Env on CTE_Z_Env.Account__c =  cte_ca.Account__c and 
                      CTE_Z_Env.Description__c like '%' + cte_ca.Product_Line__c + '%' and 
					  CTE_Z_Env.Type__c = 'Production' and 
					  cte_ca.Zuercher_Registered_Product_Name not like '%Test%' and 
					  cte_ca.Zuercher_Registered_Product_Name not like '%Train%'
left join CTE_Z_Env cte_env_test on cte_env_test.Account__c =  cte_ca.Account__c and 
                      cte_env_test.Description__c like '%' + cte_ca.Product_Line__c + '%' and 
					  cte_env_test.Type__c <> 'Production' and 
					  cte_ca.Zuercher_Registered_Product_Name like '%Test%' and 
					  cte_ca.Zuercher_Registered_Product_Name like  '%Train%' ; --(166 row(s) affected)
--(201 row(s) affected) 3/3
(2084 row(s) affected)

					  

ALTER TABLE System_Product_Version__c_Insert_Zuercher_MC ADD Ident INT Identity(1,1);


select * from  System_Product_Version__c_Insert_Zuercher_MC_preload

--check:
select Environment__c, Registered_Product__c, count(*) 
from System_Product_Version__c_Insert_Zuercher_MC_preload
where environment__c is not null
group by Environment__c, Registered_Product__c
having count(*) > 1 --(0 row(s) affected)

--check for records in this load that may already been loaded in 9.5 script.
select t2.version__c, t1.* from System_Product_Version__c_Insert_Zuercher_MC_preload t1
join System_Product_Version__c_Insert_MC_load t2 on t2.Registered_Product__c = t1.Registered_Product__c

--only insert Z registered products that are not handled in 9.5
select t1.* 
into System_Product_Version__c_Insert_Zuercher_MC
from System_Product_Version__c_Insert_Zuercher_MC_preload t1
left join System_Product_Version__c_Insert_MC_load t2 on t2.Registered_Product__c = t1.Registered_Product__c
where t2.id is null

--(0 row(s) affected) 3/30

--all records in the load table are created 3/30

--join for version_update:
update t1
set id = t2.id 
from System_Product_Version__c_Insert_Zuercher_MC t1
join System_Product_Version__c_Insert_MC_load t2 on t2.Registered_Product__c = t1.Registered_Product__c --not executed 3/30 no need.

select id, error, version__c, [version_name]
 into System_Product_Version__c_Insert_Zuercher_MC_version_update
from System_Product_Version__c_Insert_Zuercher_MC --not executed 3/30 no need.

exec SF_BulkOps 'Update','MC_SUPERION_PROD','System_Product_Version__c_Insert_Zuercher_MC_version_update' --not executed 3/30 no need.

