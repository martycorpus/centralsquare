/*****************************************************************************************************************************************************
REQ #		: REQ-0386
TITLE		: Migrate Problem Codes (Product Sub Module) - Source System to SFDC
DEVELOPER	: RTECSON and MCORPUS
CREATED DT  : 01/15/2019
DETAIL		: 	
				Per Maria Huemmer 1/10/2019,
				
				Also, Shane and I made it through the full mapping, please find it attached. We have specified the product names and then the Product Group and Line to search for a match. The query is like this

				For Product Name XYZ, select all Products where Eligible for Support is TRUE AND Product Group is <GROUP> and Product Line is <LINE> and insert a Problem Code record for each submodule.

											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
01/15/2019		Ron Tecson			Initial
01/18/2019		Ron Tecson			Reload the Problem Code based from the update file provide by Shane on 1/17.
02/28/2019		Ron Tecson			Modified the script for MapleRoots migration.
02/28/2019		Ron Tecson			Per Shane, I am unaware of any new problem codes. If we need to add any in the future, I am OK with adding them manually.
03/19/2019		Ron Tecson			Modified to reload in MapleRoots. Place logs.
03/28/2019		Ron Tecson			Repointed to Production.
03/29/2019      Marty Corpus	    Executed for production.

DEPENDENCY:
	- Need to import "Submodule Mapping by Product.xls" file provided by Maria or Shane. Modify the excel file to include the Salesforce 18 Digit Char.
	- Once the Submodule Mapping by Product.xls has been modified, import the data to the Staging_SB database and name the table, PROBLEM_CODE_MAPPING.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Tritech_Prod

Exec SF_Refresh 'MC_Tritech_Prod', 'Product_Sub_Module__c', 'yes' ; --mc/3/29 ran this
--Exec SF_Replicate 'MC_Tritech_Prod', 'Product_Sub_Module__c';


Use SUPERION_Production
--Exec SF_Refresh 'PB_Superion_FullSB', 'Contact', 'yes'
--Exec SF_Refresh 'PB_Superion_FullSB', 'RecordType', 'yes' ;

Exec SF_Refresh 'RT_Superion_PROD', 'PRODUCT2', 'yes'

select count(*) from RT_Superion_MAPLEROOTS...product2 -- 3/19 26969  --27340 

select count(*) from RT_SUPERION_FULLSB...product2 -- 3/19 20022 -- 27340

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
Use Staging_PROD

--select * INTO Staging_SB.dbo.PROBLEM_CODE_MAPPING_01182019 from Staging_SB.dbo.PROBLEM_CODE_MAPPING
--drop table Staging_SB.dbo.PROBLEM_CODE_MAPPING

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ProblemCode__c_PreLoad_RT') 
DROP TABLE ProblemCode__c_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
Use Staging_PROD;

SELECT --product_name, count(*)
*
INTO Staging_PROD.dbo.ProblemCode__c_PreLoad_RT
FROM   ( SELECT Cast( '' AS NVARCHAR(18))                                    AS [ID],
                Cast( '' AS NVARCHAR(255))                                   AS Error,
                a.[product_sub_module_id]                                    AS Legacy_CRMID__c,
                'TriTech'                                                    AS Legacy_Source_System__c,
                'true'                                                       AS Migrated_Record__c,
                Concat( a.[product_family1], ' ', a.product_name )           AS [Name],--**mapped --use Col D + ' ' + Col I from excel file
                'true'                                                       AS Active__c,--**mapped: Default to True
				case when sPSM.Active__c = 'true' then 'false' else 'true' end   as Inactive__c, --per mapping Marty added 3/29
                a.product_name                                               AS Product_Original,
                prod.id                                                      AS Product__c,--**mapped
                --a.product_sub_module_name                                    AS ProblemCodeName__c,--**mapped use Col A from the excel file.
				sPSM.Name													 AS ProblemCodeName__c,
                'Mapping Excel file where CentralSquare Line(s) IS NOT null' AS Apps_Note,--MC added 1/15 for reference
                prod.acronym_list__c                                         AS Acronym_List,--MC added 1/15 for reference
                prod.product_group__c                                        AS Product_Group,--MC added 1/15 for reference
                prod.product_line__c                                         AS Product_Line,--MC added 1/15 for reference
                prod.NAME                                                    AS Product_Name --MC added 1/15 for reference
				--a.CentralSquare_Line										 AS CentralSquare_Line
         FROM   staging_sb.dbo.PROBLEM_CODE_MAPPING a --change this to the staging table (from excel file import)
                -- Product2
                LEFT OUTER JOIN SUPERION_production.dbo.PRODUCT2 prod ON a.[centralsquare_group] = prod.product_group__c AND a.centralsquare_line = prod.product_line__c --AND prod.acronym_list__c = 'legacy ttz'
				LEFT OUTER JOIN MC_Tritech_Prod...Product_Sub_Module__c sPSM ON CAST(a.[Product_Sub_Module_ID_18Digits] AS NVARCHAR(18)) = CAST(sPSM.Id AS NVARCHAR(18))
         WHERE  prod.eligible_for_support__c = 'true' AND
                product_line__c IS NOT NULL AND
                a.[centralsquare_group] <> 'Don''t move'
         UNION
         SELECT Cast( '' AS NVARCHAR(18))                                AS [ID],
                Cast( '' AS NVARCHAR(255))                               AS Error,
                a.[product_sub_module_id]                                AS Legacy_CRMID__c,
                'TriTech'                                                AS Legacy_Source_System__c,
                'true'                                                   AS Migrated_Record__c,
                Concat( a.[product_family1], ' ', a.product_name )       AS [Name],--**mapped --use Col D + ' ' + Col I from excel file
                'true'                                                   AS Active__c,--**mapped: Default to True
				'false'                                                  as Inactive__c, --added mc 3/29
                a.product_name                                           AS Product_Original,
                prod.id                                                  AS Product__c,--**mapped
                --a.product_sub_module_name                                    AS ProblemCodeName__c,--**mapped use Col A from the excel file.
				sPSM.Name												 AS ProblemCodeName__c,
                'Mapping Excel file where CentralSquare Line(s) IS null' AS Apps_Note,--MC added 1/15 for reference
                prod.acronym_list__c                                     AS Acronym_List,--MC added 1/15 for reference
                prod.product_group__c                                    AS Product_Group,--MC added 1/15 for reference
                prod.product_line__c                                     AS Product_Line,--MC added 1/15 for reference
                prod.NAME                                                AS Product_Name --MC added 1/15 for reference
				--,a.CentralSquare_Line									 AS CentralSquare_Line
         FROM   staging_prod.dbo.PROBLEM_CODE_MAPPING a --change this to the staging table (from excel file import) --MC 3/29 changed this line to point to stagin_prod.
                LEFT OUTER JOIN Superion_Production.dbo.PRODUCT2 prod ON a.[centralsquare_group] = prod.product_group__c --and a.CentralSquare_Line = Prod.Product_Line__c and prod.acronym_list__c = 'legacy ttz'
				LEFT OUTER JOIN MC_Tritech_Prod...Product_Sub_Module__c sPSM ON CAST(a.[Product_Sub_Module_ID_18Digits] AS NVARCHAR(18)) = CAST(sPSM.Id AS NVARCHAR(18))
         WHERE  prod.eligible_for_support__c = 'true'
                AND CentralSquare_Line is null
                AND a.[centralsquare_group] <> 'Don''t move' 
) x
order by ProblemCodeName__c

--(5775 row(s) affected) 3/29
-- 3/19 (5775 rows affected)
-- 2/28 (5892 rows affected)


---------------------------------------------------------------------------------
-- Drop Load table 
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='ProblemCode__c_Load_RT') 
DROP TABLE Staging_PROD.dbo.ProblemCode__c_Load_RT;

select * INTO Staging_PROD.dbo.ProblemCode__c_Load_RT from Staging_PROD.dbo.ProblemCode__c_PreLoad_RT
--(5775 row(s) affected)
-- 3/19 (5775 rows affected)
-- 2/28 (5892 rows affected)

select count(*) from Staging_PROD.dbo.ProblemCode__c_Load_RT

--------------------------------------------------------------------------------
-- Colcompare
--------------------------------------------------------------------------------

Exec SF_ColCompare 'Insert','RT_Superion_PROD','ProblemCode__c_Load_RT'

/************************************************* L   O   G   S *************************************************
3/29
--- Starting SF_ColCompare V3.6.7
Problems found with ProblemCode__c_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 147]
--- Ending SF_ColCompare. Operation FAILED.

ErrorDesc
Salesforce object ProblemCode__c does not contain column Legacy_Source_System__c
Salesforce object ProblemCode__c does not contain column Active__c
Salesforce object ProblemCode__c does not contain column Product_Original
Salesforce object ProblemCode__c does not contain column Apps_Note
Salesforce object ProblemCode__c does not contain column Acronym_List
Salesforce object ProblemCode__c does not contain column Product_Group
Salesforce object ProblemCode__c does not contain column Product_Line
Salesforce object ProblemCode__c does not contain column Product_Names

2/28
ErrorDesc
Salesforce object ProblemCode__c does not contain column Legacy_Source_System__c
Salesforce object ProblemCode__c does not contain column Active__c
Salesforce object ProblemCode__c does not contain column Product_Original
Salesforce object ProblemCode__c does not contain column Apps_Note
Salesforce object ProblemCode__c does not contain column Acronym_List
Salesforce object ProblemCode__c does not contain column Product_Group
Salesforce object ProblemCode__c does not contain column Product_Line
Salesforce object ProblemCode__c does not contain column Product_Name

*****************************************************************************************************************/

Exec sf_bulkops 'INSERT','RT_Superion_PROD','ProblemCode__c_Load_RT'

/************************************************* L   O   G   S *************************************************
-- 3/29:
--- Starting SF_BulkOps for ProblemCode__c_Load_RT V3.6.7
16:05:41: Run the DBAmp.exe program.
16:05:41: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
16:05:41: Inserting ProblemCode__c_Load_RT (SQL01 / Staging_PROD).
16:05:41: DBAmp is using the SQL Native Client.
16:05:42: SOAP Headers: 
16:05:42: Warning: Column 'Legacy_Source_System__c' ignored because it does not exist in the ProblemCode__c object.
16:05:42: Warning: Column 'Active__c' ignored because it does not exist in the ProblemCode__c object.
16:05:42: Warning: Column 'Product_Original' ignored because it does not exist in the ProblemCode__c object.
16:05:42: Warning: Column 'Apps_Note' ignored because it does not exist in the ProblemCode__c object.
16:05:42: Warning: Column 'Acronym_List' ignored because it does not exist in the ProblemCode__c object.
16:05:42: Warning: Column 'Product_Group' ignored because it does not exist in the ProblemCode__c object.
16:05:42: Warning: Column 'Product_Line' ignored because it does not exist in the ProblemCode__c object.
16:05:42: Warning: Column 'Product_Name' ignored because it does not exist in the ProblemCode__c object.
16:06:13: 5775 rows read from SQL Table.
16:06:13: 5775 rows successfully processed.
16:06:13: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


02/28

--- Starting SF_BulkOps for ProblemCode__c_Load_RT V3.6.7
17:10:08: Run the DBAmp.exe program.
17:10:08: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
17:10:08: Inserting ProblemCode__c_Load_RT (SQL01 / Staging_SB_MapleRoots).
17:10:08: DBAmp is using the SQL Native Client.
17:10:09: SOAP Headers: 
17:10:09: Warning: Column 'Legacy_Source_System__c' ignored because it does not exist in the ProblemCode__c object.
17:10:09: Warning: Column 'Active__c' ignored because it does not exist in the ProblemCode__c object.
17:10:09: Warning: Column 'Product_Original' ignored because it does not exist in the ProblemCode__c object.
17:10:09: Warning: Column 'Apps_Note' ignored because it does not exist in the ProblemCode__c object.
17:10:09: Warning: Column 'Acronym_List' ignored because it does not exist in the ProblemCode__c object.
17:10:09: Warning: Column 'Product_Group' ignored because it does not exist in the ProblemCode__c object.
17:10:09: Warning: Column 'Product_Line' ignored because it does not exist in the ProblemCode__c object.
17:10:09: Warning: Column 'Product_Name' ignored because it does not exist in the ProblemCode__c object.
17:11:08: 5892 rows read from SQL Table.
17:11:08: 5892 rows successfully processed.
17:11:08: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


--- Starting SF_BulkOps for ProblemCode__c_Load_RT V3.6.9
15:15:31: Run the DBAmp.exe program.
15:15:31: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:15:31: Inserting ProblemCode__c_Load_RT (SQL01 / Staging_SB).
15:15:32: DBAmp is using the SQL Native Client.
15:15:32: SOAP Headers: 
15:15:32: Warning: Column 'Legacy_Source_System__c' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Active__c' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Original' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Apps_Note' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Acronym_List' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Group' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Line' ignored because it does not exist in the ProblemCode__c object.
15:15:32: Warning: Column 'Product_Name' ignored because it does not exist in the ProblemCode__c object.
15:17:09: 8884 rows read from SQL Table.
15:17:09: 8884 rows successfully processed.
15:17:09: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

*****************************************************************************************************************/

---------------------------------------------------------------------------------
-- Validate for any Error / Success
---------------------------------------------------------------------------------

select error, count(*) from ProblemCode__c_Load_RT
group by error

select ID --INTO staging_sb.dbo.ProblemCode__c_For_Deletion_RT
from PB_Superion_FULLSB...ProblemCode__c where Migrated_Record__c = 'true'

alter table staging_sb.dbo.ProblemCode__c_For_Deletion_RT
add Error NVARCHAR(255)

select * from staging_sb.dbo.ProblemCode__c_For_Deletion_RT

USE Staging_SB

Exec sf_bulkops 'DELETE','PB_Superion_FULLSB','ProblemCode__c_For_Deletion_RT'

select * into staging_sb.dbo.PROBLEM_CODE_MAPPING_V1 from staging_sb.dbo.PROBLEM_CODE_MAPPING

select * from  staging_sb.dbo.PROBLEM_CODE_MAPPING