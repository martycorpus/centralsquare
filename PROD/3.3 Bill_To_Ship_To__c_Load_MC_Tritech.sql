/*
-- Author     : Marty Corpus
-- Date       : 01/4/2019
-- Description: Migrate custom account address fields Tritech Org to Superion Salesforce BilltoShipto object for Non Customer Accounts (prospects).
Requirement Number:REQ-0347
--Change History: SL 2/4: Added CTE and LEFT JOIN to execute the accounts that are already loaded as a part of 3.2 Bill_To_Ship_To__c_Load_SL_Tritech.sql
2019-03-10 MartyC. Added Legacy_Source_System__c = 'Tritech', Migrated_Record__c = 'true'
2019-04-04 Moved and Executed for production.

-------------------------------------------------------------------------------
-- Execution Log:
2019-03-10 MartyC. Wipe and Reload.




BILL TO's LOAD:
--checks:
-- SELECT * FROM Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC WHERE Account__c = 'ID NOT FOUND' --12
-- select Account_ID_TT_orig , count(*) from Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC group by Account_ID_TT_orig having count(*) > 1 (0 row(s) affected)


-- Exec SF_ColCompare 'Insert','MC_SUPERION_PROD','Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC' 
--- Starting SF_ColCompare V3.7.7
Problems found with Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 22]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object Bill_To_Ship_To__c does not contain column Account_ID_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account_Name_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account__C_Existing_B2
Salesforce object Bill_To_Ship_To__c does not contain column Ident

-- Exec SF_Bulkops 'Insert','MC_SUPERION_PROD','Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC'
--- Starting SF_BulkOps for Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC V3.7.7
18:19:25: Run the DBAmp.exe program.
18:19:25: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:19:25: Inserting Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC (SQL01 / Staging_PROD).
18:19:25: DBAmp is using the SQL Native Client.
18:19:26: SOAP Headers: 
18:19:26: Warning: Column 'Account_ID_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
18:19:26: Warning: Column 'Account_Name_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
18:19:26: Warning: Column 'Account__C_Existing_B2' ignored because it does not exist in the Bill_To_Ship_To__c object.
18:19:26: Warning: Column 'Ident' ignored because it does not exist in the Bill_To_Ship_To__c object.
18:20:52: 19236 rows read from SQL Table.
18:20:52: 42 rows failed. See Error column of row for more information.
18:20:52: 19194 rows successfully processed.
18:20:52: Errors occurred. See Error column of row for more information.
18:20:52: Percent Failed = 0.200.
18:20:52: Error: DBAmp.exe was unsuccessful.
18:20:52: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC "SQL01"  "Staging_PROD"  "MC_SUPERION_PROD"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 33]
SF_BulkOps Error: 18:19:25: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC18:19:25: Inserting Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC (SQL01 / Staging_PROD).18:19:25: DBAmp is using the SQL Native Client.18:19:26: SOAP Headers: 18:19:26: Warning: Column 'Account_ID_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.18:19:26: Warning: Column 'Account_Name_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.18:19:26: Warning: Column 'Account__C_Existing_B2' ignored because it does not exist in the Bill_To_Ship_To__c object.18:19:26: Warning: Column 'Ident' ignored because it does not exist in the Bill_To_Ship_To__c object.18:20:52: 19236 rows read from SQL Table.18:20:52: 42 rows failed. See Error column of row for more information.18:20:52: 19194 rows successfully processed.18:20:52: Errors occurred. See Error column of row for more information.

SELECT ERROR , COUNT(*) FROM 
Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC
GROUP BY ERROR;

SHIP TO's LOAD:
--checks:
-- SELECT * FROM Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC WHERE Account__c = 'ID NOT FOUND' --12
-- select Account_ID_TT_orig , count(*) from Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC group by Account_ID_TT_orig having count(*) > 1

-- Exec SF_ColCompare 'Insert','MC_SUPERION_PROD','Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC'
--- Starting SF_ColCompare V3.7.7
Problems found with Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 64]
--- Ending SF_ColCompare. Operation FAILED.

ErrorDesc
Salesforce object Bill_To_Ship_To__c does not contain column Account_ID_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account_Name_TT_orig
Salesforce object Bill_To_Ship_To__c does not contain column Account__C_Existing_s2
Salesforce object Bill_To_Ship_To__c does not contain column Ident

-- Exec SF_Bulkops 'Insert','MC_SUPERION_PROD','Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC'
----- Starting SF_BulkOps for Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC V3.7.7
--18:28:58: Run the DBAmp.exe program.
--18:28:59: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
--18:28:59: Inserting Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC (SQL01 / Staging_PROD).
--18:28:59: DBAmp is using the SQL Native Client.
--18:29:00: SOAP Headers: 
--18:29:00: Warning: Column 'Account_ID_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
--18:29:00: Warning: Column 'Account_Name_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
--18:29:00: Warning: Column 'Account__C_Existing_s2' ignored because it does not exist in the Bill_To_Ship_To__c object.
--18:29:00: Warning: Column 'Ident' ignored because it does not exist in the Bill_To_Ship_To__c object.
--18:30:23: 19236 rows read from SQL Table.
--18:30:23: 139 rows failed. See Error column of row for more information.
--18:30:23: 19097 rows successfully processed.
--18:30:23: Errors occurred. See Error column of row for more information.
--18:30:23: Percent Failed = 0.700.
--18:30:23: Error: DBAmp.exe was unsuccessful.
--18:30:23: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC "SQL01"  "Staging_PROD"  "MC_SUPERION_PROD"  " " 
----- Ending SF_BulkOps. Operation FAILED.
--Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 76]
--SF_BulkOps Error: 18:28:59: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC18:28:59: Inserting Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC (SQL01 / Staging_PROD).18:28:59: DBAmp is using the SQL Native Client.18:29:00: SOAP Headers: 18:29:00: Warning: Column 'Account_ID_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.18:29:00: Warning: Column 'Account_Name_TT_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.18:29:00: Warning: Column 'Account__C_Existing_s2' ignored because it does not exist in the Bill_To_Ship_To__c object.18:29:00: Warning: Column 'Ident' ignored because it does not exist in the Bill_To_Ship_To__c object.18:30:23: 19236 rows read from SQL Table.18:30:23: 139 rows failed. See Error column of row for more information.18:30:23: 19097 rows successfully processed.18:30:23: Errors occurred. See Error column of row for more information.






*/
--Use Tritech_prod

exec sf_refresh 'EN_TRITECH_PROD','Account','Yes'


--use [staging_prod]

exec sf_refresh 'MC_SUPERION_PROD','Bill_To_Ship_To__c','Yes'

--create B2 address records.

--DROP TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC

Use Superion_Production;

;With CteGP_B2 as
(
select Distinct Account__C,Bill_To__c from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.' and Bill_To__c=1
) --Added CTE by SL on 2/4 
SELECT Cast( NULL AS NCHAR(18))                  AS id,
       Cast( NULL AS NVARCHAR(255))              AS Error,
       COALESCE(t2.id,'ID NOT FOUND')            AS Account__c,
       t1.id                                     AS Account_ID_TT_orig,
       'true'                                    AS Bill_To__c,
       t1.NAME                                   AS Account_Name_TT_orig,
       t1.mailing_billing_street_wmp__c          AS Street_1__c,
       t1.mailing_billing_city_wmp__c            AS City__c,
       t1.mailing_billing_country_wmp__c         AS Country__c,
       t1.mailing_billing_state_wmp__c           AS State__c,
       t1.mailing_billing_zip_postal_code_wmp__c AS Zip_Postal_Code__c,
	   b2.Account__C  as Account__C_Existing_B2,
	   Legacy_Source_System__c = 'Tritech',
	   Migrated_Record__c = 'true'
INTO Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC
FROM   [Tritech_PROD].[dbo].ACCOUNT t1
       LEFT JOIN MC_SUPERION_PROD...ACCOUNT t2
              ON t2.legacysfdcaccountid__c = t1.id
       LEFT JOIN CteGP_B2 b2
	   on t2.id=b2.Account__C --Added by SL on 2/4 
WHERE  t1.Client__c = 'false' 
and b2.Account__C is  null --Added by SL on 2/4 
--(19236 row(s) affected) MC 4/4 PROD. 00:01:21

; --00:01:05


--18723
--2824

select * from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.'

--(21460 row(s) affected) 1/4 MC E2E.

ALTER TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC ADD Ident int Identity(1,1);




--create S2 address records.

--DROP TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC
;With CteGP_S2 as
(
select distinct Account__C,Ship_to__c from Bill_To_Ship_To__c_Tritech_Excel_load
where error='Operation Successful.' and Ship_to__c=1
) --Added CTE by SL on 2/4 
SELECT Cast( NULL AS NCHAR(18))       AS id,
       Cast( NULL AS NVARCHAR(255))   AS Error,
       COALESCE(t2.id,'ID NOT FOUND') AS Account__c,
       t1.id                          AS Account_ID_TT_orig,
       t1.NAME                        AS Account_Name_TT_orig,
       'true'                         AS Ship_To__c,
       shipping_street_wmp__c         AS Street_1__c,
       shipping_city_wmp__c           AS City__c,
       shipping_country_wmp__c        AS Country__c,
       shipping_state_wmp__c          AS State__c,
       shipping_zip_postal_code__c    AS Zip_Postal_Code__c,
	   s2.Account__C  as Account__C_Existing_s2,
	   Legacy_Source_System__c = 'Tritech',
	   Migrated_Record__c = 'true'
INTO Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC
FROM   [Tritech_PROD].[dbo].ACCOUNT t1
       LEFT JOIN MC_SUPERION_PROD...ACCOUNT t2
              ON t2.legacysfdcaccountid__c = t1.id
	   LEFT JOIN CteGP_S2 s2
	   on t2.id=s2.Account__C --Added by SL on 2/4 
WHERE  t1.client__c = 'false' 
and s2.Account__C is null  --Added by SL on 2/4 
-- (19236 row(s) affected) 4/4 MC PROD.

; --(21460 row(s) affected) 1/4 E2E MC.

ALTER TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC ADD Ident int Identity(1,1);


SELECT * FROM Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC