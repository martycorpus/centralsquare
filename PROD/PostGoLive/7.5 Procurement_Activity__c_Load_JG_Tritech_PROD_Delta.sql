 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Procurement_Activity__c----------
Requirement No  :
Total Records   :  
Scope:  Migrate all data from the TriTech Procurement_Activity__c object that relates to a migrated opportunity.
        Extract related fields from the Superion Opportunity records and re-migrate back
		into the Superion Procurement_Activity__c
 */

  /*
Use Tritech_PROD
EXEC SF_Refresh 'EN_Tritech_PROD','Procurement_Activity__c','Yes'


Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Opportunity','Yes'

*/ 
 
 Use Staging_PROD;

 DECLARE @Default NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'CentralSquare API');

 --Drop table Procurement_Activity__c_Tritech_SFDC_Preload_Delta;

 Select 
 
 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =procurement.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,Assignments_Due_Date__c=Assignments_Due_Date__c

,Backup_Proposal_Manager__c_orig=Backup_Proposal_Manager__c
,Backup_Proposal_Manager__c=prpslmngrusr.Id

,Bid_Bond_Amount__c=Bid_Bond_Amount__c
,Bid_Bond_Issued__c=Bid_Bond_Issued__c
,Bid_Bond_Percentage__c=Bid_Bond_Percentage__c
,Bid_Bond_Required__c=procurement.Bid_Bond_Required__c
,Bid_Bond_Returned__c=Bid_Bond_Returned__c
,Bidder_s_Conference_Date__c=Bidder_s_Conference_Date__c
,Bidder_s_Conference_Required__c=Bidder_s_Conference_Required__c
--,CAD_Dispatcher_Call_Taker_Seats__c=CAD_Dispatcher_Call_Taker_Seats__c
--,Closed_Date__c=Closed_Date__c
,Common_Partners__c=Common_Partners__c

,Conference_Attendee__c_orig=Conference_Attendee__c
,Conference_Attendee__c=cnfrncatnduser.Id

,Consultant__c=Consultant__c

,CreatedById_orig=procurement.CreatedById
,CreatedById=IIF(crtdbyidusr.Id IS NULL,@Default,crtdbyidusr.Id)

,CreatedDate=procurement.CreatedDate
--,Current_Opportunity_Stage__c=Current_Opportunity_Stage__c
,Deadline_for_Vendor_Questions__c=Deadline_for_Vendor_Questions__c
,Extension__c=procurement.Extension__c
,Functional_Respondent_CAD_Mobile_911__c=Functional_Respondent_CAD_Mobile_911__c
,Functional_Respondent_RMS_FBR_Jail_IQ__c=Functional_Respondent_RMS_FBR_Jail_IQ__c
,Green_Team_Date__c=Green_Team_Date__c
--,Procurement_Activity__c=Id
,If_Consultant_Other__c=If_Consultant_Other__c
,Implementation_Definition_Review_Date__c=Implementation_Definition_Review_Date__c
,Internal_RFx_Location__c=Internal_RFx_Location__c
--,Procurement_Activity__c=procurement.IsDeleted
--,Jail_Beds__c=Jail_Beds__c
,Kickoff_Date__c=Kickoff_Date__c
--,Procurement_Activity__c=procurement.LastActivityDate
--,Procurement_Activity__c=procurement.LastModifiedById
--,Procurement_Activity__c=procurement.LastModifiedDate
--,Procurement_Activity__c=procurement.LastReferencedDate
--,Procurement_Activity__c=procurement.LastViewedDate
,Letter_of_Intent_Due_Date__c=Letter_of_Intent_Due_Date__c
,Letter_of_Intent_Required__c=Letter_of_Intent_Required__c
,Name=procurement.Name
,No_Bid_Letter_Due_Date__c=No_Bid_Letter_Due_Date__c
,No_Bid_Letter_Required__c=No_Bid_Letter_Required__c
,Number_of_Electronic_Copies_Required__c=Number_of_Electronic_Copies_Required__c
,Number_of_Hard_Copies_Required__c=Number_of_Hard_Copies_Required__c
,Number_of_Sworn_Officers__c=procurement.Number_of_Sworn_Officers__c

,Opportunity__c_orig=Opportunity__c
,Opportunity__c=oppty.Id

,Performance_Bond_Percentage__c=Performance_Bond_Percentage__c
,Performance_Bond_Required__c=Performance_Bond_Required__c
,Pre_Green_Team_Meeting__c=Pre_Green_Team_Meeting__c

,Pricing_Manager__c_orig=Pricing_Manager__c
,Pricing_Manager__c=prcngmngr.Id

,Procurement_Type__c=Procurement_Type__c
,Project_Size__c=Project_Size__c
,Project_Type__c=Project_Type__c

,Proposal_Manager__c_orig=procurement.Proposal_Manager__c
,Proposal_Manager__c=prpslmngr.Id

,Proposal_Stage__c=Proposal_Stage__c
,Proposal_Status__c=Proposal_Status__c
,Red_Team_Date__c=Red_Team_Date__c
,Request_Received__c=Request_Received__c
,Response_Due_Date__c=Response_Due_Date__c
,Response_Ship_Date__c=Response_Ship_Date__c
,Response_Submitted__c=Response_Submitted__c

,RFx_Contact__c_orig=RFx_Contact__c
,RFx_Contact__c=cntct.Id

,RFx_Contact_Address__c=RFx_Contact_Address__c
,RFx_Contact_Email__c=RFx_Contact_Email__c
,RFx_Contact_Number__c=RFx_Contact_Number__c
--,Selected_Date__c=Selected_Date__c
--,Shortlisted_Date__c=procurement.Shortlisted_Date__c
,Solution_Definition_Review_Date__c=Solution_Definition_Review_Date__c
,Solutions_Architect__c=Solutions_Architect__c
--,Procurement_Activity__c=procurement.SystemModstamp
,Z_AE_Have_they_seen_the_product__c=Z_AE_Have_they_seen_the_product__c
,Z_AE_is_it_a_ZT_Spec__c=Z_AE_is_it_a_ZT_Spec__c
,Z_AE_of_visits__c=Z_AE_of_visits__c
,Z_AE_What_are_our_win_themes__c=Z_AE_What_are_our_win_themes__c
,Z_AE_Who_are_the_decision_makers__c=Z_AE_Who_are_the_decision_makers__c
,Z_Are_there_product_gaps__c=Z_Are_there_product_gaps__c
,Z_Content_creation_notes__c=Z_Content_creation_notes__c
,Z_Contract_concerns__c=Z_Contract_concerns__c
,Z_Core_State__c=Z_Core_State__c
,Z_Decision__c=Z_Decision__c
,Z_Decision_Comments__c=Z_Decision_Comments__c
,Z_Do_we_have_to_be_on_state_contract__c=Z_Do_we_have_to_be_on_state_contract__c
,Z_Meet_reference_requirements__c=Z_Meet_reference_requirements__c
,Z_Other_Agencies__c=Z_Other_Agencies__c
,Z_Product_Experts__c=Z_Product_Experts__c
,Z_Project_timeline_concerns__c=Z_Project_timeline_concerns__c
,Z_RFx_timeline_concerns__c=Z_RFx_timeline_concerns__c
,Z_State_Specific_things_needed__c=Z_State_Specific_things_needed__c
,Z_Summary__c=Z_Summary__c

 into Procurement_Activity__c_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.Procurement_Activity__c procurement
inner join Staging_PROD.dbo.Opportunity_Tritech_SFDC_load_Delta oppty
on oppty.Legacy_Opportunity_ID__c=procurement.Opportunity__c

--Fetching Backup_Proposal_Manager__c (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] prpslmngrusr on
prpslmngrusr.Legacy_Tritech_Id__c=procurement.Backup_Proposal_Manager__c

--Fetching Conference_Attendee__c (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] cnfrncatnduser on
cnfrncatnduser.Legacy_Tritech_Id__c=procurement.Conference_Attendee__c

--Fetching CreatedById (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] crtdbyidusr on
crtdbyidusr.Legacy_Tritech_Id__c=procurement.CreatedById

--Fetching Pricing_Manager__c (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] prcngmngr on
prcngmngr.Legacy_Tritech_Id__c=procurement.Pricing_Manager__c

--Fetching Proposal_Manager__c (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] prpslmngr on
prpslmngr.Legacy_Tritech_Id__c=procurement.Proposal_Manager__c

--Fetching RFx_Contact__c (Contact Lookup)
left join SUPERION_PRODUCTION.dbo.Contact cntct on
cntct.Legacy_ID__c=procurement.RFx_Contact__c;
--0
--------------------------------------------------------------------------------------

select * from Staging_PROD.dbo.Opportunity_Tritech_SFDC_load_Delta oppty;--1136

select count(*) from Tritech_PROD.dbo.Procurement_Activity__c a
,TT_SFDC_Missing_Opportunities b
where a.Opportunity__c=b.tritechOpportunityID;--0

select * from TT_SFDC_Missing_Opportunities;

Select count(*) from Tritech_PROD.dbo.Procurement_Activity__c;--817

Select count(*) from Staging_PROD.dbo.Procurement_Activity__c_Tritech_SFDC_Preload_Delta;--0

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.Procurement_Activity__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1;--0

--Drop table Staging_PROD.dbo.Procurement_Activity__c_Tritech_SFDC_Load_Delta;

Select * into 
Procurement_Activity__c_Tritech_SFDC_Load_Delta
from Procurement_Activity__c_Tritech_SFDC_Preload_Delta;--0

Select * from Staging_PROD.dbo.Procurement_Activity__c_Tritech_SFDC_Load_Delta;

--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'Procurement_Activity__c_Tritech_SFDC_Load_Delta' 

/*
Salesforce object Procurement_Activity__c does not contain column Backup_Proposal_Manager__c_orig
Salesforce object Procurement_Activity__c does not contain column Conference_Attendee__c_orig
Salesforce object Procurement_Activity__c does not contain column CreatedById_orig
Salesforce object Procurement_Activity__c does not contain column Opportunity__c_orig
Salesforce object Procurement_Activity__c does not contain column Pricing_Manager__c_orig
Salesforce object Procurement_Activity__c does not contain column Proposal_Manager__c_orig
Salesforce object Procurement_Activity__c does not contain column RFx_Contact__c_orig
Column Number_of_Sworn_Officers__c is not insertable into the salesforce object Procurement_Activity__c
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','Procurement_Activity__c_Tritech_SFDC_Load_Delta'

select * 
from Procurement_Activity__c_Tritech_SFDC_Load_Delta
where error<>'Operation Successful.';--14

--drop table Procurement_Activity__c_Tritech_SFDC_Load_errors_Delta
select * 
into Procurement_Activity__c_Tritech_SFDC_Load_errors_Delta
from Procurement_Activity__c_Tritech_SFDC_Load_Delta
where error<>'Operation Successful.';--14

--drop table Procurement_Activity__c_Tritech_SFDC_Load_errors_bkp_Delta
select * 
into Procurement_Activity__c_Tritech_SFDC_Load_errors_bkp_Delta
from Procurement_Activity__c_Tritech_SFDC_Load_Delta
where error<>'Operation Successful.';--14

Select Functional_Respondent_CAD_Mobile_911__c,*
--Update a set Functional_Respondent_CAD_Mobile_911__c=NULL
from Procurement_Activity__c_Tritech_SFDC_Load_errors_Delta  a where error<>'Operation Successful.' 
and Functional_Respondent_CAD_Mobile_911__c like '%Jeff Arp%';--9


Select Functional_Respondent_RMS_FBR_Jail_IQ__c,*
--Update a set Functional_Respondent_RMS_FBR_Jail_IQ__c=NULL
from Procurement_Activity__c_Tritech_SFDC_Load_errors_Delta  a where error<>'Operation Successful.' 
and Functional_Respondent_RMS_FBR_Jail_IQ__c like '%Steve Angell%';--5




--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','Procurement_Activity__c_Tritech_SFDC_Load_errors_Delta'

--delete 
from Procurement_Activity__c_Tritech_SFDC_Load_Delta
where error<>'Operation Successful.'; --14

--insert into Procurement_Activity__c_Tritech_SFDC_Load_Delta
select * from Procurement_Activity__c_Tritech_SFDC_Load_errors_Delta;--43

Select error,count(*) from Procurement_Activity__c_Tritech_SFDC_Load_Delta
group by error;

/*

--drop table Procurement_Activity__c_Tritech_SFDC_Load_errors2
select * 
into Procurement_Activity__c_Tritech_SFDC_Load_errors2
from Procurement_Activity__c_Tritech_SFDC_Load
where error<>'Operation Successful.';--5

--drop table Procurement_Activity__c_Tritech_SFDC_Load_errors2_bkp
select * 
into Procurement_Activity__c_Tritech_SFDC_Load_errors2_bkp
from Procurement_Activity__c_Tritech_SFDC_Load
where error<>'Operation Successful.';--5

Select Functional_Respondent_RMS_FBR_Jail_IQ__c,*
--Update a set Functional_Respondent_RMS_FBR_Jail_IQ__c=NULL
from Procurement_Activity__c_Tritech_SFDC_Load_errors2  a where error<>'Operation Successful.' 
and Functional_Respondent_RMS_FBR_Jail_IQ__c like '%Steve Angell%';--5


--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','Procurement_Activity__c_Tritech_SFDC_Load_errors2'

--delete 
from Procurement_Activity__c_Tritech_SFDC_Load
where error<>'Operation Successful.'; --5

--insert into Procurement_Activity__c_Tritech_SFDC_Load
select * from Procurement_Activity__c_Tritech_SFDC_Load_errors2;--5
*/
----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


---hotfix Proposal_Stage__c

Select a.id,a.error, a.Proposal_Stage__c Proposal_Stage__c_old,b.Proposal_Stage__c
into Procurement_Activity__c_Tritech_SFDC_Load_hotfix_SL
from Procurement_Activity__c_Tritech_SFDC_Load   a 
,Procurement_Activity__c_Tritech_SFDC_Load_errors_bkp b
where 1=1
and a.Legacy_id__c=b.Legacy_id__c
and b.Proposal_Stage__c IN ('Production','Solution Development','Planning');--32 

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD ','Procurement_Activity__c_Tritech_SFDC_Load_hotfix_SL'



select * from [Superion_Production].dbo.Procurement_Activity__c
where migrated_record__C='true' and legacy_id__C is not null;--1382

/*
--delete---

select id,error,legacy_id__C
into Procurement_Activity__c_Tritech_SFDC_Delete_SL
 from Procurement_Activity__c_Tritech_SFDC_load
where error='Operation Successful.';--1402


--Exec SF_BulkOps 'Delete','SL_SUPERION_PROD ','Procurement_Activity__c_Tritech_SFDC_Delete_SL'


select error,count(*) from Procurement_Activity__c_Tritech_SFDC_Delete_SL
group by error
*/

