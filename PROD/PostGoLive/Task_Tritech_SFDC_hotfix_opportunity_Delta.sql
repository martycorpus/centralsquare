/*
Use Tritech_PROD
EXEC SF_RefreshIAD 'EN_TRITECH_PROD','task','Yes'
select count(*) from tritech_prod.dbo.[Task]--1694717

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD ','Opportunity','yes'
EXEC SF_Refresh 'SL_SUPERION_PROD ','Sales_Request__c','yes'
EXEC SF_Refresh 'SL_SUPERION_PROD ','Task','subset'
*/




use Staging_PROD 
go
-- drop table Task_Tritech_SFDC_Preload_Delta
 declare @defaultuser nvarchar(18)
 =(select  id from SUPERION_PRODUCTION.dbo.[user] where name = 'CentralSquare API')
;With CteWhatData as
(
select
a.Legacy_Opportunity_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Opportunity' as What_parent_object_name 
from SUPERION_PRODUCTION.dbo.Opportunity a
inner join TT_SFDC_Missing_Opportunities b
on a.Legacy_Opportunity_ID__c=b.tritechOpportunityID
where 1=1
and a.Legacy_Opportunity_ID__c is not null
union
select
a.Legacy_Record_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Sales_Request__c' as What_parent_object_name 
from SUPERION_PRODUCTION.dbo.Sales_Request__c a
inner join SUPERION_PRODUCTION.dbo.Opportunity b
on a.opportunity__C=b.id
inner join TT_SFDC_Missing_Opportunities c
on b.Legacy_Opportunity_ID__c=c.tritechOpportunityID
where 1=1
and a.Legacy_Record_ID__c is not null
) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from SUPERION_PRODUCTION.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null	 and migrated_record__C='true'			
 )   

SELECT  
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_task.Id
				,Legacy_Source__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_task.[Account_Name__c]
														  --,tr_task.[Account_Name_DE__c]
				,AccountId_orig							= tr_task.[AccountId]
				,AccountId								= Tar_ACcount.ID
				,Activity_Type_For_Reporting__c			= tr_task.[Activity_Type_For_Reporting__c]
				,ActivityDate							= tr_task.[ActivityDate]
														  --,tr_task.[Brand__c]
				,CallDisposition						= tr_task.[CallDisposition]
				,CallDurationInSeconds					= tr_task.[CallDurationInSeconds]
				,CallObject								= tr_task.[CallObject]
				,CallType								= tr_task.[CallType]
														  --,tr_task.[Completed_Date_Time__c]
														  --,tr_task.[Contact_Count__c]
				,CreatedById_orig						= tr_task.[CreatedById]
				,legacy_createdby_name=Legacyuser.name
				,CreatedById_target							= Target_Create.ID
				,CreatedById							= iif(Target_Create.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_task.[CreatedDate]
														 -- ,tr_task.[Customer_Risk_Level__c]
				--,Description_orig							= tr_task.[Description]
														  --,tr_task.[DummyTouchField__c]
				,Id_orig								= tr_task.[Id]
				,Isarchieved__orig = tr_task.[IsArchived]
				,IsClosed_orig=tr_task.[IsClosed]
			    ,IsDeleted_orig=    tr_task.[IsDeleted]
														  --,tr_task.[IsHighPriority]
				,IsRecurrence							= tr_task.[IsRecurrence]
				,IsReminderSet							= tr_task.[IsReminderSet]
														  --,tr_task.[LastModifiedById]
														  --,tr_task.[LastModifiedDate]
				,OwnerId_orig							= tr_task.[OwnerId]
				,legacy_owner_name=Legacyuser1.name
				,OwnerId_target							= Target_Owner.ID
				,OwnerId							    = iif(Target_owner.id is null,@defaultuser
																		--iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,Priority								= tr_task.[Priority]
				,RecurrenceActivityId_orig				= tr_task.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_task.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_task.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_task.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_task.[RecurrenceInstance]
				,RecurrenceInterval						= tr_task.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_task.[RecurrenceMonthOfYear]
				,RecurrenceRegeneratedType				= tr_task.[RecurrenceRegeneratedType]
				,RecurrenceStartDateOnly				= tr_task.[RecurrenceStartDateOnly]
				,RecurrenceTimeZoneSidKey				= tr_task.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_task.[RecurrenceType]
				,ReminderDateTime						= tr_task.[ReminderDateTime]
				,Resolution_Notes__c					= tr_task.[Resolution_Notes__c]
														  --,tr_task.[Solution_s__c]
				,Status									= tr_task.[Status]
				,Subject								= tr_task.[Subject]
														  --,tr_task.[SystemModstamp]
				--,Description							= iif(tr_task.[Task_Notes__c] is not null,concat(tr_task.[Createddate],tr_task.[Task_Notes__c]),null)
				,TaskSubtype							= tr_task.[TaskSubtype]
														 -- ,tr_task.[Time_Elapsed__c]
				,Type									= tr_task.[Type]
														  --,tr_task.[WhatCount]
				,whatId_orig							= tr_task.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
				    
														  --,tr_task.[WhoCount]
				,WhoID_orig								= tr_task.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_task.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_task.[Description]
				,Description_Comments_summary__C_orig = tr_task.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_task.[Task_Notes__c]
				--,[Description]  = concat( iif(tr_task.[Description] is not null,'Description::',''),tr_task.description,CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_task.createddate),''),tr_task.[Comments_Summary__c],CHAR(13)+CHAR(10),
				--						  iif(tr_task.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_task.createddate),''),tr_task.[Task_Notes__c]
				--						  )
				--,DESCRIPTION = IIF(tr_task.Description IS NOT NULL
				--							,IIF(tr_task.tASK_nOTES__c IS NOT NULL, 
				--								CONCAT(tr_task.DESCRIPTION,CHAR(13)+char(10),tr_task.CREATEDDATE,' ',tr_task.task_notes__c),tr_task.description),null)
				,DESCRIPTION=concat(isnull(tr_task.Description,''),
				iif(tr_task.Task_Notes__c IS NOT NULL,concat(CHAR(13)+char(10)+cast(tr_task.CREATEDDATE as nvarchar(40))+' ',
				cast(tr_task.Task_Notes__c as nvarchar(max))),''))
               -- ,tr_email_id=tr_email.id
			    ,sup_task_id= sup_task.id
				  ,sup_task_legacy_id__C=sup_task.legacy_id__C
				   ,sup_task_whatid=sup_task.whatid
				    ,sup_task_whoid=sup_task.whoid
							into Task_Tritech_SFDC_Preload_Delta
  FROM [Tritech_PROD].[dbo].[Task] tr_task
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_task.WhatId
   -----------------------------------------------------------------------------------------------------------------------
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_task.WhoId
  -----------------------------------------------------------------------------------------------------------------------
    left join SUPERION_PRODUCTION.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_task.AccountId
--and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join SUPERION_PRODUCTION.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_task.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join SUPERION_PRODUCTION.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_task.OwnerID
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_task.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_task.OwnerId
    -----------------------------------------------------------------------------------------------------------------------------
     -----------------------------------------------------------------------------------------------------------------------------
 left outer join  Superion_Production.dbo.[task] sup_task
 on tr_task.id= sup_task.Legacy_id__c 

   where 1=1
  and  tr_task.IsDeleted='False' and Legacyuser1.name <>'Act-On Sync'
  and (Wt.parent_id IS NOT NULL )
  
 ;--1557

  --()--3 :42 mins

 -- COUNT OF THE TASK RECORDS
select count(*) 
from Task_Tritech_SFDC_Preload_Delta;--1557
--   

select count(*) from tritech_prod.dbo.task;--1694717



-- CHECK FOR THE DUPLICATES.
select Legacy_ID__c,Count(*) from Task_Tritech_SFDC_Preload_Delta
group by Legacy_ID__c
having count(*)>1;--0

--drop table Task_Tritech_SFDC_insert_Delta
select * 
into Task_Tritech_SFDC_insert_Delta
from Task_Tritech_SFDC_Preload_Delta a 
where 1=1
 and (whoid is not null or whatid is not null)
 and sup_task_id is null;--1430
 
 select * from Task_Tritech_SFDC_insert_Delta;

 

--exec SF_Colcompare 'Insert','SL_SUPERION_PROD ', 'Task_Tritech_SFDC_insert_Delta' 

--exec SF_Bulkops 'Insert','SL_SUPERION_PROD ', 'Task_Tritech_SFDC_insert_Delta' 

/*
Salesforce object Task does not contain column AccountId_orig
Salesforce object Task does not contain column CreatedById_orig
Salesforce object Task does not contain column legacy_createdby_name
Salesforce object Task does not contain column CreatedById_target
Salesforce object Task does not contain column Id_orig
Salesforce object Task does not contain column Isarchieved__orig
Salesforce object Task does not contain column IsClosed_orig
Salesforce object Task does not contain column IsDeleted_orig
Salesforce object Task does not contain column OwnerId_orig
Salesforce object Task does not contain column legacy_owner_name
Salesforce object Task does not contain column OwnerId_target
Salesforce object Task does not contain column RecurrenceActivityId_orig
Salesforce object Task does not contain column whatId_orig
Salesforce object Task does not contain column whatid_parent_object_name
Salesforce object Task does not contain column whatid_legacy_id_orig
Salesforce object Task does not contain column WhoID_orig
Salesforce object Task does not contain column whoId_parent_object_name
Salesforce object Task does not contain column whoId_legacy_id_orig
Salesforce object Task does not contain column Logged_Wellness_Check__c
Salesforce object Task does not contain column Description__orig
Salesforce object Task does not contain column Description_Comments_summary__C_orig
Salesforce object Task does not contain column Description_Task_notes__C_orig
Salesforce object Task does not contain column sup_task_id
Salesforce object Task does not contain column sup_task_legacy_id__C
Salesforce object Task does not contain column sup_task_whatid
Salesforce object Task does not contain column sup_task_whoid
Column AccountId is not insertable into the salesforce object Task



*/


select error,count(*) from Task_Tritech_SFDC_insert_Delta
group by error;

-------------------------Update----

--drop table task_Tritech_SFDC_Update_delta
select id= sup_task_id,error,whatid,
sup_task_legacy_id__C,sup_task_whatid,sup_task_whoid
into task_Tritech_SFDC_Update_delta
from Task_Tritech_SFDC_Preload_Delta a 
where 1=1
 and (whoid is not null or whatid is not null)
 and sup_task_id is not null and sup_task_whatid is null and sup_task_whoid=whoid  ;--127
 
select * from task_Tritech_SFDC_Update_delta;

--exec SF_Colcompare 'Update','SL_SUPERION_PROD ', 'task_Tritech_SFDC_Update_delta' 

--exec SF_Bulkops 'Update','SL_SUPERION_PROD ', 'task_Tritech_SFDC_Update_delta' 

/*
Salesforce object task does not contain column sup_task_legacy_id__C
Salesforce object task does not contain column sup_task_whatid
Salesforce object task does not contain column sup_task_whoid

*/

select error,count(*) from task_Tritech_SFDC_Update_delta
group by error;