  /*
-- Author       : Jagan	
-- Created Date : 20-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Opportunity----------
Requirement No  : REQ-0373
Total Records   :  
Scope: - Migrate Opportunities with the following Criteria.
         a. All Open Opportunities
         b. All Closed with a close date > 1/1/2016

       - All recordtypes to be migrated, recordtypes may be merged on migration.
       - Original contract date should be determined from closed/won opportunities and stored on the account record 
	     in a new field located on the account object.
 */

 /*
Use Tritech_PROD
EXEC SF_Refresh 'EN_Tritech_PROD','Opportunity','Yes'


Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User'
EXEC SF_Refresh 'SL_SUPERION_PROD','Account','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Contact'
EXEC SF_Refresh 'SL_SUPERION_PROD','Pricebook2','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Opportunity','Yes'
*/ 

Use Staging_PROD;

--Drop table Opportunity_Tritech_SFDC_Preload_Delta

DECLARE @ProductsAndServices NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.RecordType 
where SobjectType='Opportunity' and Name='Products and Services');

DECLARE @ChangeOrder NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.RecordType 
where SobjectType='Opportunity' and Name='Change Order');

DECLARE @Default NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'CentralSquare API');

DECLARE @LegacyPricebookId NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.Pricebook2
where Name like 'Legacy Pricebook');

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,Accident_Report_Interface__c=opp.Accident_Report_Interface__c
,Accident_Reports__c=opp.Accident_Reports__c

,AccountId_orig=opp.AccountId
,AccountId=IIF(accnt.Id IS NULL,'Account Not Found',accnt.Id)

,alternate_opp_value__c=opp.alternate_opp_value__c
,Total_Contract_Value_Forecast__c=opp.Actual_Opportunity_Value__c
,Approval_Manager_Email__c=opp.Approval_Manager_Email__c
,Legacy_Associate_Account_Manager__c=opp.Associate_Account_Manager__c

,Associated_Opportunity__c_orig=opp.Associated_Opportunity__c
,Associated_Opportunity__c=targetopp.Id

--,Bid_No_Bid_Decision_Date__c=Bid_No_Bid_Decision_Date__c
,Booked_Date__c=opp.Booked_Date__c
,Booked_Value__c=opp.Booked_Value__c
,Budget_Total__c=opp.Budget_Total__c
--,Customer_s_Defined_Initiatives__c=Business_Issues_WMP__c
,Citation_Interface__c=opp.Citation_Interface__c
,Citations__c=opp.Citations__c
,CloseDate=opp.CloseDate
,Commissionable_Value__c=opp.Comm_Value_Sync__c

,Comments_Reason_TriTech_Lost__c_orig=opp.Comments_Reason_TriTech_Lost__c
,Comments_Reason_TriTech_Won__c_orig=opp.Comments_Reason_TriTech_Won__c
,Reason_TriTech_Lost__c_orig=opp.Reason_TriTech_Lost__c
--,Win_Loss_Comments__c=CONCAT(CONCAT('Comments_Reason_TriTech_Lost:',Comments_Reason_TriTech_Lost__c),',',CONCAT('Comments_Reason_TriTech_Won:',Comments_Reason_TriTech_Won__c))
,CASE
WHEN Reason_TriTech_Lost__c='No Bid' THEN 'No Bid'
WHEN Reason_TriTech_Lost__c='Other' THEN opp.Comments_Reason_TriTech_Lost__c
when Comments_Reason_TriTech_Lost__c is not null and Comments_Reason_TriTech_Won__c is not null then concat('Comments - Reason TriTech Lost: '+cast(Comments_Reason_TriTech_Lost__c as nvarchar(max)),CHAR(13)+CHAR(10),

                     'Comments - Reason TriTech Won: '+cast(Comments_Reason_TriTech_Won__c as nvarchar(max)))

else Coalesce(Comments_Reason_TriTech_Lost__c , Comments_Reason_TriTech_Won__c )

END as Win_Loss_Comments__c
,Commissionable_GM__c=opp.Commissionable_GM__c
,Compelling_Reason__c=opp.Compelling_Event__c
,Competitors__c=opp.Competitors_WMP__c

,Contract_Capture_Manager__c_orig=opp.Contract_Capture_Manager__c
,Contract_Capture_Manager__c=cptrmngr.Id

,CPQ_Quote_Number__c=opp.CPQ_Quote_Number__c

,CreatedById_orig=opp.CreatedById
,CreatedById=IIF(createdbyuser.Id IS NULL,@Default,createdbyuser.Id)

,CreatedDate=opp.CreatedDate
,Current_Annual_Support__c=opp.Current_Annual_Support__c
,Legacy_TT_Customer_Number__c=Customer_Number__c
,Daily_Fee__c=opp.Daily_Fee__c
,Date_Grant_Funds_Expire__c=opp.Date_Grant_Funds_Expire__c
,Deconfliction_Type__c=opp.Deconfliction_Type__c

,Business_Issues_WMP__c_orig=opp.Business_Issues_WMP__c
,Description_orig=opp.Description
,CASE 
 WHEN opp.Business_Issues_WMP__c IS NOT NULL AND opp.Description IS NOT NULL THEN CONCAT(opp.Description,CHAR(13)+CHAR(10),'Business Issues:',opp.Business_Issues_WMP__c)
 WHEN opp.Business_Issues_WMP__c IS NOT NULL AND opp.Description IS NULL THEN CONCAT('Business Issues:',opp.Business_Issues_WMP__c)
 WHEN opp.Business_Issues_WMP__c IS NULL AND opp.Description IS NOT NULL THEN opp.Description
 ELSE NULL
 END as Description

,Discount__c=opp.Discount__c
,Discount_Amount__c=opp.Discount_Amount__c
,End_Date__c=opp.End_Date__c
,Funding_Comments__c=opp.Funding_Comments__c
,Grant_Amount__c=opp.Grant_Amount__c
,Gross_Margin__c=opp.Gross_Margin__c
,Legacy_Opportunity_ID__c=opp.Id
--,If_No_Bid_Primary_Reason__c=If_No_Bid_Primary_Reason__c
,Implementation_Fees__c=opp.Implementation_Fees__c

,Incumbent_to_be_Replaced__c_orig=Incumbent_to_be_Replaced__c 
,Incumbent_Software_Vendor__c=incmbnt.SandboxID

,INTERNAL_Note_to_Approvers__c=opp.INTERNAL_Note_to_Approvers__c
,Last_Approval_Date__c=opp.Last_Approval_Date__c
,Last_Rejection_Date__c=opp.Last_Rejection_Date__c
,Monthly_Fee__c=opp.Monthly_Fee__c
,Most_Recent_Approval_Level__c=opp.Most_Recent_Approval_Level__c
,Legacy_TT_Opportunity_Name__c=opp.Name
,Name=opp.Name
,Name_of_Grant_Program_s__c=opp.Name_of_Grant_Program_s__c
,Next_Steps_Last_Modified_Date__c=Next_Steps_Synopsis_Modified_Date__c
,NextStep=opp.NextStep

,OwnerId_orig=opp.OwnerId
,OwnerId=IIF(ownerusr.Id IS NULL,@Default,ownerusr.Id)

,Prepaid_Subscriptions__c=opp.Prepaid_Subscriptions__c
,Price_Book__c= opp.price_book__c --'Legacy Pricebook'
,Probability=opp.Probability
,Product_s_Required__c=Products_Required__c
,Project_Related_Fees__c=opp.Project_Related_Fees__c

--,Proposal_Writer__c_orig=Proposal_Writer__c
--,Proposal_Manager__c=prpslwrtr.Id

,Purchase_Order_Date__c=opp.Purchase_Order_Date__c
,Purchase_Order_Number__c=opp.Purchase_Order_Number__c
,Quote_Number__c=opp.Quote_Number__c
,Quote_Subtotal__c=opp.Quote_Subtotal__c
,Quote_Total__c=opp.Quote_Total__c

,TT_Legacy_Opportunity_Record_Type__c=lgcyrcrdtype.Name
,Legacy_RecordTypeId=lgcyrcrdtype.Id
,Legacy_RecordTypeName=lgcyrcrdtype.Name
,CASE 
 WHEN lgcyrcrdtype.Name IN('Zuercher Competitive','Zuercher Non-Competitive','TriTech Competitive','TriTech Non-Competitive')
 THEN @ProductsAndServices
 WHEN lgcyrcrdtype.Name IN('Zuercher Change Order','TriTech Change Order')
 THEN @ChangeOrder
 END as RecordTypeId

,Recurring_Fees__c=opp.Recurring_Fees__c
,Registered_Affiliates__c=opp.Registered_Affiliates__c
,Return_to_Approval_Step__c=opp.Return_to_Approval_Step__c
,Sales_Order_Number__c=opp.Sales_Order_Number__c
,Selection_Date__c=opp.Selection_Date__c
,Shortlisted_Date__c=opp.Shortlisted_Date__c
,Reason_TriTech_Lost__c_ori=opp.Reason_TriTech_Lost__c
,StageName_orig=opp.StageName
,CASE
 WHEN Reason_TriTech_Lost__c='Duplicate Opportunity' THEN 'Disqualified'
 WHEN Reason_TriTech_Lost__c='MQL' THEN 'Rejected'
 ELSE opp.StageName
 END as StageName
,Reason_TriTech_Lost__c_origi=opp.Reason_TriTech_Lost__c
,CASE
 WHEN Reason_TriTech_Lost__c='Duplicate Opportunity' THEN 'Duplicate Opportunity'
 WHEN Reason_TriTech_Lost__c='MQL' THEN 'Unable to Reach'
 END as Return_to_marketing_reason__c
,Start_Date__c=opp.Start_Date__c
,Subscription_Term_Years__c=opp.Subscription_Term_Years__c
,Tax__c=opp.Tax__c
,Third_Party_GM__c=opp.Third_Party_GM__c
,Total_Custom_Solution__c=opp.Total_Custom_Solution__c
,Total_Hardware__c=opp.Total_Hardware__c
,Total_Implementation_Service_Fees__c=opp.Total_Implementation_Service_Fees__c
,Total_Maintenance__c=opp.Total_Maintenance__c
,Total_Project_Related_Fees__c=opp.Total_Project_Related_Fees__c
,Total_Quoted_Amount__c=opp.Total_Quoted_Amount__c
,Total_Recurring_Fees__c=opp.Total_Recurring_Fees__c
,TT_Total_Software__c=opp.Total_Software__c
,Total_T_and_E_Amount__c=opp.Total_T_and_E_Amount__c
,Total_Third_Party__c=opp.Total_Third_Party__c

,VP_Notes__c=opp.VP_Notes__c

,Winning_Competitor__c_orig=Winning_Competitor__c
,Winning_SW_Vendor_Lookup__c=winvendr.SandboxID

,Yearly_Fee__c=opp.Yearly_Fee__c
,Annual_Hosting_Fee__c=Z_Annual_Hosting_Fee__c
,CAD_911__c=Z_CAD_911__c
,COGS__c=Z_COGS__c
,Data_Conversion__c=Z_Data_Conversion__c
,Date_Affiliate_Form_Signed__c=Z_Date_Affiliate_Form_Signed__c
,Discount_Notes__c=Z_Discount_Notes__c
,Expected_RFP_Release_Date__c=Z_Expected_RFP_Release_Date__c
,Forecasting_Category__c=opp.Deal_Forecast__c
--,GIS_Conversion__c=Z_GIS_Conversion__c
,Hardware__c=Z_Hardware__c
,Interface_Hours__c=Z_Interface_Hours__c
,Interfaces__c=Z_Interfaces__c
,Interfaces_Required__c=Z_Interfaces_Required__c
,IsClosed_orig=opp.IsClosed
,IsWon_orig=opp.IsWon
,Z_Maintenance_Discount__c=opp.Z_Maintenance_Discount__c
,Migration_Discount__c=Z_Migration_Discount__c
,NCIC__c=Z_NCIC__c
,NCIC_Notes__c=Z_NCIC_Notes__c
,NCIC_Quote_Requested__c=Z_NCIC_Quote_Requested__c
,PrePaid_Maintenance__c=Z_Pre_Paid_Maintenance__c
,Product_Enhancement_Hours__c=Z_Product_Enhancement_Hours__c
,Product_Enhancements_Required__c=Z_Product_Enhancements_Required__c
,Recurring_COGS__c=Z_Recurring_COGS__c
,RFP_Notes__c=Z_RFP_Notes__c
,RFP_Required__c=Z_RFP_Required__c
,Server_Cost__c=Z_Server_Cost__c
,Server_Type__c=Z_Server_Type__c
,Services__c=Z_Services__c
,Short_Term_Holding_Only__c=Z_Short_Term_Holding_Only__c

,Z_Signatory__c_orig=Z_Signatory__c
,Signatory__c=sgntry.Id

,Software__c=Z_Software__c
,Software_Discount__c=Z_Software_Discount__c
,Special_Maintenance_Terms__c=Z_Special_Maintenance_Terms__c
,Special_Payment_Terms__c=Z_Special_Payment_Terms__c
,Subscriptions_Year_1__c=Z_Subscription_Fees__c
,Sys_Ops_Notes__c=Z_Sys_Ops_Notes__c
,Training_Days__c=Z_Training_Days__c
,Maintenance_Year_1__c=Z_Year_1_Maintenance__c
,Spec_Provided__c=Z_ZT_Spec_Provided__c

--,New field=Actual_Opportunity_Value__c
--,New field=Booked_Actual_Value_Not_Equal__c
--,=Estimated_Cost_per_Sworn_Officer__c
--,New Field=Forecast_Value__c

,Primary_Quote_Contact__c_orig=opp.Primary_Quote_Contact__c
,Primary_Quote_Contact__c=prmryquotecntct.Id

,Credited_Sales_Team__c=opp.Credited_Sales_Team__c
,Deal_Type__c_orig=opp.Deal_Type__c
,Mapping_TT_Value=typemap.[TT Value]
,Order_Type__c=typemap.[CS Value] 

,Deconfliction_Next_Step_Date__c=opp.Deconfliction_Next_Step_Date__c
,Deconfliction_Next_Steps__c=opp.Deconfliction_Next_Steps__c
,Deconfliction_Status__c=opp.Deconfliction_Status__c

--,=Edited_Last_30_days__c
--,=Implementation_Definition_Review_Date__c

,LeadSource=opp.LeadSource
,Original_Superion_Close_Date__c=opp.Original_Superion_Close_Date__c
,Original_Superion_TCV__c=opp.Original_Superion_TCV__c

,Reason_TriTech_Lost__c_original=opp.Reason_TriTech_Lost__c
--,Primary_Win_Loss_Reason__c=Reason_TriTech_Lost__c
,Reason_TriTech_Won__c_orig=Reason_TriTech_Won__c
,Primary_Win_Loss_Reason__c=map.Superion_Primary_Win_Loss_Reason__c

--,=Red_Team_Date__c
,step__c=opp.step__c

,Original_Superion_Stage__c=Superion_Stage__c
,Is_Selected__c=TriTech_Selected__c
,Is_Shortlisted__c=TriTech_Shortlisted__c
,Type=opp.Type
,Charge_Types__c=opp.Charge_Types__c
,Pricebook2Id=@LegacyPricebookId
,Solution_Family__c='PSJ'
,Deal_Forecast__c_orig=opp.Deal_Forecast__c
--,CASE 
-- WHEN opp.Deal_Forecast__c='Commit' THEN 'Commit'
---- WHEN opp.Deal_Forecast__c='Best' THEN 'Likely'
-- WHEN opp.Deal_Forecast__c='Pipeline' THEN 'Best'
-- WHEN opp.Deal_Forecast__c='Omitted' THEN 'Omit'
-- WHEN opp.Deal_Forecast__c='Best Case' THEN 'Likely'
---- WHEN opp.Deal_Forecast__c='Best Deal' THEN 'Likely'
---- WHEN opp.Deal_Forecast__c='Closed' THEN 'Commit'
-- WHEN opp.Deal_Forecast__c='Closed'  and opp.isWon='True' THEN 'Commit'
-- WHEN opp.Deal_Forecast__c='Closed'  and opp.isWon='False' THEN 'Omit'
-- WHEN opp.Deal_Forecast__c IS NULL AND opp.StageName='Booked' THEN 'Commit'
-- WHEN opp.Deal_Forecast__c IS NULL AND opp.StageName='Lost' THEN 'Omit'
-- WHEN opp.Deal_Forecast__c IS NULL AND opp.StageName='Closed Lost - No Bid' THEN 'Omit'
-- WHEN opp.Deal_Forecast__c IS NULL AND opp.StageName='Closed Lost - Other' THEN 'Omit'
-- WHEN opp.Deal_Forecast__c IS NULL AND opp.StageName='Disqualified' THEN 'Omit'
-- WHEN opp.Deal_Forecast__c IS NULL AND opp.IsClosed='False' THEN 'Best'
-- WHEN opp.IsClosed='True' AND opp.IsWon='True' THEN 'Commit'
-- WHEN opp.IsClosed='True' AND opp.IsWon='False' THEN 'Omit'
-- END as Deal_Forecast__c

,CASE 
 WHEN opp.IsClosed='True' and opp.StageName='Booked' THEN 'Commit'
 WHEN opp.IsClosed='True' and opp.StageName='Lost' THEN 'Omit'
 WHEN opp.IsClosed='False' and opp.Deal_Forecast__c IS NULL THEN 'Pipeline'
 WHEN opp.IsClosed='False' and opp.Deal_Forecast__c IS NOT NULL THEN opp.Deal_Forecast__c
 END as Deal_Forecast__c

 --,SolutionCategory__c=IIF(opp.Deal_Type__c='911 System','911','')
 ,Deal_Type__c_ori=opp.Deal_Type__c
 ,Product_Family_WMP__c_orig=opp.Product_Family_WMP__c
 ,CASE
  WHEN opp.Deal_Type__c='911 System' or opp.Product_Family_WMP__c='IP911' or opp.Product_Family_WMP__c='TT911' THEN '911'
  ELSE 'Public Safety'
  END as SolutionCategory__c
 --,SolutionCategory__c=IIF(opp.Product_Family_WMP__c='IP911' or opp.Product_Family_WMP__c='TT911','911','Public Safety')
 ,SolutionType__c=IIF((rcrdtypname.Name like '%Zuercher%') AND (opp.Product_Family_WMP__c='IP911' or opp.Product_Family_WMP__c='TT911'),'Zuercher',maptype.[CS Value])
--,=Weighted_Amount__c

into Opportunity_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.Opportunity opp

--Fetching Winning_SW_Vendor_Lookup__c (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping winvendr on 
winvendr.TriTech_Competitor=opp.Winning_Competitor__c and isnull(winvendr.Objectname,'Opportunity')='Opportunity'

--Fetching Z_Signatory__c (Contact Lookup)
left join SUPERION_PRODUCTION.dbo.Contact sgntry on
sgntry.Legacy_id__c=opp.Z_Signatory__c

--Fetching Incumbent_to_be_Replaced__c (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping incmbnt on
incmbnt.TriTech_Competitor=cast(opp.Incumbent_to_be_Replaced__c as nvarchar(max)) and isnull(incmbnt.Objectname,'Opportunity')='Opportunity'

--Fetching Primary_Quote_Contact__c (Contact Lookup)
left join SUPERION_PRODUCTION.dbo.Contact prmryquotecntct on
prmryquotecntct.Legacy_id__c=opp.Primary_Quote_Contact__c

--Fetching CreatedById (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=opp.CreatedById

--Fetching OwnerId (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] ownerusr 
on ownerusr.Legacy_Tritech_Id__c=opp.OwnerId

--Fetching Proposal_Writer__c (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] prpslwrtr 
on prpslwrtr.Legacy_Tritech_Id__c=opp.Proposal_Writer__c

--Fetching AccountId (Account Lookup)
left join SUPERION_PRODUCTION.dbo.Account accnt on
accnt.LegacySFDCAccountId__c=opp.AccountId

--Fetching Contract_Capture_Manager__c (User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] cptrmngr on
cptrmngr.Legacy_Tritech_Id__c=opp.Contract_Capture_Manager__c

--Fetching LegacyRecordTypeName
left join Tritech_PROD.dbo.RecordType lgcyrcrdtype on 
lgcyrcrdtype.Id=opp.RecordTypeId

left join Tritech_PROD.dbo.RecordType rcrdtypname
on rcrdtypname.Id=opp.RecordTypeId

left join Staging_PROD.dbo.Tritech_Opportunity_Primary_Win_loss_reason_Mapping  map
on map.TT_Reason_TriTech_Lost__c=opp.Reason_TriTech_Lost__c

left join Staging_PROD.dbo.Tritech_OpportunityTypeMapping typemap
 on typemap.[TT Value]=opp.Deal_Type__c and typemap.[Field API]='Deal_Type__c'

left join Staging_PROD.dbo.Tritech_OpportunityTypeMapping maptype
on maptype.[TT Value]=opp.Product_Family_WMP__c and maptype.[Field API]='Product_Family_WMP__c'

left join Superion_Production.dbo.Opportunity targetopp on
targetopp.Legacy_Opportunity_ID__c=opp.Associated_Opportunity__c

where opp.ID in (Select TritechOpportunityID from Staging_PROD.dbo.TT_SFDC_Missing_Opportunities)
--and opp.Associated_Opportunity__c IS NOT NULL AND targetopp.Id is NULL;
--1136
----------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.Opportunity;--38376

Select count(*) from Opportunity_Tritech_SFDC_Preload_Delta;--1136

select *   from Staging_PROD.dbo.TT_SFDC_Missing_Opportunities;--1136

Select Legacy_Opportunity_ID__c,count(*) from Staging_PROD.dbo.Opportunity_Tritech_SFDC_Preload_Delta
group by Legacy_Opportunity_ID__c
having count(*)>1; --

--Drop table Staging_PROD.dbo.Opportunity_Tritech_SFDC_Load_Delta;

Select * into 
Opportunity_Tritech_SFDC_Load_Delta
from Opportunity_Tritech_SFDC_Preload_Delta; --1136

select stagename,* from Opportunity_Tritech_SFDC_Load_Delta
where StageName_orig='Booked' and stagename<>'Booked' ;

select accountid_orig,* from Opportunity_Tritech_SFDC_Load_Delta
where accountid ='Account Not Found' ;

select Primary_Quote_Contact__c_orig,* from Opportunity_Tritech_SFDC_Load_Delta
where Primary_Quote_Contact__c is null and Primary_Quote_Contact__c_orig is not null ;



select Deal_Forecast__c_orig,Deal_Forecast__c,IsClosed_orig,iswon_orig,StageName_orig,StageName,* from Opportunity_Tritech_SFDC_Load_Delta
where Deal_Forecast__c<>isnull(Deal_Forecast__c_orig,'X') and IsClosed_orig='false'

select * from Opportunity_Tritech_SFDC_Load_Delta;
where RecordTypeId is null;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'Opportunity_Tritech_SFDC_Load_Delta' 

/*
Salesforce object Opportunity does not contain column AccountId_orig
Salesforce object Opportunity does not contain column Associated_Opportunity__c_orig
Salesforce object Opportunity does not contain column Comments_Reason_TriTech_Lost__c_orig
Salesforce object Opportunity does not contain column Comments_Reason_TriTech_Won__c_orig
Salesforce object Opportunity does not contain column Reason_TriTech_Lost__c_orig
Salesforce object Opportunity does not contain column Contract_Capture_Manager__c_orig
Salesforce object Opportunity does not contain column CreatedById_orig
Salesforce object Opportunity does not contain column Business_Issues_WMP__c_orig
Salesforce object Opportunity does not contain column Description_orig
Salesforce object Opportunity does not contain column Incumbent_to_be_Replaced__c_orig
Salesforce object Opportunity does not contain column OwnerId_orig
Salesforce object Opportunity does not contain column Legacy_RecordTypeId
Salesforce object Opportunity does not contain column Legacy_RecordTypeName
Salesforce object Opportunity does not contain column Reason_TriTech_Lost__c_ori
Salesforce object Opportunity does not contain column StageName_orig
Salesforce object Opportunity does not contain column Reason_TriTech_Lost__c_origi
Salesforce object Opportunity does not contain column Winning_Competitor__c_orig
Salesforce object Opportunity does not contain column IsClosed_orig
Salesforce object Opportunity does not contain column IsWon_orig
Salesforce object Opportunity does not contain column Z_Signatory__c_orig
Salesforce object Opportunity does not contain column Primary_Quote_Contact__c_orig
Salesforce object Opportunity does not contain column Deal_Type__c_orig
Salesforce object Opportunity does not contain column Mapping_TT_Value
Salesforce object Opportunity does not contain column Reason_TriTech_Lost__c_original
Salesforce object Opportunity does not contain column Reason_TriTech_Won__c_orig
Salesforce object Opportunity does not contain column Deal_Forecast__c_orig
Salesforce object Opportunity does not contain column Deal_Type__c_ori
Salesforce object Opportunity does not contain column Product_Family_WMP__c_orig
Column Legacy_TT_Customer_Number__c is not insertable into the salesforce object Opportunity
*/



--Exec SF_BulkOps 'Insert:batchsize(50)','SL_SUPERION_PROD','Opportunity_Tritech_SFDC_Load_Delta'

----------------------------------------------------------------------------------------


Select error,count(*) from Opportunity_Tritech_SFDC_Load_Delta
group by error;


--drop table Opportunity_Tritech_SFDC_Load_Delta_errors
select * 
into Opportunity_Tritech_SFDC_Load_Delta_errors
from Opportunity_Tritech_SFDC_Load_Delta
where error<>'Operation Successful.';--111

--drop table Opportunity_Tritech_SFDC_Load_Delta_errors_bkp
select * 
into Opportunity_Tritech_SFDC_Load_Delta_errors_bkp
from Opportunity_Tritech_SFDC_Load_Delta
where error<>'Operation Successful.';--111

--update a set Forecasting_Category__c=null
--Select Forecasting_Category__c,*
 from Opportunity_Tritech_SFDC_Load_Delta_errors a
where Forecasting_Category__c in ( select Forecasting_Category__c from Opportunity_Tritech_SFDC_Load_Delta_errors b
where b.Error like '%bad value for restricted picklist field: %'+ b.Forecasting_Category__c)

Product_s_Required__c
Training

--Select Product_s_Required__c,* 
--update a set Product_s_Required__c=null
from Opportunity_Tritech_SFDC_Load_Delta_errors a
where Product_s_Required__c like '%Training%' or Product_s_Required__c like '%Travel%';--60


--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','Opportunity_Tritech_SFDC_Load_Delta_errors'

--delete 
from Opportunity_Tritech_SFDC_Load_Delta
where error<>'Operation Successful.'; --

--insert into Opportunity_Tritech_SFDC_Load_Delta
select * from Opportunity_Tritech_SFDC_Load_Delta_errors;--111
-----------------------------------------------------------------------
--Updating Ids for fields having lookup relationship on same object.(Associated_Opportunity__c)

Use Staging_PROD;

--Drop table Opportunity_Tritech_SFDC_Update_Delta;

Select 

 Id=b.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Associated_Opportunity__c_orig=b.Associated_Opportunity__c_orig
,Associated_Opportunity__c=a.id
,Legacy_Opportunity_ID__c_orig=b.Legacy_Opportunity_ID__c

into Opportunity_Tritech_SFDC_Update_Delta

from Staging_PROD.dbo.Opportunity_Tritech_SFDC_Load_Delta b
left join
Staging_PROD.dbo.Opportunity_Tritech_SFDC_Load_Delta a
on a.Legacy_Opportunity_ID__c=b.Associated_Opportunity__c_orig
where b.Error='Operation Successful.' and
b.Associated_Opportunity__c IS NULL and b.Associated_Opportunity__c_orig IS NOT NULL;--97

Select * from Opportunity_Tritech_SFDC_Update_Delta;

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Opportunity_Tritech_SFDC_Update_Delta' 

/*
Salesforce object Opportunity does not contain column Associated_Opportunity__c_orig
Salesforce object Opportunity does not contain column Legacy_Opportunity_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','Opportunity_Tritech_SFDC_Update_Delta'

