 /*
-- Author       : Jagan	
-- Created Date : 05-Feb-19
-- Modified Date: 
-- Modified By  :   
-- Description  : Migrate from OpportunityContactRole to Key_Players_Influencers__c.

-----------Tritech Key_Players_Influencers__c----------
Requirement No  : REQ-0912
Total Records   :  
Scope: Migrate all records that relate to a migrated opportunity record.
 */

 /*
Use Tritech_PROD
EXEC SF_Refresh 'EN_Tritech_PROD','OpportunityContactRole','yes'


Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','Contact','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Opportunity','Yes'
*/ 

Use Staging_PROD;

--Drop table Key_Players_Influencers__c_Tritech_SFDC_Preload_delta

DECLARE @Default NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'CentralSquare API');

Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,legacy_id__c=ocr.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
,Name_orig=ocr.ContactId
,Name__c=IIF(con.Id IS NULL,'Contact Not Found',con.Id)
,Opportunity_orig=ocr.OpportunityId
,Opportunity__c=opp.Id
,Role_orig=ocr.Role
,CASE
 WHEN ocr.Role='Coach' THEN 'Coach'
 WHEN ocr.Role='Business User' THEN 'User'
 WHEN ocr.Role='Consultant' THEN 'Consultant'
 WHEN ocr.Role='Decision Maker' THEN 'Decision Maker'
 WHEN ocr.Role='Influencer' THEN 'Decision Influencer'
 WHEN ocr.Role='Technical Buyer' THEn 'Technical Buyer'
 WHEN ocr.Role IS NULL THEN NULL
 ELSE 'Other'
 END as Formal_Role__c
,CreatedDate=ocr.CreatedDate
,CreatedById_orig=ocr.CreatedById
,CreatedById=IIF(usr.Id IS NULL,@Default,usr.Id)
,InfluenceLevel__c ='Influencer Base'

 into Key_Players_Influencers__c_Tritech_SFDC_Preload_delta
 --select *
from Tritech_PROD.dbo.OpportunityContactRole ocr
inner join SUPERION_PRODUCTION.dbo.Opportunity opp
on opp.Legacy_Opportunity_ID__c=ocr.OpportunityId
inner join Staging_PROD.dbo.TT_SFDC_Missing_Opportunities opp_delta
on opp.Legacy_Opportunity_ID__c=opp_delta.TritechOpportunityID
left join SUPERION_PRODUCTION.dbo.Contact con
on con.Legacy_ID__c=ocr.ContactId
left join SUPERION_PRODUCTION.dbo.[User] usr
on ocr.CreatedById=usr.Legacy_Tritech_Id__c;--(8 row(s) affected)

--------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.OpportunityContactRole;--5108

select * from TT_SFDC_Missing_Opportunities;--8

Select count(*) from Key_Players_Influencers__c_Tritech_SFDC_Preload_delta;--8

Select legacy_id__c,count(*) from Staging_PROD.dbo.Key_Players_Influencers__c_Tritech_SFDC_Preload_delta
group by legacy_id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.Key_Players_Influencers__c_Tritech_SFDC_load_delta;

Select * into 
Key_Players_Influencers__c_Tritech_SFDC_load_delta
from Key_Players_Influencers__c_Tritech_SFDC_Preload_delta; --(8 row(s) affected)

Select * from Staging_PROD.dbo.Key_Players_Influencers__c_Tritech_SFDC_load_delta;--8


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'Key_Players_Influencers__c_Tritech_SFDC_load_delta' 

/*
Salesforce object Key_Players_Influencers__c does not contain column Name_orig
Salesforce object Key_Players_Influencers__c does not contain column Opportunity_orig
Salesforce object Key_Players_Influencers__c does not contain column Role_orig
Salesforce object Key_Players_Influencers__c does not contain column CreatedById_orig
*/

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_PROD','Key_Players_Influencers__c_Tritech_SFDC_load_delta'

