
/*
-- Author     : Shivani Mogullapalli
-- Date       : 24/11/2018
-- Description: Migrate the KnowledgeArticle Datacategories data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-

Scope: Load the Datacategories based on the given DataCategories Mapping.

-- drop table DataCategories_KB_Tritech_SFDC_Preload
*/
use Staging_PROD 
go
select 
							DatacategoryName		= api.Article_Category_API
							,DataCategoryGroupName	= 'Products'
							,parentid				= a.id
							,DataCategoryLabel		= c.newArticleCategory
					into DataCategories_KB_Tritech_SFDC_Preload
from KnowledgeArticle_Tritech_SFDC_load a
,Tritech_KB_Article_Data_Categories_final c
,DataCategory_API_New_Category api
where a.ArticleNumber_orig=c.ArticleNumber 
and c.NewArticleCategory = api.Article_Category_Label
and a.id like 'ka%'

select count(*) from Tritech_KB_Article_Data_Categories
--(6452 row(s) affected)



select count(*) from KnowledgeArticle_Tritech_SFDC_load
-- 6452

select * from DataCategories_KB_Tritech_SFDC_Preload
-- drop table DataCategories_KB_Tritech_SFDC_load
select * 
into DataCategories_KB_Tritech_SFDC_load 
from DataCategories_KB_Tritech_SFDC_Preload
-- (6452 row(s) affected)
select * 
--update a set a.datacategoryname='zuercher_Suite',a.datacategorylabel='Zuercher Suite'
 from DataCategories_KB_Tritech_SFDC_load a where datacategoryname='zsuite'

select * from DataCategories_KB_Tritech_SFDC_load
-- (6452 row(s) affected)

-- drop table DataCategories_KB_Tritech_SFDC_load_errors
select * from DataCategories_KB_Tritech_SFDC_load_errors
--(124 row(s) affected)
-- drop table DataCategories_KB_Tritech_SFDC_load_errors_bkp
select * into DataCategories_KB_Tritech_SFDC_load_errors_bkp 
from DataCategories_KB_Tritech_SFDC_load_errors
--(124 row(s) affected)

select * 
--update a set a.datacategoryname='zuercher_Suite',a.datacategorylabel='Zuercher Suite'
 from DataCategories_KB_Tritech_SFDC_load_errors a where datacategoryname='zsuite'
 --(121 row(s) affected)

 select * 
--update a set a.article_category_API='zuercher_Suite',a.article_category_label='Zuercher Suite'
 from DataCategory_API_New_Category a where article_category_API='zsuite'


 --zsuite
select *
--update a set a.newarticlecategory ='Zuercher Suite'
 from Tritech_KB_Article_Data_Categories_final a where a.newarticlecategory='zsuite'
 --(127 row(s) affected)

 select * from DataCategory_API_New_Category
Select distinct
   A.parentid,
        stuff((
        select ';' + B.DataCategoryLabel
        from DataCategories_KB_Tritech_SFDC_load B
        WHERE A.parentid=B.parentid
        for xml path('')),1,1,'')
   From DataCategories_KB_Tritech_SFDC_load A
   where parentid!=''


----------------------------------------------------------------------------------------------------------------------------
select * from DataCategories_KB_Tritech_SFDC_Preload

/*
Select distinct
   A.parentid,
        stuff((
        select ';' + c.article_category_api
        from DataCategories_KB_Tritech_SFDC_load B
		left join DataCategory_API_New_Category c
		on b.datacategoryLabel=c.article_category_label
        WHERE A.parentid=B.parentid
        for xml path('')),1,1,'') as Product__c
   From DataCategories_KB_Tritech_SFDC_load A
   where parentid!=''
   */

   -- execution time : 1 hour

  -- : publish time :1 hour


  select distinct newarticlecategory from Tritech_KB_Article_Data_Categories_missing
except
select distinct article_category_label from DataCategory_API_New_Category


select * from DataCategory_API_New_Category


----insert into DataCategory_API_New_Category(article_category_label, article_Category_API) values ('PSJ','PSJ') 
--insert into DataCategory_API_New_Category(article_category_label, article_Category_API) values ('DROP','DROP') 

use Staging_PROD 
go
select 
							DatacategoryName		= api.Article_Category_API
							,DataCategoryGroupName	= 'Products'
							,parentid				= a.id
							,DataCategoryLabel		= c.newArticleCategory
					into DataCategories_KB_Tritech_SFDC_Preload_MISSING
from KnowledgeArticle_Tritech_SFDC_load a
,Tritech_KB_Article_Data_Categories_missing c
,DataCategory_API_New_Category api
where a.ArticleNumber_orig=c.[TT Article Number] 
and c.NewArticleCategory = api.Article_Category_Label
and a.id like 'ka%'

select * 
--update a set a.datacategoryname='zuercher_Suite',a.datacategorylabel='Zuercher Suite'
 from DataCategories_KB_Tritech_SFDC_Preload_MISSING a where datacategoryname='zsuite'

--(6 row(s) affected)

-- both the tables merged into Tritech_KB_Article_Data_Categories_final


