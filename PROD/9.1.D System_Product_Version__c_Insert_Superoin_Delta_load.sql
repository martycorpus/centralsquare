-------------------------------------------------------------------------------
--- System Product Version load for new Registered Products. 
--  Script File Name: 9.1.D System_Product_Version__c_Insert_Superoin_Delta_load.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--- Copyright Apps Associates 2018
--- Created Date: April 10, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-04-10 Created.
--- This script will create SPVs for any newly created Registered Products from an Asset Import.
--- This script requires the DBAmp ETL tool for Salesforce.
-------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- Section 1 � Replicate Data.
---------------------------------------------------------------------------------

Use Superion_Production;

Exec SF_Refresh 'Superion_PROD','CUSTOMER_ASSET__C','Yes'

Exec SF_Refresh 'Superion_PROD','Product2','Yes'

Exec SF_Refresh 'Superion_PROD','Account','Yes'

Exec SF_Refresh 'Superion_PROD','Registered_Product__c','Yes'  

Exec SF_Refresh 'Superion_PROD','Environment__c','Yes'

Exec SF_Refresh 'Superion_PROD','Version__c','Yes'


---------------------------------------------------------------------------------
-- Section 2 � Create Load Table.
---------------------------------------------------------------------------------


-- Drop if exists.

Use Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_delta_preload')   
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_delta_preload; 

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_delta_load')  
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_delta_load; 


---------------------------------------------------------------------------------
-- Create System_Product_Version__c Staging Load Table
---------------------------------------------------------------------------------

--Create preload table (this is a working table that will be used to create the final load table.)
--Cte
WITH cte_sup_environment
     AS ( SELECT id,
                 NAME,
                 type__c,
                 account__c,
                 [description__c]
          FROM   superion_production.dbo.ENVIRONMENT__C ),
     cte_zuercher_customer_assets
     AS ( SELECT t1.id               AS Zuercher_Registered_Product_Id,
                 t1.NAME             AS Zuercher_Registered_Product_Name,
                 t2.id               AS CustomerAsset__c,
                 t2.NAME             AS CustomerAsset_Name,
                 t2.product_group__c AS CUSTOMER_ASSET_Product_Group,
                 p2.product_group__c,
                 p2.product_line__c,
                 t1.account__c,
                 a.NAME              AS accountname
          FROM   superion_production.dbo.REGISTERED_PRODUCT__C t1
                 LEFT JOIN superion_production.dbo.PRODUCT2 p2
                        ON p2.id = t1.product__c
                 JOIN superion_production.dbo.CUSTOMER_ASSET__C t2
                   ON t2.id = t1.customerasset__c
                 LEFT JOIN superion_production.dbo.ACCOUNT a
                        ON a.id = t1.account__c
          WHERE  t2.product_group__c = 'Zuercher' ),
     cte_version
     AS ( SELECT t1.id,
                 t1.replaceexistingwiththisversion__c,
                 t1.NAME,
                 t2.NAME AS ReplaceexistingwiththisVersion_Name,
                 t1.productline__c,
                 t1.legacyttzversion__c,
                 t1.versionnumber__c
          FROM   superion_production.dbo.VERSION__C t1
                 LEFT JOIN superion_production.dbo.VERSION__C t2
                        ON t2.id = t1.replaceexistingwiththisversion__c
          WHERE  t1.productline__c IS NOT NULL ),
     cte_version_majorrelease
     AS ( SELECT t1.id,
                 t1.replaceexistingwiththisversion__c,
                 t1.NAME,
                 t2.NAME AS ReplaceexistingwiththisVersion_Name,
                 t1.productline__c,
                 t1.legacymajorreleaseversion__c,
                 t1.legacyttzversion__c,
                 t1.versionnumber__c
          FROM   superion_production.dbo.VERSION__C t1
                 LEFT JOIN superion_production.dbo.VERSION__C t2
                        ON t2.id = t1.replaceexistingwiththisversion__c
          WHERE  t1.productline__c IS NOT NULL AND
                 t1.legacymajorreleaseversion__c IS NOT NULL AND
                 t1.legacyttzsource__c = 'Version reconciliation' AND
                 t1.legacymajorreleaseversion__c = '1.1' ),
     cte_version_2
     AS ( SELECT Row_number( )
                   OVER (
                     partition BY t1.versionnumber__c
                     ORDER BY (CASE WHEN t1.legacyttzsource__c = 'CS Import File' THEN 1 WHEN t1.legacyttzsource__c = 'Hardware Software' THEN 2 ELSE 3 END) ) AS "RANK",
                 t1.id,
                 t1.replaceexistingwiththisversion__c,
                 t2.NAME                                                                                                                                       AS ReplaceexistingwiththisVersion_Name,
                 t1.NAME,
                 t1.productline__c,
                 t1.legacyttzversion__c,
                 t1.legacyttzproductgroup__c,
                 t1.product_group__c,
                 t1.versionnumber__c,
                 t1.legacyttzsource__c,
                 t1.current__c
          FROM   superion_production.dbo.VERSION__C t1
                 LEFT JOIN superion_production.dbo.VERSION__C t2
                        ON t2.id = t1.replaceexistingwiththisversion__c
          WHERE  t1.legacyttzproductgroup__c = 'IMC'  OR
                 t1.product_group__c = 'IMC' ),
     cte_tt_hs -- TT Hardware Software CTE
     AS ( SELECT t1.id                      AS tt_legacy_id_hs,
                 t1.NAME                    AS tt_hs_name,
                 t1.account_name__c         AS tt_legacysfdcaccountid__c,
                 t3.id                      AS account__c,
                 t3.NAME                    AS account_name,
                 t1.cim_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'CIM_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.cim_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                            AS tt_legacy_id_hs,
                 t1.NAME                          AS tt_hs_name,
                 t1.account_name__c               AS tt_legacysfdcaccountid__c,
                 t3.id                            AS account__c,
                 t3.NAME                          AS account_name,
                 t1.cim_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'CIM_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.cim_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id                           AS tt_legacy_id_hs,
                 t1.NAME                         AS tt_hs_name,
                 t1.account_name__c              AS tt_legacysfdcaccountid__c,
                 t3.id                           AS account__c,
                 t3.NAME                         AS account_name,
                 t1.cim_test_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'CIM_Test_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.cim_test_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                                 AS tt_legacy_id_hs,
                 t1.NAME                               AS tt_hs_name,
                 t1.account_name__c                    AS tt_legacysfdcaccountid__c,
                 t3.id                                 AS account__c,
                 t3.NAME                               AS account_name,
                 t1.cim_test_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'CIM_Test_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.cim_test_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id                               AS tt_legacy_id_hs,
                 t1.NAME                             AS tt_hs_name,
                 t1.account_name__c                  AS tt_legacysfdcaccountid__c,
                 t3.id                               AS account__c,
                 t3.NAME                             AS account_name,
                 t1.cim_training_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'CIM_Training_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.cim_training_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                                     AS tt_legacy_id_hs,
                 t1.NAME                                   AS tt_hs_name,
                 t1.account_name__c                        AS tt_legacysfdcaccountid__c,
                 t3.id                                     AS account__c,
                 t3.NAME                                   AS account_name,
                 t1.cim_training_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'CIM_Training_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.cim_training_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id                            AS tt_legacy_id_hs,
                 t1.NAME                          AS tt_hs_name,
                 t1.account_name__c               AS tt_legacysfdcaccountid__c,
                 t3.id                            AS account__c,
                 t3.NAME                          AS account_name,
                 t1.fbr_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'FBR_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.fbr_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id                           AS tt_legacy_id_hs,
                 t1.NAME                         AS tt_hs_name,
                 t1.account_name__c              AS tt_legacysfdcaccountid__c,
                 t3.id                           AS account__c,
                 t3.NAME                         AS account_name,
                 t1.fbr_test_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'FBR_Test_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.fbr_test_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                                 AS tt_legacy_id_hs,
                 t1.NAME                               AS tt_hs_name,
                 t1.account_name__c                    AS tt_legacysfdcaccountid__c,
                 t3.id                                 AS account__c,
                 t3.NAME                               AS account_name,
                 t1.fbr_test_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'FBR_Test_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.fbr_test_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id                               AS tt_legacy_id_hs,
                 t1.NAME                             AS tt_hs_name,
                 t1.account_name__c                  AS tt_legacysfdcaccountid__c,
                 t3.id                               AS account__c,
                 t3.NAME                             AS account_name,
                 t1.fbr_training_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'FBR_Training_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.fbr_training_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                                     AS tt_legacy_id_hs,
                 t1.NAME                                   AS tt_hs_name,
                 t1.account_name__c                        AS tt_legacysfdcaccountid__c,
                 t3.id                                     AS account__c,
                 t3.NAME                                   AS account_name,
                 t1.fbr_training_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'FBR_Training_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.fbr_training_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id              AS tt_legacy_id_hs,
                 t1.NAME            AS tt_hs_name,
                 t1.account_name__c AS tt_legacysfdcaccountid__c,
                 t3.id              AS account__c,
                 t3.NAME            AS account_name,
                 t1.imc_version__c  AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'IMC_Version__c '
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.imc_version__c IS NOT NULL
          UNION
          SELECT t1.id                   AS tt_legacy_id_hs,
                 t1.NAME                 AS tt_hs_name,
                 t1.account_name__c      AS tt_legacysfdcaccountid__c,
                 t3.id                   AS account__c,
                 t3.NAME                 AS account_name,
                 t1.inform_me_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Inform_Me_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.inform_me_version__c IS NOT NULL
          UNION
          SELECT t1.id              AS tt_legacy_id_hs,
                 t1.NAME            AS tt_hs_name,
                 t1.account_name__c AS tt_legacysfdcaccountid__c,
                 t3.id              AS account__c,
                 t3.NAME            AS account_name,
                 t1.iq_version__c   AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'IQ_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.iq_version__c IS NOT NULL
          UNION
          SELECT t1.id                       AS tt_legacy_id_hs,
                 t1.NAME                     AS tt_hs_name,
                 t1.account_name__c          AS tt_legacysfdcaccountid__c,
                 t3.id                       AS account__c,
                 t3.NAME                     AS account_name,
                 t1.jail_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Jail_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.jail_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                            AS tt_legacy_id_hs,
                 t1.NAME                          AS tt_hs_name,
                 t1.account_name__c               AS tt_legacysfdcaccountid__c,
                 t3.id                            AS account__c,
                 t3.NAME                          AS account_name,
                 t1.rms_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'RMS_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.rms_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id                           AS tt_legacy_id_hs,
                 t1.NAME                         AS tt_hs_name,
                 t1.account_name__c              AS tt_legacysfdcaccountid__c,
                 t3.id                           AS account__c,
                 t3.NAME                         AS account_name,
                 t1.rms_test_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'RMS_Test_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.rms_test_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                                 AS tt_legacy_id_hs,
                 t1.NAME                               AS tt_hs_name,
                 t1.account_name__c                    AS tt_legacysfdcaccountid__c,
                 t3.id                                 AS account__c,
                 t3.NAME                               AS account_name,
                 t1.rms_test_software_version_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'RMS_Test_Software_Version_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.rms_test_software_version_patch__c IS NOT NULL
          UNION
          SELECT t1.id                       AS tt_legacy_id_hs,
                 t1.NAME                     AS tt_hs_name,
                 t1.account_name__c          AS tt_legacysfdcaccountid__c,
                 t3.id                       AS account__c,
                 t3.NAME                     AS account_name,
                 t1.ttms_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'TTMS_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.ttms_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                         AS tt_legacy_id_hs,
                 t1.NAME                       AS tt_hs_name,
                 t1.account_name__c            AS tt_legacysfdcaccountid__c,
                 t3.id                         AS account__c,
                 t3.NAME                       AS account_name,
                 t1.va_cad_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VA_CAD_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.va_cad_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                         AS tt_legacy_id_hs,
                 t1.NAME                       AS tt_hs_name,
                 t1.account_name__c            AS tt_legacysfdcaccountid__c,
                 t3.id                         AS account__c,
                 t3.NAME                       AS account_name,
                 t1.va_fbr_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VA_FBR_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.va_fbr_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                          AS tt_legacy_id_hs,
                 t1.NAME                        AS tt_hs_name,
                 t1.account_name__c             AS tt_legacysfdcaccountid__c,
                 t3.id                          AS account__c,
                 t3.NAME                        AS account_name,
                 t1.va_fire_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VA_Fire_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.va_fire_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                            AS tt_legacy_id_hs,
                 t1.NAME                          AS tt_hs_name,
                 t1.account_name__c               AS tt_legacysfdcaccountid__c,
                 t3.id                            AS account__c,
                 t3.NAME                          AS account_name,
                 t1.va_inform_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VA_Inform_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.va_inform_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                            AS tt_legacy_id_hs,
                 t1.NAME                          AS tt_hs_name,
                 t1.account_name__c               AS tt_legacysfdcaccountid__c,
                 t3.id                            AS account__c,
                 t3.NAME                          AS account_name,
                 t1.va_mobile_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VA_Mobile_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.va_mobile_software_version__c IS NOT NULL
          UNION
          SELECT t1.id                         AS tt_legacy_id_hs,
                 t1.NAME                       AS tt_hs_name,
                 t1.account_name__c            AS tt_legacysfdcaccountid__c,
                 t3.id                         AS account__c,
                 t3.NAME                       AS account_name,
                 t1.va_rms_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VA_RMS_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.va_rms_software_version__c IS NOT NULL
          UNION
          SELECT t1.id               AS tt_legacy_id_hs,
                 t1.NAME             AS tt_hs_name,
                 t1.account_name__c  AS tt_legacysfdcaccountid__c,
                 t3.id               AS account__c,
                 t3.NAME             AS account_name,
                 t1.visicad_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiCAD_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visicad_patch__c IS NOT NULL
          UNION
          SELECT t1.id                 AS tt_legacy_id_hs,
                 t1.NAME               AS tt_hs_name,
                 t1.account_name__c    AS tt_legacysfdcaccountid__c,
                 t3.id                 AS account__c,
                 t3.NAME               AS account_name,
                 t1.visicad_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiCAD_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visicad_version__c IS NOT NULL
          UNION
          SELECT t1.id                                 AS tt_legacy_id_hs,
                 t1.NAME                               AS tt_hs_name,
                 t1.account_name__c                    AS tt_legacysfdcaccountid__c,
                 t3.id                                 AS account__c,
                 t3.NAME                               AS account_name,
                 t1.visinet_mobile_production_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Mobile_Production_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_mobile_production_patch__c IS NOT NULL
          UNION
          SELECT t1.id                           AS tt_legacy_id_hs,
                 t1.NAME                         AS tt_hs_name,
                 t1.account_name__c              AS tt_legacysfdcaccountid__c,
                 t3.id                           AS account__c,
                 t3.NAME                         AS account_name,
                 t1.visinet_mobile_test_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Mobile_Test_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_mobile_test_patch__c IS NOT NULL
          UNION
          SELECT t1.id                             AS tt_legacy_id_hs,
                 t1.NAME                           AS tt_hs_name,
                 t1.account_name__c                AS tt_legacysfdcaccountid__c,
                 t3.id                             AS account__c,
                 t3.NAME                           AS account_name,
                 t1.visinet_mobile_test_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Mobile_Test_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_mobile_test_version__c IS NOT NULL
          UNION
          SELECT t1.id                                 AS tt_legacy_id_hs,
                 t1.NAME                               AS tt_hs_name,
                 t1.account_name__c                    AS tt_legacysfdcaccountid__c,
                 t3.id                                 AS account__c,
                 t3.NAME                               AS account_name,
                 t1.visinet_mobile_training_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Mobile_Training_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_mobile_training_version__c IS NOT NULL
          UNION
          SELECT t1.id                        AS tt_legacy_id_hs,
                 t1.NAME                      AS tt_hs_name,
                 t1.account_name__c           AS tt_legacysfdcaccountid__c,
                 t3.id                        AS account__c,
                 t3.NAME                      AS account_name,
                 t1.visinet_mobile_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Mobile_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_mobile_version__c IS NOT NULL
          UNION
          SELECT t1.id                           AS tt_legacy_id_hs,
                 t1.NAME                         AS tt_hs_name,
                 t1.account_name__c              AS tt_legacysfdcaccountid__c,
                 t3.id                           AS account__c,
                 t3.NAME                         AS account_name,
                 t1.visinet_test_system_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Test_System_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_test_system_patch__c IS NOT NULL
          UNION
          SELECT t1.id                             AS tt_legacy_id_hs,
                 t1.NAME                           AS tt_hs_name,
                 t1.account_name__c                AS tt_legacysfdcaccountid__c,
                 t3.id                             AS account__c,
                 t3.NAME                           AS account_name,
                 t1.visinet_test_system_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Test_System_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_test_system_version__c IS NOT NULL
          UNION
          SELECT t1.id                               AS tt_legacy_id_hs,
                 t1.NAME                             AS tt_hs_name,
                 t1.account_name__c                  AS tt_legacysfdcaccountid__c,
                 t3.id                               AS account__c,
                 t3.NAME                             AS account_name,
                 t1.visinet_training_system_patch__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Training_System_Patch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_training_system_patch__c IS NOT NULL
          UNION
          SELECT t1.id                                 AS tt_legacy_id_hs,
                 t1.NAME                               AS tt_hs_name,
                 t1.account_name__c                    AS tt_legacysfdcaccountid__c,
                 t3.id                                 AS account__c,
                 t3.NAME                               AS account_name,
                 t1.visinet_training_system_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'VisiNet_Training_System_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.visinet_training_system_version__c IS NOT NULL
          UNION
          SELECT t1.id                       AS tt_legacy_id_hs,
                 t1.NAME                     AS tt_hs_name,
                 t1.account_name__c          AS tt_legacysfdcaccountid__c,
                 t3.id                       AS account__c,
                 t3.NAME                     AS account_name,
                 t1.x911_software_version__c AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'X911_Software_Version__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
          WHERE  t1.x911_software_version__c IS NOT NULL
          UNION
          --new 2/13
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 t1.imp_public_safety_records_mgmt_software__c    AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'IMP_Public_Safety_Records_Mgmt_Software__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.imp_public_safety_records_mgmt_software__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 t1.imp_vcad_visual_computer_aided_dispatch__c    AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'IMP_VCAD_Visual_Computer_Aided_Dispatch__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.imp_vcad_visual_computer_aided_dispatch__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 t1.imp_comm_server__c                            AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'IMP_Comm_Server__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.imp_comm_server__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 t1.imp_advanced_mobile_online__c                 AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'IMP_Advanced_Mobile_Online__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.imp_advanced_mobile_online__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 t1.tib_prd_build__c                              AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Tib_PRD_Build__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.tib_prd_build__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 Cast( t1.tib_prd_build_date__c AS NVARCHAR(255)) AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Tib_PRD_Build_Date__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.tib_prd_build_date__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 t1.tib_trn_build__c                              AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Tib_TRN_Build__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.tib_trn_build__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 Cast( t1.tib_trn_build_date__c AS NVARCHAR(255)) AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Tib_TRN_Build_Date__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.tib_trn_build_date__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 t1.tib_dev_build__c                              AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Tib_DEV_Build__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.tib_dev_build__c IS NOT NULL
          UNION
          SELECT t1.id                                            AS tt_legacy_id_hs,
                 t1.NAME                                          AS tt_hs_name,
                 t1.account_name__c                               AS tt_legacysfdcaccountid__c,
                 COALESCE( t3.id, sam.[18-digit sup account id] ) AS account__c,
                 COALESCE( t3.NAME, sam.[sup account name] )      AS account_name,
                 Cast( t1.tib_dev_build_date__c AS NVARCHAR(255)) AS version,
                 t2.sourcefieldname,
                 t2.productgroup,
                 t2.productline,
                 t2.environmenttype
          FROM   tritech_prod.dbo.HARDWARE_SOFTWARE__C t1
                 JOIN staging_prod.dbo.SPV_HARDWARESOFTWARE_MAPPING t2
                   ON t2.sourcefieldname = t2.sourcefieldname AND
                      t2.sourcefieldname = 'Tib_DEV_Build_Date__c'
                 LEFT JOIN superion_production.dbo.ACCOUNT t3
                        ON t3.legacysfdcaccountid__c = t1.account_name__c
                 LEFT JOIN staging_prod.dbo.TT_SUPERION_ACCOUNT_MERGE sam --to handle account merges
                        ON sam.[18-digit tt account id] = t1.account_name__c
          WHERE  t1.tib_dev_build_date__c IS NOT NULL
          UNION
          SELECT NULL                      AS tt_legacy_id_hs,
                 NULL                      AS tt_hs_name,
                 t1.legacysfdcaccountid__c AS tt_legacysfdcaccountid__c,
                 t1.account__c             AS account__c,
                 t1.NAME                   AS account_name,
                 NULL                      AS version,
                 NULL                      AS sourcefieldname,
                 'Zuercher'                AS productgroup,
                 'CAD'                     asproductline,
                 'Production'              AS environmenttype
          FROM   ( SELECT DISTINCT account__c,
                                   a.legacysfdcaccountid__c,
                                   a.NAME
                   FROM   superion_production.dbo.REGISTERED_PRODUCT__C r
                          JOIN superion_production.dbo.ACCOUNT a
                            ON r.account__c = a.id
                   WHERE  r.NAME LIKE '%Zuercher%' ) t1
          UNION
          SELECT NULL                      AS tt_legacy_id_hs,
                 NULL                      AS tt_hs_name,
                 t1.legacysfdcaccountid__c AS tt_legacysfdcaccountid__c,
                 t1.account__c             AS account__c,
                 t1.NAME                   AS account_name,
                 NULL                      AS version,
                 NULL                      AS sourcefieldname,
                 'Respond'                 AS productgroup,
                 'Billing'                 AS productline,
                 'Production'              AS environmenttype
          FROM   ( SELECT DISTINCT account__c,
                                   a.legacysfdcaccountid__c,
                                   a.NAME
                   FROM   superion_production.dbo.REGISTERED_PRODUCT__C r
                          JOIN superion_production.dbo.ACCOUNT a
                            ON r.account__c = a.id
                   WHERE  r.NAME LIKE '%respond%' ) t1
          UNION
          SELECT NULL                      AS tt_legacy_id_hs,
                 NULL                      AS tt_hs_name,
                 t1.legacysfdcaccountid__c AS tt_legacysfdcaccountid__c,
                 t1.account__c             AS account__c,
                 t1.NAME                   AS account_name,
                 NULL                      AS version,
                 NULL                      AS sourcefieldname,
                 'MetroAlert'              AS productgroup,
                 'RMS'                     AS productline,
                 'Production'              AS environmenttype
          FROM   ( SELECT DISTINCT account__c,
                                   a.legacysfdcaccountid__c,
                                   a.NAME
                   FROM   superion_production.dbo.REGISTERED_PRODUCT__C r
                          JOIN superion_production.dbo.ACCOUNT a
                            ON r.account__c = a.id
                   WHERE  r.NAME LIKE '%Metro%' ) t1
          UNION
          SELECT NULL                      AS tt_legacy_id_hs,
                 NULL                      AS tt_hs_name,
                 t1.legacysfdcaccountid__c AS tt_legacysfdcaccountid__c,
                 t1.account__c             AS account__c,
                 t1.NAME                   AS account_name,
                 NULL                      AS version,
                 NULL                      AS sourcefieldname,
                 'LETG'                    AS productgroup,
                 'CAD'                     AS productline,
                 'Production'              AS environmenttype
          FROM   ( SELECT DISTINCT account__c,
                                   a.legacysfdcaccountid__c,
                                   a.NAME
                   FROM   superion_production.dbo.REGISTERED_PRODUCT__C r
                          JOIN superion_production.dbo.ACCOUNT a
                            ON r.account__c = a.id
                   WHERE  r.NAME LIKE '%LETG%' ) t1
          UNION
          SELECT NULL                      AS tt_legacy_id_hs,
                 NULL                      AS tt_hs_name,
                 t1.legacysfdcaccountid__c AS tt_legacysfdcaccountid__c,
                 t1.account__c             AS account__c,
                 t1.NAME                   AS account_name,
                 NULL                      AS version,
                 NULL                      AS sourcefieldname,
                 'PSSI'                    AS productgroup,
                 'CAD'                     AS productline,
                 'Production'              AS environmenttype
          FROM   ( SELECT DISTINCT account__c,
                                   a.legacysfdcaccountid__c,
                                   a.NAME
                   FROM   superion_production.dbo.REGISTERED_PRODUCT__C r
                          JOIN superion_production.dbo.ACCOUNT a
                            ON r.account__c = a.id
                   WHERE  r.NAME LIKE '%PSSI%' ) t1 --end new 2/13
         ),
     cte_registered_product
     AS --4/8
     ( SELECT rp.id               AS ID,
              rp.NAME             AS NAME,
              rp.account__c       AS account__c,
              pr.id               AS Product2Id,
              pr.product_group__c AS Product_Group,
              pr.product_line__c  AS Product_Line,
              rp.createddate
       FROM   superion_production.dbo.REGISTERED_PRODUCT__C rp
              LEFT JOIN superion_production.dbo.PRODUCT2 pr
                     ON pr.id = rp.product__c
              JOIN superion_production.dbo.CUSTOMER_ASSET__C cass
                ON rp.customerasset__c = cass.id
              LEFT JOIN superion_production.dbo.SYSTEM_PRODUCT_VERSION__C spv
                     ON spv.registered_product__c = rp.id
       WHERE  rp.NAME <> 'Customer Web Portal' AND
              cass.migratedasset__c = 'True' AND
              pr.ttz_product__c = 'True'
              --cass.product_group__c in
              --(
              --'Inform','IQ','PSSI','Tiburon','ETI','Impact','LETG','MetroAlert','IMC','Respond','Zuercher','VisiNet','VisionAIR'
              --)
              AND
              cass.eligible_for_registered_product__c = 'true' AND
              spv.registered_product__c IS NULL )
SELECT DISTINCT Cast ( NULL AS NCHAR(18))                                                                               AS ID,
                Cast ( NULL AS NVARCHAR(255))                                                                           AS Error,
                COALESCE( regprd.id, 'ID NOT FOUND' )                                                                   AS Registered_Product__c,
                COALESCE( regprd.NAME, 'REG PROD NOT FOUND' )                                                           AS [Registered Product Name],
                COALESCE( COALESCE( cte_env.id, COALESCE( cte_env2.id, cte_env3.id ) ), 'ID NOT FOUND' )                AS Environment__c,
                COALESCE( COALESCE( cte_env.NAME, COALESCE( cte_env2.NAME, cte_env3.NAME ) ), 'ENVIRONMENT NOT FOUND' ) AS [Environment Name],
                CASE
                  WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c, cte_mrv.id ) IS NOT NULL THEN COALESCE( cte_mrv.replaceexistingwiththisversion__c, cte_mrv.id )
                  WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
                  WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id )
                  ELSE 'Id not found'
                END                                                                                                     AS Version__c,
                CASE
                  WHEN COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL THEN COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME )
                  WHEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
                  WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME )
                  ELSE 'VERSION NOT FOUND'
                END                                                                                                     AS [VERSION_NAME],
                CTE_TT_HS.tt_legacy_id_hs,--2/19
                CTE_TT_HS.tt_hs_name,--2/19
                CTE_TT_HS.tt_legacysfdcaccountid__c,--2/19
                CTE_TT_HS.account__c,--2/19
                CTE_TT_HS.account_name,--2/19
                CTE_TT_HS.[version],--2/19
                CTE_TT_HS.sourcefieldname,--2/19
                CTE_TT_HS.productgroup,--2/19
                CTE_TT_HS.productline,--2/19
                CASE
                  WHEN regprd.[name] LIKE 'Z%' AND --2/19
                       ( regprd.[name] LIKE '%test%' AND --2/19
                         regprd.[name] LIKE '%Train%' ) THEN 'Test' --2/19
                  ELSE CTE_TT_HS.environmenttype
                END                                                                                                     AS EnvironmentType,
                CASE
                  WHEN cte_vers.replaceexistingwiththisversion__c IS NOT NULL  OR
                       cte_vers_2.replaceexistingwiththisversion__c IS NOT NULL THEN 'True'
                  ELSE 'False'
                END                                                                                                     AS Replace_Existing_Version,
                cte_vers.legacyttzversion__c                                                                            AS [cte_vers.LegacyTTZVersion__c],
                CTE_TT_HS.[version]                                                                                     AS [cte_tt_hs.[Version],
                cte_mrv.legacymajorreleaseversion__c                                                                    AS [cte_mrv.LegacyMajorReleaseVersion__c],
                regprd.product_group                                                                                    AS RegProd_ProductGroup,
                regprd.product_line                                                                                     AS RegProd_ProductLine,
                regprd.account__c                                                                                       AS RegProd_AccountId
INTO   System_Product_Version__c_Insert_delta_preload                                                                                                                                                                            
FROM   cte_registered_product regprd
       LEFT JOIN cte_tt_hs cte_tt_hs
              ON regprd.account__c = CTE_TT_HS.account__c AND
                 regprd.product_group = CTE_TT_HS.productgroup AND
                 regprd.product_line = CTE_TT_HS.productline
       LEFT JOIN cte_sup_environment cte_env
              ON cte_env.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env.type__c AND
                 cte_env.NAME LIKE '%' + CTE_TT_HS.productgroup + '%'
       LEFT JOIN cte_sup_environment cte_env2
              ON cte_env2.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env2.type__c AND
                 cte_env2.NAME LIKE '%visi%' AND --2/13
                 CTE_TT_HS.productgroup = 'Inform'
       LEFT JOIN cte_version_majorrelease cte_mrv
              ON CTE_TT_HS.[version] = cte_mrv.legacymajorreleaseversion__c
       LEFT JOIN cte_sup_environment cte_env3 --TRY match on account id and default env
              ON cte_env3.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env3.type__c AND
                 cte_env3.description__c LIKE '%Automatically%'
       LEFT JOIN cte_version cte_vers
              ON cte_vers.productline__c = CTE_TT_HS.productline AND
                 cte_vers.legacyttzversion__c LIKE '%' + CTE_TT_HS.[version] + '%'
       LEFT JOIN cte_version_2 cte_vers_2
              ON COALESCE( cte_vers_2.legacyttzproductgroup__c, cte_vers_2.product_group__c ) = CTE_TT_HS.productgroup AND
                 cte_vers_2.legacyttzversion__c LIKE '%' + CTE_TT_HS.[version] + '%' AND
                 cte_vers_2.[rank] = 1;


--Create load table
SELECT *
INTO   System_Product_Version__c_Insert_delta_load 
FROM   ( SELECT *,
                Row_number( )
                  OVER (
                    partition BY environment__c, registered_product__c, account__c
                    ORDER BY CASE WHEN version = 'id not found' THEN 0 ELSE Len(version) END DESC) [rank]
         FROM   System_Product_Version__c_Insert_delta_preload --2/19
         WHERE
         registered_product__c <> 'ID NOT FOUND' ) x
WHERE  x.[rank] = 1; 


--------------------------------------------------------------------------------------------
-- Section 3 � Transform Load Table. (Execute various updates to the load table)
--------------------------------------------------------------------------------------------

--- Execute various updates to the load table.

---Null out versions where the SFDC ID could not be found.
SELECT *
FROM   System_Product_Version__c_Insert_delta_load
WHERE  version__c = 'ID NOT FOUND';

UPDATE System_Product_Version__c_Insert_delta_load --2/19
SET    version__c = NULL
WHERE  version__c = 'ID NOT FOUND'; 




--create Training SPV's for Zuercher "Train/Test"  registered products. Only "Test" env are generated by the script. 
INSERT INTO System_Product_Version__c_Insert_delta_load
            (id,
             error,
             registered_product__c,
             [registered product name],
             environment__c,
             [environment name],
             version__c,
             version_name,
             tt_legacy_id_hs,
             tt_hs_name,
             tt_legacysfdcaccountid__c,
             account__c,
             account_name,
             version,
             sourcefieldname,
             productgroup,
             productline,
             environmenttype,
             replace_existing_version,
             [cte_vers.legacyttzversion__c],
             [cte_tt_hs.[version],
             [cte_mrv.legacymajorreleaseversion__c],
             regprod_productgroup,
             regprod_productline,
             regprod_accountid,
             [rank])
SELECT id,
       error,
       registered_product__c,
       [registered product name],
       environment__c,
       [environment name],
       version__c,
       version_name,
       tt_legacy_id_hs,
       tt_hs_name,
       tt_legacysfdcaccountid__c,
       account__c,
       account_name,
       version,
       sourcefieldname,
       productgroup,
       productline,
       'Training' AS EnvironmentType,
       replace_existing_version,
       [cte_vers.legacyttzversion__c],
       [cte_tt_hs.[version],
       [cte_mrv.legacymajorreleaseversion__c],
       regprod_productgroup,
       regprod_productline,
       regprod_accountid,
       [rank]
FROM   System_Product_Version__c_Insert_delta_load ld
WHERE  ld.registered_product__c IN
       ( SELECT t1.id AS Zuercher_Registered_Product_Id
         FROM   superion_production.dbo.REGISTERED_PRODUCT__C t1
                LEFT JOIN superion_production.dbo.PRODUCT2 p2
                       ON p2.id = t1.product__c
                JOIN superion_production.dbo.CUSTOMER_ASSET__C t2
                  ON t2.id = t1.customerasset__c
                LEFT JOIN superion_production.dbo.ACCOUNT a
                       ON a.id = t1.account__c
         WHERE  t2.product_group__c = 'Zuercher' ) AND
       [registered product name] LIKE '%test%' AND
       [registered product name] LIKE '%Train%'; 



--- update environments part 1.1 try to match to a  Env. on prod group like env name 
UPDATE t1
SET    environment__c = t2.id,
       [environment name] = t2.NAME
FROM   System_Product_Version__c_Insert_delta_load t1 --2/19
       JOIN superion_production.dbo.ENVIRONMENT__C t2
         ON t1.regprod_accountid = t2.account__c AND
            t2.NAME LIKE '%' + t1.regprod_productgroup + '%' AND
            t2.type__c = COALESCE( t1.environmenttype, 'Production' ) --2/19
            AND
            t2.id NOT IN ( SELECT id
                           FROM   ENVIRONMENT__C_LOAD_NON_ZUERCHER_RT )
WHERE  t1. environment__c = 'ID NOT FOUND';


--update environments part 1.2 try to match to a 'Default' Env.
UPDATE t1
SET    environment__c = t2.id,
       [environment name] = t2.NAME
FROM   System_Product_Version__c_Insert_delta_load t1 --2/19
       JOIN superion_production.dbo.ENVIRONMENT__C t2
         ON t1.regprod_accountid = t2.account__c AND
            t2.description__c LIKE 'Automatically Created%' AND
            t2.type__c = COALESCE( t1.environmenttype, 'Production' ) --2/19
            AND
            t2.id NOT IN ( SELECT id
                           FROM   ENVIRONMENT__C_LOAD_NON_ZUERCHER_RT )
WHERE  t1. environment__c = 'ID NOT FOUND';


--- !!!!!!
-- At this point Execute the 9.2.D Environment__c_Insert_Superoin_Deltas.sql if needed. Run the query below to check if there are SPVs in the load table that need Environments created.
-- To create Environment data for the SPV if an existing Environment is not found.

/*
This querry list the missing Environments that need to be created.
--create environments 
SELECT DISTINCT regprod_productgroup, 
                regprod_accountid,
                CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + regprod_productgroup + '-' + a.name AS Name,
                regprod_accountid                                                                                                                                        AS account__c,
                'Automatically created because no Hardware Software records existed in Tritech source'                                                                   AS description__c,
                CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production' 
				     WHEN EnvironmentType = 'Train' Then 'Training'
					 WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c
FROM   System_Product_Version__c_Insert_delta_load s
       join Superion_Production.dbo.ACCOUNT a
         ON a.id = s.regprod_accountid
WHERE  environment__c = 'ID NOT FOUND';

*/


--------------------------------------------------------------------------------------------
--Section 4 � Missing Environments.  
--------------------------------------------------------------------------------------------
UPDATE t1
SET    environment__c = t2.id,
       [environment name] = t2.[NAME]
FROM   System_Product_Version__c_Insert_delta_load t1
       JOIN staging_prod.dbo.Environment__c_Load_Non_Zuercher t2
         ON t1.regprod_accountid = t2.account__c AND
            t2.regprod_productgroup = t1.regprod_productgroup
WHERE  t1. environment__c = 'ID NOT FOUND';


-------------------------------------------------------------------------------------------------------------------------
--Section 5 � Checks.  (all these querries should return 0 records; fix any records that are returned by these querries.)
-------------------------------------------------------------------------------------------------------------------------
select * from System_Product_Version__c_Insert_delta_load where version__c = 'ID NOT FOUND';

select * from System_Product_Version__c_Insert_delta_load where environment__c = 'ID NOT FOUND';

select * from System_Product_Version__c_Insert_delta_load where environment__c = 'ID NOT FOUND'
and  RegProd_AccountId in
(
select Account__c , count(*) from Superion_Production.dbo.Environment__c
group by Account__c, Name
having count(*) > 1
)

select * from System_Product_Version__c_Insert_delta_load where Registered_Product__c = 'ID NOT FOUND';

select environment__c, Registered_Product__c , count(*) from System_Product_Version__c_Insert_delta_load group by environment__c, Registered_Product__c having count(*) > 1


-------------------------------------------------------------------------------------------------------------------------
--Section 6 � Data Import.  (Import the data into Salesforce Org.)
-- note these lines are commented out only to avoid accidental execution.
-------------------------------------------------------------------------------------------------------------------------

-------------------------------------------
-- COLCOMPARE: (check load table columns)
-------------------------------------------
-- use Staging_Prod;

-- exec SF_ColCompare 'Insert','Superion_PROD','System_Product_Version__c_Insert_delta_load';


-------------------------------------------
-- BULKOPS: (actual insert operation)
-------------------------------------------
-- use Staging_Prod;

-- exec SF_ColCompare 'Insert','Superion_PROD','System_Product_Version__c_Insert_delta_load';

