/*****************************************************************************************************************************************************
REQ #		: REQ-0831
TITLE		: Migrate Potential_Defect__c - Source System to SFDC
DEVELOPER	: RTECSON
CREATED DT  : 12/07/2018
DETAIL		: 	
				Selection Criteria:
				a. Migrate all related defects related to a migrated case.

				Discussion: Will migrate to Engineering_Issue__c
				need to Validate with Engineering what their requirements are, and need to also confirm requirements specific to defects NOT related to a case.
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
12/07/2018		Ron Tecson			Initial
12/10/2018		Ron Tecson			Fixed the Original Customer logic. Needed to reload the target table.
12/11/2018		Ron Tecson		    Fixed Legacy_TT_Defect_Number to Legacy_TT_Defect_Number__c.
									De-scripted sPD1.Legacy_Defect_Ticket_Number__c per Gabe.
									Set the transformation for the Priority__c per Gabe.
									Added TFS_Work_Item__c to the script.
12/14/2018		Ron Tecson			Modified to add the transformation logic of the Priority__c field. Proposed transformation was approved by Gabe.
12/15/2018		Ron Tecson			Per Gabe. Descript the ENGOwner__c.
01/02/2019		Ron	Tecson			Fixed the Technical Description related to RES-2237 (TEST-0280). https://superion--sandbox.cs66.my.salesforce.com/a050v000001eBg9
01/04/2019		Ron Tecson			Added the additional fields and new logics from the data mapping.
01/09/2019		Ron Tecson			Fixed the target field ReleasedIn__c.
02/04/2019		Ron Tecson			Added the following for integration with TFS:
									- 'TFS' as Target_System__c
									- sPD1.TFS_Work_Item__c as ENGIssue__c
									Run a hotfix to populate the 2 fields for TFS.
02/28/2019		Ron Tecson			Loaded data against MapleRoots.
03/20/2019		Ron Tecson			Loaded data against MapleRoots and log time and errors.
03/26/2019		Ron Tecson			Modified based from additional new fields and changed logic from Gabe and Jeff Beard for Integration Requirements.
03/28/2019		Ron Tecson			Repointed to Production.
03/29/2019      Ron Tecson		    Executed for Prod load.
03/30/2019		Ron Tecson			Applied a hotfix per Jeff Beard ( 1 of 2). The 2nd Hotfix is another script.

DECISIONS:

ERROR/FAILURES:

DATE			ERROR													RESOLUTION
=============== ======================================================= ========================================================================================
03/20/2019		Support Account: id value of incorrect type: Account	Accounts associated to the Tritech Cases doesn't exists in the target Org.
					Not Found

******************************************************************************************************************************************************/


USE Tritech_PROD

EXEC SF_Refresh 'MC_TRITECH_PROD',Potential_Defect__c, Yes 


USE SUPERION_Production

	EXEC SF_Refresh 'RT_Superion_PROD',EngineeringIssue__c, Yes
	EXEC SF_Refresh 'RT_Superion_PROD',Account, Yes
	EXEC SF_Refresh 'RT_Superion_PROD','User', Yes
	EXEC SF_Refresh 'RT_Superion_PROD','Version__c', Yes


USE Staging_PROD

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'EngineeringIssue__c_Preload' AND
            table_schema = 'dbo' )
  DROP TABLE Staging_PROD.[dbo].[EngineeringIssue__c_Preload]; 

DECLARE @UserDefault as NVARCHAR(18) = (select id from SUPERION_Production.dbo.[USER] where Name = 'Superion API'),
		@VersionDefault as NVARCHAR(18) = (select id from SUPERION_Production.dbo.[Version__c] where Name = 'PSJ-Inform v1' and LegacyTTZProductGroup__c  = 'TC CAD/Mobile');

select 
ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
,sPD1.Id AS Legacy_Id__c
,IIF(tAcc2.Id IS NOT NULL, sPD1.Original_Customer__c, sPD1.Account_Name__c) AS AccountId_Orig
,sPD1.Original_Contact__c AS Contact_Orig
,IIF(tCon1.Id IS NOT NULL,tCon1.ID,NULL) AS Support_Contact__c
,CASE sPD1.Area_Path__c
	WHEN 'CustomInterface' THEN 'TriTech-IntegratedSolutions'
	WHEN 'EMSBilling' THEN 'Respond-Billing'
	WHEN 'ePCR' THEN 'Respond-ePCR'
	WHEN 'Inform 5' THEN 'TriTech-IQ'
	WHEN 'Inform 911' THEN 'Tritech-911'
	WHEN 'InformCAD911' THEN 'Inform-CAD911'
	WHEN 'IntegratedSolutions' THEN 'TriTech-IntegratedSolutions'
	WHEN 'TTMS' THEN 'TriTech-MessageSwitch'
	WHEN 'VisionCAD MOBILE MOL' THEN 'Vision-CADMobile'
	WHEN 'VisionRMSJAILFBRFIRE MOL' THEN 'Inform-RMS'
	ELSE sPD1.Area_Path__c
 END AS ENGProject__c
,sPD1.CreatedById AS CreatedById_Orig
,IIF(tUser1.Id IS NOT NULL, tUser1.Id, @UserDefault) AS CreatedById -- 0056A000000lfqkQAA
,sPD1.CreatedDate AS CreatedDate
,sPD1.Defect_Ticket_ID__c AS Legacy_TT_Defect_Number__c
,CASE 
	WHEN sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NOT NULL AND sPD1.CCB_Notes__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10),CAST(sPD1.TFS_Comments__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.TFS_Comments__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10), CAST(sPD1.TFS_Comments__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CONCAT('CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CAST(sPD1.Description__c as nvarchar(max))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CAST(sPD1.TFS_Comments__c as nvarchar(max))
	ELSE NULL
END AS Technical_Description__c
--,sPD1.Found_in_Build__c AS Version__c
,sPD1.Id AS Legacy_CRMId
,sPD1.Internal_Steps_to_Recreate_Issue__c AS TroubleshootingSteps__c
--,sPD1.Known_Issues_Description__c AS KnownIssueDescription__c -- 1/3 RT. Descript. This field should be sourced from Case.
--,sPD1.Legacy_Defect_Ticket_Number__c AS Legacy_CRMId__c
,sPD1.MOL_Score__c AS ENGEngineering_Value__c
,sPD1.Name AS Summary__c
,sPD1.OwnerId  AS OwnerID_Orig
--,IIF(tOwner.Id IS NOT NULL, tOwner.Id, @UserDefault) AS ENGOwner__c
,sPD1.Patch_Release_Branch__c AS Patch_Release_Branch__c
,sPD1.Patch_Status_Comments__c AS Patch_Status_Comments__c
,CASE 
	WHEN sPD1.Priority__c like '%1' THEN '1 - Urgent'
	WHEN sPD1.Priority__c like '%2' THEN '2 - Critical'
	WHEN sPD1.Priority__c like '%3' THEN '3 - Non Critical'
	ELSE '4 - Minor'
 END AS Priority__c
--,sPD1.Product_Sub_Module__c AS Problem_Code__c
,sPD1.Released_In__c AS ReleasedIn__c
--,sPD1.Reported_Major_Version__c AS Version__c
,sPD1.Resolution_Notes__c AS ENGResolution__c
--,sPD1.Severity__c AS Severity__c
,sPD1.Priority__c as sPD1_Priority__c_Orig
,sPD1.Priority__c AS Severity__c
,tSOwner.Id AS Support_Owner__c
,sPD1.Target_Patch_Release_Date__c AS Target_Patch_Release_Date__c
,sPD1.TFS_Area_Path__c AS sPD1_TFS_Area_Path__c_Orig
--,IIF(sPD1.TFS_Area_Path__c = NULL AND sPD1.TFS_Area_Path__c = '', sPD1.Area_Path__c, sPD1.TFS_Area_Path__c) AS ENGAreaPath__c
,IIF(sPD1.TFS_Area_Path__c IS NULL, sPD1.Area_Path__c, sPD1.TFS_Area_Path__c) AS ENGAreaPath__c
,sPD1.TFS_Collection__c AS Collection__c
, CASE	
	WHEN sPD1.Ticket_Status__c = 'Assessing Issue ' OR sPD1.Ticket_Status__c = 'Fix for Release in Progress' THEN 'Committed'
	WHEN sPD1.Ticket_Status__c = 'Duplicate ' THEN 'Removed' 
	WHEN sPD1.Ticket_Status__c = 'Fix ready for Release' THEN 'Done' 
	WHEN sPD1.Ticket_Status__c = 'Future Consideration' THEN 'New'
  END AS ENGEngineering_Status__c
, sCase1.Patch_Requested_Date_Time__c AS Patch_Requested_Date_Time__c
, CASE
	WHEN sCase1.Known_Issue_Confirmed__c = 'true' THEN 'Approved' 
	WHEN sCase1.Known_Issues_List__c = 'true' and sCase1.Known_Issue_Confirmed__c = 'false' THEN 'Submitted'
END AS KnownIssueStatus__c
, sCase1.Patch_Status__c as Patch_Status__c_Orig
, CASE 
	WHEN sCase1.Patch_Status__c = 'Assess for Patch' OR sCase1.Patch_Status__c = 'Pending Decision to Assess' OR sCase1.Patch_Status__c = 'Pending Decision to Patch' THEN 'Patch Requested'
	ELSE sCase1.Patch_Status__c
END AS Patch_Status__c
, IIF(sCase1.Known_Issue_Confirmed__c = 'true', sCase1.Known_Issue_Description__c, NULL) AS KnownIssueDescription__c
--, IIF(sCase1.Known_Issues_List__c = 'true' and sCase1.Known_Issue_Confirmed__c = 'false', 'Submitted', NULL)
, sPD1.TFS_Work_Item__c AS TFSWorkItemLegacyID__c
, tCase2.AccountId AS Support_Account__c
, 'TFS' as Target_System__c
, sPD1.TFS_Work_Item__c as ENGIssue__c
, sCase1.Id as srcCaseId
, sAccount.Name AS src_AccountName
, sAccount.Id As Legacy_Account_Id
, @VersionDefault AS Version__c
INTO Staging_PROD.[dbo].[EngineeringIssue__c_Preload] --3/28 
FROM tritech_Prod.dbo.[case] sCase1
JOIN [Tritech_PROD].[dbo].[Potential_Defect__c] sPD1 ON sPD1.Defect_Ticket_ID__c = sCase1.Defect_Number__c
LEFT JOIN SUPERION_Production.dbo.Account tAcc1 ON tAcc1.LegacySFDCAccountId__c = sPD1.Account_Name__c
LEFT JOIN SUPERION_Production.dbo.[User] tUser1 ON tUser1.Legacy_Tritech_Id__c = sPD1.CreatedById
LEFT JOIN SUPERION_Production.dbo.[User] tOwner ON tOwner.Legacy_Tritech_Id__c = sPD1.OwnerId
LEFT JOIN SUPERION_Production.dbo.[User] tSOwner ON tSOwner.Email = sPD1.Support_Ticket_Owner_Email__c
LEFT JOIN SUPERION_Production.dbo.Account tAcc2 ON tAcc2.LegacySFDCAccountId__c = sPD1.Original_Customer__c
LEFT JOIN SUPERION_Production.dbo.Contact tCon1 ON tCon1.Legacy_ID__c = sPD1.Original_Contact__c
LEFT JOIN Staging_PROD.dbo.Case_Tritech_SFDC_load tCase2 ON tCase2.Legacy_id__c = sCase1.Id
LEFT JOIN tritech_Prod.dbo.Account sAccount ON sAccount.Id = sCase1.AccountId -- 3/20 RT. Added for troublshooting purposes.
WHERE
sPD1.Ticket_Status__c in (
'Assessing Issue',
'Duplicate',
'Fix for Release in Progress',
'Fix ready for Release',
'Future Consideration')

-- 3/29 -- (16337 row(s) affected)

IF EXISTS
( SELECT *
    FROM   information_schema.TABLES
    WHERE  table_name = 'EngineeringIssue__c_Load' AND
        table_schema = 'dbo' )
DROP TABLE Staging_PROD.[dbo].[EngineeringIssue__c_Load]; 

SELECT *
INTO Staging_PROD.dbo.EngineeringIssue__c_Load
FROM Staging_PROD.dbo.EngineeringIssue__c_Preload 

-- (15725 rows affected) 01/04
-- (16157 rows affected) 02/28
-- (16267 rows affected) 03/20
--(16337 row(s) affected) 3/28 Prod.



USE Staging_PROD

EXEC SF_ColCompare 'Insert','RT_Superion_PROD', 'EngineeringIssue__c_Load' 

select * from RT_Superion_PROD...EngineeringIssue__c where EngIssue__c = '251318'

/************************************* L   O   G   S **************************************
3/28
--- Starting SF_ColCompare V3.6.7
Problems found with EngineeringIssue__c_Load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 212]
--- Ending SF_ColCompare. Operation FAILED.

ErrorDesc
Salesforce object EngineeringIssue__c does not contain column AccountId_Orig
Salesforce object EngineeringIssue__c does not contain column Contact_Orig
Salesforce object EngineeringIssue__c does not contain column CreatedById_Orig
Salesforce object EngineeringIssue__c does not contain column Legacy_CRMId
Salesforce object EngineeringIssue__c does not contain column OwnerID_Orig
Salesforce object EngineeringIssue__c does not contain column sPD1_Priority__c_Orig
Salesforce object EngineeringIssue__c does not contain column sPD1_TFS_Area_Path__c_Orig
Salesforce object EngineeringIssue__c does not contain column Patch_Status__c_Orig
Salesforce object EngineeringIssue__c does not contain column srcCaseId
Salesforce object EngineeringIssue__c does not contain column src_AccountName
Salesforce object EngineeringIssue__c does not contain column Legacy_Account_Id
Column Target_System__c is not insertable into the salesforce object EngineeringIssue__c --this is a formula field so it is not insertable.


************************************** L   O   G   S **************************************/

EXEC SF_BulkOps 'INSERT','RT_Superion_PROD', 'EngineeringIssue__c_Load'

/************************************** L   O   G   S **************************************

3/28
--- Starting SF_BulkOps for EngineeringIssue__c_Load V3.6.7
13:32:01: Run the DBAmp.exe program.
13:32:01: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
13:32:01: Inserting EngineeringIssue__c_Load (SQL01 / Staging_PROD).
13:32:02: DBAmp is using the SQL Native Client.
13:32:02: SOAP Headers: 
13:32:02: Warning: Column 'AccountId_Orig' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'Contact_Orig' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'CreatedById_Orig' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'Legacy_CRMId' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'OwnerID_Orig' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'sPD1_Priority__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'sPD1_TFS_Area_Path__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'Patch_Status__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'Target_System__c' ignored because it not insertable in the EngineeringIssue__c object.
13:32:02: Warning: Column 'srcCaseId' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'src_AccountName' ignored because it does not exist in the EngineeringIssue__c object.
13:32:02: Warning: Column 'Legacy_Account_Id' ignored because it does not exist in the EngineeringIssue__c object.
13:34:03: 16337 rows read from SQL Table.
13:34:03: 16337 rows successfully processed.
13:34:03: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


************************************** L   O   G   S **************************************/

select ERROR, count(*)
from EngineeringIssue__c_Load
group by ERROR

select * from EngineeringIssue__c_Load where error = 'Value too large max length:30000 Your length: 31013'

select * from EngineeringIssue__c_Load where Error = 'Patch Status: bad value for restricted picklist field: Patch Requested'

'insufficient access rights on cross-reference id: 0018000001KXiSf'

select * from Superion_FULLSB.dbo.Account where Id = '0018000001KXiSfAAL'

select * from EngineeringIssue__c_Load where error not like '%success%'

select Id, AccountId, count(*) from Staging_PROD.dbo.Case_Tritech_SFDC_load where AccountId = 'Account Not Found'
group by Id, AccountId

/************************************** REFRESH LOCAL DATABASE **************************************/

USE SUPERION_Production
EXEC SF_Refresh 'RT_Superion_PROD',EngineeringIssue__c, Yes

-------------------------------------------------------------------------
-- Production - HOTFIXes
-------------------------------------------------------------------------

/* 1.) Per Jeff Beard, delete the EngineeringIssue__c where TFSWorkItemLegacyID__c IS NULL from the migrated records */

select * INTO EngineeringIssue__c_Del_TFSWorkItemLegacyID_Null
from EngineeringIssue__c_Load
where TFSWorkItemLegacyID__c IS NULL

Exec SF_BulkOps 'DELETE', 'RT_Superion_PROD', 'EngineeringIssue__c_Del_TFSWorkItemLegacyID_Null' 

/* 2.) -	Migrate the Potential Defect Records where the Ticket_Status__c = ‘Targeted for Future Release’. The transformation 
value for this ticket status will be ‘New’ in Production. In the current Tritech Org, there is a total of 82 P-Defect records with the Status of “Targeted for Future Release”.
*/