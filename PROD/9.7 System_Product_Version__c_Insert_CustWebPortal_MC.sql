-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 9.7 System_Product_Version__c_Insert_CustWebPortal_MC.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--  REQ-0832
--- Copyright Apps Associates 2019
--- Created Date: Jan 25, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-01-25 Created.
--- 2019-01-29 Change logic for Envrironment lookup per Gabe.
--- 2019-01-30 Added logic to include only TTZ Accounts and client__c = true.
--  2019-02-05 Wipe and Reload.
--  2019-02-12 Wipe and Reload.
--  2019-02-26 Wipe and Reload.
--  2019-03-14 Only one spv per customer web portal registered product per Maria. Pick any environment.
--  2019-03-30 Executed for Prod.

--- Prerequisites for Script to execute
--- 
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-02-12 Wipe and Reloaded in Superion FullSbx.
-- 2019-02-15 Wipe and Reloaded in Superion FullSbx.
-- 2019-02-18 Wipe and Reloaded in Superion FullSbx.
-- 2019-02-26 Wipe and Reloaded in Superion FullSbx.
-- 2019-03-03 Wipe and Reloaded in Superion FullSbx.
-- 2019-03-08 delete last SPV load in prep for Asset load.
-- 2019-03-15 wipe.
-- 2019-03-25 Wipe and Reload.
-- 2019-03-28 Move to Production.
-- 2019-03-30 Executed for Prod.
-- (4671 row(s) affected) 3/25

-- WIPE: 
-- SELECT * INTO System_Product_Version__c_Delete_WebPortal_MC_25Mar from System_Product_Version__c_Insert_Webportal_MC_Load where error = 'Operation Successful.' 
--(4671 row(s) affected) 3/25

--select * INTO System_Product_Version__c_Delete_WebPortal_MC_25Mar2 from System_Product_Version__c_Insert_Webportal_MC_Load  where error = 'Operation Successful.' 





-- RELOAD: 3/26

-- COLCOMPARE:
-- EXEC SF_ColCompare 'Insert','MC_SUPERION_PROD','System_Product_Version__c_Insert_Webportal_MC_Load' 
/*
--- Starting SF_ColCompare V3.6.7
Problems found with System_Product_Version__c_Insert_Webportal_MC_Load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 50]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object System_Product_Version__c does not contain column registered_product_name
Salesforce object System_Product_Version__c does not contain column registered_product_accountid
Salesforce object System_Product_Version__c does not contain column environment_name
Salesforce object System_Product_Version__c does not contain column version
Salesforce object System_Product_Version__c does not contain column accountname
Salesforce object System_Product_Version__c does not contain column legacy_tt_accountid
*/

-- INSERT:
-- EXEC SF_Bulkops 'Insert','MC_SUPERION_PROD','System_Product_Version__c_Insert_Webportal_MC_Load'


/*
--- Starting SF_BulkOps for System_Product_Version__c_Insert_Webportal_MC_Load V3.6.7
15:51:27: Run the DBAmp.exe program.
15:51:27: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:51:27: Inserting System_Product_Version__c_Insert_Webportal_MC_Load (SQL01 / Staging_PROD).
15:51:28: DBAmp is using the SQL Native Client.
15:51:29: SOAP Headers: 
15:51:29: Warning: Column 'registered_product_name' ignored because it does not exist in the System_Product_Version__c object.
15:51:29: Warning: Column 'registered_product_accountid' ignored because it does not exist in the System_Product_Version__c object.
15:51:29: Warning: Column 'environment_name' ignored because it does not exist in the System_Product_Version__c object.
15:51:29: Warning: Column 'version' ignored because it does not exist in the System_Product_Version__c object.
15:51:29: Warning: Column 'accountname' ignored because it does not exist in the System_Product_Version__c object.
15:51:29: Warning: Column 'legacy_tt_accountid' ignored because it does not exist in the System_Product_Version__c object.
15:51:45: 4845 rows read from SQL Table.
15:51:45: 4845 rows successfully processed.
15:51:45: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.



select error, count(*)
from System_Product_Version__c_Insert_Webportal_MC_Load
group by error

error	(No column name)
Environment: id value of incorrect type: ID NOT FOUND	1
Operation Successful.	5167

select * from System_Product_Version__c_Insert_Webportal_MC_Load
where error <> 'Operation Successful.'
*/


---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------


Use Superion_Production;

Exec SF_Refresh 'MC_SUPERION_PROD','CUSTOMER_ASSET__C','Yes'
--- Starting SF_Refresh for CUSTOMER_ASSET__C V3.6.7
--15:40:19: Using Schema Error Action of yes
--15:40:19: Using last run time of 2019-03-30 14:13:00
--15:42:12: Identified 133720 updated/inserted rows.
--15:42:12: Identified 0 deleted rows.
--15:42:12: Adding updated/inserted rows into CUSTOMER_ASSET__C
--- Ending SF_Refresh. Operation successful.


Exec SF_Refresh 'MC_SUPERION_PROD','Registered_Product__c','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Environment__c','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Version__c','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Account','Yes'


---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_WebPortal_MC_load') 
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_WebPortal_MC_load;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_WebPortal_MC_preload') 
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_WebPortal_MC_preload;


;WITH cte_reg_proudct_custweb_portal
     AS ( SELECT a.NAME AS accountname,
                 rp.*
          FROM   Superion_Production.dbo.REGISTERED_PRODUCT__C rp
                 JOIN Superion_Production.dbo.ACCOUNT a
                   ON a.id = rp.account__c
          WHERE  product__c = '01t6A000000GyHWQA0'
         --and asset_status__c = 'Activated'
         ),
     cte_environment
     AS ( SELECT id,
                 account__c,
                 NAME,
                 type__c
          FROM   Superion_Production.dbo.ENVIRONMENT__C),
         -- WHERE  NAME LIKE '%Customer Web Portal%' )
cte_TTZ_Accounts as
(
SELECT id,
             NAME,
              LegacySFDCAccountId__c AS Legacy_TT_Accountid,
              Client__c
        FROM   Superion_Production.dbo.ACCOUNT
        WHERE  migrated_record__c = 'True' AND
             legacy_source_system__c = 'Tritech'
        UNION
        SELECT ffSAccnt.[18-digit sup account id] AS ID,
            ffSAccnt.[sup account name]        AS NAME,
            ffSAccnt.[18-digit TT Account ID]  AS Legacy_TT_Accountid,
            tAccntCte.Client__c
        FROM   TT_SUPERION_ACCOUNT_MERGE ffSAccnt
        JOIN Superion_Production.dbo.Account tAccntCte ON tAccntCte.Id = ffSAccnt.[18-digit sup account id]
        WHERE  [merge?] = 'Yes'
)
SELECT Cast( NULL AS NCHAR(18))                          AS Id,
       Cast( NULL AS NVARCHAR(255))                       AS Error,
       cwp.id                                            AS Registered_Product__c,
       cwp.NAME                                          AS Registered_Product_name,
	   cwp.Account__c                                    AS Registered_Product_AccountID,
       COALESCE( cte_env.id, 'ID NOT FOUND' )            AS Environment__c,
       COALESCE( cte_env.NAME, 'ENVIRONMENT NOT FOUND' ) AS Environment_Name,
	   NULL                                              AS [Version],
	   cte_ttz.Name                                      AS AccountName,
	   cte_ttz.Legacy_TT_Accountid                       AS Legacy_TT_Accountid
INTO System_Product_Version__c_Insert_WebPortal_MC_preload
FROM   cte_reg_proudct_custweb_portal cwp
            JOIN cte_TTZ_Accounts cte_ttz on cte_ttz.id = cwp.Account__c and
			     cte_ttz.Client__c = 'True'
       LEFT JOIN cte_environment cte_env
              ON cte_env.account__c = cwp.account__c; 
-- (5362 row(s) affected) 3/26
--(5342 row(s) affected) 3/15
--(5325 row(s) affected)  2/26
--(5383 row(s) affected) 3/3
--(5168 row(s) affected) 3/12
--(8267 row(s) affected) 3/30


--create load table

SELECT id,
       error,
       registered_product__c,
       registered_product_name,
       registered_product_accountid,
       environment__c,
       environment_name,
       version,
       accountname,
       legacy_tt_accountid
INTO   System_Product_Version__c_Insert_Webportal_MC_Load
FROM   ( SELECT Row_number( )
                  OVER (
                    partition BY t1.registered_product_accountid
                    ORDER BY (CASE WHEN t1.environment_name LIKE '%Prod%' THEN 1 ELSE 2 END) ) AS "RANK",
                id,
                error,
                registered_product__c,
                registered_product_name,
                registered_product_accountid,
                environment__c,
                environment_name,
                version,
                accountname,
                legacy_tt_accountid
         FROM   System_Product_Version__c_Insert_Webportal_MC_Preload t1 ) x
WHERE  [rank] = 1 
--(4671 row(s) affected) 3/15
--(4671 row(s) affected) 3/26
--(4845 row(s) affected) 3/30


--have ron create missing envs for Customer Web Portal.

update t1
set environment__c = t2.id, [Environment_Name] = t2.Name
from System_Product_Version__c_Insert_Webportal_MC_Load t1 
join Staging_PROD.dbo.Environment__c_Load_Customer_Web_Portal_RT t2 
 on t1.registered_product_accountid = t2.Account__c and t2.registered_product_name = t1.registered_product_name
where t1. environment__c = 'ID NOT FOUND';
--(2059 row(s) affected) 3/26


--checks:

select * from System_Product_Version__c_Insert_Webportal_MC_Load where Environment__c = 'ID NOT FOUND';



select * from System_Product_Version__c_Insert_Webportal_MC_Load where Environment__c is null;

select Registered_Product__c, Environment__c, count(*) from System_Product_Version__c_Insert_Webportal_MC_Load group by Registered_Product__c, Environment__c having count(*) > 1;

--only 1 spv per customer web portal registered product is allowed.
select Registered_Product__c, Registered_Product_AccountID, count(*) 
from System_Product_Version__c_Insert_Webportal_MC_Load 
group by Registered_Product__c, Registered_Product_AccountID having count(*) > 1;

select Registered_Product__c, Environment_Name, count(*) from System_Product_Version__c_Insert_Webportal_MC_Load group by Registered_Product__c, Environment_Name having count(*) > 1;


select Registered_Product__c, Registered_Product_AccountID, count(*) from System_Product_Version__c_Insert_Webportal_MC_Load group by Registered_Product__c, Registered_Product_AccountID having count(*) > 1;


select * from System_Product_Version__c_Insert_Webportal_MC_Load
where Registered_Product_AccountID = '0010v00000IkJb4AAF'