/*****************************************************************************************************************************************************
--- Create Environments for SPVs that could not find an existing Environment record.
--  Script File Name: 9.2.D Environment__c_Insert_Superoin_Deltas.sql
--- Developed for CentralSquare
--- Developed by Ron Tecson and Marty Corpus
--- Copyright Apps Associates 2019
--- Created Date: April 10, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-04-10 Created.
--- This script will create Environments for SPVs that are being generated for any newly created Registered Products from an Asset Import.
--- This script requires the DBAmp ETL tool for Salesforce.

******************************************************************************************************************************************************/


---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

Use Superion_Production;

Exec SF_Refresh 'RT_SUPERION_PROD', 'Account', 'subset';

---------------------------------------------------------------------------------
-- Drop Environment__c_PreLoad_Non_Zuercher
---------------------------------------------------------------------------------
USE Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_Non_Zuercher') 
DROP TABLE Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher;

select distinct 
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		RegProd_ProductGroup, 
		RegProd_AccountId, 
		SUBSTRING(CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + regprod_productgroup + '-' + a.name,1,80) AS Name,
		RegProd_AccountId as Account__c,
		'Automatically created because no Hardware Software records existed in Tritech source'  as description__c,
              CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production'
                    WHEN EnvironmentType = 'Train' Then 'Training'
                    WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c,
		a.Name as AccountName,
		'Yes' AS Active__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
INTO Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher
FROM Staging_PROD.dbo.System_Product_Version__c_Insert_Superoin_Delta_load s
	JOIN Superion_Production.dbo.ACCOUNT a ON a.id = s.regprod_accountid
WHERE  environment__c = 'ID NOT FOUND'

---------------------------------------------------------------------------------
-- Drop Environment__c_Load_Non_Zuercher_RT
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_Load_Non_Zuercher') 
DROP TABLE Staging_PROD.dbo.Environment__c_Load_Non_Zuercher;

select * INTO Staging_PROD.dbo.Environment__c_Load_Non_Zuercher
from Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher;


EXEC SF_ColCompare 'INSERT','RT_SUPERION_PROD','Environment__c_Load_Non_Zuercher'


EXEC sf_bulkops 'INSERT','RT_SUPERION_PROD','Environment__c_Load_Non_Zuercher'


------------------------------------------------------------------------------------
-- VALIDATION
------------------------------------------------------------------------------------

select error, count(*) from Environment__c_Load_Non_Zuercher
group by error

select LEN(Name), * from Environment__c_Load_Non_Zuercher where error <> 'Operation Successful.'


select * from Environment__c_Load_Non_Zuercher_RT

--*****************************************************************************************************

-------------------------------------------------------------------------------------------------------
-- Refresh Local Database
-------------------------------------------------------------------------------------------------------

USE Superion_Production

EXEC SF_Refresh 'RT_Superion_PROD', 'Environment__c', 'subset';