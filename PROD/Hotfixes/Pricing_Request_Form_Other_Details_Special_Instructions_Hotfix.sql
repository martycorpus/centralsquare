
--Other_Details_Special_Instructions__c Hotfix:

--Drop table Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_Update_4apr_JG;

Select 
 Id=tar.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Record_ID__c_orig=tar.Legacy_Record_ID__c
,Other_Details_Special_Instructions__c_sup=tar.Other_Details_Special_Instructions__c
,Quick_Summary_Basic_Quote__c_tt=tt.Quick_Summary_Basic_Quote__c
,Other_Details_Special_Instructions__c=IIF(tar.Other_Details_Special_Instructions__c IS NULL,tt.Quick_Summary_Basic_Quote__c,CONCAT(tar.Other_Details_Special_Instructions__c,CHAR(13)+CHAR(10),tt.Quick_Summary_Basic_Quote__c))

into Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_Update_4apr_JG

from Superion_Production.dbo.Sales_Request__c tar
inner join Tritech_PROD.dbo.Pricing_Request_Form__c tt
on tar.Legacy_Record_ID__c=tt.ID
where tt.Quick_Summary_Basic_Quote__c IS NOT NULL;--666

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_Update_4apr_JG' 

Select * from Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_Update_4apr_JG
where Other_Details_Special_Instructions__c_sup is null;

/*
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
Salesforce object Sales_Request__c does not contain column Other_Details_Special_Instructions__c_sup
Salesforce object Sales_Request__c does not contain column Quick_Summary_Basic_Quote__c_tt
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','Sales_Request__c_Tritech_SFDC_PricingRequestForm_load_Update_4apr_JG'