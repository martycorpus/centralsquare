/*
use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__ka','yes'
EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__kav','yes'
*/
use Staging_PROD
go 
-- drop table DataCategories_KB_Tritech_SFDC_load_hotfix
select DatacategoryName as datacategoryName_orig , 
DatacategoryName= iif(DatacategoryName='911','z911','zsuite')
,DataCategoryLabel= iif(DatacategoryName='911','z911','zsuite')
,DataCategoryLabel as datacategoryLabel_orig 
,DataCategoryGroupName,Parentid
--into DataCategories_KB_Tritech_SFDC_load_hotfix
from DataCategories_KB_Tritech_SFDC_load  a 
inner join Superion_Production.dbo.Article__kav b 
on a.parentid=b.id
 where datacategoryName in('zuercher_Suite','911')



select *  from DataCategories_KB_Tritech_SFDC_load_hotfix



