SELECT [Id]
            ,DeveloperName
             ,[Name]
             ,[Type]
 FROM [Superion_Production].dbo.[Group]
 WHERE 1=1 -- ID = '00G6A000000P27XUAS' 
 and type='Queue'
;

select a.*
into Superion_Production.dbo.[case_Inactive_owner_bkp_SL_2APR]
 from Superion_Production.dbo.[case] a
,tt_case_inactive_owner_list b
where a.id=b.id
;

select a.id,error=cast(space(255) as nvarchar(255)) ,ownerid='00G6A000000P27XUAS'
,isclosed_sup=a.isclosed,ownerid_sup=a.ownerid
into Case_TT_Inactive_owner_update_hotfix_SL_2APR2019
from Superion_Production.dbo.[case] a
inner join tt_case_inactive_owner_list b
on a.id=b.id
where a.OwnerId='0056A000000lfqkQAA' and migrated_record__c='true';--4470


select * from Case_TT_Inactive_owner_update_hotfix_SL_2APR2019
where isclosed_sup='true';--26


--exec SF_ColCompare 'Update','SL_SUPERION_PROD ', 'Case_TT_Inactive_owner_update_hotfix_SL_2APR2019' 


/*
Salesforce object Case does not contain column isclosed_sup
Salesforce object Case does not contain column ownerid_sup

*/


--Exec SF_BulkOps 'Update:batchsize(50)','SL_SUPERION_PROD ','Case_TT_Inactive_owner_update_hotfix_SL_2APR2019'

select error,count(*) from Case_TT_Inactive_owner_update_hotfix_SL_2APR2019
 group by error;