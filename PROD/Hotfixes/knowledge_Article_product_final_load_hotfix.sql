
use Staging_PROD
go 
-- drop table KnowledgeArticle_Delete_product_related
select a.id as Draftid, a.knowledgearticleid as ID ,a.title ,a.UrlName,a.ArticleCaseAttachCount,a.product__c
into KnowledgeArticle_Delete_product_related 
from Superion_Production.dbo.Article__kav a
inner join KnowledgeArticle_Tritech_SFDC_load b 
on a.UrlName=b.urlname 
where b.product__c like '%zuercher suite%' or
 b.id in(select parentid from DataCategories_KB_Tritech_SFDC_load where DatacategoryName='911')


 select id from KnowledgeArticle_Delete_product_related
 --------------------------------INSERT--------------------------------------------------------

 -- drop table KnowledgeArticle_insert_product_related
select b.[ID]
      ,b.[ERROR]
      ,b.[id_orig]
      ,b.[KnowledgeArticleId_orig]
      ,b.[Title]
      ,b.[Summary]
      ,b.[UrlName]
      ,b.[File__body__S]
      ,b.[File__Name__s]
      ,b.[File__ContentType__S]
      ,b.[Description__c]
      ,b.[ArticleNumber_orig]
      ,b.[Language]
      ,b.[OwnerId_orig]
      ,b.[CreatedById_orig]
      ,b.[CreatedDate_orig]
      ,b.[IsVisibleInApp]
      ,b.[IsVisibleInCsp]
      ,b.[IsVisibleInPkb]
      ,b.[IsVisibleInPrm]
      ,b.[product__c] as old_product__c
	  ,product__c =replace( replace(b.product__c,'zuercher suite','zsuite'),'911;Inform 911','z911;Inform 911')
into KnowledgeArticle_Insert_product_related 
from Superion_Production.dbo.Article__kav a
inner join KnowledgeArticle_Tritech_SFDC_load b 
on a.UrlName=b.urlname 
where b.product__c like '%zuercher suite%' or
 b.id in(select parentid from DataCategories_KB_Tritech_SFDC_load where DatacategoryName='911')

 select * from KnowledgeArticle_Insert_product_related
 -----------------------------------------------------------------------------------------------------------------------

 ---------------------------------- DATA CATEGORY INSERT-----------------------------------------

--use SUPERION_PRODUCTION
--EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__ka','yes'
--EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__kav','yes'


use Staging_PROD 
go
-- drop table DataCategories_KB_Tritech_SFDC_load_product__c_Related_insert
select 
							DatacategoryName		= api.Article_Category_API
							,DataCategoryGroupName	= 'Products'
							,parentid				= b.id
							,DataCategoryLabel		= c.newArticleCategory
					into DataCategories_KB_Tritech_SFDC_load_product__c_Related_insert
from KnowledgeArticle_Insert_product_related a
,Superion_Production.dbo.Article__kav b
,Tritech_KB_Article_Data_Categories_final c
,DataCategory_API_New_Category api
where a.urlname = b.urlname 
and a.ArticleNumber_orig=c.ArticleNumber 
and c.NewArticleCategory = api.Article_Category_Label

--(132 row(s) affected)
select * from DataCategories_KB_Tritech_SFDC_load_product__c_Related_insert 


 select * 
--update a set a.article_category_API='zsuite',a.article_category_label='zsuite'
 from DataCategory_API_New_Category a where article_category_API='Zuercher_Suite'


  select * 
--update a set a.article_category_API='z911',a.article_category_label='z911'
 from DataCategory_API_New_Category a where article_category_API='911'

 select *
 -- update a set a.newarticlecategory='zsuite'
  from Tritech_KB_Article_Data_Categories_final a
  where a.newarticlecategory like 'zuercher suite' 


  select *
 -- update a set a.newarticlecategory='z911'
  from Tritech_KB_Article_Data_Categories_final a
  where a.newarticlecategory ='911' 
 