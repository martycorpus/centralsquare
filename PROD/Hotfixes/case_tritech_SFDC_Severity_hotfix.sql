--Use SUPERION_PRODUCTION
--EXEC SF_Refresh 'SL_SUPERION_PROD ','Case','yes'

/*
a.       Update all migrated records that have blank Severity__c values

b.      If severity field is null and priority field is not null, set the severity field the same as the priority value.

c.       If severity field is null and priority field is null, set the severity field = �4 � Minor�
*/

use Staging_PROD
go 
-- drop table Case_tritech_SFDC_load_severity_hotfix
select id,ERROR	 =   CAST(SPACE(255) as NVARCHAR(255)),
severity__c=iif( a.priority is null,'4 - Minor',a.priority)
,a.severity__c as severity_orig
, a.priority as priorirty_orig
into Case_tritech_SFDC_load_severity_hotfix 
from Superion_Production.dbo.[case] a 
where severity__c is null and Migrated_Record__c='True'


--(250201 row(s) affected)
select * from Case_tritech_SFDC_load_severity_hotfix where priorirty_orig is null


--Exec SF_colcompare 'Update','SL_SUPERION_PROD ','Case_tritech_SFDC_load_severity_hotfix'


--Exec SF_BulkOps 'Update:batchsize(100)','SL_SUPERION_PROD ','Case_tritech_SFDC_load_severity_hotfix'
/*
Salesforce object Case does not contain column severity_orig
Salesforce object Case does not contain column priorirty_orig
*/

select error, count(*) from Case_tritech_SFDC_load_severity_hotfix 
group by error