-- update of product__c records.
/*use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__ka','yes'
EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__kav','yes'
*/

select count(*) from Superion_Production.dbo.Article__kav where Product__c in('911','zuercher Suite')
--126

use Staging_PROD
go 

-- drop table KnowledeArticle_product_field_delete_ids
select id as Draftid, knowledgearticleid as ID ,title ,UrlName,a.ArticleCaseAttachCount
--into KnowledeArticle_product_field_delete_ids

from Superion_Production.dbo.Article__kav a where Product__c like '%zuercher Suite'

select * from KnowledeArticle_product_field_delete_ids
-- drop table KnowledeArticle_product_field_insert_ids
select a.* 
into KnowledeArticle_product_field_insert_ids
from KnowledgeArticle_Tritech_SFDC_load a 
inner join KnowledeArticle_product_field_delete_ids b 
on a.UrlName=b.UrlName
--(126 row(s) affected)
select product__C
--update a set a.product__c='z911' 
from KnowledeArticle_product_field_insert_ids where product__c='911'

select product__C
--update a set a.product__c='zsuite' 
from KnowledeArticle_product_field_insert_ids where product__c='zuercher suite'

select * from KnowledeArticle_product_field_insert_ids


select product__c,* from KnowledgeArticle_Tritech_SFDC_load 
where product__c like '%zuercher suite%' or
 id in(select parentid from DataCategories_KB_Tritech_SFDC_load where DatacategoryName='911')


 -- drop table KnowledeArticle_product_field_delete_ids
select id as Draftid, knowledgearticleid as ID ,title ,UrlName,a.ArticleCaseAttachCount
--into KnowledeArticle_product_field_delete_ids

from Superion_Production.dbo.Article__kav a where Product__c like '%zuercher Suite'


-- drop table KnowledgeArticle_Delete_product_related
select a.id as Draftid, a.knowledgearticleid as ID ,a.title ,a.UrlName,a.ArticleCaseAttachCount,a.product__c
into KnowledgeArticle_Delete_product_related 
from Superion_Production.dbo.Article__kav a
inner join KnowledgeArticle_Tritech_SFDC_load b 
on a.UrlName=b.urlname 
where b.product__c like '%zuercher suite%' or
 b.id in(select parentid from DataCategories_KB_Tritech_SFDC_load where DatacategoryName='911')


select * from DataCategories_KB_Tritech_SFDC_load where DatacategoryName='911'


