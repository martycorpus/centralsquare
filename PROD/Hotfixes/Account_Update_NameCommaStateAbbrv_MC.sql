--2019-03-22 MartyC. 
--hotfix to update the migrated TT account name field to <accountname>, <state abbrv.>


drop table Account_Update_NameCommaStateAbbrv_MC_preload;

SELECT id                     AS Id,
       cast(null as nvarchar(255)) as Error,
	   CASE
         WHEN st.state_code IS NOT NULL THEN Substring(name, 1, Len(name)-3) + ', '
                                             + Right(name, 2)
         ELSE t.name
       END                    AS Name,

       legacysfdcaccountid__c AS TT_Prod_ID,
       t.name                 AS TT_AccountName,
       --entity_name__c,
       CASE
         WHEN st.state_code IS NOT NULL THEN Right( name, 2 )
         ELSE '<no State Code in Name>'
       END                    AS Name_state,
       CASE
         WHEN st.state_code IS NOT NULL THEN Substring(name, 1, Len(name)-3) + ', '
                                             + Right(name, 2)
         ELSE t.name
       END                    AS Hotfix_Name,
       CASE
         WHEN st.state_code IS NOT NULL THEN 'Hotfix to be applied.'
         ELSE 'Exception - TT name does note have StateCode - Name will not be hotfixed'
       END                    AS COMMENT
INTO  Account_Update_NameCommaStateAbbrv_MC_preload
FROM   ACCOUNT_TRITECH_SFDC_LOAD t
       left join ( SELECT state_code
                   FROM   staging_sb.dbo.AA_COUNTRY_STATE_LOOKUP
                   WHERE  country_code = 'US' ) st
              ON st.state_code = Right( t.name, 2 )
WHERE  t.name NOT LIKE '%,%'
ORDER  BY CASE
            WHEN st.state_code IS NOT NULL THEN 'Hotfix'
            ELSE 'Exception - TT name does note have StateCode'
          END ; --(384 row(s) affected)


update Account_Update_NameCommaStateAbbrv_MC_preload
set comment = 'Exception - TT name does note have StateCode - Name will not be hotfixed'
where TT_AccountName in 
(
'Intelutions - Puerto Rico',
'TCS/ ComTech - TeleCommunication Systems Inc',
'AppArmor',
'Agnovi',
'Crimedar',
'Marathon Data Systems',
'Online Business Systems',
'CommonTime',
'ARMS'
)
--(9 row(s) affected)


-- drop table Account_Update_NameCommaStateAbbrv_MC

select * into Account_Update_NameCommaStateAbbrv_MC
from Account_Update_NameCommaStateAbbrv_MC_preload
where comment <> 'Exception - TT name does note have StateCode - Name will not be hotfixed'

select * from Account_Update_NameCommaStateAbbrv_MC;

exec SF_ColCompare 'Update','MC_SUPERION_PROD','Account_Update_NameCommaStateAbbrv_MC'
--- Starting SF_ColCompare V3.6.7
Problems found with Account_Update_NameCommaStateAbbrv_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 68]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object Account does not contain column TT_Prod_ID
Salesforce object Account does not contain column TT_AccountName
Salesforce object Account does not contain column Name_state
Salesforce object Account does not contain column Hotfix_Name
Salesforce object Account does not contain column COMMENT


exec SF_BulkOps 'Update','MC_SUPERION_PROD','Account_Update_NameCommaStateAbbrv_MC'

--- Starting SF_BulkOps for Account_Update_NameCommaStateAbbrv_MC V3.6.7
--21:34:11: Run the DBAmp.exe program.
--21:34:11: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
--21:34:11: Updating Salesforce using Account_Update_NameCommaStateAbbrv_MC (SQL01 / Staging_PROD) .
--21:34:11: DBAmp is using the SQL Native Client.
--21:34:12: SOAP Headers: 
--21:34:12: Warning: Column 'TT_Prod_ID' ignored because it does not exist in the Account object.
--21:34:12: Warning: Column 'TT_AccountName' ignored because it does not exist in the Account object.
--21:34:12: Warning: Column 'Name_state' ignored because it does not exist in the Account object.
--21:34:12: Warning: Column 'Hotfix_Name' ignored because it does not exist in the Account object.
--21:34:12: Warning: Column 'COMMENT' ignored because it does not exist in the Account object.
--21:34:25: 325 rows read from SQL Table.
--21:34:25: 325 rows successfully processed.
--21:34:25: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


select * from Account_Update_NameCommaStateAbbrv_MC


select * from  Account_Update_NameCommaStateAbbrv_MC_preload
where comment = 'Exception - TT name does note have StateCode - Name will not be hotfixed'