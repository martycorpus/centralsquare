Use Superion_Production;

exec SF_Refresh 'MC_SUPERION_PROD','EngineeringIssue__c','Yes';


exec SF_Refresh 'MC_SUPERION_PROD','Registered_product__c','Yes';

exec SF_Refresh 'MC_SUPERION_PROD','EngineeringProjectAreaPathMapping__c','Yes';

Use Staging_PROD;

SELECT ENGAreaPath__c, Support_Account__c, *
FROM Superion_Production.dbo.EngineeringIssue__c  ei --7490
where ei.Migrated_Record__c = 'true'
and ei.Registered_Product__c is null
and ei.Legacy_Source_System__c = 'Tritech'
and ei.ENGAreaPath__c is not null; --7464

/*

drop table EngineeringIssue__c_RP_hotfix_preload
drop table EngineeringIssue__c_RP_hotfix_load

*/


;with cte_AreaPathMapping as
(SELECT Row_number( )
over (
           PARTITION BY maps.AreaPath__c
           ORDER BY Family__c,Group__c,Line__c ) AS "RANK",
 maps.AreaPath__c, maps.Family__c, maps.Group__c, maps.Line__c
FROM (select distinct map.AreaPath__c, map.Family__c, Group__c, map.Line__c from Superion_Production.dbo.EngineeringProjectAreaPathMapping__c map where Family__c = 'Public Safety & Justice') maps)
SELECT EI.id, ENGAreaPath__c, Support_Account__c, cteam.AreaPath__c, cteam.Family__c,cteam.Group__c,cteam.Line__c, Registered_Product__c, cast(null as nvarchar(5)) as Eligible_For_Support__c
into EngineeringIssue__c_RP_hotfix_preload
FROM Superion_Production.dbo.EngineeringIssue__c  ei 
left join cte_AreaPathMapping cteam on cteam.AreaPath__c = case when EI.ENGAreaPath__c not like '%\%' then ENGAreaPath__c
                                                               else SUBSTRING(ei.ENGAreaPath__c,1,(CHARINDEX('\',ENGAreaPath__c,1)-1)) end
where ei.Migrated_Record__c = 'true'
and ei.Registered_Product__c is null
and ei.Legacy_Source_System__c = 'Tritech'
and ei.ENGAreaPath__c is not null; --(13941 row(s) affected) 4/16




alter table EngineeringIssue__c_RP_hotfix_preload add product_line nvarchar(255);

alter table EngineeringIssue__c_RP_hotfix_preload add product_family nvarchar(255);


alter table EngineeringIssue__c_RP_hotfix_preload add product_group nvarchar(255);




update t1
set Registered_Product__c = rp.Id, product_family =  p2.Family , product_line = p2.Product_Line__c, product_group = p2.Product_Group__c, Eligible_For_Support__c = p2.Eligible_For_Support__c
from EngineeringIssue__c_RP_hotfix_preload t1
join Superion_Production.dbo.Registered_Product__c rp on rp.Account__c = t1.Support_Account__c
join Superion_Production.dbo.Product2 p2 on p2.id = rp.Product__c and 
                                     coalesce(t1.family__c,'x') = coalesce(P2.Family,'x') and 
									 coalesce(t1.group__c,'x') = coalesce(p2.Product_Group__c,'x') 
									 and coalesce(t1.line__c,'x') = coalesce( p2.Product_Line__c,'x'); --(8163 row(s) affected)

									 

select id, cast(null as nvarchar(255)) as error, Registered_Product__c, ENGAreaPath__c	as ENGAreaPath,AreaPath__c	,Family__c	,Group__c	,Line__c,Eligible_For_Support__c
into EngineeringIssue__c_RP_hotfix_load
from(
select 
Row_number( )
over (
       
	    PARTITION BY  id
		order by Registered_Product__c desc, AreaPath__c,	Family__c,	Group__c,	Line__c) as "Rank",
id,	ENGAreaPath__c, Registered_Product__c, AreaPath__c,	Family__c,	Group__c,	Line__c, Eligible_For_Support__c
from  EngineeringIssue__c_RP_hotfix_preload
) l
where l.Rank = 1 and l.Registered_Product__c is not null
and Eligible_For_Support__c <> 'false'

--(4566 row(s) affected)


select * from EngineeringIssue__c_RP_hotfix_load

exec SF_ColCompare 'Update','MC_Superion_Prod','EngineeringIssue__c_RP_hotfix_load'

----- Starting SF_ColCompare V3.7.7
--Problems found with EngineeringIssue__c_RP_hotfix_load. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 87]
----- Ending SF_ColCompare. Operation FAILED.

--ErrorDesc
--Salesforce object EngineeringIssue__c does not contain column ENGAreaPath
--Salesforce object EngineeringIssue__c does not contain column AreaPath__c
--Salesforce object EngineeringIssue__c does not contain column Family__c
--Salesforce object EngineeringIssue__c does not contain column Group__c
--Salesforce object EngineeringIssue__c does not contain column Line__c
--Salesforce object EngineeringIssue__c does not contain column Eligible_For_Support__c

select * from EngineeringIssue__c_RP_hotfix_load

--  exec SF_Bulkops 'Update','MC_Superion_Prod','EngineeringIssue__c_RP_hotfix_load'


--- Starting SF_BulkOps for EngineeringIssue__c_RP_hotfix_load V3.7.7
--16:19:29: Run the DBAmp.exe program.
--16:19:29: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
--16:19:29: Updating Salesforce using EngineeringIssue__c_RP_hotfix_load (SQL01 / Staging_PROD) .
--16:19:30: DBAmp is using the SQL Native Client.
--16:19:30: SOAP Headers: 
--16:19:30: Warning: Column 'ENGAreaPath' ignored because it does not exist in the EngineeringIssue__c object.
--16:19:30: Warning: Column 'AreaPath__c' ignored because it does not exist in the EngineeringIssue__c object.
--16:19:30: Warning: Column 'Family__c' ignored because it does not exist in the EngineeringIssue__c object.
--16:19:30: Warning: Column 'Group__c' ignored because it does not exist in the EngineeringIssue__c object.
--16:19:30: Warning: Column 'Line__c' ignored because it does not exist in the EngineeringIssue__c object.
--16:19:30: Warning: Column 'Eligible_For_Support__c' ignored because it does not exist in the EngineeringIssue__c object.
--16:20:15: 4566 rows read from SQL Table.
--16:20:15: 4566 rows successfully processed.
--16:20:15: Percent Failed = 0.000.
----- Ending SF_BulkOps. Operation successful.



/*									 

select * from Superion_Production.dbo.product2

select distinct * from EngineeringIssue__c_RP_hotfix_preload
where areapath__c is not null

select  * from EngineeringIssue__c_RP_hotfix_preload
where registered_product__c is not null
order by id


select 
Row_number( )
over (
       
	    PARTITION BY  id
		order by Registered_Product__c desc, AreaPath__c,	Family__c,	Group__c,	Line__c) as "Rank",
id,	ENGAreaPath__c, Registered_Product__c, AreaPath__c,	Family__c,	Group__c,	Line__c
into 
from  EngineeringIssue__c_RP_hotfix_preload

select 
id,	ENGAreaPath__c, Registered_Product__c, AreaPath__c,	Family__c,	Group__c,	Line__c
from from EngineeringIssue__c_RP_hotfix_preload

id	ENGAreaPath__c
a932G000000TQaCQAW	Administrative
select distinct Family__c from EngineeringIssue__c_RP_hotfix_preload
where areapath__c is not null


select distinct Family__c from EngineeringIssue__c_RP_hotfix_preload
select Family, Product_Group__c, Product_Line__c, id, name from Superion_Production.dbo.Product2 where Family = 'Public Safety & Justice'

*/