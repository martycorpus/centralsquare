/*
FIX postal codes that did not have leading zero's


*/

/* ----  EXECUTION ----
use staging_prod;

BILL-TOs:

EXEC SF_COLCOMPARE 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_B2s'

EXEC SF_BULKOPS 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_B2s'



SHIPTO SPLITS:

EXEC SF_COLCOMPARE 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_S2s'

EXEC SF_BULKOPS 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_S2s'



*/




---------RERESH-----------------------
/*

USE Superion_Production;
exec SF_Refresh 'MC_SUPERION_PROD','Bill_To_Ship_To__c';

--- Starting SF_Refresh for Bill_To_Ship_To__c V3.6.7
06:08:50: Using last run time of 2019-04-03 16:25:00
06:08:51: Identified 896 updated/inserted rows.
06:08:51: Identified 2 deleted rows.
06:08:51: Removing deleted rows from Bill_To_Ship_To__c
06:08:51: Adding updated/inserted rows into Bill_To_Ship_To__c
--- Ending SF_Refresh. Operation successful.

select * into Bill_To_Ship_To__c_bkp_4apr2019_SL
from Bill_To_Ship_To__c;--53384

*/

--FIX BILLTO RECORDS
Use Staging_PROD;

--drop table Bill_to_Ship_to__c_update_fix_zip_codes_B2s;--1088

SELECT t1.id,
       cast(null as nvarchar(255)) as Error,
       --t1.error as error_orig,
	   a.name as Parent_Account_Name_Sup,
       '0' + t1.zip_postal_code__c AS zip_postal_code__c,--fix leading zeros issue
       t1.zip_postal_code__c       AS zip_postal_code_orig,
       t1.account__c               AS Account_orig,
       t1.legacy_address_id__c     AS legacy_address_id_orig,
       t1.address_code__c          AS address_code_orig,
       t2.name                     AS name_orig,
       t1.primary__c               AS primary_orig,
       t1.bill_to__c               AS bill_to_orig,
       t1.ship_to__c               AS ship_to_orig,
       t2.createddate              AS createddate_orig,
	   '0' + cast(t2.name - 1 as nvarchar(10))  as name_min1_shiptoname
INTO   Bill_to_Ship_to__c_update_fix_zip_codes_B2s
FROM   BILL_TO_SHIP_TO__C_TRITECH_EXCEL_LOAD t1
       join superion_production.dbo.BILL_TO_SHIP_TO__C t2
         ON t2.id = t1.id
       join superion_production.dbo.ACCOUNT a
         ON a.id = t1.account__c
WHERE  t1.error = 'Operation Successful.' AND
       Len( t2.zip_postal_code__c ) = 4 
--AND t2.name = '057926'


select * from Bill_to_Ship_to__c_update_fix_zip_codes_B2s;--1088

BILL-TOs:

--EXEC SF_COLCOMPARE 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_B2s'

/*
Salesforce object Bill_to_Ship_to__c does not contain column Parent_Account_Name_Sup
Salesforce object Bill_to_Ship_to__c does not contain column zip_postal_code_orig
Salesforce object Bill_to_Ship_to__c does not contain column Account_orig
Salesforce object Bill_to_Ship_to__c does not contain column legacy_address_id_orig
Salesforce object Bill_to_Ship_to__c does not contain column address_code_orig
Salesforce object Bill_to_Ship_to__c does not contain column name_orig
Salesforce object Bill_to_Ship_to__c does not contain column primary_orig
Salesforce object Bill_to_Ship_to__c does not contain column bill_to_orig
Salesforce object Bill_to_Ship_to__c does not contain column ship_to_orig
Salesforce object Bill_to_Ship_to__c does not contain column createddate_orig
Salesforce object Bill_to_Ship_to__c does not contain column name_min1_shiptoname


*/

--EXEC SF_BULKOPS 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_B2s'

/*
--- Starting SF_BulkOps for Bill_to_Ship_to__c_update_fix_zip_codes_B2s V3.7.7
06:15:19: Run the DBAmp.exe program.
06:15:19: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
06:15:19: Updating Salesforce using Bill_to_Ship_to__c_update_fix_zip_codes_B2s (SQL01 / Staging_PROD) .
06:15:20: DBAmp is using the SQL Native Client.
06:15:21: SOAP Headers: 
06:15:21: Warning: Column 'Parent_Account_Name_Sup' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'zip_postal_code_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'Account_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'legacy_address_id_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'address_code_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'name_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'primary_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'bill_to_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'ship_to_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'createddate_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:15:21: Warning: Column 'name_min1_shiptoname' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:16:01: 1088 rows read from SQL Table.
06:16:01: 1088 rows successfully processed.
06:16:01: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.
*/

--DROP TABle Bill_to_Ship_to__c_update_fix_zip_codes_S2s

--FIX SHIPTO RECORDS
select t2.id,
       cast(null as nvarchar(255)) as Error,
       '0' + t2.zip_postal_code__c AS zip_postal_code__c,--fix leading zeros issue
	   t2.zip_postal_code__c as zip_postal_code_orig_bad_needs_fixin,
	   t2.name as b2s2_name,
	   t1.Parent_Account_Name_Sup
INTO Bill_to_Ship_to__c_update_fix_zip_codes_S2s
from Bill_to_Ship_to__c_update_fix_zip_codes_B2s t1
join superion_production.dbo.BILL_TO_SHIP_TO__C t2
         ON 		 t1.Account_orig = t2.Account__c 
		 and Primary__c = 'true' and Ship_To__c = 'true' and Bill_To__c = 'false'
		-- and t1.address_code_orig = t2.Address_Code__c
WHERe  len(t2.Zip_Postal_Code__c) = 4
		 and t2.id not in (select id from Bill_to_Ship_to__c_update_fix_zip_codes_B2s)
		 ;--1024

SELECT * FROM Bill_to_Ship_to__c_update_fix_zip_codes_S2s;       



SHIPTO SPLITS:

--EXEC SF_COLCOMPARE 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_S2s'

/*
Salesforce object Bill_to_Ship_to__c does not contain column zip_postal_code_orig_bad_needs_fixin
Salesforce object Bill_to_Ship_to__c does not contain column b2s2_name
Salesforce object Bill_to_Ship_to__c does not contain column Parent_Account_Name_Sup

*/

--EXEC SF_BULKOPS 'UPDATE','MC_SUPERION_PROD','Bill_to_Ship_to__c_update_fix_zip_codes_S2s'

/*
--- Starting SF_BulkOps for Bill_to_Ship_to__c_update_fix_zip_codes_S2s V3.7.7
06:18:23: Run the DBAmp.exe program.
06:18:23: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
06:18:23: Updating Salesforce using Bill_to_Ship_to__c_update_fix_zip_codes_S2s (SQL01 / Staging_PROD) .
06:18:23: DBAmp is using the SQL Native Client.
06:18:23: SOAP Headers: 
06:18:24: Warning: Column 'zip_postal_code_orig_bad_needs_fixin' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:18:24: Warning: Column 'b2s2_name' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:18:24: Warning: Column 'Parent_Account_Name_Sup' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:19:09: 1024 rows read from SQL Table.
06:19:09: 13 rows failed. See Error column of row for more information.
06:19:09: 1011 rows successfully processed.
06:19:09: Errors occurred. See Error column of row for more information.
06:19:09: Percent Failed = 1.300.
06:19:09: Error: DBAmp.exe was unsuccessful.
06:19:09: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe update Bill_to_Ship_to__c_update_fix_zip_codes_S2s "SQL01"  "Staging_PROD"  "MC_SUPERION_PROD"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 162]
SF_BulkOps Error: 06:18:23: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC06:18:23: Updating Salesforce using Bill_to_Ship_to__c_update_fix_zip_codes_S2s (SQL01 / Staging_PROD) .06:18:23: DBAmp is using the SQL Native Client.06:18:23: SOAP Headers: 06:18:24: Warning: Column 'zip_postal_code_orig_bad_needs_fixin' ignored because it does not exist in the Bill_To_Ship_To__c object.06:18:24: Warning: Column 'b2s2_name' ignored because it does not exist in the Bill_To_Ship_To__c object.06:18:24: Warning: Column 'Parent_Account_Name_Sup' ignored because it does not exist in the Bill_To_Ship_To__c object.06:19:09: 1024 rows read from SQL Table.06:19:09: 13 rows failed. See Error column of row for more information.06:19:09: 1011 rows successfully processed.06:19:09: Errors occurred. See Error column of row for more information.


*/

select distinct * from Bill_to_Ship_to__c_update_fix_zip_codes_S2s
where error<>'Operation Successful.'