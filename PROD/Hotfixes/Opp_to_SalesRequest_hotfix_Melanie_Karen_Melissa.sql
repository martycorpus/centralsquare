select * from sys.tables
where name like 'Procure%'
order by create_date desc

SELECT Cast( NULL AS NCHAR(18))     AS Id,
       Cast( NULL AS NVARCHAR(255)) AS Error,
       o.id                         AS OpportunityID_Superion_Orig,
	   '0122G000000zx4TQAQ'         as recordtypeid, --General_Request
       o.foia_req_d__c              AS Date_of_Request__c,
       o.foia_req_date__c           AS FOIA_Sent_to_Agency__c,
       o.foia_rec_d__c              AS FOIA_Request_Acknowledged__c,
       o.foia_rec_date__c           AS Date_Completed__c,
       o.foia_req_d__c              AS Opportunity_FOIA_req_d_c,
       foia_req_date__c             AS Opportunity_FOIA_Req_Date_c,
       foia_rec_d__c                AS Opportunity_FOIA_Rec_d_c,
       foia_rec_date__c             AS Opportunity_FOIA_Rec_Date_c
into 
FROM   superion_production.dbo.OPPORTUNITY o
       JOIN Procurement_Activity__c_Opportunity_Tritech_SFDC_load pa
         ON pa.opportunity__c = o.id 


