/****** Script for SelectTopNRows command from SSMS  ******/

--drop table [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar]
SELECT TOP (1000) a.[ID]
      ,[ERROR]
      ,[LegacySFDCAccountId__c_old]=[LegacySFDCAccountId__c]
      ,[LegacySFDCAccountId__c]=[LegacySFDCAccountId__c_Sup]
      ,[Entity_Name__c_sup]
      ,[Entity_Name__c_tt]
	  ,b.Entity_Name__c Entity_Name__c_new
      ,[PSAP_Type__c_sup]
      ,[PSAP__c_sup]
      ,[PSAP_Type__c]=[PSAP_Type__c_sup]
      ,[PSAP__c]=[PSAP__c_sup]
      ,[Sales_Team__c_tt]
      ,[CurrentCADSWVendor__c_tt]
      ,[CurrentCADSWVendor__c_sup]
      ,[CurrentCADSWVendor__c]=[CurrentCADSWVendor__c_sup]
      ,[Client__c_superion]
      ,[Client__c]=[Client__c_superion]
      ,[RecordTypeId_sup]
      ,[RecordTypeId]=[RecordTypeId_sup]
      ,[County_c_sup]
      ,[County__c]=[County_c_sup]
      ,[Ship_To_County__c_sup]
      ,[Ship_To_County__c]=[Ship_To_County__c_sup]
      ,[CreatedDate_sup]
      ,[CreatedDate_tt]
      ,[Fax_sup]
      ,[Fax]=[Fax_sup]
      ,[NumberofBeds__c_sup]
      ,[NumberofBeds__c]=[NumberofBeds__c_sup]
      ,[Last_SW_Purchase_Date_JMS__c_sup]
      ,[Last_SW_Purchase_Date_JMS__c]=[Last_SW_Purchase_Date_JMS__c_sup]
      ,[Current_Justice_SW_Vendor__c_sup]
      ,[Current_Justice_SW_Vendor__c]=[Current_Justice_SW_Vendor__c_sup]
      ,[NumberofMobileUnits__c_sup]
      ,[NumberofMobileUnits__c]=[NumberofMobileUnits__c_sup]
      ,[Mailing_Billing_City_WMP__c_btst]
      ,[Mailing_Billing_State_WMP__c_btst]
      ,[Mailing_Billing_Street_WMP__c_btst]
      ,[Mailing_Billing_Zip_Postal_Code_WMP__c_btst]
      ,[Mailing_Billing_Country_WMP__c_btst]
      ,[Current_Mobile_SW_Vendor__c_sup]
      ,[Current_Mobile_SW_Vendor__c]=[Current_Mobile_SW_Vendor__c_sup]
      ,[Name_sup]
     -- ,[Name]=[Name_sup]
	  ,b.name as name_new
      ,[NumberOfEmployees_sup]
      ,[NumberOfEmployees]=[NumberOfEmployees_sup]
      ,[OwnerId_sup]
      ,[OwnerId]=[OwnerId_sup]
      ,[ParentId_orig]
      ,[Planning_to_replace_911_provider__c_sup]
      ,[Planning_to_replace_911_provider__c]=[Planning_to_replace_911_provider__c_sup]
      ,[Planning_to_replace_CAD_provider__c_sup]
      ,[Planning_to_replace_CAD_provider__c]=[Planning_to_replace_CAD_provider__c_sup]
      ,[Planning_to_replace_Jail_provider__c_sup]
      ,[Planning_to_replace_Jail_provider__c]=[Planning_to_replace_Jail_provider__c_sup]
      ,[Planning_to_replace_RMS_provider__c_sup]
      ,[Planning_to_replace_RMS_provider__c]=[Planning_to_replace_RMS_provider__c_sup]
      ,[Phone_sup]
      ,[Phone]=[Phone_sup]
      ,[Population__c_leg]
      ,[Population__c_sup]
      ,[Sensitive_Account_WMP__c_legacy]
      ,[Sensitive_Account_WMP__c_tar]
      ,[Account_Notes__c]=[Sensitive_Account_WMP__c_tar]
      ,[PS_Agency_Type__c_sup]
      ,[PS_Agency_Type__c_tt]
      ,[PS_Agency_Type__c]=[PS_Agency_Type__c_sup]
      ,[Shipping_City_WMP__c_btst]
      ,[Shipping_State_WMP__c_btst]
      ,[Shipping_Street_WMP__c_btst]
      ,[Shipping_Zip_Postal_Code__c_btst]
      ,[Time_Zone__c_sup]
      ,[Time_Zone__c_tt]
      ,[Website_sup]
      ,[Website_tt]
      ,[AccountSource_sup]
      ,[AccountSource_tt]
      ,[Legacy_TT_Client_Product_Families__c_sup]
      ,[Legacy_TT_Client_Product_Families__c]=[Legacy_TT_Client_Product_Families__c_sup]
      ,[Legacy_TT_Client_Product_Groups__c_sup]
      ,[Legacy_TT_Client_Product_Groups__c]=[Legacy_TT_Client_Product_Groups__c_sup]
      ,[Legacy_TT_Customer_Number__c_sup]
      ,[Legacy_TT_Customer_Number__c]=[Legacy_TT_Customer_Number__c_sup]
      ,[Last_SW_Purchase_Date_ePCR__c_sup]
      ,[Last_SW_Purchase_Date_ePCR__c]=[Last_SW_Purchase_Date_ePCR__c_sup]
      ,[ORI__c_sup]
      ,[ORI__c]=[ORI__c_sup]
      ,[Last_SW_Purchase_Date_Fire_RMS__c_sup]
      ,[Last_SW_Purchase_Date_Fire_RMS__c]=[Last_SW_Purchase_Date_Fire_RMS__c_sup]
      ,[Legacy_TT_Inactive_Contract_Types__c_sup]
      ,[Legacy_TT_Inactive_Contract_Types__c]=[Legacy_TT_Inactive_Contract_Types__c_sup]
      ,[NumberofCallsforService__c_sup]
      ,[NumberofCallsforService__c]=[NumberofCallsforService__c_sup]
      ,[BillingCountry_sup]
      ,[BillingCountry]=[BillingCountry_sup]
      ,[Last_SW_Purchase_Date_Mapping__c_sup]
      ,[Last_SW_Purchase_Date_Mapping__c]=[Last_SW_Purchase_Date_Mapping__c_sup]
      ,[Last_SW_Purchase_Date_Mobile__c_sup]
      ,[Last_SW_Purchase_Date_Mobile__c]=[Last_SW_Purchase_Date_Mobile__c_sup]
      ,[Legacy_TT_Parent_Active_Client_Product_F__c_sup]
      ,[Legacy_TT_Parent_Active_Client_Product_F__c]=[Legacy_TT_Parent_Active_Client_Product_F__c_sup]
      ,[Legacy_TT_Parent_Active_Contract_Types__c_sup]
      ,[Legacy_TT_Parent_Active_Contract_Types__c]=[Legacy_TT_Parent_Active_Contract_Types__c_sup]
      ,[Last_SW_Purchase_Date_Law_RMS__c_sup]
      ,[Last_SW_Purchase_Date_Law_RMS__c]=[Last_SW_Purchase_Date_Law_RMS__c_sup]
      ,[ShippingCountry_sup]
      ,[ShippingCountry]=[ShippingCountry_sup]
      ,[User_Conference_Attendee__c_sup]
      ,[User_Conference_Attendee__c]=[User_Conference_Attendee__c_sup]
      ,[Legacy_TT_Data_Conversion_Analyst__c_sup]
      ,[Legacy_TT_Data_Conversion_Analyst__c]=[Legacy_TT_Data_Conversion_Analyst__c_sup]
      ,[Legacy_TT_GIS_Analyst__c_sup]
      ,[Legacy_TT_GIS_Analyst__c]=[Legacy_TT_GIS_Analyst__c_sup]
      ,[TT_Account_At_Risk__c_sup]
      ,[TT_Account_At_Risk__c]=[TT_Account_At_Risk__c_sup]
      ,[TT_Account_Status_Comments__c_sup]
      ,[TT_Account_Status_Comments__c]=[TT_Account_Status_Comments__c_sup]
      ,[TT_Account_Status_Issues_Problems__c_sup]
      ,[TT_Account_Status_Issues_Problems__c]=[TT_Account_Status_Issues_Problems__c_sup]
      ,[Wellness_Check_Contact_Goal__c_sup]
      ,[Wellness_Check_Contact_Goal__c]=[Wellness_Check_Contact_Goal__c_sup]
      ,[Account_Renewal_Confidence__c_sup]
      ,[Account_Renewal_Confidence__c]=[Account_Renewal_Confidence__c_sup]
      ,[Account_Renewal_Notes__c_sup]
      ,[Account_Renewal_Notes__c]=[Account_Renewal_Notes__c_sup]
      ,[Change_in_Leadership__c_sup]
      ,[Change_in_Leadership__c]=[Change_in_Leadership__c_sup]
      ,[Vocal_with_Issues__c_sup]
      ,[Vocal_with_Issues__c]=[Vocal_with_Issues__c_sup]
      ,[of_firefighters_at_largest_PSAP_agency__c_sup]
      ,[of_firefighters_at_largest_PSAP_agency__c]=[of_firefighters_at_largest_PSAP_agency__c_sup]
      ,[of_Fire_Stations__c_sup]
      ,[of_Fire_Stations__c]=[of_Fire_Stations__c_sup]
      ,[of_Jail_Users__c_sup]
      ,[of_Jail_Users__c]=[of_Jail_Users__c_sup]
      ,[Number_of_Sworn_Personnel_WMP__c_sup]
      ,[Number_of_Sworn_Personnel_WMP__c]=[Number_of_Sworn_Personnel_WMP__c_sup]
      ,[X3rd_Party_Type__c_sup]
      ,[X3rd_Party_Type__c]=[X3rd_Party_Type__c_sup]
      ,[X911_Call_Taker_Positions__c_sup]
      ,[X911_Call_Taker_Positions__c]=[X911_Call_Taker_Positions__c_sup]
      ,[X911_Current_SW_Vendor__c_sup]
      ,[X911_Current_SW_Vendor__c]=[X911_Current_SW_Vendor__c_sup]
      ,[X911_Parent_Account__c_sup]
      ,[X911_Parent_Account__c_tt_orig]
      ,[X911_Parent_Account__c]=[X911_Parent_Account__c_sup]
      ,[X911_Parent_Total_Sworn_Officers__c_sup]
      ,[X911_Parent_Total_Sworn_Officers__c]=[X911_Parent_Total_Sworn_Officers__c_sup]
      ,[Actions_Taken_Required__c_sup]
      ,[Actions_Taken_Required__c]=[Actions_Taken_Required__c_sup]
      ,[Advertise_Relationship__c_sup]
      ,[Advertise_Relationship__c]=[Advertise_Relationship__c_sup]
      ,[Agreement_Effective_Date__c_sup]
      ,[Agreement_Effective_Date__c]=[Agreement_Effective_Date__c_sup]
      ,[Agreement_Status__c_sup]
      ,[Agreement_Status__c]=[Agreement_Status__c_sup]
      ,[Annual_Calls_for_Service_Received__c_sup]
      ,[Annual_Calls_for_Service_Received__c]=[Annual_Calls_for_Service_Received__c_sup]
      ,[Average_Inmate_Daily_Population__c_sup]
      ,[Average_Inmate_Daily_Population__c]=[Average_Inmate_Daily_Population__c_sup]
      ,[Billing_Current_SW_Vendor__c_sup]
      ,[Billing_Current_SW_Vendor__c]=[Billing_Current_SW_Vendor__c_sup]
      ,[Bomgar_Approved_Client__c_sup]
      ,[Bomgar_Approved_Client__c]=[Bomgar_Approved_Client__c_sup]
      ,[Last_SW_Purchase_Date_CAD__c_sup]
      ,[Last_SW_Purchase_Date_CAD__c_tt]
      ,[Last_SW_Purchase_Date_CAD__c]=[Last_SW_Purchase_Date_CAD__c_sup]
      ,[CAD_Dispatcher_Call_Taker_Seats__c_sup]
      ,[CAD_Dispatcher_Call_Taker_Seats__c]=[CAD_Dispatcher_Call_Taker_Seats__c_sup]
      ,[CAD_Parent_Fire_EMS_Users__c_sup]
      ,[CAD_Parent_Fire_EMS_Users__c]=[CAD_Parent_Fire_EMS_Users__c_sup]
      ,[CAD_Parent_Total_Sworn_Officers__c_sup]
      ,[CAD_Parent_Total_Sworn_Officers__c]=[CAD_Parent_Total_Sworn_Officers__c_sup]
      ,[Completed_Yearly_Wellness_Checks__c_sup]
      ,[Completed_Yearly_Wellness_Checks__c]=[Completed_Yearly_Wellness_Checks__c_sup]
      ,[ePCR_Current_SW_Vendor__c_sup]
      ,[ePCR_Current_SW_Vendor__c]=[ePCR_Current_SW_Vendor__c_sup]
      ,[FBR_Current_SW_Vendor__c_sup]
      ,[FBR_Current_SW_Vendor__c]=[FBR_Current_SW_Vendor__c_sup]
      ,[Fire_Career__c_sup]
      ,[Fire_Career__c]=[Fire_Career__c_sup]
      ,[Fire_Civilian_Personnel__c_sup]
      ,[Fire_Civilian_Personnel__c]=[Fire_Civilian_Personnel__c_sup]
      ,[Fire_Mobile_Units__c_sup]
      ,[Fire_Mobile_Units__c]=[Fire_Mobile_Units__c_sup]
      ,[Fire_RMS_Current_SW_Vendor__c_sup]
      ,[Fire_RMS_Current_SW_Vendor__c]=[Fire_RMS_Current_SW_Vendor__c_sup]
      ,[Fire_RMS_Users__c_sup]
      ,[Fire_RMS_Users__c]=[Fire_RMS_Users__c_sup]
      ,[Fire_Volunteer_Firefighting__c_sup]
      ,[Fire_Volunteer_Firefighting__c]=[Fire_Volunteer_Firefighting__c_sup]
      ,[Fire_Volunteer_Non_Firefighting__c_sup]
      ,[Fire_Volunteer_Non_Firefighting__c]=[Fire_Volunteer_Non_Firefighting__c_sup]
      ,[Install_Name__c_sup]
      ,[Install_Name__c]=[Install_Name__c_sup]
      ,[Last_Modified_Actions__c_sup]
      ,[Last_Modified_Actions__c]=[Last_Modified_Actions__c_sup]
      ,[Last_Modified_Comments__c_sup]
      ,[Last_Modified_Comments__c]=[Last_Modified_Comments__c_sup]
      ,[Last_Modified_Issues__c_sup]
      ,[Last_Modified_Issues__c]=[Last_Modified_Issues__c_sup]
      ,[Last_SW_Purchase_Date_911__c_sup]
      ,[Last_SW_Purchase_Date_911__c]=[Last_SW_Purchase_Date_911__c_sup]
      ,[Last_SW_Purchase_Date_Billing__c_sup]
      ,[Last_SW_Purchase_Date_Billing__c]=[Last_SW_Purchase_Date_Billing__c_sup]
      ,[Last_SW_Purchase_Date_FBR__c_sup]
      ,[Last_SW_Purchase_Date_FBR__c]=[Last_SW_Purchase_Date_FBR__c_sup]
      ,[Last_Wellness_Check_Date__c_sup]
      ,[Last_Wellness_Check_Date__c]=[Last_Wellness_Check_Date__c_sup]
      ,[Last_Wellness_Check_Type__c_sup]
      ,[Last_Wellness_Check_Type__c]=[Last_Wellness_Check_Type__c_sup]
      ,[Law_Civilian_Personnel__c_sup]
      ,[Law_Civilian_Personnel__c]=[Law_Civilian_Personnel__c_sup]
      ,[Law_RMS_Current_SW_Vendor__c_Sup]
      ,[Law_RMS_Current_SW_Vendor__c]=[Law_RMS_Current_SW_Vendor__c_Sup]
      ,[Law_RMS_Users__c_sup]
      ,[Law_RMS_Users__c]=[Law_RMS_Users__c_sup]
      ,[Legacy_TT_Active_Contract_Types__c_sup]
      ,[Legacy_TT_Active_Contract_Types__c]=[Legacy_TT_Active_Contract_Types__c_sup]
      ,[Low_Agency_Ownership_User_Adoption__c_sup]
      ,[Low_Agency_Ownership_User_Adoption__c]=[Low_Agency_Ownership_User_Adoption__c_sup]
      ,[Maintenance_Lapse__c_sup]
      ,[Maintenance_Lapse__c]=[Maintenance_Lapse__c_sup]
      ,[Mapping_Current_SW_Vendor__c_sup]
      ,[Mapping_Current_SW_Vendor__c]=[Mapping_Current_SW_Vendor__c_sup]
      ,[NDA_Expiration__c_sup]
      ,[NDA_Expiration__c]=[NDA_Expiration__c_sup]
      ,[NDA_in_Place__c_sup]
      ,[NDA_in_Place__c]=[NDA_in_Place__c_sup]
      ,[Next_Renewal_Date__c_sup]
      ,[Next_Renewal_Date__c]=[Next_Renewal_Date__c_sup]
      ,[No_Communication_From_Customer__c_sup]
      ,[No_Communication_From_Customer__c]=[No_Communication_From_Customer__c_sup]
      ,[Not_a_Full_Suite_Customer__c_sup]
      ,[Not_a_Full_Suite_Customer__c]=[Not_a_Full_Suite_Customer__c_sup]
      ,[Parent_Info_Not_Validated__c_sup]
      ,[Parent_Info_Not_Validated__c]=[Parent_Info_Not_Validated__c_sup]
      ,[Reference_List_Use__c_sup]
      ,[Reference_List_Use__c]=[Reference_List_Use__c_sup]
      ,[Renewal_Expected_Next_Year__c_sup]
      ,[Renewal_Expected_Next_Year__c]=[Renewal_Expected_Next_Year__c_sup]
      ,[RMS_Parent_Account__c_sup]
      ,[RMS_Parent_Account__c_tt_orig]
      ,[RMS_Parent_Account__c]=[RMS_Parent_Account__c_sup]
      ,[RMS_Parent_Total_Sworn_Officers__c_sup]
      ,[RMS_Parent_Total_Sworn_Officers__c]=[RMS_Parent_Total_Sworn_Officers__c_sup]
      ,[Sales_Tax_ID__c_sup]
      ,[Sales_Tax_ID__c]=[Sales_Tax_ID__c_sup]
      ,[Sales_Tax_Rate__c_sup]
      ,[Sales_Tax_Rate__c]=[Sales_Tax_Rate__c_sup]
      ,[Total_Maintenance_Amount__c_sup]
      ,[Total_Maintenance_Amount__c]=[Total_Maintenance_Amount__c_sup]
      ,[TT_Account_Status__c_sup]
      ,[TT_Account_Status__c]=[TT_Account_Status__c_sup]
      ,[TT_Legacy_Description__c_sup]
      ,[TT_Legacy_Description__c]=[TT_Legacy_Description__c_sup]
      ,[ParentId_sup]
      ,[ParentId]=[ParentId_sup]
      ,[Fire_Paid_Per_Call__c_sup]
      ,[Fire_Paid_Per_Call__c]=[Fire_Paid_Per_Call__c_sup]
      ,[Secure_Folder_ID__c_sup]
      ,[Secure_Folder_ID__c_leg]
      ,[Secure_Folder_ID__c]=[Secure_Folder_ID__c_sup]
      ,[Sensitive_Account_WMP__C_leg]
      ,[Sensitive_Account_WMP__C_sup]
      ,[SupportNotes__c]=[Sensitive_Account_WMP__C_sup]
      ,[Client__c_sup]
      ,[Client__c_leg]
      ,[AAM_PSJ__c_leg]
      ,[AAM_PSJ__c_sup]
      ,[Customer_Success_Liaison__c_leg]
      ,[Customer_Success_Liaison__c_tar]
      ,[AAM_PSJ__c]=[Customer_Success_Liaison__c_tar]
      ,[PSJ_Territory__c_sup]
      ,[PSJ_Territory__c_leg]
      ,[PSJ_Territory__c]=[PSJ_Territory__c_sup]
      ,[Account_Executive_Install_PSJ__c_sup]
      ,[Account_Executive_Install_PSJ__c_leg]
      ,[Account_Executive_Install_PSJ__c]=[Account_Executive_Install_PSJ__c_sup]
      ,[TTOwnerIsActive]
      ,[Account_Executive_del_del__c_sup]
      ,[OwnerID_leg]
      ,[Account_Executive_del_del__c]=[Account_Executive_del_del__c_sup]
      ,[IQ_Account_Owner__c_leg]
	  ,[AE_IQ__c_old]=[AE_IQ__c]
      ,[AE_IQ__c]=null
      ,[X911_Account_Owner__c_leg]
      ,[AE_911__c]=null
      ,[Type_sup]
      ,[Type]=[Type_sup]
	 	  into [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar]
  FROM [Staging_PROD].[dbo].[Account_Tritech_SFDC_UpdateMergingAccounts] a
  ,(select id,entity_name__C,name from Superion_Production.dbo.Account) b
    where a.id in ('0016A00000AMqr8QAD','0016A00000AMqulQAD','0016A00000AMqx8QAD','0016A00000AMqkrQAD')
	and a.id=b.id
 
  ;--User_Conference_Attendee__c
NULL

  select * from [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar];


   --Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar' 

   /*
   Salesforce object Account does not contain column LegacySFDCAccountId__c_old
Salesforce object Account does not contain column Entity_Name__c_sup
Salesforce object Account does not contain column Entity_Name__c_tt
Salesforce object Account does not contain column Entity_Name__c_new
Salesforce object Account does not contain column PSAP_Type__c_sup
Salesforce object Account does not contain column PSAP__c_sup
Salesforce object Account does not contain column Sales_Team__c_tt
Salesforce object Account does not contain column CurrentCADSWVendor__c_tt
Salesforce object Account does not contain column CurrentCADSWVendor__c_sup
Salesforce object Account does not contain column Client__c_superion
Salesforce object Account does not contain column RecordTypeId_sup
Salesforce object Account does not contain column County_c_sup
Salesforce object Account does not contain column Ship_To_County__c_sup
Salesforce object Account does not contain column CreatedDate_sup
Salesforce object Account does not contain column CreatedDate_tt
Salesforce object Account does not contain column Fax_sup
Salesforce object Account does not contain column NumberofBeds__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_JMS__c_sup
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_sup
Salesforce object Account does not contain column NumberofMobileUnits__c_sup
Salesforce object Account does not contain column Mailing_Billing_City_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_State_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_Street_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_Zip_Postal_Code_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_Country_WMP__c_btst
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_sup
Salesforce object Account does not contain column Name_sup
Salesforce object Account does not contain column name_new
Salesforce object Account does not contain column NumberOfEmployees_sup
Salesforce object Account does not contain column OwnerId_sup
Salesforce object Account does not contain column ParentId_orig
Salesforce object Account does not contain column Planning_to_replace_911_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_RMS_provider__c_sup
Salesforce object Account does not contain column Phone_sup
Salesforce object Account does not contain column Population__c_leg
Salesforce object Account does not contain column Population__c_sup
Salesforce object Account does not contain column Sensitive_Account_WMP__c_legacy
Salesforce object Account does not contain column Sensitive_Account_WMP__c_tar
Salesforce object Account does not contain column PS_Agency_Type__c_sup
Salesforce object Account does not contain column PS_Agency_Type__c_tt
Salesforce object Account does not contain column Shipping_City_WMP__c_btst
Salesforce object Account does not contain column Shipping_State_WMP__c_btst
Salesforce object Account does not contain column Shipping_Street_WMP__c_btst
Salesforce object Account does not contain column Shipping_Zip_Postal_Code__c_btst
Salesforce object Account does not contain column Time_Zone__c_sup
Salesforce object Account does not contain column Time_Zone__c_tt
Salesforce object Account does not contain column Website_sup
Salesforce object Account does not contain column Website_tt
Salesforce object Account does not contain column AccountSource_sup
Salesforce object Account does not contain column AccountSource_tt
Salesforce object Account does not contain column Legacy_TT_Client_Product_Families__c_sup
Salesforce object Account does not contain column Legacy_TT_Client_Product_Groups__c_sup
Salesforce object Account does not contain column Legacy_TT_Customer_Number__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_ePCR__c_sup
Salesforce object Account does not contain column ORI__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Fire_RMS__c_sup
Salesforce object Account does not contain column Legacy_TT_Inactive_Contract_Types__c_sup
Salesforce object Account does not contain column NumberofCallsforService__c_sup
Salesforce object Account does not contain column BillingCountry_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Mapping__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Mobile__c_sup
Salesforce object Account does not contain column Legacy_TT_Parent_Active_Client_Product_F__c_sup
Salesforce object Account does not contain column Legacy_TT_Parent_Active_Contract_Types__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Law_RMS__c_sup
Salesforce object Account does not contain column ShippingCountry_sup
Salesforce object Account does not contain column User_Conference_Attendee__c_sup
Salesforce object Account does not contain column Legacy_TT_Data_Conversion_Analyst__c_sup
Salesforce object Account does not contain column Legacy_TT_GIS_Analyst__c_sup
Salesforce object Account does not contain column TT_Account_At_Risk__c_sup
Salesforce object Account does not contain column TT_Account_Status_Comments__c_sup
Salesforce object Account does not contain column TT_Account_Status_Issues_Problems__c_sup
Salesforce object Account does not contain column Wellness_Check_Contact_Goal__c_sup
Salesforce object Account does not contain column Account_Renewal_Confidence__c_sup
Salesforce object Account does not contain column Account_Renewal_Notes__c_sup
Salesforce object Account does not contain column Change_in_Leadership__c_sup
Salesforce object Account does not contain column Vocal_with_Issues__c_sup
Salesforce object Account does not contain column of_firefighters_at_largest_PSAP_agency__c_sup
Salesforce object Account does not contain column of_Fire_Stations__c_sup
Salesforce object Account does not contain column of_Jail_Users__c_sup
Salesforce object Account does not contain column Number_of_Sworn_Personnel_WMP__c_sup
Salesforce object Account does not contain column X3rd_Party_Type__c_sup
Salesforce object Account does not contain column X911_Call_Taker_Positions__c_sup
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column X911_Parent_Account__c_sup
Salesforce object Account does not contain column X911_Parent_Account__c_tt_orig
Salesforce object Account does not contain column X911_Parent_Total_Sworn_Officers__c_sup
Salesforce object Account does not contain column Actions_Taken_Required__c_sup
Salesforce object Account does not contain column Advertise_Relationship__c_sup
Salesforce object Account does not contain column Agreement_Effective_Date__c_sup
Salesforce object Account does not contain column Agreement_Status__c_sup
Salesforce object Account does not contain column Annual_Calls_for_Service_Received__c_sup
Salesforce object Account does not contain column Average_Inmate_Daily_Population__c_sup
Salesforce object Account does not contain column Billing_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Bomgar_Approved_Client__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_CAD__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_CAD__c_tt
Salesforce object Account does not contain column CAD_Dispatcher_Call_Taker_Seats__c_sup
Salesforce object Account does not contain column CAD_Parent_Fire_EMS_Users__c_sup
Salesforce object Account does not contain column CAD_Parent_Total_Sworn_Officers__c_sup
Salesforce object Account does not contain column Completed_Yearly_Wellness_Checks__c_sup
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Fire_Career__c_sup
Salesforce object Account does not contain column Fire_Civilian_Personnel__c_sup
Salesforce object Account does not contain column Fire_Mobile_Units__c_sup
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Fire_RMS_Users__c_sup
Salesforce object Account does not contain column Fire_Volunteer_Firefighting__c_sup
Salesforce object Account does not contain column Fire_Volunteer_Non_Firefighting__c_sup
Salesforce object Account does not contain column Install_Name__c_sup
Salesforce object Account does not contain column Last_Modified_Actions__c_sup
Salesforce object Account does not contain column Last_Modified_Comments__c_sup
Salesforce object Account does not contain column Last_Modified_Issues__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_911__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Billing__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_FBR__c_sup
Salesforce object Account does not contain column Last_Wellness_Check_Date__c_sup
Salesforce object Account does not contain column Last_Wellness_Check_Type__c_sup
Salesforce object Account does not contain column Law_Civilian_Personnel__c_sup
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_Sup
Salesforce object Account does not contain column Law_RMS_Users__c_sup
Salesforce object Account does not contain column Legacy_TT_Active_Contract_Types__c_sup
Salesforce object Account does not contain column Low_Agency_Ownership_User_Adoption__c_sup
Salesforce object Account does not contain column Maintenance_Lapse__c_sup
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column NDA_Expiration__c_sup
Salesforce object Account does not contain column NDA_in_Place__c_sup
Salesforce object Account does not contain column Next_Renewal_Date__c_sup
Salesforce object Account does not contain column No_Communication_From_Customer__c_sup
Salesforce object Account does not contain column Not_a_Full_Suite_Customer__c_sup
Salesforce object Account does not contain column Parent_Info_Not_Validated__c_sup
Salesforce object Account does not contain column Reference_List_Use__c_sup
Salesforce object Account does not contain column Renewal_Expected_Next_Year__c_sup
Salesforce object Account does not contain column RMS_Parent_Account__c_sup
Salesforce object Account does not contain column RMS_Parent_Account__c_tt_orig
Salesforce object Account does not contain column RMS_Parent_Total_Sworn_Officers__c_sup
Salesforce object Account does not contain column Sales_Tax_ID__c_sup
Salesforce object Account does not contain column Sales_Tax_Rate__c_sup
Salesforce object Account does not contain column Total_Maintenance_Amount__c_sup
Salesforce object Account does not contain column TT_Account_Status__c_sup
Salesforce object Account does not contain column TT_Legacy_Description__c_sup
Salesforce object Account does not contain column ParentId_sup
Salesforce object Account does not contain column Fire_Paid_Per_Call__c_sup
Salesforce object Account does not contain column Secure_Folder_ID__c_sup
Salesforce object Account does not contain column Secure_Folder_ID__c_leg
Salesforce object Account does not contain column Sensitive_Account_WMP__C_leg
Salesforce object Account does not contain column Sensitive_Account_WMP__C_sup
Salesforce object Account does not contain column Client__c_sup
Salesforce object Account does not contain column Client__c_leg
Salesforce object Account does not contain column AAM_PSJ__c_leg
Salesforce object Account does not contain column AAM_PSJ__c_sup
Salesforce object Account does not contain column Customer_Success_Liaison__c_leg
Salesforce object Account does not contain column Customer_Success_Liaison__c_tar
Salesforce object Account does not contain column PSJ_Territory__c_sup
Salesforce object Account does not contain column PSJ_Territory__c_leg
Salesforce object Account does not contain column Account_Executive_Install_PSJ__c_sup
Salesforce object Account does not contain column Account_Executive_Install_PSJ__c_leg
Salesforce object Account does not contain column TTOwnerIsActive
Salesforce object Account does not contain column Account_Executive_del_del__c_sup
Salesforce object Account does not contain column OwnerID_leg
Salesforce object Account does not contain column IQ_Account_Owner__c_leg
Salesforce object Account does not contain column AE_IQ__c_old
Salesforce object Account does not contain column X911_Account_Owner__c_leg
Salesforce object Account does not contain column Type_sup

   */

   --Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_PROD','Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar'

   --drop table [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors]
   select * into [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors]
   from [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar]
   where error<>'Operation Successful.'
      ;

	  select CreatedById from Account_Tritech_SFDC_Preload;
--drop table [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors_bkp]
   select * into [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors_bkp]
   from [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar]
   where error<>'Operation Successful.'
   ;

   --update [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors] set User_Conference_Attendee__c=null
   where error like 'bad value for restricted picklist field: CentralSquare 2019';

   select * from Tritech_PROD.dbo.account where id='0018000000tzBUXAA2'
   --update a set PS_Agency_Type__c=null
   --select PS_Agency_Type__c
   from [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors] a
   where error like 'bad value for restricted picklist field: Law Enforcement'

   select * from [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors];


     --Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_PROD','Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar_errors'


  --select 1d,parentid from Superion_Production.dbo.Account --_bkp_19mar_jg
  -- where id in ('0016A00000AMqr8QAD','0016A00000AMqulQAD','0016A00000AMqx8QAD','0016A00000AMqkrQAD')


  select * from [Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar];


  select 	ID
--,	ERROR
,	LegacySFDCAccountId__c
,	PSAP_Type__c
,	PSAP__c
,	CurrentCADSWVendor__c
,	Client__c
,	RecordTypeId
,	County__c
,	Ship_To_County__c
,	Fax
,	NumberofBeds__c
,	Last_SW_Purchase_Date_JMS__c
,	Current_Justice_SW_Vendor__c
,	NumberofMobileUnits__c
,	Current_Mobile_SW_Vendor__c
,	Name
,	NumberOfEmployees
,	OwnerId
,	Planning_to_replace_911_provider__c
,	Planning_to_replace_CAD_provider__c
,	Planning_to_replace_Jail_provider__c
,	Planning_to_replace_RMS_provider__c
,	Phone
,	Account_Notes__c
,	PS_Agency_Type__c
,	Legacy_TT_Client_Product_Families__c
,	Legacy_TT_Client_Product_Groups__c
,	Legacy_TT_Customer_Number__c
,	Last_SW_Purchase_Date_ePCR__c
,	ORI__c
,	Last_SW_Purchase_Date_Fire_RMS__c
,	Legacy_TT_Inactive_Contract_Types__c
,	NumberofCallsforService__c
,	BillingCountry
,	Last_SW_Purchase_Date_Mapping__c
,	Last_SW_Purchase_Date_Mobile__c
,	Legacy_TT_Parent_Active_Client_Product_F__c
,	Legacy_TT_Parent_Active_Contract_Types__c
,	Last_SW_Purchase_Date_Law_RMS__c
,	ShippingCountry
,	User_Conference_Attendee__c
,	Legacy_TT_Data_Conversion_Analyst__c
,	Legacy_TT_GIS_Analyst__c
,	TT_Account_At_Risk__c
,	TT_Account_Status_Comments__c
,	TT_Account_Status_Issues_Problems__c
,	Wellness_Check_Contact_Goal__c
,	Account_Renewal_Confidence__c
,	Account_Renewal_Notes__c
,	Change_in_Leadership__c
,	Vocal_with_Issues__c
,	of_firefighters_at_largest_PSAP_agency__c
,	of_Fire_Stations__c
,	of_Jail_Users__c
,	Number_of_Sworn_Personnel_WMP__c
,	X3rd_Party_Type__c
,	X911_Call_Taker_Positions__c
,	X911_Current_SW_Vendor__c
,	X911_Parent_Account__c
,	X911_Parent_Total_Sworn_Officers__c
,	Actions_Taken_Required__c
,	Advertise_Relationship__c
,	Agreement_Effective_Date__c
,	Agreement_Status__c
,	Annual_Calls_for_Service_Received__c
,	Average_Inmate_Daily_Population__c
,	Billing_Current_SW_Vendor__c
,	Bomgar_Approved_Client__c
,	Last_SW_Purchase_Date_CAD__c
,	CAD_Dispatcher_Call_Taker_Seats__c
,	CAD_Parent_Fire_EMS_Users__c
,	CAD_Parent_Total_Sworn_Officers__c
,	Completed_Yearly_Wellness_Checks__c
,	ePCR_Current_SW_Vendor__c
,	FBR_Current_SW_Vendor__c
,	Fire_Career__c
,	Fire_Civilian_Personnel__c
,	Fire_Mobile_Units__c
,	Fire_RMS_Current_SW_Vendor__c
,	Fire_RMS_Users__c
,	Fire_Volunteer_Firefighting__c
,	Fire_Volunteer_Non_Firefighting__c
,	Install_Name__c
,	Last_Modified_Actions__c
,	Last_Modified_Comments__c
,	Last_Modified_Issues__c
,	Last_SW_Purchase_Date_911__c
,	Last_SW_Purchase_Date_Billing__c
,	Last_SW_Purchase_Date_FBR__c
,	Last_Wellness_Check_Date__c
,	Last_Wellness_Check_Type__c
,	Law_Civilian_Personnel__c
,	Law_RMS_Current_SW_Vendor__c
,	Law_RMS_Users__c
,	Legacy_TT_Active_Contract_Types__c
,	Low_Agency_Ownership_User_Adoption__c
,	Maintenance_Lapse__c
,	Mapping_Current_SW_Vendor__c
,	NDA_Expiration__c
,	NDA_in_Place__c
,	Next_Renewal_Date__c
,	No_Communication_From_Customer__c
,	Not_a_Full_Suite_Customer__c
,	Parent_Info_Not_Validated__c
,	Reference_List_Use__c
,	Renewal_Expected_Next_Year__c
,	RMS_Parent_Account__c
,	RMS_Parent_Total_Sworn_Officers__c
,	Sales_Tax_ID__c
,	Sales_Tax_Rate__c
,	Total_Maintenance_Amount__c
,	TT_Account_Status__c
,	TT_Legacy_Description__c
,	ParentId
,	Fire_Paid_Per_Call__c
,	Secure_Folder_ID__c
,	SupportNotes__c
,	AAM_PSJ__c
,	PSJ_Territory__c
,	Account_Executive_Install_PSJ__c
,	Account_Executive_del_del__c
,	AE_IQ__c
,	AE_911__c
,	Type
from Account_Tritech_SFDC_MergeAccount_Hotfix_SL_28Mar
;
select 	ID
--,	ERROR
,	LegacySFDCAccountId__c
,	PSAP_Type__c
,	PSAP__c
,	CurrentCADSWVendor__c
,	Client__c
,	RecordTypeId
,	County__c
,	Ship_To_County__c
,	Fax
,	NumberofBeds__c
,	Last_SW_Purchase_Date_JMS__c
,	Current_Justice_SW_Vendor__c
,	NumberofMobileUnits__c
,	Current_Mobile_SW_Vendor__c
,	Name
,	NumberOfEmployees
,	OwnerId
,	Planning_to_replace_911_provider__c
,	Planning_to_replace_CAD_provider__c
,	Planning_to_replace_Jail_provider__c
,	Planning_to_replace_RMS_provider__c
,	Phone
,	Account_Notes__c
,	PS_Agency_Type__c
,	Legacy_TT_Client_Product_Families__c
,	Legacy_TT_Client_Product_Groups__c
,	Legacy_TT_Customer_Number__c
,	Last_SW_Purchase_Date_ePCR__c
,	ORI__c
,	Last_SW_Purchase_Date_Fire_RMS__c
,	Legacy_TT_Inactive_Contract_Types__c
,	NumberofCallsforService__c
,	BillingCountry
,	Last_SW_Purchase_Date_Mapping__c
,	Last_SW_Purchase_Date_Mobile__c
,	Legacy_TT_Parent_Active_Client_Product_F__c
,	Legacy_TT_Parent_Active_Contract_Types__c
,	Last_SW_Purchase_Date_Law_RMS__c
,	ShippingCountry
,	User_Conference_Attendee__c
,	Legacy_TT_Data_Conversion_Analyst__c
,	Legacy_TT_GIS_Analyst__c
,	TT_Account_At_Risk__c
,	TT_Account_Status_Comments__c
,	TT_Account_Status_Issues_Problems__c
,	Wellness_Check_Contact_Goal__c
,	Account_Renewal_Confidence__c
,	Account_Renewal_Notes__c
,	Change_in_Leadership__c
,	Vocal_with_Issues__c
,	of_firefighters_at_largest_PSAP_agency__c
,	of_Fire_Stations__c
,	of_Jail_Users__c
,	Number_of_Sworn_Personnel_WMP__c
,	X3rd_Party_Type__c
,	X911_Call_Taker_Positions__c
,	X911_Current_SW_Vendor__c
,	X911_Parent_Account__c
,	X911_Parent_Total_Sworn_Officers__c
,	Actions_Taken_Required__c
,	Advertise_Relationship__c
,	Agreement_Effective_Date__c
,	Agreement_Status__c
,	Annual_Calls_for_Service_Received__c
,	Average_Inmate_Daily_Population__c
,	Billing_Current_SW_Vendor__c
,	Bomgar_Approved_Client__c
,	Last_SW_Purchase_Date_CAD__c
,	CAD_Dispatcher_Call_Taker_Seats__c
,	CAD_Parent_Fire_EMS_Users__c
,	CAD_Parent_Total_Sworn_Officers__c
,	Completed_Yearly_Wellness_Checks__c
,	ePCR_Current_SW_Vendor__c
,	FBR_Current_SW_Vendor__c
,	Fire_Career__c
,	Fire_Civilian_Personnel__c
,	Fire_Mobile_Units__c
,	Fire_RMS_Current_SW_Vendor__c
,	Fire_RMS_Users__c
,	Fire_Volunteer_Firefighting__c
,	Fire_Volunteer_Non_Firefighting__c
,	Install_Name__c
,	Last_Modified_Actions__c
,	Last_Modified_Comments__c
,	Last_Modified_Issues__c
,	Last_SW_Purchase_Date_911__c
,	Last_SW_Purchase_Date_Billing__c
,	Last_SW_Purchase_Date_FBR__c
,	Last_Wellness_Check_Date__c
,	Last_Wellness_Check_Type__c
,	Law_Civilian_Personnel__c
,	Law_RMS_Current_SW_Vendor__c
,	Law_RMS_Users__c
,	Legacy_TT_Active_Contract_Types__c
,	Low_Agency_Ownership_User_Adoption__c
,	Maintenance_Lapse__c
,	Mapping_Current_SW_Vendor__c
,	NDA_Expiration__c
,	NDA_in_Place__c
,	Next_Renewal_Date__c
,	No_Communication_From_Customer__c
,	Not_a_Full_Suite_Customer__c
,	Parent_Info_Not_Validated__c
,	Reference_List_Use__c
,	Renewal_Expected_Next_Year__c
,	RMS_Parent_Account__c
,	RMS_Parent_Total_Sworn_Officers__c
,	Sales_Tax_ID__c
,	Sales_Tax_Rate__c
,	Total_Maintenance_Amount__c
,	TT_Account_Status__c
,	TT_Legacy_Description__c
,	ParentId
,	Fire_Paid_Per_Call__c
,	Secure_Folder_ID__c
,	SupportNotes__c
,	AAM_PSJ__c
,	PSJ_Territory__c
,	Account_Executive_Install_PSJ__c
,	Account_Executive_del_del__c
,	AE_IQ__c
,	AE_911__c
,	Type
from Superion_Production.dbo.Account_bkp_19mar_jg
 where id in ('0016A00000AMqr8QAD','0016A00000AMqulQAD','0016A00000AMqx8QAD','0016A00000AMqkrQAD')
 ;