/*
Hotfix requested:
From: Jen Vansant <jen.vansant@centralsquare.com> 
Sent: Saturday, March 30, 2019 2:44 PM
To: Eve Nunez <eve.nunez@appsassociates.com>
Cc: kgraham <kgraham@vepcg.com>
Subject: Opportunities Price Book field HotFix

Hotfix the Price Book field as listed below on Opportunities:

TT Field API >> Price_Book__c
CS Field API >> Price_Book__c

Picklist values are a one-to-one match between both:
Inform = Inform
GSA = GSA
Perform = Perform
Respond = Respond
Zuercher = Zuercher
Rapid Implementation = Rapid Implementation

C:\Users\mcorpus\Documents\Superion\Central Square Repo\PROD\Hotfixes\Opportunity_Update_Price_Book_field.sql

*/

--Refresh
USE Tritech_PROD;

USE tritech_prod;

EXEC Sf_refresh 'MC_TRITECH_PROD','Opportunity','yes';

USE staging_prod;

-- drop table Opportunity_Update_Price_Book_field;
SELECT supopptyload.id,
       Cast( NULL AS NVARCHAR(255)) AS Error,
	   supopptyload.name as supOppty_Name,
       supopptyload.legacy_opportunity_id__c,
       ttoppty.price_book__c
INTO   Opportunity_Update_Price_Book_field
FROM   OPPORTUNITY_TRITECH_SFDC_LOAD supopptyload
       JOIN tritech_prod.dbo.OPPORTUNITY ttoppty
         ON ttoppty.id = supopptyload.legacy_opportunity_id__c
WHERE  supopptyload.error = 'Operation Successful.' ;

--(19375 row(s) affected)

--checks:
select distinct price_book__c
from Opportunity_Update_Price_Book_field

Select *
from Opportunity_Update_Price_Book_field
where price_book__c is null; --43

------------------------------------------------------------
/* Execution Log:

ColCompare:

exec SF_ColCompare 'Update','MC_SUPERION_PROD','Opportunity_Update_Price_Book_field';
--- Starting SF_ColCompare V3.6.7
Problems found with Opportunity_Update_Price_Book_field. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 61]
--- Ending SF_ColCompare. Operation FAILED.

ErrorDesc
Salesforce object Opportunity does not contain column supOppty_Name


Update:
exec SF_Bulkops 'Update','MC_SUPERION_PROD','Opportunity_Update_Price_Book_field';
--- Starting SF_BulkOps for Opportunity_Update_Price_Book_field V3.6.7
22:04:04: Run the DBAmp.exe program.
22:04:04: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
22:04:04: Updating Salesforce using Opportunity_Update_Price_Book_field (SQL01 / Staging_PROD) .
22:04:05: DBAmp is using the SQL Native Client.
22:04:06: SOAP Headers: 
22:04:06: Warning: Column 'supOppty_Name' ignored because it does not exist in the Opportunity object.
22:20:52: 19375 rows read from SQL Table.
22:20:52: 7 rows failed. See Error column of row for more information.
22:20:52: 19368 rows successfully processed.
22:20:52: Errors occurred. See Error column of row for more information.
22:20:52: Percent Failed = 0.000.
22:20:52: Error: DBAmp.exe was unsuccessful.
22:20:52: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe update Opportunity_Update_Price_Book_field "SQL01"  "Staging_PROD"  "MC_SUPERION_PROD"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 72]
SF_BulkOps Error: 22:04:04: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC22:04:04: Updating Salesforce using Opportunity_Update_Price_Book_field (SQL01 / Staging_PROD) .22:04:05: DBAmp is using the SQL Native Client.22:04:06: SOAP Headers: 22:04:06: Warning: Column 'supOppty_Name' ignored because it does not exist in the Opportunity object.22:20:52: 19375 rows read from SQL Table.22:20:52: 7 rows failed. See Error column of row for more information.22:20:52: 19368 rows successfully processed.22:20:52: Errors occurred. See Error column of row for more information.


select error, count(*) from Opportunity_Update_Price_Book_field
group by error

MQL should only be used by BDR.	6
Rejected stage can only be used on MQLs. Please use Disqualify stage.	1
Operation Successful.	19368

select * from Opportunity_Update_Price_Book_field
where error <> 'Operation Successful.'