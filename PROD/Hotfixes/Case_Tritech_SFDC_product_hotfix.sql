--Use SUPERION_PRODUCTION
--EXEC SF_Refresh 'SL_SUPERION_PROD ','Case','yes'

/*
 product update in case from the given table Product_MisMatch_TC_TE_Tritech
  */


 select distinct [tt_productName] from Product_MisMatch_TC_TE_Tritech

  select * from Product_MisMatch_TC_TE_Tritech

  use Staging_PROD
  go

  -- drop table Case_Tritech_SFDC_product_hotfix
  select  a.id ,ERROR	 =   CAST(SPACE(255) as NVARCHAR(255)),
  a.ProductId as productid_orig,productid='01t2G0000067tfQQAQ',a.ProductCode__c as productcode_orig,
  b.[data import product_name]
 into Case_Tritech_SFDC_product_hotfix
  from Superion_Production.dbo.[case] a 
  inner join Product_MisMatch_TC_TE_Tritech b
  on a.id = b.[superion case ID]


  --(11372 row(s) affected)
select * from Case_Tritech_SFDC_product_hotfix 


--Exec SF_colcompare 'Update','SL_SUPERION_PROD ','Case_Tritech_SFDC_product_hotfix'


--Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_PROD ','Case_Tritech_SFDC_product_hotfix'
/*
Salesforce object Case does not contain column productid_orig
Salesforce object Case does not contain column productcode_orig
Salesforce object Case does not contain column data import product_name
*/

select error, count(*) from Case_Tritech_SFDC_product_hotfix 
group by error


select * from Case_Tritech_SFDC_product_hotfix where error<>'Operation Successful.'