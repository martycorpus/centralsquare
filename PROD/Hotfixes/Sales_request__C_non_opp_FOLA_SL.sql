--USE SUPERION_Production
--EXEC SF_Refresh 'SL_Superion_PROD','Opportunity', Yes
--MC fixed mapping.

use Staging_PROD

--drop table sales_request__C_non_opp_FOIA_SL
declare @defaultrecordtype nvarchar(18)=(select id from superion_production.dbo.[recordtype]
where SobjectType='sales_request__C' and name='General Request' )


select id=cast(space(18) as nvarchar(18))
,error=cast(space(255) as nvarchar(255))
 ,Date_of_Request__c=FOIA_Req_Date__c --fix MC
 ,FOIA_Req_Date__c_opp=FOIA_Req_Date__c
 , FOIA_Sent_to_Agency__c  = FOIA_req_d__c --fix MC
,FOIA_req_d__c_opp=FOIA_req_d__c
,FOIA_Request_Acknowledged__c  = FOIA_Rec_d__c
,FOIA_Rec_d__c_opp=FOIA_Rec_d__c
,Date_Completed__c= FOIA_Rec_Date__c
,FOIA_Rec_Date__c_opp=FOIA_Rec_Date__c
,What_is_Needed__c = 'FOIA Request'
,recordtypeid=@defaultrecordtype
,opportunity__C=a.id
into sales_request__C_non_opp_FOIA_SL
from Superion_Production.dbo.opportunity a
where  migrated_record__C='false'
and (FOIA_req_d__c is not null or FOIA_Req_Date__c is not null or FOIA_Rec_d__c is not null or FOIA_Rec_Date__c is not null)
;--176

select * from sales_request__C_non_opp_FOIA_SL;

-- exec SF_ColCompare 'Insert','SL_Superion_prod','sales_request__C_non_opp_FOIA_SL'

/*
Salesforce object sales_request__C does not contain column FOIA_Req_Date__c_opp
Salesforce object sales_request__C does not contain column FOIA_req_d__c_opp
Salesforce object sales_request__C does not contain column FOIA_Rec_d__c_opp
Salesforce object sales_request__C does not contain column FOIA_Rec_Date__c_opp
*/

-- exec SF_Bulkops 'Insert','SL_Superion_prod','sales_request__C_non_opp_FOIA_SL'


select *  from sales_request__C_non_opp_FOIA_SL
where error<>'Operation successful.' and FOIA_Request_Acknowledged__c in ('Denied','Pending')

FOIA_Sent_to_Agency__c
Yes

FOIA_Request_Acknowledged__c
Pending


FOIA Request Acknowledged: bad value for restricted picklist field: Pending


select *  
into sales_request__C_non_opp_FOIA_SL_errors
from sales_request__C_non_opp_FOIA_SL
where error<>'Operation successful.';


select *  
into sales_request__C_non_opp_FOIA_SL_errors_bkp
from sales_request__C_non_opp_FOIA_SL
where error<>'Operation successful.'

select * 
--update a set FOIA_Request_Acknowledged__c=null
 from sales_request__C_non_opp_FOIA_SL_errors a
where error<>'Operation successful.' and FOIA_Request_Acknowledged__c in ('Denied','Pending');--18


select * 
--update a set FOIA_Request_Acknowledged__c='NA'
 from sales_request__C_non_opp_FOIA_SL_errors a
where error<>'Operation successful.' and FOIA_Request_Acknowledged__c ='N/A' ;--7

select * 
--update a set FOIA_Sent_to_Agency__c='NA'
 from sales_request__C_non_opp_FOIA_SL_errors a
where error<>'Operation successful.' and FOIA_Sent_to_Agency__c ='N/A' ;--9

-- exec SF_Bulkops 'Insert','SL_Superion_prod','sales_request__C_non_opp_FOIA_SL_errors'

select error,count(*) from sales_request__C_non_opp_FOIA_SL_errors
group by error;

select *  
--delete
from sales_request__C_non_opp_FOIA_SL
where error<>'Operation successful.';

--insert into sales_request__C_non_opp_FOIA_SL
select * from sales_request__C_non_opp_FOIA_SL_errors

select id,opportunity__C,FOIA_Request_Acknowledged__c,FOIA_Rec_d__c_opp
 from sales_request__C_non_opp_FOIA_SL
where FOIA_Rec_d__c_opp in ('Denied','Pending');--18

