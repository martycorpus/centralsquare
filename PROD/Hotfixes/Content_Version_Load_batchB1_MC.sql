exec  SF_ColCompare 'Insert','rT_SUPERION_PROD','ContentVersion_LoadAttachments_RT_A_1_1'
--- Starting SF_ColCompare V3.6.7
--Problems found with ContentVersion_LoadAttachments_RT_A_1_1. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 0]
--- Ending SF_ColCompare. Operation FAILED.

--ErrorDesc
--Salesforce object ContentVersion does not contain column ParentId
--Salesforce object ContentVersion does not contain column BodyLength
--Salesforce object ContentVersion does not contain column ROWNUM

SELECT TOP 5 * FROM ContentVersion_LoadAttachments_RT_B_1

exec SF_BulkOps 'Insert:batchsize(10)','rT_SUPERION_PROD','ContentVersion_LoadAttachments_RT_B_1' -->5 MB

--- Starting SF_BulkOps for ContentVersion_LoadAttachments_RT_B_1 V3.6.7
--19:38:06: Run the DBAmp.exe program.
--19:38:06: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
--19:38:06: Inserting ContentVersion_LoadAttachments_RT_B_1 (SQL01 / Staging_PROD).
--19:38:07: DBAmp is using the SQL Native Client.
--19:38:07: Batch size reset to 10 rows per batch.
--19:38:08: SOAP Headers: 
--19:38:08: Warning: Column 'ParentId' ignored because it does not exist in the ContentVersion object.
--19:38:08: Warning: Column 'BodyLength' ignored because it does not exist in the ContentVersion object.
--21:30:22: Error: Update for SQL Table failed.
--Source: Microsoft SQL Server Native Client 11.0
--Description: Multiple-step OLE DB operation generated errors. Check each OLE DB status value, if available. No work was done.
--Source: Microsoft SQL Server Native Client 11.0
--Description: Communication link failure
--21:30:22: 5960 rows read from SQL Table.
--21:30:22: 5950 rows successfully processed.
--21:30:22: Errors occurred. See Error column of row for more information.
--21:30:22: Percent Failed = 57.600.
--21:30:22: Error: DBAmp.exe was unsuccessful.
--21:30:22: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:batchsize(10) ContentVersion_LoadAttachments_RT_B_1 "SQL01"  "Staging_PROD"  "rT_SUPERION_PROD"  " " 
----- Ending SF_BulkOps. Operation FAILED.
--Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 13]
--SF_BulkOps Error: 19:38:06: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC19:38:06: Inserting ContentVersion_LoadAttachments_RT_B_1 (SQL01 / Staging_PROD).19:38:07: DBAmp is using the SQL Native Client.19:38:07: Batch size reset to 10 rows per batch.19:38:08: SOAP Headers: 19:38:08: Warning: Column 'ParentId' ignored because it does not exist in the ContentVersion object.19:38:08: Warning: Column 'BodyLength' ignored because it does not exist in the ContentVersion object.21:30:22: Error: Update for SQL Table failed.Source: Microsoft SQL Server Native Client 11.0Description: Multiple-step OLE DB operation generated errors. Check each OLE DB status value, if available. No work was done.Source: Microsoft SQL Server Native Client 11.0Description: Communication link failure21:30:22: 5960 rows read from SQL Table.21:30:22: 5950 rows successfully processed.21:30:22: Errors occurred. See Error column of row for more information.


select error, count(*)
from ContentVersion_LoadAttachments_RT_B_1
group by error;

alter table ContentVersion_LoadAttachments_RT_B_1 add ident int identity(1,1);

select 
*
into ContentVersion_LoadAttachments_RT_B_2 
from ContentVersion_LoadAttachments_RT_B_1
where error <> 'Operation Successful.' --(8069 row(s) affected)

exec SF_BulkOps 'Insert:batchsize(10)','rT_SUPERION_PROD','ContentVersion_LoadAttachments_RT_B_2'

