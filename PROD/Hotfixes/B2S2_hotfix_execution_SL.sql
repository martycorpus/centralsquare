Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','Account','Yes'

--select * 
--into account_4APr19_bkp_SL
--from account

use Staging_PROD

select name, * from SL_SUPERION_PROD...[Profile]
where id='00e6A000000HCY8'

select * from Bill_to_Ship_to__c_AssignedAcct;--359

--Exec SF_Colcompare  'Insert ','SL_SUPERION_PROD', 'Bill_to_Ship_to__c_AssignedAcct'

/*
Salesforce object Bill_to_Ship_to__c does not contain column GP Company ID Orig
Salesforce object Bill_to_Ship_to__c does not contain column Account_orig
Salesforce object Bill_to_Ship_to__c does not contain column customernumber_before
Salesforce object Bill_to_Ship_to__c does not contain column Addressee
Salesforce object Bill_to_Ship_to__c does not contain column tr_accountid
Salesforce object Bill_to_Ship_to__c does not contain column Account__C_sp
Salesforce object Bill_to_Ship_to__c does not contain column Address Code orig
Salesforce object Bill_to_Ship_to__c does not contain column Street_2_orig
Salesforce object Bill_to_Ship_to__c does not contain column Address 3 Orig
Salesforce object Bill_to_Ship_to__c does not contain column Country__c_orig
Salesforce object Bill_to_Ship_to__c does not contain column Zip_Postal_Code__c_nontext
Salesforce object Bill_to_Ship_to__c does not contain column Phone__c_orig
Salesforce object Bill_to_Ship_to__c does not contain column Phone1_orig
Salesforce object Bill_to_Ship_to__c does not contain column Primary Billto Address Code Orig
Salesforce object Bill_to_Ship_to__c does not contain column Primary Shipto Address Code Orig
Salesforce object Bill_to_Ship_to__c does not contain column Taxable__c_orig
Salesforce object Bill_to_Ship_to__c does not contain column DeliveryPreference_orig
Salesforce object Bill_to_Ship_to__c does not contain column rowref
Salesforce object Bill_to_Ship_to__c does not contain column F42

*/

--Exec SF_BulkOps 'Insert:batchsize(1)', 'SL_SUPERION_PROD','Bill_to_Ship_to__c_AssignedAcct' --need to set the batchsize to 1.


/*
--- Starting SF_BulkOps for Bill_to_Ship_to__c_AssignedAcct V3.7.7
05:55:33: Run the DBAmp.exe program.
05:55:33: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
05:55:33: Inserting Bill_to_Ship_to__c_AssignedAcct (SQL01 / Staging_PROD).
05:55:34: DBAmp is using the SQL Native Client.
05:55:34: Batch size reset to 1 rows per batch.
05:55:34: SOAP Headers: 
05:55:34: Warning: Column 'GP Company ID Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Account_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'customernumber_before' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Addressee' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'tr_accountid' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Account__C_sp' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Address Code orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Street_2_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Address 3 Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Country__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Zip_Postal_Code__c_nontext' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Phone__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Phone1_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Primary Billto Address Code Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Primary Shipto Address Code Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'Taxable__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'DeliveryPreference_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'rowref' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:55:34: Warning: Column 'F42' ignored because it does not exist in the Bill_To_Ship_To__c object.
05:58:07: 359 rows read from SQL Table.
05:58:07: 1 rows failed. See Error column of row for more information.
05:58:07: 358 rows successfully processed.
05:58:07: Errors occurred. See Error column of row for more information.
05:58:07: Percent Failed = 0.300.
05:58:07: Error: DBAmp.exe was unsuccessful.
05:58:07: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:batchsize(1) Bill_to_Ship_to__c_AssignedAcct "SQL01"  "Staging_PROD"  "SL_SUPERION_PROD"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 39]
SF_BulkOps Error: 05:55:33: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC05:55:33: Inserting Bill_to_Ship_to__c_AssignedAcct (SQL01 / Staging_PROD).05:55:34: DBAmp is using the SQL Native Client.05:55:34: Batch size reset to 1 rows per batch.05:55:34: SOAP Headers: 05:55:34: Warning: Column 'GP Company ID Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Account_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'customernumber_before' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Addressee' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'tr_accountid' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Account__C_sp' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Address Code orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Street_2_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Address 3 Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Country__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Zip_Postal_Code__c_nontext' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Phone__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Phone1_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Primary Billto Address Code Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Primary Shipto Address Code Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.05:55:34: Warning: Column 'Taxable__c_orig' ignored bec...





*/


select error,count(*) from Bill_to_Ship_to__c_AssignedAcct
group by error;

select * from Bill_to_Ship_to__c_Delta_Other;--39

--Exec SF_Colcompare  'Insert' ,'SL_SUPERION_PROD','Bill_to_Ship_to__c_Delta_Other'
/*
Salesforce object Bill_to_Ship_to__c does not contain column ERROR_ORIG
Salesforce object Bill_to_Ship_to__c does not contain column GP Company ID Orig
Salesforce object Bill_to_Ship_to__c does not contain column Account_orig
Salesforce object Bill_to_Ship_to__c does not contain column customernumber_before
Salesforce object Bill_to_Ship_to__c does not contain column Addressee
Salesforce object Bill_to_Ship_to__c does not contain column tr_accountid
Salesforce object Bill_to_Ship_to__c does not contain column Account__C_sp
Salesforce object Bill_to_Ship_to__c does not contain column Address Code orig
Salesforce object Bill_to_Ship_to__c does not contain column Street_2_orig
Salesforce object Bill_to_Ship_to__c does not contain column Address 3 Orig
Salesforce object Bill_to_Ship_to__c does not contain column Country__c_orig
Salesforce object Bill_to_Ship_to__c does not contain column Phone__c_orig
Salesforce object Bill_to_Ship_to__c does not contain column Phone1_orig
Salesforce object Bill_to_Ship_to__c does not contain column Primary Billto Address Code Orig
Salesforce object Bill_to_Ship_to__c does not contain column Primary Shipto Address Code Orig
Salesforce object Bill_to_Ship_to__c does not contain column Taxable__c_orig
Salesforce object Bill_to_Ship_to__c does not contain column DeliveryPreference_orig
Salesforce object Bill_to_Ship_to__c does not contain column ROWREF

*/

--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_PROD','Bill_to_Ship_to__c_Delta_Other' --need to set the batchsize to 1.
/*
--- Starting SF_BulkOps for Bill_to_Ship_to__c_Delta_Other V3.7.7
06:05:14: Run the DBAmp.exe program.
06:05:14: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
06:05:14: Inserting Bill_to_Ship_to__c_Delta_Other (SQL01 / Staging_PROD).
06:05:15: DBAmp is using the SQL Native Client.
06:05:15: Batch size reset to 1 rows per batch.
06:05:15: SOAP Headers: 
06:05:15: Warning: Column 'ERROR_ORIG' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'GP Company ID Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Account_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'customernumber_before' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Addressee' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'tr_accountid' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Account__C_sp' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Address Code orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Street_2_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Address 3 Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Country__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Phone__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Phone1_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Primary Billto Address Code Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Primary Shipto Address Code Orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'Taxable__c_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'DeliveryPreference_orig' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:15: Warning: Column 'ROWREF' ignored because it does not exist in the Bill_To_Ship_To__c object.
06:05:31: 39 rows read from SQL Table.
06:05:31: 39 rows successfully processed.
06:05:31: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

*/

select error,count(*) from Bill_to_Ship_to__c_Delta_Other
group by error;