Use Tritech_PROD
EXEC SF_Refresh 'EN_TRITECH_PROD','Case','Yes'
EXEC SF_Refresh 'EN_TRITECH_PROD','User','Yes'
EXEC SF_Refresh 'EN_TRITECH_PROD','Group','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD ','Case','yes'

use Staging_PROD;

select b.id,error=cast(space(255) as nvarchar(max)),
tt_user.name,tt_grp.name,a.[owner in TT Org],b.ownerid
 from tt_case_inactive_owner_list a
inner join Superion_Production.dbo.[case] b
on a.id=b.id
left outer join Tritech_PROD.dbo.[case] c
on b.Legacy_ID__c=c.id
left outer join Tritech_PROD.dbo.[user] tt_user
on c.ownerid=tt_user.id
left outer join Tritech_PROD.dbo.[Group] tt_grp
on c.ownerid=tt_grp.id
where [owner in TT Org]<>'#N/A'
and b.migrated_record__C='true'
and isnull(tt_user.name,tt_grp.name)<>[owner in TT Org];


DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'CentralSquare API');

select id=cast(space(18) as nvarchar(18)),error=cast(space(255) as nvarchar(255))
----------
,Body										 =	 'Original TT Owner: '+a.[owner in TT Org]
,CreatedById=@DefaultUser
			,Visibility								 = 'InternalUsers'
				,Parentid					=   b.Id
-------------

,tt_user.name tt_user_name,tt_grp.name tt_group_name,
a.[owner in TT Org],b.ownerid  ownerid_sup,c.ownerid ownerid_tt
	into feedItem_Superion_Case_owner_update_SL_hotfix
 from tt_case_inactive_owner_list a
inner join Superion_Production.dbo.[case] b
on a.id=b.id
left outer join Tritech_PROD.dbo.[case] c
on b.Legacy_ID__c=c.id
left outer join Tritech_PROD.dbo.[user] tt_user
on c.ownerid=tt_user.id
left outer join Tritech_PROD.dbo.[Group] tt_grp
on c.ownerid=tt_grp.id
where [owner in TT Org]<>'#N/A'
and b.migrated_record__C='true'
;--4521

select * from feedItem_Superion_Case_owner_update_SL_hotfix;

-- insert Casecomment records as Chatter comment.

-- EXEC SF_ColCompare 'Insert','SL_SUPERION_PROD ', 'feedItem_Superion_Case_owner_update_SL_hotfix' 

/*
Salesforce object feedItem does not contain column tt_user_name
Salesforce object feedItem does not contain column tt_group_name
Salesforce object feedItem does not contain column owner in TT Org
Salesforce object feedItem does not contain column ownerid_sup
Salesforce object feedItem does not contain column ownerid_tt


*/

-- EXEC SF_BulkOps 'Insert','SL_SUPERION_PROD ','feedItem_Superion_Case_owner_update_SL_hotfix' 

