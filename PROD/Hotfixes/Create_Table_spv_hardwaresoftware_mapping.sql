USE [Staging_PROD]
GO

/****** Object:  Table [dbo].[spv_hardwaresoftware_mapping]    Script Date: 4/12/2019 3:22:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[spv_hardwaresoftware_mapping](
	[SourceObject] [nvarchar](255) NULL,
	[SourceFieldName] [nvarchar](255) NULL,
	[ProductGroup] [nvarchar](255) NULL,
	[ProductLine] [nvarchar](255) NULL,
	[EnvironmentType] [nvarchar](255) NULL
) ON [PRIMARY]

GO


