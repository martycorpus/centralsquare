select * 
	from [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2]--10,282 

--************************************************************************************************************
--										START ANALYSIS
--************************************************************************************************************

-- Original file
SELECT COUNT(*) 
	FROM [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2] --10,282 

-- Loaded file has 2 records more than source file [Bill_To_Ship_To__c_Tritech_Excel_load] file
SELECT COUNT(*) 
	FROM [Staging_PROD].[dbo].Bill_To_Ship_To__c_Tritech_Excel_load --10,284 
		/*
				Address_Code__c		customernumber_before	TOTAL
				-------------------------------------------------
				PRIMARY-TSS				CA297					2
				PRIMARY-TSS				OH175					2

		SELECT Address_Code__c, customernumber_before, account__c
			FROM [Bill_To_Ship_To__c_Tritech_Excel_load]
			WHERE [Address_Code__c] IN( 'PRIMARY-TSS')
			AND [customernumber_before] IN ( 'CA297','OH175')

		TWO LEGACY ADDRESS RECORDS LINKED TO TWO ACCOUNTS

		
		SELECT COUNT(*) 
			FROM Superion_Production.dbo.Bill_To_Ship_To__c 
			WHERE Migrated_Record__c= 'true' --14,973
		*/



--************************************************************************************************************
--										START HOTFIX
--************************************************************************************************************
/*
	USE Superion_Production
	EXEC SF_Refresh [EN_Superion_PROD],'Bill_To_Ship_To__c','Yes'
	USE Staging_PROD
*/

USE Staging_PROD

-- DROP TABLE Bill_To_Ship_To__c_Customer_Attention_To
-----------------------------------------------------------------------
-- Link load file to original file and update (excludes cloned records)
-----------------------------------------------------------------------
SELECT b2_load.ID
      ,Error						= CAST(SPACE(255) as nvarchar)
	  ,Customer_Attention_To__c		= CASE
										WHEN b2_orig.ShipToName IS NOT NULL AND b2_orig.ShipToName <> '' THEN b2_orig.ShipToName
										WHEN b2_orig.[Statement Name] IS NOT NULL AND b2_orig.[Statement Name] <> '' THEN b2_orig.[Statement Name]
										ELSE b2_orig.[Customer Name]
										--b2_orig.ShipToName > ''

									  END
	  ,b2_orig.ShipToName
	  ,b2_orig.[Statement Name]
	  ,b2_orig.[Customer Name]
	  ,b2_orig_file_Address_Code	= b2_orig.[Address Code]
	  ,b2_orig_file_Customer_Number	= b2_orig.[Customer Number]
	  ,b2_orig_file_GP				= b2_orig.[GP Company ID]
	  ,b2_load_file_Address_Code	= b2_load.[Address_Code__c] 
	  ,b2_load_file_Customer_Number	= b2_load.[customernumber_before]
	  ,b2_load_file_GP				= b2_load.[GP Company ID Orig]
	  ,key1_load_file				= b2_load.Address_Code__c + '-' + b2_load.customernumber_before
	  ,key2_orig_file				= b2_orig.[Address Code] + '-' +b2_orig.[GP Company ID] + '-'+b2_orig.[customernumber_before]
  INTO Bill_To_Ship_To__c_Customer_Attention_To
  FROM [Staging_PROD].[dbo].[Bill_To_Ship_To__c_Tritech_Excel_load] b2_load -- 10,284
  LEFT JOIN [TRITECH_NON_SFDC].[dbo].[TRITECH_CUSTOMER_B2S2] b2_orig		-- 10,282
  ON b2_load.Address_Code__c + '-' + b2_load.customernumber_before		 = b2_orig.[Address Code] + '-' +b2_orig.[GP Company ID] + '-'+b2_orig.[customernumber_before]
  WHERE b2_load.ID <> '' -- 9116

--EXEC SF_ColCompare 'Update',[EN_Superion_PROD],'Bill_To_Ship_To__c_Customer_Attention_To'
--EXEC SF_Bulkops 'Update',[EN_Superion_PROD],'Bill_To_Ship_To__c_Customer_Attention_To'

-----------------------------------------------------------------------
-- Isolate cloned records
-----------------------------------------------------------------------
;WITH cte_Migrated_B2 AS
	(SELECT ID
		FROM Bill_To_Ship_To__c_Tritech_Excel_load
		WHERE ID <> '')
SELECT b2_prod.ID
	  ,b2_prod.Customer_Attention_To__c
	  ,b2_prod.Address_Code__c
	  ,b2_prod.Legacy_Address_ID__c
	  ,b2_prod.CreatedById
	  , *
	FROM Superion_Production.dbo.Bill_To_Ship_To__c b2_prod
			WHERE b2_prod.Migrated_Record__c= 'true' --14,973
			AND ID NOT IN (SELECT ID	
							FROM cte_Migrated_B2)


-- DROP TABLE Bill_To_Ship_To__c_Customer_Attention_To_Cloned
;WITH cte_Migrated_B2 AS
	(SELECT ID
		FROM Bill_To_Ship_To__c_Tritech_Excel_load
		WHERE ID <> '')
SELECT b2_prod.ID
      ,Error							= CAST(SPACE(255) as nvarchar)
	  ,Customer_Attention_To__c_PROD	= b2_prod.Customer_Attention_To__c
	  ,Customer_Attention_To__c			= b2_prod.Account_Name__c
	INTO Bill_To_Ship_To__c_Customer_Attention_To_Cloned
	FROM Superion_Production.dbo.Bill_To_Ship_To__c b2_prod
			WHERE b2_prod.Migrated_Record__c= 'true' --14,973
			AND ID NOT IN (SELECT ID	
							FROM cte_Migrated_B2) --5857

-- SELECT * FROM Bill_To_Ship_To__c_Customer_Attention_To_Cloned
							
--EXEC SF_ColCompare 'Update',[EN_Superion_PROD],'Bill_To_Ship_To__c_Customer_Attention_To_Cloned'
--EXEC SF_Bulkops 'Update',[EN_Superion_PROD],'Bill_To_Ship_To__c_Customer_Attention_To_Cloned'