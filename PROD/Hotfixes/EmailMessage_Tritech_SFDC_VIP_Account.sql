

--use Tritech_Prod
--EXEC SF_Refresh 'MC_Tritech_PROD','EmailMessage','yes'
--EXEC SF_Refresh 'MC_TRITECH_PROD','EmailMEssageRelation','Yes'

-- use SUPERION_PRODUCTION
--EXEC SF_refresh 'SL_SUPERION_PROD ','EmailMessage','yes'
--EXEC SF_refresh 'SL_SUPERION_PROD ','Task','yes'
/* Turn off the Email message master process builder */

-- select count(*) from Tritech_PROD.dbo.EmailMessage
-- 208946
use Staging_PROD 

go

-- drop table EmailMessage_Tritech_SFDC_Preload_VIP_Account
declare @defaultuser varchar(18)
select @defaultuser = id  from [SUPERION_PRODUCTION].dbo.[user]
							where name like 'CentralSquare API'

	SELECT		 
          		ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_email.Id
				,Legacy_Source_System__c                 ='Tritech'
				,Migrated_Record__c                      ='True'
				,ActivityID_orig						= tr_email.[ActivityId]
				,ActivityId									=target_task.id
				,BccAddress_orig						 = tr_email.[BccAddress]
				,BCcAddress								 = cast([BccAddress] as nvarchar(max))
				,CCAddress_orig							 = tr_email.[CcAddress]
				,CcAddress								 =	cast([CcAddress] as nvarchar(max))
				,CreatedById_orig						= tr_email.[CreatedById]
				,CreatedById							= iif(Tar_CreateID.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Tar_createID.id )
				,CreatedDate							= tr_email.[CreatedDate]
				,FromAddress_orig						= tr_email.[FromAddress]
				,FromAddress							= cast([FromAddress] as nvarchar(max))
				,FromName								= tr_email.[FromName]
			
														  --,tr_email.[HasAttachment]
				,Headers								= tr_email.[Headers]
				,HTMLBody								= tr_email.[HtmlBody]
				,id_orig								= tr_email.[Id]
				,Incoming								= tr_email.[Incoming]
				,IsClientManaged_orig						= tr_email.[IsClientManaged]
				,IsClientManaged						='True'
														  --,tr_email.[IsDeleted]
				,IsExternallyVisible					= tr_email.[IsExternallyVisible]
														  --,tr_email.[LastModifiedById]
														  --,tr_email.[LastModifiedDate]
				,MessageDate							= tr_email.[MessageDate]
				,MessageIdentifier						= tr_email.[MessageIdentifier]
						,ParentID_orig					=	tr_email.[ParentId]
						,ParentID						=  Target_case.id
				,RelatedToId_orig							= tr_email.[RelatedToId]
				,ReplyToEmailMessageId_orig					= tr_email.[ReplyToEmailMessageId]
				,ReplyToEmailMessageId=iif(tr_email.ReplyToEmailMessageId is null,null,'Legacy_Id__C:' + tr_email.ReplyToEmailMessageId)
				,Status							= tr_email.[Status]
				
				,Subject								= tr_email.[Subject]
														  --,tr_email.[SystemModstamp]
				,TextBody								= tr_email.[TextBody]
				,ThreadIdentifier						= tr_email.[ThreadIdentifier]
				,ToAddress_orig							= tr_email.[ToAddress]
				,ToAddress								= cast(tr_email.[ToAddress] as nvarchar(max))
				--,ValidatedFromAddress					= replace(cast(tr_email.[ValidatedFromAddress] as nvarchar(max)),'@','@dummy.')
					,tt_emr_EmailMessageId=tt_emr.EmailMessageId	
					,tt_emr_EmailMessagerelationId=tt_emr.id	
					,RelationId_orig=tt_emr.RelationId	
					,RelationId_sup=target_cnt.id
					,RelationType_orig=tt_emr.RelationType
					,sp_task_whoid=target_task.whoid 
					,tt_task_whoid=tt_task.whoid
					 into EmailMessage_Tritech_SFDC_Preload_VIP_Account
 FROM [Tritech_PROD].[dbo].[EmailMessage] tr_email
  inner join staging_prod.dbo.Case_tritech_SFDC_load_VIP_Account target_Case
  on target_Case.legacy_ID__c= tr_Email.ParentId
  -----------------------------------------------------------------------------------
  left outer join Tritech_PROD.dbo.[EmailMessageRelation] tt_emr
  on tr_email.id=tt_emr.EmailMessageId and tt_emr.RelationObjectType='Contact'
  -----------------------------------------------------------------------------------
  left outer join  SUPERION_PRODUCTION.dbo.contact target_cnt
  on target_cnt.legacy_id__c= tt_emr.RelationId
  -----------------------------------------------------------------------------------
  left outer join  Tritech_PROD.dbo.task tt_task
  on tt_task.id= tr_email.ActivityId
 
  -----------------------------------------------------------------------------------
  left outer join  SUPERION_PRODUCTION.dbo.task target_task
  on target_task.legacy_id__c= tr_email.ActivityId
  -----------------------------------------------------------------------------------
  left join SUPERION_PRODUCTION.dbo.[User] Tar_CreateID
  on Tar_CreateID.Legacy_Tritech_Id__c=tr_email.CreatedById
  and Tar_CreateId.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------
  left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_email.createdbyId
  ------------------------------------------------------------------------------------
  where 1=1
  
  ;

  --
  -- 


  select count(*) from EmailMessage_Tritech_SFDC_Preload_VIP_Account;--

  select * from EmailMessage_Tritech_SFDC_Preload_VIP_Account
where sp_task_whoid is null and ActivityID_orig is not null and tt_task_whoid is not null;

--


  -- check for the duplicates.
  select legacy_id__c ,count(*) from EmailMessage_Tritech_SFDC_Preload_VIP_Account
  group by legacy_id__c
  having count(*)>1;--0

  -- drop table EmailMessage_Tritech_SFDC_load_VIP_Account
  select  
  * 
  into EmailMessage_Tritech_SFDC_load_VIP_Account
  from EmailMessage_Tritech_SFDC_Preload_VIP_Account
  where 1=1
  and ReplyToEmailMessageId_orig is null
  -- 4 mins
   -- order by parentid_orig,MessageDate
  
    --(149673 row(s) affected)

	select count(*) from EmailMessage_Tritech_SFDC_load_VIP_Account ;
  --149673
  
--: Run batch program to create EmailMessage related to cases
  
--exec SF_ColCompare 'Insert','SL_SUPERION_PROD ', 'EmailMessage_Tritech_SFDC_load_VIP_Account' 

--check column names
  

/*
Salesforce object EmailMessage does not contain column ActivityID_orig
Salesforce object EmailMessage does not contain column BccAddress_orig
Salesforce object EmailMessage does not contain column CCAddress_orig
Salesforce object EmailMessage does not contain column CreatedById_orig
Salesforce object EmailMessage does not contain column FromAddress_orig
Salesforce object EmailMessage does not contain column id_orig
Salesforce object EmailMessage does not contain column IsClientManaged_orig
Salesforce object EmailMessage does not contain column ParentID_orig
Salesforce object EmailMessage does not contain column RelatedToId_orig
Salesforce object EmailMessage does not contain column ReplyToEmailMessageId_orig
Salesforce object EmailMessage does not contain column ToAddress_orig
Salesforce object EmailMessage does not contain column tt_emr_EmailMessageId
Salesforce object EmailMessage does not contain column tt_emr_EmailMessagerelationId
Salesforce object EmailMessage does not contain column RelationId_orig
Salesforce object EmailMessage does not contain column RelationId_sup
Salesforce object EmailMessage does not contain column RelationType_orig
Salesforce object EmailMessage does not contain column sp_task_whoid
Salesforce object EmailMessage does not contain column tt_task_whoid
*/



--Exec SF_BulkOps 'Insert:batchsize(50)','SL_SUPERION_PROD ','EmailMessage_Tritech_SFDC_load_VIP_Account'


select error, count(*) from EmailMessage_Tritech_SFDC_load_VIP_Account
group by error;

--------------------------------------------------
-- drop table EmailMessage_Tritech_SFDC_load_replyto_VIP_Account
  select IDENTITY(INT, 1, 1) sort_order,
     * 
  into EmailMessage_Tritech_SFDC_load_replyto_VIP_Account
  from EmailMessage_Tritech_SFDC_Preload_VIP_Account
  where 1=1
  and ReplyToEmailMessageId_orig is not null
  order by parentid,messagedate
  --(31102 row(s) affected)

  
  select * from EmailMessage_Tritech_SFDC_load_replyto_VIP_Account;

--: Run batch program to create EmailMessage related to Cases
  
--exec SF_ColCompare 'Insert','SL_SUPERION_PROD ', 'EmailMessage_Tritech_SFDC_load_replyto_VIP_Account' 

--check column names
  
--Exec SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_PROD ','EmailMessage_Tritech_SFDC_load_replyto_VIP_Account'

/*
Salesforce object EmailMessage does not contain column sort_order
Salesforce object EmailMessage does not contain column ActivityID_orig
Salesforce object EmailMessage does not contain column BccAddress_orig
Salesforce object EmailMessage does not contain column CCAddress_orig
Salesforce object EmailMessage does not contain column CreatedById_orig
Salesforce object EmailMessage does not contain column FromAddress_orig
Salesforce object EmailMessage does not contain column id_orig
Salesforce object EmailMessage does not contain column IsClientManaged_orig
Salesforce object EmailMessage does not contain column ParentID_orig
Salesforce object EmailMessage does not contain column RelatedToId_orig
Salesforce object EmailMessage does not contain column ReplyToEmailMessageId_orig
Salesforce object EmailMessage does not contain column ToAddress_orig
Salesforce object EmailMessage does not contain column tt_emr_EmailMessageId
Salesforce object EmailMessage does not contain column tt_emr_EmailMessagerelationId
Salesforce object EmailMessage does not contain column RelationId_orig
Salesforce object EmailMessage does not contain column RelationId_sup
Salesforce object EmailMessage does not contain column RelationType_orig
Salesforce object EmailMessage does not contain column sp_task_whoid
Salesforce object EmailMessage does not contain column tt_task_whoid
*/
select * from EmailMessage_Tritech_SFDC_load_replyto_VIP_Accounts
where error<>'Operation Successful.'




