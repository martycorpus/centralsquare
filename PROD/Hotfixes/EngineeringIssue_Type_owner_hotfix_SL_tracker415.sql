/*

select * from Superion_Production.dbo.EngineeringIssue__c
where migrated_record__C='true'
and  type__C is null --9998

select support_owner__C,* from Superion_Production.dbo.EngineeringIssue__c
where migrated_record__C='true'
and support_owner__C is  null --10129

select * from Superion_Production.dbo.[group]
where id in ('00G6A000000P27XUAS'
,'00G6A000000xGi9UAE')
select name,isactive,email,* from superion_production.dbo.[user]
where name like 'Gabe Beadel'
*/

--USE SUPERION_Production
--EXEC SF_Refresh 'SL_Superion_PROD','EngineeringIssue__c', Yes
--EXEC SF_Refresh 'SL_Superion_PROD','Case', Yes

use Staging_PROD
--drop table EngineeringIssue__c_update_Type_Owner_SL

declare @defaultowner nvarchar(18)=(select id from superion_production.dbo.[user]
where name like 'Gabe Beadel')

select a.id
,error=cast(space(255) as nvarchar(255))
,support_owner__C=isnull(support_owner__C, iif(b.ownerid like '00G%' and b.ownerid is not null,@defaultowner,b.ownerid ))
 ,type__C= isnull(type__C,'Defect')
 ,type__C_old=type__C
  ,support_owner__C_old=support_owner__C,
  Parent_Engineering_Issue__c_case=b.Parent_Engineering_Issue__c,
 b.OwnerId case_ownerid
 ,b.id case_id
 into EngineeringIssue__c_update_Type_Owner_SL
  from Superion_Production.dbo.EngineeringIssue__c a
 left outer join Superion_Production.dbo.[case] b
 on a.id=b.Parent_Engineering_Issue__c
where a.migrated_record__C='true'
and ( type__C is null  or support_owner__C is  null)--10150
--and a.support_owner__C is  null
--and b.ownerid is null;

select * from EngineeringIssue__c_update_Type_Owner_SL;--10150


-- exec SF_ColCompare 'Update','SL_Superion_prod','EngineeringIssue__c_update_Type_Owner_SL'

/*
Salesforce object EngineeringIssue__c does not contain column type__C_old
Salesforce object EngineeringIssue__c does not contain column support_owner__C_old
Salesforce object EngineeringIssue__c does not contain column Parent_Engineering_Issue__c_case
Salesforce object EngineeringIssue__c does not contain column case_ownerid
Salesforce object EngineeringIssue__c does not contain column case_id


*/

-- exec SF_Bulkops 'Update','SL_Superion_prod','EngineeringIssue__c_update_Type_Owner_SL'


select * from EngineeringIssue__c_update_Type_Owner_SL
where error<>'Operation successful.';


select * 
into EngineeringIssue__c_update_Type_Owner_SL_errors
from EngineeringIssue__c_update_Type_Owner_SL
where error<>'Operation successful.';

select * 
into EngineeringIssue__c_update_Type_Owner_SL_errors_bkp
from EngineeringIssue__c_update_Type_Owner_SL
where error<>'Operation successful.'

select * from  EngineeringIssue__c_update_Type_Owner_SL_errors;


-- exec SF_Bulkops 'Update','SL_Superion_prod','EngineeringIssue__c_update_Type_Owner_SL_errors'
