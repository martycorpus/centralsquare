
--Use superion_production
--EXEC SF_Refresh 'sl_superion_prod ','Bill_To_Ship_To__c','yes'


select * from Superion_Production.dbo.Bill_To_Ship_To__c where Move_to_accounting_system__c='True'
and (phone__C is null or phone__c='0') and Migrated_Record__c='True'

--Move_to_accounting_system__c
--false

--(999)999-9999
use Staging_PROD
go 

select id, ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
, Phone__c='(999)999-9999'
,Phone__c as phone_orig
, Move_to_accounting_system__c as Move_to_accounting_system__c_orig
 into Bill_To_Ship_To__c_Superion_Phone_Hotfix_SL_0904
 from Superion_Production.dbo.Bill_To_Ship_To__c where Move_to_accounting_system__c='True'
and (phone__C is null or phone__c='0') and Migrated_Record__c='True'

--(1296 row(s) affected)

select * from Bill_To_Ship_To__c_Superion_Phone_Hotfix_SL_0904


--1296


--Exec SF_ColCompare 'Update','sl_superion_prod ','Bill_To_Ship_To__c_Superion_Phone_Hotfix_SL_0904'


/*
Salesforce object Bill_To_Ship_To__c does not contain column phone_orig
Salesforce object Bill_To_Ship_To__c does not contain column Move_to_accounting_system__c_orig
*/

--Exec SF_BulkOps 'Update:batchsize(1)','sl_superion_prod ','Bill_To_Ship_To__c_Superion_Phone_Hotfix_SL_0904'

select error, count(*) from Bill_To_Ship_To__c_Superion_Phone_Hotfix_SL_0904 
group by error