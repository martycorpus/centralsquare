use Staging_PROD
go 
-- drop table Knowledge_article_Prod_Delete_ids
select a.id as DraftId,a.KnowledgeArticleId as Id ,a.title,a.urlname ,a.ArticleCaseAttachCount,a.ArticleTotalViewCount
into Knowledge_article_Prod_Delete_ids 
from superion_production.dbo.Article__kav a
inner join Staging_PROD.dbo.knowledgeArticle_Tritech_SFDC_Load b
on a.id = b.id 
inner join staging_sb_mapleroots.dbo.knowledgeArticle_Tritech_SFDC_Load_errors1 c
on b.KnowledgeArticleId_orig= c.KnowledgeArticleId_orig

--(79 row(s) affected)

select id from Knowledge_article_Prod_Delete_ids
use Staging_PROD
go 
-- drop table Knowledge_article_Prod_Delta_Insert_ids
select  ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255)),* 
				into Knowledge_article_Prod_Delta_Insert_ids
 from knowledgeArticle_Tritech_SFDC_preLoad a 
 where KnowledgeArticleId_orig in 
 (select KnowledgeArticleId_orig from staging_sb_mapleroots.dbo.knowledgeArticle_Tritech_SFDC_Load_errors1) 
 --and a.File__body__S is null and a.File__ContentType__S is not null


 select *
 --update a set a.file__body__s=null,a.file__Name__s=null,file__Contenttype__S=null
  from Knowledge_article_Prod_Delta_Insert_ids a where a.URLName='Inform-CAD-User-s-Guide-5-8'
  where a.File__body__S is null and a.File__ContentType__S is not null

 --ka02G000000QGJ8QAO
 select * from Article__ka
 select count(*) from staging_sb_mapleroots.dbo.knowledgeArticle_Tritech_SFDC_Load_errors1
 --79

select *

--update b set b.id=a.id ,b.error='Operation Successful.'
 from [KnowledgeArticle_hotfix_ids] a 
inner join Knowledge_article_Prod_Delta_Insert_ids b 
on a.id_orig = b.id_orig 


select * from Knowledge_article_Prod_Delta_Insert_ids

use Staging_PROD 
go
-- drop table DataCategories_KB_Tritech_SFDC_load_hotfix_79_inserts
select 
							DatacategoryName		= iif(api.Article_Category_API='zuercher_suite','zsuite',api.Article_Category_API)
							,DatacategoryName_orig = api.Article_Category_API
							,DataCategoryGroupName	= 'Products'
							,parentid				= a.id
							,DataCategoryLabel		= iif(c.newArticleCategory='zuercher_suite','zsuite',c.newArticleCategory)
							,DatacategoryLabel_orig = c.newarticlecategory
					into DataCategories_KB_Tritech_SFDC_load_hotfix_79_inserts
from Knowledge_article_Prod_Delta_Insert_ids a
,Tritech_KB_Article_Data_Categories_final c
,DataCategory_API_New_Category api
where a.ArticleNumber_orig=c.ArticleNumber 
and c.NewArticleCategory = api.Article_Category_Label
and a.id like 'ka%'


-- 105 inserts.
--(105 row(s) affected)


select * from DataCategories_KB_Tritech_SFDC_load_hotfix_79_inserts where DatacategoryName in ('911','zuercher suite')

