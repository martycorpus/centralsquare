/*
Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD ','Case','yes'
*/

 --For all cases that were migrated: if the Migrated value was = �Resolution Provided� , update the case status to �Closed�
 use Staging_PROD
 go 
 -- drop table Case_tritech_SFDC_Status_Closed_hotfix
 select id,ERROR	 =   CAST(SPACE(255) as NVARCHAR(255))
 ,isclosed as isclosed_orig,closeddate as closeddate_orig,status as status_orig 
 ,status='Closed'
 into Case_tritech_SFDC_Status_Closed_hotfix 
 from Superion_Production.dbo.[case] 
 where status='Resolution Provided' and Migrated_Record__c='True'


 --(9435 row(s) affected)
select * from Case_tritech_SFDC_Status_Closed_hotfix 


--Exec SF_colcompare 'Update','SL_SUPERION_PROD ','Case_tritech_SFDC_Status_Closed_hotfix'


--Exec SF_BulkOps 'Update:batchsize(200)','SL_SUPERION_PROD ','Case_tritech_SFDC_Status_Closed_hotfix'
/*
Salesforce object Case does not contain column isclosed_orig
Salesforce object Case does not contain column closeddate_orig
Salesforce object Case does not contain column status_orig
*/

select error, count(*) from Case_tritech_SFDC_Status_Closed_hotfix 
group by error