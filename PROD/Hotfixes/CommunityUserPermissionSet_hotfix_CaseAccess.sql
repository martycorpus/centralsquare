
use Staging_PROD;

-- drop table Contact_CommunityPermissions_hotfix_CaseAccess
select id, cast(null as nvarchar(255)) as error, 'Case Access;Delegated Admin Dedicated' as Community_Permissions__c
into Contact_CommunityPermissions_hotfix_CaseAccess
from Contact_CommunityPermissions_Load_RT
where Community_Permissions__c = 'Delegated Admin Dedicated'; --(4223 row(s) affected)

select * from Contact_CommunityPermissions_hotfix_CaseAccess;--4223

--exec SF_BulkOps 'Update','MC_SUPERION_PROD','Contact_CommunityPermissions_hotfix_CaseAccess'

--Performed by Sree

/*
--- Starting SF_BulkOps for Contact_CommunityPermissions_hotfix_CaseAccess V3.6.7
04:23:43: Run the DBAmp.exe program.
04:23:43: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
04:23:43: Updating Salesforce using Contact_CommunityPermissions_hotfix_CaseAccess (SQL01 / Staging_PROD) .
04:23:44: DBAmp is using the SQL Native Client.
04:23:44: SOAP Headers: 
04:24:21: 4223 rows read from SQL Table.
04:24:21: 4223 rows successfully processed.
04:24:21: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.





*/

----------------------------------------------------------------------------------
-- PermissionSetAssignment --this section is completed. MC 4/1
-----------------------------------------------------------------------------------
use Staging_PROD;

select cast(null as nchar(18)) as Id, 
       cast(null as nvarchar(255)) as Error,
	   u.id as AssigneeID,
	   u.name as AssigneeName,
	   '0PS6A000000M3FPWA0' as PermissionSetID,
	   'Community_Case_Access' as Superion_PermissionSet_Name
into PermissionSetAssignment_Insert_MC_hotfix_CaseAccess
from Contact_CommunityPermissions_hotfix_CaseAccess c
join superion_production.dbo.[user] u on u.ContactId = c.id --(4223 row(s) affected)

select * from PermissionSetAssignment_Insert_MC_hotfix_CaseAccess;


exec SF_BulkOps 'Insert','mc_superion_prod','PermissionSetAssignment_Insert_MC_hotfix_CaseAccess'
----- Starting SF_BulkOps for PermissionSetAssignment_Insert_MC_hotfix_CaseAccess V3.6.7
--18:11:51: Run the DBAmp.exe program.
--18:11:51: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
--18:11:51: Inserting PermissionSetAssignment_Insert_MC_hotfix_CaseAccess (SQL01 / Staging_PROD).
--18:11:52: DBAmp is using the SQL Native Client.
--18:11:53: SOAP Headers: 
--18:11:53: Warning: Column 'AssigneeName' ignored because it does not exist in the PermissionSetAssignment object.
--18:11:53: Warning: Column 'Superion_PermissionSet_Name' ignored because it does not exist in the PermissionSetAssignment object.
--18:16:50: 4223 rows read from SQL Table.
--18:16:50: 45 rows failed. See Error column of row for more information.
--18:16:50: 4178 rows successfully processed.
--18:16:50: Errors occurred. See Error column of row for more information.
--18:16:50: Percent Failed = 1.100.
--18:16:50: Error: DBAmp.exe was unsuccessful.
--18:16:50: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert PermissionSetAssignment_Insert_MC_hotfix_CaseAccess "SQL01"  "Staging_PROD"  "mc_superion_prod"  " " 
----- Ending SF_BulkOps. Operation FAILED.
--Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 41]
--SF_BulkOps Error: 18:11:51: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC18:11:51: Inserting PermissionSetAssignment_Insert_MC_hotfix_CaseAccess (SQL01 / Staging_PROD).18:11:52: DBAmp is using the SQL Native Client.18:11:53: SOAP Headers: 18:11:53: Warning: Column 'AssigneeName' ignored because it does not exist in the PermissionSetAssignment object.18:11:53: Warning: Column 'Superion_PermissionSet_Name' ignored because it does not exist in the PermissionSetAssignment object.18:16:50: 4223 rows read from SQL Table.18:16:50: 45 rows failed. See Error column of row for more information.18:16:50: 4178 rows successfully processed.18:16:50: Errors occurred. See Error column of row for more information.


SELECT ERROR, COUNT(*) FROM PermissionSetAssignment_Insert_MC_hotfix_CaseAccess
GROUP BY ERROR

Duplicate PermissionSetAssignment.  Assignee: 0052G000009INpP; Permission Set: 0PS6A000000M3FP