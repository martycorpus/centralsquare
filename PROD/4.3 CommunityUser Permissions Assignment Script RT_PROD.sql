/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: CommunityUser Permission Migration Script - Source System to SFDC
DEVELOPER	: RTECSON
CREATED DT  : 02/06/2019
DETAIL		: 	
										
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/06/2019		Ron Tecson			Initial. Copied and modified from 4.3 CommunityUser Permissions Assignment Script PB.sql
02/07/2019		Ron Tecson			Corrected the refreshes of the local database.
02/27/2019		Ron Tecson			
03/28/2019		Ron Tecson			Repoint to Production

DECISION	:
*********************************************************************************************************************************
3/18		- For Mapleroots and Production, all Community Users will have a permission sets of "Community Delegated Admin_Login"


DATE			ERROR																		RESOLUTION
===============	===========================================================================	==================================================================================
02/27/2019		REQUIRED_FIELD_MISSING:Required fields are missing: [FeedItemId]:FeedItemId Needed to update the Id column of the load table after re-running the errors from
																							15.3 Chatter FeedItem Migration RT_MR.sql

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE SUPERION_Production
Exec SF_Refresh 'RT_Superion_PROD', 'Profile', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'Contact', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'User', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'PermissionSet', 'yes';
Exec SF_Refresh 'RT_Superion_PROD', 'PermissionSetAssignment', 'yes';

USE Tritech_PROD
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Case', 'yes'


---------------------------------------------------------------------------------
-- DROP STAGING TABLE IF EXISTS 
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='PermissionSetAssignment_PreLoad_RT') 
	DROP TABLE Staging_PROD.dbo.[PermissionSetAssignment_PreLoad_RT];


---------------------------------------------------------------------------------
-- CREATE PERMISSION SET ASSIGNMENT STAGING TABLE 
---------------------------------------------------------------------------------

SELECT
	CAST('' as nvarchar(18)) as ID,
	CAST('' as nvarchar(255)) as Error,
	a.Legacy_Tritech_ID__c as AssigneeID_Original,
	a.ID as AssigneeID,
	SrcProf.[Name] as PermissionSetID_original,
	Perm.ID as PermissionSetID
	INTO PermissionSetAssignment_PreLoad_RT
	FROM SUPERION_Production.dbo.[User] a
	LEFT OUTER JOIN TriTech_Prod.dbo.[user] SrcUser ON a.Legacy_Tritech_Id__c = SrcUser.ID																							----> Source User
	LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] SrcProf ON SrcUser.ProfileId = SrcProf.ID																							----> Source Profile
	LEFT OUTER JOIN SUPERION_Production.dbo.[PermissionSet] Perm ON Perm.[Name] =																										----> Permission Set
																			CASE SrcProf.[Name] WHEN 'TriTech Portal Read Only with Tickets' THEN 'Community_Case_Access_Readonly'
																								WHEN 'TriTech Portal Standard User' THEN 'Community_Case_Access'
																								WHEN 'TriTech Portal Manager' THEN 'Community_Delegated_Admin_Dedicated'
																								ELSE '' END
	WHERE migrated_record__c = 'true'
	and SrcProf.[Name] in ('TriTech Portal Read Only with Tickets', 'TriTech Portal Standard User', 'TriTech Portal Manager')

-- 03/29 -- PROD: (8214 rows affected)
-- 03/18 -- (8306 rows affected)

select permissionSetId, count(*) from PermissionSetAssignment_PreLoad_RT
group by permissionSetId



---------------------------------------------------------------------------------
-- Set related community users to 'Active'
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='USER_Update_toActive_RT') 
	DROP TABLE Staging_PROD.dbo.[USER_Update_toActive_RT];

Select AssigneeID as ID,
CAST('' as nvarchar(255)) as error,
'true' as isActive
INTO Staging_PROD.dbo.USER_Update_toActive_RT
From Staging_PROD.dbo.PermissionSetAssignment_PreLoad_RT

Alter table Staging_PROD.dbo.USER_Update_toActive_RT
Add [Sort] int identity (1,1)

/*
select * from 
USER_Update_toActive_RT where error not like '%success%'
Id = '0050r000000DsyJAAS'

select * from User_CommunityUser_Load_RT where ID = '0050r000000DsyJAAS'

select T3.ID, T3.ProfileId, t4.Name from USER_Update_toActive_RT T1
INNER JOIN PermissionSetAssignment_PreLoad_RT T2 ON T1.Id = T2.AssigneeId
inner join User_CommunityUser_Load_RT T3 ON T3.Id = T2.AssigneeId 
Inner join RT_Superion_Mapleroots...[Profile] T4 ON T3.ProfileId = T4.Id
where T1.Error not like '%success%'

select * from PermissionSetAssignment_PreLoad_RT where AssigneeId = '0050r000000DsyJAAS'

0050r000000DsyJAAS
*/

USE Staging_Prod

Exec SF_BulkOps 'update:bulkapi,batchsize(800)', 'RT_Superion_PROD', 'USER_Update_toActive_RT'

/**************************** L O G ***************************************************************

-- 3/29 -- in Production 10 mins.

--- Starting SF_BulkOps for USER_Update_toActive_RT V3.6.7
13:21:15: Run the DBAmp.exe program.
13:21:15: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
13:21:15: Updating Salesforce using USER_Update_toActive_RT (SQL01 / Staging_PROD) .
13:21:16: DBAmp is using the SQL Native Client.
13:21:16: Batch size reset to 800 rows per batch.
13:21:17: Sort column will be used to order input rows.
13:21:17: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
13:21:17: Job 7502G000009dOP4QAM created.
13:21:17: Batch 7512G00000G5MMNQA3 created with 800 rows.
13:21:17: Batch 7512G00000G5MLzQAN created with 800 rows.
13:21:17: Batch 7512G00000G5MMSQA3 created with 800 rows.
13:21:18: Batch 7512G00000G5MMXQA3 created with 800 rows.
13:21:18: Batch 7512G00000G5MMcQAN created with 800 rows.
13:21:18: Batch 7512G00000G5MMhQAN created with 800 rows.
13:21:18: Batch 7512G00000G5MMmQAN created with 800 rows.
13:21:18: Batch 7512G00000G5MMrQAN created with 800 rows.
13:21:18: Batch 7512G00000G5MIvQAN created with 800 rows.
13:21:18: Batch 7512G00000G5MMwQAN created with 800 rows.
13:21:18: Batch 7512G00000G5MN1QAN created with 214 rows.
13:21:18: Job submitted.
13:21:18: 8214 rows read from SQL Table.
13:21:19: Job still running.
13:21:34: Job still running.
13:22:36: Job still running.
13:23:38: Job still running.
13:24:38: Job still running.
13:25:39: Job still running.
13:26:40: Job still running.
13:27:41: Job still running.
13:28:41: Job still running.
13:29:43: Job still running.
13:30:44: Job Complete.
13:30:44: DBAmp is using the SQL Native Client.
13:30:45: 8214 rows successfully processed.
13:30:45: 0 rows failed.
13:30:45: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


-- 3/18 -- 10 Mins
--- Starting SF_BulkOps for USER_Update_toActive_RT V3.6.7
20:01:23: Run the DBAmp.exe program.
20:01:23: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
20:01:23: Updating Salesforce using USER_Update_toActive_RT (SQL01 / Staging_SB_MapleRoots) .
20:01:24: DBAmp is using the SQL Native Client.
20:01:24: Batch size reset to 800 rows per batch.
20:01:24: Sort column will be used to order input rows.
20:01:24: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
20:01:26: Job 7500r00000002ryAAA created.
20:01:26: Batch 7510r00000005NwAAI created with 800 rows.
20:01:26: Batch 7510r00000005O1AAI created with 800 rows.
20:01:27: Batch 7510r00000005O6AAI created with 800 rows.
20:01:27: Batch 7510r00000005OBAAY created with 800 rows.
20:01:27: Batch 7510r00000005OGAAY created with 800 rows.
20:01:27: Batch 7510r00000005OLAAY created with 800 rows.
20:01:27: Batch 7510r00000005OQAAY created with 800 rows.
20:01:27: Batch 7510r00000005OVAAY created with 800 rows.
20:01:27: Batch 7510r00000005OaAAI created with 800 rows.
20:01:28: Batch 7510r00000005OfAAI created with 800 rows.
20:01:28: Batch 7510r00000005OkAAI created with 306 rows.
20:01:29: Job submitted.
20:01:29: 8306 rows read from SQL Table.
20:01:29: Job still running.
20:01:46: Job still running.
20:02:48: Job still running.
20:03:51: Job still running.
20:04:51: Job still running.
20:05:53: Job still running.
20:06:54: Job still running.
20:07:56: Job still running.
20:08:58: Job still running.
20:10:01: Job Complete.
20:10:01: DBAmp is using the SQL Native Client.
20:10:03: 8306 rows successfully processed.
20:10:03: 0 rows failed.
20:10:03: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

-- 2/27
--- Starting SF_BulkOps for USER_Update_toActive_RT V3.6.7
18:53:09: Run the DBAmp.exe program.
18:53:09: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
18:53:09: Updating Salesforce using USER_Update_toActive_RT (SQL01 / Staging_SB_MapleRoots) .
18:53:10: DBAmp is using the SQL Native Client.
18:53:10: Batch size reset to 800 rows per batch.
18:53:10: Sort column will be used to order input rows.
18:53:10: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
18:53:12: Job 7501h000001x6u2AAA created.
18:53:12: Batch 7511h000001vBqLAAU created with 800 rows.
18:53:12: Batch 7511h000001vBqQAAU created with 800 rows.
18:53:12: Batch 7511h000001vBqVAAU created with 800 rows.
18:53:12: Batch 7511h000001vBqaAAE created with 800 rows.
18:53:12: Batch 7511h000001vBqfAAE created with 800 rows.
18:53:12: Batch 7511h000001vBqkAAE created with 800 rows.
18:53:12: Batch 7511h000001vBqpAAE created with 800 rows.
18:53:13: Batch 7511h000001vBquAAE created with 800 rows.
18:53:13: Batch 7511h000001vBqzAAE created with 800 rows.
18:53:13: Batch 7511h000001vBr4AAE created with 800 rows.
18:53:13: Batch 7511h000001vBr9AAE created with 251 rows.
18:53:13: Job submitted.
18:53:13: 8251 rows read from SQL Table.
18:53:14: Job still running.
18:53:32: Job still running.
18:54:35: Job still running.
18:55:38: Job still running.
18:56:40: Job still running.
18:57:43: Job still running.
18:58:46: Job still running.
18:59:49: Job still running.
19:00:49: Job still running.
19:01:50: Job still running.
19:02:51: Job still running.
19:03:52: Job Complete.
19:03:52: DBAmp is using the SQL Native Client.
19:03:53: 6360 rows successfully processed.
19:03:53: 1891 rows failed.
19:03:53: Errors occurred. See Error column of row and above messages for more information.
19:03:53: Percent Failed = 22.900.
19:03:53: Error: DBAmp.exe was unsuccessful.
19:03:53: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe update:bulkapi,batchsize(800) USER_Update_toActive_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 85]
SF_BulkOps Error: 18:53:09: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC18:53:09: Updating Salesforce using USER_Update_toActive_RT (SQL01 / Staging_SB_MapleRoots) .18:53:10: DBAmp is using the SQL Native Client.18:53:10: Batch size reset to 800 rows per batch.18:53:10: Sort column will be used to order input rows.18:53:10: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.18:53:12: Job 7501h000001x6u2AAA created.18:53:12: Batch 7511h000001vBqLAAU created with 800 rows.18:53:12: Batch 7511h000001vBqQAAU created with 800 rows.18:53:12: Batch 7511h000001vBqVAAU created with 800 rows.18:53:12: Batch 7511h000001vBqaAAE created with 800 rows.18:53:12: Batch 7511h000001vBqfAAE created with 800 rows.18:53:12: Batch 7511h000001vBqkAAE created with 800 rows.18:53:12: Batch 7511h000001vBqpAAE created with 800 rows.18:53:13: Batch 7511h000001vBquAAE created with 800 rows.18:53:13: Batch 7511h000001vBqzAAE created with 800 rows.18:53:13: Batch 7511h000001vBr4AAE created with 800 rows.18:53:13: Batch 7511h000001vBr9AAE created with 251 rows.18:53:13: Job submitted.18:53:13: 8251 rows read from SQL Table.18:53:14: Job still running.18:53:32: Job still running.18:54:35: Job still running.18:55:38: Job still running.18:56:40: Job still running.18:57:43: Job still running.18:58:46: Job still running.18:59:49: Job still running.19:00:49: Job still running.19:01:50: Job still running.19:02:51: Job still running.19:03:52: Job Complete.19:03:52: DBAmp is using the SQL Native Client.19:03:53: 6360 rows successfully processed.19:03:53: 1891 rows failed.19:03:53: Errors occurred. See Error column of row and above messages for more information.

**************************** L O G ***************************************************************/

--Select * from USER_Update_toActive_RT where error not like '%success%'

Select Id, Cast('' as nvarchar(255)) as Error, '00e6A000000MgNwQAK' AS ProfileId  INTO USER_Update_UserProfile_RT from USER_Update_toActive_RT where error not like '%success%'

Exec SF_BulkOps 'UPDATE','RT_Superion_PROD', 'USER_Update_UserProfile_RT' --'User_CommunityUser_Error_Load_Limit_Exceeded' --'User_CommunityUser_Error_Load_Unable_To_Obtain_2'


---------------------------------------------------------------------------------
-- Drop Load Table if it exists
---------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='PermissionSetAssignment_Load_RT') 
	DROP TABLE Staging_PROD.dbo.PermissionSetAssignment_Load_RT;

SELECT *
INTO Staging_PROD.dbo.PermissionSetAssignment_Load_RT
FROM PermissionSetAssignment_PreLoad_RT

select Id, Name from RT_SUPERION_PROD...PermissionSet where Name = 
'Community_Delegated_Admin_Login'

select * from PermissionSetAssignment_Load_RT

update PermissionSetAssignment_Load_RT
set PermissionSetId = '0PS6A000000M3FRWA0'

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE Staging_PROD.dbo.PermissionSetAssignment_Load_RT
ADD [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_PROD

EXEC SF_BulkOps 'insert:bulkapi,batchsize(800)', 'RT_Superion_PROD', 'PermissionSetAssignment_Load_RT';

/*************************************************************************************

--03/29 - 6 mins in Production
--- Starting SF_BulkOps for PermissionSetAssignment_Load_RT V3.6.7
13:35:17: Run the DBAmp.exe program.
13:35:17: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
13:35:17: Inserting PermissionSetAssignment_Load_RT (SQL01 / Staging_PROD).
13:35:18: DBAmp is using the SQL Native Client.
13:35:18: Batch size reset to 800 rows per batch.
13:35:18: Sort column will be used to order input rows.
13:35:18: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
13:35:18: Warning: Column 'AssigneeID_Original' ignored because it does not exist in the PermissionSetAssignment object.
13:35:18: Warning: Column 'PermissionSetID_original' ignored because it does not exist in the PermissionSetAssignment object.
13:35:19: Job 7502G000009dOP9QAM created.
13:35:19: Batch 7512G00000G5MN6QAN created with 800 rows.
13:35:19: Batch 7512G00000G5MNBQA3 created with 800 rows.
13:35:19: Batch 7512G00000G5MNGQA3 created with 800 rows.
13:35:19: Batch 7512G00000G5MNLQA3 created with 800 rows.
13:35:19: Batch 7512G00000G5MNQQA3 created with 800 rows.
13:35:19: Batch 7512G00000G5MNVQA3 created with 800 rows.
13:35:19: Batch 7512G00000G5MNaQAN created with 800 rows.
13:35:19: Batch 7512G00000G5MNfQAN created with 800 rows.
13:35:19: Batch 7512G00000G5MNkQAN created with 800 rows.
13:35:19: Batch 7512G00000G5MNpQAN created with 800 rows.
13:35:20: Batch 7512G00000G5MNuQAN created with 214 rows.
13:35:20: Job submitted.
13:35:20: 8214 rows read from SQL Table.
13:35:20: Job still running.
13:35:36: Job still running.
13:36:37: Job still running.
13:37:37: Job still running.
13:38:37: Job still running.
13:39:38: Job still running.
13:40:39: Job Complete.
13:40:39: DBAmp is using the SQL Native Client.
13:40:45: 3991 rows successfully processed.
13:40:45: 4223 rows failed.
13:40:45: Errors occurred. See Error column of row and above messages for more information.
13:40:45: Percent Failed = 51.400.
13:40:45: Error: DBAmp.exe was unsuccessful.
13:40:45: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:bulkapi,batchsize(800) PermissionSetAssignment_Load_RT "SQL01"  "Staging_PROD"  "RT_Superion_PROD"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 288]
SF_BulkOps Error: 13:35:17: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC13:35:17: Inserting PermissionSetAssignment_Load_RT (SQL01 / Staging_PROD).13:35:18: DBAmp is using the SQL Native Client.13:35:18: Batch size reset to 800 rows per batch.13:35:18: Sort column will be used to order input rows.13:35:18: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.13:35:18: Warning: Column 'AssigneeID_Original' ignored because it does not exist in the PermissionSetAssignment object.13:35:18: Warning: Column 'PermissionSetID_original' ignored because it does not exist in the PermissionSetAssignment object.13:35:19: Job 7502G000009dOP9QAM created.13:35:19: Batch 7512G00000G5MN6QAN created with 800 rows.13:35:19: Batch 7512G00000G5MNBQA3 created with 800 rows.13:35:19: Batch 7512G00000G5MNGQA3 created with 800 rows.13:35:19: Batch 7512G00000G5MNLQA3 created with 800 rows.13:35:19: Batch 7512G00000G5MNQQA3 created with 800 rows.13:35:19: Batch 7512G00000G5MNVQA3 created with 800 rows.13:35:19: Batch 7512G00000G5MNaQAN created with 800 rows.13:35:19: Batch 7512G00000G5MNfQAN created with 800 rows.13:35:19: Batch 7512G00000G5MNkQAN created with 800 rows.13:35:19: Batch 7512G00000G5MNpQAN created with 800 rows.13:35:20: Batch 7512G00000G5MNuQAN created with 214 rows.13:35:20: Job submitted.13:35:20: 8214 rows read from SQL Table.13:35:20: Job still running.13:35:36: Job still running.13:36:37: Job still running.13:37:37: Job still running.13:38:37: Job still running.13:39:38: Job still running.13:40:39: Job Complete.13:40:39: DBAmp is using the SQL Native Client.13:40:45: 3991 rows successfully processed.13:40:45: 4223 rows failed.13:40:45: Errors occurred. See Error column of row and above messages for more information.


--3/18 -- 10 mins
--- Starting SF_BulkOps for PermissionSetAssignment_Load_RT V3.6.7
20:20:16: Run the DBAmp.exe program.
20:20:16: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
20:20:16: Inserting PermissionSetAssignment_Load_RT (SQL01 / Staging_SB_MapleRoots).
20:20:17: DBAmp is using the SQL Native Client.
20:20:17: Batch size reset to 800 rows per batch.
20:20:18: Sort column will be used to order input rows.
20:20:18: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
20:20:18: Warning: Column 'AssigneeID_Original' ignored because it does not exist in the PermissionSetAssignment object.
20:20:18: Warning: Column 'PermissionSetID_original' ignored because it does not exist in the PermissionSetAssignment object.
20:20:19: Job 7500r00000002s3AAA created.
20:20:19: Batch 7510r00000005OpAAI created with 800 rows.
20:20:19: Batch 7510r00000005OuAAI created with 800 rows.
20:20:19: Batch 7510r00000005OzAAI created with 800 rows.
20:20:19: Batch 7510r00000005P4AAI created with 800 rows.
20:20:19: Batch 7510r00000005P9AAI created with 800 rows.
20:20:19: Batch 7510r00000005PEAAY created with 800 rows.
20:20:20: Batch 7510r00000005PJAAY created with 800 rows.
20:20:20: Batch 7510r00000005POAAY created with 800 rows.
20:20:20: Batch 7510r00000005PTAAY created with 800 rows.
20:20:20: Batch 7510r00000005PYAAY created with 800 rows.
20:20:20: Batch 7510r00000005PdAAI created with 306 rows.
20:20:21: Job submitted.
20:20:21: 8306 rows read from SQL Table.
20:20:21: Job still running.
20:20:37: Job still running.
20:21:38: Job still running.
20:22:38: Job still running.
20:23:39: Job still running.
20:24:40: Job still running.
20:25:41: Job still running.
20:26:41: Job Complete.
20:26:42: DBAmp is using the SQL Native Client.
20:26:43: 8306 rows successfully processed.
20:26:43: 0 rows failed.
20:26:43: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.



--- Starting SF_BulkOps for PermissionSetAssignment_Load_RT V3.6.7
21:01:38: Run the DBAmp.exe program.
21:01:38: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
21:01:38: Inserting PermissionSetAssignment_Load_RT (SQL01 / Staging_SB_MapleRoots).
21:01:38: DBAmp is using the SQL Native Client.
21:01:38: Batch size reset to 800 rows per batch.
21:01:39: Sort column will be used to order input rows.
21:01:39: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
21:01:39: Warning: Column 'AssigneeID_Original' ignored because it does not exist in the PermissionSetAssignment object.
21:01:39: Warning: Column 'PermissionSetID_original' ignored because it does not exist in the PermissionSetAssignment object.
21:01:39: Job 7501h000001x7J2AAI created.
21:01:39: Batch 7511h000001vCNAAA2 created with 800 rows.
21:01:40: Batch 7511h000001vCNFAA2 created with 800 rows.
21:01:40: Batch 7511h000001vCNKAA2 created with 800 rows.
21:01:40: Batch 7511h000001vCNPAA2 created with 800 rows.
21:01:40: Batch 7511h000001vCNUAA2 created with 800 rows.
21:01:40: Batch 7511h000001vCMmAAM created with 800 rows.
21:01:40: Batch 7511h000001vCNZAA2 created with 800 rows.
21:01:40: Batch 7511h000001vCNeAAM created with 800 rows.
21:01:40: Batch 7511h000001vCNjAAM created with 800 rows.
21:01:40: Batch 7511h000001vCNoAAM created with 800 rows.
21:01:40: Batch 7511h000001vCNtAAM created with 251 rows.
21:01:41: Job submitted.
21:01:41: 8251 rows read from SQL Table.
21:01:42: Job still running.
21:01:58: Job still running.
21:02:59: Job still running.
21:03:59: Job still running.
21:05:00: Job still running.
21:06:01: Job still running.
21:07:01: Job still running.
21:08:02: Job still running.
21:09:03: Job still running.
21:10:03: Job still running.
21:11:04: Job Complete.
21:11:05: DBAmp is using the SQL Native Client.
21:11:05: 4203 rows successfully processed.
21:11:05: 4048 rows failed.
21:11:05: Errors occurred. See Error column of row and above messages for more information.
21:11:05: Percent Failed = 49.100.
21:11:05: Error: DBAmp.exe was unsuccessful.
21:11:05: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:bulkapi,batchsize(800) PermissionSetAssignment_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_MAPLEROOTS"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 170]
SF_BulkOps Error: 21:01:38: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC21:01:38: Inserting PermissionSetAssignment_Load_RT (SQL01 / Staging_SB_MapleRoots).21:01:38: DBAmp is using the SQL Native Client.21:01:38: Batch size reset to 800 rows per batch.21:01:39: Sort column will be used to order input rows.21:01:39: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.21:01:39: Warning: Column 'AssigneeID_Original' ignored because it does not exist in the PermissionSetAssignment object.21:01:39: Warning: Column 'PermissionSetID_original' ignored because it does not exist in the PermissionSetAssignment object.21:01:39: Job 7501h000001x7J2AAI created.21:01:39: Batch 7511h000001vCNAAA2 created with 800 rows.21:01:40: Batch 7511h000001vCNFAA2 created with 800 rows.21:01:40: Batch 7511h000001vCNKAA2 created with 800 rows.21:01:40: Batch 7511h000001vCNPAA2 created with 800 rows.21:01:40: Batch 7511h000001vCNUAA2 created with 800 rows.21:01:40: Batch 7511h000001vCMmAAM created with 800 rows.21:01:40: Batch 7511h000001vCNZAA2 created with 800 rows.21:01:40: Batch 7511h000001vCNeAAM created with 800 rows.21:01:40: Batch 7511h000001vCNjAAM created with 800 rows.21:01:40: Batch 7511h000001vCNoAAM created with 800 rows.21:01:40: Batch 7511h000001vCNtAAM created with 251 rows.21:01:41: Job submitted.21:01:41: 8251 rows read from SQL Table.21:01:42: Job still running.21:01:58: Job still running.21:02:59: Job still running.21:03:59: Job still running.21:05:00: Job still running.21:06:01: Job still running.21:07:01: Job still running.21:08:02: Job still running.21:09:03: Job still running.21:10:03: Job still running.21:11:04: Job Complete.21:11:05: DBAmp is using the SQL Native Client.21:11:05: 4203 rows successfully processed.21:11:05: 4048 rows failed.21:11:05: Errors occurred. See Error column of row and above messages for more information.


*************************************************************************************/

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

SELECT ERROR, count(*) 
FROM PermissionSetAssignment_Load_RT
GROUP BY ERROR

SELECT error --INTO PermissionSetAssignment_Update_From_Error_RT 
FROM  PermissionSetAssignment_Load_RT WHERE error NOT LIKE '%success%'

SELECT '''' + AssigneeID + ''',' --INTO PermissionSetAssignment_Update_From_Error_RT 
FROM  PermissionSetAssignment_Load_RT WHERE error NOT LIKE '%success%'

SELECT * FROM  PermissionSetAssignment_Load_RT WHERE error LIKE '%success%'

select LastName, FirstName, ProfileId from  RT_Superion_PROD...[User] where Id = '0052G000009IOLcQAO'

select id, name, * from [RT_SUPERION_PROD]...[permissionset] -- where id = '0PS6A000000M3FQWA0' (Community_Delegated_Admin_Dedicated)
where name like '%community%'

select Id, NAME from [RT_SUPERION_PROD]...permissionset where name like '%login%'

select * INTO PermissionSetAssignment_Load_RT_HF
from PermissionSetAssignment_Load_RT
where error not like '%Operation Successful.%'

update PermissionSetAssignment_Load_RT_HF
set PermissionSetId = '0PS6A000000M3FRWA0'
where error not like '%success%'

EXEC SF_BulkOps 'insert:bulkapi,batchsize(800)', 'RT_Superion_PROD', 'PermissionSetAssignment_Load_RT_HF';

0PS6A000000M3FQWA0
0PS6A000000M3FQWA0	Community_Delegated_Admin_Dedicated
0PS6A000000M3FRWA0	Community_Delegated_Admin_Login

update PermissionSetAssignment_Update_From_Error_RT
set PermissionSetID = '0PS6A000000M3FRWA0'

EXEC SF_BulkOps 'insert:bulkapi,batchsize(800)', 'RT_Superion_PROD', 'PermissionSetAssignment_Update_From_Error_RT';

select * from PermissionSetAssignment_Update_From_Error_RT

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE SUPERION_Production

Exec SF_Refresh 'RT_Superion_PROD', 'PermissionSetAssignment', 'yes';

---------------------------------------------------------------------------------
-- Set related community users to 'inActive'
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='USER_Update_toInActive_RT') 
	DROP TABLE Staging_PROD.dbo.[USER_Update_toInActive_RT];

Select AssigneeID as ID,
CAST('' as nvarchar(255)) as error,
'false' as isActive
INTO Staging_PROD.dbo.USER_Update_toInActive_RT
From Staging_PROD.dbo.PermissionSetAssignment_Load_RT

Alter table Staging_PROD.dbo.User_Update_toInActive_RT
Add [Sort] int identity (1,1)

USE Staging_PROD

Exec SF_BulkOps 'update:bulkapi,batchsize(800)', 'RT_Superion_PROD', 'User_Update_toInActive_RT'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE SUPERION_Production

Exec SF_Refresh 'RT_Superion_PROD', 'PermissionSetAssignment', 'yes';

--Select * from User_Update_toActive_PB where error not like '%success%'