/*
-- Author     : Shivani Mogullapalli
-- Date       : 06/11/2018
-- Description: Migrate the Case data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-0022
Scope:
All Case Comments related to a migrated case will be migrated to the Superion instance.
Migrate the case comments as Chatter comments to case
*/

--use Tritech_Prod
--EXEC SF_Refresh 'MC_Tritech_PROD','CaseComment','yes'
-- execution time : 1 min 

select count(*) from Tritech_PROD.dbo.CaseComment
--2484746

select count(*) from MC_Tritech_PROD...casecomment 


use Staging_PROD 
go 
-- drop table feedItem_Tritech_SFDC_CaseComment_Preload


declare @defaultuser nvarchar(18)
 select @defaultuser = id from SUPERION_PRODUCTION.dbo.[user] where name = 'CentralSquare API'
SELECT 
         ID											 =   CAST(SPACE(18) as NVARCHAR(18))
		,ERROR										 =   CAST(SPACE(255) as NVARCHAR(255))
		,Body										 =	 Tr_Cmt.[CommentBody]
		,CreatedbyId_orig							 =	 Tr_Cmt.[CreatedById]
		,CreatedById								 =   iif(Target_Create.Id is null,@defaultuser,Target_Create.Id)
		,CreatedDate								 =	 Tr_Cmt.[CreatedDate]
		,Id_orig									 =   Tr_Cmt.[Id]
														  --,Tr_Cmt.[IsDeleted]
		,IsNotificationSelected						 = Tr_Cmt.[IsNotificationSelected]
		,Visibility								 = iif(Tr_Cmt.[IsPublished] ='True' ,'AllUsers','InternalUsers')
		,IsPublished_orig							=tr_Cmt.IsPublished
														  --,Tr_Cmt.[LastModifiedById]
														  --,Tr_Cmt.[LastModifiedDate]
				,Parentid_orig						=   Tr_Cmt.[ParentId]
				,Parentid							=   Target_Parent.ID
														  --,Tr_Cmt.[SystemModstamp]
				into feedItem_Tritech_SFDC_CaseComment_preload
  FROM [Tritech_PROD].[dbo].[CaseComment] Tr_Cmt
  inner join SUPERION_PRODUCTION.dbo.[case] Target_Parent
  on Target_Parent.Legacy_ID__c=Tr_cmt.ParentId
  left join SUPERION_PRODUCTION.dbo.[User] Target_Create
  on Target_create.Legacy_Tritech_Id__c=Tr_Cmt.CreatedById
--(952614 row(s) affected)
--2:30 mins



select count(*) from
feedItem_Tritech_SFDC_CaseComment_preload

--952614

select id_orig, count(*)
from feedItem_Tritech_SFDC_CaseComment_preload
group by id_orig 
having count(*)>1

-- drop table feedItem_Tritech_SFDC_CaseComment_load
select * 
into feedITem_Tritech_SFDC_CaseComment_load
from feedItem_Tritech_SFDC_CaseComment_preload

-- (952614 row(s) affected)

select * from feedITem_Tritech_SFDC_CaseComment_load

-- insert Casecomment records as Chatter comment.

-- EXEC SF_ColCompare 'Insert','SL_SUPERION_PROD ', 'feedITem_Tritech_SFDC_CaseComment_load' 

-- EXEC SF_BulkOps 'Insert','SL_SUPERION_PROD ','feedItem_Tritech_SFDC_CaseComment_load' 

/*
Salesforce object feedITem does not contain column CreatedbyId_orig
Salesforce object feedITem does not contain column Id_orig
Salesforce object feedITem does not contain column IsNotificationSelected
Salesforce object feedITem does not contain column IsPublished_orig
Salesforce object feedITem does not contain column Parentid_orig
*/

select error, count(*)
from feedItem_Tritech_SFDC_CaseComment_load
group by error 

select * from feedItem_Tritech_SFDC_CaseComment_load 
where error <>'Operation Successful.'

select error, count(*)
from feedItem_Tritech_SFDC_CaseComment_load
group by error 

-- drop table feedItem_Tritech_SFDC_CaseComment_load_errors
select *
into feedItem_Tritech_SFDC_CaseComment_load_errors
from feedItem_Tritech_SFDC_CaseComment_load
where error <>'Operation Successful.' and error not like'Required%'
-- (999 row(s) affected)


-- drop table feedItem_Tritech_SFDC_CaseComment_load_errors_bkp
select *
into feedItem_Tritech_SFDC_CaseComment_load_errors_bkp
from feedItem_Tritech_SFDC_CaseComment_load
where error <>'Operation Successful.'
-- (1617 row(s) affected)


-- EXEC SF_BulkOps 'Insert:batchsize(1)','SL_SUPERION_PROD ','feedItem_Tritech_SFDC_CaseComment_load_errors' 


select error, count(*) from feedItem_Tritech_SFDC_CaseComment_load
group by error 


select *
-- delete 
from feedItem_Tritech_SFDC_CaseComment_load where error<>'Operation Successful.'
--(1617 row(s) affected)


-- insert into  feedItem_Tritech_SFDC_CaseComment_load
select * from feedItem_Tritech_SFDC_CaseComment_load_errors 

-- 999 

-- insert into  feedItem_Tritech_SFDC_CaseComment_load
select * from feedItem_Tritech_SFDC_CaseComment_load_errors_bkp where error like 'Required fields%'
--(618 row(s) affected)

select error, count(*) from feeditem_Tritech_SFDC_casecomment_load 
group by error

select count(*) from feeditem_Tritech_SFDC_casecomment_load

--941662





