/*
-- Author       : Shivani Mogullapalli
-- Created Date : 22/1/2019
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Opportunity----------
Requirement No  : 
Total Records   :  
Scope: Create Superion opportunites as Procurement Activity records.
 */

 /*
Use SUPERION_PRODUCTION
EXEC sf_refresh 'SL_SUPERION_PROD ','Opportunity','yes'
EXEC SF_Refresh 'SL_SUPERION_PROD ','Procurement_Activity__C','yes'
*/ 
 --select count(*) from SUPERION_PRODUCTION.dbo.Opportunity where Legacy_Source_System__c is null
 --select count(*) from SL_SUPERION_PROD ...Opportunity where Legacy_Source_System__c<>'Tritech'
 --33843


Use Staging_PROD ;

 --Drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload;

 Select  
								 ID = CAST(SPACE(18) as NVARCHAR(18))
								,ERROR = CAST(SPACE(255) as NVARCHAR(255))
								,Migrated_Record__c  ='True'  
								,RFPStatus__c_orig = RFPStatus__c
								,ROW_NUMBER() OVER(partition by opp.id order by iif(isnull(isactive ,'false')='true',1,2)) as rownum
								,Proposal_Stage__c = iif(RFPStatus__c='Under Review' ,'Bid Review',RFPStatus__c)
								,Procurement_Type__c = RFP_Type__c
								,Proposal_Manager__c_orig = Proposal_Specialist__c
								,Proposal_manager__c = mgrusr.id
								,Request_Received__c = Receipt_Date__c
								,Response_Due_Date__c = Due_Date__c
								,Letter_of_Intent_Completed__c_orig = Letter_of_Intent_Completed__c
								,Letter_of_Intent_Required__c = iif(Letter_of_Intent_Completed__c='yes',1,0)
								,Letter_of_Intent_Due_Date__c = Letter_of_Intent__c
								,Deadline_for_Vendor_Questions__c = Question_Deadline__c
								,Bidder_s_Conference_Date__c = Date_of_Bidder_s_Conference__c
								,Bidder_s_Conference__c_orig = Bidder_s_Conference__c
								,Bidder_s_Conference_Required__c = iif(Bidder_s_Conference__c ='yes',1,0)
								,Bid_Bond_Required__c_orig = Bid_Bond_Required__c
								,Bid_Bond_Required__c = iif(Bid_Bond_Required__c='yes',1,0)
								,Proposal_Manager_Notes__c = RFP_Summary__c
								,Response_Ship_Date__c = Date_Shipped__c
								,Overall_Fit__c = replace(Fit_Analysis__c,'%','')
								,CAD_Fit__c = replace(CAD__c,'%','')
								,RMS_Fit__c = replace(RMS__c,'%','')
								,JMS_Fit__c = replace(JMS__c,'%','')
								,Mobile_Fit__c = replace(Mobile__c,'%','')
								,If_Consultant_Other__c = Consultant_Company__c
								,Common_Partners__c = Third_Party_Vendors__c
								,Opportunity__c =opp.id
								,Name = opp.name
into Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload
 from SUPERION_PRODUCTION.dbo.Opportunity opp
 left join SUPERION_PRODUCTION.dbo.[User] mgrusr 
 on mgrusr.name=opp.Proposal_Specialist__c  
where 1=1
and isnull(opp.Legacy_Source_System__c,'X')<>'Tritech'
and Opp.RFPStatus__c is not null


--(1163 row(s) affected)


select distinct * from Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload

-- drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load
select * 
into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
from Procurement_Activity__c_Opportunity_Tritech_SFDC_Preload where rownum=1

--(1152 row(s) affected)

select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load

--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD ', 'Procurement_Activity__c_Opportunity_Tritech_SFDC_load' 

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD ','Procurement_Activity__c_Opportunity_Tritech_SFDC_load'
/*
Salesforce object Procurement_Activity__c does not contain column RFPStatus__c_orig
Salesforce object Procurement_Activity__c does not contain column rownum
Salesforce object Procurement_Activity__c does not contain column Proposal_Manager__c_orig
Salesforce object Procurement_Activity__c does not contain column Letter_of_Intent_Completed__c_orig
Salesforce object Procurement_Activity__c does not contain column Bidder_s_Conference__c_orig
Salesforce object Procurement_Activity__c does not contain column Bid_Bond_Required__c_orig
*/


select count(*) 
from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 
where error ='Operation successful.'
-- 446

-- drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
select * 
into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 
where error<>'Operation Successful.'
--(706 row(s) affected)

-- drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors_bkp
select * 
into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors_bkp
from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 
where error<>'Operation Successful.'
--(706 row(s) affected)

select cad_fit__c,rms_fit__c,jms_fit__c,mobile_fit__c
/*update a set a.CAD_Fit__c = replace(a.CAD_Fit__c,'%','')
								,a.RMS_Fit__c = replace(a.RMS_Fit__c,'%','')
								,a.JMS_Fit__c = replace(a.JMS_Fit__c,'%','')
								,a.Mobile_Fit__c = replace(a.Mobile_Fit__c,'%','')
								,a.overall_fit__C = replace(a.overall_fit__C,'%','')*/
								from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors  a
								--(706 row(s) affected)


								
select Proposal_manager_notes__C
--update a set proposal_manager_notes__c =null
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors a where len(proposal_manager_notes__c)>255
 --31 rows affected.

 select if_consultant_other__c
--update a set if_consultant_other__c =null
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors a where len(if_consultant_other__c)>12
 --73 rows affected.

 select error , count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
 group by error

 select error,proposal_Stage__c 
 -- update a set a.proposal_stage__c =null 
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors a where error like 'Bad value%'
 --(601 row(s) affected)

 --Exec SF_BulkOps 'Insert','SL_SUPERION_PROD ','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors'


 select error, count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors 
 group by error

 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors where error like 'Bad %'
 
 -- drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1
 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'
 --(88 row(s) affected)

 -- drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1_bkp
 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1_bkp
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors
 where error <>'Operation Successful.'
 --(88 row(s) affected)

 
select *
 -- update a set a.cad_fit__c=null
  from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 a where cad_fit__c LIKE '[A-Za-z]%'
  --(2 row(s) affected)
  select * 
  -- update a set a.RMS_fit__c=null
  from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 a where RMS_fit__c LIKE '[A-Za-z]%'
  --(2 row(s) affected)
   select * 
   -- update a set a.JMS_fit__c=null
   from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 a where JMS_fit__c LIKE '[A-Za-z]%'
   --(3 row(s) affected)
    select * 
	-- update a set a.Mobile_fit__c=null
	from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 a where Mobile_fit__c LIKE '[A-Za-z]%'
	--(1 row(s) affected)
	select * 
	-- update a set a.overall_fit__c=null
	from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 a where Overall_Fit__c LIKE '[A-Za-z]%'
	--(5 row(s) affected)

	 select proposal_stage__c, procurement_type__c, error 
	 -- update a set proposal_stage__c = null 
	 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1  a where Proposal_Stage__c in ('Heads up','Completed','Not Required')
	 --(75 row(s) affected)


	  select procurement_type__c, error 
	 -- update a set procurement_type__c = null 
	 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1  a where procurement_type__c in ('Boilerplate Proposal','Needs Assessment','Questionnaire')
	 --(14 row(s) affected)

	 select error, count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1
	 group by error
 --Exec SF_BulkOps 'Insert','SL_SUPERION_PROD ','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1'

 select error, count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1
 group by error

 
select count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors_bkp 
where proposal_stage__c in ('Heads up','Completed','Not Required','Not Out Yet','No')
--675

select count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors_bkp 
where procurement_type__c in ('Boilerplate Proposal','Needs Assessment','Questionnaire')
--14

 --drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2
 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 
 where error <>'Operation Successful.'


 --drop table Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2_bkp
 select * 
 into Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2_bkp
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 
 where error <>'Operation Successful.'


 select *
 --update a set a.Mobile_fit__c=null,a.overall_fit__C = replace(a.overall_fit__C,'%','')
	from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2 a where error='80 / MFR 89 is not a valid percent'

	--(1 row(s) affected)

	--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD ','Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2'

	select *
 --delete 
 from Procurement_Activity__c_Opportunity_Tritech_SFDC_load where error<>'Operation Successful.'

 --(706 row(s) affected)

 --insert into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors where error='Operation Successful.'
 --(618 row(s) affected)

 --insert into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors1 where error='Operation Successful.'
 --(87 row(s) affected)


 --insert into Procurement_Activity__c_Opportunity_Tritech_SFDC_load
 select * from Procurement_Activity__c_Opportunity_Tritech_SFDC_load_errors2 where error='Operation Successful.'
 --(1 row(s) affected)


 select count(*) from Procurement_Activity__c_Opportunity_Tritech_SFDC_load 
 --1152