/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: CommunityUser Migration Script
DEVELOPER	: RTECSON
CREATED DT  : 02/06/2019
DETAIL		: 	
				Selection Criteria:
				a. Migrate all related defects related to a migrated case.

				Discussion: Will migrate to Engineering_Issue__c
				need to Validate with Engineering what their requirements are, and need to also confirm requirements specific to defects NOT related to a case.

DECISION	:
=============
3/18		- For Mapleroots and Production, all Community Users will have a profile of "CentralSquare Community Standard - Staging"
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/06/2019		Ron Tecson			Initial. Copied and modified from 4.2 CommunityUser Migration Script PB.sql.
02/26/2019		Ron Tecson			Modified for MapleRoots Data Load. 
03/18/2019		Ron Tecson			Reload in MapleRoots. Added the log and failures. Ran an update to the main load table after running the process from User_CommunityUser_Load_RT_Error_03182019 table.
03/18/2019		Ron Tecson			Added Refresh of the Local Database after import process. And defaulting the Profile to "CentralSquare Community Standard - Staging".
03/28/2019		Ron Tecson			Repoint to Prod
03/29/2019		Ron Tecson			Created a backup of the load table after the bulk insert. Back Load Table: User_CommunityUser_Load_RT_Orig. Updated the actual table after the rerun of the hotfixes.
									

ERROR/FAILURES:

DATE			ERROR													RESOLUTION
=============== ======================================================= ========================================================================================
02/26/2019		Unable to obtain exclusive to this record or 1 records	Re-run the error and set the batchsize to 1
				Limit Exceeded											Asked Maria H. to increase the # of licenses for community users.
				Duplicate Nickname.<br>Another user has already			Fixed the Nickname by CommunityNickName = 'admin1a'
					selected this nickname.<br>Please select another.	

03/18/2019		'Cannot create a portal user without contact'			27 Records. Account related to these failed contacts are out of scope. Thus not migrated the contacts.
				'Duplicate Nickname.<br>Another user has already		Fixed the Nickname by CommunityNickName = 'admin1a'
					selected this nickname.<br>Please select another.'
				Unable to obtain exclusive to this record or 1 records	Re-run the error and set the batchsize to 1

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

-- Start: -- 
Use SUPERION_Production; 

Exec SF_Refresh 'RT_Superion_PROD', 'Profile', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'Account', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'Contact', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'User', 'yes'

USE Tritech_PROD
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Case', 'yes'
-- End: -- 

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUser_PreLoad_RT') 
	DROP TABLE Staging_PROD.dbo.User_CommunityUser_PreLoad_RT;


---------------------------------------------------------------------------------
-- Load Staging Table -- Estimated Query Time: 3:10 for 8128 records
---------------------------------------------------------------------------------
USE Staging_PROD;

DECLARE @Default_Profile NVARCHAR(18) = (SELECT [Id]  FROM SUPERION_Production.[dbo].[Profile] WHERE NAME  =  'CentralSquare Community Standard - Staging');

Select
		Cast('' as nchar(18)) as [ID],
		Cast('' as nvarchar(255)) as Error,
		a.[ID] as Legacy_Tritech_Id__c,
		'Tritech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		a.[Username] as Username,  --update for production
		a.[LastName] as LastName,
		a.[FirstName] as FirstName,
		a.[CompanyName] as CompanyName,
		a.[Division] as Division,
		a.[Department] as Department,
		a.[Title] as Title,
		a.[Street] as Street,
		a.[City] as City,
		a.[State] as State,
		a.[PostalCode] as PostalCode,
		a.[Country] as Country,
		a.[Email] as Email,  --update for production
		a.[Phone] as Phone,
		a.[Fax] as Fax,
		a.[MobilePhone] as MobilePhone,
		a.[Alias] as Alias,
		a.[CommunityNickname] as CommunityNickname,  -- Remove the '1' value for production load, only necessary for UAT
		'false' as IsActive,
		a.[TimeZoneSidKey] as TimeZoneSidKey,
		a.[LocaleSidKey] as LocaleSidKey,
		a.[ReceivesInfoEmails] as ReceivesInfoEmails,
		a.[ReceivesAdminInfoEmails] as ReceivesAdminInfoEmails,
		a.[EmailEncodingKey] as EmailEncodingKey,
		TT_Profile.[Name] as ProfileName_Original,
		--Spr_Profile.ID as ProfileID,
		@Default_Profile as ProfileID,
		a.[LanguageLocaleKey] as LanguageLocaleKey,
		a.[EmployeeNumber] as EmployeeNumber,
		a.[UserPermissionsMarketingUser] as UserPermissionsMarketingUser,
		a.[UserPermissionsOfflineUser] as UserPermissionsOfflineUser,
		a.[UserPermissionsCallCenterAutoLogin] as UserPermissionsCallCenterAutoLogin,
		a.[UserPermissionsMobileUser] as UserPermissionsMobileUser,
		a.[UserPermissionsSFContentUser] as UserPermissionsSFContentUser,
		a.[UserPermissionsKnowledgeUser] as UserPermissionsKnowledgeUser,
		a.[UserPermissionsInteractionUser] as UserPermissionsInteractionUser,
		a.[UserPermissionsSupportUser] as UserPermissionsSupportUser,
		a.[ForecastEnabled] as ForecastEnabled,
		a.[UserPreferencesActivityRemindersPopup] as UserPreferencesActivityRemindersPopup,
		a.[UserPreferencesEventRemindersCheckboxDefault] as UserPreferencesEventRemindersCheckboxDefault,
		a.[UserPreferencesTaskRemindersCheckboxDefault] as UserPreferencesTaskRemindersCheckboxDefault,
		a.[UserPreferencesReminderSoundOff] as UserPreferencesReminderSoundOff,
		a.[UserPreferencesApexPagesDeveloperMode] as UserPreferencesApexPagesDeveloperMode,
		a.[UserPreferencesHideCSNGetChatterMobileTask] as UserPreferencesHideCSNGetChatterMobileTask,
		a.[UserPreferencesHideCSNDesktopTask] as UserPreferencesHideCSNDesktopTask,
		a.[UserPreferencesSortFeedByComment] as UserPreferencesSortFeedByComment,
		a.[UserPreferencesLightningExperiencePreferred] as UserPreferencesLightningExperiencePreferred,
		a.[CallCenterId] as CallCenterId,
		a.[Extension] as Extension,
		a.[PortalRole] as PortalRole,
		a.[FederationIdentifier] as FederationIdentifier,
		a.[AboutMe] as AboutMe,
		a.[DigestFrequency] as DigestFrequency,
		Cont.ID as ContactID, 
		a.[Bomgar_Username__c] as Bomgar_Username__c
		INTO Staging_PROD.dbo.User_CommunityUser_PreLoad_RT
		FROM Tritech_PROD.dbo.[User] a
		LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] TT_Profile ON  a.ProfileId = TT_Profile.ID											----> TriTech Profiles
		LEFT OUTER JOIN Superion_Production.dbo.[Profile] Spr_Profile ON Spr_Profile.[Name] =												----> Superion Profile
								CASE TT_Profile.[Name] 
									WHEN 'TriTech Portal Read Only with Tickets' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Read-Only User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Standard User' THEN 'CentralSquare Community Standard - Login'
									WHEN 'TriTech Portal Manager' THEN 'CentralSquare Community Standard - Dedicated'
									ELSE 'Superion standard user' END
		LEFT OUTER JOIN Superion_Production.dbo.[Contact] Cont ON a.ContactID = Cont.Legacy_ID__c											----> Superion Contact
		WHERE 
		TT_Profile.[Name] IN
					('TriTech Portal Read Only with Tickets',
					 'TriTech Portal Read-Only User',
					 'Overage High Volume Customer Portal User',
					 'Overage Customer Portal Manager Custom',
					 'TriTech Portal Standard User',
					 'TriTech Portal Manager')
		AND
		(
			(	
				a.isActive = 'true'
				AND  -- requires the user to have either logged into their account since 1/1/2018 or the account was created since 1/1/2018
				(
					a.LastLoginDate >= Cast('01/01/2018' as datetime)
					OR
					a.CreatedDate >= Cast('01/01/2018' as datetime)
				)
			)
			OR  -- Includes any Community User that has submitted a case since 1/1/2016
			(
			  a.ContactID IN
			  (
				select distinct contactID from Tritech_PROD.dbo.[CASE] 
				WHERE CreatedDate >= CAST('01/01/2016' as Datetime)
			  )
			)
		)
		Order by Spr_Profile.ID

-- (8473 rows affected) 1 min.
-- (8527 rows affected) - Prod 1 min.

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='User_CommunityUser_Load_RT') 
	DROP TABLE Staging_PROD.dbo.User_CommunityUser_Load_RT;

SELECT * 
INTO Staging_PROD.dbo.User_CommunityUser_Load_RT
FROM Staging_PROD.dbo.User_CommunityUser_PreLoad_RT

/***** For Testing Purposes before Production 
--USE Staging_SB_MapleRoots
--Exec SF_ColCompare 'Update', 'RT_Superion_PROD', 'User_CommunityUser_Load_RT', 'Legacy_Tritech_ID__c'
*********************************************/

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
Alter table Staging_PROD.dbo.User_CommunityUser_Load_RT
Add [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Delete preloaded records for a delta load
---------------------------------------------------------------------------------
Delete CommUsr_Load

select * 
FROM User_CommunityUser_Load_RT CommUsr_Load
INNER JOIN Staging_PROD.dbo.[User] ON CommUsr_Load.Legacy_Tritech_Id__c = [User].Legacy_Tritech_Id__c

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_PROD

Exec SF_BulkOps 'upsert:batchsize(20)', 'RT_Superion_PROD', 'User_CommunityUser_Load_RT', 'Legacy_Tritech_ID__c'

/*---------------------------------------------------------------------------------

03/29 -- 1:32 mins in Production
--- Starting SF_BulkOps for User_CommunityUser_Load_RT V3.6.7
11:04:01: Run the DBAmp.exe program.
11:04:02: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
11:04:02: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_PROD) .
11:04:03: DBAmp is using the SQL Native Client.
11:04:03: Batch size reset to 20 rows per batch.
11:04:03: SOAP Headers: 
11:04:04: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.
11:04:04: Warning: Column 'Sort' ignored because it does not exist in the User object.
12:35:56: 8527 rows read from SQL Table.
12:35:56: 386 rows failed. See Error column of row for more information.
12:35:56: 8141 rows successfully processed.
12:35:56: Errors occurred. See Error column of row for more information.
12:35:56: Percent Failed = 4.500.
12:35:56: Error: DBAmp.exe was unsuccessful.
12:35:56: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe upsert:batchsize(20) User_CommunityUser_Load_RT "SQL01"  "Staging_PROD"  "RT_Superion_PROD"  "Legacy_Tritech_ID__c"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 218]
SF_BulkOps Error: 11:04:02: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC11:04:02: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_PROD) .11:04:03: DBAmp is using the SQL Native Client.11:04:03: Batch size reset to 20 rows per batch.11:04:03: SOAP Headers: 11:04:04: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.11:04:04: Warning: Column 'Sort' ignored because it does not exist in the User object.12:35:56: 8527 rows read from SQL Table.12:35:56: 386 rows failed. See Error column of row for more information.12:35:56: 8141 rows successfully processed.12:35:56: Errors occurred. See Error column of row for more information.



1 hr 17 mins. 3/18

--- Starting SF_BulkOps for User_CommunityUser_Load_RT V3.6.7
15:49:23: Run the DBAmp.exe program.
15:49:23: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:49:23: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .
15:49:23: DBAmp is using the SQL Native Client.
15:49:23: Batch size reset to 20 rows per batch.
15:49:25: SOAP Headers: 
15:49:26: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.
15:49:26: Warning: Column 'Sort' ignored because it does not exist in the User object.
17:06:34: 8473 rows read from SQL Table.
17:06:34: 48 rows failed. See Error column of row for more information.
17:06:34: 8425 rows successfully processed.
17:06:34: Errors occurred. See Error column of row for more information.
17:06:34: Percent Failed = 0.600.
17:06:34: Error: DBAmp.exe was unsuccessful.
17:06:34: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe upsert:batchsize(20) User_CommunityUser_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_MAPLEROOTS"  "Legacy_Tritech_ID__c"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 202]
SF_BulkOps Error: 15:49:23: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC15:49:23: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .15:49:23: DBAmp is using the SQL Native Client.15:49:23: Batch size reset to 20 rows per batch.15:49:25: SOAP Headers: 15:49:26: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.15:49:26: Warning: Column 'Sort' ignored because it does not exist in the User object.17:06:34: 8473 rows read from SQL Table.17:06:34: 48 rows failed. See Error column of row for more information.17:06:34: 8425 rows successfully processed.17:06:34: Errors occurred. See Error column of row for more information.


1 hr 36 mins. 2/26

--- Starting SF_BulkOps for User_CommunityUser_Load_RT V3.6.7
15:52:20: Run the DBAmp.exe program.
15:52:20: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:52:20: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .
15:52:21: DBAmp is using the SQL Native Client.
15:52:21: Batch size reset to 20 rows per batch.
15:52:22: SOAP Headers: 
15:52:22: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.
15:52:22: Warning: Column 'Sort' ignored because it does not exist in the User object.
17:27:59: 8389 rows read from SQL Table.
17:27:59: 3267 rows failed. See Error column of row for more information.
17:27:59: 5122 rows successfully processed.
17:27:59: Errors occurred. See Error column of row for more information.
17:27:59: Percent Failed = 38.900.
17:27:59: Error: DBAmp.exe was unsuccessful.
17:27:59: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe upsert:batchsize(20) User_CommunityUser_Load_RT "SQL01"  "Staging_SB_MapleRoots"  "RT_Superion_MAPLEROOTS"  "Legacy_Tritech_ID__c"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 184]
SF_BulkOps Error: 15:52:20: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC15:52:20: Upserting Salesforce using User_CommunityUser_Load_RT (SQL01 / Staging_SB_MapleRoots) .15:52:21: DBAmp is using the SQL Native Client.15:52:21: Batch size reset to 20 rows per batch.15:52:22: SOAP Headers: 15:52:22: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.15:52:22: Warning: Column 'Sort' ignored because it does not exist in the User object.17:27:59: 8389 rows read from SQL Table.17:27:59: 3267 rows failed. See Error column of row for more information.17:27:59: 5122 rows successfully processed.17:27:59: Errors occurred. See Error column of row for more information.

--------------------------------------------------------------------------------- */

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

SELECT ERROR, count(*) 
FROM User_CommunityUser_Load_RT
where error not like '%success%'
GROUP BY ERROR

select * from User_CommunityUser_Load_RT where error like '%success%'

select * from User_CommunityUser_Load_RT where error like '%success%'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------

Use SUPERION_Production; 

Exec SF_Refresh 'RT_Superion_PROD', 'User', 'yes'

----------------------------------------------------------------------------------------------------------------
-- HOT FIX -- FOR PRODUCTION
----------------------------------------------------------------------------------------------------------------
USE Staging_PROD

-- #1 --- Rerun

select * INTO User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access
from User_CommunityUser_Load_RT
where error like 'unable to obtain exclusive%'

Exec SF_BulkOps 'Insert:batchsize(1)', 'RT_Superion_PROD', 'User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access'

		--- Starting SF_BulkOps for User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access V3.6.7
		12:40:17: Run the DBAmp.exe program.
		12:40:17: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
		12:40:17: Inserting User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access (SQL01 / Staging_PROD).
		12:40:18: DBAmp is using the SQL Native Client.
		12:40:18: Batch size reset to 1 rows per batch.
		12:40:18: SOAP Headers: 
		12:40:18: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.
		12:40:18: Warning: Column 'Sort' ignored because it does not exist in the User object.
		12:42:02: 177 rows read from SQL Table.
		12:42:02: 2 rows failed. See Error column of row for more information.
		12:42:02: 175 rows successfully processed.
		12:42:02: Errors occurred. See Error column of row for more information.
		12:42:02: Percent Failed = 1.100.
		12:42:02: Error: DBAmp.exe was unsuccessful.
		12:42:02: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert:batchsize(1) User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access "SQL01"  "Staging_PROD"  "RT_Superion_PROD"  " " 
		--- Ending SF_BulkOps. Operation FAILED.
		Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 319]
		SF_BulkOps Error: 12:40:17: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC12:40:17: Inserting User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access (SQL01 / Staging_PROD).12:40:18: DBAmp is using the SQL Native Client.12:40:18: Batch size reset to 1 rows per batch.12:40:18: SOAP Headers: 12:40:18: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.12:40:18: Warning: Column 'Sort' ignored because it does not exist in the User object.12:42:02: 177 rows read from SQL Table.12:42:02: 2 rows failed. See Error column of row for more information.12:42:02: 175 rows successfully processed.12:42:02: Errors occurred. See Error column of row for more information.



select * INTO User_CommunityUser_Load_RT_HF_Duplicate_Nickname
from User_CommunityUser_Load_RT
where
error = 'Duplicate Nickname.<br>Another user has already selected this nickname.<br>Please select another.'

update User_CommunityUser_Load_RT_HF_Duplicate_Nickname
set CommunityNickname = CommunityNickname + '_Prod'

Exec SF_BulkOps 'Insert:batchsize(1)', 'RT_Superion_PROD', 'User_CommunityUser_Load_RT_HF_Duplicate_Nickname'

		--- Starting SF_BulkOps for User_CommunityUser_Load_RT_HF_Duplicate_Nickname V3.6.7
		12:48:57: Run the DBAmp.exe program.
		12:48:57: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
		12:48:57: Inserting User_CommunityUser_Load_RT_HF_Duplicate_Nickname (SQL01 / Staging_PROD).
		12:48:57: DBAmp is using the SQL Native Client.
		12:48:57: Batch size reset to 1 rows per batch.
		12:48:57: SOAP Headers: 
		12:48:58: Warning: Column 'ProfileName_Original' ignored because it does not exist in the User object.
		12:48:58: Warning: Column 'Sort' ignored because it does not exist in the User object.
		12:49:20: 38 rows read from SQL Table.
		12:49:20: 38 rows successfully processed.
		12:49:20: Percent Failed = 0.000.
		--- Ending SF_BulkOps. Operation successful.

--
select * INTO User_CommunityUser_Load_RT_Orig from User_CommunityUser_Load_RT

update T1
set T1.Id = T2.Id, T1.Error = T2.Error from User_CommunityUser_Load_RT T1
INNER JOIN User_CommunityUser_Load_RT_HF_Duplicate_Nickname T2 ON T1.Legacy_Tritech_Id__c = T2.Legacy_Tritech_Id__c
WHERE T1.error = 'Duplicate Nickname.<br>Another user has already selected this nickname.<br>Please select another.'

update T1
set T1.Id = T2.Id, T1.Error = T2.Error from User_CommunityUser_Load_RT T1
INNER JOIN User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access T2 ON T1.Legacy_Tritech_Id__c = T2.Legacy_Tritech_Id__c
WHERE T1.error like '%unable to obtain%'

