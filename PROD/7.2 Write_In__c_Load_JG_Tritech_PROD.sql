 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : Migrate from Write_In__c to Sales_Request__c.

-----------Tritech Sales_Request__c----------
Requirement No  :
Total Records   :  
Scope: Migrate all records that relate to a migrated opportunity record.
 */

 /*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','Write_In__c','Yes'


Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','RecordType','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Contact','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Opportunity','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Sales_Request__c','Yes'
*/ 

declare @recordtypeid NVARCHAR(18) = (Select top 1 ID from SUPERION_PRODUCTION.dbo.recordtype 
where name ='Custom Write-In' and SobjectType='Sales_Request__c');

declare @defaultuser nvarchar(18)=(select top 1 id from SUPERION_PRODUCTION.dbo.[User] 
                                 where name ='CentralSquare API' )

Use Staging_PROD;

--Drop table Sales_Request__c_Tritech_SFDC_WriteIn_Preload

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Record_ID__c =write.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

--,Action_Summary_Documented_and_Attached__c=Action_Summary_Documented_and_Attached__c
,Action_Taken_or_Cancellation_Reason__c=Action_Taken_or_Cancellation_Reason__c
--Config ,Actual_Opportunity_Value__c=Actual_Opportunity_Value__c
--,Added_to_Quote__c=Added_to_Quote__c
,Additional_Hardware_System_Required__c=Additional_Hardware_System_Required__c
,Additional_Information_Required__c=Additional_Information_Required__c
,Shipment_Notes__c=Additional_Notes__c
--,Age_of_Request__c=Age_of_Request__c
--,Agency__c=Agency__c

,Agency_Contact_Name__c_orig=Agency_Contact_Name__c
,Contact__c=cntct.Id

--,Agency_Contact_Phone__c=Agency_Contact_Phone__c
--,Agency_State__c=Agency_State__c
,Agency_Type__c=Agency_Type__c
,Any_Pricing_Assumptions__c=Any_Pricing_Assumptions__c
--,Are_Product_Modifications_Required__c=Are_Product_Modifications_Required__c

,Assigned_To__c_orig=Assigned_To__c
,Assigned_To__c=assgndusr.Id

--,Assignment_Complete__c=Assignment_Complete__c
,Availability__c=Availability__c
,BA_PdM_Analysis_Design_Hours__c=BA_PdM_Analysis_Design_Hours__c
--,Can_be_delivered_prior_to_end_of_2016__c=Can_be_delivered_prior_to_end_of_2016__c
,Category_Interface__c=Category_Interface__c

,Completed_By__c_orig=Completed_By__c
,Completed_By__c=cmpltduser.Id

,Completed_Functional_Matrix_with_Comment__c=Completed_Functional_Matrix_with_Comment__c
,Configuration_Request_Form_Attached__c=Configuration_Request_Form_Attached__c

,CreatedById_orig=write.CreatedById
,CreatedById=IIF(createdbyuser.Id IS NULL,@defaultuser,createdbyuser.Id)

,CreatedDate=write.CreatedDate
,Current_RMS_Production_Svr_OS_Version__c=Current_RMS_Production_Svr_OS_Version__c
--,Current_RMS_Production_Svr_SQL_Version__c=Current_RMS_Production_Svr_SQL_Version__c
,Current_VisionRMS_Modules_Used__c=Current_VisionRMS_Modules_Used__c
,Custom_Engineering_Services__c=Custom_Engineering_Services__c
,CWI_Deliverable__c=CWI_Deliverable__c
,Status__c=CWI_Status__c
,Data_Transfer_Action__c=Data_Transfer_Action__c
,Date_Completed__c=Date_Completed__c
,Date_of_Request__c=Date_of_Request__c
,Date_Sent_to_Engineering__c=Date_Sent_to_Engineering__c
,Other_Details_Special_Instructions__c=Describe_any_Special_Requirements__c
,Describe_Objective_of_the_Needed_Support__c=Describe_Objective_of_the_Needed_Support__c
,Describe_the_Expected_User_Experience__c=Describe_the_Expected_User_Experience__c
,Disaster_Recovery__c=Disaster_Recovery__c
,Documentation_Translation_Hours__c=Documentation_Translation_Hours__c
--,Does_Client_Currently_Use_Barcoding__c=Does_Client_Currently_Use_Barcoding__c
--,Does_Client_Currently_Use_GeoComm__c=Does_Client_Currently_Use_GeoComm__c
,Does_Client_Currently_Use_IQ__c=Does_Client_Currently_Use_IQ__c
--,Does_Client_Currently_Use_VisionCAD__c=Does_Client_Currently_Use_VisionCAD__c
,Does_Client_Currently_Use_VisionFBR__c=Does_Client_Currently_Use_VisionFBR__c
--,Does_Client_Currently_Use_VisionJAIL__c=Does_Client_Currently_Use_VisionJAIL__c
,Does_Client_Currently_Use_VisionMobile__c=Does_Client_Currently_Use_VisionMobile__c
--,Does_Client_have_Test_Training_for_RMS__c=Does_Client_have_Test_Training_for_RMS__c
,Does_Client_Use_Mugshots_with_RMS__c=Does_the_client_use_Mugshots_with_RMS__c
,Does_Client_Use_Photo_Lineups_with_RMS__c=Does_the_client_use_photo_lineups_with_R__c
--,Dummyrecordtouchfield__c=Dummyrecordtouchfield__c
--,Email_Opportunity_Owner__c=Email_Opportunity_Owner__c
--,Email_Opportunity_Owner_Manager__c=Email_Opportunity_Owner_Manager__c
--,Email_Opportunity_Pricing_Manager__c=Email_Opportunity_Pricing_Manager__c
,Engineering_Hours__c=Engineering_Hours__c
--,Exceeds_Permissible_Effort_Cap__c=Exceeds_Permissible_Effort_Cap__c
,Grant_Funded_Interface__c=Grant_Funded_Interface__c
--,GSA_PM_Cost__c=GSA_PM_Cost__c
--,GSA_PM_List_Price__c=GSA_PM_List_Price__c
,Hardware_Cost__c=Hardware_Cost__c
,Hardware_Documentation_Attached__c=Hardware_Documentation_Attached__c
,Hardware_List_Price__c=Hardware_List_Price__c
--,How_many_depts_use_VisionRMS_today__c=How_many_depts_use_VisionRMS_today__c
,Hyperconverged_Nutanix_Quoted__c=Hyperconverged_Nutanix_Quoted__c
--,Id=Id
--,If_client_uses_IQ_what_version__c=If_client_uses_IQ_what_version__c
--,If_client_uses_VisionJAIL_what_version__c=If_client_uses_VisionJAIL_what_version__c
--,If_not_using_NIBRS_planning_to_migrate__c=If_not_using_NIBRS_planning_to_migrate__c
--,Inform_PM_Cost__c=Inform_PM_Cost__c
--,Inform_PM_List_Price__c=Inform_PM_List_Price__c
,Interface_Direction__c=Interface_Direction__c
--,Internal_Engineering_Notes__c=Internal_Engineering_Notes__c
,Is_Client_Currently_Using_NIBRS__c=Is_Client_Currently_Using_NIBRS__c
--,Write_In__c=IsDeleted
,Itemized_List_of_RFP_Exceptions__c=Itemized_List_of_RFP_Exceptions__c
,Itemized_Price_Quotation__c=Itemized_Price_Quotation__c
,Known_Hardware_Vendor_Preference__c=Known_Hardware_Vendor_Preference__c
--,Write_In__c=LastActivityDate
--,Write_In__c=LastModifiedById
--,Write_In__c=LastModifiedDate
--,Write_In__c=LastReferencedDate
--,Write_In__c=LastViewedDate
,Multi_Jurisdiction_Agency_Support__c=Multi_Jurisdiction_Agency_Support__c
,Multi_Products__c=Multi_Products__c
,Legacy_Request_Name__c=write.Name
,Needed_By_Date__c=Needed_By_Date__c
,of_Users__c=of_Users__c

,Opportunity__c_orig=Opportunity__c
,Opportunity__c=oppty.Id

--,Opportunity_ID__c=Opportunity_ID__c
--,Opportunity_Name__c=Opportunity_Name__c
--,Opportunity_Owner_Manager__c=Opportunity_Owner_Manager__c
--,Opportunity_Price_Book__c=Opportunity_Price_Book__c
--,Opportunity_Stage__c=write.Opportunity_Stage__c
,Other_Hours__c=Other_Hours__c
--,Payment_Terms__c=Payment_Terms__c
--,Please_list_all_departments__c=Please_list_all_departments__c
,PM_Duration_Months__c=PM_Duration_Months__c
,PM_Percent_Allocation__c=PM_Percent_Allocation__c
,PMO_Hours__c=PMO_Hours__c
--,Price_Book_Chosen__c=Price_Book_Chosen__c
,PRICE_QUOTE_COMPLETE__c=PRICE_QUOTE_COMPLETE__c
--,Pricing_Validity_Period__c=Pricing_Validity_Period__c
,Product_and_or_Service_Descriptions__c=Product_and_or_Service_Descriptions__c
,Product_Line__c=Product_Line__c
--,Quote_Documented_and_Attached__c=Quote_Documented_and_Attached__c

,RecordTypeId_orig=write.RecordTypeId
--,RecordTypeId=recordtype.Id
,RecordTypeId=@recordtypeid

,CASE
 WHEN rcrdtype.Name='3rd Party' THEN '3rd Party'
 WHEN rcrdtype.Name='Hardware' THEN 'Hardware'
 WHEN rcrdtype.Name='Integrated Solution' THEN 'Integrated Solution'
 WHEN rcrdtype.Name='Product Modification' THEN 'Product Modification'
 WHEN rcrdtype.Name='Solution Architect - Assistance Requests' THEN 'Solution Architect - Assistance Request'
 WHEN rcrdtype.Name='Solution Architect - Interfaces' THEN 'Solution Architect - Interfaces'
 WHEN rcrdtype.Name='xInform Web RMS Upgrade' THEN 'Inform Web RMS Upgrade'
 WHEN rcrdtype.Name='xTiburon Custom Product' THEN 'Tiburon Custom Product'
 WHEN rcrdtype.Name='xTiburon Services' THEN 'Tiburon Services'
 END as CWI_Type__c

,Related_CWI__c_orig=Related_CWI__c
,Related_Write_In__c_orig=Related_Write_In__c

,REQUEST_COMPLETE__c=REQUEST_COMPLETE__c
,Request_Type__c=Request_Type__c
,Reviewed__c=Reviewed__c
,RFP_Name__c=RFP_Name__c
,RFP_Reference_Section__c=RFP_Reference_Section__c

--,Send_Request_To__c_orig=Send_Request_To__c
--,Send_Request_To__c=reqstuser.Id

,Send_to_Engineering__c=Send_to_Engineering__c
,Services_Recommended__c=Services_Recommended__c
--,Services_Write_In_Cost__c=Services_Write_In_Cost__c
,Project_Services_Write_In_List_Price__c=CAST(Services_Write_In_List_Price__c as DECIMAL(18,0))
--,SOW_Language__c=SOW_Language__c
,Special_Considerations__c=Special_Considerations__c
,System_Integration_Fee__c=System_Integration_Fee__c
--,Write_In__c=SystemModstamp
--,Target_Implementation_Date__c=Target_Implementation_Date__c
--,Task_Cancelled__c=Task_Cancelled__c
,Task_Complete__c=Task_Complete__c
,Tech_Hours__c=Tech_Hours__c
,Testing_Training__c=Testing_Training__c
,TFS_Work_Item__c=TFS_Work_Item__c
,Third_Party_Contact_Name__c=Third_Party_Contact_Name__c
,Third_Party_Documentation_Attached__c=Third_Party_Documentation_Attached__c
,Third_Party_Email__c=Third_Party_Email__c
,Third_Party_Phone__c=Third_Party_Phone__c
,Third_Party_Vendor__c=Third_Party_Vendor__c
--,Third_Party_Vendor_Identified__c=Third_Party_Vendor_Identified__c
--,Third_Party_Vendor_Product_Version__c=Third_Party_Vendor_Product_Version__c
,Third_Party_Write_In_Cost__c=Third_Party_Write_In_Cost__c
,Third_Party_Write_In_List_Price__c=Third_Party_Write_In_List_Price__c
,Title__c=Title__c
,Total_DBA_Hours__c=Total_DBA_Hours__c
--,Total_DBA_Services_Cost__c=Total_DBA_Services_Cost__c
--,Total_DBA_Services_List_Price__c=Total_DBA_Services_List_Price__c
--,Total_Hours__c=Total_Hours__c
--,Total_Integrated_Solution_Cost__c=Total_Integrated_Solution_Cost__c
--,Total_Integrated_Solution_List_Price__c=Total_Integrated_Solution_List_Price__c
--,Total_Product_Modification_Cost__c=Total_Product_Modification_Cost__c
--,Total_Product_Modification_List_Price__c=Total_Product_Modification_List_Price__c
,Total_Tiburon_CWI_Cost__c=Total_Tiburon_CWI_Cost__c
--,Total_Tiburon_CWI_List_Price__c=Total_Tiburon_CWI_List_Price__c
--,TotalISCost__c=TotalISCost__c
--,TotalISList__c=TotalISList__c
,Training_Hours__c=Training_Hours__c
,Travel_List_Price_Cost__c=CAST(Travel_List_Price_Cost__c as decimal(18,0))
--,Travel_of_Staff_Traveling__c=Travel_of_Staff_Traveling__c
--,Travel_of_Travel_Days__c=Travel_of_Travel_Days__c
--,Travel_of_Trips__c=Travel_of_Trips__c
,Trigger_Event__c=Trigger_Event__c
,Product_Version__c=TriTech_Product_Version__c
,Product_Involved__c=TriTech_Products_Involved__c

,Requestor__c_orig=TriTech_Requestor__c
,Requestor__c=tritechuser.Id

--,TriTech_Requestor_Phone__c=TriTech_Requestor_Phone__c
--,TTRequestor__c=TTRequestor__c
,Virtualized__c=Virtualized__c
,Warranty_Maintenance_Pricing__c=Warranty_Maintenance_Pricing__c
,Write_In_Description__c=Write_In_Description__c
--,X911_BOM_Change_Type__c=X911_BOM_Change_Type__c
--,Zuercher_Project__c=Zuercher_Project__c

into Sales_Request__c_Tritech_SFDC_WriteIn_Preload

from Tritech_PROD.dbo.Write_In__c write
inner join SUPERION_PRODUCTION.dbo.Opportunity oppty
on oppty.Legacy_Opportunity_ID__c=write.Opportunity__c

--Fetching Assigned_To__c(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] assgndusr
on assgndusr.Legacy_Tritech_Id__c=write.Assigned_To__c
 
--Fetching Agency_Contact_Name__c( Contact Lookup)
left join SUPERION_PRODUCTION.dbo.[Contact] cntct
on cntct.Legacy_id__c=write.Agency_Contact_Name__c

--Fetching Completed_By__c(UserLookup) 
left join SUPERION_PRODUCTION.dbo.[User] cmpltduser
on cmpltduser.Legacy_Tritech_Id__c=write.Completed_By__c
 
--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=write.CreatedById

--Fetching Send_Request_To__c(User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] reqstuser
on reqstuser.Legacy_Tritech_Id__c=write.Send_Request_To__c
 
--Fetching TriTech_Requestor__c(User Lookup)
left join SUPERION_PRODUCTION.dbo.[User] tritechuser
on tritechuser.Legacy_Tritech_Id__c=write.TriTech_Requestor__c

--Fetching  (RecordType Lookup)
left join Tritech_PROD.dbo.RecordType rcrdtype
on rcrdtype.Id=write.RecordTypeId
left join SUPERION_PRODUCTION.dbo.recordtype recordtype
on recordtype.Name=rcrdtype.Name; 

--------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.Write_In__c;--10901

Select count(*) from Sales_Request__c_Tritech_SFDC_WriteIn_Preload;--9013

Select Legacy_Record_ID__c,count(*) from Staging_PROD.dbo.Sales_Request__c_Tritech_SFDC_WriteIn_Preload
group by Legacy_Record_ID__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.Sales_Request__c_Tritech_SFDC_WriteIn_load;

Select * into 
Sales_Request__c_Tritech_SFDC_WriteIn_load
from Sales_Request__c_Tritech_SFDC_WriteIn_Preload; --9013

Select * from Staging_PROD.dbo.Sales_Request__c_Tritech_SFDC_WriteIn_load;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'Sales_Request__c_Tritech_SFDC_WriteIn_load' 

/*
Salesforce object Sales_Request__c does not contain column Agency_Contact_Name__c_orig
Salesforce object Sales_Request__c does not contain column Assigned_To__c_orig
Salesforce object Sales_Request__c does not contain column Completed_By__c_orig
Salesforce object Sales_Request__c does not contain column CreatedById_orig
Salesforce object Sales_Request__c does not contain column Opportunity__c_orig
Salesforce object Sales_Request__c does not contain column RecordTypeId_orig
Salesforce object Sales_Request__c does not contain column Related_CWI__c_orig
Salesforce object Sales_Request__c does not contain column Related_Write_In__c_orig
Salesforce object Sales_Request__c does not contain column Requestor__c_orig
Column Total_Tiburon_CWI_Cost__c is not insertable into the salesforce object Sales_Request__c
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','Sales_Request__c_Tritech_SFDC_WriteIn_load'

----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--Updating Ids for fields having lookup relationship on same object.(Related_CWI__c,Related_Write_In__c)

--Drop table Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateCWI;

Select 

 Id=b.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Related_CWI__c_orig=b.Related_CWI__c_orig
,Related_CWI__c=a.Id
,Legacy_Record_ID__c_orig=b.Legacy_Record_ID__c

into Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateCWI

from Sales_Request__c_Tritech_SFDC_WriteIn_load a,Sales_Request__c_Tritech_SFDC_WriteIn_load b
where a.Legacy_Record_ID__c=b.Related_CWI__c_orig
and b.Related_CWI__c_orig IS NOT NULL;--268

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateCWI' 

/*
Salesforce object Sales_Request__c does not contain column Related_CWI__c_orig
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateCWI'

------------------------------------------------------------------------------------------------------
--Drop table Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateWriteIn;

Select 

 Id=b.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))
,Related_Write_In__c_orig=b.Related_Write_In__c_orig
,Related_Write_In__c=a.Id
,Legacy_Record_ID__c_orig=b.Legacy_Record_ID__c

into Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateWriteIn

from Sales_Request__c_Tritech_SFDC_WriteIn_load a,Sales_Request__c_Tritech_SFDC_WriteIn_load b
where a.Legacy_Record_ID__c=b.Related_Write_In__c_orig
and b.Related_Write_In__c_orig IS NOT NULL;--0

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateWriteIn' 

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateWriteIn'

------------------------------------------------------------------------------------------------------

--Shipment_Notes__c Hotfix:

Select 
 Id=ld.Id
,ERROR=CAST(SPACE(255) as NVARCHAR(255))
,Shipment_Notes__c=ld.Notes__c
,Legacy_Record_ID__c_orig=ld.Legacy_Record_ID__c

--into Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateShipmentNotes

from Sales_Request__c_Tritech_SFDC_WriteIn_load ld;--8809

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateShipmentNotes' 

/*
Salesforce object Sales_Request__c does not contain column Legacy_Record_ID__c_orig
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','Sales_Request__c_Tritech_SFDC_WriteIn_load_UpdateShipmentNotes'

