Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'Account_Tritech_SFDC_Load'

--- Starting SF_ColCompare V3.6.7
Problems found with Account_Tritech_SFDC_Load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 0]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Account does not contain column Alarm_Management_Vendor__c_orig
Salesforce object Account does not contain column Alarm_Mgmt_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Billing_Vendor__c_orig
Salesforce object Account does not contain column Billing_Vendor__c_sp
Salesforce object Account does not contain column CAD_Vendor__c_orig
Salesforce object Account does not contain column CurrentCADSWVendor__c_sp
Salesforce object Account does not contain column Client_c_orig
Salesforce object Account does not contain column Type_orig
Salesforce object Account does not contain column CreatedById_orig
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column FDID__c_orig
Salesforce object Account does not contain column ORI__c_orig
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Jail_Vendor__c_orig
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_sp
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Mobile_Vendor__c_orig
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_sp
Salesforce object Account does not contain column Name_orig
Salesforce object Account does not contain column NPS_Score__c_orig
Salesforce object Account does not contain column NPS_Survey_Contact__c_orig
Salesforce object Account does not contain column OwnerId_orig
Salesforce object Account does not contain column Customer_Success_Liaison__c_orig
Salesforce object Account does not contain column ParentId_orig
Salesforce object Account does not contain column Post_Go_Live_Survey_Comments__c_orig
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Survey_Date__c_orig
Salesforce object Account does not contain column Survey_Notes__c_orig
Salesforce object Account does not contain column TriCON_Attendee__c_orig
Salesforce object Account does not contain column Z1_Attendee__c_orig
--Salesforce object Account does not contain column TriCON_Attendee__c
Salesforce object Account does not contain column Z1_Attendee__c_test
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column IQ_Account_Owner__c_orig
Salesforce object Account does not contain column X911_Account_Owner__c_orig
Salesforce object Account does not contain column RMS_Parent_Account__c_orig
Salesforce object Account does not contain column X911_Parent_Account__c_orig
Salesforce object Account does not contain column LegacySFDCAccountId_Superion
Salesforce object Account does not contain column Superion_id
Salesforce object Account does not contain column MergeFlag
Salesforce object Account does not contain column MergeSuperionId
Salesforce object Account does not contain column MergeTritechId
Column CreatedById is not insertable into the salesforce object Account
Column CreatedDate is not insertable into the salesforce object Account