/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  [AccountId]
      ,[Asset_Description__c]
      ,[Asset_Location_911__c]
      ,[Asset_Total_Price__c]
      ,[Asset_Version__c]
      ,[AssetURL__c]
      ,[ContactId]
      ,[CreatedById]
      ,[CreatedDate]
      ,[Date_Activated_WMP__c]
      ,[Description]
      ,[Expired_Date_WMP__c]
      ,[Extended_Net_Price__c]
      ,[Id]
      ,[InstallDate]
      ,[IsCompetitorProduct]
      ,[IsDeleted]
      ,[LastModifiedById]
      ,[LastModifiedDate]
      ,[LastReferencedDate]
      ,[LastViewedDate]
      ,[List_Price_Per_Unit__c]
      ,[Manufacturer__c]
      ,[Name]
      ,[ParentId]
      ,[Price]
      ,[Product2Id]
      ,[Product_Family_WMP__c]
      ,[Product_Sub_Module__c]
      ,[Product_WMP__c]
      ,[PurchaseDate]
      ,[Quantity]
      ,[Quantity_WMP__c]
      ,[RootAssetId]
      ,[Sales_Order__c]
      ,[Serial_Number_WMP__c]
      ,[SerialNumber]
      ,[Status]
      ,[StockKeepingUnit]
      ,[Support_Price__c]
      ,[SystemModstamp]
      ,[Under_Maintenance__c]
      ,[UsageEndDate]
      ,[Warranty__c]
      ,[X911_Asset_Vendor__c]
  FROM [Tritech_PROD].[dbo].[Asset]



  select distinct status  FROM [Tritech_PROD].[dbo].[Asset]


  select * from camp
  
  
  select distinct stagename
  from [EN_Tritech_PROD]...opportunity


  select top 1 * from [EN_Tritech_PROD]...note

  select distinct isprivate,count(*) from [EN_Tritech_PROD]...note group by isprivate

  select distinct substring(parentid,1,3) from [EN_Tritech_PROD]...note
  where isprivate = 'true'


  select count(*)
  from [EN_Tritech_PROD]...[case]
  where createddate > '1/1/2016'


  select count(*) 
  from [EN_Tritech_PROD]...Potential_Defect__c


  select createddate, c.*
  from project__c c
  order by c.createddate desc

  select count(*) from project__c

  select count(*) from  [EN_Tritech_PROD]...casecomment


  select count(*) from [dbo].[KnowledgeArticle]

  select * from INFORMATION_SCHEMA.COLUMNS
  where table_name like '%category%'

  select distinct 'select * from ' + table_name +  ' union '
  from INFORMATION_SCHEMA.COLUMNS
  where table_name like '%datacategory%'


    select distinct table_name
  from INFORMATION_SCHEMA.COLUMNS
  where column_name like '%body%'



    select distinct table_name
  from INFORMATION_SCHEMA.COLUMNS
  where table_name like '%knowledge%'


  select * from idea

  select * from Defect__DataCategorySelection

    select ka.* from [dbo].[KnowledgeArticle] ka

  
  select ka.*, def.* from [dbo].[KnowledgeArticle] ka
  join Defect__DataCategorySelection def on def.ParentId = ka.id


  select * 
  into sys_sfobjects
  from [EN_Tritech_PROD]...sys_sfobjects


  select * from sys_sfobjects
  where name like '%category%'

  select * 
  
  from [EN_Tritech_PROD]...KnowledgeArticleVersion

 exec sf_replicate 'EN_Tritech_PROD','KnowledgeArticleVersion'


 select * from KnowledgeArticle ka
 join KnowledgeArticleVersion kv on kv.KnowledgeArticleId = ka.id

 select * from KnowledgeArticleVersion



select * from [EN_Tritech_PROD]...CLDY_Template_Product_Category__c
select * from [EN_Tritech_PROD]...CategoryData
select * from [EN_Tritech_PROD]...CategoryNode

with cte_datacategory as
(
select * from Defect__DataCategorySelection union 
select * from FAQ__DataCategorySelection union 
select * from How_To__DataCategorySelection union 
select * from Release_Notes__DataCategorySelection union 
select * from Tech_Advisory__DataCategorySelection union 
select * from Tech_Tips__DataCategorySelection union 
select * from Ticket_Solutions__DataCategorySelection union 
select * from User_Manuals__DataCategorySelection union 
select * from Video__DataCategorySelection
)

 select ka.id as KnowledgeArticleId,
        kv.id as KnowledgeArticleVersionId,
		ka.ArticleNumber,
		Kv.title as KnowledgeArticle_title,
		kv.articletype,
		kv.summary,
		kv.versionnumber,
		cte.DataCategoryGroupName,
		cte.DataCategoryName
into ZZ_temp_marty_KA_dataCategory
		 from KnowledgeArticle ka

 join KnowledgeArticleVersion kv on kv.KnowledgeArticleId = ka.id
 join cte_datacategory cte on cte.parentid = kv.id