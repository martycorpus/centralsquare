select * 
into TT_Superion_Account_Merge_old_with_dups
from 
TT_Superion_Account_Merge

alter table TT_Superion_Account_Merge add ident int identity(1,1)


select * from TT_Superion_Account_Merge


--LIST DUPLICATE TT (SOURCE) account merges.

select * from TT_Superion_Account_Merge
where [18-digit TT Account ID] in
(
select [18-digit TT Account ID]
 from TT_Superion_Account_Merge
 group by [18-digit TT Account ID]
having count(*) > 1
) 
order by [18-digit TT Account ID]

--fix wanyne county
BEGIN tran wayne
update TT_Superion_Account_Merge
set [Account Id] = '0018000001PZcbi', [18-digit TT Account ID] = '0018000001PZcbiAAD', [TT Account Name] = 'Wayne County Communications, NC'
where ident = 1521;

-- commit 

--fix Lagrange

begin tran
update TT_Superion_Account_Merge
set [18-digit SUP Account ID] = '0016A00000AMrBvQAL' , [18-digit TT Account ID] = '0018000001O2ngEAAR', [Account Id] = '0018000001O2ngE'
where ident = 244;


delete from TT_Superion_Account_Merge where ident = 1147

--fix johnson county

update TT_Superion_Account_Merge
set [18-digit SUP Account ID] = '0016A00000AMqseQAD' , [18-digit TT Account ID] = '0018000001PZcGNAA1', [Account Id] = '0018000001PZcGN'
where ident = 635;


delete from TT_Superion_Account_Merge where ident = 1122;


--fix santa crua/santa fe
update TT_Superion_Account_Merge
set [18-digit TT Account ID] = '0018000001PZcVCAA1', [Account Id] = '0018000001PZcVC'
where ident = 1401;


-- select * FROM  TT_Superion_Account_Merge where ident in (1521,1522,244,1147,635,1122,1401,1404) order by [SUP Account Name]



--LIST DUPLICATE Superion (Target) account merges.

select * from TT_Superion_Account_Merge
where [18-digit Sup Account ID] in
(
select [18-digit Sup Account ID]
 from TT_Superion_Account_Merge
 group by [18-digit Sup Account ID]
having count(*) > 1
) 
order by [18-digit Sup Account ID]

--fix Chenney/EaternWa 
begin transaction

update TT_Superion_Account_Merge
set [SUP Account Name] = 'Eastern Washington University, WA' , [18-digit sup Account ID] = '0016A00000AMql3QAD'
where ident = 998;

--fix Mt view/Morrisville 
update TT_Superion_Account_Merge
set [18-digit SUP Account ID] = '0016A00000RSuYBQA1'
where ident = 1253;
-- commit

-- select * from TT_Superion_Account_Merge where ident in (913,998,1250,1253) order by [18-digit SUP Account ID]


--checks:
select [18-digit SUP Account ID]
 from TT_Superion_Account_Merge --1555
 
 select distinct [18-digit SUP Account ID]
 from TT_Superion_Account_Merge --1555

select t1.[18-digit SUP Account ID]
 from TT_Superion_Account_Merge t1
 join [SUPERION_MAPLESB].dbo.account t2 on t2.id = t1.[18-digit SUP Account ID] --1553


select t1.[18-digit SUP Account ID]
 from TT_Superion_Account_Merge t1
 left join [SUPERION_MAPLESB].dbo.account t2 on t2.id = t1.[18-digit SUP Account ID] --1553
 where t2.id is null;

 00100000001713dAAA
0016A00000MsbqDQAR


select * from TT_Superion_Account_Merge
where [18-digit SUP Account ID] in ('00100000001713dAAA','0016A00000MsbqDQAR')