use Tritech_Prod;

select count(id)  from [Tritech_Prod].[dbo].[Attachment] --369,734




-- drop table attachment_ids_ttprod_delta

-- get all attachment ids from TT prod and download them into "attachment_ids_ttprod_delta" table.
-- drop table attachment_ids_ttprod_delta
select id , getdate() as sync_date
 into attachment_ids_ttprod_delta
from Mc_Tritech_Prod...Attachment; -- (381126 row(s)) 00:01:58

--remove attachment ids from the list ( "attachment_ids_ttprod_delta" table) that were arlready exported previously.
delete from attachment_ids_ttprod_delta
where id in (select id from [Tritech_Prod].[dbo].[Attachment]) --(369625 row(s) affected)

--count number of delta records.
select count(*) from attachment_ids_ttprod_delta; --11501

--download the delta attachment records.
select      [Body]
           ,[BodyLength]
           ,[ContentType]
           ,[CreatedById]
           ,[CreatedDate]
           ,[Description]
           ,a.[Id]
           ,[IsDeleted]
           ,[IsPrivate]
           ,[LastModifiedById]
           ,[LastModifiedDate]
           ,[Name]
           ,[OwnerId]
           ,[ParentId]
           ,[SystemModstamp]
into attachments_ttprod_delta
from Mc_Tritech_Prod...Attachment a
join attachment_ids_ttprod_delta d on d.id = a.id; --(11501 row(s) affected) 3/31 00:48:55


--insert all the delta attachments into the main attachment table.
USE [Tritech_PROD]
GO

INSERT INTO [dbo].[Attachment]
           ([Body]
           ,[BodyLength]
           ,[ContentType]
           ,[CreatedById]
           ,[CreatedDate]
           ,[Description]
           ,[Id]
           ,[IsDeleted]
           ,[IsPrivate]
           ,[LastModifiedById]
           ,[LastModifiedDate]
           ,[Name]
           ,[OwnerId]
           ,[ParentId]
           ,[SystemModstamp])
     select 
            [Body]
           ,[BodyLength]
           ,[ContentType]
           ,[CreatedById]
           ,[CreatedDate]
           ,[Description]
           ,[Id]
           ,[IsDeleted]
           ,[IsPrivate]
           ,[LastModifiedById]
           ,[LastModifiedDate]
           ,[Name]
           ,[OwnerId]
           ,[ParentId]
           ,[SystemModstamp]
		   from attachments_ttprod_delta --(11501 row(s) affected) 00:02:05 3/31
GO





