
 -- use Staging_SB;

-- USE Superion_FULLSB;



SELECT 
   'DROP TABLE ' + t.name+ ';' as drop_command,
    t.NAME AS TableName,
    s.Name AS SchemaName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    CAST(ROUND(((SUM(a.total_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS TotalSpaceMB,
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    CAST(ROUND(((SUM(a.used_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS UsedSpaceMB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB,
    CAST(ROUND(((SUM(a.total_pages) - SUM(a.used_pages)) * 8) / 1024.00, 2) AS NUMERIC(36, 2)) AS UnusedSpaceMB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN 
    sys.schemas s ON t.schema_id = s.schema_id
WHERE 
    t.NAME NOT LIKE 'dt%' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
	--and t.name like '%MC'
GROUP BY 
    t.Name, s.Name, p.Rows                                                                                                                                                                                            
ORDER BY 
    --t.Name
	CAST(ROUND(((SUM(a.total_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2))  desc

	-- DROP TABLE Case_Update_Current_Version_MC_preload_0304


	/*
		drop table EmailMessage
drop table Task

DROP TABLE	Bill_To_Ship_To__c_Tritech_ProspectAccounts_S2s_MC_Delete_10Mar
DROP TABLE Bill_To_Ship_To__c_Tritech_ProspectAccounts_B2s_MC_Delete_10Mar
DROP TABLE Case_Update_Current_Version_MC_preload
DROP TABLE System_Product_Version__c_Insert_MC_delete_15Feb
DROP TABLE System_Product_Version__c_Delete_WebPortal_MC_26Feb
DROP TABLE System_Product_Version__c_Delete_WebPortal_MC_08Mar
DROP TABLE System_Product_Version__c_Delete_WebPortal_MC_03Mar
DROP TABLE System_Product_Version__c_Delete_WebPortal_MC_15Feb
DROP TABLE System_Product_Version__c_Delete_WebPortal_MC_15Mar
DROP TABLE System_Product_Version__c_Delete_WebPortal_MC_18Feb
DROP TABLE System_Product_Version__c_Insert_MC_delete_26Feb
DROP TABLE System_Product_Version__c_Insert_MC_delete_18Feb
DROP TABLE System_Product_Version__c_Insert_MC_delete_18Feba
DROP TABLE OpportunityLineItem_Update_MC_0060v000004lMbTAAU
DROP TABLE OpportunityLineItem_Update_MC_0060v000004lMbTAAU_part2
DROP TABLE OpportunityLineItem_Update_MC_0060v000004lMbTAAU_part3
DROP TABLE OpportunityLineItem_Update_MC_0060v000004lMbTAAU_part4
DROP TABLE OpportunityLineItem_Update_MC_0060v000004lMbTAAU_part5
DROP TABLE Case_Update_Current_Version_MC_load_UNmatched
DROP TABLE  Case_Update_Product_MC
	*/


	