/*Run this code:
DBCC SHRINKFILE(Tritech_PROD_log, 2)

DBCC SHRINKFILE(Staging_SB_MapleRoots_log, 2)

DBCC SHRINKFILE(Staging_SB_log, 2)

NOTE: If the target size is not reached, proceed to the next step.
Run this code if you want to truncate the transaction log and not keep a backup of the transaction log. Truncate_only invalidates your transaction log backup sequence. Take a full backup of your database after you perform backup log with truncate_only:


BACKUP LOG pubs WITH TRUNCATE_ONLY
-or-
Run this code if you want to keep a backup of your transaction log and keep your transaction log backup sequence intact. See SQL Server Books Online topic "BACKUP" for more information:
BACKUP LOG pubs TO pubslogbackup
Run this code:
DBCC SHRINKFILE(pubs_log,2)


select * from sys.database_files

*/

drop table [dbo].[CollaborationGroup_load_PB]

drop table [dbo].[CollaborationGroupFeed_load_PB_Result]

drop table [dbo].[CollaborationGroup_Wipe_UAT15_MC]

select 