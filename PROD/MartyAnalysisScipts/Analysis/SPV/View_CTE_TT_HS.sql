-- drop view View_CTE_TT_HS
create view View_CTE_TT_HS-- TT Hardware Software CTE
AS
(
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          t3.id                      AS account__c,
          t3.name                    AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.cim_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.cim_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.cim_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.cim_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.cim_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.fbr_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.fbr_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.fbr_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.fbr_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.fbr_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.imc_version__c  AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMC_Version__c '
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.imc_version__c IS NOT NULL
UNION
SELECT    t1.id                   AS tt_legacy_id_hs,
          t1.name                 AS tt_hs_name,
          t1.account_name__c      AS tt_legacysfdcaccountid__c,
          t3.id                   AS account__c,
          t3.name                 AS account_name,
          t1.inform_me_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Inform_Me_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.inform_me_version__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.iq_version__c   AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IQ_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.iq_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.jail_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Jail_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.jail_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.rms_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.rms_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.rms_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.ttms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'TTMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.ttms_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_cad_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_CAD_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_cad_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_fbr_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_FBR_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fbr_software_version__c IS NOT NULL
UNION
SELECT    t1.id                          AS tt_legacy_id_hs,
          t1.name                        AS tt_hs_name,
          t1.account_name__c             AS tt_legacysfdcaccountid__c,
          t3.id                          AS account__c,
          t3.name                        AS account_name,
          t1.va_fire_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Fire_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fire_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_inform_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Inform_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_inform_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_mobile_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Mobile_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_mobile_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_rms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_RMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_rms_software_version__c IS NOT NULL
UNION
SELECT    t1.id               AS tt_legacy_id_hs,
          t1.name             AS tt_hs_name,
          t1.account_name__c  AS tt_legacysfdcaccountid__c,
          t3.id               AS account__c,
          t3.name             AS account_name,
          t1.visicad_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_patch__c IS NOT NULL
UNION
SELECT    t1.id                 AS tt_legacy_id_hs,
          t1.name               AS tt_hs_name,
          t1.account_name__c    AS tt_legacysfdcaccountid__c,
          t3.id                 AS account__c,
          t3.name               AS account_name,
          t1.visicad_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_production_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Production_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_production_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_mobile_test_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_mobile_test_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_training_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Training_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_training_version__c IS NOT NULL
UNION
SELECT    t1.id                        AS tt_legacy_id_hs,
          t1.name                      AS tt_hs_name,
          t1.account_name__c           AS tt_legacysfdcaccountid__c,
          t3.id                        AS account__c,
          t3.name                      AS account_name,
          t1.visinet_mobile_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_version__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_test_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_test_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_version__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.visinet_training_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_training_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.x911_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'X911_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.x911_software_version__c IS NOT NULL
union
--new 2/13
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_Public_Safety_Records_Mgmt_Software__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Public_Safety_Records_Mgmt_Software__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Public_Safety_Records_Mgmt_Software__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_VCAD_Visual_Computer_Aided_Dispatch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_VCAD_Visual_Computer_Aided_Dispatch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_VCAD_Visual_Computer_Aided_Dispatch__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_Comm_Server__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Comm_Server__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Comm_Server__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_Advanced_Mobile_Online__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Advanced_Mobile_Online__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Advanced_Mobile_Online__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.Tib_PRD_Build__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_PRD_Build__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_PRD_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          cast(t1.Tib_PRD_Build_Date__c as nvarchar(255)) AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_PRD_Build_Date__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_PRD_Build_Date__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.Tib_TRN_Build__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_TRN_Build__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_TRN_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          cast(t1.Tib_TRN_Build_Date__c as nvarchar(255)) AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_TRN_Build_Date__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_TRN_Build_Date__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_DEV_Build__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_DEV_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          cast(t1.Tib_DEV_Build_Date__c as nvarchar(255)) AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_DEV_Build_Date__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_DEV_Build_Date__c IS NOT NULL
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'Zuercher' as productgroup,
          'CAD'  asproductline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%Zuercher%') t1
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'Respond' as productgroup,
          'Billing'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%respond%') t1
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'MetroAlert' as productgroup, 	
          'RMS'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%Metro%') t1 
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'LETG' as productgroup, 	 
          'CAD'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%LETG%') t1 
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'PSSI' as productgroup, 	 
          'CAD'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_FULLSB.dbo.registered_product__c r
       join Superion_FULLSB.dbo.Account a on r.Account__c = a.id where r.name like '%PSSI%') t1  --end new 2/13
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.cim_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.cim_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.fbr_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.fbr_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.imc_version__c  AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMC_Version__c '
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.imc_version__c IS NOT NULL
UNION
SELECT    t1.id                   AS tt_legacy_id_hs,
          t1.name                 AS tt_hs_name,
          t1.account_name__c      AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.inform_me_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Inform_Me_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.inform_me_version__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.iq_version__c   AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IQ_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.iq_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.jail_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Jail_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.jail_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.rms_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.rms_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.rms_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.rms_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.rms_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.rms_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.ttms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'TTMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.ttms_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_cad_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_CAD_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_cad_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_fbr_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_FBR_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_fbr_software_version__c IS NOT NULL
UNION
SELECT    t1.id                          AS tt_legacy_id_hs,
          t1.name                        AS tt_hs_name,
          t1.account_name__c             AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_fire_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Fire_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_fire_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_inform_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Inform_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_inform_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_mobile_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Mobile_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_mobile_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.va_rms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_RMS_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.va_rms_software_version__c IS NOT NULL
UNION
SELECT    t1.id               AS tt_legacy_id_hs,
          t1.name             AS tt_hs_name,
          t1.account_name__c  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visicad_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visicad_patch__c IS NOT NULL
UNION
SELECT    t1.id                 AS tt_legacy_id_hs,
          t1.name               AS tt_hs_name,
          t1.account_name__c    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visicad_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visicad_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_production_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Production_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_production_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_test_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_test_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_test_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_test_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_training_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Training_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_training_version__c IS NOT NULL
UNION
SELECT    t1.id                        AS tt_legacy_id_hs,
          t1.name                      AS tt_hs_name,
          t1.account_name__c           AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_mobile_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_mobile_version__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_test_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_test_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_test_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_test_system_version__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_training_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Patch__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_training_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.visinet_training_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.visinet_training_system_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.x911_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      staging_sb.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'X911_Software_Version__c'
left join superion_fullsb.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_SB.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.x911_software_version__c IS NOT NULL
)