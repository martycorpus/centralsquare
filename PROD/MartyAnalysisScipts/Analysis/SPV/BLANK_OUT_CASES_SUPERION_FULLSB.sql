USE Superion_FULLSB;

exec SF_Refresh 'MC_SUPERION_FULLSB','Case','yES';

--- Starting SF_Refresh for Case V3.6.7
--05:11:34: Using Schema Error Action of yes
--05:11:36: Using last run time of 2019-03-15 21:08:00
--05:15:30: Identified 47127 updated/inserted rows.
--05:15:31: Identified 0 deleted rows.
--05:15:31: Adding updated/inserted rows into Case
--- Ending SF_Refresh. Operation successful.



use Staging_SB;

select id, cast(null as nvarchar(255)) as Error, cast(null as nchar(18)) as  Environment__c, Environment__c as Environment_pre_delete,
'Null out case.environment__c field in order to wipe and reload the Environment__c object.' as apps_comment
-- into Case_update_Environment_to_null
from Superion_FULLSB.dbo.[Case] 
where 
Environment__c is not null
and 
Migrated_Record__c = 'true';

-- exec sf_bulkops 'update:bulkapi,batchsize(10000),'rt_superion_fullsb',' Case_update_Environment_to_null'



