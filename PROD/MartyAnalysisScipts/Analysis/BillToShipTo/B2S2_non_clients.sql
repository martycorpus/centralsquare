select id, name, client__c
from [Tritech_PROD].[dbo].account
where client__c = 'false'



select *
from INFORMATION_SCHEMA.columns
where table_name = 'Account'
and (column_name like '%Billing%'
or column_name like '%Shipping%');



Mailing_Billing_City_WMP__c
Mailing_Billing_Country_WMP__c
Mailing_Billing_State_WMP__c
Mailing_Billing_Street_WMP__c
Mailing_Billing_Zip_Postal_Code_WMP__c


Shipping_City_WMP__c
Shipping_Country_WMP__c
Shipping_State_WMP__c
Shipping_Street_WMP__c
Shipping_Zip_Postal_Code__c

select 
cast(null as nchar(18)) as id,
cast(null as nvarchar(255)) as Error,
t2.id as Account__c,
t1.id as Account_ID_TT_orig,
t1.name as  Account_Name_TT_orig,
'true' as Ship_To__c,
Shipping_Street_WMP__c as Street_1__c,
Shipping_City_WMP__c as City__c,
Shipping_Country_WMP__c as Country__c,
Shipping_State_WMP__c  as State__c,
Shipping_Zip_Postal_Code__c as Zip_Postal_Code__c 
from [Tritech_PROD].[dbo].account t1
left join [Superion_FULLSB].dbo.account t2 on t2.LegacySFDCAccountId__c = t1.id
where client__c = 'false'




select 
cast(null as nchar(18)) as id,
cast(null as nvarchar(255)) as Error,
t2.id as Account__c,
t1.id as Account_ID_TT_orig,
'true' as Bill_To__c,
t1.name as  Account_Name_TT_orig,
t1.Mailing_Billing_Street_WMP__c Street_1__c,
t1.Mailing_Billing_City_WMP__c as City__c,
t1.Mailing_Billing_Country_WMP__c as Country__c,
t1.Mailing_Billing_State_WMP__c as State__c,
t1.Mailing_Billing_Zip_Postal_Code_WMP__c Zip_Postal_Code__c
from [Tritech_PROD].[dbo].account t1
left join [Superion_FULLSB].dbo.account t2 on t2.LegacySFDCAccountId__c = t1.id
where client__c = 'false'


select * from sys_sfobjects where name like 'Bill%'

exec SF_Replicate 'MC_SUPERION_FULLSB','Bill_To_Ship_To__c'

select * from AA_Country_State_Lookup


select * 
from sys.tables
where name like '%State%'
order by create_date desc;


Column_name
Account__c
Account_Name__c
Account_Name_to_Text__c
Address_Code__c
Attention_To__c
Bill_To__c
CaseSafeID__c
City__c
Contact__c
Country__c
County__c
CreatedById
CreatedDate
CurrencyIsoCode
Customer_Attention_To__c
Email__c
Fax__c
Id
InvoiceDeliveryEmail__c
InvoiceDeliveryMethod__c
IsDeleted
LastModifiedById
LastModifiedDate
LastReferencedDate
LastViewedDate
Legacy_Address_ID__c
Legacy_ID_Softrax__c
Legacy_Source_System__c
Migrated_Record__c
Move_to_accounting_system__c
Name
NetSuite_Address_Label__c
NS_Address_Internal_ID__c
Original_Bill_To_Ship_To__c
OwnerId
Phone__c
Primary__c
Secondary_Email__c
Ship_To__c
State__c
Street_1__c
Street_2__c
SystemModstamp
Taxable__c
Zip_Postal_Code__c