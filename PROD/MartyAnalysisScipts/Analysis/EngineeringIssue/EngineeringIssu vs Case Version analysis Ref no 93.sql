/*
select *
from sys.tables
where name like 'Engineering%'

use Superion_Production;

exec SF_Refresh 'MC_SUPERION_PROD','EngineeringIssue__c','Yes'

EXEC SF_COLCOMPARE 'Update','MC_Superion_prod','EngineeringIssue__c_Update_version_hotfix'
--- Starting SF_ColCompare V3.7.7
Problems found with EngineeringIssue__c_Update_version_hotfix. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 9]
--- Ending SF_ColCompare. Operation FAILED.
Salesforce object EngineeringIssue__c does not contain column EngIssuse_Id
Salesforce object EngineeringIssue__c does not contain column EngIssue_Name
Salesforce object EngineeringIssue__c does not contain column EngIssuse_VersionId
Salesforce object EngineeringIssue__c does not contain column EngIssuse_VersionName
Salesforce object EngineeringIssue__c does not contain column EngIssuse_TTCaseID
Salesforce object EngineeringIssue__c does not contain column Case_ID
Salesforce object EngineeringIssue__c does not contain column Case_CurrentVersion__c
Salesforce object EngineeringIssue__c does not contain column Case_Currentversion_VersionName
Salesforce object EngineeringIssue__c does not contain column Case_CurrentVersionNumber__c
Salesforce object EngineeringIssue__c does not contain column Case_Parent_Engineering_Issue__c

EXEC sf_bulkops 'Update','MC_Superion_prod','EngineeringIssue__c_Update_version_hotfix'

----- Starting SF_BulkOps for EngineeringIssue__c_Update_version_hotfix V3.7.7
--15:51:54: Run the DBAmp.exe program.
--15:51:55: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
--15:51:55: Updating Salesforce using EngineeringIssue__c_Update_version_hotfix (SQL01 / Staging_PROD) .
--15:51:55: DBAmp is using the SQL Native Client.
--15:51:56: SOAP Headers: 
--15:51:56: Warning: Column 'EngIssuse_Id' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'EngIssue_Name' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'EngIssuse_VersionId' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'EngIssuse_VersionName' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'EngIssuse_TTCaseID' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'Case_ID' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'Case_CurrentVersion__c' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'Case_Currentversion_VersionName' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'Case_CurrentVersionNumber__c' ignored because it does not exist in the EngineeringIssue__c object.
--15:51:56: Warning: Column 'Case_Parent_Engineering_Issue__c' ignored because it does not exist in the EngineeringIssue__c object.
--15:52:13: 4713 rows read from SQL Table.
--15:52:13: 3993 rows failed. See Error column of row for more information.
--15:52:13: 720 rows successfully processed.
--15:52:13: Errors occurred. See Error column of row for more information.
--15:52:13: Percent Failed = 84.700.
--15:52:13: Error: DBAmp.exe was unsuccessful.
--15:52:13: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe update EngineeringIssue__c_Update_version_hotfix "SQL01"  "Staging_PROD"  "MC_Superion_prod"  " " 
----- Ending SF_BulkOps. Operation FAILED.
--Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 24]
--SF_BulkOps Error: 15:51:55: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC15:51:55: Updating Salesforce using EngineeringIssue__c_Update_version_hotfix (SQL01 / Staging_PROD) .15:51:55: DBAmp is using the SQL Native Client.15:51:56: SOAP Headers: 15:51:56: Warning: Column 'EngIssuse_Id' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'EngIssue_Name' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'EngIssuse_VersionId' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'EngIssuse_VersionName' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'EngIssuse_TTCaseID' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'Case_ID' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'Case_CurrentVersion__c' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'Case_Currentversion_VersionName' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'Case_CurrentVersionNumber__c' ignored because it does not exist in the EngineeringIssue__c object.15:51:56: Warning: Column 'Case_Parent_Engineering_Issue__c' ignored because it does not exist in the EngineeringIssue__c object.15:52:13: 4713 rows read from SQL Table.15:52:13: 3993 rows failed. See Error column of row for more information.15:52:13: 720 rows successfully processed.15:52:13: Errors occurred. See Error column of row for more information.

select error, count(*) from Registered Product field not required.
group by error


alter table EngineeringIssue__c_Update_version_hotfix add ident int identity(1,1)

select * into EngineeringIssue__c_Update_version_hotfix_try2 from EngineeringIssue__c_Update_version_hotfix
where error not like '%success%' --(3993 row(s) affected)

EXEC sf_bulkops 'Update','MC_Superion_prod','EngineeringIssue__c_Update_version_hotfix_try2';
--- Starting SF_BulkOps for EngineeringIssue__c_Update_version_hotfix_try2 V3.7.7
02:52:50: Run the DBAmp.exe program.
02:52:50: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
02:52:50: Updating Salesforce using EngineeringIssue__c_Update_version_hotfix_try2 (SQL01 / Staging_PROD) .
02:52:51: DBAmp is using the SQL Native Client.
02:52:51: SOAP Headers: 
02:52:51: Warning: Column 'EngIssuse_Id' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'EngIssue_Name' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'EngIssuse_VersionId' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'EngIssuse_VersionName' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'EngIssuse_TTCaseID' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'Case_ID' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'Case_CurrentVersion__c' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'Case_Currentversion_VersionName' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'Case_CurrentVersionNumber__c' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'Case_Parent_Engineering_Issue__c' ignored because it does not exist in the EngineeringIssue__c object.
02:52:51: Warning: Column 'ident' ignored because it does not exist in the EngineeringIssue__c object.
02:53:09: 3993 rows read from SQL Table.
02:53:09: 3993 rows successfully processed.
02:53:09: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

alter table EngineeringIssue__c_Update_version_hotfix add error_orig nvarchar(255);
update EngineeringIssue__c_Update_version_hotfix set error_orig = error



update t1
set error = t2.error, id = t2.id
from EngineeringIssue__c_Update_version_hotfix t1
join EngineeringIssue__c_Update_version_hotfix_try2 t2 on t2.ident = t1.ident

select * from EngineeringIssue__c_Update_version_hotfix




*/

use staging_prod;

-- drop table EngineeringIssue__c_Update_version_hotfix;

--main query
;with Cte_Eng_Issues as
(
SELECT t1.id                          AS EngIssuse_Id,
       t5.[name]                      AS EngIssue_Name,
       t1.version__c                  AS EngIssuse_VersionId,
       t3.[name]                      AS EngIssuse_VersionName,
       t1.srccaseid                   AS EngIssuse_TTCaseID,
       t2.id                          AS Case_ID,
       t2.currentversion__c           AS Case_CurrentVersion__c,
       t4.[name]                      AS Case_Currentversion_VersionName,
       t2.currentversionnumber__c     AS Case_CurrentVersionNumber__c,
       t2.parent_engineering_issue__c AS Case_Parent_Engineering_Issue__c
FROM   staging_prod.dbo.ENGINEERINGISSUE__C_LOAD t1
       LEFT JOIN superion_production.dbo.[CASE] t2
              ON t2.legacy_id__c = t1.srccaseid
       LEFT JOIN superion_production.dbo.VERSION__C t3
              ON t3.id = t1.version__c
       LEFT JOIN superion_production.dbo.VERSION__C t4
              ON t4.id = t2.currentversion__c
       LEFT JOIN superion_production.dbo.ENGINEERINGISSUE__C t5
              ON t5.id = t1.id
WHERE  t1.version__c IS NOT NULL AND
       t2.currentversion__c IS NOT NULL AND
       t1.id = t2.parent_engineering_issue__c
UNION
SELECT t1.id                          AS EngIssuse_Id,
       t5.[name]                      AS EngIssue_Name,
       t1.version__c                  AS EngIssuse_VersionId,
       t3.[name]                      AS EngIssuse_VersionName,
       t1.srccaseid                   AS EngIssuse_TTCaseID,
       t2.id                          AS Case_ID,
       t2.currentversion__c           AS Case_CurrentVersion__c,
       t4.[name]                      AS Case_Currentversion_VersionName,
       t2.currentversionnumber__c     AS Case_CurrentVersionNumber__c,
       t2.parent_engineering_issue__c AS Case_Parent_Engineering_Issue__c
FROM   staging_prod.dbo.ENGINEERINGISSUE__C_LOAD_SC1 t1
       LEFT JOIN superion_production.dbo.[CASE] t2
              ON t2.legacy_id__c = t1.srccaseid
       LEFT JOIN superion_production.dbo.VERSION__C t3
              ON t3.id = t1.version__c
       LEFT JOIN superion_production.dbo.VERSION__C t4
              ON t4.id = t2.currentversion__c
       LEFT JOIN superion_production.dbo.ENGINEERINGISSUE__C t5
              ON t5.id = t1.id
WHERE  t1.version__c IS NOT NULL AND
       t2.currentversion__c IS NOT NULL AND
       t1.id = t2.parent_engineering_issue__c 
)
select cte.EngIssuse_Id AS Id,
       CAST(NULL AS nvarchar(255)) AS Error,
	   cte.Case_CurrentVersion__c as version__c,
	   cte.*
INTO EngineeringIssue__c_Update_version_hotfix
  from Cte_Eng_Issues cte;


  select * from EngineeringIssue__c_Update_version_hotfix;

/*
select t1.id as EngIssuse_Id, t5.[Name] as EngIssue_Name, t1.Version__c as EngIssuse_VersionId, t3.[Name] as EngIssuse_VersionName, 
t1.srcCaseId as EngIssuse_TTCaseID , t2.id as Case_ID, t2.CurrentVersion__c as Case_CurrentVersion__c, t4.[Name] as Case_Currentversion_VersionName,
 t2.CurrentVersionNumber__c as Case_CurrentVersionNumber__c, t2.Parent_Engineering_Issue__c as Case_Parent_Engineering_Issue__c
from Staging_Prod.dbo.EngineeringIssue__c_Load_SC1 t1
left join Superion_Production.dbo.[Case] t2 on t2.Legacy_ID__c = t1.srcCaseId
left join Superion_Production.dbo.Version__c t3 on t3.id = t1.Version__c
left join Superion_Production.dbo.Version__c t4 on t4.id = t2.CurrentVersion__c
left join Superion_Production.dbo.EngineeringIssue__c t5 on t5.id = t1.id
where t1.Version__c is not null and t2.CurrentVersion__c  is not null
and t1.id = t2.Parent_Engineering_Issue__c
and t1.id = 'a932G000000TSv0QAG'


select * from Staging_Prod.dbo.EngineeringIssue__c_Load 
where id = 'a932G000000TSv0QAG'
*/