


select id, name, TTZ_Product__c, Legacy_SF_ID__c, Legacy_SF_SKU__c, Legacy_Source_Id__c
from [MC_SUPERION_FULLSB]...product2
where TTZ_Product__c = 'true'
and Legacy_SF_ID__c is not null;




	
Legacy SF ID
Legacy_SF_ID__c	 	Text(50)	 	 	Andrew Rubinett, 10/2/2017 10:18 PM	Not Checked
Edit | Del	 	
Legacy SF SKU
Legacy_SF_SKU__c	 	Text(250)	 	 	Andrew Rubinett, 10/2/2017 10:18 PM	Not Checked
Edit | Del	 	
Legacy Source Id
Legacy_Source_Id__c

with cte_product2 as
(
select id, name, TTZ_Product__c, Legacy_SF_ID__c, Legacy_SF_SKU__c, Legacy_Source_Id__c,Acronym_List__c
from [MC_SUPERION_FULLSB]...product2
where TTZ_Product__c = 'true'
and Legacy_SF_ID__c is not null
and Acronym_List__c <> 'NetSuite Product'
)
select cte_p2.id as superion_product2id, t1.*
from [MC_TRITECH_PROD]...Product_Sub_Module__c t1
join cte_product2 cte_p2 on cte_p2.Legacy_SF_ID__c =  t1.Product__c




select Legacy_SF_ID__c, count(*)

from 
(
select id, name, TTZ_Product__c, Legacy_SF_ID__c, Legacy_SF_SKU__c, Legacy_Source_Id__c
from [MC_SUPERION_FULLSB]...product2
where TTZ_Product__c = 'true'
and Acronym_List__c <> 'NetSuite Product'
and Legacy_SF_ID__c is not null
) x
group by Legacy_SF_ID__c
having count(*) > 1

01t80000003rbi2AAA	3
01t80000008WrX1AAK	3
01t8000000HwbdvAAB	3

select id, name, TTZ_Product__c, Legacy_SF_ID__c, Legacy_SF_SKU__c, Legacy_Source_Id__c, Acronym_List__c 
from [MC_SUPERION_FULLSB]...product2
where TTZ_Product__c = 'true'
and Legacy_SF_ID__c in 
(
'01t80000003rbi2AAA',	
'01t80000008WrX1AAK',	
'01t8000000HwbdvAAB'	
)
order by Legacy_SF_ID__c