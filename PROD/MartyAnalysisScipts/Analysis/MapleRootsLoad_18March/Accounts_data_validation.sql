---check for client__c matches between TT account and Superion Account.

use [SUPERION_MAPLESB];

exec SF_Refresh 'MC_SUPERION_MAPLEROOTS','Account','Yes'



select spa.id as ID_Superion, spa.LegacySFDCAccountId__c as ID_TTZ, spa.[name], Migrated_Record__c, tta.Client__c as TTZ_client__c, spa.Client__c as SPA_client__c
from SUPERION_MAPLESB.dbo.[Account] spa 
join [Tritech_PROD].dbo.[Account] tta on spa.LegacySFDCAccountId__c = tta.id 
where coalesce(spa.Client__c,'false') <> coalesce(tta.Client__c,'false') --(0 row(s) affected) 3/18.



select spa.id as ID_Superion, spa.LegacySFDCAccountId__c as ID_TTZ, spa.[name], Migrated_Record__c, tta.[Type] as TTZ_type, spa.[Type] as SPA__type
from SUPERION_MAPLESB.dbo.[Account] spa 
join [Tritech_PROD].dbo.[Account] tta on spa.LegacySFDCAccountId__c = tta.id 
where coalesce(spa.[Type] ,'x') <> coalesce(tta.[Type] ,'x') --(0 row(s) affected) 3/18.



