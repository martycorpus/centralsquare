--------------------------------
-- LINKED SERVERS:
--------------------------------
/*
 MC_TRITECH_PROD...

 MC_SUPERION_FULLSB...

 MC_SUPERION_MAPLEROOTS...

 MC_SUPERION_PROD...
*/
--------------------------------

--------------------------------
-- STAGING db's:
--------------------------------
/*
 [Staging_SB_MapleRoots].dbo.[]

 [Staging_SB].dbo.[]

 [Staging_PROD].dbo.[]

 [Tritech_PROD].dbo.[]

 MC_SUPERION_PROD...
*/
--------------------------------

select distinct isactive  from User_Tritech_SFDC_load_final


select id, lastname, firstname, isactive from User_Tritech_SFDC_load_final
where lastname = 'James'


select * from Opportunity_Tritech_SFDC_Load
where recordtypeid = '0126A000000cMl5QAE'

select * from User_CommunityUser_Load_RT

select * 
from sys.tables
where name like 'Know%' --and name like '%load%' and name not like '%pre%' and name not like '%SERIES%' and name not like '%update%' and name not like '%error%'
order by create_date desc;


select * from knowledgeArticle_Tritech_SFDC_Load
where UrlName like 'TRICON%' and UrlName like '%Collins%'

--TRICON-2018-Inform RMS Web: What's New and What's Coming_Erica Mathis
/*
--TRICON-2018-TC RMS NIBRS Troubleshooting_Sandy Collins
ID
ka00v0000005Yg5AAE
KnowledgeArticleId_orig
kAA80000000PBufGAG
id_orig
kaA80000000PCbiEAG
Title
TRICON-2018-TC RMS NIBRS Troubleshooting_Sandy Collins
product__c
Inform 911;TC RMS;TC CAD/Mobile;Inform RMS

'Zuercher Suite 12.0 Change Log
select * from knowledgeArticle_Tritech_SFDC_Load
where UrlName like 'Zuercher%' and UrlName like '%log%'

select * from Tritech_KB_Article_Data_Categories
where summary like '%Noverdose%'


select * from knowledgeArticle_Tritech_SFDC_Load
where UrlName like '%noverdose%' 


select distinct newarticlecategory from Tritech_KB_Article_Data_Categories
order by newarticlecategory


newarticlecategory
zSuite

*/



ID	ERROR	id_orig	KnowledgeArticleId_orig	Title	Summary
ka00v0000005ZBnAAM	                                                                                                                                                                                                                                                               	kaA80000000PCYtEAO	kAA80000000PBrqGAG	TRICON-2018-Inform RMS Web: What's New and What's Coming_Erica Mathis	TRICON-2018-Inform RMS Web: What's New and What's Coming_Erica Mathis

select * 
from sys.tables
where name like 'Opp%' and name like '%load%' and name not like '%pre%' and name not like '%SERIES%' and name not like '%update%' and name not like '%error%'
order by create_date desc;


select distinct Order_Type__c  from Opportunity_Tritech_SFDC_Load

select distinct amount from Opportunity_Tritech_SFDC_Load

select * from recordtype
where SobjectType = 'Opportunity'

select * from Account_Tritech_SFDC_UpdateMergingAccounts where LegacySFDCAccountId__c = '0018000001R5qhWAAR'
select * from Account_Tritech_SFDC_UpdateMergingAccounts where Id = '0012G00001YbMbrQAF'


select id, name, Entity_Name__c, * from Account_Tritech_SFDC_Load where  id  = '0012G00001YbMbrQAF'

select id as ID_CentratlSQ_Production, LegacySFDCAccountId__c as TT_Prod_ID ,  t.name as TT_AccountName, Entity_Name__c,

 case when st.state_code is not null then right(name,2)
else '<no State Code in Name>' end as Name_state , 
case when st.state_code is not null then substring(name, 1, len(name)-3) + ', ' + right(name,2)
else t.name end as Hotfix_Name , 
case when st.state_code is not null then 'Hotfix to be applied.' else 'Exception - TT name does note have StateCode - Name will not be hotfixed' end as comment 
from Account_Tritech_SFDC_Load t

left join (
select state_code from staging_sb.dbo.AA_Country_State_Lookup
where Country_code = 'US') st on st.state_code = right(t.name,2)
where t.name not like '%,%'
order by 
case when st.state_code is not null then 'Hotfix' else 'Exception - TT name does note have StateCode' end

select id, LegacySFDCAccountId__c,  name, Entity_Name__c,

 case when st.state_code is not null then right(name,2)
else '<no State in Name>' end as Name_state , 
case when st.state_code is not null then substring(name, 1, len(name)-3) + ', ' + right(name,2)
else t.name end as Name_Hotfix , 
case when st.state_code is not null then 'Hotfix' else 'Exception - TT name does note have StateCode' end as comment 
from Account_Tritech_SFDC_Load t

left join (
select state_code from staging_sb.dbo.AA_Country_State_Lookup
where Country_code = 'US') st on st.state_code = right(t.name,2)
where t.name  like '%,%'


select * from AA_Country_State_Lookup
where State_code = 'WA'

select * from AA_Country_State_Lookup
where Country_code = 'US'

select * from INFORMATION_SCHEMA.columns
where TABLE_NAME like '%country%'




select * 
from sys.tables
where name like 'BGInteg%' and name like '%load%' and name not like '%pre%' and name not like '%delta%' and name not like '%update%' and name not like '%error%'
order by create_date desc;

select id, name, Bomgar_Username__c, * from User_Tritech_SFDC_load
where accountid = '0010r000002HqcgAAC'

--test create 2 dummy contacts with live email ---Mapleroots
--use marty and ron's email addresses (appsassociates.com)
--test import these 2 dummy contacts and make sure: 
  -- 1) that email deliverability is 'All access'
  -- 2) bypass workflow is checked for the apps user account.
--check if we get any sf emails upon data load


select * from Account_Tritech_SFDC_Load


Select *
from information_schema.columns
where table_name = ''



--ERROR HANDLING:

select error, count(*)
from BGIntegration__BomgarSession__c_Tritech_SFDC_Load
group by error;


SELECT * INTO load_table_name_try2 
from load_table_name
where error not like '%success%'

update t1
set id = t2.id, error = t2.error
from load_table_name t1
join load_table_name_try2 t2 on t2.ident = t2.ident
where t2.error like  '%success%';

--ACCOUNT
select error, count(*)
from Account_Tritech_SFDC_Load
group by error; --24,826

select error, count(*)
from Account_Tritech_SFDC_Preload
group by error; --26,378


BGIntegration__Team__c 
BGIntegration__Event_Data__c
BGIntegration__Event__c
BGIntegration__Customer_Exit_Survey_Response__c
BGIntegration__Recording__c_Tritech_SFDC_Load
BGIntegration__BomgarSessionReady__c
BGIntegration__BomgarSession__c
BGIntegration__Custom_Attribute__c
BGIntegration__System_Information__c
BGIntegration__Customer__c
BGIntegration__Representative__c


seelct 
