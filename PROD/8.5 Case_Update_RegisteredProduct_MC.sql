-------------------------------------------------------------------------------
--- 
--  Script File Name: 8.5 Case_Update_RegisteredProduct_MC.sql
--- Developed for CentralSquare - Requested by Gabe Beadle. Update migrated TT cases with Registered Product.
--- Developed by Marty Corpus
--- Copyright Apps Associates 2019
--- Created Date: February 20, 2019
--- Last Updated: 
--- Change Log: 
--- 03-13-2018 Added Customer Asset and Env mappings.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-03-06 Executed in Superion FullSbx.  
-- 2019-03-12 Re-Executed in Superion FullSbx after SPV wipe and reload + Reg Product refresh.
-- 2019-03-13 Re-load with script changes.
-- 2019-03-15 Re-load.
-- 2019-03-26 Re-load.
-- 2019-03-30 Moved and Executed in Production.


/*
-- COLCOMPARE:
-- exec SF_colcompare 'Update','MC_SUPERION_PROD','Case_Update_RegiteredProduct_SPV_MC'
--- Starting SF_ColCompare V3.6.7
Problems found with Case_Update_RegiteredProduct_SPV_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 23]
--- Ending SF_ColCompare. Operation FAILED.


ErrorDesc
Salesforce object Case does not contain column RegisteredProduct_name
Salesforce object Case does not contain column TT_Case_Id
Salesforce object Case does not contain column TT_Legacy_TicketNumber
Salesforce object Case does not contain column SPV_Environment_Type

-- UPDATE:
-- exec SF_Bulkops 'Update','MC_SUPERION_PROD','Case_Update_RegiteredProduct_SPV_MC'
--select * from Case_Update_RegiteredProduct_SPV_MC --(63874 row(s) affected) 3/30.

--- Starting SF_BulkOps for Case_Update_RegiteredProduct_SPV_MC V3.6.7
03:44:41: Run the DBAmp.exe program.
03:44:41: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
03:44:41: Updating Salesforce using Case_Update_RegiteredProduct_SPV_MC (SQL01 / Staging_PROD) .
03:44:42: DBAmp is using the SQL Native Client.
03:44:42: SOAP Headers: 
03:44:42: Warning: Column 'RegisteredProduct_name' ignored because it does not exist in the Case object.
03:44:42: Warning: Column 'TT_Case_Id' ignored because it does not exist in the Case object.
03:44:42: Warning: Column 'TT_Legacy_TicketNumber' ignored because it does not exist in the Case object.
03:44:42: Warning: Column 'SPV_Environment_Type' ignored because it does not exist in the Case object.
03:56:46: 63874 rows read from SQL Table.
03:56:46: 63874 rows successfully processed.
03:56:47: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

*/

-------------------------------------------------------------------------------
-- Refresh local tables
-------------------------------------------------------------------------------

Use Superion_Production;

Exec SF_Refresh 'MC_SUPERION_PROD','Case','Yes'
Exec SF_Refresh 'MC_SUPERION_PROD','System_product_version__c','Yes'
Exec SF_Refresh 'MC_SUPERION_PROD','registered_product__c','Yes'

-------------------------------------------------------------------------------
-- DROP
-------------------------------------------------------------------------------
USE Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Case_Update_RegiteredProduct_SPV_MC')  --2/19
DROP TABLE Staging_PROD.dbo.Case_Update_RegiteredProduct_SPV_MC; --2/15

USE Staging_PROD;

WITH Cte_SPV as
(
select spv.id, rp.id as Registered_Product__c, e.Type__c, spv.environment__c, rp.customerasset__C,
Row_number( )
         OVER (
           partition BY rp.id
           ORDER BY (CASE WHEN e.type__c = 'Production' then 1 WHEN e.type__c = 'Test' then 2 ELSE 3 END) )AS "RANK"
from [Superion_Production].dbo.[SYSTEM_PRODUCT_VERSION__C] spv
join [Superion_Production].dbo.REGISTERED_PRODUCT__C rp on rp.id = spv.Registered_Product__c
left join [Superion_Production].dbo.Environment__c e on e.id = spv.Environment__c)
SELECT DISTINCT c.id,
                cast(NULL as nvarchar(255)) as Error,
                rp.id              AS Registered_Product__c,
                spv.id             AS [SystemProductVersion__c],
				spv.CustomerAsset__c as [CustomerAsset__c],
				spv.Environment__c as [environment__c],
                rp.[name]          AS RegisteredProduct_name,
                c.legacy_id__c     AS TT_Case_Id,
                c.legacy_number__c AS TT_Legacy_TicketNumber,
				spv.Type__c        AS SPV_Environment_Type
INTO  Case_Update_RegiteredProduct_SPV_MC				
FROM   [Superion_Production].dbo.REGISTERED_PRODUCT__C rp
       JOIN [Superion_Production].dbo.[CASE] c
         ON c.productid = rp.product__c AND
            c.accountid = rp.account__c --44015
       JOIN Cte_SPV spv
         ON spv.registered_product__c = rp.id and spv.[RANK] = 1
WHERE  c.legacy_source_system__c = 'Tritech' AND
       c.migrated_record__c = 'TRUE';
--(69824 row(s) affected) 3/26
--(63874 row(s) affected) 3/30 PROD.



--check:
select id, count(*) from Case_Update_RegiteredProduct_SPV_MC group by id having count(*) > 2 --(0 row(s) affected) 3/12.
--(0 row(s) affected) 3/15.
--(0 row(s) affected) 3/13.
--(0 row(s) affected) 3/26.
--(0 row(s) affected) 3/30


