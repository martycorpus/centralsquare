/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Migrate Environment__c - Source System to SFDC
DEVELOPER	: RTECSON, MCORPUS, and PBOWEN
CREATED DT  : 01/31/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
01/31/2019		Ron Tecson			Script for the creation of the Zuercher Environments.
02/26/2019		Ron Tecson			Wipe and Reload
03/04/2019		Ron Tecson			Wipe and Reload
03/12/2019		Ron Tecson			Wipe and Reload. Added a Substring to the Name. The length of the Name target field is 80.
03/15/2019		Ron Tecson			Wipe and Reload
03/26/2019		Ron Tecson			Wipe and Reload. Execution Duration: 6 mins.
03/28/2019		Ron Tecson			Repointed to Production.
03/30/2019		Ron Tecson			Executed in Production. Total Record Count: 480 with no errors.
									
DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------
USE Superion_Production
EXEC SF_Refresh 'RT_Superion_PROD', 'Account', 'SUBSET';
EXEC SF_Refresh 'RT_Superion_PROD', 'Registered_Product__c', 'subset';
EXEC SF_Refresh 'RT_Superion_PROD', 'Product2', 'SUBSET';
EXEC SF_Refresh 'RT_Superion_PROD', 'CUSTOMER_ASSET__C', 'SUBSET';

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_For_Zuercher_Products_PreLoad_RT
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_For_Zuercher_Products_PreLoad_RT_1st') 
DROP TABLE Staging_PROD.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st;

---------------------------------------------------------------------------------
-- Environment for Zuercher Products
---------------------------------------------------------------------------------

SELECT *
INTO Staging_PROD.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st
FROM
(
		SELECT DISTINCT
			'Prod-Zuercher-' + a.Name as Name,
			t1.id as Zuercher_Registered_Product_Id,
			--Concat('Prod', '-', p2.product_line__c, '-', a.name) as Name,
			t1.name as Zuercher_Registered_Product_Name,
			t2.id   AS CustomerAsset__c,
			t2.name AS CustomerAsset_Name,
			t2.product_group__c as CUSTOMER_ASSET_Product_Group,
			p2.product_group__c,
			p2.product_line__c,
			t1.Account__c,
			a.Name as accountname,
			'Prod' AS Environment_Type
		FROM Superion_Production.dbo.REGISTERED_PRODUCT__C t1
			LEFT JOIN Superion_Production.dbo.PRODUCT2 p2 ON p2.id = t1.product__c
			JOIN Superion_Production.dbo.CUSTOMER_ASSET__C t2 ON t2.id = t1.customerasset__c
			LEFT jOIN  Superion_Production.dbo.account a on a.Id = t1.Account__c 
		WHERE t2.product_group__c = 'Zuercher' and t1.name not like '%test%' and t1.name not like '%train%'
		UNION
		SELECT 
			'Test-Zuercher-' + a.Name as Name,
			t1.id as Zuercher_Registered_Product_Id,
			t1.name as Zuercher_Registered_Product_Name,
			t2.id   AS CustomerAsset__c,
			t2.name AS CustomerAsset_Name,
			t2.product_group__c as CUSTOMER_ASSET_Product_Group,
			p2.product_group__c,
			p2.product_line__c,
			t1.Account__c,
			a.Name as accountname,
			'Test' AS Environment_Type
		FROM Superion_Production.dbo.REGISTERED_PRODUCT__C t1
			LEFT JOIN Superion_Production.dbo.PRODUCT2 p2 ON p2.id = t1.product__c
			JOIN Superion_Production.dbo.CUSTOMER_ASSET__C t2 ON t2.id = t1.customerasset__c
			LEFT JOIN Superion_Production.dbo.account a ON a.Id = t1.Account__c 
		WHERE  t2.product_group__c = 'Zuercher' and t1.name like '%test%' 
		UNION		
		SELECT 
			'Train-Zuercher-' + a.Name as Name,
			t1.id as Zuercher_Registered_Product_Id,
			t1.name as Zuercher_Registered_Product_Name,
			t2.id   AS CustomerAsset__c,
			t2.name AS CustomerAsset_Name,
			t2.product_group__c as CUSTOMER_ASSET_Product_Group,
			p2.product_group__c,
			p2.product_line__c,
			t1.Account__c,
			a.Name as accountname,
			'Training' AS Environment_Type
		FROM Superion_Production.dbo.REGISTERED_PRODUCT__C t1
			LEFT JOIN Superion_Production.dbo.PRODUCT2 p2 ON p2.id = t1.product__c
			JOIN Superion_Production.dbo.CUSTOMER_ASSET__C t2 ON t2.id = t1.customerasset__c
			LEFT JOIN Superion_Production.dbo.account a on a.Id = t1.Account__c 
		WHERE t2.product_group__c = 'Zuercher' and t1.name like '%train%'
		)
A

-- 3/12 -- (314 rows affected)
-- 3/15 -- (491 rows affected)
-- 3/26 -- (2575 rows affected)
-- 3/30 -- Production --> (2206 rows affected)

select distinct name from Staging_PROD.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_For_Zuercher_Products_PreLoad_RT
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_For_Zuercher_Products_PreLoad_RT_2nd') 
DROP TABLE Staging_PROD.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_2nd;

/*Create Environment records for Zuercher Records*/
SELECT DISTINCT
	CAST('' AS NVARCHAR(255)) AS ID,
	CAST('' AS NVARCHAR(255)) AS error,	
	'TriTech' as Legacy_Source_System__c,
	'true' as Migrated_Record__c,
	Account__c,
	AccountName,
	Replace(Name, CHAR(39), '') AS [Name],
	'Yes' AS Active__c,
	IIF(Environment_Type = 'Prod', 'Production', Environment_Type) AS Type__c,
	--'Zuercher' + '-' + Product_Line__c as Description__c,
	Description__c = 'Registered Prods Id: ' + 
			
			STUFF
	(
		( 	-- You only want to combine rows for a single ID here:
			SELECT ', ' + Zuercher_Registered_Product_Id
				
			FROM Staging_PROD.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st AS T2 
			-- You only want to combine rows for a single ID here:
			WHERE T2.Account__c = T1.Account__c --and T1.Name = T2.Name
			ORDER BY T2.Name
			FOR XML PATH (''), TYPE
		).value('.', 'varchar(max)')
	, 1, 1, '') ,
	NULL AS Legacy_SystemID__c,
	NULL AS LegacySystemIds,	
	NULL AS Winning_RecordType,
	NULL AS RecordTypes,
	NULL AS connection_type__c,
	NULL AS Notes__c
INTO Staging_PROD.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_2nd
FROM Staging_PROD.dbo.Environment__c_For_Zuercher_Products_PreLoad_RT_1st T1
GROUP BY Account__c, AccountName, Name, Environment_Type

-- 3/12 -- (104 rows affected)
-- 3/15 -- (105 rows affected)
-- 3/26 -- (529 rows affected)
-- 3/30 -- Production --> (480 rows affected)

---------------------------------------------------------------------------------
-- Drop PreLoad Table: Environment__c_For_Zuercher_Products_PreLoad_RT
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_For_Zuercher_Products_Load_RT') 
DROP TABLE Staging_PROD.dbo.Environment__c_For_Zuercher_Products_Load_RT;

SELECT
	ID,
	error,	
	Legacy_Source_System__c,
	Migrated_Record__c,
	Account__c,
	AccountName,
	SUBSTRING([Name], 1, 80) AS [Name],
	IIF(Len([Name]) > 80, [Name], NULL) AS Environment_Name_Orig,
	Active__c,
	Type__c,
	CAST(SUBSTRING(Description__c, 1, 255) AS NVARCHAR(255)) as Description__c,
	NULL AS Legacy_SystemID__c,
	NULL AS LegacySystemIds,	
	NULL AS Winning_RecordType,
	NULL AS RecordTypes,
	NULL AS connection_type__c,
	NULL AS Notes__c
INTO Staging_PROD.dbo.Environment__c_For_Zuercher_Products_Load_RT
FROM Environment__c_For_Zuercher_Products_PreLoad_RT_2nd

-- 3/15 -- (105 rows affected)
-- 3/26 -- (529 rows affected)
-- 3/30 -- Production --> (480 rows affected)


--------------------------------------------------------------------------------
-- Migrate Data Into SFDC Environment__c
--------------------------------------------------------------------------------
USE Staging_PROD

EXEC SF_ColCompare 'INSERT','RT_Superion_PROD','Environment__c_For_Zuercher_Products_Load_RT'

/****************************************************************************************
--- Starting SF_ColCompare V3.6.7
Problems found with Environment__c_For_Zuercher_Products_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 199]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment__c does not contain column AccountName
Salesforce object Environment__c does not contain column Environment_Name_Orig
Salesforce object Environment__c does not contain column LegacySystemIds
Salesforce object Environment__c does not contain column Winning_RecordType
Salesforce object Environment__c does not contain column RecordTypes

****************************************************************************************/

EXEC sf_bulkops 'INSERT','RT_Superion_PROD','Environment__c_For_Zuercher_Products_Load_RT'

/****************************************************************************************

3/30: Production

--- Starting SF_BulkOps for Environment__c_For_Zuercher_Products_Load_RT V3.6.7
14:25:41: Run the DBAmp.exe program.
14:25:41: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
14:25:41: Inserting Environment__c_For_Zuercher_Products_Load_RT (SQL01 / Staging_PROD).
14:25:42: DBAmp is using the SQL Native Client.
14:25:42: SOAP Headers: 
14:25:43: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
14:25:43: Warning: Column 'Environment_Name_Orig' ignored because it does not exist in the Environment__c object.
14:25:43: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
14:25:43: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
14:25:43: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
14:25:44: 480 rows read from SQL Table.
14:25:44: 480 rows successfully processed.
14:25:44: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

****************************************************************************************/

--------------------------------------------------------------------------------
-- Validation
--------------------------------------------------------------------------------

SELECT error, count(*) 
FROM Staging_PROD.dbo.Environment__c_For_Zuercher_Products_Load_RT 
GROUP BY error

select Len(Name), name from Staging_PROD.dbo.Environment__c_For_Zuercher_Products_Load_RT where error <> 'Operation Successful.'

USE Superion_Production

EXEC SF_Refresh 'RT_Superion_PROD', 'Environment__c', 'subset';
