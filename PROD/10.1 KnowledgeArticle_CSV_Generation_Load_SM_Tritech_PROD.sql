/*
-- Author     : Shivani Mogullapalli
-- Date       : 24/11/2018
-- Description: Migrate the KnowledgeArticle data from Tritech Org to Superion Salesforce.
Requirement Number:REQ-

Scope:
a. Migrate all the Knowledge Articles.
b. Migrate only the current knowledge Article Version.


*/
--select distinct articletype from knowledgearticleVersion
/*
use Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_PROD','How_To__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','Tech_Advisory__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','Defect__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','Video__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','Ticket_Solutions__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','FAQ__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','Tech_Tips__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','Release_Notes__kav','yes'
EXEC SF_Refresh 'MC_Tritech_PROD','User_Manuals__kav','yes'
-- total : 5 mins

use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__ka','yes'
EXEC SF_Refresh 'SL_SUPERION_PROD ','Article__kav','yes'
*/

use Staging_PROD 
go 
-- drop table KnowledgeArticle_Tritech_SFDC_Preload
with Article_CSV_File as(
select  distinct
						 id_orig				= id
						,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__body__S			= iif(Attachments_How_To__Body__s is null, null, 'D:\Articles_Files\'+Attachments_How_To__Name__s)
						,File__Name__s			= Attachments_How_To__Name__s
						,File__ContentType__S	= Attachments_How_To__ContentType__s
						,Description__c			= cast(Article_Body_How_To__c as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
						
from Tritech_PROD.dbo.How_To__kav k

											union 
select 
						 id_orig				= id
						,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__body__S			= iif(Attachments_Tech_Advisory__Body__s is null,null,'D:\Articles_Files\'+Attachments_Tech_Advisory__Name__s)
						,File__Name__s			= Attachments_Tech_Advisory__Name__s
						,File__ContentType__S	= Attachments_Tech_Advisory__ContentType__s
						,Description__c			= cast(Tech_Advisory_Article_Body__c as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
						
from Tritech_PROD.dbo.Tech_Advisory__kav 
												union 
select 
						 id_orig				= id
						,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__body__S			= iif(Attachments_Defects__Body__s is null, null, 'D:\Articles_Files\'+Attachments_Defects__Name__s)
						,File__Name__s			= Attachments_Defects__Name__s
						,File__ContentType__S	= Attachments_Defects__ContentType__s
						,Description__c			= cast(Article_Body_Defects__c as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
						
from Tritech_PROD.dbo.Defect__kav 
										union 
select
						 id_orig				= id
						,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__body__S			= iif(Article_Attachments_Video__body__s is null, null, 'D:\Articles_Files\'+Article_Attachments_Video__Name__s)
						,File__Name__s			= Article_Attachments_Video__Name__s
						,File__ContentType__S	= Article_Attachments_Video__ContentType__s
						,Description__c			= cast(concat(iif(Article_body_Video__c is null, null,Article_body_video__C ),CHAR(13)+CHAR(10),
															 iif(Video_link__c is null, null,'Video Link:'+Video_Link__C ))as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
from Tritech_PROD.dbo.Video__kav 
										union 
select 
						 id_orig				= id
						 ,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__body__S			= iif(Attachments_Ticket_Solutions__Body__s is null, null, 'D:\Articles_Files\'+Attachments_Ticket_Solutions__Name__s)
						,File__Name__s			= Attachments_Ticket_Solutions__Name__s
						,File__ContentType__S	= Attachments_Ticket_Solutions__ContentType__s
						,Description__c			= cast(Article_Body_Ticket_Solutions__c as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
from Tritech_PROD.dbo.Ticket_Solutions__kav
										union 
select 
						 id_orig				= id
						 ,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__body__S			= iif(Attachments_Tech_Tips__Body__s is null, null, 'D:\Articles_Files\'+Attachments_Tech_Tips__Name__s)
						,File__Name__s			= Attachments_Tech_Tips__Name__s
						,File__ContentType__S	= Attachments_Tech_Tips__ContentType__s
						,Description__c			= cast(Article_Body_Tech_Tips__c as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
from Tritech_PROD.dbo.Tech_Tips__kav
											union 
select 
						 id_orig				= id
						 ,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__Body__S			= iif(Attachment_Release_Notes__Body__s is null, null, 'D:\Articles_Files\'+Attachment_Release_Notes__Name__s)
						,File__Name__s			= Attachment_Release_Notes__Name__s
						,File__ContentType__S	= Attachment_Release_Notes__ContentType__s
						,Description__c			= cast(Article_Body_Release_Notes__C as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
from Tritech_PROD.dbo.Release_Notes__kav
												union 
select 
						 id_orig				= id
						  ,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__Body__S			= iif(Attachments_User_Manuals__Body__s is null, null, 'D:\Articles_Files\'+Attachments_User_Manuals__Name__s)
						,File__Name__s			= Attachments_User_Manuals__Name__s
						,File__ContentType__S	= Attachments_User_Manuals__ContentType__s
						,Description__c			= cast(Article_Body_User_Manuals__C as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
from Tritech_PROD.dbo.User_Manuals__kav
													union
select 
						 id_orig				= id
						 ,KnowledgeArticleId_orig = KnowledgeArticleId
						,Title					= Title
						,Summary				= Summary
						,UrlName				= UrlName
						,File__Body__S			= null
						,File__Name__s			= null
						,File__ContentType__S	= null
						,Description__c			= cast(concat(IIF(Question__C is null, null,concat('<u><b>QUESTION: </b></u><br>',Question__c)),CHAR(13)+CHAR(10), 
														IIF(Answer__c is null, null,concat('<br><u><b>ANSWER: </b></u><br>',Answer__c))) as nvarchar(max))
						,ArticleNumber_orig		= ArticleNumber
						,Language				= Language
						,OwnerId_orig			= OwnerId
						,CreatedById_orig		= CreatedById
						,CreatedDate_orig		= CreatedDate
						,IsVisibleInApp			= IsVisibleInApp
						,IsVisibleInCsp			= IsVisibleInCsp
						,IsVisibleInPkb			= IsVisibleInPkb
						,IsVisibleInPrm			= IsVisibleInPrm
from Tritech_PROD.dbo.FAQ__kav
)
select *,product__c             = stuff((
															select ';' + B.newarticlecategory
															from Tritech_KB_Article_Data_Categories_final B
															WHERE k.KnowledgeArticleId_orig=B.knowledgearticleid
															for xml path('')),1,1,'')
into KnowledgeArticle_Tritech_SFDC_Preload 
from Article_CSV_File k

--(5767 row(s) affected)
-- 1 min



select count(*) from KnowledgeArticle_Tritech_SFDC_Preload
--(5767 row(s) affected)


-- CHECK FOR THE DUPLICATES.
select id_orig, count(*) from KnowledgeArticle_Tritech_SFDC_Preload
group by id_orig 
having count(*)>1


-- drop table knowledgeArticle_Tritech_SFDC_Load

select			 ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				, * 
into knowledgeArticle_Tritech_SFDC_Load
from knowledgeArticle_Tritech_SFDC_PreLoad

--(5767 row(s) affected)

select * from knowledgeArticle_Tritech_SFDC_Load
/* not needed 
-- drop table knowledgeArticle_Tritech_SFDC_Load
select							
						ID										 =   CAST(SPACE(18) as NVARCHAR(18))
						,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
								,ID_orig							= Art.Id_orig
								,Title 							= Art.Title
								,Summary						= Art.Summary
								,UrlName						= Art.UrlName
								,File__body__S					= Art.File__body__S
								,File__ContentType__S			= art.File__ContentType__S
								,File__Name__s					= art.File__Name__s
								,Description__C					= Art.Description__c
								,Language						= Art.Language
								,OWnerID_orig					= Art.OwnerId_orig
								,OwnerId_						= Target_Owner.Id
								,CreatedbyId_orig				= Art.CreatedById_orig
								,CreatedById					= Target_Create.Id
								,Createddate					= Art.CreatedDate_orig
								
		--into knowledgeArticle_Tritech_SFDC_Load
from knowledgeArticle_Tritech_SFDC_PreLoad Art
--------------------------------------------------------------------------------------------------------------------------
left join SUPERION_PRODUCTION.dbo.[User] Target_Create
on Target_Create.Legacy_Tritech_Id__c=Art.CreatedById_orig
and Target_Create.Legacy_Source_System__c='Tritech'
--------------------------------------------------------------------------------------------------------------------------
left join SUPERION_PRODUCTION.dbo.[User] Target_Owner
on Target_Owner.Legacy_Tritech_Id__c=Art.OwnerId_orig
and Target_Owner.Legacy_Source_System__c='Tritech'
--------------------------------------------------------------------------------------------------------------------------
 
 */


 select *
 --update a set a.file__body__s=null,a.file__Name__s=null,file__Contenttype__S=null
 from knowledgeArticle_Tritech_SFDC_Load a 
 where KnowledgeArticleId_orig in 
 (select KnowledgeArticleId_orig from staging_sb_mapleroots.dbo.knowledgeArticle_Tritech_SFDC_Load_errors1)

 -- (79 row(s) affected) 

 select count(*)  from knowledgeArticle_Tritech_SFDC_Load


  select count(*) from knowledgeArticle_Tritech_SFDC_Load


  -- CHECK FOR THE DUPLICATES.
select id_orig, count(*) from knowledgeArticle_Tritech_SFDC_Load
group by id_orig 
having count(*)>1

-- 40 mins 

-- drop table knowledgeArticle_Tritech_SFDC_Load_ids


select id_orig, count(*) from knowledgeArticle_Tritech_SFDC_Load_ids
group by id_orig
having count(*)>1

select * from knowledgeArticle_Tritech_SFDC_Load_ids

--The id field gets updated in the load table 
select a.id_orig,a.id,b.id as targetid,b.id_orig
-- update a set a.id=b.id,a.error='Operation Successful.'
from knowledgeArticle_Tritech_SFDC_Load a
inner join knowledgeArticle_Tritech_SFDC_Load_ids b
on a.id_orig= b.id_orig
--(5767 row(s) affected)


-- for the error and id field
-- drop table knowledgeArticle_Tritech_SFDC_Load_errors
select count(*) from knowledgeArticle_Tritech_SFDC_Load_errors

-- drop table knowledgeArticle_Tritech_SFDC_Load_errors1
select a.* 
into knowledgeArticle_Tritech_SFDC_Load_errors1
from knowledgeArticle_Tritech_SFDC_Load a 
inner join knowledgeArticle_Tritech_SFDC_Load_errors b 
on a.id_orig=b.id_orig

--(79 row(s) affected)
-- drop table knowledgeArticle_Tritech_SFDC_Load_errors1_bkp
select a.* 
into knowledgeArticle_Tritech_SFDC_Load_errors1_bkp
from knowledgeArticle_Tritech_SFDC_Load a 
inner join knowledgeArticle_Tritech_SFDC_Load_errors b 
on a.id_orig=b.id_orig
-- (79 row(s) affected)



 select * from knowledgeArticle_Tritech_SFDC_Load_errors1 -- (79 errors) 
select * from knowledgeArticle_Tritech_SFDC_Load_errors2--( 1 error)

/* When the csv file is loaded through the Dataloader , a success files gets generated with Id's of the records, which are loaded.
those files are again imported into the Staging_PROD  for displaying ids, in the Load table.

*/
-- At present they are stored in the table 'KnowledgeArticle_Tritech_Imported_Articles'


-- drop table KnowledgeArticle_Tritech_Imported_Articles

select * 
--into KnowledgeArticle_Tritech_Imported_Articles
 from knowledgeArticle_Tritech_SFDC_Load_ids
--(5764 row(s) affected)

--The id field gets updated in the load table 
select a.id_orig,a.id,b.id as targetid,b.id_orig
-- update a set a.id=b.id,a.error='Operation Successful.'
from knowledgeArticle_Tritech_SFDC_Load a
inner join KnowledgeArticle_Tritech_Imported_Articles b
on a.id_orig= b.id_orig
--(5764 row(s) affected)





