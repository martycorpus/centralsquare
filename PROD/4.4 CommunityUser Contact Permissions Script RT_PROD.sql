/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: CommunityUser Contact Community Permissions Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 4.3 CommunityUser Contact Permissions Script PB.sql
02/27/2019		Ron Tecson			Modified for MapleRoots data migration. 
03/28/2019		Ron Tecson			Repoint to Production.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE SUPERION_Production
Exec SF_Refresh 'RT_Superion_PROD', 'Profile', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'Contact', 'yes'
Exec SF_Refresh 'RT_Superion_PROD', 'User', 'yes'

-- Exec SF_REPLICATE 'RT_Superion_PROD', 'User', 'yes'

USE Tritech_PROD
Exec SF_Refresh 'MC_Tritech_Prod', 'User', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Profile', 'yes'
Exec SF_Refresh 'MC_Tritech_Prod', 'Case', 'yes'


---------------------------------------------------------------------------------
-- DROP STAGING TABLE IF EXISTENT 
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Contact_CommunityPermissions_PreLoad_RT') 
	DROP TABLE Staging_PROD.dbo.[Contact_CommunityPermissions_PreLoad_RT];


---------------------------------------------------------------------------------
-- CREATE CONTACT COMMUNITY PERMISSION STAGING TABLE 
---------------------------------------------------------------------------------

SELECT
	a.ContactID as ID,
	CAST('' as nvarchar(255)) as Error,
	CASE SrcProf.[Name] WHEN 'TriTech Portal Read Only with Tickets' THEN 'Case Access Read Only'
						WHEN 'TriTech Portal Standard User' THEN 'Case Access'
						WHEN 'TriTech Portal Manager' THEN 'Delegated Admin Dedicated'
						ELSE '' END as Community_Permissions__c	
INTO Staging_PROD.dbo.Contact_CommunityPermissions_PreLoad_RT
FROM SUPERION_Production.dbo.[User] a
LEFT OUTER JOIN TriTech_Prod.dbo.[user] SrcUser ON a.Legacy_Tritech_Id__c = SrcUser.ID										----> Source User
LEFT OUTER JOIN TriTech_Prod.dbo.[Profile] SrcProf ON SrcUser.ProfileId = SrcProf.ID										----> Source Profile
WHERE migrated_record__c = 'true'
		AND SrcProf.[Name] in ('TriTech Portal Read Only with Tickets', 'TriTech Portal Standard User', 'TriTech Portal Manager')

-- 03/29 -- Production: (8214 rows affected) for 2 mins.
-- 02/27 -- 8251 rows affected 
-- 03/18 -- (8306 rows affected)

select migrated_record__c,Legacy_Tritech_Id__c from SUPERION_Production.dbo.[User]

---------------------------------------------------------------------------------
-- DROP LOAD TABLE IF EXIST IN TARGET
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Contact_CommunityPermissions_Load_RT') 
	DROP TABLE Staging_PROD.dbo.Contact_CommunityPermissions_Load_RT;

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
SELECT *
INTO Staging_PROD.dbo.Contact_CommunityPermissions_Load_RT
FROM Staging_PROD.dbo.Contact_CommunityPermissions_PreLoad_RT

ALTER TABLE Staging_PROD.dbo.Contact_CommunityPermissions_Load_RT
ADD [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_PROD

EXEC SF_BulkOps 'update:bulkapi,batchsize(800)', 'RT_Superion_PROD', 'Contact_CommunityPermissions_Load_RT'

/*********************************** L O G    F O R    B U L K O p s - U P D A T E
-- 03/29 -- Production 2 mins and 21 secs.
--- Starting SF_BulkOps for Contact_CommunityPermissions_Load_RT V3.6.7
15:03:25: Run the DBAmp.exe program.
15:03:25: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:03:25: Updating Salesforce using Contact_CommunityPermissions_Load_RT (SQL01 / Staging_PROD) .
15:03:26: DBAmp is using the SQL Native Client.
15:03:26: Batch size reset to 800 rows per batch.
15:03:26: Sort column will be used to order input rows.
15:03:26: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
15:03:27: Job 7502G000009dOUJQA2 created.
15:03:27: Batch 7512G00000G5MZlQAN created with 800 rows.
15:03:27: Batch 7512G00000G5MZqQAN created with 800 rows.
15:03:27: Batch 7512G00000G5MZvQAN created with 800 rows.
15:03:27: Batch 7512G00000G5Ma5QAF created with 800 rows.
15:03:27: Batch 7512G00000G5MaAQAV created with 800 rows.
15:03:27: Batch 7512G00000G5MaFQAV created with 800 rows.
15:03:27: Batch 7512G00000G5MaKQAV created with 800 rows.
15:03:27: Batch 7512G00000G5MaPQAV created with 800 rows.
15:03:27: Batch 7512G00000G5MaUQAV created with 800 rows.
15:03:27: Batch 7512G00000G5MaZQAV created with 800 rows.
15:03:28: Batch 7512G00000G5MaeQAF created with 214 rows.
15:03:28: Job submitted.
15:03:28: 8214 rows read from SQL Table.
15:03:28: Job still running.
15:03:44: Job still running.
15:04:44: Job still running.
15:05:45: Job Complete.
15:05:45: DBAmp is using the SQL Native Client.
15:05:46: 8214 rows successfully processed.
15:05:46: 0 rows failed.
15:05:46: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


--03/18 -- 2 mins and 24 secs.
--- Starting SF_BulkOps for Contact_CommunityPermissions_Load_RT V3.6.7
22:13:06: Run the DBAmp.exe program.
22:13:06: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
22:13:06: Updating Salesforce using Contact_CommunityPermissions_Load_RT (SQL01 / Staging_SB_MapleRoots) .
22:13:07: DBAmp is using the SQL Native Client.
22:13:07: Batch size reset to 800 rows per batch.
22:13:08: Sort column will be used to order input rows.
22:13:08: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
22:13:09: Job 7500r0000000315AAA created.
22:13:09: Batch 7510r00000005qyAAA created with 800 rows.
22:13:09: Batch 7510r00000005r3AAA created with 800 rows.
22:13:09: Batch 7510r00000005r8AAA created with 800 rows.
22:13:09: Batch 7510r00000005rDAAQ created with 800 rows.
22:13:09: Batch 7510r00000005rIAAQ created with 800 rows.
22:13:09: Batch 7510r00000005rNAAQ created with 800 rows.
22:13:10: Batch 7510r00000005rSAAQ created with 800 rows.
22:13:10: Batch 7510r00000005rXAAQ created with 800 rows.
22:13:10: Batch 7510r00000005rcAAA created with 800 rows.
22:13:10: Batch 7510r00000005qzAAA created with 800 rows.
22:13:10: Batch 7510r00000005r4AAA created with 306 rows.
22:13:11: Job submitted.
22:13:11: 8306 rows read from SQL Table.
22:13:11: Job still running.
22:13:27: Job still running.
22:14:28: Job still running.
22:15:28: Job Complete.
22:15:29: DBAmp is using the SQL Native Client.
22:15:30: 8306 rows successfully processed.
22:15:30: 0 rows failed.
22:15:30: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.
*********************************** L O G    F O R    B U L K O p s - U P D A T E */

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------
SELECT * FROM Contact_CommunityPermissions_Load_RT WHERE error not like '%success%'

SELECT error, COUNT(*)
FROM Contact_CommunityPermissions_Load_RT
GROUP BY error

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

USE SUPERION_Production

EXEC SF_Refresh 'RT_Superion_PROD', 'Contact', 'yes'