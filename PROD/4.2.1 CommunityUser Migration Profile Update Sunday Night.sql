-- Update the community users profile to from the 'dummy' staging profile to the 'live' login profile. 
-- live profile name = 'CentralSquare Community Standard - Login'
-- dummy profile name  = 'CentralSquare Community Standard - Staging'
-- Community Users were initially created in to Production  on 3/29 with the 'staging' profile, but now its time for them to have the live profile so that they can login the Superion Production 
-- community site.
-- Setting the profile to the live profile will send the notification to the community user via email - with a welcome message and their credentials.
-- 2019-03-31 MartyC. Created.
-- 2019-03-31 Executed for Production.

/*-------------------------------------------------------
Execution Log:

COLCOMPARE
-- Exec Sf_colcompare 'UPDATE','MC_SUPERION_PROD','User_CommunityUser_Update_profile_MC';
--- Starting SF_ColCompare V3.6.7
Problems found with User_CommunityUser_Update_profile_MC. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 12]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object User does not contain column profilename_original
Salesforce object User does not contain column ProfileID_Name
Salesforce object User does not contain column ProfileId_transform
Salesforce object User does not contain column ProfileId_Name_transform
Salesforce object User does not contain column User_CommunityUser_Load_RT_error

UPDATE
alter table User_CommunityUser_Update_profile_MC add error nvarchar(255);
-- Exec Sf_Bulkops 'UPDATE','MC_SUPERION_PROD','User_CommunityUser_Update_profile_MC';
--- Starting SF_BulkOps for User_CommunityUser_Update_profile_MC V3.6.7
01:06:27: Run the DBAmp.exe program.
01:06:27: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
01:06:27: Updating Salesforce using User_CommunityUser_Update_profile_MC (SQL01 / Staging_PROD) .
01:06:27: DBAmp is using the SQL Native Client.
01:06:28: SOAP Headers: 
01:06:28: Warning: Column 'profilename_original' ignored because it does not exist in the User object.
01:06:28: Warning: Column 'ProfileID_Name' ignored because it does not exist in the User object.
01:06:28: Warning: Column 'ProfileId_transform' ignored because it does not exist in the User object.
01:06:28: Warning: Column 'ProfileId_Name_transform' ignored because it does not exist in the User object.
01:06:28: Warning: Column 'User_CommunityUser_Load_RT_error' ignored because it does not exist in the User object.
01:18:57: 8354 rows read from SQL Table.
01:18:57: 8354 rows successfully processed.
01:18:57: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.




*/

use Staging_PROD;


-- drop table User_CommunityUser_Update_profile_MC;

SELECT id,
       profilename_original,
	   	   (SELECT id
                FROM   superion_production.dbo.PROFILE
                WHERE  name = 'CentralSquare Community Standard - Login') as ProfileID, --default all to Login because there are not enought licenses.
	   'CentralSquare Community Standard - Login' as ProfileID_Name,
       CASE
         WHEN profilename_original = 'TriTech Portal Manager' THEN ( SELECT id
                                                                     FROM   superion_production.dbo.PROFILE
                                                                     WHERE  name = 'CentralSquare Community Standard - Dedicated' )
         ELSE ( SELECT id
                FROM   superion_production.dbo.PROFILE
                WHERE  name = 'CentralSquare Community Standard - Login' )
       END   AS ProfileId_transform,
      CASE
         WHEN profilename_original = 'TriTech Portal Manager' THEN 'CentralSquare Community Standard - Dedicated'
         ELSE 'CentralSquare Community Standard - Login'
       END   AS ProfileId_Name_transform,
       error AS User_CommunityUser_Load_RT_error
INTO   User_CommunityUser_Update_profile_MC
FROM   USER_COMMUNITYUSER_LOAD_RT cul
WHERE  cul.error = 'Operation Successful.'  --(8354 row(s) affected)

alter table User_CommunityUser_Update_profile_MC add error nvarchar(255);

--checks
select ProfileId_Name , count(*)
from
User_CommunityUser_Update_profile_MC
group by ProfileId_Name;

/*

select * from User_CommunityUser_Update_profile_MC

select * from User_CommunityUser_Update_profile_MC where profileid is null;

select * 
from sys.tables
where name like 'user%' and  name like '%Community%'
order by create_date desc;

select * from User_CommunityUser_Load_RT_Orig --8527
select * from User_CommunityUser_Load_RT_HF_Duplicate_Nickname
select * from User_CommunityUser_Load_RT_HF_Unable_Exclusive_Access
select * from User_CommunityUser_Load_RT where id = '0052G000009IPMXQA4' or Legacy_Tritech_Id__c = '0058000000AIu6JAAT'
User_CommunityUser_PreLoad_RT
*/


