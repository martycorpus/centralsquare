/*
-- Author     : Marty Corpus	
-- Date       : 11/6/2018
-- Description: Migrate Requirement__c data from the Tritech Prod org to the Superion org (References, API: Contact_Reference_Link__c)
Requirement Number:REQ-0846
Scope:
Migrate all records that relate to a migrated opportunity.

2018-11-06 Missing 2 custome fields - config needed.
           1. [Reference_Document_Event__c]
		   2. [If_Other_Please_List_Document_Event__c]
2018-11-10 Scripted Missing fields.
           Opp load table pending - waiting for Patrick to load for UAT1
2018-11-12 Executed for UAT1 Load.
2018-12-04 Executed for UAT2 Load.
2018-12-18 Fix join to get account id.
2019-01-02 MartyC. Executed for E2E load.
2019-03-28 Moved to Production.
2019-03-29 MartyC. Executed for Production.

*/


USE [Tritech_PROD]

EXEC Sf_refresh 'MC_TRITECH_PROD','References__c','Yes';
--- Starting SF_Refresh for References__c V3.6.7
--16:30:03: Using Schema Error Action of yes
--16:30:03: Using last run time of 2019-03-29 02:12:00
--16:30:04: Identified 0 updated/inserted rows.
--16:30:04: Identified 0 deleted rows.
--- Ending SF_Refresh. Operation successful.


EXEC SF_Refresh 'MC_TRITECH_PROD','user','yes'

use Superion_Production;

EXEC SF_Refresh 'MC_SUPERION_PROD','Opportunity','yes'
--- Starting SF_Refresh for Opportunity V3.6.7
--16:32:46: Using Schema Error Action of yes
--16:32:47: Using last run time of 2019-03-29 15:41:00
--16:32:48: Identified 0 updated/inserted rows.
--16:32:48: Identified 0 deleted rows.
--- Ending SF_Refresh. Operation successful.


EXEC SF_Refresh 'MC_SUPERION_PROD','User','yes'

--- Starting SF_Refresh for User V3.6.7
16:33:12: Using Schema Error Action of yes
16:33:13: Using last run time of 2019-03-29 14:30:00
16:33:30: Identified 69 updated/inserted rows.
16:33:31: Identified 0 deleted rows.
16:33:31: Adding updated/inserted rows into User
--- Ending SF_Refresh. Operation successful.





USE [Staging_PROD]



go

-- drop table Contact_Reference_Link__c_Load_MC_Tritech

-- User: 0056A000000lfqkQAA	Superion API Fullsb

SELECT Cast( NULL AS NCHAR(18))         AS Id,
       Cast( NULL AS NVARCHAR(255))     AS Error,
       rf.[id]                          AS Legacy_Id__c,
       'true'                           AS Migrated_Record__c,
       'Tritech'                        AS Legacy_Source_System__c,
       coalesce(u.id,'0056A000000lfqkQAA')                            AS Ownerid,
	   tu.name + ' | ' + tu.Isactive       as CreatedbyIdName_orig,
	   rf.Account__c                    AS Account_orig ,
       coalesce(a.id,'ID NOT FOUND')    AS Account__c, --12/18
       coalesce(opp.id,'ID NOT FOUND')  AS Opportunity__c, --11/9 waiting on Partick's opp load.
       rf.[reference_document_event__c] AS [Reference_Document_Event__c], --11/9
       rf.[if_other_please_list_document_event__c] as [If_Other_Please_List_Document_Event__c], --11/9
       rf.[createdbyid]                 [CreatedById_orig],
       rf.[createddate]                 AS [CreatedDate_orig],
       rf.[isdeleted]                   as [isdeleted_orig]  ,
       rf.[lastactivitydate]            AS [LastActivityDate_orig],
       rf.[lastmodifiedbyid]            AS [LastModifiedById_orig],
       rf.[lastmodifieddate]            AS [LastModifiedDate_orig],
       rf.[name]                        AS [Name_orig],
       rf.[opportunity__c]              AS [Opportunity_orig],
       rf.[systemmodstamp]              AS [SystemModstamp_orig]
INTO   Contact_Reference_Link__c_Load_MC_Tritech
FROM   [Tritech_PROD].[dbo].[REFERENCES__C] rf
       LEFT JOIN [Tritech_PROD].[dbo].[user] tu on tu.id = rf.CreatedById
       LEFT JOIN Superion_Production.dbo.[Opportunity] opp
              ON opp.legacy_opportunity_id__c = rf.opportunity__c --11/9 waiting on Partick's opp load.
       LEFT JOIN Superion_Production.dbo.[USER] u
              ON u.[Legacy_Tritech_Id__c] = rf.createdbyid
       LEFT JOIN [MC_SUPERION_PROD]...[Account] a --12/18
              ON a.legacysfdcaccountid__c = rf.account__c; --(150 row(s) affected) 11/12
			  --(173 row(s) affected) MC 3/29 PROD

  


select  * from Contact_Reference_Link__c_Load_MC_Tritech



--checks:

--check for dups: select Legacy_Id__c, count(*) from Contact_Reference_Link__c_Load_MC_Tritech group by Legacy_Id__c having count(*) > 1 --3/29 E2E MC 0 records
--checK for account ids not found: select * from Contact_Reference_Link__c_Load_MC_Tritech where account__c is null or account__c = 'ID NOT FOUND'  --3/29 E2E MC MC 0 records
--check for   oppty ids not found: select * from Contact_Reference_Link__c_Load_MC_Tritech where Opportunity__c is null or account__c = 'ID NOT FOUND' --3/29 E2E MC 0 records
--check count of legacy source table: select count(*) from [Tritech_PROD].[dbo].[REFERENCES__C] --173 3/29 E2E MC
--check piclist values: select distinct Reference_Document_Event__c from Contact_Reference_Link__c_Load_MC_Tritech;
/*Demo
Demo
Other
Proactive Proposal
Reference Letter
RFX Response
*/

-- exec sf_colcompare 'Insert','MC_SUPERION_PROD','Contact_Reference_Link__c_Load_MC_Tritech'
/*
--- Starting SF_ColCompare V3.6.7
Problems found with Contact_Reference_Link__c_Load_MC_Tritech. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 125]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object Contact_Reference_Link__c does not contain column CreatedbyIdName_orig
Salesforce object Contact_Reference_Link__c does not contain column Account_orig
Salesforce object Contact_Reference_Link__c does not contain column CreatedById_orig
Salesforce object Contact_Reference_Link__c does not contain column CreatedDate_orig
Salesforce object Contact_Reference_Link__c does not contain column isdeleted_orig
Salesforce object Contact_Reference_Link__c does not contain column LastActivityDate_orig
Salesforce object Contact_Reference_Link__c does not contain column LastModifiedById_orig
Salesforce object Contact_Reference_Link__c does not contain column LastModifiedDate_orig
Salesforce object Contact_Reference_Link__c does not contain column Name_orig
Salesforce object Contact_Reference_Link__c does not contain column Opportunity_orig
Salesforce object Contact_Reference_Link__c does not contain column SystemModstamp_orig

*/

-- alter table Contact_Reference_Link__c_Load_MC_Tritech add ident int identity(1,1);
select * from Contact_Reference_Link__c_Load_MC_Tritech;



 -- exec SF_BulkOps 'Insert','MC_SUPERION_PROD','Contact_Reference_Link__c_Load_MC_Tritech';

 /*
--- Starting SF_BulkOps for Contact_Reference_Link__c_Load_MC_Tritech V3.6.7
16:38:11: Run the DBAmp.exe program.
16:38:11: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
16:38:11: Inserting Contact_Reference_Link__c_Load_MC_Tritech (SQL01 / Staging_PROD).
16:38:12: DBAmp is using the SQL Native Client.
16:38:12: SOAP Headers: 
16:38:12: Warning: Column 'CreatedbyIdName_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'Account_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'CreatedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'CreatedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'isdeleted_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'LastActivityDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'LastModifiedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'LastModifiedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'Name_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'Opportunity_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'SystemModstamp_orig' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:12: Warning: Column 'ident' ignored because it does not exist in the Contact_Reference_Link__c object.
16:38:13: 173 rows read from SQL Table.
16:38:13: 1 rows failed. See Error column of row for more information.
16:38:13: 172 rows successfully processed.
16:38:13: Errors occurred. See Error column of row for more information.
16:38:13: Percent Failed = 0.600.
16:38:13: Error: DBAmp.exe was unsuccessful.
16:38:13: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert Contact_Reference_Link__c_Load_MC_Tritech "SQL01"  "Staging_PROD"  "MC_SUPERION_PROD"  " " 
--- Ending SF_BulkOps. Operation FAILED.
Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 151]
SF_BulkOps Error: 16:38:11: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC16:38:11: Inserting Contact_Reference_Link__c_Load_MC_Tritech (SQL01 / Staging_PROD).16:38:12: DBAmp is using the SQL Native Client.16:38:12: SOAP Headers: 16:38:12: Warning: Column 'CreatedbyIdName_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'Account_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'CreatedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'CreatedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'isdeleted_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'LastActivityDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'LastModifiedById_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'LastModifiedDate_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'Name_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'Opportunity_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'SystemModstamp_orig' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:12: Warning: Column 'ident' ignored because it does not exist in the Contact_Reference_Link__c object.16:38:13: 173 rows read from SQL Table.16:38:13: 1 rows failed. See Error column of row for more information.16:38:13: 172 rows successfully processed.16:38:13: Errors occurred. See Error column of row for more information.

select * from Contact_Reference_Link__c_Load_MC_Tritech 
where error not like '%success%'


*/