/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: ChatterGroup (CollaborationGroup) - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 15.1 Chatter Groups Migration PB.sql
02/26/2019		Ron Tecson			Configured for MapleRoots connection. Loaded 13 records.
03/19/2019		Ron Tecson			Loaded 16 records.
03/28/2019		Ron Tecson			Repointed to Production.
03/29/2019		Ron Tecson			Loaded Production with out error.

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------
Use Tritech_Prod

Exec SF_Refresh 'MC_Tritech_Prod', 'CollaborationGroup', 'yes';

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
Use Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroup_PreLoad_RT') 
DROP TABLE Staging_PROD.dbo.CollaborationGroup_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_PROD;

Select
	CAST('' as nchar(18)) as [ID],
	CAST('' as nvarchar(255)) as Error,
	a.Name as Name,
	a.CollaborationType as CollaborationType,
	a.[Description] as [Description],
	a.InformationTitle as InformationTitle,
	a.InformationBody as InformationBody,
	a.CanHaveGuests as CanHaveGuests,
	a.IsArchived as IsArchived,
	a.IsAutoArchiveDisabled as IsAutoArchiveDisabled,
	a.IsBroadcast as IsBroadCast
INTO Staging_PROD.dbo.CollaborationGroup_PreLoad_RT
FROM TriTech_Prod.dbo.CollaborationGroup a
WHERE a.IsArchived = 'false'
	AND a.MemberCount > 0
	AND a.LastFeedModifiedDate >= CAST('09/30/2018' as Datetime)

-- 3/29 (17 rows affected)

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
USE Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='CollaborationGroup_Load_RT') 
DROP TABLE Staging_PROD.dbo.CollaborationGroup_Load_RT;

SELECT *
INTO Staging_PROD.dbo.CollaborationGroup_Load_RT
FROM Staging_PROD.dbo.CollaborationGroup_PreLoad_RT

-- 3/29 (17 rows affected)

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE Staging_PROD.dbo.CollaborationGroup_load_RT
ADD [Sort] int identity (1,1)


---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
use Staging_PROD

Exec SF_ColCompare 'insert', 'RT_Superion_PROD', 'CollaborationGroup_load_RT'

/********************************** COLCOMPARE L   O   G   ********************************************************

--- Starting SF_ColCompare V3.6.7
Problems found with CollaborationGroup_load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 86]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object CollaborationGroup does not contain column Sort

********************************** L   O   G   ********************************************************/

Exec SF_BulkOps 'insert:bulkapi,batchsize(4000)', 'RT_Superion_PROD', 'CollaborationGroup_load_RT'

/********************************** L   O   G   ********************************************************

3/29
--- Starting SF_BulkOps for CollaborationGroup_load_RT V3.6.7
15:41:37: Run the DBAmp.exe program.
15:41:37: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:41:37: Inserting CollaborationGroup_load_RT (SQL01 / Staging_PROD).
15:41:37: DBAmp is using the SQL Native Client.
15:41:37: Batch size reset to 4000 rows per batch.
15:41:38: Sort column will be used to order input rows.
15:41:38: SF_Bulkops will poll every 60 seconds for up to 3600 seconds.
15:41:39: Job 7502G000009dOVlQAM created.
15:41:39: Batch 7512G00000G5Mf0QAF created with 17 rows.
15:41:39: Job submitted.
15:41:39: 17 rows read from SQL Table.
15:41:39: Job Complete.
15:41:40: DBAmp is using the SQL Native Client.
15:41:40: 17 rows successfully processed.
15:41:40: 0 rows failed.
15:41:40: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

********************************** L   O   G   ********************************************************/

---------------------------------------------------------------------------------
-- Refresh Data to the local database
---------------------------------------------------------------------------------
USE Superion_Production;

EXEC SF_Refresh 'RT_Superion_PROD', 'CollaborationGroup', 'yes';