/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Survey Feedback Object Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 02/07/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/07/2019		Ron Tecson			Initial. Copied and modified from 11.1 Survey Feedback Migration PB.sql
03/30/2019		Ron Tecson			Executed in Production. Total Number of Records imported: 2850

DECISIONS:

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Data in the Local Database
---------------------------------------------------------------------------------

USE Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_Prod', 'Account', 'yes' ;
EXEC SF_Refresh 'MC_Tritech_Prod', 'Contact', 'yes' ;

USE SUPERION_Production
EXEC SF_Refresh 'RT_SUPERION_PROD', 'Contact', 'yes' -- 6 mins
EXEC SF_Refresh 'RT_SUPERION_PROD', 'RecordType', 'yes' ;
EXEC SF_Refresh 'RT_SUPERION_PROD', 'Account', 'yes' ; -- 2 mins

---------------------------------------------------------------------------------
-- Drop Preload tables 
---------------------------------------------------------------------------------
Use Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Survey_Feedback__c_PreLoad_RT') 
DROP TABLE Survey_Feedback__c_PreLoad_RT;

---------------------------------------------------------------------------------
-- Load Staging Table
---------------------------------------------------------------------------------
USE Staging_PROD;

SELECT
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.[ID] as Legacy_Id__c,
	'TriTech' as Legacy_Source_System__c,
	'true' as Migrated_Record__c,
	RT.ID as RecordTypeID,
	a.NPS_Score__c as Legacy_NPS_Score__c,
	a.NPS_Survey_Contact__c as NPS_Survey_Contact_Original,
	Cont.ID as Contact2__c,
	a.Survey_Notes__c as Customer_Comments__c,
	a.ID as Account_Original__c,
	Acct.ID as Account__c,
	a.Survey_Date__c as CreatedDate
INTO Survey_Feedback__c_PreLoad_RT
FROM Tritech_PROD.dbo.Account a
LEFT OUTER JOIN SUPERION_Production.dbo.RecordType RT on 'NPS' = RT.[Name]							----> Record Type
LEFT OUTER JOIN SUPERION_Production.dbo.Contact Cont ON a.NPS_Survey_Contact__c = Cont.Legacy_ID__c ----> Contact
LEFT OUTER JOIN SUPERION_Production.dbo.Account Acct ON a.ID = Acct.LegacySFDCAccountId__c			----> Account
WHERE a.NPS_Score__c is not null

-- 3/30 Production: (2850 rows affected)

---------------------------------------------------------------------------------
-- Drop Load tables if exist
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Survey_Feedback__c_Load_RT') 
DROP TABLE Survey_Feedback__c_Load_RT;

SELECT *
INTO Staging_PROD.dbo.Survey_Feedback__c_Load_RT
FROM Staging_PROD.dbo.Survey_Feedback__c_PreLoad_RT

-- 3/30 Production: (2850 rows affected)

---------------------------------------------------------------------------------
-- Add sort column to speed bulk load performance
---------------------------------------------------------------------------------
ALTER TABLE  Staging_PROD.dbo.Survey_Feedback__c_Load_RT
ADD [Sort] int identity (1,1)

---------------------------------------------------------------------------------
-- Insert/Update records
---------------------------------------------------------------------------------
USE Staging_PROD

--EXEC SF_BulkOps 'upsert:bulkapi,batchsize(5000)', 'RT_SUPERION_PROD', 'Survey_Feedback__c_Load_RT', 'Legacy_Id__c';
EXEC SF_BulkOps 'insert', 'RT_SUPERION_PROD', 'Survey_Feedback__c_Load_RT'

/************************************* L   O   G   S **************************************
3/30 -- Production:

--- Starting SF_BulkOps for Survey_Feedback__c_Load_RT V3.6.7
23:22:48: Run the DBAmp.exe program.
23:22:48: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
23:22:48: Inserting Survey_Feedback__c_Load_RT (SQL01 / Staging_PROD).
23:22:49: DBAmp is using the SQL Native Client.
23:22:49: SOAP Headers: 
23:22:49: Warning: Column 'NPS_Survey_Contact_Original' ignored because it does not exist in the Survey_Feedback__c object.
23:22:49: Warning: Column 'Account_Original__c' ignored because it does not exist in the Survey_Feedback__c object.
23:22:49: Warning: Column 'Sort' ignored because it does not exist in the Survey_Feedback__c object.
23:23:12: 2850 rows read from SQL Table.
23:23:12: 2850 rows successfully processed.
23:23:12: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

************************************** L   O   G   S **************************************/

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------
SELECT ERROR, count(*)
FROM Survey_Feedback__c_Load_RT
GROUP BY ERROR

select * from Survey_Feedback__c_Load_RT where error not like '%success%'

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE SUPERION_Production

EXEC SF_Refresh 'RT_SUPERION_PROD', 'Survey_Feedback', 'yes'
