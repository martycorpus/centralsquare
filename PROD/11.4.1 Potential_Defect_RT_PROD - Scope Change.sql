/*****************************************************************************************************************************************************
REQ #		: REQ-0831
TITLE		: Migrate Potential_Defect__c - Source System to SFDC
DEVELOPER	: RTECSON
CREATED DT  : 12/07/2018
DETAIL		: 	
				Selection Criteria:
				a. Migrate all related defects related to a migrated case.

				Discussion: Will migrate to Engineering_Issue__c
				need to Validate with Engineering what their requirements are, and need to also confirm requirements specific to defects NOT related to a case.
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
03/30/2019		Ron Tecson			Applied a hotfix per Jeff Beard ( 1 of 2). The 2nd Hotfix is another script.

DECISIONS:

ERROR/FAILURES:

DATE			ERROR													RESOLUTION
=============== ======================================================= ========================================================================================
03/20/2019		Support Account: id value of incorrect type: Account	Accounts associated to the Tritech Cases doesn't exists in the target Org.
					Not Found

******************************************************************************************************************************************************/

USE Tritech_PROD

EXEC SF_Refresh 'MC_TRITECH_PROD',Potential_Defect__c, Yes 

USE SUPERION_Production

	EXEC SF_Refresh 'RT_Superion_PROD',EngineeringIssue__c, Yes
	EXEC SF_Refresh 'RT_Superion_PROD',Account, Yes
	EXEC SF_Refresh 'RT_Superion_PROD','User', Yes
	EXEC SF_Refresh 'RT_Superion_PROD','Version__c', Yes


USE Staging_PROD

IF EXISTS
   ( SELECT *
     FROM   information_schema.TABLES
     WHERE  table_name = 'EngineeringIssue__c_Preload_SC1' AND
            table_schema = 'dbo' )
  DROP TABLE Staging_PROD.[dbo].EngineeringIssue__c_Preload_SC1; 

DECLARE @UserDefault as NVARCHAR(18) = (select id from SUPERION_Production.dbo.[USER] where Name = 'Superion API'),
		@VersionDefault as NVARCHAR(18) = (select id from SUPERION_Production.dbo.[Version__c] where Name = 'PSJ-Inform v1' and LegacyTTZProductGroup__c  = 'TC CAD/Mobile');

select 
ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
,sPD1.Id AS Legacy_Id__c
,IIF(tAcc2.Id IS NOT NULL, sPD1.Original_Customer__c, sPD1.Account_Name__c) AS AccountId_Orig
,sPD1.Original_Contact__c AS Contact_Orig
,IIF(tCon1.Id IS NOT NULL,tCon1.ID,NULL) AS Support_Contact__c
,CASE sPD1.Area_Path__c
	WHEN 'CustomInterface' THEN 'TriTech-IntegratedSolutions'
	WHEN 'EMSBilling' THEN 'Respond-Billing'
	WHEN 'ePCR' THEN 'Respond-ePCR'
	WHEN 'Inform 5' THEN 'TriTech-IQ'
	WHEN 'Inform 911' THEN 'Tritech-911'
	WHEN 'InformCAD911' THEN 'Inform-CAD911'
	WHEN 'IntegratedSolutions' THEN 'TriTech-IntegratedSolutions'
	WHEN 'TTMS' THEN 'TriTech-MessageSwitch'
	WHEN 'VisionCAD MOBILE MOL' THEN 'Vision-CADMobile'
	WHEN 'VisionRMSJAILFBRFIRE MOL' THEN 'Inform-RMS'
	ELSE sPD1.Area_Path__c
 END AS ENGProject__c
,sPD1.CreatedById AS CreatedById_Orig
,IIF(tUser1.Id IS NOT NULL, tUser1.Id, @UserDefault) AS CreatedById -- 0056A000000lfqkQAA
,sPD1.CreatedDate AS CreatedDate
,sPD1.Defect_Ticket_ID__c AS Legacy_TT_Defect_Number__c
,CASE 
	WHEN sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NOT NULL AND sPD1.CCB_Notes__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10),CAST(sPD1.TFS_Comments__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.TFS_Comments__c as nvarchar(max)), CHAR(10), 'CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CONCAT(CAST(sPD1.Description__c as nvarchar(max)), CHAR(10), CAST(sPD1.TFS_Comments__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NOT NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CONCAT('CCB Notes: ', CAST(sPD1.CCB_Notes__c as nvarchar(max)))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NOT NULL AND sPD1.TFS_Comments__c IS NULL THEN
		CAST(sPD1.Description__c as nvarchar(max))
	WHEN sPD1.CCB_Notes__c IS NULL AND sPD1.Description__c IS NULL AND sPD1.TFS_Comments__c IS NOT NULL THEN
		CAST(sPD1.TFS_Comments__c as nvarchar(max))
	ELSE NULL
END AS Technical_Description__c
,sPD1.Id AS Legacy_CRMId
,sPD1.Internal_Steps_to_Recreate_Issue__c AS TroubleshootingSteps__c
,sPD1.MOL_Score__c AS ENGEngineering_Value__c
,sPD1.Name AS Summary__c
,sPD1.OwnerId  AS OwnerID_Orig
,sPD1.Patch_Release_Branch__c AS Patch_Release_Branch__c
,sPD1.Patch_Status_Comments__c AS Patch_Status_Comments__c
,CASE 
	WHEN sPD1.Priority__c like '%1' THEN '1 - Urgent'
	WHEN sPD1.Priority__c like '%2' THEN '2 - Critical'
	WHEN sPD1.Priority__c like '%3' THEN '3 - Non Critical'
	ELSE '4 - Minor'
 END AS Priority__c
,sPD1.Released_In__c AS ReleasedIn__c
,sPD1.Resolution_Notes__c AS ENGResolution__c
,sPD1.Priority__c as sPD1_Priority__c_Orig
,sPD1.Priority__c AS Severity__c
,tSOwner.Id AS Support_Owner__c
,sPD1.Target_Patch_Release_Date__c AS Target_Patch_Release_Date__c
,sPD1.TFS_Area_Path__c AS sPD1_TFS_Area_Path__c_Orig
,IIF(sPD1.TFS_Area_Path__c IS NULL, sPD1.Area_Path__c, sPD1.TFS_Area_Path__c) AS ENGAreaPath__c
,sPD1.TFS_Collection__c AS Collection__c
, CASE	
	WHEN sPD1.Ticket_Status__c = 'Targeted for Future Release' THEN 'New'
  END AS ENGEngineering_Status__c
, sCase1.Patch_Requested_Date_Time__c AS Patch_Requested_Date_Time__c
, CASE
	WHEN sCase1.Known_Issue_Confirmed__c = 'true' THEN 'Approved' 
	WHEN sCase1.Known_Issues_List__c = 'true' and sCase1.Known_Issue_Confirmed__c = 'false' THEN 'Submitted'
END AS KnownIssueStatus__c
, sCase1.Patch_Status__c as Patch_Status__c_Orig
, CASE 
	WHEN sCase1.Patch_Status__c = 'Assess for Patch' OR sCase1.Patch_Status__c = 'Pending Decision to Assess' OR sCase1.Patch_Status__c = 'Pending Decision to Patch' THEN 'Patch Requested'
	ELSE sCase1.Patch_Status__c
END AS Patch_Status__c
, IIF(sCase1.Known_Issue_Confirmed__c = 'true', sCase1.Known_Issue_Description__c, NULL) AS KnownIssueDescription__c
, sPD1.TFS_Work_Item__c AS TFSWorkItemLegacyID__c
, tCase2.AccountId AS Support_Account__c
, 'TFS' as Target_System__c
, sPD1.TFS_Work_Item__c as ENGIssue__c
, sCase1.Id as srcCaseId
, sAccount.Name AS src_AccountName
, sAccount.Id As Legacy_Account_Id
, @VersionDefault AS Version__c
INTO Staging_PROD.[dbo].[EngineeringIssue__c_Preload_SC1] --3/28 
FROM tritech_Prod.dbo.[case] sCase1
JOIN [Tritech_PROD].[dbo].[Potential_Defect__c] sPD1 ON sPD1.Defect_Ticket_ID__c = sCase1.Defect_Number__c
LEFT JOIN SUPERION_Production.dbo.Account tAcc1 ON tAcc1.LegacySFDCAccountId__c = sPD1.Account_Name__c
LEFT JOIN SUPERION_Production.dbo.[User] tUser1 ON tUser1.Legacy_Tritech_Id__c = sPD1.CreatedById
LEFT JOIN SUPERION_Production.dbo.[User] tOwner ON tOwner.Legacy_Tritech_Id__c = sPD1.OwnerId
LEFT JOIN SUPERION_Production.dbo.[User] tSOwner ON tSOwner.Email = sPD1.Support_Ticket_Owner_Email__c
LEFT JOIN SUPERION_Production.dbo.Account tAcc2 ON tAcc2.LegacySFDCAccountId__c = sPD1.Original_Customer__c
LEFT JOIN SUPERION_Production.dbo.Contact tCon1 ON tCon1.Legacy_ID__c = sPD1.Original_Contact__c
--LEFT JOIN Staging_PROD.dbo.Case_Tritech_SFDC_load tCase2 ON tCase2.Legacy_id__c = sCase1.Id
LEFT JOIN SUPERION_Production.dbo.[Case] tCase2 ON tCase2.Legacy_id__c = sCase1.Id
LEFT JOIN tritech_Prod.dbo.Account sAccount ON sAccount.Id = sCase1.AccountId -- 3/20 RT. Added for troublshooting purposes.
WHERE
sPD1.Ticket_Status__c in ('Targeted for Future Release') 

--  (82 rows affected) 3/30 Prod.

IF EXISTS
( SELECT *
    FROM   information_schema.TABLES
    WHERE  table_name = 'EngineeringIssue__c_Load_SC1' AND
        table_schema = 'dbo' )
DROP TABLE Staging_PROD.[dbo].EngineeringIssue__c_Load_SC1; 

SELECT *
INTO Staging_PROD.dbo.EngineeringIssue__c_Load_SC1
FROM Staging_PROD.dbo.[EngineeringIssue__c_Preload_SC1] 
WHERE TFSWorkItemLegacyID__c IS NOT NULL
--  (32 rows affected) 3/30 Prod.

select * from EngineeringIssue__c_Load_SC1 order by TFSWorkItemLegacyID__c

USE Staging_PROD

EXEC SF_ColCompare 'Insert','RT_Superion_PROD', 'EngineeringIssue__c_Load_SC1' 

/************************************* L   O   G   S **************************************
3/30
--- Starting SF_ColCompare V3.6.7
Problems found with EngineeringIssue__c_Load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 212]
--- Ending SF_ColCompare. Operation FAILED.

ErrorDesc
Salesforce object EngineeringIssue__c does not contain column AccountId_Orig
Salesforce object EngineeringIssue__c does not contain column Contact_Orig
Salesforce object EngineeringIssue__c does not contain column CreatedById_Orig
Salesforce object EngineeringIssue__c does not contain column Legacy_CRMId
Salesforce object EngineeringIssue__c does not contain column OwnerID_Orig
Salesforce object EngineeringIssue__c does not contain column sPD1_Priority__c_Orig
Salesforce object EngineeringIssue__c does not contain column sPD1_TFS_Area_Path__c_Orig
Salesforce object EngineeringIssue__c does not contain column Patch_Status__c_Orig
Salesforce object EngineeringIssue__c does not contain column srcCaseId
Salesforce object EngineeringIssue__c does not contain column src_AccountName
Salesforce object EngineeringIssue__c does not contain column Legacy_Account_Id
Column Target_System__c is not insertable into the salesforce object EngineeringIssue__c --this is a formula field so it is not insertable.


************************************** L   O   G   S **************************************/

EXEC SF_BulkOps 'INSERT','RT_Superion_PROD', 'EngineeringIssue__c_Load_SC1'

/************************************** L   O   G   S **************************************

3/30
--- Starting SF_BulkOps for EngineeringIssue__c_Load_SC1 V3.6.7
22:27:23: Run the DBAmp.exe program.
22:27:24: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
22:27:24: Inserting EngineeringIssue__c_Load_SC1 (SQL01 / Staging_PROD).
22:27:24: DBAmp is using the SQL Native Client.
22:27:24: SOAP Headers: 
22:27:25: Warning: Column 'AccountId_Orig' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'Contact_Orig' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'CreatedById_Orig' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'Legacy_CRMId' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'OwnerID_Orig' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'sPD1_Priority__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'sPD1_TFS_Area_Path__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'Patch_Status__c_Orig' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'Target_System__c' ignored because it not insertable in the EngineeringIssue__c object.
22:27:25: Warning: Column 'srcCaseId' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'src_AccountName' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: Warning: Column 'Legacy_Account_Id' ignored because it does not exist in the EngineeringIssue__c object.
22:27:25: 32 rows read from SQL Table.
22:27:25: 32 rows successfully processed.
22:27:25: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

************************************** L   O   G   S **************************************/

select ERROR, count(*)
from EngineeringIssue__c_Load_SC1
group by ERROR

/************************************** REFRESH LOCAL DATABASE **************************************/

USE SUPERION_Production
EXEC SF_Refresh 'RT_Superion_PROD',EngineeringIssue__c, Yes
