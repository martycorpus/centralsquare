-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 8.4 CurrentVersion_SPV_RegProd_Update_MC.sql
--- Developed for CentralSquare - Requested by Gabe Beadle.
--- Developed by Marty Corpus
--- Copyright Apps Associates 2019
--- Created Date: February 20, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-02-21 Base script, sent report to Gabe, Shane for validation.
--- 2019-02-22 Executed for update of cases in the Superion Fullsb.
--- 2019-02-27 Added filter from Gabe on the mapping sheet. 
--  2019-03-28 Move to Production.
--  2019-03-30 executed for Prod.
--- Addet mapping for Upgrade_Start_Version__c to 
--- "Use this source field where Case Record Type <> "Work Order Ticket" AND Work Order Type <> 'Upgrade' "
--- added mapping TT Upgrade_End_Version__c for UpgradingToVersion__c 
--- 
--- C:\Users\mcorpus\Documents\Superion\Central Square Repo\WIP\8.4 CurrentVersion_SPV_RegProd_Update_MC.sql
--- 
-------------------------------------------------------------------------------
/*
Notes:
This script will update the migrated TT cases current version field using these SPV script matching logic.


*/

/* 
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-03-30 Executed in Superion FullSbx.

exec SF_ColCompare 'Update','MC_SUPERION_PROD','Case_Update_Current_Version_MC_load'  
--- Starting SF_ColCompare V3.6.7
Problems found with Case_Update_Current_Version_MC_load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 35]
--- Ending SF_ColCompare. Operation FAILED.


ErrorDesc
Salesforce object Case does not contain column version_name
Salesforce object Case does not contain column Legacy_Ticket_Number
Salesforce object Case does not contain column Sup_Case_Number
Salesforce object Case does not contain column versionnumber__c_sup
Salesforce object Case does not contain column Product_Group
Salesforce object Case does not contain column Product_Line
Salesforce object Case does not contain column TT_Reported_Major_Version_WMP
Salesforce object Case does not contain column TT_Reported_Patch_WMP
Salesforce object Case does not contain column legacy_tt_case_id
Salesforce object Case does not contain column Product_ID
Salesforce object Case does not contain column Product_Name
Salesforce object Case does not contain column versionnumber__matchtype
Salesforce object Case does not contain column rank
Salesforce object Case does not contain column legacy_ticket_status
Salesforce object Case does not contain column Ident

exec SF_Bulkops 'Update','MC_SUPERION_PROD','Case_Update_Current_Version_MC_load' 
--- Starting SF_BulkOps for Case_Update_Current_Version_MC_load V3.6.7
02:40:00: Run the DBAmp.exe program.
02:40:01: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
02:40:01: Updating Salesforce using Case_Update_Current_Version_MC_load (SQL01 / Staging_PROD) .
02:40:01: DBAmp is using the SQL Native Client.
02:40:02: SOAP Headers: 
02:40:02: Warning: Column 'version_name' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'Legacy_Ticket_Number' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'Sup_Case_Number' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'versionnumber__c_sup' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'Product_Group' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'Product_Line' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'TT_Reported_Major_Version_WMP' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'TT_Reported_Patch_WMP' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'legacy_tt_case_id' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'Product_ID' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'Product_Name' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'versionnumber__matchtype' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'rank' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'legacy_ticket_status' ignored because it does not exist in the Case object.
02:40:02: Warning: Column 'Ident' ignored because it does not exist in the Case object.
02:57:47: 92697 rows read from SQL Table.
02:57:47: 92697 rows successfully processed.
02:57:47: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


exec SF_ColCompare 'Update','MC_SUPERION_PROD','Case_Update_UpgradingToVersion_MC_load'
--- Starting SF_ColCompare V3.6.7
Problems found with Case_Update_UpgradingToVersion_MC_load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 87]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object Case does not contain column UpgradingToVersion_Name
Salesforce object Case does not contain column tt_upgrade_end_version_c
Salesforce object Case does not contain column Legacy_Ticket_Number
Salesforce object Case does not contain column Sup_Case_Number
Salesforce object Case does not contain column Product_Group
Salesforce object Case does not contain column Product_Line
Salesforce object Case does not contain column legacy_tt_case_id
Salesforce object Case does not contain column Product_ID
Salesforce object Case does not contain column Product_Name
Salesforce object Case does not contain column rank
Salesforce object Case does not contain column legacy_ticket_status
Salesforce object Case does not contain column Ident


select * from Case_Update_UpgradingToVersion_MC_load
exec SF_Bulkops 'Update','MC_SUPERION_PROD','Case_Update_UpgradingToVersion_MC_load'

--- Starting SF_BulkOps for Case_Update_UpgradingToVersion_MC_load V3.6.7
03:17:55: Run the DBAmp.exe program.
03:17:55: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
03:17:55: Updating Salesforce using Case_Update_UpgradingToVersion_MC_load (SQL01 / Staging_PROD) .
03:17:56: DBAmp is using the SQL Native Client.
03:17:56: SOAP Headers: 
03:17:56: Warning: Column 'UpgradingToVersion_Name' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'tt_upgrade_end_version_c' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'Legacy_Ticket_Number' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'Sup_Case_Number' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'Product_Group' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'Product_Line' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'legacy_tt_case_id' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'Product_ID' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'Product_Name' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'rank' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'legacy_ticket_status' ignored because it does not exist in the Case object.
03:17:56: Warning: Column 'Ident' ignored because it does not exist in the Case object.
03:18:36: 3728 rows read from SQL Table.
03:18:36: 3728 rows successfully processed.
03:18:36: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


*/




/*

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------
Use Tritech_PROD;

Exec SF_Refresh 'MC_TRITECH_PROD','Case','Yes'

Exec SF_Refresh 'MC_TRITECH_PROD','product2','Yes'


Use [Superion_Production];

Exec SF_Refresh 'MC_SUPERION_PROD','product2','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Version__c','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Case','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','CUSTOMER_ASSET__C','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Registered_Product__c','Yes'

Exec SF_Refresh 'MC_SUPERION_PROD','Environment__c','Yes'
*/

---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_PROD;

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Case_Update_Current_Version_MC_load') 
DROP TABLE [Staging_PROD].dbo.Case_Update_Current_Version_MC_load;


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Case_Update_Current_Version_MC_preload')   
DROP TABLE [Staging_PROD].dbo.Case_Update_Current_Version_MC_preload; --2/15


--Create Preload table. 


WITH cte_version
     AS ( SELECT t1.id,
                 t1.replaceexistingwiththisversion__c,
                 t1.NAME,
                 t2.NAME AS ReplaceexistingwiththisVersion_Name,
                 t1.productline__c,
                 t1.legacyttzversion__c,
                 t1.versionnumber__c
          FROM   [Superion_Production].dbo.VERSION__C t1
                 LEFT JOIN [Superion_Production].dbo.VERSION__C t2
                        ON t2.id = t1.replaceexistingwiththisversion__c
          WHERE  t1.productline__c IS NOT NULL ),
     cte_version_majorrelease
     AS ( SELECT t1.id,
                 t1.replaceexistingwiththisversion__c,
                 t1.NAME,
                 t2.NAME AS ReplaceexistingwiththisVersion_Name,
                 t1.productline__c,
                 t1.legacymajorreleaseversion__c,
                 t1.legacyttzversion__c,
                 t1.versionnumber__c
          FROM   [Superion_Production].dbo.VERSION__C t1
                 LEFT JOIN [Superion_Production].dbo.VERSION__C t2
                        ON t2.id = t1.replaceexistingwiththisversion__c
          WHERE  t1.productline__c IS NOT NULL AND
                 t1.legacymajorreleaseversion__c IS NOT NULL AND
                 t1.legacyttzsource__c = 'Version reconciliation' --and t1.LegacyMajorReleaseVersion__c = '1.1'
         ),
     cte_version_2
     AS ( SELECT Row_number( )
                   OVER (
                     partition BY t1.versionnumber__c
                     ORDER BY (CASE WHEN t1.legacyttzsource__c = 'CS Import File' THEN 1 WHEN t1.legacyttzsource__c = 'Hardware Software' THEN 2 ELSE 3 END) ) AS "RANK",
                 t1.id,
                 t2.id                                                                                                                                         AS Legacy_TT_Case_id,
                 t1.replaceexistingwiththisversion__c,
                 t2.NAME                                                                                                                                       AS ReplaceexistingwiththisVersion_Name,
                 t1.NAME,
                 t1.productline__c,
                 t1.legacyttzversion__c,
                 t1.legacyttzproductgroup__c,
                 t1.product_group__c,
                 t1.versionnumber__c,
                 t1.legacyttzsource__c,
                 t1.current__c
          FROM   [Superion_Production].dbo.VERSION__C t1
                 LEFT JOIN [Superion_Production].dbo.VERSION__C t2
                        ON t2.id = t1.replaceexistingwiththisversion__c
         --WHERE  t1.legacyttzproductgroup__c = 'IMC'  OR
         --       t1.product_group__c = 'IMC' 
         )
SELECT
-- top 1 
DISTINCT CASE
           WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c, cte_mrv.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN COALESCE( cte_mrv.replaceexistingwiththisversion__c, cte_mrv.id )
           -- WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
           WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id )
           WHEN COALESCE( cte_vers_2a.replaceexistingwiththisversion__c, cte_vers_2a.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN COALESCE( cte_vers_2a.replaceexistingwiththisversion__c, cte_vers_2a.id )
           WHEN COALESCE( cte_mrv_start.replaceexistingwiththisversion__c, cte_mrv_start.id ) IS NOT NULL THEN COALESCE( cte_mrv_start.replaceexistingwiththisversion__c, cte_mrv_start.id )
           WHEN COALESCE( cte_vers_2_start.replaceexistingwiththisversion__c, cte_vers_2_start.id ) IS NOT NULL THEN COALESCE( cte_vers_2_start.replaceexistingwiththisversion__c, cte_vers_2_start.id )
           WHEN COALESCE( cte_vers_2a_start.replaceexistingwiththisversion__c, cte_vers_2a_start.id ) IS NOT NULL THEN COALESCE( cte_vers_2a_start.replaceexistingwiththisversion__c, cte_vers_2a_start.id )
           ELSE 'Id not found'
         END                                      AS Version__c,
         CASE
           WHEN COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME )
           -- WHEN  COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
           WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME )
           WHEN COALESCE( cte_vers_2a.replaceexistingwiththisversion_name, cte_vers_2a.NAME ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN COALESCE( cte_vers_2a.replaceexistingwiththisversion_name, cte_vers_2a.NAME )
           WHEN COALESCE( cte_mrv_start.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL THEN COALESCE( cte_mrv_start.replaceexistingwiththisversion_name, cte_mrv_start.NAME )
           -- WHEN  COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
           WHEN COALESCE( cte_vers_2_start.replaceexistingwiththisversion_name, cte_vers_2_start.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2_start.replaceexistingwiththisversion_name, cte_vers_2_start.NAME )
           WHEN COALESCE( cte_vers_2a_start.replaceexistingwiththisversion_name, cte_vers_2a_start.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2a_start.replaceexistingwiththisversion_name, cte_vers_2a_start.NAME )
           ELSE 'VERSION NOT FOUND'
         END                                      AS [VERSION_NAME],
         CASE
           WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c, cte_mrv.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN cte_mrv.versionnumber__c
           -- WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
           WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN cte_vers_2.versionnumber__c
           WHEN COALESCE( cte_vers_2a.replaceexistingwiththisversion__c, cte_vers_2a.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN cte_vers_2a.versionnumber__c
           WHEN COALESCE( cte_mrv_start.replaceexistingwiththisversion__c, cte_mrv_start.id ) IS NOT NULL THEN cte_mrv_start.versionnumber__c
           -- WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
           WHEN COALESCE( cte_vers_2_start.replaceexistingwiththisversion__c, cte_vers_2_start.id ) IS NOT NULL THEN cte_vers_2_start.versionnumber__c
           WHEN COALESCE( cte_vers_2a_start.replaceexistingwiththisversion__c, cte_vers_2a_start.id ) IS NOT NULL THEN cte_vers_2a_start.versionnumber__c
           ELSE 'Id not found'
         END                                      AS VersionNumber__c_Sup,
         CASE
           WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c, cte_mrv.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN 'cte_mrv'
           WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN 'cte_vers_2'
           WHEN COALESCE( cte_vers_2a.replaceexistingwiththisversion__c, cte_vers_2a.id ) IS NOT NULL AND
                COALESCE( rt.[name], 'x' ) <> 'Work Order Ticket' AND
                COALESCE( t2.[work_order_type__c], 'x' ) <> 'Upgrade' THEN 'cte_vers_2a'
           WHEN COALESCE( cte_mrv_start.replaceexistingwiththisversion__c, cte_mrv_start.id ) IS NOT NULL THEN 'cte_mrv_start'
           WHEN COALESCE( cte_vers_2_start.replaceexistingwiththisversion__c, cte_vers_2_start.id ) IS NOT NULL THEN 'cte_vers_2_start'
           WHEN COALESCE( cte_vers_2a_start.replaceexistingwiththisversion__c, cte_vers_2a_start.id ) IS NOT NULL THEN 'cte_vers_2a_start'
           ELSE 'Id not found'
         END                                      AS VersionNumber__Matchtype,
         --UpgradingToVersion__c
         CASE
           WHEN COALESCE( cte_mrv_end.replaceexistingwiththisversion__c, cte_mrv_end.id ) IS NOT NULL THEN COALESCE( cte_mrv_end.replaceexistingwiththisversion__c, cte_mrv_end.id )
           WHEN COALESCE( cte_vers_2_end.replaceexistingwiththisversion__c, cte_vers_2_end.id ) IS NOT NULL THEN COALESCE( cte_vers_2_end.replaceexistingwiththisversion__c, cte_vers_2_end.id )
           WHEN COALESCE( cte_vers_2a_end.replaceexistingwiththisversion__c, cte_vers_2a_end.id ) IS NOT NULL THEN COALESCE( cte_vers_2a_end.replaceexistingwiththisversion__c, cte_vers_2a_end.id )
           ELSE 'Id not found'
         END                                      AS UpgradingToVersion__c,
         CASE
           WHEN COALESCE( cte_mrv_end.replaceexistingwiththisversion_name, cte_mrv_end.NAME ) IS NOT NULL THEN COALESCE( cte_mrv_end.replaceexistingwiththisversion_name, cte_mrv_end.NAME )
           WHEN COALESCE( cte_vers_2_end.replaceexistingwiththisversion_name, cte_vers_2_end.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2_end.replaceexistingwiththisversion_name, cte_vers_2_end.NAME )
           WHEN COALESCE( cte_vers_2a_end.replaceexistingwiththisversion_name, cte_vers_2a_end.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2a_end.replaceexistingwiththisversion_name, cte_vers_2a_end.NAME )
           ELSE 'VERSION NOT FOUND'
         END                                      AS [UpgradingToVersion_Name],
         t2.upgrade_end_version__c                AS TT_Upgrade_End_Version_c,
         t2.reported_major_version_wmp__c,
         COALESCE( t2.reported_patch_wmp__c, '' ) AS Reported_Patch_WMP__c,
         t2.upgrade_start_version__c              AS TT_Upgrade_start_Version_c,
         t1.currentversion__c,
         t1.id,
         t2.id                                    AS Legacy_TT_case_id,
         t1.legacy_number__c,
         t1.casenumberformula__c,
         p2.NAME,
         t1.productid,
         p2.product_group__c,
         p2.product_line__c,
         t2.[status]                              AS Legacy_Ticket_Status,
         rt.id                                    AS TT_RecordtypeID,
         rt.[name]                                AS TT_RecordtypeID_Name,
         t2.[work_order_type__c]                  AS TT_Work_Order_Type
INTO   CASE_UPDATE_CURRENT_VERSION_MC_PRELOAD
FROM   [Superion_Production].dbo.[CASE] t1
       LEFT JOIN tritech_prod.dbo.[CASE] t2
              ON t2.id = t1.legacy_id__c
       LEFT JOIN [Superion_Production].dbo.[PRODUCT2] p2
              ON p2.id = t1.productid
       ------------------------------------------------------------------------------
       -- Joins for reported_major_version_wmp__c
       ------------------------------------------------------------------------------
       LEFT JOIN cte_version_majorrelease cte_mrv
              ON t2.reported_major_version_wmp__c = cte_mrv.legacymajorreleaseversion__c AND
                 cte_mrv.productline__c = p2.product_line__c
       LEFT JOIN cte_version_2 cte_vers_2
              ON COALESCE( cte_vers_2.legacyttzproductgroup__c, cte_vers_2.product_group__c ) = p2.product_group__c AND
                 cte_vers_2.legacyttzversion__c LIKE '%' + t2.reported_major_version_wmp__c + '%' AND
                 cte_vers_2.legacyttzversion__c LIKE '%' + t2.reported_patch_wmp__c + '%'
       LEFT JOIN cte_version_2 cte_vers_2a
              ON COALESCE( cte_vers_2a.legacyttzproductgroup__c, cte_vers_2a.product_group__c ) = p2.product_group__c AND
                 cte_vers_2a.legacyttzversion__c LIKE '%' + t2.reported_major_version_wmp__c + '%' AND
                 Substring( cte_vers_2a.versionnumber__c, 1, 1 ) = Substring( t2.reported_major_version_wmp__c, 1, 1 )
       ------------------------------------------------------------------------------
       -- Joins for Upgrade_Start_Version__c
       ------------------------------------------------------------------------------
       LEFT JOIN cte_version_majorrelease cte_mrv_start
              ON t2.upgrade_start_version__c = cte_mrv_start.legacymajorreleaseversion__c AND
                 cte_mrv_start.productline__c = p2.product_line__c
       LEFT JOIN cte_version_2 cte_vers_2_start
              ON COALESCE( cte_vers_2_start.legacyttzproductgroup__c, cte_vers_2_start.product_group__c ) = p2.product_group__c AND
                 cte_vers_2_start.legacyttzversion__c LIKE '%' + t2.upgrade_start_version__c + '%' AND
                 cte_vers_2_start.legacyttzversion__c LIKE '%' + t2.reported_patch_wmp__c + '%'
       LEFT JOIN cte_version_2 cte_vers_2a_start
              ON COALESCE( cte_vers_2a_start.legacyttzproductgroup__c, cte_vers_2a_start.product_group__c ) = p2.product_group__c AND
                 cte_vers_2a_start.legacyttzversion__c LIKE '%' + t2.upgrade_start_version__c + '%' AND
                 Substring( cte_vers_2a_start.versionnumber__c, 1, 1 ) = Substring( t2.upgrade_start_version__c, 1, 1 )
       ------------------------------------------------------------------------------
       -- Joins for Upgrade_End_Version__c
       ------------------------------------------------------------------------------
       LEFT JOIN cte_version_majorrelease cte_mrv_end
              ON t2.upgrade_end_version__c = cte_mrv_end.legacymajorreleaseversion__c AND
                 cte_mrv_end.productline__c = p2.product_line__c
       LEFT JOIN cte_version_2 cte_vers_2_end
              ON COALESCE( cte_vers_2_end.legacyttzproductgroup__c, cte_vers_2.product_group__c ) = p2.product_group__c AND
                 cte_vers_2_end.legacyttzversion__c LIKE '%' + t2.upgrade_end_version__c + '%' AND
                 cte_vers_2_end.legacyttzversion__c LIKE '%' + t2.reported_patch_wmp__c + '%'
       LEFT JOIN cte_version_2 cte_vers_2a_end
              ON COALESCE( cte_vers_2a_end.legacyttzproductgroup__c, cte_vers_2a_end.product_group__c ) = p2.product_group__c AND
                 cte_vers_2a_end.legacyttzversion__c LIKE '%' + t2.upgrade_end_version__c + '%' AND
                 Substring( cte_vers_2a_end.versionnumber__c, 1, 1 ) = Substring( t2.upgrade_end_version__c, 1, 1 )
       ------------------------------------------------------------------------------
       LEFT JOIN tritech_prod.dbo.[RECORDTYPE] rt
              ON rt.id = t2.recordtypeid
WHERE  ( t2.reported_major_version_wmp__c IS NOT NULL  OR
         t2.upgrade_end_version__c IS NOT NULL  OR
         t2.upgrade_start_version__c IS NOT NULL ) AND
         t1.productid IS NOT NULL AND
         t1.migrated_record__c = 'True' AND
         t1.legacy_source_system__c = 'Tritech'; 
--3/5 (23402 row(s) affected)
--3/26  (694603 row(s) affected) --00:12:30
-- 3/30 (730990 row(s) affected) 00:18:35



select count(*) from [Superion_Production].dbo.[CASE] t1 where t1.migrated_record__c = 'True' and
	   t1.legacy_Source_system__c = 'Tritech' and t1.productid IS NOT NULL --23905

select count(*) from [Superion_Production].dbo.[CASE] t1 where t1.migrated_record__c = 'True' and
	   t1.legacy_Source_system__c = 'Tritech' and t1.productid IS NOT NULL --287193

select /*top 100*/ t1.id, t1.ProductId, t1.Legacy_ID__c,t1.AccountId, t2.id as mr_caseid from [Superion_Production].dbo.[CASE] t1 
       left join [Superion_Production].dbo.[CASE] t2 on t2.legacy_id__c = t1.Legacy_ID__c
where t2.migrated_record__c = 'True' and
	   t2.legacy_Source_system__c = 'Tritech' and t1.productid IS NOT NULL --287193
	   and t2.productid is null

select id, productid from [MC_SUPERION_PROD]...[CASE]
where id = '5001h000003baGaAAI'

select * from   [MC_SUPERION_PROD]...[product2]
where id = '01t1h000003A0UkAAK' /*CreatedDate
2019-02-21 14:48:47.0000000*/


select * from [Superion_Production].dbo.product2
where id = '01t1h000003A0UkAAK' 


select Acronym_List__c, Legacy_SF_ID__c, id, name from   [MC_SUPERION_PROD]...[product2]
where Acronym_List__c = 'legacy ttz' --2259
and Legacy_SF_ID__c = '01t80000003rbiDAAQ'

select  Acronym_List__c, Legacy_SF_ID__c, id, name  from   [MC_SUPERION_PROD]...[product2]
where Acronym_List__c = 'legacy ttz'
and name like 'TE Client License%'

select  Acronym_List__c, Legacy_SF_ID__c, id, name  from   [MC_SUPERION_PROD]...[product2]
where id = 
'01t0v000002wBt3AAE'

-- drop table Case_Update_Current_Version_MC_load
--Create Load table  for CurrentVersion__c.
SELECT caseid_in_superion            AS Id,
       Cast( NULL AS NVARCHAR(255))  AS Error,
       version__c                    AS CurrentVersion__c,
       version_name,
       legacy_number__c              AS Legacy_Ticket_Number,
       casenumberformula__c          AS Sup_Case_Number,
       versionnumber__c_sup,
       product_group__c              AS Product_Group,
       product_line__c               AS Product_Line,
       reported_major_version_wmp__c AS TT_Reported_Major_Version_WMP,
       reported_patch_wmp__c         AS TT_Reported_Patch_WMP,
       legacy_tt_case_id,
       productid                     AS Product_ID,
       NAME                          AS Product_Name,
       versionnumber__matchtype,
       rank,
       legacy_ticket_status
INTO   CASE_UPDATE_CURRENT_VERSION_MC_LOAD
FROM   ( SELECT Row_number( )
                  OVER (
                    partition BY t1.id
                    ORDER BY (CASE WHEN t1.versionnumber__c_sup = t1.reported_major_version_wmp__c THEN 1 WHEN t1.versionnumber__c_sup = 'ID not Found' THEN 2 ELSE 3 END) ) AS "RANK",
                t1.version__c,
                t1.version_name,
                t1.versionnumber__c_sup,
                t1.product_group__c,
                t1.product_line__c,
                t1.versionnumber__matchtype,
                t1.reported_major_version_wmp__c,
                t1.reported_patch_wmp__c,
                t1.currentversion__c,
                t1.id                                                                                                                                                        CaseId_In_Superion,
                t1.legacy_tt_case_id,
                t1.legacy_number__c,
                t1.casenumberformula__c,
                t1.NAME,
                t1.productid,
                t1.legacy_ticket_status
         FROM   CASE_UPDATE_CURRENT_VERSION_MC_PRELOAD t1
         WHERE  t1.version__c <> 'Id not found' ) x
WHERE  [rank] = 1 AND
       reported_major_version_wmp__c NOT IN ( 'Other', 'Product Review', 'Roadmap', 'Support',
                                              'Support Issues', 'Undecided', 'Unplanned' ) --2/22 00:01:00(62375 row(s) affected)
											 --(83011 row(s) affected) 3/26
											 --(92697 row(s) affected)  affected) 3/30



ALTER TABLE Case_Update_Current_Version_MC_load ADD Ident INT IDENTITY(1,1);

SELECT * FROM Case_Update_Current_Version_MC_load;

-- drop table Case_Update_UpgradingToVersion_MC_preload
-- drop table Case_Update_UpgradingToVersion_MC_load
--Create Load table  for UpgradingToVersion__c.
SELECT caseid_in_superion           AS Id,
       Cast( NULL AS NVARCHAR(255)) AS Error,
       upgradingtoversion__c        AS UpgradingToVersion__c,
       [upgradingtoversion_name]    AS [UpgradingToVersion_Name],
       tt_upgrade_end_version_c,
       legacy_number__c             AS Legacy_Ticket_Number,
       casenumberformula__c         AS Sup_Case_Number,
       product_group__c             AS Product_Group,
       product_line__c              AS Product_Line,
       legacy_tt_case_id,
       productid                    AS Product_ID,
       NAME                         AS Product_Name,
       rank,
       legacy_ticket_status
INTO   Case_Update_UpgradingToVersion_MC_preload
FROM   ( SELECT Row_number( )
                  OVER (
                    partition BY t1.id
                    ORDER BY [upgradingtoversion_name] DESC ) AS "RANK",
                t1.upgradingtoversion__c,
                t1.[upgradingtoversion_name],
                t1.tt_upgrade_end_version_c,
                t1.product_group__c,
                t1.product_line__c,
                t1.currentversion__c,
                t1.id                                         CaseId_In_Superion,
                t1.legacy_tt_case_id,
                t1.legacy_number__c,
                t1.casenumberformula__c,
                t1.NAME,
                t1.productid,
                t1.legacy_ticket_status,
                t1.tt_recordtypeid_name,
                t1.tt_work_order_type
         FROM   CASE_UPDATE_CURRENT_VERSION_MC_PRELOAD t1
         WHERE  -- t1.UpgradingToVersion__c <> 'Id not found' AND
         t1.tt_recordtypeid_name = 'Work Order Ticket' AND
         t1.tt_work_order_type = 'Upgrade' ) x
WHERE  [rank] = 1;  --(8057 row(s) affected) 3/26

select * into Case_Update_UpgradingToVersion_MC_load from Case_Update_UpgradingToVersion_MC_preload where UpgradingToVersion__c <> 'Id not found';
--(3397 row(s) affected)
--(3728 row(s) affected)3/30


ALTER TABLE Case_Update_UpgradingToVersion_MC_load ADD Ident INT IDENTITY(1,1);

SELECT * FROM Case_Update_UpgradingToVersion_MC_load;


-------------dev querries-------------------

--duplicate version matches by case.
select * from Case_Update_Current_Version_MC_preload
where id in 
(
select id
from  Case_Update_Current_Version_MC_preload
group by id
having count(*) > 1
)
order by ID



--rank dupes

SELECT *
into Case_Update_Current_Version_MC_load
FROM 
(
SELECT Row_number( )
         over (
           PARTITION BY t1.ID
           ORDER BY (CASE WHEN t1.VersionNumber__c_Sup = t1.Reported_Major_Version_WMP__c THEN 1 when t1.VersionNumber__c_Sup = 'ID not Found' then 2 else 3 END) ) AS "RANK",
t1.Version__c,
t1.VERSION_NAME,
t1.VersionNumber__c_Sup,
t1.Product_Group__c,
t1.product_line__c,
t1.VersionNumber__Matchtype,
t1.Reported_Major_Version_WMP__c,
t1.Reported_Patch_WMP__c,
t1.CurrentVersion__c,
t1.id CaseId_In_Superion,
t1.Legacy_TT_case_id,
t1.Legacy_Number__c,
t1.CaseNumberFORMULA__c,
t1.name as ProductName,
t1.Productid
FROM   Case_Update_Current_Version_MC_preload t1
--WHERE T1.version__c <> 'Id not found'
) X  WHERE [RANK] = 1
and Reported_Major_Version_WMP__c
not in 
(
'Other',
'Product Review',
'Roadmap',
'Support',
'Support Issues',
'Undecided',
'Unplanned'
)
--and x.id = '5000v000003Ra0hAAC'
AND VERSION__C = 'id not found'


SELECT distinct Reported_Major_Version_WMP__c, Product_Group__c, product_line__c
FROM 
(
SELECT Row_number( )
         over (
           PARTITION BY t1.ID
           ORDER BY (CASE WHEN t1.VersionNumber__c_Sup = t1.Reported_Major_Version_WMP__c THEN 1 when t1.VersionNumber__c_Sup = 'ID not Found' then 2 else 3 END) ) AS "RANK",
t1.Version__c,
t1.VERSION_NAME,
t1.VersionNumber__c_Sup,
t1.Product_Group__c,
t1.product_line__c,
t1.VersionNumber__Matchtype,
t1.Reported_Major_Version_WMP__c,
t1.Reported_Patch_WMP__c,
t1.CurrentVersion__c,
t1.id,
t1.Legacy_TT_case_id,
t1.Legacy_Number__c,
t1.CaseNumberFORMULA__c,
t1.name,
t1.Productid
FROM   Case_Update_Current_Version_MC_preload t1
--WHERE T1.version__c <> 'Id not found'
) X  WHERE [RANK] = 1
and Reported_Major_Version_WMP__c
not in 
(
'Other',
'Product Review',
'Roadmap',
'Support',
'Support Issues',
'Undecided',
'Unplanned'
)
AND VERSION__C = 'id not found'
order by Product_Group__c, product_line__c, Reported_Major_Version_WMP__c;


SELECT * FROM Case_Update_Current_Version_MC_preload
WHERE Product_Group__c = 'tIBURON' AND	product_line__c =  'rms'
AND Reported_Major_Version_WMP__c = '7.10'



select id, count(*)
from  Case_Update_Current_Version_MC_preload
group by id
having count(*) > 1




--matches no dupes
select * from Case_Update_Current_Version_MC_preload
where id  not in 
(
select id
from  Case_Update_Current_Version_MC_preload
group by id
having count(*) > 1
)
and version__c <> 'Id not found'
and VersionNumber__c_Sup <> Reported_Major_Version_WMP__c
and substring(VersionNumber__c_Sup,1,1) <> substring(Reported_Major_Version_WMP__c,1,1)
order by ID


--unmatched version filtered
select * from Case_Update_Current_Version_MC_preload
where id  not in 
(
select id
from  Case_Update_Current_Version_MC_preload
group by id
having count(*) > 1
)
and version__c = 'Id not found'
and Reported_Major_Version_WMP__c
not in 
(
'Other',
'Product Review',
'Roadmap',
'Support',
'Support Issues',
'Undecided',
'Unplanned'
)
order by ID

select distinct Reported_Major_Version_WMP__c from Case_Update_Current_Version_MC_preload
where id  not in 
(
select id
from  Case_Update_Current_Version_MC_preload
group by id
having count(*) > 1
)
and version__c = 'Id not found'
order by Reported_Major_Version_WMP__c

select * from [Superion_Production].dbo.version__c
where id in (
'a9o0v0000004QxnAAE',
'a9o0v0000004QxrAAE',
'a9o0v0000004QxyAAE',
'a9o0v0000004QxtAAE',
'a9o0v0000004Qy6AAE',
'a9o0v0000004QyAAAU',
'a9o0v0000004QyFAAU')