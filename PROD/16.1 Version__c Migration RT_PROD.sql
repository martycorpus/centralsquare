/*****************************************************************************************************************************************************
REQ #		: 
TITLE		: Version__c Migration Script - Source System to SFDC
DEVELOPER	: Ron Tecson
CREATED DT  : 03/06/2019
DETAIL		: 
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
03/06/2019		Ron Tecson			Initial
03/26/2019		Ron Tecson			Modified the whole script and reloaded in MapleRoots. Execution Duration: 20 mins.
03/28/2019		Ron Tecson			Repointed to Production
03/29/2019		Ron Tecson			Executed in production with no errors. Execution Time: 15 mins.

DECISIONS:
	- 3/26. Approved by Gabe to default the CreatedById and OwnerId to "Superion API" Id.

******************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Drop Preload table
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Version__c_FullSB_Extract') 
DROP TABLE Staging_PROD.dbo.Version__c_FullSB_Extract;

---------------------------------------------------------------------------------
-- Extract the Data from Superion Full Sandbox.
---------------------------------------------------------------------------------
DECLARE @Default_UserId NVARCHAR(18) = (Select ID from SUPERION_Production.dbo.[User] where [Name] = 'Superion API');

SELECT 
	  Cast('' as nchar(18)) as [ID]
	  ,Cast('' as nvarchar(255)) as Error
      ,T1.ID AS [Legacy_SFDC_ID__c]
	  ,@Default_UserId AS CreatedById
	  ,T1.CreatedById AS CreatedById_Orig
      ,T1.[CreatedDate]
	  ,@Default_UserId As OwnerId
	  ,T1.OwnerID AS OwnerID_Orig
	  ,T1.[CurrencyIsoCode]
      ,T1.[Current__c]
      ,T1.[FullVersion_Id__c]
      ,@Default_UserId AS [LastModifiedById]
	  ,T1.[LastModifiedById] AS LastModifiedById_Orig
      ,T1.[LastModifiedDate]
      ,T1.[LastReferencedDate]
      ,T1.[LastViewedDate]
      ,T1.[Legacy_VersionId__c]
      ,T1.[LegacyHSName__c]
      ,T1.[LegacyMajorReleaseVersion__c]
      ,T1.[LegacyPatchNumber__c]
      ,T1.[LegacyTTZProductFamily__c]
      ,T1.[LegacyTTZProductGroup__c]
      ,T1.[LegacyTTZSource__c]
      ,T1.[LegacyTTZVersion__c]
      ,T1.[Name]
      ,T1.[Product_Group__c]
      ,T1.[Product_Line_Sub_Group__c]
      ,T1.[ProductFamily__c]
      ,T1.[ProductLine__c]
	  ,ReplaceexistingwiththisVersion__c
      ,T1.[ReplaceexistingwiththisVersion__c] AS ReplaceexistingwiththisVersion__c_Orig
      ,T1.[TTZ__c]
      ,T1.[VersionBuild__c]
      ,T1.[VersionCode__c]
      ,T1.[VersionNumber__c]
  INTO Version__c_FullSB_Extract
  FROM [Superion_FULLSB].[dbo].[Version__c] T1
  WHERE NOT EXISTS
		(SELECT * FROM RT_SUPERION_PROD...Version__c T2
			WHERE T1.Id = T2.Id)
	AND T1.TTZ__c = 'true' 

-- 03/29 -- (1905 rows affected) Production

---------------------------------------------------------------------------------
-- Drop staging tables 
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Version__c_Parent') 
DROP TABLE Staging_PROD.dbo.Version__c_Parent;

---------------------------------------------------------------------------------
-- Load the Version__c_Parent
---------------------------------------------------------------------------------
SELECT * 
INTO Version__c_Parent
FROM Version__c_FullSB_Extract 
WHERE ReplaceexistingwiththisVersion__c IS NULL

-- (1629 rows affected)

---------------------------------------------------------------------------------
-- Column Compare Parent Version__c record
---------------------------------------------------------------------------------
EXEC SF_ColCompare 'INSERT', 'RT_SUPERION_PROD', 'Version__c_Parent'

/************************** L O G *****************************************************

--- Starting SF_ColCompare V3.6.7
Problems found with Version__c_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 49]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Version__c does not contain column CreatedById_Orig
Salesforce object Version__c does not contain column OwnerID_Orig
Salesforce object Version__c does not contain column LastModifiedById_Orig
Salesforce object Version__c does not contain column ReplaceexistingwiththisVersion__c_Orig
Column FullVersion_Id__c is not insertable into the salesforce object Version__c
Column LastReferencedDate is not insertable into the salesforce object Version__c
Column LastViewedDate is not insertable into the salesforce object Version__c
Column TTZ__c is not insertable into the salesforce object Version__c
Column VersionCode__c is not insertable into the salesforce object Version__c

************************** L O G *****************************************************/

---------------------------------------------------------------------------------
-- Import Parent Version__c record
---------------------------------------------------------------------------------

EXEC SF_BulkOps 'INSERT', 'RT_SUPERION_PROD', 'Version__c_Parent'

/************************** L O G *****************************************************
-- 3/29
--- Starting SF_BulkOps for Version__c_Parent V3.6.7
15:15:08: Run the DBAmp.exe program.
15:15:08: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:15:08: Inserting Version__c_Parent (SQL01 / Staging_PROD).
15:15:08: DBAmp is using the SQL Native Client.
15:15:09: SOAP Headers: 
15:15:09: Warning: Column 'CreatedById_Orig' ignored because it does not exist in the Version__c object.
15:15:09: Warning: Column 'OwnerID_Orig' ignored because it does not exist in the Version__c object.
15:15:09: Warning: Column 'FullVersion_Id__c' ignored because it not insertable in the Version__c object.
15:15:09: Warning: Column 'LastModifiedById_Orig' ignored because it does not exist in the Version__c object.
15:15:09: Warning: Column 'LastReferencedDate' ignored because it not insertable in the Version__c object.
15:15:09: Warning: Column 'LastViewedDate' ignored because it not insertable in the Version__c object.
15:15:09: Warning: Column 'ReplaceexistingwiththisVersion__c_Orig' ignored because it does not exist in the Version__c object.
15:15:09: Warning: Column 'TTZ__c' ignored because it not insertable in the Version__c object.
15:15:09: Warning: Column 'VersionCode__c' ignored because it not insertable in the Version__c object.
15:15:16: 1629 rows read from SQL Table.
15:15:16: 1629 rows successfully processed.
15:15:16: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

************************** L O G *****************************************************/

---------------------------------------------------------------------------------
-- Validation of Version__c records
---------------------------------------------------------------------------------

select error,count(*) from Version__c_Parent
where error <> 'Operation Successful.'
group by error

---------------------------------------------------------------------------------
-- Drop staging table: Version__c_Child
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Version__c_Child') 
DROP TABLE Staging_PROD.dbo.Version__c_Child;

---------------------------------------------------------------------------------
-- Load the Version__c_Child
---------------------------------------------------------------------------------

SELECT 
	  Cast('' as nchar(18)) as [ID]
	  ,Cast('' as nvarchar(255)) as Error
      ,T1.[Legacy_SFDC_ID__c]
	  ,T1.CreatedById
	  ,T1.CreatedById AS CreatedById_Orig
      ,T1.[CreatedDate]
	  ,T1.OwnerId
	  ,T1.OwnerID AS OwnerID_Orig
	  ,T1.[CurrencyIsoCode]
      ,T1.[Current__c]
      ,T1.[FullVersion_Id__c]
      ,T1.[LastModifiedById]
	  ,T1.[LastModifiedById] AS LastModifiedById_Orig
      ,T1.[LastModifiedDate]
      ,T1.[LastReferencedDate]
      ,T1.[LastViewedDate]
      ,T1.[Legacy_VersionId__c]
      ,T1.[LegacyHSName__c]
      ,T1.[LegacyMajorReleaseVersion__c]
      ,T1.[LegacyPatchNumber__c]
      ,T1.[LegacyTTZProductFamily__c]
      ,T1.[LegacyTTZProductGroup__c]
      ,T1.[LegacyTTZSource__c]
      ,T1.[LegacyTTZVersion__c]
      ,T1.[Name]
      ,T1.[Product_Group__c]
      ,T1.[Product_Line_Sub_Group__c]
      ,T1.[ProductFamily__c]
      ,T1.[ProductLine__c]
	  ,T2.ID AS ReplaceexistingwiththisVersion__c
      ,T1.[ReplaceexistingwiththisVersion__c] AS ReplaceexistingwiththisVersion__c_Orig
      ,T1.[TTZ__c]
      ,T1.[VersionBuild__c]
      ,T1.[VersionCode__c]
      ,T1.[VersionNumber__c] 
INTO Version__c_Child
FROM Version__c_FullSB_Extract T1
LEFT JOIN Version__c_Parent T2 ON T2.[Legacy_SFDC_ID__c] = T1.ReplaceexistingwiththisVersion__c_Orig
WHERE T1.ReplaceexistingwiththisVersion__c IS NOT NULL

---------------------------------------------------------------------------------
-- Import Parent Version__c record
---------------------------------------------------------------------------------

EXEC SF_BulkOps 'INSERT', 'RT_SUPERION_PROD', 'Version__c_Child'

/************************** L O G *****************************************************

3/26:

--- Starting SF_BulkOps for Version__c_Child V3.6.7
15:13:46: Run the DBAmp.exe program.
15:13:46: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:13:46: Inserting Version__c_Child (SQL01 / Staging_SB_MapleRoots).
15:13:47: DBAmp is using the SQL Native Client.
15:13:47: SOAP Headers: 
15:13:47: Warning: Column 'CreatedById_Orig' ignored because it does not exist in the Version__c object.
15:13:47: Warning: Column 'OwnerID_Orig' ignored because it does not exist in the Version__c object.
15:13:47: Warning: Column 'FullVersion_Id__c' ignored because it not insertable in the Version__c object.
15:13:47: Warning: Column 'LastModifiedById_Orig' ignored because it does not exist in the Version__c object.
15:13:47: Warning: Column 'LastReferencedDate' ignored because it not insertable in the Version__c object.
15:13:47: Warning: Column 'LastViewedDate' ignored because it not insertable in the Version__c object.
15:13:47: Warning: Column 'ReplaceexistingwiththisVersion__c_Orig' ignored because it does not exist in the Version__c object.
15:13:47: Warning: Column 'TTZ__c' ignored because it not insertable in the Version__c object.
15:13:47: Warning: Column 'VersionCode__c' ignored because it not insertable in the Version__c object.
15:13:50: 276 rows read from SQL Table.
15:13:50: 276 rows successfully processed.
15:13:50: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.
************************** L O G *****************************************************/

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------
USE SUPERION_Production;

EXEC SF_Refresh 'RT_SUPERION_PROD', 'Version__c', 'Yes'
