-------------------------------------------------------------------------------
--- System Product Version load base 
--  Script File Name: 9.8 System_Product_Version__c_Insert_MC_Deltas.sql
--- Developed for CentralSquare
--- Developed by Marty Corpus
--- Copyright Apps Associates 2018
--- Created Date: April 8, 2019
--- Last Updated: 
--- Change Log: 
--- 2019-04-08 Created.
-- 
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Execution Log:
-- 

-- LOAD: 

-- COLCOMPARE:
-- use Superion_production;
-- exec SF_ColCompare 'Insert','MC_Superion_Prod','System_Product_Version__c_Insert_MC_delta_load'
/*
--- Starting SF_ColCompare V3.7.7
Problems found with System_Product_Version__c_Insert_MC_delta_load. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 21]
--- Ending SF_ColCompare. Operation FAILED.
ErrorDesc
Salesforce object System_Product_Version__c does not contain column Registered Product Name
Salesforce object System_Product_Version__c does not contain column Environment Name
Salesforce object System_Product_Version__c does not contain column VERSION_NAME
Salesforce object System_Product_Version__c does not contain column tt_legacy_id_hs
Salesforce object System_Product_Version__c does not contain column tt_hs_name
Salesforce object System_Product_Version__c does not contain column tt_legacysfdcaccountid__c
Salesforce object System_Product_Version__c does not contain column account__c
Salesforce object System_Product_Version__c does not contain column Account_Name
Salesforce object System_Product_Version__c does not contain column version
Salesforce object System_Product_Version__c does not contain column SourceFieldName
Salesforce object System_Product_Version__c does not contain column ProductGroup
Salesforce object System_Product_Version__c does not contain column ProductLine
Salesforce object System_Product_Version__c does not contain column EnvironmentType
Salesforce object System_Product_Version__c does not contain column Replace_Existing_Version
Salesforce object System_Product_Version__c does not contain column cte_vers.LegacyTTZVersion__c
Salesforce object System_Product_Version__c does not contain column cte_tt_hs.[Version
Salesforce object System_Product_Version__c does not contain column cte_mrv.LegacyMajorReleaseVersion__c
Salesforce object System_Product_Version__c does not contain column RegProd_ProductGroup
Salesforce object System_Product_Version__c does not contain column RegProd_ProductLine
Salesforce object System_Product_Version__c does not contain column RegProd_AccountId
Salesforce object System_Product_Version__c does not contain column rank

*/

/*
-- INSERT:
-- select * from System_Product_Version__c_Insert_MC_delta_load --
-- exec SF_Bulkops 'Insert','MC_Superion_Prod','System_Product_Version__c_Insert_MC_delta_load'

--- Starting SF_BulkOps for System_Product_Version__c_Insert_MC_delta_load V3.7.7
19:48:56: Run the DBAmp.exe program.
19:48:56: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
19:48:56: Inserting System_Product_Version__c_Insert_MC_delta_load (SQL01 / Staging_PROD).
19:48:56: DBAmp is using the SQL Native Client.
19:48:57: SOAP Headers: 
19:48:57: Warning: Column 'Registered Product Name' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'Environment Name' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'VERSION_NAME' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'tt_legacy_id_hs' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'tt_hs_name' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'tt_legacysfdcaccountid__c' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'account__c' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'Account_Name' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'version' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'SourceFieldName' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'ProductGroup' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'ProductLine' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'EnvironmentType' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'Replace_Existing_Version' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'cte_vers' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'cte_tt_hs' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'cte_mrv' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'RegProd_ProductLine' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the System_Product_Version__c object.
19:48:57: Warning: Column 'rank' ignored because it does not exist in the System_Product_Version__c object.
19:49:02: 1601 rows read from SQL Table.
19:49:02: 1601 rows successfully processed.
19:49:02: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


*/

---------------------------------------------------------------------------------
-- Replicate Data 
---------------------------------------------------------------------------------

Use Superion_Production;

Exec SF_Refresh 'MC_Superion_Prod','CUSTOMER_ASSET__C','Yes'
--- Starting SF_Refresh for CUSTOMER_ASSET__C V3.6.7
--18:03:14: Using Schema Error Action of yes
--18:03:14: Using last run time of 2019-04-05 19:28:00
--18:06:22: Identified 258000 updated/inserted rows.
--18:06:23: Identified 0 deleted rows.
--18:06:23: Adding updated/inserted rows into CUSTOMER_ASSET__C
--- Ending SF_Refresh. Operation successful.



Exec SF_Refresh 'MC_Superion_Prod','Product2','Yes'
--- Starting SF_Refresh for Product2 V3.6.7
--18:13:30: Using Schema Error Action of yes
--18:13:31: Using last run time of 2019-03-30 23:45:00
--18:14:50: Identified 26764 updated/inserted rows.
--18:14:50: Identified 3 deleted rows.
--18:14:50: Removing deleted rows from Product2
--18:14:50: Adding updated/inserted rows into Product2
--- Ending SF_Refresh. Operation successful.



Exec SF_Refresh 'MC_Superion_Prod','Registered_Product__c','Yes' --select count(*) from Registered_Product__c 
--- Starting SF_Refresh for Registered_Product__c V3.6.7
--18:15:34: Using Schema Error Action of yes
--18:15:34: Using last run time of 2019-04-05 19:15:00
--18:16:24: Identified 122622 updated/inserted rows.
--18:16:24: Identified 1 deleted rows.
--18:16:24: Removing deleted rows from Registered_Product__c
--18:16:24: Adding updated/inserted rows into Registered_Product__c
--- Ending SF_Refresh. Operation successful.



Exec SF_Refresh 'MC_Superion_Prod','Environment__c','Yes'
----- Starting SF_Refresh for Environment__c V3.6.7
--18:25:54: Using Schema Error Action of yes
--18:25:55: Using last run time of 2019-04-01 13:44:00
--18:26:06: Identified 21093 updated/inserted rows.
--18:26:06: Identified 0 deleted rows.
--18:26:06: Adding updated/inserted rows into Environment__c
--- Ending SF_Refresh. Operation successful.


Exec SF_Refresh 'MC_Superion_Prod','Version__c','Yes'
--- Starting SF_Refresh for Version__c V3.6.7
--18:27:12: Using Schema Error Action of yes
--18:27:12: Using last run time of 2019-03-30 23:46:00
--18:27:13: Identified 60 updated/inserted rows.
--18:27:13: Identified 0 deleted rows.
--18:27:13: Adding updated/inserted rows into Version__c
--- Ending SF_Refresh. Operation successful.




---------------------------------------------------------------------------------
-- Drop 
---------------------------------------------------------------------------------
Use Staging_PROD;

--IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_MC') 
--DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_MC;


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_MC_delta_load_delta_preload')   
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_MC_delta_load_delta_preload; --3/30

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='System_Product_Version__c_Insert_MC_delta_load_delta_load')  
DROP TABLE Staging_PROD.dbo.System_Product_Version__c_Insert_MC_delta_load_delta_preload; --3/30





---------------------------------------------------------------------------------
-- Create System_Product_Version__c Staging Load Table
---------------------------------------------------------------------------------

--Cte
WITH CTE_SUP_Environment As
(
SELECT Id, Name, Type__c, Account__c, [description__c] from Superion_Production.dbo.Environment__c 
),
CTE_Zuercher_Customer_assets as
(
SELECT t1.id as Zuercher_Registered_Product_Id,
       t1.name as Zuercher_Registered_Product_Name,
       t2.id   AS CustomerAsset__c,
       t2.name AS CustomerAsset_Name,
       t2.product_group__c as CUSTOMER_ASSET_Product_Group,
       p2.product_group__c,
       p2.product_line__c,
       t1.Account__c,
       a.Name as accountname
FROM   Superion_Production.dbo.REGISTERED_PRODUCT__C t1
        LEFT JOIN Superion_Production.dbo.PRODUCT2 p2
        ON p2.id = t1.product__c
       join Superion_Production.dbo.CUSTOMER_ASSET__C t2
         ON t2.id = t1.customerasset__c
       left join  Superion_Production.dbo.account a on a.Id = t1.Account__c 
WHERE  t2.product_group__c = 'Zuercher' 
),
Cte_version as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, t1.ProductLine__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_Production.dbo.version__c t1
left join Superion_Production.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
),
Cte_version_MajorRelease as
(
select t1.id,        t1.ReplaceexistingwiththisVersion__c, t1.name, t2.name as ReplaceexistingwiththisVersion_Name, 
t1.ProductLine__c, t1.LegacyMajorReleaseVersion__c,
t1.LegacyTTZVersion__c  ,  t1.versionnumber__c  
from Superion_Production.dbo.version__c t1
left join Superion_Production.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
where t1.ProductLine__c is not null
and t1.LegacyMajorReleaseVersion__c is not null
and t1.legacyttzsource__c = 'Version reconciliation' and t1.LegacyMajorReleaseVersion__c = '1.1'
),
Cte_version_2 as
(
SELECT Row_number( )
         over (
           PARTITION BY t1.versionnumber__c
           ORDER BY (CASE WHEN t1.legacyttzsource__c = 'CS Import File' THEN 1 WHEN t1.legacyttzsource__c = 'Hardware Software' THEN 2 ELSE 3 END) ) AS "RANK",
       t1.id,
       t1.ReplaceexistingwiththisVersion__c,
       t2.name as ReplaceexistingwiththisVersion_Name,       
       t1.name,
       t1.productline__c,
       t1.legacyttzversion__c,
       t1.legacyttzproductgroup__c,
       t1.product_group__c,
       t1.versionnumber__c,
       t1.legacyttzsource__c,
       t1.current__c
FROM   Superion_Production.dbo.VERSION__C t1
left join Superion_Production.dbo.version__c  t2 on t2.id = t1.ReplaceexistingwiththisVersion__c
WHERE  t1.legacyttzproductgroup__c = 'IMC'  OR
       t1.product_group__c = 'IMC' 
),
CTE_TT_HS -- TT Hardware Software CTE
AS
(
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          t3.id                      AS account__c,
          t3.name                    AS account_name,
          t1.cim_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.cim_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.cim_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.cim_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Test_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.cim_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.cim_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'CIM_Training_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.cim_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.fbr_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.fbr_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.fbr_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Test_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.fbr_training_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                     AS tt_legacy_id_hs,
          t1.name                                   AS tt_hs_name,
          t1.account_name__c                        AS tt_legacysfdcaccountid__c,
          t3.id                                     AS account__c,
          t3.name                                   AS account_name,
          t1.fbr_training_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'FBR_Training_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.fbr_training_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.imc_version__c  AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMC_Version__c '
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.imc_version__c IS NOT NULL
UNION
SELECT    t1.id                   AS tt_legacy_id_hs,
          t1.name                 AS tt_hs_name,
          t1.account_name__c      AS tt_legacysfdcaccountid__c,
          t3.id                   AS account__c,
          t3.name                 AS account_name,
          t1.inform_me_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Inform_Me_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.inform_me_version__c IS NOT NULL
UNION
SELECT    t1.id              AS tt_legacy_id_hs,
          t1.name            AS tt_hs_name,
          t1.account_name__c AS tt_legacysfdcaccountid__c,
          t3.id              AS account__c,
          t3.name            AS account_name,
          t1.iq_version__c   AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IQ_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.iq_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.jail_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Jail_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.jail_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.rms_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.rms_test_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.rms_test_software_version_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'RMS_Test_Software_Version_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.rms_test_software_version_patch__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.ttms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'TTMS_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.ttms_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_cad_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_CAD_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_cad_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_fbr_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_FBR_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fbr_software_version__c IS NOT NULL
UNION
SELECT    t1.id                          AS tt_legacy_id_hs,
          t1.name                        AS tt_hs_name,
          t1.account_name__c             AS tt_legacysfdcaccountid__c,
          t3.id                          AS account__c,
          t3.name                        AS account_name,
          t1.va_fire_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Fire_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_fire_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_inform_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Inform_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_inform_software_version__c IS NOT NULL
UNION
SELECT    t1.id                            AS tt_legacy_id_hs,
          t1.name                          AS tt_hs_name,
          t1.account_name__c               AS tt_legacysfdcaccountid__c,
          t3.id                            AS account__c,
          t3.name                          AS account_name,
          t1.va_mobile_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_Mobile_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_mobile_software_version__c IS NOT NULL
UNION
SELECT    t1.id                         AS tt_legacy_id_hs,
          t1.name                       AS tt_hs_name,
          t1.account_name__c            AS tt_legacysfdcaccountid__c,
          t3.id                         AS account__c,
          t3.name                       AS account_name,
          t1.va_rms_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VA_RMS_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.va_rms_software_version__c IS NOT NULL
UNION
SELECT    t1.id               AS tt_legacy_id_hs,
          t1.name             AS tt_hs_name,
          t1.account_name__c  AS tt_legacysfdcaccountid__c,
          t3.id               AS account__c,
          t3.name             AS account_name,
          t1.visicad_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_patch__c IS NOT NULL
UNION
SELECT    t1.id                 AS tt_legacy_id_hs,
          t1.name               AS tt_hs_name,
          t1.account_name__c    AS tt_legacysfdcaccountid__c,
          t3.id                 AS account__c,
          t3.name               AS account_name,
          t1.visicad_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiCAD_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visicad_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_production_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Production_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_production_patch__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_mobile_test_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_mobile_test_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Test_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_test_version__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_mobile_training_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Training_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_training_version__c IS NOT NULL
UNION
SELECT    t1.id                        AS tt_legacy_id_hs,
          t1.name                      AS tt_hs_name,
          t1.account_name__c           AS tt_legacysfdcaccountid__c,
          t3.id                        AS account__c,
          t3.name                      AS account_name,
          t1.visinet_mobile_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Mobile_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_mobile_version__c IS NOT NULL
UNION
SELECT    t1.id                           AS tt_legacy_id_hs,
          t1.name                         AS tt_hs_name,
          t1.account_name__c              AS tt_legacysfdcaccountid__c,
          t3.id                           AS account__c,
          t3.name                         AS account_name,
          t1.visinet_test_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                             AS tt_legacy_id_hs,
          t1.name                           AS tt_hs_name,
          t1.account_name__c                AS tt_legacysfdcaccountid__c,
          t3.id                             AS account__c,
          t3.name                           AS account_name,
          t1.visinet_test_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Test_System_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_test_system_version__c IS NOT NULL
UNION
SELECT    t1.id                               AS tt_legacy_id_hs,
          t1.name                             AS tt_hs_name,
          t1.account_name__c                  AS tt_legacysfdcaccountid__c,
          t3.id                               AS account__c,
          t3.name                             AS account_name,
          t1.visinet_training_system_patch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Patch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_patch__c IS NOT NULL
UNION
SELECT    t1.id                                 AS tt_legacy_id_hs,
          t1.name                               AS tt_hs_name,
          t1.account_name__c                    AS tt_legacysfdcaccountid__c,
          t3.id                                 AS account__c,
          t3.name                               AS account_name,
          t1.visinet_training_system_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'VisiNet_Training_System_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.visinet_training_system_version__c IS NOT NULL
UNION
SELECT    t1.id                       AS tt_legacy_id_hs,
          t1.name                     AS tt_hs_name,
          t1.account_name__c          AS tt_legacysfdcaccountid__c,
          t3.id                       AS account__c,
          t3.name                     AS account_name,
          t1.x911_software_version__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'X911_Software_Version__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
WHERE     t1.x911_software_version__c IS NOT NULL
union
--new 2/13
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_Public_Safety_Records_Mgmt_Software__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Public_Safety_Records_Mgmt_Software__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Public_Safety_Records_Mgmt_Software__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_VCAD_Visual_Computer_Aided_Dispatch__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_VCAD_Visual_Computer_Aided_Dispatch__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_VCAD_Visual_Computer_Aided_Dispatch__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_Comm_Server__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Comm_Server__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Comm_Server__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.IMP_Advanced_Mobile_Online__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'IMP_Advanced_Mobile_Online__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.IMP_Advanced_Mobile_Online__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.Tib_PRD_Build__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_PRD_Build__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_PRD_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          cast(t1.Tib_PRD_Build_Date__c as nvarchar(255)) AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_PRD_Build_Date__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_PRD_Build_Date__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.Tib_TRN_Build__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_TRN_Build__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_TRN_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          cast(t1.Tib_TRN_Build_Date__c as nvarchar(255)) AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_TRN_Build_Date__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_TRN_Build_Date__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          t1.Tib_DEV_Build__c AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_DEV_Build__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_DEV_Build__c IS NOT NULL
UNION
SELECT    t1.id                      AS tt_legacy_id_hs,
          t1.name                    AS tt_hs_name,
          t1.account_name__c         AS tt_legacysfdcaccountid__c,
          coalesce(t3.id,sam.[18-digit SUP Account ID])     AS account__c,
          coalesce(t3.name,sam.[SUP Account Name]) AS account_name,
          cast(t1.Tib_DEV_Build_Date__c as nvarchar(255)) AS version ,
          t2.sourcefieldname,
          t2.productgroup,
          t2.productline,
          t2.environmenttype
FROM      tritech_prod.dbo.hardware_software__c t1
join      Staging_PROD.dbo.spv_hardwaresoftware_mapping t2
ON        t2.sourcefieldname = t2.sourcefieldname
          AND t2.sourcefieldname = 'Tib_DEV_Build_Date__c'
left join Superion_Production.dbo.account t3
ON        t3.legacysfdcaccountid__c = t1.account_name__c
left join Staging_PROD.dbo.TT_Superion_Account_Merge sam --to handle account merges
on        sam.[18-digit TT Account ID] = t1.account_name__c
WHERE     t1.Tib_DEV_Build_Date__c IS NOT NULL
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'Zuercher' as productgroup,
          'CAD'  asproductline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_Production.dbo.registered_product__c r
       join Superion_Production.dbo.Account a on r.Account__c = a.id where r.name like '%Zuercher%') t1
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'Respond' as productgroup,
          'Billing'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_Production.dbo.registered_product__c r
       join Superion_Production.dbo.Account a on r.Account__c = a.id where r.name like '%respond%') t1
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'MetroAlert' as productgroup, 	
          'RMS'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_Production.dbo.registered_product__c r
       join Superion_Production.dbo.Account a on r.Account__c = a.id where r.name like '%Metro%') t1 
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'LETG' as productgroup, 	 
          'CAD'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_Production.dbo.registered_product__c r
       join Superion_Production.dbo.Account a on r.Account__c = a.id where r.name like '%LETG%') t1 
UNION
SELECT    null                     AS tt_legacy_id_hs,
          NULL                   AS tt_hs_name,
          t1.LegacySFDCAccountId__c        AS tt_legacysfdcaccountid__c,
          t1.Account__c                      AS account__c,
          t1.name                     AS account_name,
          NULL AS version ,
          NULL AS sourcefieldname,
          'PSSI' as productgroup, 	 
          'CAD'  as productline,
          'Production' as environmenttype
from (select distinct Account__c , a.LegacySFDCAccountId__c , a.name from Superion_Production.dbo.registered_product__c r
       join Superion_Production.dbo.Account a on r.Account__c = a.id where r.name like '%PSSI%') t1  --end new 2/13
),
CTE_Registered_Product  as --4/8
(SELECT rp.id as ID,
        rp.Name as Name,
		rp.account__c as account__c,
		pr.id as Product2Id,
		pr.product_group__c as Product_Group,
		pr.product_line__c as Product_Line,
		rp.createddate
from Superion_Production.dbo.Registered_Product__c rp
left join Superion_Production.dbo.product2 pr on pr.id = rp.product__c
join Superion_Production.dbo.Customer_Asset__c cass  on rp.customerasset__c = cass.id
left join Superion_Production.dbo.System_product_Version__c spv on spv.Registered_Product__c = rp.Id
where 
rp.name <> 'Customer Web Portal'
and cass.MigratedAsset__c = 'True'
and pr.TTZ_Product__c = 'True'
--cass.product_group__c in
--(
--'Inform','IQ','PSSI','Tiburon','ETI','Impact','LETG','MetroAlert','IMC','Respond','Zuercher','VisiNet','VisionAIR'
--)
and cass.Eligible_for_Registered_Product__c = 'true'
and spv.Registered_Product__c is null
)
SELECT DISTINCT Cast (null as nchar(18)) as ID,
                Cast (null as nvarchar(255)) as Error,
                COALESCE( regprd.id, 'ID NOT FOUND' )   AS Registered_Product__c,
                COALESCE( regprd.NAME, 'REG PROD NOT FOUND' )       AS [Registered Product Name],
                COALESCE( COALESCE(cte_env.id,coalesce(cte_env2.id,cte_env3.Id)), 'ID NOT FOUND' )       AS Environment__c,
                COALESCE( coalesce(cte_env.NAME,coalesce(cte_env2.name,cte_env3.Name)), 'ENVIRONMENT NOT FOUND' )     AS [Environment Name],
                CASE 
                   WHEN COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id) is not null then COALESCE( cte_mrv.replaceexistingwiththisversion__c,cte_mrv.Id)
                   WHEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion__c, cte_vers.id )
                   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion__c, cte_vers_2.id )
                   else 'Id not found'
                   end as Version__c,
               CASE 
                   when COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) IS NOT NULL then COALESCE( cte_mrv.replaceexistingwiththisversion_name, cte_mrv.NAME ) 
                   WHEN  COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME ) IS NOT NULL THEN COALESCE( cte_vers.replaceexistingwiththisversion_name, cte_vers.NAME )
                   WHEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) IS NOT NULL THEN COALESCE( cte_vers_2.replaceexistingwiththisversion_name, cte_vers_2.NAME ) 
                else 'VERSION NOT FOUND'
                end as [VERSION_NAME],
                cte_tt_hs.tt_legacy_id_hs,  --2/19
                cte_tt_hs.tt_hs_name, --2/19
                cte_tt_hs.tt_legacysfdcaccountid__c, --2/19
                cte_tt_hs.account__c, --2/19
                cte_tt_hs.Account_Name, --2/19
                cte_tt_hs.[version], --2/19
                cte_tt_hs.SourceFieldName, --2/19
                cte_tt_hs.ProductGroup, --2/19
                cte_tt_hs.ProductLine, --2/19
                CASE WHEN regprd.[Name] LIKE 'Z%' AND  --2/19
				          (regprd.[Name] like '%test%' and  --2/19
						   regprd.[Name] like '%Train%') Then 'Test' --2/19
                     ELSE  cte_tt_hs.environmenttype 
				END  as EnvironmentType,
                CASE
                  WHEN cte_vers.replaceexistingwiththisversion__c IS NOT NULL  OR
                       cte_vers_2.replaceexistingwiththisversion__c IS NOT NULL THEN 'True'
                  ELSE 'False'
                END AS Replace_Existing_Version  ,
				cte_vers.LegacyTTZVersion__c as [cte_vers.LegacyTTZVersion__c] ,
				 cte_tt_hs.[Version]   as [cte_tt_hs.[Version]   ,
				 cte_mrv.LegacyMajorReleaseVersion__c as [cte_mrv.LegacyMajorReleaseVersion__c],
				 regprd.Product_Group as RegProd_ProductGroup,
				 regprd.Product_Line as RegProd_ProductLine,
				 regprd.account__c as RegProd_AccountId
INTO   System_Product_Version__c_Insert_MC_delta_preload          --2/19                                                                                                                                                                            
FROM   CTE_Registered_Product regprd
       left join CTE_TT_HS cte_tt_hs
              ON regprd.account__c = CTE_TT_HS.account__c AND
                 regprd.product_group = CTE_TT_HS.productgroup AND
                 regprd.product_line = CTE_TT_HS.productline
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env
              ON cte_env.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env.type__c AND
                 cte_env.name like '%' + CTE_TT_HS.productgroup + '%'
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env2
              ON cte_env2.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env2.type__c AND
                 cte_env2.name like '%visi%' AND --2/13
                 CTE_TT_HS.productgroup = 'Inform'
       LEFT JOIN Cte_version_MajorRelease cte_mrv on cte_tt_hs.[Version]  = cte_mrv.LegacyMajorReleaseVersion__c
       LEFT JOIN CTE_SUP_ENVIRONMENT cte_env3 --TRY match on account id and default env
              ON cte_env3.account__c = CTE_TT_HS.account__c AND
                 CTE_TT_HS.environmenttype = cte_env3.type__c AND
				 cte_env3.Description__c like '%Automatically%'
       LEFT JOIN CTE_VERSION cte_vers on cte_vers.ProductLine__c = cte_tt_hs.productline and cte_vers.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%'
       LEFT JOIN Cte_version_2 cte_vers_2 
              ON coalesce(cte_vers_2.LegacyTTZProductGroup__c, cte_vers_2.Product_Group__c) =  cte_tt_hs.productgroup AND
                 cte_vers_2.LegacyTTZVersion__c like '%' + cte_tt_hs.[Version] + '%' AND
                 cte_vers_2.[RANK] =  1

--(4388 row(s) affected) 4/8 00:03:26


--Create load 
SELECT *
INTO   System_Product_Version__c_Insert_MC_delta_load --3/26
FROM   ( SELECT *,
                Row_number( )
                  OVER (
                    partition BY environment__c, registered_product__c, account__c
                    ORDER BY CASE WHEN version = 'id not found' THEN 0 ELSE Len(version) END DESC) [rank]
         FROM   System_Product_Version__c_Insert_MC_delta_preload --2/19
         WHERE
         registered_product__c <> 'ID NOT FOUND' ) x
WHERE  x.[rank] = 1  
--(1597 row(s) affected) 4/8



--for Z training and test reg combined products, create 1 spv for each env (1 for test 1 for train).



---Null out versions
select * from System_Product_Version__c_Insert_MC_delta_load
where version__c = 'ID NOT FOUND'

UPdate System_Product_Version__c_Insert_MC_delta_load --2/19
set Version__c = null
where version__c = 'ID NOT FOUND' 
--(1353 row(s) affected) 4/8



--create Training SPV's for Zuercher "Train/Test"  registered products. Only "Test" env are generated by the script. --2/19
INSERT INTO System_Product_Version__c_Insert_MC_delta_load
            (id,
             error,
             registered_product__c,
             [registered product name],
             environment__c,
             [environment name],
             version__c,
             version_name,
             tt_legacy_id_hs,
             tt_hs_name,
             tt_legacysfdcaccountid__c,
             account__c,
             account_name,
             version,
             sourcefieldname,
             productgroup,
             productline,
             environmenttype,
             replace_existing_version,
             [cte_vers.legacyttzversion__c],
             [cte_tt_hs.[version],
             [cte_mrv.legacymajorreleaseversion__c],
             regprod_productgroup,
             regprod_productline,
             regprod_accountid,
             [rank])
SELECT id,
       error,
       registered_product__c,
       [registered product name],
       environment__c,
       [environment name],
       version__c,
       version_name,
       tt_legacy_id_hs,
       tt_hs_name,
       tt_legacysfdcaccountid__c,
       account__c,
       account_name,
       version,
       sourcefieldname,
       productgroup,
       productline,
       'Training' AS EnvironmentType,
       replace_existing_version,
       [cte_vers.legacyttzversion__c],
       [cte_tt_hs.[version],
       [cte_mrv.legacymajorreleaseversion__c],
       regprod_productgroup,
       regprod_productline,
       regprod_accountid,
       [rank]
FROM   System_Product_Version__c_Insert_MC_delta_load ld
WHERE  ld.registered_product__c IN
       ( SELECT t1.id AS Zuercher_Registered_Product_Id
         FROM   Superion_Production.dbo.REGISTERED_PRODUCT__C t1
                LEFT JOIN Superion_Production.dbo.PRODUCT2 p2
                       ON p2.id = t1.product__c
                JOIN Superion_Production.dbo.CUSTOMER_ASSET__C t2
                  ON t2.id = t1.customerasset__c
                LEFT JOIN Superion_Production.dbo.ACCOUNT a
                       ON a.id = t1.account__c
         WHERE  t2.product_group__c = 'Zuercher' ) AND
       [registered product name] LIKE '%test%' AND
       [registered product name] LIKE '%Train%'; 
	   --(4 row(s) affected) 4/8


--- update environments part 1.1 try to match to a  Env. on prod group like env name 
Update t1
set environment__c = t2.id, [Environment Name] = t2.Name
from System_Product_Version__c_Insert_MC_delta_load t1 --2/19
join Superion_Production.dbo.Environment__c t2 on t1.RegProd_AccountId = t2.Account__c and 
     t2.name like '%' + t1.RegProd_ProductGroup + '%' and t2.type__c = coalesce(t1.environmenttype,'Production') --2/19
and t2.id not in (select id from Environment__c_Load_Non_Zuercher_RT)
where t1. environment__c = 'ID NOT FOUND' 
--(1022 row(s) affected) 4/8

--update environments part 1.2 try to match to a 'Default' Env.
Update t1
set environment__c = t2.id, [Environment Name] = t2.Name
from System_Product_Version__c_Insert_MC_delta_load t1 --2/19
join Superion_Production.dbo.Environment__c t2 on t1.RegProd_AccountId = t2.Account__c and 
     t2.Description__c like 'Automatically Created%' and t2.type__c = coalesce(t1.environmenttype,'Production') --2/19
and t2.id not in (select id from Environment__c_Load_Non_Zuercher_RT)
where t1. environment__c = 'ID NOT FOUND' 
-- (69 row(s) affected) 4/8

--(4 row(s) affected)
--(1048 row(s) affected) 3/12
--(1061 row(s) affected) 3/3
--(536 row(s) affected)

--give ron list of missing Envs...
/*

--create environments 
SELECT DISTINCT regprod_productgroup, --2/19
                regprod_accountid,
                CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + regprod_productgroup + '-' + a.name AS Name,
                regprod_accountid                                                                                                                                        AS account__c,
                'Automatically created because no Hardware Software records existed in Tritech source'                                                                   AS description__c,
                CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production' 
				     WHEN EnvironmentType = 'Train' Then 'Training'
					 WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c
FROM   System_Product_Version__c_Insert_MC_delta_load s
       join Superion_Production.dbo.ACCOUNT a
         ON a.id = s.regprod_accountid
WHERE  environment__c = 'ID NOT FOUND' 

*/


-- select * from System_Product_Version__c_Insert_MC_delta_load where environment__c = 'ID NOT FOUND'
--- update environments part 2 (additional from Ron) 
--- select * from Environment__c_Load_Non_Zuercher_RT
update t1
set environment__c = t2.id, [Environment Name] = t2.Name
from System_Product_Version__c_Insert_MC_delta_load t1 
join Staging_PROD.dbo.Environment__c_Load_Non_Zuercher_RT t2 on t1.RegProd_AccountId = t2.Account__c and t2.RegProd_ProductGroup = t1.RegProd_ProductGroup
where t1. environment__c = 'ID NOT FOUND' 
--(30 row(s) affected) 4/8

update t1
set environment__c = t2.id, [Environment Name] = t2.Name
from System_Product_Version__c_Insert_MC_delta_load t1 
join Staging_PROD.dbo.Environment__c_Load_Non_Zuercher_Delta_RT t2 on t1.RegProd_AccountId = t2.Account__c and t2.RegProd_ProductGroup = t1.RegProd_ProductGroup
where t1. environment__c = 'ID NOT FOUND' 

--(194 row(s) affected) 4/8

select distinct RegProd_AccountId , RegProd_ProductGroup, RegProd_ProductLine from System_Product_Version__c_Insert_MC_delta_load where Environment__c = 'ID NOT FOUND'


--checks:
select * from System_Product_Version__c_Insert_MC_delta_load where version__c = 'ID NOT FOUND';


select * from System_Product_Version__c_Insert_MC_delta_load where environment__c = 'ID NOT FOUND';

select * from System_Product_Version__c_Insert_MC_delta_load where [Account_Name] like 'Lansing%'


select * from System_Product_Version__c_Insert_MC_delta_load where environment__c = 'ID NOT FOUND'
and  RegProd_AccountId in
(
select Account__c , count(*) from Superion_Production.dbo.Environment__c
group by Account__c, Name
having count(*) > 1
)


select * from System_Product_Version__c_Insert_MC_delta_load where Registered_Product__c = 'ID NOT FOUND';
select environment__c, Registered_Product__c , count(*) from System_Product_Version__c_Insert_MC_delta_load group by environment__c, Registered_Product__c having count(*) > 1

select * FROM   System_Product_Version__c_Insert_MC_delta_load ld
WHERE  ld.registered_product__c IN
       ( SELECT t1.id AS Zuercher_Registered_Product_Id
         FROM   Superion_Production.dbo.REGISTERED_PRODUCT__C t1
                LEFT JOIN Superion_Production.dbo.PRODUCT2 p2
                       ON p2.id = t1.product__c
                JOIN Superion_Production.dbo.CUSTOMER_ASSET__C t2
                  ON t2.id = t1.customerasset__c
                LEFT JOIN Superion_Production.dbo.ACCOUNT a
                       ON a.id = t1.account__c
         WHERE  t2.product_group__c = 'Zuercher' ) AND
       [registered product name] LIKE '%test%' AND
       [registered product name] LIKE '%Train%'; 