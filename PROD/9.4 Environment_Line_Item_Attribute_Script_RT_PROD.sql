/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		: Environment Line Attribute Migration
DEVELOPER	: RTECSON, MCORPUS, and PBOWEN
CREATED DT  : 01/15/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
01/08/2019		Patrick Bowen		Initial - 
01/18/2019		Ron Tecson			Per Maria, Patch_Version__c is to be descripted.
01/20/2019		Ron Tecson			Corrected some Target API fields from Maria.
01/21/2019		Ron Tecson			Per Gabe, Version__c is not apply here but applies on the SPV.
02/05/2019		Ron Tecson			Cleaned the Script and changed the logic of the data. Before it was grouping it by product family. Took that logic out.
02/05/2019		Ron Tecson			Reloaded the target and used the proper Environment Load Table.
02/12/2019		Ron Tecson			Wipe and Reload. Fixed the Record Type issue by adding Trg_RT.SobjectType = 'Environment_Product_Attribute__c'
02/18/2019		Ron Tecson			Wipe and Reload. Added the logs.
02/26/2019		Ron Tecson			Wipe and Reload. Added the logs.
03/04/2019		Ron Tecson			Wipe and Reload
03/15/2019		Ron Tecson			Wipe and Reload.
03/19/2019		Ron Tecson			Had Staged the data but hasn't executed the SF_BulkOps Insert.
03/26/2019		Ron Tecson			Wipe and Reload.
03/30/2019		Ron Tecson			Executed in Production. Total Record Count: 4730 with no errors.

PREQUISITES:
============
1.) Need to place the actual Environment Load table used to associate the EPA with the Environment. Replace the load table in cte_GetHwSwWinningLegacyId.

*****************************************************************************************************************************************************/

---------------------------------------------------------------------------------
-- Refresh Tables to the Local Database 
---------------------------------------------------------------------------------

USE Tritech_Prod
EXEC SF_Refresh 'MC_Tritech_Prod', 'Hardware_Software__c', 'yes';
EXEC SF_Refresh 'MC_Tritech_Prod', 'RecordType', 'yes';
EXEC SF_Refresh 'MC_Tritech_Prod', 'Account', 'yes';

USE SUPERION_Production
EXEC SF_Refresh 'RT_SUPERION_PROD', 'RecordType', 'yes'
EXEC SF_Refresh 'RT_SUPERION_PROD', 'Environment__c', 'yes'

---------------------------------------------------------------------------------
-- Drop PreLoad Table
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment_Product_Attribute__c_PreLoad_RT') 
DROP TABLE Staging_PROD.dbo.Environment_Product_Attribute__c_PreLoad_RT;

----------------------------------------------------------------------------
--- ENVIRONMENT TYPE BREAKOUT
----------------------------------------------------------------------------
WITH cte_Environment_Type 
AS
(
		select 
			ID as Legacy_ID__c, 
			'Prod' as Environment_Type
			from Tritech_PROD.dbo.Hardware_Software__c 

		UNION
		SELECT 
			ID as Legacy_ID__c, 
			'Test' as Environment_Type 
			from Tritech_PROD.dbo.Hardware_Software__c 
			where 
			(
			isnull(VisiNet_Test_System_Version__c, '') <> ''
			OR isNull(VisiNet_Mobile_Test_Version__c, '') <> ''
			OR isnull(FBR_Test_Software_Version__c, '') <> ''
			OR isNULL(TTMS_Test_Software_Version__c, '') <> ''
			OR isNull(CIM_Test_Software_Version__c, '') <> ''
			OR isNull(RMS_Test_Software_Version__c, '') <> ''
			OR isNull(RMS_Web_Test_Software_Version__c, '') <> ''
			OR isNull(Tib_DEV_Build__c, '') <> ''
			)

		UNION
		SELECT
			a.ID as Legacy_ID__c, 
			'Train' as Environment_Type 
			from Tritech_PROD.dbo.Hardware_Software__c a
			where 
			(
			isnull(VisiNet_Training_System_Version__c, '') <> ''
			OR isNull(VisiNet_Mobile_Training_Version__c, '') <> ''
			OR isnull(FBR_Training_Software_Version__c, '') <> ''
			OR isNULL(TTMS_Training_Software_Version__c, '') <> ''
			OR isNull(CIM_Training_Software_Version__c, '') <> ''
			OR isNull(RMS_Training_Software_Version__c, '') <> ''
			OR isNull(RMS_Web_Training_Software_Version__c, '') <> ''
			OR isNull(Tib_TRN_Build__c, '') <> ''
			)
),

cte_Product_Family AS
(
		-- INFORM 911
		SELECT a.ID as Legacy_ID__c,  ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - 911' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = '911 Hardware and Software'
		UNION
		-- INFORM - CAD
		SELECT a.ID as Legacy_ID__c,  ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - CAD' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Inform Hardware and Software'
				AND
				(isNull(a.VisiCAD_Version__c, '') <> '' OR
				 isNull(a.VisiNet_Test_System_Version__c, '') <> '' OR	
				 isnull(a.VisiNet_Training_System_Version__c, '') <> ''	
				) 
		UNION
		-- INFORM - MOBILE
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - Mobile' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Inform Hardware and Software'
				AND
				(isNull(a.VisiNet_Mobile_Version__c, '') <> '' OR
				 isNull(a.VisiNet_Mobile_Test_Version__c, '') <> '' OR
				 isNull(a.VisiNet_Mobile_Training_Version__c, '') <> ''
				)
		UNION
		-- INFORM - TTMS		 
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - TTMS' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.TTMS_Software_Version__c, '') <> '' OR
				 isNULL(a.TTMS_Test_Software_Version__c, '') <> '' OR
				 isNULL(a.TTMS_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - CIM
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - CIM' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.CIM_Software_Version__c, '') <> '' OR
				 isNULL(a.CIM_Test_Software_Version__c, '') <> '' OR
				 isNULL(a.CIM_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - RMS WEB
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - RMS Web' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.RMS_Web_Software_Version__c, '') <> '' OR
				 isNULL(a.RMS_Web_Test_Software_Version__c, '') <> '' OR
				 isNULL(a.RMS_Web_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - FIRE
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - Fire' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_Fire_Software_Version__c, '') <> ''
				)
		UNION
		-- INFORM - JAIL
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - Jail' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.Jail_Software_Version__c, '') <> ''
				)
		UNION
		-- Inform - RMS Classic
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - RMS Classic' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_RMS_Software_Version__c, '') <> '' OR
				 isNull(RMS_Test_Software_Version__c, '') <> '' OR
				 isNull(RMS_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- Inform - FBR
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - FBR' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_FBR_Software_Version__c, '') <> '' OR
				 isNull(a.FBR_Test_Software_Version__c, '') <> '' OR
				 isNull(a.FBR_Training_Software_Version__c, '') <> ''
				)
		UNION
		-- Inform - ME
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Inform' as Product_Family, 'Inform - ME' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Inform Hardware and Software'
				AND
				(isNull(a.Inform_Me_Version__c, '') <> '' 
				)
		UNION
		-- IMC
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'IMC' as Product_Family, 'IMC' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'IMC Hardware and Software'
		UNION
		-- Impact
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Impact' as Product_Family, 'Impact' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Impact Hardware and Software'
		UNION
		-- Respond
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Respond' as Product_Family, 'Respond' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Respond Hardware and Software'
		UNION
		-- Tiburon
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'Tiburon' as Product_Family, 'Tiburon' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'Tiburon Hardware and Software'
		UNION
		-- VisionAir - CAD
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'VisionAir' as Product_Family, 'VisionAir - CAD' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_CAD_Software_Version__c, '') <> '' 
				)
		UNION
		-- VisionAir - Mobile
		SELECT a.ID as Legacy_ID__c, ET.Environment_type as Environment_Type, 'VisionAir' as Product_Family, 'VisionAir - CAD' as Product_Group
				FROM Tritech_PROD.dbo.Hardware_Software__c a
				INNER JOIN cte_Environment_Type ET ON a.ID = ET.Legacy_ID__c
				LEFT OUTER JOIN Tritech_Prod.dbo.RecordType RT ON a.RecordTypeID =  RT.[ID] and sobjecttype = 'hardware_software__c' 
				WHERE RT.[NAME] = 'VisionAIR Hardware and Software'
				AND
				(isNull(a.VA_Mobile_Software_Version__c, '') <> '' 
				)
),

--***************** WARNING: Need to place the actual Environment Load table used to associate the EPA with the Environment
cte_GetHwSwWinningLegacyId AS
(
	SELECT t.Legacy_SystemID__c, t.LegacySystemIds, s.value
						FROM   Staging_PROD.dbo.Environment__c_Load_RT t CROSS apply String_split( t.LegacySystemIds, ',' ) s
						WHERE  t.Legacy_SystemID__c IS NOT NULL 
			--order by t.Legacy_SystemID__c
)

Select distinct
	Cast('' as nchar(18)) as [ID],
	Cast('' as nvarchar(255)) as Error,
	a.[ID] as LegacySFDCAccountId__c,
	'TriTech' as Legacy_Source_System__c,
	'true' as Migrated_Record__c,
	Env.Name as tEnvironment_Name,
	cteEnvType.Legacy_ID__c,
	cteEnvType.Environment_Type,
	ctegtHwSwId.Legacy_SystemID__c as WinningLegacySystemId,
	ctePF.Legacy_ID__c as ctePF_Legacy_ID__c,
	--ctePF.Product_Group,
	--ctePF.Product_Family,

	IIF(Env.ID IS NOT NULL, Env.ID, NULL) as Environment__c,
	IIF(Src_rt.[Name] = '911 Hardware and Software', sAccount.Insight_Version__c, NULL) as Insight_Version__c,
	a.Backup_Software_WMP__c as Backup_Software__c,
	a.Certificate_Expiration_Date__c as RMS_Certificate_Expiration_Date__c,
	CASE 
		WHEN cteEnvType.Environment_Type = 'Prod' THEN a.CIM_Additional_Information__c
		WHEN cteEnvType.Environment_Type = 'Test' THEN a.CIM_Test_Additional_Information__c
		WHEN cteEnvType.Environment_Type = 'Train' THEN a.CIM_Training_Additional_Information__c
		ELSE NULL
	END as CIM_Additional_Information__c,
	a.CIM_DR__c as CIM_DR__c,
	a.CIM_DR_Software__c as CIM_DR_Software__c,
	CASE WHEN cteEnvType.Environment_Type = 'Prod' THEN a.CIM_Software_Expiration_Date__c
		 WHEN cteEnvType.Environment_Type = 'Test' THEN a.CIM_Test_Software_Expiration_Date__c 
		 WHEN cteEnvType.Environment_Type = 'Train' THEN a.CIM_Training_Software_Expiration_Date__c
		 ELSE NULL 
	END as CIM_Software_Expiration_Date__c,
	a.Current_Pervasive_Version__c as Current_Pervasive_Version__c,
	a.Current_SQL_Version_WMP__c as Respond_SQL_Version__c,
	a.Custom_Interfaces__c as Custom_Interfaces__c,
	a.DR__c as DR__c,
	a.DR_Mobile__c as DR_Mobile__c,
	a.External_VisiNet_Browser__c as External_VisiNet_Browser__c,
	a.FBR_DR__c as FBR_DR__c,
	--CAST(a.VA_FBR_Additional_Information__c AS NVARCHAR(max))
	CASE cteEnvType.Environment_Type 
		WHEN 'Prod' THEN CAST(a.VA_FBR_Additional_Information__c AS NVARCHAR(max))
		WHEN 'Test' THEN CAST(a.FBR_Test_Additional_Information__c AS NVARCHAR(max))
		ELSE NULL 
	END as FBR_Additional_Information__c,
	CASE cteEnvType.Environment_Type 
		WHEN 'Prod' THEN a.VA_FBR_OS__c
		WHEN 'Test' THEN a.FBR_Test_OS__c
		WHEN 'Train' THEN a.FBR_Training_OS__c
		ELSE NULL 
	END as FBR_OS__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod'  THEN a.VA_FBR_SQL_Version__c
		WHEN 'Test'  THEN a.FBR_Test_SQL_Version__c
		WHEN 'Train' THEN a.FBR_Training_SQL_Version__c
		ELSE NULL 
	END as FBR_SQL_Version__c,
	a.Geo_Installed_Test__c as Telemetry_Enabled__c,
	CASE cteEnvType.Environment_Type 
		WHEN 'Prod' THEN a.GIS_Framework_Version__c 
		WHEN 'Test' THEN a.GIS_Framework_Test_Version__c
		WHEN 'Train' THEN a.GIS_Framework_Training_Version__c
		ELSE NULL 
	END as GIS_Framework_Version__c,
	a.IMP_Academy_System__c as Academy_System__c,
	a.IMP_Advanced_Mobile_Online__c as Advanced_Mobile_Online__c,
	a.IMP_ATLAS_CLIENT__c as ATLAS_CLIENT__c,
	a.IMP_ATLAS_SERVER__c as ATLAS_SERVER__c,
	a.IMP_Authentication__c as Authentication__c,
	a.IMP_Bar_Coding__c as Bar_Coding__c,
	a.IMP_Comm_Server__c as IMP_Comm_Server__c,
	a.IMP_INT_Biometrics__c as Biometrics__c,
	a.IMP_INT_CAD_Incident_Data_Export__c as CAD_Incident_Data_Export__c,
	a.IMP_INT_Comnetix_LiveScan_CardScan__c as Comnetix_LiveScan_CardScan__c,
	a.IMP_INT_Coplogic__c as Coplogic__c,
	a.IMP_INT_E911__c as E911__c,
	a.IMP_INT_N_DEX_Data_Exchange__c as N_DEX_Data_Exchange__c,
	a.IMP_INT_NCIC__c as NCIC__c,
	a.IMP_INT_NIEM_Data_Exchange__c as NIEM_Data_Exchange__C,
	a.IMP_INT_NYS_TRACS_Products__c as NYS_TRACS_Products__c,
	a.IMP_INT_PICTOMETRY__c as PICTOMETRY__c,
	a.IMP_INT_Priority_Dispatch_Pro_QA_EMD__c as Priority_Dispatch_Pro_QA_EMD__c,
	a.IMP_INT_Red_Alert_Incident_Per_Fire_Agen__c as Red_Alert_Incident_Per_Fire_Agency__c,
	a.IMP_INT_SEI_Court__c as SEI_Court__c,
	a.IMP_IRIS_IMPACT_Remote_Intel_Service__c as IRIS_IMPACT_Remote_Intel_Service__c,
	a.IMP_Jewelry_Pawn_Shop_System__c as Jewelry_Pawn_Shop_System__c,
	a.IMP_Mobile_Ticketing_IMT__c as Mobile_Ticketing_IMT__c,
	a.IMP_Public_Safety_Records_Mgmt_Software__c as Public_Safety_Records_Mgmt_Software__c,
	a.IMP_Taxi_Limonsine_Permiting_System__c as Taxi_Limosine_Permiting_System__c, 
	a.IMP_VCAD_Visual_Computer_Aided_Dispatch__c as VCAD_Visual_Computer_Aided_Dispatch__c,
	a.IMP_VSTAT__c as VSTAT__c,
	a.Jail_Additional_Information__c as Jail_Additional_Information__c,
	a.Jail_OS__c as Jail_OS__c,
	a.Jail_SQL_Version__c as Jail_SQL_Version__c,
	CONCAT(CASE WHEN a.Misc_Info_WMP__c Is Null THEN NULL ELSE 'Misc Info: ' END, isnull(a.Misc_Info_WMP__c, ''),
		   CASE WHEN a.Notes_Area_WMP__c Is Null THEN NULL ELSE ' Area: ' END, isNull(a.Notes_Area_WMP__c, ''),
		   CASE WHEN a.VA_Additional_Information__c Is NULL THEN NULL ELSE 'Additional Information: ' END, isnull(a.VA_Additional_Information__c, ''),
		   CASE WHEN Src_rt.[Name] = '911 Hardware and Software' and a.X911_Additional_System_Information__c is NOT Null THEN 'Additional System Information: ' ELSE '' END, isnull(a.X911_Additional_System_Information__c, '')
	) as Notes__c,
	a.Mobile_Maps_Enabled__c as Mobile_Maps_Enabled__c,
	a.New_Queues__c as New_Queues__c,
	a.Number_of_Reporting_Servers__c as Number_of_Reporting_Servers__c,
	Trg_RT.id as RecordTypeId,  
	Trg_RT.Name as RecordTypeName,
	a.Reporting_Server__c as Reporting_Server__c,
	a.RMS_DR__c as RMS_DR__c,
	a.RMS_DR_Software__c as RMS_DR_Software__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN a.RMS_Integration_Version__c 
		WHEN 'Test' THEN a.RMS_Test_Integration_Version__c 
		WHEN 'Train' THEN a.RMS_Training_Integration_Version__c
		ELSE NULL 
	END as RMS_Integration_Version__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN CAST(a.VA_RMS_Additional_Information__c as NVARCHAR(MAX))
		WHEN 'Test' THEN CAST(a.RMS_Test_Additional_Information__c  as NVARCHAR(MAX))
		WHEN 'Train' THEN CAST(a.RMS_Training_Additional_Information__c  as NVARCHAR(MAX))
		ELSE NULL
	END as RMS_Additional_Information__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN a.VA_RMS_OS__c
		WHEN 'Test' THEN a.RMS_Test_OS__c 
		WHEN 'Train' THEN a.RMS_Training_OS__c
		ELSE NULL 
	END as RMS_OS__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN a.VA_RMS_SQL_Version__c
		WHEN 'Test' THEN a.RMS_Test_SQL_Version__c
		WHEN 'Train' THEN a.RMS_Training_SQL_Version__c 
		ELSE NULL 
	END as RMS_SQL_Version__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN a.RMS_Web_API_Version__c 
		WHEN 'Test' THEN a.RMS_Web_Test_API_Version__c
		WHEN 'Train' THEN a.RMS_Web_Training_API_Version__c
		ELSE NULL 
	END as RMS_Web_API_Version__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN a.RMS_Web_OS__c
		WHEN 'Test' THEN a.RMS_Web_Test_OS__c
		WHEN 'Train' THEN a.RMS_Web_Training_OS__c
		ELSE NULL 
	END as RMS_Web_OS__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN a.RMS_Web_SQL_Version__c 
		WHEN 'Test' THEN a.RMS_Web_Test_SQL_Version__c
		WHEN 'Train' THEN a.RMS_Web_Training_SQL_Version__c
		ELSE NULL 
	END as RMS_Web_SQL_Version__c,
	a.Server_Name_WMP__c as Respond_Server_Name__c,
	a.Server_Version_WMP__c as Respond_Server_OS_Version__c,
	a.Tib_Application__c as Application__c,
	a.Tib_Application_Version__c as Application_Version__c,
	a.Tib_DEV_Build__c as DEV_Build__c,
	a.Tib_DEV_Build_Date__c as DEV_Build_Date__c,
	a.Tib_Go_Live__c as Go_Live__c,
	a.Tib_Hosted__c as Hosted__c,
	a.Tib_Multi_Agency__c as Multi_Agency__c,
	a.Tib_PRD_Build__c as PRD_Build__c,
	a.Tib_PRD_Build_Date__c as PRD_Build_Date__c,
	a.Tib_Support_Level__c as Support_Level__c,
	a.Tib_TRN_Build__c as TRN_Build__c,
	a.Tib_TRN_Build_Date__c as TRN_Build_Date__c,
	CASE cteEnvType.Environment_Type
		WHEN 'Prod' THEN a.TTMS_Additional_Information__c 
		WHEN 'Test' THEN a.TTMS_Test_Additional_Information__c 
		WHEN 'Train' THEN a.TTMS_Training_Additional_Information__c
		ELSE NULL 
	END as TTMS_Additional_Information__c,
	CASE cteEnvType.Environment_Type 
		WHEN 'Prod' THEN a.TTMS_Certificate_Expiration_Date__c 
		WHEN 'Test' THEN a.TTMS_Test_Certificate_Expiration_Date__c
		WHEN 'Train' THEN a.TTMS_Training_Certificate_Expiration_Dat__c
		ELSE NULL 
	END as TTMS_Certificate_Expiration_Date__c,
	a.TTMS_DR__c as TTMS_DR__c,
	a.TTMS_DR_Software__c as TTMS_DR_Software__c,
	CAST(a.VA_CAD_Additional_Information__c AS NVARCHAR(MAX)) as CAD_Additional_Information__c,
	CAST(a.VA_Fire_Additional_Information__c AS NVARCHAR(MAX)) as Fire_Additional_Information__c,
	a.VA_Fire_OS__c as Fire_OS__c,
	a.VA_Fire_SQL_Version__c as Fire_SQL_Version__c,
	CAST(a.VA_Inform_Additional_Information__c AS NVARCHAR(MAX)) as IQ_Additional_Information__c,
	a.VA_Inform_OS__c as IQ_OS__c,
	a.VA_Inform_SQL_Version__c as IQ_SQL_Version__c,
	CAST(a.VA_Mobile_Additional_Information__c AS NVARCHAR(MAX)) as Mobile_Additional_Information__c,
	a.VA_Mobile_OS__c as Mobile_OS__c,
	a.VA_Mobile_SQL_Version__c as Mobile_SQL_Version__c,
	a.VA_Preferred_Connection_Method__c as Preferred_Connection_Method__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.x911_Admin_Stations__c ELSE NULL END as X911_Admin_Stations__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Admin_Trunks__c ELSE NULL END as X911_Admin_Trunks__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_ALI_Provider__c ELSE NULL END as X911_ALI_Provider__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN CAST(a.x911_Audiocodes_Type__c AS NVARCHAR(MAX)) ELSE NULL END as X911_Audiocodes_Type__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN Cast(a.X911_Available_MAC_Programming_On_Site__c as varchar(20)) ELSE NULL END as X911_Available_MAC_Programming_On_Site__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Certificate_Expiration_Date__c ELSE NULL END as X911_Certificate_Expiration_Date__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_CO_ALI_Server_OS_SW_Versio__c ELSE 'false' END as X911_CO_ALI_Server__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Full_Positions__c ELSE NULL END as X911_Full_Positions__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_HA_System__c ELSE 'false' END as X911_HA_System__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Half_Positions__c ELSE NULL END as X911_Half_Positions__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.x911_Inform_911_CTI_Patch_Level__c ELSE NULL END as X911_CTI_Patch_Level__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.x911_Inform_911_Server_Patch_Level__c ELSE NULL END as X911_Server_Patch_Level__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Install_Upgrade_Refresh_Date__c ELSE NULL END as X911_Install_Upgrade_Refresh_Date__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Landline_911_Trunks__c ELSE NULL END as Landline_911_Trunks__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.x911_PBX_Type__c ELSE NULL END as X911_PBX_Type__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_PSAP_ID__c ELSE NULL END as X911_Telco_PSAP_ID__c, --X911_PSAP_ID__c, Correct per Maria.
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.x911_QRConnect_Patch_Level__c ELSE NULL END as X911_QRConnect_Patch_Level__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Radio_and_Radio_Interface__c ELSE NULL END as X911_Radio_and_Radio_Interface__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Reverse_ALI__c ELSE 'false' END  as X911_Reverse_ALI__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Ring_Down_Circuits__c ELSE NULL END as X911_Ring_Down_Circuits__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_SQL_Server_OS_SW_Version__c ELSE NULL END  as X911_SQL_Server_OS_SW_Version__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.x911_SQL_Version__c ELSE NULL END as X911_SQL_Version__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Telco__c ELSE NULL END as X911_Telco__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN CAST(a.X911_Trunk_Type__c AS NVARCHAR(MAX)) ELSE NULL END as X911_Trunk_Type__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_VLR_Vendor__c ELSE NULL END as X911_VLR_Vendor__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.x911_VMWare_Version__c ELSE NULL END as X911_VMWare_Version__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Wireless_911_Trunks__c ELSE NULL END as Wireless_911_Trunks__c,
	CASE WHEN Src_RT.[Name] = '911 Hardware and Software' THEN a.X911_Workstation_OS_SW_Version__c ELSE NULL END as X911_Workstation_OS__c,
	a.X911_Available_MAC_Programming_REMOTE__c as X911_Available_MAC_Programming_Remote__c,
	'' as blank
INTO Environment_Product_Attribute__c_PreLoad_RT
FROM TriTech_Prod.dbo.Hardware_Software__c a													--> Source Tritech Hardware and Software 
LEFT OUTER JOIN Tritech_PROD.dbo.RecordType Src_RT ON a.RecordTypeId = Src_RT.id				--> Source Tritech Record Type
LEFT OUTER JOIN SUPERION_Production.dbo.RecordType Trg_RT ON Trg_RT.[Name] = CASE Src_RT.[Name] WHEN	'911 Hardware and Software' THEN 'Inform 911'				-->	TARGET RECORDTYPE 
																								WHEN	'IMC Hardware and Software' THEN 'IMC'
																								WHEN	'Impact Hardware and Software' THEN 'Impact'
																								WHEN	'Inform Hardware and Software' THEN 'Inform'
																								WHEN	'Respond Hardware and Software' THEN 'Respond'
																								WHEN	'Tiburon Hardware and Software' THEN 'Tiburon'
																								WHEN	'VisionAIR Hardware and Software' THEN 'VisionAIR' END
																		AND Trg_RT.SobjectType = 'Environment_Product_Attribute__c'
LEFT OUTER JOIN cte_Environment_Type cteEnvType ON cteEnvType.Legacy_ID__c = a.Id
LEFT OUTER JOIN cte_GetHwSwWinningLegacyId ctegtHwSwId ON LTRIM(RTRIM(ctegtHwSwId.value)) = LTRIM(RTRIM(Concat(a.ID, '-', cteEnvType.Environment_type))) -- ctegtHwSwId.LegacySystemIds like Concat('%',a.ID, '-', cteEnvType.Environment_type,'%')
LEFT OUTER JOIN SUPERION_Production.dbo.ENVIRONMENT__c Env ON Env.Legacy_SystemId__c = ctegtHwSwId.Legacy_SystemID__c 
INNER JOIN cte_Product_Family ctePF on a.ID = ctePF.Legacy_ID__c
LEFT OUTER JOIN Tritech_PROD.dbo.Account sAccount on sAccount.Id = a.Account_Name__c
order by a.id, cteEnvType.Environment_Type

-- 3/30 -- Production --> (4736 rows affected)

select * from Environment_Product_Attribute__c_PreLoad_RT where Environment__c is null -- 2 records

-- 3/30 -- 6 Records

---------------------------------------------------------------------------------
-- Drop PreLoad Table
---------------------------------------------------------------------------------

USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment_Product_Attribute__c_Load_RT') 
DROP TABLE Staging_PROD.dbo.Environment_Product_Attribute__c_Load_RT;

select * INTO Staging_PROD.dbo.Environment_Product_Attribute__c_Load_RT
from Staging_PROD.dbo.Environment_Product_Attribute__c_PreLoad_RT
where Environment__c IS NOT NULL 		

-- 03/30 -- Production --> (4730 rows affected)
	
select * from Environment_Product_Attribute__c_Load_RT

EXEC SF_ColCompare 'INSERT','RT_SUPERION_PROD','Environment_Product_Attribute__c_Load_RT'

/*

--- Starting SF_ColCompare V3.6.7
Problems found with Environment_Product_Attribute__c_Load_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 536]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment_Product_Attribute__c does not contain column LegacySFDCAccountId__c
Salesforce object Environment_Product_Attribute__c does not contain column Legacy_Source_System__c
Salesforce object Environment_Product_Attribute__c does not contain column Migrated_Record__c
Salesforce object Environment_Product_Attribute__c does not contain column tEnvironment_Name
Salesforce object Environment_Product_Attribute__c does not contain column Legacy_ID__c
Salesforce object Environment_Product_Attribute__c does not contain column Environment_Type
Salesforce object Environment_Product_Attribute__c does not contain column WinningLegacySystemId
Salesforce object Environment_Product_Attribute__c does not contain column ctePF_Legacy_ID__c
Salesforce object Environment_Product_Attribute__c does not contain column RecordTypeName
Salesforce object Environment_Product_Attribute__c does not contain column blank

*/

EXEC sf_bulkops 'INSERT','RT_SUPERION_PROD','Environment_Product_Attribute__c_Load_RT'

/***************** LOG ********************************************************************************************************************
03/30 Production:

--- Starting SF_BulkOps for Environment_Product_Attribute__c_Load_RT V3.6.7
14:45:54: Run the DBAmp.exe program.
14:45:54: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
14:45:54: Inserting Environment_Product_Attribute__c_Load_RT (SQL01 / Staging_PROD).
14:45:54: DBAmp is using the SQL Native Client.
14:45:55: SOAP Headers: 
14:45:55: Warning: Column 'LegacySFDCAccountId__c' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'Legacy_Source_System__c' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'Migrated_Record__c' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'tEnvironment_Name' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'Legacy_ID__c' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'Environment_Type' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'WinningLegacySystemId' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'ctePF_Legacy_ID__c' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'RecordTypeName' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:45:55: Warning: Column 'blank' ignored because it does not exist in the Environment_Product_Attribute__c object.
14:47:11: 4730 rows read from SQL Table.
14:47:11: 4730 rows successfully processed.
14:47:11: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


***************** LOG ********************************************************************************************************************/

---------------------------------------------------------------------------------
-- Validation
---------------------------------------------------------------------------------

select error, count(*) from Environment_Product_Attribute__c_Load_RT
--where error not like '%success%'
group by error

---------------------------------------------------------------------------------
-- Refresh Local Database
---------------------------------------------------------------------------------

USE SUPERION_Production

EXEC SF_Refresh 'RT_SUPERION_PROD', 'Environment_Product_Attribute__c', 'yes';
