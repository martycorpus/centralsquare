/*
Create substiture Reqistered Products for Missing Assets.

Requirements in Google Doc: https://docs.google.com/spreadsheets/d/1ZlJIZ70jiyeo3Cko_stu8kyRiJSgZzLZ0TWb8W880yw/edit#gid=1551731943
"Tritech Service Contract to Central Square Registered Product Mapping"

"If we created a substitute RP and SPV, we need to re-parent the SPV rather than creating a new one

How do we know that there's already a susbtitute?
If there is a RP on the ""Customer Web Portal"" customer asset with the product group and line
- Admin: Field lookup to Registered Product -- real registered product
- Update the Substitute Registered Product ""Real Registered Product"" lookup with the ID of the new (correct) RP
- This gives the Business Data team a ""flag"" with an ID that tells them that the substitute is ready to be replaced

SPV Environment Selection:
- Type  = Production
- Search in the Environment name for the ""CentralSquare Product Group"" (Column C on the Mapping Worksheet)

What all needs to be updated once the SPV is reparented?
- Salesforce Admin needs to allowe RP update
- Update the existing SPV to the new RP"

*/

-------------------------------------------------------------------------------
-- Execution Log:
-- 2019-04-08 Executed for Production

-- Registered Products.

-- COLCOMPARE:

-- EXEC sf_colcompare 'Insert','MC_Superion_PROD','REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create'

--- Starting SF_ColCompare V3.7.7
--- Starting SF_ColCompare V3.7.7
--Problems found with REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 30]
--- Ending SF_ColCompare. Operation FAILED.

--ErrorDesc
--Salesforce object REGISTERED_PRODUCT__C does not contain column Create_Registered_Product_Load_Record
--Salesforce object REGISTERED_PRODUCT__C does not contain column Existing_Sup_Registered_Product_id
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_ServiceContract_ProdFamily
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_ServiceContract_ContractType
--Salesforce object REGISTERED_PRODUCT__C does not contain column SUP_Customer_Asset_Name
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_ServiceContract_Status
--Salesforce object REGISTERED_PRODUCT__C does not contain column TT_AccountName
--Salesforce object REGISTERED_PRODUCT__C does not contain column CentralSquare Product Group
--Salesforce object REGISTERED_PRODUCT__C does not contain column CentralSquare Product Line

-- BULKOPS:

-- select count(*) from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create --2113
-- select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create

-- EXEC sf_bulkops 'Insert','MC_Superion_PROD','REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create'
--- Starting SF_BulkOps for REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create V3.7.7
--04:08:04: Run the DBAmp.exe program.
--04:08:04: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
--04:08:04: Inserting REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create (SQL01 / Staging_PROD).
--04:08:05: DBAmp is using the SQL Native Client.
--04:08:05: SOAP Headers: 
--04:08:05: Warning: Column 'Create_Registered_Product_Load_Record' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'Existing_Sup_Registered_Product_id' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'TT_ServiceContract_ProdFamily' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'TT_ServiceContract_ContractType' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'SUP_Customer_Asset_Name' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'TT_ServiceContract_Status' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'TT_AccountName' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'CentralSquare Product Group' ignored because it does not exist in the Registered_Product__c object.
--04:08:05: Warning: Column 'CentralSquare Product Line' ignored because it does not exist in the Registered_Product__c object.
--04:08:16: 2113 rows read from SQL Table.
--04:08:16: 1 rows failed. See Error column of row for more information.
--04:08:16: 2112 rows successfully processed.
--04:08:16: Errors occurred. See Error column of row for more information.
--04:08:16: Percent Failed = 0.000.
--04:08:16: Error: DBAmp.exe was unsuccessful.
--04:08:16: Error: Command string is C:\"Program Files"\DBAmp\DBAmp.exe insert REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create "SQL01"  "Staging_PROD"  "MC_Superion_PROD"  " " 
----- Ending SF_BulkOps. Operation FAILED.
--Msg 50000, Level 16, State 1, Procedure SF_BulkOps, Line 223 [Batch Start Line 54]
--SF_BulkOps Error: 04:08:04: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC04:08:04: Inserting REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create (SQL01 / Staging_PROD).04:08:05: DBAmp is using the SQL Native Client.04:08:05: SOAP Headers: 04:08:05: Warning: Column 'Create_Registered_Product_Load_Record' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'Existing_Sup_Registered_Product_id' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'TT_ServiceContract_ProdFamily' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'TT_ServiceContract_ContractType' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'SUP_Customer_Asset_Name' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'TT_ServiceContract_Status' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'TT_AccountName' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'CentralSquare Product Group' ignored because it does not exist in the Registered_Product__c object.04:08:05: Warning: Column 'CentralSquare Product Line' ignored because it does not exist in the Registered_Product__c object.04:08:16: 2113 rows read from SQL Table.04:08:16: 1 rows failed. See Error column of row for more information.04:08:16: 2112 rows successfully processed.04:08:16: Errors occurred. See Error column of row for more information.

--select error, count(*) from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create
--group by error

--duplicate value found: Legacy_SFDC_ID__c duplicates value on record with id: a9a2G000000TRgh


---SPVs.

-- COLCOMPARE:

-- EXEC sf_colcompare 'Insert','MC_Superion_PROD','SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE'
--- Starting SF_ColCompare V3.7.7
--Problems found with SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 90]
--- Ending SF_ColCompare. Operation FAILED.
--ErrorDesc
--Salesforce object SYSTEM_PRODUCT_VERSION__C does not contain column Environment_Name
--Salesforce object SYSTEM_PRODUCT_VERSION__C does not contain column Environment_match_type
--Salesforce object SYSTEM_PRODUCT_VERSION__C does not contain column CentralSquare Product Group
--Salesforce object SYSTEM_PRODUCT_VERSION__C does not contain column Account__c

-- EXEC sf_bulkops 'Insert','MC_Superion_PROD','SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE'
--- Starting SF_BulkOps for SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE V3.7.7
--04:23:48: Run the DBAmp.exe program.
--04:23:48: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
--04:23:48: Inserting SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE (SQL01 / Staging_PROD).
--04:23:49: DBAmp is using the SQL Native Client.
--04:23:49: SOAP Headers: 
--04:23:49: Warning: Column 'Environment_Name' ignored because it does not exist in the System_Product_Version__c object.
--04:23:49: Warning: Column 'Environment_match_type' ignored because it does not exist in the System_Product_Version__c object.
--04:23:49: Warning: Column 'CentralSquare Product Group' ignored because it does not exist in the System_Product_Version__c object.
--04:23:49: Warning: Column 'Account__c' ignored because it does not exist in the System_Product_Version__c object.
--04:23:57: 2112 rows read from SQL Table.
--04:23:57: 2112 rows successfully processed.
--04:23:57: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.


-------------------------------------------------------------------------------

-- REFRESH:

-- exec SF_Refresh en_Tritech_Prod,ServiceContract,'yes';
--- Starting SF_Refresh for ServiceContract V3.7.7
--02:35:12: Using Schema Error Action of yes
--02:35:12: Using last run time of 2019-04-05 19:15:00
--02:35:13: Identified 1 updated/inserted rows.
--02:35:13: Identified 0 deleted rows.
--02:35:13: Adding updated/inserted rows into ServiceContract
--- Ending SF_Refresh. Operation successful.



-- use Superion_Production;

-- exec sf_refresh 'MC_superion_prod','Registered_Product__c','Yes';
--- Starting SF_Refresh for Registered_Product__c V3.6.7
--02:35:36: Using Schema Error Action of yes
--02:35:36: Using last run time of 2019-04-08 17:46:00
--02:35:37: Identified 885 updated/inserted rows.
--02:35:37: Identified 0 deleted rows.
--02:35:37: Adding updated/inserted rows into Registered_Product__c
--- Ending SF_Refresh. Operation successful.


-- exec sf_refresh 'MC_superion_prod','Customer_Asset__c','Yes';
--- Starting SF_Refresh for Customer_Asset__c V3.6.7
--02:36:25: Using Schema Error Action of yes
--02:36:25: Using last run time of 2019-04-08 17:33:00
--02:36:29: Identified 1935 updated/inserted rows.
--02:36:29: Identified 0 deleted rows.
--02:36:29: Adding updated/inserted rows into Customer_Asset__c
--- Ending SF_Refresh. Operation successful.


-- exec sf_refresh 'MC_superion_prod','System_Product_Version__c','Yes';
--- Starting SF_Refresh for System_Product_Version__c V3.6.7
--02:37:30: Using Schema Error Action of yes
--02:37:30: Using last run time of 2019-04-05 19:46:00
--02:37:35: Identified 9628 updated/inserted rows.
--02:37:35: Identified 0 deleted rows.
--02:37:35: Adding updated/inserted rows into System_Product_Version__c
--- Ending SF_Refresh. Operation successful.



USE staging_prod;

DROP TABLE REGISTERED_PRODUCT__C_ADDITIONAL_CWP;
drop table REGISTERED_PRODUCT__C_ADDITIONAL_CWP_donot_create;
drop table REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create;

;
WITH cte_tt_svc_contract
     AS ( SELECT tts.id,
                 tts.accountid AS TT_Accountid,
                 ta.NAME       AS TTAcocuntName,
                 sa.id         AS Accountid_SUP,
                 tts.status,
                 tts.NAME,
                 tts.product_family_wmp__c,
                 tts.contract_type__c,
                 ttm.[tt product family],
                 ttm.[tt contract type],
                 ttm.productid,
				 ttm.[CentralSquare Product Group],
				 ttm.[CentralSquare Product Line],
                 p2.NAME       AS ttm_productname
          FROM   tritech_prod.dbo.SERVICECONTRACT tts
                 LEFT JOIN staging_prod.dbo.[TT SERVICE CONTRACT MAPPING] ttm
                        ON ttm.[tt product family] = tts.product_family_wmp__c AND
                           ttm.[tt contract type] = tts.contract_type__c
                 LEFT JOIN superion_production.dbo.ACCOUNT sa
                        ON sa.legacysfdcaccountid__c = tts.accountid
                 LEFT JOIN tritech_prod.dbo.ACCOUNT ta
                        ON ta.id = tts.accountid
                 LEFT JOIN superion_production.dbo.PRODUCT2 p2
                        ON p2.id = ttm.productid
          WHERE  status <> 'Expired' -- 'Expired'
         )
SELECT
--sca.*, cte.*
Cast( NULL AS NCHAR(18))     AS Id,
Cast( NULL AS NVARCHAR(255)) AS Error,
coalesce(cte.productid,'ID NOT FOUND')                AS Product__c,
cte.ttm_productname          AS NAME,
coalesce(sca.id,'ID NOT FOUND')               AS CustomerAsset__c,
sca.account__c               AS Account__c,
'True'                       AS Needs_Review__c,
cast('True' as nvarchar(5))  AS Create_Registered_Product_Load_Record,
cast(null as nchar(18))      as Existing_Sup_Registered_Product_id,
cte.product_family_wmp__c    AS TT_ServiceContract_ProdFamily,
cte.contract_type__c         AS TT_ServiceContract_ContractType,
sca.NAME                     AS SUP_Customer_Asset_Name,
cte.id                       AS Legacy_SFDC_ID__c,
cte.status                   AS TT_ServiceContract_Status,
cte.ttacocuntname            AS TT_AccountName,
cte.[CentralSquare Product Group],
cte.[CentralSquare Product Line]
INTO   REGISTERED_PRODUCT__C_ADDITIONAL_CWP
FROM   superion_production.dbo.CUSTOMER_ASSET__C sca
       JOIN cte_tt_svc_contract cte
         ON cte.accountid_sup = sca.account__c
WHERE  sca.product__c = '01t6A000000GyHWQA0'; --(6465 row(s) affected)

--exclude registered products to create based on account id and product id (already existing).

Update t1
set Create_Registered_Product_Load_Record = 'False',  Existing_Sup_Registered_Product_id = t2.id
from REGISTERED_PRODUCT__C_ADDITIONAL_CWP t1
join (select distinct id, Account__c, Product__c from Superion_Production.dbo.Registered_Product__c) t2 on t2.Product__c = t1.Product__c and t1.Account__c = t2.Account__c;
--(2395 row(s) affected)

--exclude registered products to create based on account id, product line, and product group (already existing).
Update t1
set Create_Registered_Product_Load_Record = 'False',  Existing_Sup_Registered_Product_id = t2.id
from REGISTERED_PRODUCT__C_ADDITIONAL_CWP t1
join (select t2.Product_Line__c,t2.Product_Group__c , t1.id, t1.Account__c  from Superion_Production.dbo.Registered_Product__c t1
join Superion_Production.dbo.product2 t2 on t2.id = t1.product__c) t2 on t2.Product_Line__c = t1.[CentralSquare Product Line] and t1.[CentralSquare Product Group] = t2.Product_Group__c
     and t2.Account__c = t1.Account__c
where t1.Create_Registered_Product_Load_Record = 'True'; --(1847 row(s) affected)


--exclude registered products to create based on if a customer has a substitute RP already manually created by Support or Finance group.
select t1.* from superion_production.dbo.Registered_Product__c t1
join tritech_prod.dbo.ServiceContract ts on ts.id = t1.Legacy_SFDC_ID__c and t1.Needs_Review__c = 'true' --(99 row(s) affected)


Update t1
set Create_Registered_Product_Load_Record = 'False',  Existing_Sup_Registered_Product_id = 'Already Created.'
from REGISTERED_PRODUCT__C_ADDITIONAL_CWP t1
join (select  distinct rpc.account__c from superion_production.dbo.Registered_Product__c rpc
join tritech_prod.dbo.ServiceContract ts on ts.id = rpc.Legacy_SFDC_ID__c and rpc.Needs_Review__c = 'true') t2 on  t2.Account__c = t1.Account__c
where t1.Create_Registered_Product_Load_Record = 'True';  --(110 row(s) affected)




SELECT *
into REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create
FROM   staging_prod.dbo.REGISTERED_PRODUCT__C_ADDITIONAL_CWP
where Create_Registered_Product_Load_Record = 'True'; --(2113 row(s) affected)


SELECT *
into REGISTERED_PRODUCT__C_ADDITIONAL_CWP_donot_create
FROM   staging_prod.dbo.REGISTERED_PRODUCT__C_ADDITIONAL_CWP
where Create_Registered_Product_Load_Record = 'false'; --(4352 row(s) affected)

SELECT *
FROM   staging_prod.dbo.REGISTERED_PRODUCT__C_ADDITIONAL_CWP
where Create_Registered_Product_Load_Record = 'True';

select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_donot_create

select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create

--checks:
select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create where Product__c = 'ID NOT FOUND';

update REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create set Product__c = '01t2G0000067rDEQAY', name = 'Crimemapping.com'  where Product__c = 'ID NOT FOUND'; ----(2 row(s) affected)

select * from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create where CustomerAsset__c = 'ID NOT FOUND'; --(0 row(s) affected)





------Create SPVs For Substitute RP's

drop table System_Product_Version__c_Insert_substitute_ADDITIONAL_CWP_create;

SELECT Cast( NULL AS NCHAR(18))     AS Id,
       Cast( NULL AS NVARCHAR(255)) AS Error,
	   t1.id as Registered_Product__c,
	   Cast( NULL AS NCHAR(18))     AS Environment__c,
	   Cast( NULL AS NVARCHAR(255)) AS Environment_Name,
	   Cast( NULL AS NVARCHAR(255)) AS Environment_match_type,
	   t1.[CentralSquare Product Group] ,
	   t1.Account__c
 INTO System_Product_Version__c_Insert_substitute_ADDITIONAL_CWP_create
 FROM REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create t1
 WHERE error = 'Operation Successful.'; --(2112 row(s) affected)


--find an environment based on account__c and CentralSquare Product Group (col c in the mapping).
UPDATE rp
SET    environment__c = se.id, Environment_Name =  se.Name, Environment_match_type = 'Account and CS Prod group'
FROM   SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE rp
       JOIN ( SELECT *
              FROM   superion_production.dbo.ENVIRONMENT__C
              WHERE  migrated_record__c = 'true' AND
                     legacy_source_system__c = 'Tritech' AND
                     NAME LIKE 'PROD%' ) se
         ON se.account__c = rp.account__c AND
            se.[name] LIKE '%' + rp.[centralsquare product group] + '%' ; --(1038 row(s) affected)

--for the records that did not find an environment based on account__c and CentralSquare Product Group (col c in the mapping), 
--default to the CWP environment for that Account.

UPDATE rp
SET    environment__c =spv.Environment__c, Environment_Name =  spv.Name, Environment_match_type = 'CWP default.'
FROM   SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE rp
join (
select rp.account__c, rp.Id as RP_Id, spv.Environment__c, e.Name  from superion_production.dbo.SYSTEM_PRODUCT_VERSION__C spv
join superion_production.dbo.registered_product__c rp on rp.id = spv.registered_product__c
join superion_production.dbo.Environment__c e on e.Id = spv.Environment__c 
where Product_Name__c like '%customer web%') spv on spv.Account__c = rp.Account__c 
where rp.Environment__c is null --(1073 row(s) affected)


select * from SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE
where environment__c is null; --a9a2G000000TSVDQA4

update SYSTEM_PRODUCT_VERSION__C_INSERT_SUBSTITUTE_ADDITIONAL_CWP_CREATE
 set Environment__c = 'a962G000000Gnc8QAC',  Environment_Name = 'Prod-Respond-Osceola County EMS, MI',
 Environment_match_type = 'Manual'
where Registered_Product__c = 'a9a2G000000TSVDQA4' and Environment__c is null;



---dev
/*

select sca.*
FROM   superion_production.dbo.CUSTOMER_ASSET__C sca
WHERE  sca.product__c = '01t6A000000GyHWQA0';

select * from product2 where id = '01t6A000000GyHWQA0' --CentralSquare Customer Web Portal

select * from superion_production.dbo.SYSTEM_PRODUCT_VERSION__C spv
join superion_production.dbo.registered_product__c rp on rp.id = spv.registered_product__c
where Product_Name__c like '%customer web%'



--find an environment based on account__c and CentralSquare Product Group (col c in the mapping).

select se.id, se.name, rp.[CentralSquare Product Group] ,  rp.* from REGISTERED_PRODUCT__C_ADDITIONAL_CWP_create rp 
join 
(
select * from Superion_Production.dbo.Environment__c
where Migrated_Record__c = 'true' and Legacy_Source_System__c = 'Tritech'
and name like 'PROD%') se on se.Account__c = rp.Account__c and 
se.[name] like '%'+ rp.[CentralSquare Product Group] + '%'
select dbo.fnSFDC18([Product ID 15]) , [Product ID 15], *
from [TT Service Contract Mapping]

select * from [TT SERVICE CONTRACT MAPPING]


select t2.Product_Line__c,t2.Product_Group__c , t1.id  from Superion_Production.dbo.Registered_Product__c t1
join Superion_Production.dbo.product2 t2 on t2.id = t1.product__c


[CentralSquare Product Group]	[CentralSquare Product Line]
Respond	ePCR
ePCR	Respond ePCR Base

update [TT Service Contract Mapping]
set [ProductID] = dbo.fnSFDC18([Product ID 15])
where [Product ID 15] like '01%'

select tts.AccountId, tts.status, tts.name, tts.Product_Family_WMP__c, tts.Contract_Type__c
from tritech_prod.dbo.ServiceContract tts
join staging_prod.dbo.[TT Service Contract Mapping] ttm on ttm.[TT Product Family] = tts.Product_Family_WMP__c and
      ttm.[TT Contract Type] = tts.Contract_Type__c
where Product_Family_WMP__c  = 'ems' AND Contract_Type__c  = 'Respond CAD'
AND STATUS <> 'Expired'

select id, name from product2 where name like '%customer web%'

y yall -- support mentioned one additional dimension on the service contract that could help us generate more precise registered products 
-- it's called "Contract Type" -- here's a TT prod report with that dimension added 
-- we would still provide a CS product ID for each combination of family/contract type 
-- any concerns on your end? If so, I won't show it to the business on this call  https://na70.salesforce.com/00O1E000006ZqWU



;WITH cte_customer_asset_sup as
(select * from Superion_Production.customer_asset__c )
SELECT tts.id,
       tts.accountid AS TT_Accountid,
       ta.NAME       AS TTAcocuntName,
       sa.id         AS Accountid_SUP,
       tts.status    AS TT_SupportContact_Status,   
       tts.NAME      AS TT_SupportContact_Name,
       tts.product_family_wmp__c AS TT_SupportContact_product_family_wmp,
       tts.contract_type__c  as TT_SupportContact_contract_type,
       ttm.[tt product family],
       ttm.[tt contract type],
       ttm.productid as Mapping_contract_type
FROM   tritech_prod.dbo.SERVICECONTRACT tts
       LEFT JOIN staging_prod.dbo.[TT SERVICE CONTRACT MAPPING] ttm
              ON ttm.[tt product family] = tts.product_family_wmp__c AND
                 ttm.[tt contract type] = tts.contract_type__c
       LEFT JOIN superion_production.dbo.ACCOUNT sa
              ON sa.legacysfdcaccountid__c = tts.accountid
       LEFT JOIN tritech_prod.dbo.ACCOUNT ta
              ON ta.id = tts.accountid
WHERE  status <> 'Expired' -- 'Expired'

select Product_Family_WMP__c, Contract_Type__c, count(*) from 
(
select tts.id, tts.AccountId as TT_Accountid, ta.name as TTAcocuntName, sa.id as Accountid_SUP, tts.status, tts.name, tts.Product_Family_WMP__c, tts.Contract_Type__c, ttm.[TT Product Family], ttm.[TT Contract Type],ttm.productid
from tritech_prod.dbo.ServiceContract tts
left join staging_prod.dbo.[TT Service Contract Mapping] ttm on ttm.[TT Product Family] = tts.Product_Family_WMP__c and
      ttm.[TT Contract Type] = tts.Contract_Type__c
left join Superion_Production.dbo.Account sa on sa.LegacySFDCAccountId__c = tts.AccountId
left join Tritech_PROD.dbo.account ta on ta.id = tts.AccountId
where status <>  'Expired' -- 'Expired'
and sa.id is null
--and ttm.[TT Product Family] is null 
--and ttm.[TT Contract Type] is null
) x
group by Product_Family_WMP__c, Contract_Type__c
order by Product_Family_WMP__c, Contract_Type__c
*/