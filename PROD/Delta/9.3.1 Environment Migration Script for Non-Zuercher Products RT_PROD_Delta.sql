/*****************************************************************************************************************************************************
REQ #		: REQ-
TITLE		:  Script
DEVELOPER	: RTECSON, MCORPUS, and PBOWEN
CREATED DT  : 02/14/2019
DETAIL		: 	
											
MODIFIED DT		MODIFIED BY			DESCRIPTION
===========		===========			===========
02/14/2019		Ron Tecson			Create Environment records for Non-Zuercher products.
02/19/2019		Ron Tecson			Execute a delta load (11 Records). Modified the main SQL since Marty provided a new script.
02/26/2019		Ron Tecson			Wipe and reload.
03/04/2019		Ron Tecson			Wipe and Reload
03/12/2019		Ron Tecson			Wipe and Reload.
03/15/2019		Ron Tecson			Wipe and Reload.
03/26/2019		Ron Tecson			Wipe and Reload.
03/28/2019		Ron Tecson			Repointed to Production.
03/30/2019		Ron Tecson			Executed in Production. Total Record Count: 3084 with no errors.
04/08/2019		Ron Tecson			Executed in Production for Delta. Total Record Count: 85 with no errors.

DECISIONS:

Pre-requisite:
	- Need to run the SPV Process to populate the load table SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD.

******************************************************************************************************************************************************/


---------------------------------------------------------------------------------
-- Refresh Data 
---------------------------------------------------------------------------------

Use Superion_Production
Exec SF_Refresh 'RT_SUPERION_PROD', 'Account', 'subset';

---------------------------------------------------------------------------------
-- Drop Environment__c_PreLoad_Non_Zuercher_RT
---------------------------------------------------------------------------------
USE Staging_PROD

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_PreLoad_Non_Zuercher_Delta_RT') -- 'Environment__c_PreLoad_Non_Zuercher_RT') 
DROP TABLE Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher_RT;

select distinct 
		CAST('' AS NVARCHAR(255)) AS ID,
		CAST('' AS NVARCHAR(255)) AS error,	
		'TriTech' as Legacy_Source_System__c,
		'true' as Migrated_Record__c,
		RegProd_ProductGroup, 
		RegProd_AccountId, 
		SUBSTRING(CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'PROD' ELSE Upper(environmenttype) END + '-' + regprod_productgroup + '-' + a.name,1,80) AS Name,
		RegProd_AccountId as Account__c,
		'Automatically created because no Hardware Software records existed in Tritech source'  as description__c,
              CASE WHEN Coalesce(environmenttype, 'Production') = 'Production' THEN 'Production'
                    WHEN EnvironmentType = 'Train' Then 'Training'
                    WHEN EnvironmentType = 'Test' Then 'Test' END as Type__c,
		a.Name as AccountName,
		'Yes' AS Active__c,
		NULL AS Legacy_SystemID__c,
		NULL AS LegacySystemIds,	
		NULL AS Winning_RecordType,
		NULL AS RecordTypes,
		NULL AS connection_type__c,
		NULL AS Notes__c
--INTO Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher_RT
INTO Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher_Delta_RT
--FROM Staging_PROD.dbo.SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD s
FROM Staging_PROD.dbo.System_Product_Version__c_Insert_MC_delta_load s
	JOIN Superion_Production.dbo.ACCOUNT a ON a.id = s.regprod_accountid
WHERE  environment__c = 'ID NOT FOUND'

-- 03/12 -- (208 rows affected)
-- 03/15 -- (1098 rows affected)
-- 03/26 -- (1388 rows affected)
-- 03/30 -- Production --> (3084 rows affected)
-- 04/08 -- Production Delta -- (85 rows affected)

---------------------------------------------------------------------------------
-- Drop Environment__c_Load_Non_Zuercher_RT
---------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='Environment__c_Load_Non_Zuercher_Delta_RT') 
DROP TABLE Staging_PROD.dbo.Environment__c_Load_Non_Zuercher_RT;

select * INTO Staging_PROD.dbo.Environment__c_Load_Non_Zuercher_Delta_RT -- Staging_PROD.dbo.Environment__c_Load_Non_Zuercher_RT
from Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher_Delta_RT
--from Staging_PROD.dbo.Environment__c_PreLoad_Non_Zuercher_RT

-- 03/30 -- Production --> (3084 rows affected)
-- 04/08 -- Production Delta -- (85 rows affected)

EXEC SF_ColCompare 'INSERT','RT_SUPERION_PROD','Environment__c_Load_Non_Zuercher_Delta_RT'

/************************************** ColCompare L O G **************************************
--- Starting SF_ColCompare V3.7.7
Problems found with Environment__c_Load_Non_Zuercher_Delta_RT. See output table for details.
Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 89]
--- Ending SF_ColCompare. Operation FAILED.

Salesforce object Environment__c does not contain column RegProd_ProductGroup
Salesforce object Environment__c does not contain column RegProd_AccountId
Salesforce object Environment__c does not contain column AccountName
Salesforce object Environment__c does not contain column LegacySystemIds
Salesforce object Environment__c does not contain column Winning_RecordType
Salesforce object Environment__c does not contain column RecordTypes
************************************** ColCompare L O G **************************************/

EXEC sf_bulkops 'INSERT','RT_SUPERION_PROD','Environment__c_Load_Non_Zuercher_Delta_RT'

/************************************** INSERT L O G **************************************

04/08/2019 - Production Delta
--- Starting SF_BulkOps for Environment__c_Load_Non_Zuercher_Delta_RT V3.7.7
19:22:28: Run the DBAmp.exe program.
19:22:28: DBAmp Bulk Operations. V3.7.7 (c) Copyright 2006-2017 forceAmp.com LLC
19:22:28: Inserting Environment__c_Load_Non_Zuercher_Delta_RT (SQL01 / Staging_PROD).
19:22:29: DBAmp is using the SQL Native Client.
19:22:29: SOAP Headers: 
19:22:29: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the Environment__c object.
19:22:29: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the Environment__c object.
19:22:29: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
19:22:29: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
19:22:29: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
19:22:29: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
19:22:30: 85 rows read from SQL Table.
19:22:30: 85 rows successfully processed.
19:22:30: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

03/30/2019 - Production

--- Starting SF_BulkOps for Environment__c_Load_Non_Zuercher_RT V3.6.7
15:11:24: Run the DBAmp.exe program.
15:11:24: DBAmp Bulk Operations. V3.6.7 (c) Copyright 2006-2017 forceAmp.com LLC
15:11:24: Inserting Environment__c_Load_Non_Zuercher_RT (SQL01 / Staging_PROD).
15:11:25: DBAmp is using the SQL Native Client.
15:11:25: SOAP Headers: 
15:11:25: Warning: Column 'RegProd_ProductGroup' ignored because it does not exist in the Environment__c object.
15:11:25: Warning: Column 'RegProd_AccountId' ignored because it does not exist in the Environment__c object.
15:11:25: Warning: Column 'AccountName' ignored because it does not exist in the Environment__c object.
15:11:25: Warning: Column 'LegacySystemIds' ignored because it does not exist in the Environment__c object.
15:11:25: Warning: Column 'Winning_RecordType' ignored because it does not exist in the Environment__c object.
15:11:25: Warning: Column 'RecordTypes' ignored because it does not exist in the Environment__c object.
15:11:37: 3084 rows read from SQL Table.
15:11:37: 3084 rows successfully processed.
15:11:37: Percent Failed = 0.000.
--- Ending SF_BulkOps. Operation successful.

************************************** INSERT L O G **************************************/

------------------------------------------------------------------------------------
-- VALIDATION
------------------------------------------------------------------------------------

select error, count(*) from Environment__c_Load_Non_Zuercher_Delta_RT
group by error

select LEN(Name), * from Environment__c_Load_Non_Zuercher_Delta_RT where error <> 'Operation Successful.'

select * from System_Product_Version__c_Insert_WebPortal_MC
where error <> 'Operation Successful.'

SYSTEM_PRODUCT_VERSION__C_INSERT_MC_LOAD

select * from Environment__c_Load_Non_Zuercher_Delta_RT

--*****************************************************************************************************

-------------------------------------------------------------------------------------------------------
-- Refresh Local Database
-------------------------------------------------------------------------------------------------------

USE Superion_Production

EXEC SF_Refresh 'RT_Superion_PROD', 'Environment__c', 'subset';