 /*
-- Author       : Jagan	
-- Created Date : 20-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech Account----------
Requirement No  :
Total Records   :  
Scope: 
 */

 /*
Use Tritech_PROD

EXEC SF_Refresh 'MC_TRITECH_PROD','User','Yes'
EXEC SF_Refresh 'MC_TRITECH_PROD','Account','Yes'
EXEC SF_Refresh 'MC_TRITECH_PROD','RecordType','Yes'


Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','Account','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','RecordType','Yes'
*/ 

--Prod Account Backup

--Drop table SUPERION_PRODUCTION.dbo.Account_bkp_29mar_jg_v1

Select * 
into SUPERION_PRODUCTION.dbo.Account_bkp_29mar_jg_v1
from SUPERION_PRODUCTION.dbo.Account;--48763

Select count(*) from SUPERION_PRODUCTION.dbo.Account
where Migrated_Record__c='True';--24826



Use Staging_PROD;

--Drop table Account_Tritech_SFDC_Preload_Delta;

DECLARE @Standard NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.RecordType 
where SobjectType='Account' and Name='Standard');

DECLARE @Prospect NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.RecordType 
where SobjectType='Account' and Name='Prospect');

DECLARE @3rdpartyTritech NVARCHAR(18) = (Select top 1 Id from Tritech_PROD.dbo.RecordType 
where SobjectType='Account' and Name='3rd Party');

DECLARE @AgencyTritech NVARCHAR(18) = (Select top 1 Id from Tritech_PROD.dbo.RecordType 
where SobjectType='Account' and Name='Agency');

DECLARE @3rdpartySuperion NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.RecordType 
where SobjectType='Account' and Name='Third Party');

DECLARE @Default NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'CentralSquare API');

DECLARE @Owner NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Id='0056A000000GyGbQAK')


Select

 ID = iif(supacc.id is not null,supacc.id,CAST(SPACE(18) as NVARCHAR(18)))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,LegacySFDCAccountId__c=acc.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,Entity_Name__c=iif(supacc.id is not null,supacc.Entity_Name__c,acc.Entity_Name__c)
,Entity_Name__c_orig=acc.Entity_Name__c
,Entity_Name__c_sup_new=supacc.Entity_Name__c
,Legacy_TT_Client_Product_Families__c=Active_Client_Product_Families__c
,Legacy_TT_Client_Product_Groups__c=Active_Contract_Product_Groups__c
,Advertise_Relationship__c=acc.Advertise_Relationship__c --(Added __c)
,Agreement_Effective_Date__c=acc.Agreement_Effective_Date__c
,Agreement_Status__c=acc.Agreement_Status__c

,Alarm_Management_Vendor__c_orig=Alarm_Management_Vendor__c
,Alarm_Mgmt_Current_SW_Vendor__c_sp=iif(alrmvendor.SandboxID is null and acc.Alarm_Management_Vendor__c is not null,'Alarm_Mgmt_Current_SW_Vendor__c is not found',alrmvendor.SandboxID)
,Alarm_Mgmt_Current_SW_Vendor__c=alrmvendor.SandboxID

,Average_Inmate_Daily_Population__c=acc.Average_Inmate_Daily_Population__c
,Last_SW_Purchase_Date_Billing__c=acc.Billing_Installed_Year__c

,Billing_Vendor__c_orig=Billing_Vendor__c
,Billing_Vendor__c_sp=iif(billingvendor.SandboxID is null and acc.Billing_Vendor__c is not null,'Billing_Current_SW_Vendor__c is not found',billingvendor.SandboxID)
,Billing_Current_SW_Vendor__c=billingvendor.SandboxID

,Bomgar_Approved_Client__c=acc.Bomgar_Approved_Client__c
,CAD_Dispatcher_Call_Taker_Seats__c=acc.CAD_Dispatcher_Call_Taker_Seats__c
,Last_SW_Purchase_Date_CAD__c=CAD_Installed_Year__c
,CAD_Parent_Fire_EMS_Users__c=acc.CAD_Parent_Fire_EMS_Users__c
,CAD_Parent_Total_Sworn_Officers__c=acc.CAD_Parent_Total_Sworn_Officers__c

,ContractDate__c=acc.Initial_Contract_Signed__c

,PSAP_Type__c=CAD_PSAP_Type__c
,CASE
 WHEN CAD_PSAP_Type__c IS NOT NULL THEN 'true'
 WHEN CAD_PSAP_Type__c IS NULL THEN 'false'
 END as PSAP__c

,CAD_Vendor__c_orig=CAD_Vendor__c
,CurrentCADSWVendor__c_sp=iif(cadvendor.SandboxId is null and acc.CAD_Vendor__c is not null,'CurrentCADSWVendor__c is not found',cadvendor.SandboxId)
,CurrentCADSWVendor__c= cadvendor.SandboxId                   

,Annual_Calls_for_Service_Received__c=Call_Volume_Annual__c
,Client__c=acc.Client__c
,Client_c_orig=acc.Client__c
,CASE
 WHEN acc.RecordTypeId=@3rdpartyTritech THEN @3rdpartySuperion
 WHEN acc.Client__c='true' THEN @Standard 
 WHEN acc.Client__c='false' THEN @Prospect
 END as RecordTypeId

,Type_orig=acc.Type
,Type=IIF(acc.Client__c='True','Customer','')

,Legacy_TT_Active_Contract_Types__c=Contract_Types__c
,County__c=acc.County__c
,Ship_To_County__c=acc.County__c

,CreatedById_orig=acc.CreatedById
,CreatedById=@Default

,CreatedDate=acc.CreatedDate
,TT_Legacy_Description__c=acc.Description
,Legacy_TT_Customer_Number__c=EMS_Customer_Number_WMP__c
,Last_SW_Purchase_Date_ePCR__c=ePCR_Installed_Year__c

,ePCR_Current_SW_Vendor__c_orig=ePCR_Vendor__c
,ePCR_Current_SW_Vendor__c_sp=iif(epcrvendor.SandboxId is null and acc.ePCR_Vendor__c is not null,'ePCR_Current_SW_Vendor__c is not found',epcrvendor.SandboxId)
,ePCR_Current_SW_Vendor__c=epcrvendor.SandboxID

,Fax=acc.Fax
,Last_SW_Purchase_Date_FBR__c=FBR_Installed_Year__c

,FBR_Current_SW_Vendor__c_orig=FBR_Vendor__c
,FBR_Current_SW_Vendor__c_sp=iif(fbrvendor.SandboxId is null and acc.FBR_Vendor__c is not null,'FBR_Current_SW_Vendor__c is not found',fbrvendor.SandboxId)
,FBR_Current_SW_Vendor__c=fbrvendor.SandboxID

,FDID__c_orig=FDID__c --(Added __c)
,ORI__c_orig=acc.ORI__c
,ORI__c=IIF(acc.ORI__c IS NULL,acc.FDID__c,acc.ORI__c)
,Fire_Career__c=acc.Fire_Career__c
,Fire_Civilian_Personnel__c=acc.Fire_Civilian_Personnel__c
,Fire_Paid_Per_Call__c=acc.Fire_Fighter_Paid_Per_Call__c
,Last_SW_Purchase_Date_Fire_RMS__c=Fire_Installed_Year__c
,Fire_Mobile_Units__c=acc.Fire_Mobile_Units__c
,Fire_RMS_Users__c=acc.Fire_RMS_Users__c

,Fire_RMS_Current_SW_Vendor__c_orig=Fire_Vendor__c
,Fire_RMS_Current_SW_Vendor__c_sp=iif(firevendor.SandboxId is null and acc.Fire_Vendor__c is not null,'Fire_RMS_Current_SW_Vendor__c is not found',firevendor.SandboxId)
,Fire_RMS_Current_SW_Vendor__c=firevendor.SandboxID

,Fire_Volunteer_Firefighting__c=acc.Fire_Volunteer_Firefighting__c
,Fire_Volunteer_Non_Firefighting__c=acc.Fire_Volunteer_Non_Firefighting__c
,Legacy_TT_Inactive_Contract_Types__c=Inactive_Contract_Types__c
,NumberofCallsforService__c=Incident_Volume__c
,NumberofBeds__c=Jail_Beds__c
,Last_SW_Purchase_Date_JMS__c=Jail_Installed_Year__c

,Jail_Vendor__c_orig=Jail_Vendor__c
,Current_Justice_SW_Vendor__c_sp=iif(jailvendor.SandboxID is null and acc.Jail_Vendor__c is not null,'Current_Justice_SW_Vendor__c is not found',jailvendor.SandboxID)
,Current_Justice_SW_Vendor__c=jailvendor.SandboxID

,Law_Civilian_Personnel__c=acc.Law_Civilian_Personnel__c
,NumberofMobileUnits__c=Law_Mobile_Units__c
,Law_RMS_Users__c=acc.Law_RMS_Users__c
,BillingCity=Mailing_Billing_City_WMP__c
,BillingCountry=Mailing_Billing_Country_WMP__c
,BillingState=Mailing_Billing_State_WMP__c
,BillingStreet=Mailing_Billing_Street_WMP__c
,BillingPostalCode=Mailing_Billing_Zip_Postal_Code_WMP__c
,Last_SW_Purchase_Date_Mapping__c=Mapping__c

,Mapping_Current_SW_Vendor__c_orig=Mapping_Vendor__c
,Mapping_Current_SW_Vendor__c_sp= iif(mpngvendor.SandboxID is null and acc.Mapping_Vendor__c is not null,'Mapping_Current_SW_Vendor__c is not found',mpngvendor.SandboxID)
,Mapping_Current_SW_Vendor__c=mpngvendor.SandboxID

,Last_SW_Purchase_Date_Mobile__c=Mobile_Installed_Year__c

,Mobile_Vendor__c_orig=Mobile_Vendor__c
,Current_Mobile_SW_Vendor__c_sp= iif(mobilevendor.SandboxID is null and acc.Mobile_Vendor__c is not null,'Current_Mobile_SW_Vendor__c is not found',mobilevendor.SandboxID)
,Current_Mobile_SW_Vendor__c=mobilevendor.SandboxID

,Name_orig=acc.Name
,Name_sup_new=supacc.Name

--,Name=(IIF(acc.RecordTypeId=@AgencyTritech,
--CONCAT(SUBSTRING(acc.Name,1,len(acc.Name)-3),', ',RIGHT(acc.Name,2)),acc.Name)

,Name=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
IIF(acc.RecordTypeId=@AgencyTritech and acc.name like '% __',CONCAT(SUBSTRING(acc.Name,1,len(acc.Name)-3),', ',RIGHT(acc.Name,2)),acc.Name),'Department of Public Safety','DPS'),'Sheriff''s Department','Sheriff')

,'Sheriff Department','Sheriff'),'Police Department','Police'),'Sheriff''s Dept.','Sheriff'),'Sheriff Dept.','Sheriff'),'Police Dept.','Police')

,'Sheriff''s Office','Sheriff'),'Sheriff Office','Sheriff')

,NDA_Expiration__c=acc.NDA_Expiration__c
,NDA_in_Place__c=acc.NDA_in_Place__c--(Added __c)
,NPS_Score__c_orig=NPS_Score__c
,NPS_Survey_Contact__c_orig=NPS_Survey_Contact__c
,Number_of_Sworn_Personnel_WMP__c=acc.Number_of_Sworn_Personnel_WMP__c
,NumberOfEmployees=acc.NumberOfEmployees
,of_Fire_Stations__c=acc.of_Fire_Stations__c
,of_firefighters_at_largest_PSAP_agency__c=acc.of_firefighters_at_largest_PSAP_agency__c
,of_Jail_Users__c=acc.of_Jail_Users__c


,OwnerId_orig=acc.OwnerId
--,OwnerId=ownerusr.Id
,OwnerId=@Owner
--,Account_Executive_Fin__c=IIF(acc.Client__c='False',ownrname.[Name],NULL) --commented by SL on 2/27 as per transformation rule changes
--,Installed_PA__c=IIF(acc.Client__c='True',ownrname.[Name],NULL)  --commented by SL on 2/27 as per transformation rule changes
--,Account_Executive_del_del__c=IIF(acc.Client__c='False',ownrname.[Name],NULL) --added by SL on 2/27 as per transformation rule changes
--,Account_Executive_Install_PSJ__c=IIF(acc.Client__c='True',ownrname.[Name],NULL)  --added by SL on 2/27 as per transformation rule changes
,PSJ_Territory__c =IIF(acc.Client__c='True',acc.PSJ_Territory__c,NULL) 
,Account_Executive_Install_PSJ__c=IIF(acc.Client__c='True',acc.AM_PSJ__C,NULL)
,Account_Executive_del_del__c  =IIF(acc.Client__c='False',IIF(ownrname.Isactive='False','OPEN',ownrname.[Name]),NULL)
,Customer_Success_Liaison__c_orig=Customer_Success_Liaison__c
,AAM_PSJ__c=IIF(acc.Client__c='True',IIF(acc.AAM_PSJ__c IS NULL,cslname.[Name],acc.AAM_PSJ__c),NULL)

,Legacy_TT_Parent_Active_Client_Product_F__c=Parent_Active_Client_Product_Families__c
,Legacy_TT_Parent_Active_Contract_Types__c=Parent_Active_Contract_Types__c
,Parent_Info_Not_Validated__c=acc.Parent_Info_Not_Validated__c

,ParentId_orig=acc.ParentId

,Planning_to_replace_911_provider__c_tt=acc.Planning_to_replace_911_provider__c
,Planning_to_replace_911_providerTTAPIValue=[911pcklst].PickListValue
,Planning_to_replace_911_providerTTPickListLabel=[911pcklst].PickListLabel
,Planning_to_replace_911_provider__c=[911pcklst].PickListLabel

,Planning_to_replace_CAD_provider__c_tt=acc.Planning_to_replace_CAD_provider__c
,Planning_to_replace_CAD_provider__cTTAPIValue=CADpcklst.PickListValue
,Planning_to_replace_CAD_provider__cTTPickListLabel=CADpcklst.PickListLabel
,Planning_to_replace_CAD_provider__c=CADpcklst.PickListLabel

,Planning_to_replace_Jail_provider__c_tt=acc.Planning_to_replace_Jail_provider__c
,Planning_to_replace_Jail_provider__cTTAPIValue=JAILpcklst.PickListValue
,Planning_to_replace_Jail_provider__cTTPickListLabel=JAILpcklst.PickListLabel
,Planning_to_replace_Jail_provider__c=JAILpcklst.PickListLabel

,Planning_to_replace_RMS_provider__c_tt=acc.Planning_to_replace_RMS_provider__c
,Planning_to_replace_RMS_providerTTAPIValue=RMSpcklst.PickListValue
,Planning_to_replace_RMS_providerTTPickListLabel=RMSpcklst.PickListLabel
,Planning_to_replace_RMS_provider__c=RMSpcklst.PickListLabel

,Phone=acc.Phone
,Population__c=Population_of_Area_Served_WMP__c
,Post_Go_Live_Survey_Comments__c_orig=Post_Go_Live_Survey_Comments__c
,Reference_List_Use__c=acc.Reference_List_Use__c
,Last_SW_Purchase_Date_Law_RMS__c=RMS_Installed_Year__c
,RMS_Parent_Total_Sworn_Officers__c=acc.RMS_Parent_Total_Sworn_Officers__c

,Law_RMS_Current_SW_Vendor__c_orig=RMS_Vendor__c
,Law_RMS_Current_SW_Vendor__c_sp= iif(lawvendor.SandboxID is null and acc.RMS_Vendor__c is not null,'Law_RMS_Current_SW_Vendor__c is not found',lawvendor.SandboxID)
,Law_RMS_Current_SW_Vendor__c=lawvendor.SandboxID

,Sales_Tax_ID__c=acc.Sales_Tax_ID__c
,Sales_Tax_Rate__c=acc.Sales_Tax_Rate__c
,Account_Notes__c=Sensitive_Account_WMP__c
,SupportNotes__c=Sensitive_Account_WMP__c
,PS_Agency_Type__c=Service_Type__c
,ShippingCity=Shipping_City_WMP__c
,ShippingCountry=Shipping_Country_WMP__c
,ShippingState=Shipping_State_WMP__c
,ShippingStreet=Shipping_Street_WMP__c
,ShippingPostalCode=Shipping_Zip_Postal_Code__c

,Survey_Date__c_orig=Survey_Date__c
,Survey_Notes__c_orig=Survey_Notes__c

,TriCON_Attendee__c_orig=TriCON_Attendee__c
,Z1_Attendee__c_orig=Z1_Attendee__c
,CASE
 WHEN TriCON_Attendee__c IS NOT NULL and Z1_Attendee__c IS NULL
 THEN CONCAT('TriCON ',REPLACE(CAST(TriCON_Attendee__c as varchar),';',';TriCON '))
 WHEN Z1_Attendee__c IS NOT NULL and TriCON_Attendee__c IS NULL
 THEN CONCAT('z1 ',REPLACE(CAST(Z1_Attendee__c as varchar),';',';z1 '))
 WHEN TriCON_Attendee__c IS NOT NULL and Z1_Attendee__c IS  NOT NULL
 THEN CONCAT(CONCAT('TriCON ',REPLACE(CAST(TriCON_Attendee__c as varchar),';',';TriCON ')),';',CONCAT('z1 ',REPLACE(CAST(Z1_Attendee__c as varchar),';',';z1 ')))
 END as User_Conference_Attendee__c
 ,TriCON_Attendee__c=replace(replace(replace(replace(cast(Z1_Attendee__c as varchar ),'2016','TriCON 2016'),
 '2017','TriCON 2017'),'2018','TriCON 2018'),'2019','TriCON 2019')
 ,Z1_Attendee__c_test=replace(replace(replace(replace(cast(Z1_Attendee__c as varchar),'2016','z1 2016'),
 '2017','z1 2017'),'2018','z1 2018'),'2019','z1 2019')

--,TriTech_Team_Manager__c= acc.TriTech_Team_Manager__c
,Website=acc.Website
,X3rd_Party_Type__c=acc.X3rd_Party_Type__c
,X911_Call_Taker_Positions__c=acc.X911_Call_Taker_Positions__c
,X911_Parent_Total_Sworn_Officers__c=acc.X911_Parent_Total_Sworn_Officers__c

,X911_Current_SW_Vendor__c_orig=X911_System_Manufacturer__c
,X911_Current_SW_Vendor__c_sp= iif(x911vendor.SandboxID is null and acc.X911_System_Manufacturer__c is not null,'X911_Current_SW_Vendor__c is not found',x911vendor.SandboxID)
,X911_Current_SW_Vendor__c=x911vendor.SandboxID

,Last_SW_Purchase_Date_911__c=X911_Telephony_Installed_Year__c

,Legacy_TT_Data_Conversion_Analyst__c=Z_Data_Conversion_Analyst__c
--,Data_Conversion_Analyst__c=dataanalyst.Id

,Legacy_TT_GIS_Analyst__c=Z_GIS_Analyst__c
--,GIS_Analyst__c=gisanalyst.Id

,Install_Name__c=Z_Install_Name__c


--",New Field (User Lookup) Account Manager=Account_Manager__c"

--",New Field (User Lookup) Assigned CAD BA=Assigned_BA_1__c"

--",New Field (User Lookup) Assigned Mobile BA=Assigned_BA_2__c"

--",New Field (User Lookup) Assigned RMS BA=Assigned_BA_3__c"

--",New Field (User Lookup) Assigned PM=Assigned_PM__c"

--",New Field (User Lookup) Assigned PM #2=Assigned_PM_2__c"

--",New Field (User Lookup) CAD Support Manager=CAD_Support_Manager__c"

,IQ_Account_Owner__c_orig=IQ_Account_Owner__c
,AE_IQ__c=iqname.[Name]

,X911_Account_Owner__c_orig=X911_Account_Owner__c
,AE_911__c=x911name.[Name]

--",New field (Account Lookup) Resale Company=Resale_Company__c"

--",New Field (User Lookup) RMS/Jail Manager=RMS_Jail_Support_Manager__c"

,RMS_Parent_Account__c_orig=acc.RMS_Parent_Account__c

--",New field (User Lookup) Field Name: AE-911=X911_Account_Owner__c"

,X911_Parent_Account__c_orig=acc.X911_Parent_Account__c

--",New Field (User Lookup) 911 Support Manager=X911_Support_Manager__c"

--",New Field (User Lookup) Field Name: Implementation Analyst=Z_Implementation_Analyst__c"

--",New Field (User Lookup) Field Name: Implementation Analyst #2=Z_Implementation_Analyst_2__c"

--",New Field (User Lookup) Field Name: Interface Analyst=Z_Interface_Analyst__c"

--",New Field (User Lookup) Project Manager=Z_Project_Manager__c"

,TT_Account_At_Risk__c=Account_At_Risk__c
,TT_Account_Status__c=acc.Account_Status__c
,Actions_Taken_Required__c=acc.Actions_Taken_Required__c
,TT_Account_Status_Comments__c=Comments__c

--,=Insight_SQL_Server_Address__c
--,=Insight_Version__c

,TT_Account_Status_Issues_Problems__c=Issues_Problems__c
,Last_Modified_Actions__c=acc.Last_Modified_Actions__c
,Last_Modified_Comments__c=acc.Last_Modified_Comments__c
,Last_Modified_Issues__c=acc.Last_Modified_Issues__c

--,Planning_to_replace_911_provider__c=acc.Planning_to_replace_911_provider__c
--,Planning_to_replace_CAD_provider__c=acc.Planning_to_replace_CAD_provider__c
--,Planning_to_replace_Jail_provider__c=acc.Planning_to_replace_Jail_provider__c
--,Planning_to_replace_RMS_provider__c=acc.Planning_to_replace_RMS_provider__c
--,=Post_Go_Live_Admin_Satisfaction_Survey__c
--,=Post_Go_Live_User_Satisfaction_Survey__c

,Secure_Folder_ID__c=acc.Secure_Folder_ID__c
,Wellness_Check_Contact_Goal__c=Client_Relations_Contact_Goal__c
,Account_Renewal_Confidence__c=Z_Account_Renewal_Confidence__c
,Account_Renewal_Notes__c=Z_Account_Renewal_Notes__c
,Change_in_Leadership__c=Z_Change_in_Leadership__c
,Completed_Yearly_Wellness_Checks__c=Z_Completed_Yearly_Contacts__c
,Last_Wellness_Check_Date__c=Z_Last_Yearly_Contact_Date__c
,Last_Wellness_Check_Type__c=Z_Last_Yearly_Contact_Type__c
,Low_Agency_Ownership_User_Adoption__c=Z_Low_Agency_Ownership_User_Adoption__c
,Maintenance_Lapse__c=Z_Maintenance_Lapse__c
,Next_Renewal_Date__c=Z_Next_Renewal_Date__c
,No_Communication_From_Customer__c=Z_No_Communication_From_Customer__c
,Not_a_Full_Suite_Customer__c=Z_Not_a_Full_Suite_Customer__c
,Renewal_Expected_Next_Year__c=Z_Renewal_Expected_Next_Year__c
,Total_Maintenance_Amount__c=Z_Total_Maintenance_Amount__c
,Vocal_with_Issues__c=Z_Vocal_with_Issues__c
,LegacySFDCAccountId_Superion=supacc.LegacySFDCAccountId__c
,Superion_id=supacc.id
,MergeFlag=accntmrg.[MERGE?]
,MergeSuperionId=accntmrg.[18-digit SUP Account ID]
,MergeTritechId=accntmrg.[18-digit TT Account ID]

 into Account_Tritech_SFDC_Preload_Delta
from Tritech_PROD.dbo.Account acc

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=acc.CreatedById

--Fetching CreatedById status
left join Tritech_PROD.dbo.[User] us on
us.Id=acc.CreatedById

--Fetching OwnerName(UserLookup)
left join Tritech_PROD.dbo.[User] ownrname
on ownrname.Id=acc.OwnerId

--Fetching OwnerId(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] ownerusr 
on ownerusr.Legacy_Tritech_Id__c=acc.OwnerId

--Fetching Alarm_Management_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping alrmvendor on
alrmvendor.TriTech_Competitor=acc.Alarm_Management_Vendor__c and isnull(alrmvendor.Objectname,'Account')='Account'

--Fetching CAD_Vendor__cId (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping cadvendor on 
cadvendor.TriTech_Competitor=acc.CAD_Vendor__c and isnull(cadvendor.Objectname,'Account')='Account'

--Fetching Jail_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping jailvendor on 
jailvendor.TriTech_Competitor=acc.Jail_Vendor__c and isnull(jailvendor.Objectname,'Account')='Account'

--Fetching Mobile_Vendor__c Id (Competitor Lookup) 
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping mobilevendor on 
mobilevendor.TriTech_Competitor=acc.Mobile_Vendor__c and isnull(mobilevendor.Objectname,'Account')='Account'

--Fetching Billing_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping billingvendor on
billingvendor.TriTech_Competitor=acc.Billing_Vendor__c and isnull(billingvendor.Objectname,'Account')='Account'

--Fetching ePCR_Current_SW_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping epcrvendor on
epcrvendor.TriTech_Competitor=acc.ePCR_Vendor__c and isnull(epcrvendor.Objectname,'Account')='Account'

--Fetching FBR_Current_SW_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping fbrvendor on
fbrvendor.TriTech_Competitor=acc.FBR_Vendor__c and isnull(fbrvendor.Objectname,'Account')='Account'

--Fetching Fire_RMS_Current_SW_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping firevendor on
firevendor.TriTech_Competitor=acc.Fire_Vendor__c and isnull(firevendor.Objectname,'Account')='Account'

--Fetching Mapping_Current_SW_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping mpngvendor on
mpngvendor.TriTech_Competitor=acc.Mapping_Vendor__c and isnull(mpngvendor.Objectname,'Account')='Account'

--Fetching Law_RMS_Current_SW_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping lawvendor on
lawvendor.TriTech_Competitor=acc.RMS_Vendor__c and isnull(lawvendor.Objectname,'Account')='Account'

--Fetching X911_Current_SW_Vendor__c Id (Competitor Lookup)
left join Staging_PROD.dbo.Tritech_competitor_lookup_mapping x911vendor on
x911vendor.TriTech_Competitor=acc.X911_System_Manufacturer__c and isnull(x911vendor.Objectname,'Account')='Account'

left join tt_prod_sys_sfpicklists RMSpcklst
on RMSpcklst.objectname='Account' and RMSpcklst.Fieldname='Planning_to_replace_RMS_provider__c'
and RMSpcklst.PickListValue=acc.Planning_to_replace_RMS_provider__c

left join tt_prod_sys_sfpicklists [911pcklst]
on [911pcklst].objectname='Account' and [911pcklst].Fieldname='Planning_to_replace_911_provider__c'
and [911pcklst].PickListValue=acc.Planning_to_replace_911_provider__c

left join tt_prod_sys_sfpicklists CADpcklst
on CADpcklst.objectname='Account' and CADpcklst.Fieldname='Planning_to_replace_CAD_provider__c'
and CADpcklst.PickListValue=acc.Planning_to_replace_CAD_provider__c

left join tt_prod_sys_sfpicklists JAILpcklst
on JAILpcklst.objectname='Account' and JAILpcklst.Fieldname='Planning_to_replace_Jail_provider__c'
and JAILpcklst.PickListValue=acc.Planning_to_replace_Jail_provider__c

----Fetching Z_Data_Conversion_Analyst__c Id(UserLookup)
--left join Superion_FULLSB.dbo.[User] dataanalyst 
--on dataanalyst.Legacy_Tritech_Id__c=acc.Z_Data_Conversion_Analyst__c

----Fetching Z_GIS_Analyst__c Id(UserLookup)
--left join Superion_FULLSB.dbo.[User] gisanalyst 
--on gisanalyst.Legacy_Tritech_Id__c=acc.Z_GIS_Analyst__c

--Fetching Account LegacySFDCAccountId__c in Target
left join SUPERION_PRODUCTION.dbo.Account supacc
on supacc.LegacySFDCAccountId__c=acc.Id 

left join Staging_PROD.dbo.TT_Superion_Account_Merge accntmrg
on accntmrg.[18-digit TT Account ID]=acc.Id  and accntmrg.[MERGE?]='Yes'

--Fetching IQ_Account_Owner__cName(UserLookup)
left join Tritech_PROD.dbo.[User] iqname 
on iqname.Id=acc.IQ_Account_Owner__c

----Fetching Customer_Success_Liaison__c_Name(UserLookup)
left join Tritech_PROD.dbo.[User] cslname 
on cslname.Id=acc.Customer_Success_Liaison__c

--Fetching X911_Account_Owner__cName(UserLookup)
left join Tritech_PROD.dbo.[User] x911name 
on x911name.Id=acc.X911_Account_Owner__c
where (isnull(acc.EMS_Customer_Number_WMP__c,'X')<>'Internal' and acc.LastModifiedDate>='19-MAR-2019')--added by SL 3/1 as per new changes
or (acc.EMS_Customer_Number_WMP__c='Internal' 
and acc.[Name] IN ('EMS Test Account','Omega CrimeMapping Public Support','TriTech Software Systems','IMC','Zuercher Technologies','Tiburon Test Account'))

;--6310


----------------------------------------------------------------------------------------------
--Update  acc set acc.Name=CONCAT(SUBSTRING(acc.Name,1,len(acc.Name)-3),', ',RIGHT(acc.Name,2))
--select name,Name_sup_new,name_orig, * 
from Account_Tritech_SFDC_Preload_Delta acc
where Name  like '% __' and Name not like '%,%'
------------------------------------------------------------------
select name,Name_sup_new,name_orig, * 
--update acc set  name=Name_sup_new
from Account_Tritech_SFDC_Preload_Delta acc
where Name<>isnull(Name_sup_new,name);--23

select name,Name_sup_new,name_orig, Entity_Name__c,Entity_Name__c_orig,Entity_Name__c_sup_new
--update acc set  name=Name_sup_new
from Account_Tritech_SFDC_Preload_Delta acc
where Entity_Name__c<>isnull(Entity_Name__c_sup_new,Entity_Name__c);--0


--------------------------------------------
Select count(*) from Tritech_PROD.dbo.Account;--26408

Select count(*) from Account_Tritech_SFDC_Preload_Delta;--6310

select count(*) from Tritech_PROD.dbo.Account
where EMS_Customer_Number_WMP__c='Internal' ;--21

select * from Account_Tritech_SFDC_Preload_Delta
where Legacy_TT_Customer_Number__c='Internal'

Select LegacySFDCAccountId__c,count(*) from Staging_PROD.dbo.Account_Tritech_SFDC_Preload_Delta
group by LegacySFDCAccountId__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.Account_Tritech_SFDC_Insert_Delta;

Select * into 
Account_Tritech_SFDC_Insert_Delta
from Account_Tritech_SFDC_Preload_Delta
where mergeflag is  null and superion_id is null;
; --17

Select name,* from Account_Tritech_SFDC_Insert_Delta;

Select LegacySFDCAccountId__c,count(*) from Staging_PROD.dbo.Account_Tritech_SFDC_Insert_Delta
group by LegacySFDCAccountId__c
having count(*)>1;--0

--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'Account_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object Account does not contain column Entity_Name__c_orig
Salesforce object Account does not contain column Entity_Name__c_sup_new
Salesforce object Account does not contain column Alarm_Management_Vendor__c_orig
Salesforce object Account does not contain column Alarm_Mgmt_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Billing_Vendor__c_orig
Salesforce object Account does not contain column Billing_Vendor__c_sp
Salesforce object Account does not contain column CAD_Vendor__c_orig
Salesforce object Account does not contain column CurrentCADSWVendor__c_sp
Salesforce object Account does not contain column Client_c_orig
Salesforce object Account does not contain column Type_orig
Salesforce object Account does not contain column CreatedById_orig
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column FDID__c_orig
Salesforce object Account does not contain column ORI__c_orig
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Jail_Vendor__c_orig
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_sp
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Mobile_Vendor__c_orig
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_sp
Salesforce object Account does not contain column Name_orig
Salesforce object Account does not contain column Name_sup_new
Salesforce object Account does not contain column NPS_Score__c_orig
Salesforce object Account does not contain column NPS_Survey_Contact__c_orig
Salesforce object Account does not contain column OwnerId_orig
Salesforce object Account does not contain column Customer_Success_Liaison__c_orig
Salesforce object Account does not contain column ParentId_orig
Salesforce object Account does not contain column Planning_to_replace_911_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_911_providerTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_911_providerTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__cTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__cTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__cTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__cTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_RMS_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_RMS_providerTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_RMS_providerTTPickListLabel
Salesforce object Account does not contain column Post_Go_Live_Survey_Comments__c_orig
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Survey_Date__c_orig
Salesforce object Account does not contain column Survey_Notes__c_orig
Salesforce object Account does not contain column TriCON_Attendee__c_orig
Salesforce object Account does not contain column Z1_Attendee__c_orig
Salesforce object Account does not contain column TriCON_Attendee__c
Salesforce object Account does not contain column Z1_Attendee__c_test
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column IQ_Account_Owner__c_orig
Salesforce object Account does not contain column X911_Account_Owner__c_orig
Salesforce object Account does not contain column RMS_Parent_Account__c_orig
Salesforce object Account does not contain column X911_Parent_Account__c_orig
Salesforce object Account does not contain column LegacySFDCAccountId_Superion
Salesforce object Account does not contain column Superion_id
Salesforce object Account does not contain column MergeFlag
Salesforce object Account does not contain column MergeSuperionId
Salesforce object Account does not contain column MergeTritechId
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','Account_Tritech_SFDC_Insert_Delta' --(24:26)


----------------------------------------------------------------------------------------

--drop table Account_Tritech_SFDC_Insert_Delta_errors
select * 
into Account_Tritech_SFDC_Insert_Delta_errors
from Account_Tritech_SFDC_Insert_Delta
where error<>'Operation Successful.';--2

--drop table Account_Tritech_SFDC_Insert_Delta_errors_bkp
select * 
into Account_Tritech_SFDC_Insert_Delta_errors_bkp
from Account_Tritech_SFDC_Insert_Delta
where error<>'Operation Successful.';--2


select  PS_Agency_Type__c,User_Conference_Attendee__c,AAM_PSJ__c,* from Account_Tritech_SFDC_Insert_Delta_errors;


Select  recordtypeid, aam_psj__C,error,*
--update a set aam_psj__C=null
from Account_Tritech_SFDC_Insert_Delta_errors a
where  aam_psj__C in (select b.aam_psj__C from Account_Tritech_SFDC_Insert_Delta_errors b
 where b.error like '%bad value for restricted picklist field%'+b.aam_psj__C)--2


--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','Account_Tritech_SFDC_Insert_Delta_errors' --(9:22)

--delete 
from Account_Tritech_SFDC_Insert_Delta
where error<>'Operation Successful.'; --2

--insert into Account_Tritech_SFDC_Insert_Delta
select * from Account_Tritech_SFDC_Insert_Delta_errors;--2

--------------------------------------------------------------------------------------------------
Select error,count(*) from Account_Tritech_SFDC_Insert_Delta
group by error;--17
------------------------------------------------------------------------------------------------------

--Drop table Staging_PROD.dbo.Account_Tritech_SFDC_update_Delta;

Select * into 
Account_Tritech_SFDC_update_Delta
from Account_Tritech_SFDC_Preload_Delta
where mergeflag is  null and superion_id is not null;
; --5981

Select name,* from Account_Tritech_SFDC_update_Delta;

Select LegacySFDCAccountId__c,count(*) from Staging_PROD.dbo.Account_Tritech_SFDC_update_Delta
group by LegacySFDCAccountId__c
having count(*)>1;--0

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Account_Tritech_SFDC_update_Delta' 
/*
Salesforce object Account does not contain column Entity_Name__c_orig
Salesforce object Account does not contain column Entity_Name__c_sup_new
Salesforce object Account does not contain column Alarm_Management_Vendor__c_orig
Salesforce object Account does not contain column Alarm_Mgmt_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Billing_Vendor__c_orig
Salesforce object Account does not contain column Billing_Vendor__c_sp
Salesforce object Account does not contain column CAD_Vendor__c_orig
Salesforce object Account does not contain column CurrentCADSWVendor__c_sp
Salesforce object Account does not contain column Client_c_orig
Salesforce object Account does not contain column Type_orig
Salesforce object Account does not contain column CreatedById_orig
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column FDID__c_orig
Salesforce object Account does not contain column ORI__c_orig
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Jail_Vendor__c_orig
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_sp
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Mobile_Vendor__c_orig
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_sp
Salesforce object Account does not contain column Name_orig
Salesforce object Account does not contain column Name_sup_new
Salesforce object Account does not contain column NPS_Score__c_orig
Salesforce object Account does not contain column NPS_Survey_Contact__c_orig
Salesforce object Account does not contain column OwnerId_orig
Salesforce object Account does not contain column Customer_Success_Liaison__c_orig
Salesforce object Account does not contain column ParentId_orig
Salesforce object Account does not contain column Planning_to_replace_911_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_911_providerTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_911_providerTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__cTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__cTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__cTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__cTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_RMS_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_RMS_providerTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_RMS_providerTTPickListLabel
Salesforce object Account does not contain column Post_Go_Live_Survey_Comments__c_orig
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column Survey_Date__c_orig
Salesforce object Account does not contain column Survey_Notes__c_orig
Salesforce object Account does not contain column TriCON_Attendee__c_orig
Salesforce object Account does not contain column Z1_Attendee__c_orig
Salesforce object Account does not contain column TriCON_Attendee__c
Salesforce object Account does not contain column Z1_Attendee__c_test
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_orig
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_sp
Salesforce object Account does not contain column IQ_Account_Owner__c_orig
Salesforce object Account does not contain column X911_Account_Owner__c_orig
Salesforce object Account does not contain column RMS_Parent_Account__c_orig
Salesforce object Account does not contain column X911_Parent_Account__c_orig
Salesforce object Account does not contain column LegacySFDCAccountId_Superion
Salesforce object Account does not contain column Superion_id
Salesforce object Account does not contain column MergeFlag
Salesforce object Account does not contain column MergeSuperionId
Salesforce object Account does not contain column MergeTritechId
Column CreatedById is not updateable in the salesforce object Account
Column CreatedDate is not updateable in the salesforce object Account

*/

--Exec sf_bulkops 'Update:batchsize(10)','SL_SUPERION_PROD', 'Account_Tritech_SFDC_update_Delta' 

select *  from Account_Tritech_SFDC_update_Delta
where error<>'Operation Successful.';--1657

select error,count(*) from Account_Tritech_SFDC_update_Delta
group by error


--drop table Account_Tritech_SFDC_update_Delta_errors
select * 
into Account_Tritech_SFDC_update_Delta_errors
from Account_Tritech_SFDC_update_Delta
where error<>'Operation Successful.';--1657

--drop table Account_Tritech_SFDC_update_Delta_errors_bkp
select * 
into Account_Tritech_SFDC_update_Delta_errors_bkp
from Account_Tritech_SFDC_update_Delta
where error<>'Operation Successful.';--1657

--Update a set AE_911__c=NULL
--select * 
from Account_Tritech_SFDC_update_Delta_errors a
where AE_911__c in ('Fred Himburg','Brian Fluegeman')--1642


--Update a set AAM_PSJ__C=NULL
select distinct AAM_PSJ__C--,* 
from Account_Tritech_SFDC_update_Delta_errors a
--where aam_psj__c in 
where error  NOT IN('AE-911: bad value for restricted picklist field: Fred Himburg',
'bad value for restricted picklist field: Brian Fluegeman','Operation Successful.')


select distinct AAM_PSJ__C--,* 
--update a set AAM_PSJ__C=null
from Account_Tritech_SFDC_update_Delta_errors a
--where aam_psj__c in 
where error  like '%bad value for restricted picklist field:%'+AAM_PSJ__C;--22

select  AE_IQ__c--,* 
--update a set AE_IQ__c=null
from Account_Tritech_SFDC_update_Delta_errors a
--where aam_psj__c in 
where error  like '%bad value for restricted picklist field:%'+AE_IQ__c;--6

--Exec sf_bulkops 'Update:batchsize(10)','SL_SUPERION_PROD', 'Account_Tritech_SFDC_update_Delta_errors' 


select AAM_PSJ__c,AE_IQ__c,AE_911__c,*  from Account_Tritech_SFDC_update_Delta_errors
where error<>'Operation Successful.';--1657

select error,count(*) from Account_Tritech_SFDC_update_Delta_errors
group by error

--delete 
--select *  
from Account_Tritech_SFDC_update_Delta
where error<>'Operation Successful.';--1657

--insert into Account_Tritech_SFDC_update_Delta
select * from Account_Tritech_SFDC_update_Delta_errors;

--drop table Account_Tritech_SFDC_update_Delta_errors2
select * 
into Account_Tritech_SFDC_update_Delta_errors2
from Account_Tritech_SFDC_update_Delta
where error<>'Operation Successful.';--1

--drop table Account_Tritech_SFDC_update_Delta_errors2_bkp
select * 
into Account_Tritech_SFDC_update_Delta_errors2_bkp
from Account_Tritech_SFDC_update_Delta
where error<>'Operation Successful.';--1 


select  AE_IQ__c--,* 
--update a set AE_IQ__c=null
from Account_Tritech_SFDC_update_Delta_errors2 a
--where aam_psj__c in 
where error  like '%bad value for restricted picklist field:%'+AE_IQ__c;--6

--Exec sf_bulkops 'Update:batchsize(10)','SL_SUPERION_PROD', 'Account_Tritech_SFDC_update_Delta_errors2' 

--delete 
--select *  
from Account_Tritech_SFDC_update_Delta
where error<>'Operation Successful.';--1

--insert into Account_Tritech_SFDC_update_Delta
select * from Account_Tritech_SFDC_update_Delta_errors2;



--Update Script for Account Records

Use Staging_PROD;

--Drop table Account_Tritech_SFDC_UpdateMergingAccounts_Delta;

  DECLARE @Owner NVARCHAR(18) = (Select top 1 id  from SUPERION_PRODUCTION.dbo.[User] 
 where  Name='Deborah Solorzano' and Id='0056A000000GyGbQAK');

 DECLARE @Standard NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.RecordType 
 where SobjectType='Account' and Name='Standard');

 Select 

 ID = leg.MergeSuperionId
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,LegacySFDCAccountId__c=leg.MergeTritechId
,LegacySFDCAccountId__c_Sup=tar.LegacySFDCAccountId__c
,Entity_Name__c_sup=tar.Entity_Name__c
,Entity_Name__c_tt=leg.Entity_Name__c
--,Alarm_Mgmt_Current_SW_Vendor__c_sup=tar.Alarm_Mgmt_Current_SW_Vendor__c
--,Alarm_Mgmt_Current_SW_Vendor__c_tt=leg.Alarm_Mgmt_Current_SW_Vendor__c
--,Last_SW_Purchase_Date_CAD__c_sup=tar.Last_SW_Purchase_Date_CAD__c
--,Last_SW_Purchase_Date_CAD__c_tt=leg.Last_SW_Purchase_Date_CAD__c
,PSAP_Type__c_sup=tar.PSAP_Type__c
,PSAP__c_sup=tar.PSAP__c
,PSAP_Type__c=leg.PSAP_Type__c
,PSAP__c=leg.PSAP__c
,Sales_Team__c_tt=tt.Sales_Team__c
,CurrentCADSWVendor__c_tt=leg.CurrentCADSWVendor__c
,CurrentCADSWVendor__c_sup=tar.CurrentCADSWVendor__c
,CurrentCADSWVendor__c=IIF(tt.Sales_Team__c like 'Zuer%',leg.CurrentCADSWVendor__c,tar.CurrentCADSWVendor__c)
,Client__c_superion=tar.Client__c
,Client__c=leg.Client_c_orig
,RecordTypeId_sup=tar.RecordTypeId
,CASE
 WHEN leg.Client_c_orig='true' THEN @Standard 
 ELSE tar.RecordTypeId
 END as RecordTypeId
,County_c_sup=tar.County__c
,County__c=leg.County__c
,Ship_To_County__c_sup=tar.Ship_To_County__c
,Ship_To_County__c=leg.Ship_To_County__c
,CreatedDate_sup=tar.CreatedDate
,CreatedDate_tt=leg.CreatedDate
,Fax_sup=tar.Fax
,Fax=leg.Fax
,NumberofBeds__c_sup=tar.NumberofBeds__c
,NumberofBeds__c=leg.NumberofBeds__c
,Last_SW_Purchase_Date_JMS__c_sup=tar.Last_SW_Purchase_Date_JMS__c
,Last_SW_Purchase_Date_JMS__c=leg.Last_SW_Purchase_Date_JMS__c
,Current_Justice_SW_Vendor__c_sup=tar.Current_Justice_SW_Vendor__c
,Current_Justice_SW_Vendor__c=leg.Current_Justice_SW_Vendor__c
,NumberofMobileUnits__c_sup=tar.NumberofMobileUnits__c
,NumberofMobileUnits__c=leg.NumberofMobileUnits__c
,Mailing_Billing_City_WMP__c_btst=tt.Mailing_Billing_City_WMP__c
,Mailing_Billing_State_WMP__c_btst=tt.Mailing_Billing_State_WMP__c
,Mailing_Billing_Street_WMP__c_btst=tt.Mailing_Billing_Street_WMP__c
,Mailing_Billing_Zip_Postal_Code_WMP__c_btst=tt.Mailing_Billing_Zip_Postal_Code_WMP__c
,Mailing_Billing_Country_WMP__c_btst=tt.Mailing_Billing_Country_WMP__c
,Current_Mobile_SW_Vendor__c_sup=tar.Current_Mobile_SW_Vendor__c
,Current_Mobile_SW_Vendor__c=leg.Current_Mobile_SW_Vendor__c
,Name_sup=tar.Name
,Name=leg.Name
,NumberOfEmployees_sup=tar.NumberOfEmployees
,NumberOfEmployees=leg.NumberOfEmployees
,OwnerId_sup=tar.OwnerId
,OwnerId=@Owner

,ParentId_orig=leg.ParentId_orig
--,ParentId=prnt.Id

,Planning_to_replace_911_provider__c_sup=tar.Planning_to_replace_911_provider__c
,Planning_to_replace_911_provider__c_tt=tt.Planning_to_replace_911_provider__c
,Planning_to_replace_911_providerTTAPIValue=[911pcklst].PickListValue
,Planning_to_replace_911_providerTTPickListLabel=[911pcklst].PickListLabel
,Planning_to_replace_911_provider__c=[911pcklst].PickListLabel

,Planning_to_replace_CAD_provider__c_sup=tar.Planning_to_replace_CAD_provider__c
,Planning_to_replace_CAD_provider__c_tt=tt.Planning_to_replace_CAD_provider__c
,Planning_to_replace_CAD_provider__cTTAPIValue=CADpcklst.PickListValue
,Planning_to_replace_CAD_provider__cTTPickListLabel=CADpcklst.PickListLabel
,Planning_to_replace_CAD_provider__c=CADpcklst.PickListLabel

,Planning_to_replace_Jail_provider__c_sup=tar.Planning_to_replace_Jail_provider__c
,Planning_to_replace_Jail_provider__c_tt=tt.Planning_to_replace_Jail_provider__c
,Planning_to_replace_Jail_provider__cTTAPIValue=JAILpcklst.PickListValue
,Planning_to_replace_Jail_provider__cTTPickListLabel=JAILpcklst.PickListLabel
,Planning_to_replace_Jail_provider__c=JAILpcklst.PickListLabel

,Planning_to_replace_RMS_provider__c_sup=tar.Planning_to_replace_RMS_provider__c
,Planning_to_replace_RMS_provider__c_tt=tt.Planning_to_replace_RMS_provider__c
,Planning_to_replace_RMS_providerTTAPIValue=RMSpcklst.PickListValue
,Planning_to_replace_RMS_providerTTPickListLabel=RMSpcklst.PickListLabel
,Planning_to_replace_RMS_provider__c=RMSpcklst.PickListLabel

,Phone_sup=tar.Phone
,Phone=leg.Phone
,Population__c_leg=leg.Population__c
,Population__c_sup=tar.Population__c
,Sensitive_Account_WMP__c_legacy=leg.Account_Notes__c
,Sensitive_Account_WMP__c_tar=tar.Account_Notes__c
,CASE
WHEN isnull(cast(leg.Account_Notes__c as nvarchar(max)),'X')=isnull(cast(tar.Account_Notes__c as nvarchar(max)),'X') THEN tar.Account_Notes__c
 WHEN leg.Account_Notes__c IS NULL and tar.Account_Notes__c IS NOT NULL THEN tar.Account_Notes__c
 WHEN leg.Account_Notes__c IS NOT NULL and tar.Account_Notes__c IS NULL THEN leg.Account_Notes__c
 WHEN leg.Account_Notes__c IS NOT NULL and tar.Account_Notes__c IS NOT NULL THEN CONCAT(leg.Account_Notes__c,';',tar.Account_Notes__c)
 ELSE NULL
 END as Account_Notes__c
,PS_Agency_Type__c_sup=cast(tar.PS_Agency_Type__c as nvarchar(max))
,PS_Agency_Type__c_tt=cast(leg.PS_Agency_Type__c as nvarchar(max))
/*,CASE 
 WHEN CAST(leg.PS_Agency_Type__c as NVARCHAR(MAX))=CAST(tar.PS_Agency_Type__c as NVARCHAR(max))THEN tar.PS_Agency_Type__c
 ELSE CONCAT(CAST(leg.PS_Agency_Type__c as NVARCHAR(MAX)),CAST(tar.PS_Agency_Type__c as NVARCHAR(max)))
 END as PS_Agency_Type__c*/
  ,cast(CASE 
 WHEN CAST(isnull(leg.PS_Agency_Type__c,'X') as NVARCHAR(MAX))=CAST(isnull(tar.PS_Agency_Type__c,'X') as NVARCHAR(max))
 THEN tar.PS_Agency_Type__c
 WHEN leg.PS_Agency_Type__c is null and tar.PS_Agency_Type__c is not null THEN replace(replace(CAST(tar.PS_Agency_Type__c as NVARCHAR(MAX)),'911 Communications Center','911'),'Law Enforcement','Law')
 WHEN leg.PS_Agency_Type__c is not null and tar.PS_Agency_Type__c is  null THEN leg.PS_Agency_Type__c
 else CONCAT(replace(replace(CAST(tar.PS_Agency_Type__c as NVARCHAR(MAX)),'911 Communications Center','911'),'Law Enforcement','Law'),';',CAST(leg.PS_Agency_Type__c as NVARCHAR(max)))
 END as nvarchar(max)) as PS_Agency_Type__c

,Shipping_City_WMP__c_btst=tt.Shipping_City_WMP__c
,Shipping_State_WMP__c_btst=tt.Shipping_State_WMP__c
,Shipping_Street_WMP__c_btst=tt.Shipping_Street_WMP__c
,Shipping_Zip_Postal_Code__c_btst=tt.Shipping_Zip_Postal_Code__c
,Time_Zone__c_sup=tar.Time_Zone__c
,Time_Zone__c_tt=tt.Time_Zone__c
,Website_sup=tar.Website
,Website_tt=leg.Website
,AccountSource_sup=tar.AccountSource
,AccountSource_tt=tt.AccountSource

,Legacy_TT_Client_Product_Families__c_sup=tar.Legacy_TT_Client_Product_Families__c
,Legacy_TT_Client_Product_Families__c=leg.Legacy_TT_Client_Product_Families__c
,Legacy_TT_Client_Product_Groups__c_sup=tar.Legacy_TT_Client_Product_Groups__c
,Legacy_TT_Client_Product_Groups__c=leg.Legacy_TT_Client_Product_Groups__c
--,Last_SW_Purchase_Date_Billing__c_sup=tar.Last_SW_Purchase_Date_Billing__c
--,Last_SW_Purchase_Date_Billing__c=leg.Last_SW_Purchase_Date_Billing__c
--,Annual_Calls_for_Service_Received__c_sup=tar.Annual_Calls_for_Service_Received__c
--,Annual_Calls_for_Service_Received__c=leg.Annual_Calls_for_Service_Received__c
--,Legacy_TT_Active_Contract_Types__c_sup=tar.Legacy_TT_Active_Contract_Types__c
--,Legacy_TT_Active_Contract_Types__c=leg.Legacy_TT_Active_Contract_Types__c
,Legacy_TT_Customer_Number__c_sup=tar.Legacy_TT_Customer_Number__c
,Legacy_TT_Customer_Number__c=leg.Legacy_TT_Customer_Number__c
,Last_SW_Purchase_Date_ePCR__c_sup=tar.Last_SW_Purchase_Date_ePCR__c
,Last_SW_Purchase_Date_ePCR__c=leg.Last_SW_Purchase_Date_ePCR__c
--,Last_SW_Purchase_Date_FBR__c_sup=tar.Last_SW_Purchase_Date_FBR__c
--,Last_SW_Purchase_Date_FBR__c=leg.Last_SW_Purchase_Date_FBR__c
,ORI__c_sup=tar.ORI__c
,ORI__c=leg.ORI__c
--,Fire_Fighter_Paid_Per_Call__c_sup=tar.Fire_Paid_Per_Call__c
--,Fire_Fighter_Paid_Per_Call__c=leg.Fire_Paid_Per_Call__c
,Last_SW_Purchase_Date_Fire_RMS__c_sup=tar.Last_SW_Purchase_Date_Fire_RMS__c
,Last_SW_Purchase_Date_Fire_RMS__c=leg.Last_SW_Purchase_Date_Fire_RMS__c
,Legacy_TT_Inactive_Contract_Types__c_sup=tar.Legacy_TT_Inactive_Contract_Types__c
,Legacy_TT_Inactive_Contract_Types__c=leg.Legacy_TT_Inactive_Contract_Types__c
,NumberofCallsforService__c_sup=tar.NumberofCallsforService__c
,NumberofCallsforService__c=leg.NumberofCallsforService__c
,BillingCountry_sup=tar.BillingCountry
,BillingCountry=leg.BillingCountry
,Last_SW_Purchase_Date_Mapping__c_sup=tar.Last_SW_Purchase_Date_Mapping__c
,Last_SW_Purchase_Date_Mapping__c=leg.Last_SW_Purchase_Date_Mapping__c
,Last_SW_Purchase_Date_Mobile__c_sup=tar.Last_SW_Purchase_Date_Mobile__c
,Last_SW_Purchase_Date_Mobile__c=leg.Last_SW_Purchase_Date_Mobile__c
,Legacy_TT_Parent_Active_Client_Product_F__c_sup=tar.Legacy_TT_Parent_Active_Client_Product_F__c
,Legacy_TT_Parent_Active_Client_Product_F__c=leg.Legacy_TT_Parent_Active_Client_Product_F__c
,Legacy_TT_Parent_Active_Contract_Types__c_sup=tar.Legacy_TT_Parent_Active_Contract_Types__c
,Legacy_TT_Parent_Active_Contract_Types__c=leg.Legacy_TT_Parent_Active_Contract_Types__c
,Last_SW_Purchase_Date_Law_RMS__c_sup=tar.Last_SW_Purchase_Date_Law_RMS__c
,Last_SW_Purchase_Date_Law_RMS__c=leg.Last_SW_Purchase_Date_Law_RMS__c
,ShippingCountry_sup=tar.ShippingCountry
,ShippingCountry=leg.ShippingCountry
,User_Conference_Attendee__c_sup=tar.User_Conference_Attendee__c
,User_Conference_Attendee__c=leg.User_Conference_Attendee__c
--,Last_SW_Purchase_Date_911__c_sup=tar.Last_SW_Purchase_Date_911__c
--,Last_SW_Purchase_Date_911__c=leg.Last_SW_Purchase_Date_911__c
,Legacy_TT_Data_Conversion_Analyst__c_sup=tar.Legacy_TT_Data_Conversion_Analyst__c
,Legacy_TT_Data_Conversion_Analyst__c=leg.Legacy_TT_Data_Conversion_Analyst__c
,Legacy_TT_GIS_Analyst__c_sup=tar.Legacy_TT_GIS_Analyst__c
,Legacy_TT_GIS_Analyst__c=leg.Legacy_TT_GIS_Analyst__c
--,Install_Name__c_sup=tar.Install_Name__c
--,Install_Name__c=leg.Install_Name__c
,TT_Account_At_Risk__c_sup=tar.TT_Account_At_Risk__c
,TT_Account_At_Risk__c=leg.TT_Account_At_Risk__c
,TT_Account_Status_Comments__c_sup=tar.TT_Account_Status_Comments__c
,TT_Account_Status_Comments__c=leg.TT_Account_Status_Comments__c
,TT_Account_Status_Issues_Problems__c_sup=tar.TT_Account_Status_Issues_Problems__c
,TT_Account_Status_Issues_Problems__c=leg.TT_Account_Status_Issues_Problems__c
,Wellness_Check_Contact_Goal__c_sup=tar.Wellness_Check_Contact_Goal__c
,Wellness_Check_Contact_Goal__c=leg.Wellness_Check_Contact_Goal__c
,Account_Renewal_Confidence__c_sup=tar.Account_Renewal_Confidence__c
,Account_Renewal_Confidence__c=leg.Account_Renewal_Confidence__c
,Account_Renewal_Notes__c_sup=tar.Account_Renewal_Notes__c
,Account_Renewal_Notes__c=leg.Account_Renewal_Notes__c
,Change_in_Leadership__c_sup=tar.Change_in_Leadership__c
,Change_in_Leadership__c=leg.Change_in_Leadership__c
--,Completed_Yearly_Wellness_Checks__c_sup=tar.Completed_Yearly_Wellness_Checks__c
--,Completed_Yearly_Wellness_Checks__c=leg.Completed_Yearly_Wellness_Checks__c
--,Last_Wellness_Check_Date__c_sup=tar.Last_Wellness_Check_Date__c
--,Last_Wellness_Check_Date__c=leg.Last_Wellness_Check_Date__c
--,Last_Wellness_Check_Type__c_sup=tar.Last_Wellness_Check_Type__c
--,Last_Wellness_Check_Type__c=leg.Last_Wellness_Check_Type__c
--,Low_Agency_Ownership_User_Adoption__c_sup=tar.Low_Agency_Ownership_User_Adoption__c
--,Low_Agency_Ownership_User_Adoption__c=leg.Low_Agency_Ownership_User_Adoption__c
--,Maintenance_Lapse__c_sup=tar.Maintenance_Lapse__c
--,Maintenance_Lapse__c=leg.Maintenance_Lapse__c
--,Next_Renewal_Date__c_sup=tar.Next_Renewal_Date__c
--,Next_Renewal_Date__c=leg.Next_Renewal_Date__c
--,No_Communication_From_Customer__c_sup=tar.No_Communication_From_Customer__c
--,No_Communication_From_Customer__c=leg.No_Communication_From_Customer__c
--,Not_a_Full_Suite_Customer__c_sup=tar.Not_a_Full_Suite_Customer__c
--,Not_a_Full_Suite_Customer__c=leg.Not_a_Full_Suite_Customer__c
--,Renewal_Expected_Next_Year__c_sup=tar.Renewal_Expected_Next_Year__c
--,Renewal_Expected_Next_Year__c=leg.Renewal_Expected_Next_Year__c
--,Total_Maintenance_Amount__c_sup=tar.Total_Maintenance_Amount__c
--,Total_Maintenance_Amount__c=leg.Total_Maintenance_Amount__c
,Vocal_with_Issues__c_sup=tar.Vocal_with_Issues__c
,Vocal_with_Issues__c=leg.Vocal_with_Issues__c

,of_firefighters_at_largest_PSAP_agency__c_sup=tar.of_firefighters_at_largest_PSAP_agency__c
,of_firefighters_at_largest_PSAP_agency__c=leg.of_firefighters_at_largest_PSAP_agency__c
,of_Fire_Stations__c_sup=tar.of_Fire_Stations__c
,of_Fire_Stations__c=leg.of_Fire_Stations__c
,of_Jail_Users__c_sup=tar.of_Jail_Users__c
,of_Jail_Users__c=leg.of_Jail_Users__c
,Number_of_Sworn_Personnel_WMP__c_sup=tar.Number_of_Sworn_Personnel_WMP__c
,Number_of_Sworn_Personnel_WMP__c=leg.Number_of_Sworn_Personnel_WMP__c
,X3rd_Party_Type__c_sup=tar.X3rd_Party_Type__c
,X3rd_Party_Type__c=leg.X3rd_Party_Type__c
,X911_Call_Taker_Positions__c_sup=tar.X911_Call_Taker_Positions__c
,X911_Call_Taker_Positions__c=leg.X911_Call_Taker_Positions__c
,X911_Current_SW_Vendor__c_sup=tar.X911_Current_SW_Vendor__c
,X911_Current_SW_Vendor__c=leg.X911_Current_SW_Vendor__c

,X911_Parent_Account__c_sup=tar.X911_Parent_Account__c
,X911_Parent_Account__c_tt_orig=leg.X911_Parent_Account__c_orig
,X911_Parent_Account__c=X911Parent.Id

,X911_Parent_Total_Sworn_Officers__c_sup=tar.X911_Parent_Total_Sworn_Officers__c
,X911_Parent_Total_Sworn_Officers__c=leg.X911_Parent_Total_Sworn_Officers__c
--,Account_Notes__c_sup=tar.Account_Notes__c
--,Account_Notes__c=leg.Account_Notes__c
--,Account_Renewal_Confidence__c_sup=tar.Account_Renewal_Confidence__c
--,Account_Renewal_Confidence__c=leg.Account_Renewal_Confidence__c
--,Account_Renewal_Notes__c_sup=tar.Account_Renewal_Notes__c
--,Account_Renewal_Notes__c=leg.Account_Renewal_Notes__c
,Actions_Taken_Required__c_sup=tar.Actions_Taken_Required__c
,Actions_Taken_Required__c=leg.Actions_Taken_Required__c
,Advertise_Relationship__c_sup=tar.Advertise_Relationship__c
,Advertise_Relationship__c=leg.Advertise_Relationship__c
,Agreement_Effective_Date__c_sup=tar.Agreement_Effective_Date__c
,Agreement_Effective_Date__c=leg.Agreement_Effective_Date__c
,Agreement_Status__c_sup=tar.Agreement_Status__c
,Agreement_Status__c=leg.Agreement_Status__c
,Annual_Calls_for_Service_Received__c_sup=tar.Annual_Calls_for_Service_Received__c
,Annual_Calls_for_Service_Received__c=leg.Annual_Calls_for_Service_Received__c
,Average_Inmate_Daily_Population__c_sup=tar.Average_Inmate_Daily_Population__c
,Average_Inmate_Daily_Population__c=leg.Average_Inmate_Daily_Population__c
,Billing_Current_SW_Vendor__c_sup=tar.Billing_Current_SW_Vendor__c
,Billing_Current_SW_Vendor__c=leg.Billing_Current_SW_Vendor__c
,Bomgar_Approved_Client__c_sup=tar.Bomgar_Approved_Client__c
,Bomgar_Approved_Client__c=leg.Bomgar_Approved_Client__c

--,CurrentCADSWVendor__c_sup=tar.CurrentCADSWVendor__c
--,CurrentCADSWVendor__c_tt=leg.CurrentCADSWVendor__c
--,CurrentCADSWVendor__c=IIF(tar.CurrentCADSWVendor__c is null,leg.CurrentCADSWVendor__c,tar.CurrentCADSWVendor__c)

--,Alarm_Mgmt_Current_SW_Vendor__c_sup=tar.Alarm_Mgmt_Current_SW_Vendor__c
--,Alarm_Mgmt_Current_SW_Vendor__c_tt=leg.Alarm_Mgmt_Current_SW_Vendor__c
--,Alarm_Mgmt_Current_SW_Vendor__c=IIF(tar.Alarm_Mgmt_Current_SW_Vendor__c is null,leg.Alarm_Mgmt_Current_SW_Vendor__c,tar.Alarm_Mgmt_Current_SW_Vendor__c)

,Last_SW_Purchase_Date_CAD__c_sup=tar.Last_SW_Purchase_Date_CAD__c
,Last_SW_Purchase_Date_CAD__c_tt=leg.Last_SW_Purchase_Date_CAD__c
,Last_SW_Purchase_Date_CAD__c=IIF(tar.Last_SW_Purchase_Date_CAD__c is null,leg.Last_SW_Purchase_Date_CAD__c,tar.Last_SW_Purchase_Date_CAD__c)

,CAD_Dispatcher_Call_Taker_Seats__c_sup=tar.CAD_Dispatcher_Call_Taker_Seats__c
,CAD_Dispatcher_Call_Taker_Seats__c=leg.CAD_Dispatcher_Call_Taker_Seats__c
,CAD_Parent_Fire_EMS_Users__c_sup=tar.CAD_Parent_Fire_EMS_Users__c
,CAD_Parent_Fire_EMS_Users__c=leg.CAD_Parent_Fire_EMS_Users__c
,CAD_Parent_Total_Sworn_Officers__c_sup=tar.CAD_Parent_Total_Sworn_Officers__c
,CAD_Parent_Total_Sworn_Officers__c=leg.CAD_Parent_Total_Sworn_Officers__c
--,Change_in_Leadership__c_sup=tar.Change_in_Leadership__c
--,Change_in_Leadership__c=leg.Change_in_Leadership__c
,Completed_Yearly_Wellness_Checks__c_sup=tar.Completed_Yearly_Wellness_Checks__c
,Completed_Yearly_Wellness_Checks__c=leg.Completed_Yearly_Wellness_Checks__c
,ePCR_Current_SW_Vendor__c_sup=tar.ePCR_Current_SW_Vendor__c
,ePCR_Current_SW_Vendor__c=leg.ePCR_Current_SW_Vendor__c
,FBR_Current_SW_Vendor__c_sup= tar.FBR_Current_SW_Vendor__c
,FBR_Current_SW_Vendor__c=leg.FBR_Current_SW_Vendor__c
,Fire_Career__c_sup=tar.Fire_Career__c
,Fire_Career__c=leg.Fire_Career__c
,Fire_Civilian_Personnel__c_sup=tar.Fire_Civilian_Personnel__c
,Fire_Civilian_Personnel__c=leg.Fire_Civilian_Personnel__c
,Fire_Mobile_Units__c_sup=tar.Fire_Mobile_Units__c
,Fire_Mobile_Units__c=leg.Fire_Mobile_Units__c

,Fire_RMS_Current_SW_Vendor__c_sup=tar.Fire_RMS_Current_SW_Vendor__c
,Fire_RMS_Current_SW_Vendor__c=leg.Fire_RMS_Current_SW_Vendor__c

,Fire_RMS_Users__c_sup=tar.Fire_RMS_Users__c
,Fire_RMS_Users__c=leg.Fire_RMS_Users__c
,Fire_Volunteer_Firefighting__c_sup=tar.Fire_Volunteer_Firefighting__c
,Fire_Volunteer_Firefighting__c=leg.Fire_Volunteer_Firefighting__c
,Fire_Volunteer_Non_Firefighting__c_sup=tar.Fire_Volunteer_Non_Firefighting__c
,Fire_Volunteer_Non_Firefighting__c=leg.Fire_Volunteer_Non_Firefighting__c
,Install_Name__c_sup=tar.Install_Name__c
,Install_Name__c=leg.Install_Name__c
,Last_Modified_Actions__c_sup=tar.Last_Modified_Actions__c
,Last_Modified_Actions__c=leg.Last_Modified_Actions__c
,Last_Modified_Comments__c_sup=tar.Last_Modified_Comments__c
,Last_Modified_Comments__c=leg.Last_Modified_Comments__c
,Last_Modified_Issues__c_sup=tar.Last_Modified_Issues__c
,Last_Modified_Issues__c=leg.Last_Modified_Issues__c
,Last_SW_Purchase_Date_911__c_sup=tar.Last_SW_Purchase_Date_911__c
,Last_SW_Purchase_Date_911__c=leg.Last_SW_Purchase_Date_911__c
,Last_SW_Purchase_Date_Billing__c_sup=tar.Last_SW_Purchase_Date_Billing__c
,Last_SW_Purchase_Date_Billing__c=leg.Last_SW_Purchase_Date_Billing__c
--,Last_SW_Purchase_Date_ePCR__c_sup=tar.Last_SW_Purchase_Date_ePCR__c
--,Last_SW_Purchase_Date_ePCR__c=leg.Last_SW_Purchase_Date_ePCR__c
,Last_SW_Purchase_Date_FBR__c_sup=tar.Last_SW_Purchase_Date_FBR__c
,Last_SW_Purchase_Date_FBR__c=leg.Last_SW_Purchase_Date_FBR__c
--,Last_SW_Purchase_Date_Fire_RMS__c_sup=tar.Last_SW_Purchase_Date_Fire_RMS__c
--,Last_SW_Purchase_Date_Fire_RMS__c=leg.Last_SW_Purchase_Date_Fire_RMS__c
--,Last_SW_Purchase_Date_Law_RMS__c_sup=tar.Last_SW_Purchase_Date_Law_RMS__c
--,Last_SW_Purchase_Date_Law_RMS__c=leg.Last_SW_Purchase_Date_Law_RMS__c
--,Last_SW_Purchase_Date_Mapping__c_sup=tar.Last_SW_Purchase_Date_Mapping__c
--,Last_SW_Purchase_Date_Mapping__c=leg.Last_SW_Purchase_Date_Mapping__c
--,Last_SW_Purchase_Date_Mobile__c_sup=tar.Last_SW_Purchase_Date_Mobile__c
--,Last_SW_Purchase_Date_Mobile__c=leg.Last_SW_Purchase_Date_Mobile__c
,Last_Wellness_Check_Date__c_sup=tar.Last_Wellness_Check_Date__c
,Last_Wellness_Check_Date__c=leg.Last_Wellness_Check_Date__c
,Last_Wellness_Check_Type__c_sup=tar.Last_Wellness_Check_Type__c
,Last_Wellness_Check_Type__c=leg.Last_Wellness_Check_Type__c
,Law_Civilian_Personnel__c_sup=tar.Law_Civilian_Personnel__c
,Law_Civilian_Personnel__c=leg.Law_Civilian_Personnel__c
,Law_RMS_Current_SW_Vendor__c_Sup=tar.Law_RMS_Current_SW_Vendor__c
,Law_RMS_Current_SW_Vendor__c=leg.Law_RMS_Current_SW_Vendor__c
,Law_RMS_Users__c_sup=tar.Law_RMS_Users__c
,Law_RMS_Users__c=leg.Law_RMS_Users__c
,Legacy_TT_Active_Contract_Types__c_sup=tar.Legacy_TT_Active_Contract_Types__c
,Legacy_TT_Active_Contract_Types__c=leg.Legacy_TT_Active_Contract_Types__c
--,Legacy_TT_Client_Product_Families__c_sup=tar.Legacy_TT_Client_Product_Families__c
--,Legacy_TT_Client_Product_Families__c=leg.Legacy_TT_Client_Product_Families__c
--,Legacy_TT_Client_Product_Groups__c_sup=tar.Legacy_TT_Client_Product_Groups__c
--,Legacy_TT_Client_Product_Groups__c=leg.Legacy_TT_Client_Product_Groups__c
--,Legacy_TT_Customer_Number__c_sup=tar.Legacy_TT_Customer_Number__c
--,Legacy_TT_Customer_Number__c=leg.Legacy_TT_Customer_Number__c
--,Legacy_TT_Data_Conversion_Analyst__c_sup=tar.Legacy_TT_Data_Conversion_Analyst__c
--,Legacy_TT_Data_Conversion_Analyst__c=leg.Legacy_TT_Data_Conversion_Analyst__c
--,Legacy_TT_GIS_Analyst__c_sup=tar.Legacy_TT_GIS_Analyst__c
--,Legacy_TT_GIS_Analyst__c=leg.Legacy_TT_GIS_Analyst__c
--,Legacy_TT_Inactive_Contract_Types__c_sup=tar.Legacy_TT_Inactive_Contract_Types__c
--,Legacy_TT_Inactive_Contract_Types__c=leg.Legacy_TT_Inactive_Contract_Types__c
--,Legacy_TT_Parent_Active_Client_Product_F__c_sup=tar.Legacy_TT_Parent_Active_Client_Product_F__c
--,Legacy_TT_Parent_Active_Client_Product_F__c=leg.Legacy_TT_Parent_Active_Client_Product_F__c
--,Legacy_TT_Parent_Active_Contract_Types__c_sup=tar.Legacy_TT_Parent_Active_Contract_Types__c
--,Legacy_TT_Parent_Active_Contract_Types__c=leg.Legacy_TT_Parent_Active_Contract_Types__c
,Low_Agency_Ownership_User_Adoption__c_sup=tar.Low_Agency_Ownership_User_Adoption__c
,Low_Agency_Ownership_User_Adoption__c=leg.Low_Agency_Ownership_User_Adoption__c
,Maintenance_Lapse__c_sup=tar.Maintenance_Lapse__c
,Maintenance_Lapse__c=leg.Maintenance_Lapse__c
,Mapping_Current_SW_Vendor__c_sup=tar.Mapping_Current_SW_Vendor__c
,Mapping_Current_SW_Vendor__c=leg.Mapping_Current_SW_Vendor__c
,NDA_Expiration__c_sup=tar.NDA_Expiration__c
,NDA_Expiration__c=leg.NDA_Expiration__c
,NDA_in_Place__c_sup=tar.NDA_in_Place__c
,NDA_in_Place__c=leg.NDA_in_Place__c
,Next_Renewal_Date__c_sup=tar.Next_Renewal_Date__c
,Next_Renewal_Date__c=leg.Next_Renewal_Date__c
,No_Communication_From_Customer__c_sup=tar.No_Communication_From_Customer__c
,No_Communication_From_Customer__c=leg.No_Communication_From_Customer__c
,Not_a_Full_Suite_Customer__c_sup=tar.Not_a_Full_Suite_Customer__c
,Not_a_Full_Suite_Customer__c=leg.Not_a_Full_Suite_Customer__c
--,ORI__C_sup=tar.ORI__c
--,ORI__c=leg.ORI__c
,Parent_Info_Not_Validated__c_sup=tar.Parent_Info_Not_Validated__c
,Parent_Info_Not_Validated__c=leg.Parent_Info_Not_Validated__c
,Reference_List_Use__c_sup=tar.Reference_List_Use__c
,Reference_List_Use__c=leg.Reference_List_Use__c
,Renewal_Expected_Next_Year__c_sup=tar.Renewal_Expected_Next_Year__c
,Renewal_Expected_Next_Year__c=leg.Renewal_Expected_Next_Year__c

,RMS_Parent_Account__c_sup=tar.RMS_Parent_Account__c
,RMS_Parent_Account__c_tt_orig=leg.RMS_Parent_Account__c_orig
,RMS_Parent_Account__c=RMSParent.Id

,RMS_Parent_Total_Sworn_Officers__c_sup=tar.RMS_Parent_Total_Sworn_Officers__c
,RMS_Parent_Total_Sworn_Officers__c=leg.RMS_Parent_Total_Sworn_Officers__c
,Sales_Tax_ID__c_sup=tar.Sales_Tax_ID__c
,Sales_Tax_ID__c=leg.Sales_Tax_ID__c
,Sales_Tax_Rate__c_sup=tar.Sales_Tax_Rate__c
,Sales_Tax_Rate__c=leg.Sales_Tax_Rate__c
,Total_Maintenance_Amount__c_sup=tar.Total_Maintenance_Amount__c
,Total_Maintenance_Amount__c=leg.Total_Maintenance_Amount__c
--,TT_Account_At_Risk__c_sup=tar.TT_Account_At_Risk__c
--,TT_Account_At_Risk__c=leg.TT_Account_At_Risk__c
,TT_Account_Status__c_sup=tar.TT_Account_Status__c
,TT_Account_Status__c=leg.TT_Account_Status__c
--,TT_Account_Status_Comments__c_sup=tar.TT_Account_Status_Comments__c
--,TT_Account_Status_Comments__c=leg.TT_Account_Status_Comments__c
--,TT_Account_Status_Issues_Problems__c_sup=tar.TT_Account_Status_Issues_Problems__c
--,TT_Account_Status_Issues_Problems__c=leg.TT_Account_Status_Issues_Problems__c
,TT_Legacy_Description__c_sup=tar.TT_Legacy_Description__c
,TT_Legacy_Description__c=leg.TT_Legacy_Description__c
--,User_Conference_Attendee__c_sup=tar.User_Conference_Attendee__c
--,User_Conference_Attendee__c=leg.User_Conference_Attendee__c
--,Vocal_with_Issues__c_sup=tar.Vocal_with_Issues__c
--,Vocal_with_Issues__c=leg.Vocal_with_Issues__c
--,Wellness_Check_Contact_Goal__c_sup=tar.Wellness_Check_Contact_Goal__c
--,Wellness_Check_Contact_Goal__c=leg.Wellness_Check_Contact_Goal__c
,ParentId_sup=tar.ParentId
,ParentId=IIF(tar.type='customer' AND tar.ParentId IS NOT NULL,tar.ParentId,'')

,Fire_Paid_Per_Call__c_sup=tar.Fire_Paid_Per_Call__c
,Fire_Paid_Per_Call__c=tt.Fire_Fighter_Paid_Per_Call__c

,Secure_Folder_ID__c_sup=tar.Secure_Folder_ID__c
,Secure_Folder_ID__c_leg=tt.Secure_Folder_ID__c
,Secure_Folder_ID__c=ISNULL(tar.Secure_Folder_ID__c,tt.Secure_Folder_ID__c)

,Sensitive_Account_WMP__C_leg=tt.Sensitive_Account_WMP__c
,Sensitive_Account_WMP__C_sup=tar.SupportNotes__c
,SupportNotes__c=iif(cast(tar.SupportNotes__c as nvarchar(max))=cast(tt.Sensitive_Account_WMP__c as nvarchar(max)),tar.SupportNotes__c,
CONCAT(tar.SupportNotes__c,tt.Sensitive_Account_WMP__c))

--,Installed_PA__c_sup=tar.Installed_PA__c --commented by SL on 2/27 as per newtransformation rule
--,Account_Executive_Install_PSJ__c_sup=tar.Account_Executive_Install_PSJ__c --added by SL on 2/27 as per newtransformation rule
--,Client__c_supe=tar.Client__c
--,OwnerID_legacy=tt.OwnerId
--,Account_Executive_Install_PSJ__c=IIF(tar.Account_Executive_Install_PSJ__c IS NULL AND tar.Client__c='true',ownrname.Name,tar.Account_Executive_Install_PSJ__c) --added by SL on 2/27 as per newtransformation rule
--,Installed_PA__c=IIF(tar.Installed_PA__c IS NULL AND tar.Client__c='true',ownrname.Name,tar.Installed_PA__c) ----commented by SL on 2/27 as per newtransformation rule

--,Account_Executive_Fin__c_sup=tar.Account_Executive_Fin__c----commented by SL on 2/27 as per newtransformation rule
--,Account_Executive_del_del__c_sup=tar.Account_Executive_del_del__c --Added by SL on 2/27 as per newtransformation rule
--,Client__c_superion=tar.Client__c
--,OwnerID_leg=tt.OwnerId
--,Account_Executive_del_del__c=IIF(tar.Account_Executive_del_del__c IS NULL AND tar.Client__c='true',ownrname.Name,tar.Account_Executive_del_del__c)--Added by SL on 2/27 as per newtransformation rule
--,Account_Executive_Fin__c=IIF(tar.Account_Executive_Fin__c IS NULL AND tar.Client__c='true',ownrname.Name,tar.Account_Executive_Fin__c)--commented by SL on 2/27 as per newtransformation rule

,Client__c_sup=tar.Client__c
,Client__c_leg=tt.Client__c
,AAM_PSJ__c_leg=tt.AAM_PSJ__c
,AAM_PSJ__c_sup=tar.AAM_PSJ__c
,Customer_Success_Liaison__c_leg=tt.Customer_Success_Liaison__c
,Customer_Success_Liaison__c_tar=cslname.Name
,AAM_PSJ__c=IIF(tt.Client__c='True',ISNULL(tar.AAM_PSJ__c,ISNULL(tt.AAM_PSJ__c,cslname.Name)),NULL)

,PSJ_Territory__c_sup=tar.PSJ_Territory__c
,PSJ_Territory__c_leg=tt.PSJ_Territory__c
,PSJ_Territory__c=IIF(tt.Client__c='True',ISNULL(tar.PSJ_Territory__c,tt.PSJ_Territory__c),NULL)


,Account_Executive_Install_PSJ__c_sup = tar.Account_Executive_Install_PSJ__c 
,Account_Executive_Install_PSJ__c_leg = tt.AM_PSJ__c
,Account_Executive_Install_PSJ__c=IIF(tt.Client__c='True',ISNULL(tar.Account_Executive_Install_PSJ__c,tt.AM_PSJ__c),NULL)

,TTOwnerIsActive=ownrname.IsActive
,Account_Executive_del_del__c_sup=tar.Account_Executive_del_del__c  
,OwnerID_leg=tt.OwnerId
,Account_Executive_del_del__c= IIF(tt.Client__c='False',IIF(ownrname.IsActive='True',ownrname.[Name],'OPEN'),NULL)

,IQ_Account_Owner__c_leg=tt.IQ_Account_Owner__c
,AE_IQ__c=iqname.[Name]

,X911_Account_Owner__c_leg=tt.X911_Account_Owner__c
,AE_911__c=x911name.[Name]

,Type_sup=tar.Type
,Type=IIF(leg.Client_c_orig='True','Customer',tar.Type)

into Account_Tritech_SFDC_UpdateMergingAccounts_Delta

 from Staging_PROD.dbo.Account_Tritech_SFDC_Preload_delta leg

 left join SUPERION_PRODUCTION.dbo.Account tar
 on tar.Id=leg.MergeSuperionId

 left join Tritech_PROD.dbo.Account tt
 on tt.id=leg.MergeTritechId

 left join SUPERION_PRODUCTION.dbo.Account RMSParent
 on leg.RMS_Parent_Account__c_orig=RMSParent.LegacySFDCAccountId__c

 left join SUPERION_PRODUCTION.dbo.Account X911Parent
 on leg.X911_Parent_Account__c_orig=X911Parent.LegacySFDCAccountId__c

 --left join Superion_FULLSB.dbo.Account prnt
 --on prnt.LegacySFDCAccountId__c=leg.ParentId_orig

 left join Tritech_PROD.dbo.[User] cslname 
on cslname.Id=tt.Customer_Success_Liaison__c
left join Tritech_PROD.dbo.[User] ownrname
on ownrname.Id=tt.OwnerId
left join Tritech_PROD.dbo.[User] iqname 
on iqname.Id=tt.IQ_Account_Owner__c
left join Tritech_PROD.dbo.[User] x911name 
on x911name.Id=tt.X911_Account_Owner__c

left join tt_prod_sys_sfpicklists RMSpcklst
on RMSpcklst.objectname='Account' and RMSpcklst.Fieldname='Planning_to_replace_RMS_provider__c'
and RMSpcklst.PickListValue=tar.Planning_to_replace_RMS_provider__c

left join tt_prod_sys_sfpicklists [911pcklst]
on [911pcklst].objectname='Account' and [911pcklst].Fieldname='Planning_to_replace_911_provider__c'
and [911pcklst].PickListValue=tar.Planning_to_replace_911_provider__c

left join tt_prod_sys_sfpicklists CADpcklst
on CADpcklst.objectname='Account' and CADpcklst.Fieldname='Planning_to_replace_CAD_provider__c'
and CADpcklst.PickListValue=tar.Planning_to_replace_CAD_provider__c

left join tt_prod_sys_sfpicklists JAILpcklst
on JAILpcklst.objectname='Account' and JAILpcklst.Fieldname='Planning_to_replace_Jail_provider__c'
and JAILpcklst.PickListValue=tar.Planning_to_replace_Jail_provider__c

 where leg.MergeFlag='Yes';--(314 row(s) affected)


 select count(*) from Account_Tritech_SFDC_UpdateMergingAccounts_Delta;--314

 select e.PS_Agency_Type__c,act_sp.PS_Agency_Type__c, * 
--update act_sp set act_sp.PS_Agency_Type__c=e.PS_Agency_Type__c
from Account_Tritech_SFDC_UpdateMergingAccounts_Delta  AS act_sp 
CROSS APPLY
 ( SELECT STUFF( ( SELECT TOP 50 ';' + b2.data 
    FROM (
  select distinct b.data,a.id
   from Account_Tritech_SFDC_UpdateMergingAccounts_Delta a
  cross apply dbo.SF_Split(PS_Agency_Type__c,';',0) b
  where a.id=act_sp.id
  ) as b2
  FOR XML PATH('') ), 1, 1, '') As PS_Agency_Type__c
   ) AS e ;--314

   select SupportNotes__c,Sensitive_Account_WMP__C_leg,Sensitive_Account_WMP__c_tar, * from Account_Tritech_SFDC_UpdateMergingAccounts_Delta;
 

 --Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Account_Tritech_SFDC_UpdateMergingAccounts_Delta' 

/*
Salesforce object Account does not contain column LegacySFDCAccountId__c_Sup
Salesforce object Account does not contain column Entity_Name__c_sup
Salesforce object Account does not contain column Entity_Name__c_tt
Salesforce object Account does not contain column PSAP_Type__c_sup
Salesforce object Account does not contain column PSAP__c_sup
Salesforce object Account does not contain column Sales_Team__c_tt
Salesforce object Account does not contain column CurrentCADSWVendor__c_tt
Salesforce object Account does not contain column CurrentCADSWVendor__c_sup
Salesforce object Account does not contain column Client__c_superion
Salesforce object Account does not contain column RecordTypeId_sup
Salesforce object Account does not contain column County_c_sup
Salesforce object Account does not contain column Ship_To_County__c_sup
Salesforce object Account does not contain column CreatedDate_sup
Salesforce object Account does not contain column CreatedDate_tt
Salesforce object Account does not contain column Fax_sup
Salesforce object Account does not contain column NumberofBeds__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_JMS__c_sup
Salesforce object Account does not contain column Current_Justice_SW_Vendor__c_sup
Salesforce object Account does not contain column NumberofMobileUnits__c_sup
Salesforce object Account does not contain column Mailing_Billing_City_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_State_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_Street_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_Zip_Postal_Code_WMP__c_btst
Salesforce object Account does not contain column Mailing_Billing_Country_WMP__c_btst
Salesforce object Account does not contain column Current_Mobile_SW_Vendor__c_sup
Salesforce object Account does not contain column Name_sup
Salesforce object Account does not contain column NumberOfEmployees_sup
Salesforce object Account does not contain column OwnerId_sup
Salesforce object Account does not contain column ParentId_orig
Salesforce object Account does not contain column Planning_to_replace_911_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_911_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_911_providerTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_911_providerTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__cTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_CAD_provider__cTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__cTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_Jail_provider__cTTPickListLabel
Salesforce object Account does not contain column Planning_to_replace_RMS_provider__c_sup
Salesforce object Account does not contain column Planning_to_replace_RMS_provider__c_tt
Salesforce object Account does not contain column Planning_to_replace_RMS_providerTTAPIValue
Salesforce object Account does not contain column Planning_to_replace_RMS_providerTTPickListLabel
Salesforce object Account does not contain column Phone_sup
Salesforce object Account does not contain column Population__c_leg
Salesforce object Account does not contain column Population__c_sup
Salesforce object Account does not contain column Sensitive_Account_WMP__c_legacy
Salesforce object Account does not contain column Sensitive_Account_WMP__c_tar
Salesforce object Account does not contain column PS_Agency_Type__c_sup
Salesforce object Account does not contain column PS_Agency_Type__c_tt
Salesforce object Account does not contain column Shipping_City_WMP__c_btst
Salesforce object Account does not contain column Shipping_State_WMP__c_btst
Salesforce object Account does not contain column Shipping_Street_WMP__c_btst
Salesforce object Account does not contain column Shipping_Zip_Postal_Code__c_btst
Salesforce object Account does not contain column Time_Zone__c_sup
Salesforce object Account does not contain column Time_Zone__c_tt
Salesforce object Account does not contain column Website_sup
Salesforce object Account does not contain column Website_tt
Salesforce object Account does not contain column AccountSource_sup
Salesforce object Account does not contain column AccountSource_tt
Salesforce object Account does not contain column Legacy_TT_Client_Product_Families__c_sup
Salesforce object Account does not contain column Legacy_TT_Client_Product_Groups__c_sup
Salesforce object Account does not contain column Legacy_TT_Customer_Number__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_ePCR__c_sup
Salesforce object Account does not contain column ORI__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Fire_RMS__c_sup
Salesforce object Account does not contain column Legacy_TT_Inactive_Contract_Types__c_sup
Salesforce object Account does not contain column NumberofCallsforService__c_sup
Salesforce object Account does not contain column BillingCountry_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Mapping__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Mobile__c_sup
Salesforce object Account does not contain column Legacy_TT_Parent_Active_Client_Product_F__c_sup
Salesforce object Account does not contain column Legacy_TT_Parent_Active_Contract_Types__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Law_RMS__c_sup
Salesforce object Account does not contain column ShippingCountry_sup
Salesforce object Account does not contain column User_Conference_Attendee__c_sup
Salesforce object Account does not contain column Legacy_TT_Data_Conversion_Analyst__c_sup
Salesforce object Account does not contain column Legacy_TT_GIS_Analyst__c_sup
Salesforce object Account does not contain column TT_Account_At_Risk__c_sup
Salesforce object Account does not contain column TT_Account_Status_Comments__c_sup
Salesforce object Account does not contain column TT_Account_Status_Issues_Problems__c_sup
Salesforce object Account does not contain column Wellness_Check_Contact_Goal__c_sup
Salesforce object Account does not contain column Account_Renewal_Confidence__c_sup
Salesforce object Account does not contain column Account_Renewal_Notes__c_sup
Salesforce object Account does not contain column Change_in_Leadership__c_sup
Salesforce object Account does not contain column Vocal_with_Issues__c_sup
Salesforce object Account does not contain column of_firefighters_at_largest_PSAP_agency__c_sup
Salesforce object Account does not contain column of_Fire_Stations__c_sup
Salesforce object Account does not contain column of_Jail_Users__c_sup
Salesforce object Account does not contain column Number_of_Sworn_Personnel_WMP__c_sup
Salesforce object Account does not contain column X3rd_Party_Type__c_sup
Salesforce object Account does not contain column X911_Call_Taker_Positions__c_sup
Salesforce object Account does not contain column X911_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column X911_Parent_Account__c_sup
Salesforce object Account does not contain column X911_Parent_Account__c_tt_orig
Salesforce object Account does not contain column X911_Parent_Total_Sworn_Officers__c_sup
Salesforce object Account does not contain column Actions_Taken_Required__c_sup
Salesforce object Account does not contain column Advertise_Relationship__c_sup
Salesforce object Account does not contain column Agreement_Effective_Date__c_sup
Salesforce object Account does not contain column Agreement_Status__c_sup
Salesforce object Account does not contain column Annual_Calls_for_Service_Received__c_sup
Salesforce object Account does not contain column Average_Inmate_Daily_Population__c_sup
Salesforce object Account does not contain column Billing_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Bomgar_Approved_Client__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_CAD__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_CAD__c_tt
Salesforce object Account does not contain column CAD_Dispatcher_Call_Taker_Seats__c_sup
Salesforce object Account does not contain column CAD_Parent_Fire_EMS_Users__c_sup
Salesforce object Account does not contain column CAD_Parent_Total_Sworn_Officers__c_sup
Salesforce object Account does not contain column Completed_Yearly_Wellness_Checks__c_sup
Salesforce object Account does not contain column ePCR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column FBR_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Fire_Career__c_sup
Salesforce object Account does not contain column Fire_Civilian_Personnel__c_sup
Salesforce object Account does not contain column Fire_Mobile_Units__c_sup
Salesforce object Account does not contain column Fire_RMS_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column Fire_RMS_Users__c_sup
Salesforce object Account does not contain column Fire_Volunteer_Firefighting__c_sup
Salesforce object Account does not contain column Fire_Volunteer_Non_Firefighting__c_sup
Salesforce object Account does not contain column Install_Name__c_sup
Salesforce object Account does not contain column Last_Modified_Actions__c_sup
Salesforce object Account does not contain column Last_Modified_Comments__c_sup
Salesforce object Account does not contain column Last_Modified_Issues__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_911__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_Billing__c_sup
Salesforce object Account does not contain column Last_SW_Purchase_Date_FBR__c_sup
Salesforce object Account does not contain column Last_Wellness_Check_Date__c_sup
Salesforce object Account does not contain column Last_Wellness_Check_Type__c_sup
Salesforce object Account does not contain column Law_Civilian_Personnel__c_sup
Salesforce object Account does not contain column Law_RMS_Current_SW_Vendor__c_Sup
Salesforce object Account does not contain column Law_RMS_Users__c_sup
Salesforce object Account does not contain column Legacy_TT_Active_Contract_Types__c_sup
Salesforce object Account does not contain column Low_Agency_Ownership_User_Adoption__c_sup
Salesforce object Account does not contain column Maintenance_Lapse__c_sup
Salesforce object Account does not contain column Mapping_Current_SW_Vendor__c_sup
Salesforce object Account does not contain column NDA_Expiration__c_sup
Salesforce object Account does not contain column NDA_in_Place__c_sup
Salesforce object Account does not contain column Next_Renewal_Date__c_sup
Salesforce object Account does not contain column No_Communication_From_Customer__c_sup
Salesforce object Account does not contain column Not_a_Full_Suite_Customer__c_sup
Salesforce object Account does not contain column Parent_Info_Not_Validated__c_sup
Salesforce object Account does not contain column Reference_List_Use__c_sup
Salesforce object Account does not contain column Renewal_Expected_Next_Year__c_sup
Salesforce object Account does not contain column RMS_Parent_Account__c_sup
Salesforce object Account does not contain column RMS_Parent_Account__c_tt_orig
Salesforce object Account does not contain column RMS_Parent_Total_Sworn_Officers__c_sup
Salesforce object Account does not contain column Sales_Tax_ID__c_sup
Salesforce object Account does not contain column Sales_Tax_Rate__c_sup
Salesforce object Account does not contain column Total_Maintenance_Amount__c_sup
Salesforce object Account does not contain column TT_Account_Status__c_sup
Salesforce object Account does not contain column TT_Legacy_Description__c_sup
Salesforce object Account does not contain column ParentId_sup
Salesforce object Account does not contain column Fire_Paid_Per_Call__c_sup
Salesforce object Account does not contain column Secure_Folder_ID__c_sup
Salesforce object Account does not contain column Secure_Folder_ID__c_leg
Salesforce object Account does not contain column Sensitive_Account_WMP__C_leg
Salesforce object Account does not contain column Sensitive_Account_WMP__C_sup
Salesforce object Account does not contain column Client__c_sup
Salesforce object Account does not contain column Client__c_leg
Salesforce object Account does not contain column AAM_PSJ__c_leg
Salesforce object Account does not contain column AAM_PSJ__c_sup
Salesforce object Account does not contain column Customer_Success_Liaison__c_leg
Salesforce object Account does not contain column Customer_Success_Liaison__c_tar
Salesforce object Account does not contain column PSJ_Territory__c_sup
Salesforce object Account does not contain column PSJ_Territory__c_leg
Salesforce object Account does not contain column Account_Executive_Install_PSJ__c_sup
Salesforce object Account does not contain column Account_Executive_Install_PSJ__c_leg
Salesforce object Account does not contain column TTOwnerIsActive
Salesforce object Account does not contain column Account_Executive_del_del__c_sup
Salesforce object Account does not contain column OwnerID_leg
Salesforce object Account does not contain column IQ_Account_Owner__c_leg
Salesforce object Account does not contain column X911_Account_Owner__c_leg
Salesforce object Account does not contain column Type_sup
*/

Select * from Account_Tritech_SFDC_UpdateMergingAccounts_Delta;

--Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_PROD','Account_Tritech_SFDC_UpdateMergingAccounts_Delta'

--drop table Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors
select * 
into Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors
from Account_Tritech_SFDC_UpdateMergingAccounts_Delta
where error<>'Operation Successful.';--81

--drop table Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors_bkp
select * 
into Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors_bkp
from Account_Tritech_SFDC_UpdateMergingAccounts_Delta
where error<>'Operation Successful.';--81
----------------

Select  recordtypeid, aam_psj__C,error,*
--update a set aam_psj__C=null
from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors a
where  aam_psj__C in (select b.aam_psj__C from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors b
 where b.error like '%bad value for restricted picklist field%'+b.aam_psj__C)--3

 

 Select  recordtypeid, AE_IQ__c,error,*
--update a set AE_IQ__c=null
from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors a
where  AE_IQ__c in (select b.AE_IQ__c from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors b
 where b.error like '%bad value for restricted picklist field%'+b.AE_IQ__c) --3

  Select  recordtypeid, AE_911__c,error,*
--update a set AE_911__c=null
from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors a
where  AE_911__c in (select b.AE_911__c from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors b
 where b.error like '%bad value for restricted picklist field%'+b.AE_911__c) --76
 -------------------
Select count(*) from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors;--81

--Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_PROD','Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors' --(14:42)

--delete 
from Account_Tritech_SFDC_UpdateMergingAccounts_Delta
where error<>'Operation Successful.'; --81

--insert into Account_Tritech_SFDC_UpdateMergingAccounts_Delta
select * from Account_Tritech_SFDC_UpdateMergingAccounts_Delta_errors;--81

-----------------------------------
Select error,count(*) from Account_Tritech_SFDC_UpdateMergingAccounts_Delta
group by error;

----------------------------------------------------------------------------------------------------------

 --Updating Ids for fields having lookup relationship on same object.(ParentId)

--Drop table Account_Tritech_SFDC_UpdateParentId_delta;

Use SUPERION_PRODUCTION
--EXEC SF_Refresh 'SL_SUPERION_PROD','Account','Yes'

Use Staging_PROD

Select 

Id=b.Id
,Error=CAST(SPACE(255) as NVARCHAR(255))
,ParentId_orig=b.ParentId_orig
,ParentId=a.Id
,Legacy_id__c_orig=b.LegacySFDCAccountId__c
,parentid_sup=c.parentid

into Account_Tritech_SFDC_UpdateParentId_Delta

from SUPERION_PRODUCTION.dbo.Account a
,(Select Id,LegacySFDCAccountId__c,ParentId_orig from Staging_PROD.dbo.Account_Tritech_SFDC_insert_Delta
UNION
Select Id,LegacySFDCAccountId__c,ParentId_orig from Staging_PROD.dbo.Account_Tritech_SFDC_update_Delta
UNION
Select Id,LegacySFDCAccountId__c,ParentId_orig from Account_Tritech_SFDC_UpdateMergingAccounts_Delta
where ParentId is Null
) b
,Superion_Production.dbo.Account c
where a.LegacySFDCAccountId__c=b.ParentId_orig
and b.ParentId_orig IS NOT NULL
and b.LegacySFDCAccountId__c=c.LegacySFDCAccountId__c
and isnull(c.parentid,'X')<> a.id
;--14

Select * from Account_Tritech_SFDC_UpdateParentId_Delta;

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Account_Tritech_SFDC_UpdateParentId_Delta' 

/*
Salesforce object Account does not contain column ParentId_orig
Salesforce object Account does not contain column Legacy_id__c_orig
Salesforce object Account does not contain column parentid_sup
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','Account_Tritech_SFDC_UpdateParentId_Delta'

Select error,count(*) from Account_Tritech_SFDC_UpdateParentId_Delta
group by error;
------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--Updating Ids for fields having lookup relationship on same object.(RMS_Parent_Account__c)

--Drop table Account_Tritech_SFDC_UpdateRMS_ParentAccount_Delta;

Select 

 Id=b.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))
,RMS_Parent_Account__c_orig=b.RMS_Parent_Account__c_orig
,RMS_Parent_Account__c=a.Id
,Legacy_id__c_orig=b.LegacySFDCAccountId__c

into Account_Tritech_SFDC_UpdateRMS_ParentAccount_Delta

from SUPERION_PRODUCTION.dbo.Account a,(Select Error,RMS_Parent_Account__c_orig,LegacySFDCAccountId__c,Id from Staging_PROD.dbo.Account_Tritech_SFDC_Insert_Delta
UNION Select Error,RMS_Parent_Account__c_orig,LegacySFDCAccountId__c,Id from Staging_PROD.dbo.Account_Tritech_SFDC_Update_Delta) b
where a.LegacySFDCAccountId__c=b.RMS_Parent_Account__c_orig
and b.RMS_Parent_Account__c_orig IS NOT NULL and b.Error='Operation Successful.';--150

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Account_Tritech_SFDC_UpdateRMS_ParentAccount_Delta' 

/*
Salesforce object Account does not contain column RMS_Parent_Account__c_orig
Salesforce object Account does not contain column Legacy_id__c_orig
*/

Select * from Account_Tritech_SFDC_UpdateRMS_ParentAccount_Delta;

--Exec SF_BulkOps 'Update:batchsize(10)','SL_SUPERION_PROD','Account_Tritech_SFDC_UpdateRMS_ParentAccount_Delta'

Select error,count(*) from Staging_PROD.dbo.Account_Tritech_SFDC_UpdateRMS_ParentAccount_Delta
group by error;
------------------------------------------------------------------------------------------------------

--Updating Ids for fields having lookup relationship on same object.(X911_Parent_Account__c)

--Drop table Account_Tritech_SFDC_UpdateX911ParentAccount_Delta;

Select 

 Id=b.Id 
,Error=CAST(SPACE(255) as NVARCHAR(255))
,X911_Parent_Account__c_orig=b.X911_Parent_Account__c_orig
,X911_Parent_Account__c=a.Id
,Legacy_id__c_orig=b.LegacySFDCAccountId__c

into Account_Tritech_SFDC_UpdateX911ParentAccount_Delta

from SUPERION_PRODUCTION.dbo.Account a,(Select X911_Parent_Account__c_orig,Error,LegacySFDCAccountId__c,Id from Staging_PROD.dbo.Account_Tritech_SFDC_Insert_Delta
UNION Select X911_Parent_Account__c_orig,Error,LegacySFDCAccountId__c,Id from Staging_PROD.dbo.Account_Tritech_SFDC_Update_Delta) b
where a.LegacySFDCAccountId__c=b.X911_Parent_Account__c_orig
and b.X911_Parent_Account__c_orig IS NOT NULL and b.Error='Operation Successful.';--30

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'Account_Tritech_SFDC_UpdateX911ParentAccount_Delta' 

/*
Salesforce object Account does not contain column ParentId_orig
Salesforce object Account does not contain column Legacy_id__c_orig
*/

Select * from Account_Tritech_SFDC_UpdateX911ParentAccount_Delta;--30

--Exec SF_BulkOps 'Update:batchsize(1)','SL_SUPERION_PROD','Account_Tritech_SFDC_UpdateX911ParentAccount_Delta'

Select error,count(*) from Account_Tritech_SFDC_UpdateX911ParentAccount_Delta
group by error;
--------------------------------------------------------------------------------------------------