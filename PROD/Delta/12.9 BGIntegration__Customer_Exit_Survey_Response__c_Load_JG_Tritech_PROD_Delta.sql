/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Customer_Exit_Survey_Response__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','BGIntegration__Customer_Exit_Survey_Response__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Customer_Exit_Survey_Response__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload_Delta;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__BomgarSession__c_orig  =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c  =  bs.Id

,BGIntegration__GS_Number__c      =  bg.BGIntegration__GS_Number__c
,BGIntegration__Value__c          =  bg.BGIntegration__Value__c

,CreatedById_orig                 =  bg.CreatedById
,CreatedById                      =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                      =  bg.CreatedDate
--,Legacy_ID__c                     =  Id
,Name                             =  bg.[Name]
,TargetId                                  =  trgt.ID
,LastModifiedDate_orig                     =  bg.LastModifiedDate

into BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

----For Delta Load
left join Superion_PRODUCTION.dbo.BGIntegration__Customer_Exit_Survey_Response__c trgt
on trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(28404 row(s) affected)

--------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c;--28404

Select count(*) from BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload_Delta;--28404

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Insert_Delta;

Select * into 
BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Insert_Delta
from BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload_Delta
where TargetId IS NULL; --(232 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Insert_Delta ;


--Exec SF_ColCompare_test 'Insert','SL_SUPERION_PROD', 'BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object BGIntegration__Customer_Exit_Survey_Response__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Customer_Exit_Survey_Response__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Customer_Exit_Survey_Response__c does not contain column TargetId
Salesforce object BGIntegration__Customer_Exit_Survey_Response__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Insert_Delta' --(1:21)

----------------------------------------------------------------------------------------
 
 
--Drop table Staging_PROD.dbo.BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Update_Delta;

Select * into 
BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Update_Delta
from BGIntegration__Customer_Exit_Survey_Response__c_Tritech_SFDC_Preload_Delta
where TargetId IS NOT NULL AND LastModifiedDate_orig>='25-MAR-2019'; --(0 row(s) affected)
