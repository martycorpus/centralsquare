/*
Use superion_production

EXEC SF_Refresh 'sl_superion_prod ','Contact','yes'
EXEC SF_Refresh 'sl_superion_prod ','Account','yes'
EXEC SF_Refresh 'sl_superion_prod ','Event','yes'
EXEC SF_Refresh 'sl_superion_prod ','case','yes'




Use Tritech_PROD
EXEC SF_Refresh 'EN_TRITECH_PROD','Event','Yes'
select count(*) from tritech_prod.dbo.[Event]--46249
*/

*/


-- drop table Event_Tritech_SFDC_Preload_VIP_Account
use staging_prod 
go

 declare @defaultuser nvarchar(18)
 =(select  id from superion_production.dbo.[user] where name = 'CentralSquare API')
;With CteWhatData as
(
select
a.Legacy_ID__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Case' as What_parent_object_name 
from superion_production.dbo.[case] a
inner join Case_tritech_SFDC_load_VIP_Account b 
on a.id=b.id 
where 1=1
and a.Legacy_ID__c is not null and a.migrated_record__C='true'
						) 
,CteWhoData as
(
select 
a.Legacy_id__c as Legacy_id_orig, 
a.Legacy_Source_System__c Legacy_Source_System,
a.Id Parent_id,
'Contact' as Who_parent_object_name 
from superion_production.dbo.Contact a
where 1=1 
and a.Legacy_ID__c is not null	 and migrated_record__C='true'			
 )   

SELECT  
				ID										 =   CAST(SPACE(18) as NVARCHAR(18))
				,ERROR								     =   CAST(SPACE(255) as NVARCHAR(255))
				,Legacy_id__c                            = tr_event.Id
				,Legacy_Source__c                        ='Tritech'
				,Migrated_Record__c                      ='True'
														  --,tr_event.[Account_Name__c]
														  --,tr_event.[Account_Name_DE__c]
				,AccountID_orig						    = tr_event.[AccountId]
				,AccountId								= iif(Tar_Account.Id is null or tr_event.[AccountId] is null, null,Tar_ACcount.ID)
				,Activity_Type_For_Reporting__c			= tr_event.[Activity_Type_For_Reporting__c]
														  ,ActivityDate = tr_event.[ActivityDate]
														  ,ActivityDateTime = tr_event.[ActivityDateTime]
														--  ,tr_event.[Brand__c]
				--,Description							 = concat( tr_event.createddate ,': ',tr_event.[Comments_Summary__c])
														  --,tr_event.[Completed_Date_Time__c]
														  --,tr_event.[Contact_Count__c]
				,CreatedbyId_orig						= tr_event.[CreatedById]
				,CreatedbyId_target							= Target_Create.id
				,CreatedById							= iif(Target_Create.id is null, @defaultuser
																		--iif(legacyuser.isActive='False',@defaultuser,'CreatedbyId not Found')
																		,Target_Create.id )
				,CreatedDate							= tr_event.[CreatedDate]
														 -- ,tr_event.[Customer_Risk_Level__c]
				--,Description							= tr_event.[Description]
														 -- ,tr_event.[DummyTouchField__c]
				,DurationInMinutes						= tr_event.[DurationInMinutes]
				,EndDateTime							= tr_event.[EndDateTime]
				,EventSubtype							= tr_event.[EventSubtype]
														  --,tr_event.[GroupEventType]
				,ID_orig								= tr_event.[Id]
				,IsAllDayEvent							= tr_event.[IsAllDayEvent]
				,IsArchived_orig				     =tr_event.[IsArchived]
														  --,tr_event.[IsChild]
				,IsDeleted_orig=						  tr_event.[IsDeleted]
														  --,tr_event.[IsGroupEvent]
				,IsPrivate								= tr_event.[IsPrivate]
				,IsRecurrence							= tr_event.[IsRecurrence]
				,IsReminderSet							= tr_event.[IsReminderSet]
														  --,tr_event.[LastModifiedById]
														  --,tr_event.[LastModifiedDate]
				,Location								= tr_event.[Location]
				,OwnerId_orig							= tr_event.[OwnerId]
				,ownerId_target								= Target_owner.ID
				,OwnerId							    = iif(Target_owner.id is null,@defaultuser
																		--iif(legacyuser1.isActive='False',@defaultuser,'OwnerId not found')
																		,Target_owner.id)
				,RecurrenceActivityID_orig				= tr_event.[RecurrenceActivityId]
				,RecurrenceDayOfMonth					= tr_event.[RecurrenceDayOfMonth]
				,RecurrenceDayOfWeekMask				= tr_event.[RecurrenceDayOfWeekMask]
				,RecurrenceEndDateOnly					= tr_event.[RecurrenceEndDateOnly]
				,RecurrenceInstance						= tr_event.[RecurrenceInstance]
				,RecurrenceInterval						= tr_event.[RecurrenceInterval]
				,RecurrenceMonthOfYear					= tr_event.[RecurrenceMonthOfYear]
				,RecurrenceStartDateTime				= tr_event.[RecurrenceStartDateTime]
				,RecurrenceTimeZoneSidKey				= tr_event.[RecurrenceTimeZoneSidKey]
				,RecurrenceType							= tr_event.[RecurrenceType]
				,ReminderDateTime						= tr_event.[ReminderDateTime]
				,Resolution_Notes__c					= tr_event.[Resolution_Notes__c]
				,ShowAs									= tr_event.[ShowAs]
														  --,tr_event.[Solution_s__c]
				,StartDateTime							= tr_event.[StartDateTime]
				,Subject								= tr_event.[Subject]
														  --,tr_event.[SystemModstamp]
				--,Description							= tr_event.[Task_Notes__c]
														 -- ,tr_event.[Time_Elapsed__c]
				,Type									= tr_event.[Type]
														  --,tr_event.[WhatCount]
				,whatID_orig							= tr_event.[WhatId]
				,whatid_parent_object_name				= wt.What_parent_object_name
				,whatid_legacy_id_orig					= wt.Legacy_id_orig
				,whatId									= wt.Parent_id
														  --,tr_event.[WhoCount]
				,whoid_orig								= tr_event.[WhoId]
				,whoId_parent_object_name				= wo.Who_parent_object_name
				,whoId_legacy_id_orig					= wo.Legacy_id_orig
				,whoId									= wo.Parent_id
				,Logged_Wellness_Check__c				= tr_event.[Z_Logged_Client_Relations_Contact__c]
				,Description__orig = tr_event.[Description]
				,Description_Comments_summary__C_orig = tr_event.[Comments_Summary__c]
				,Description_Task_notes__C_orig = tr_event.[Task_Notes__c]
				--,[Description]  = concat( iif(tr_event.[Description] is not null,'Description::',''),tr_event.description,CHAR(13)+CHAR(10),
				--						  iif(tr_event.[Comments_Summary__c] is not null,concat('Comments Summary:: ',tr_event.createddate),''),tr_event.[Comments_Summary__c],CHAR(13)+CHAR(10),
				--						  iif(tr_event.[Task_Notes__c] is not null,concat('Task Notes:: ',tr_event.createddate),''),tr_event.[Task_Notes__c]
				--						  )
				 ,DESCRIPTION = IIF(tr_event.Description IS NOT NULL
											,IIF(tr_event.tASK_nOTES__c IS NOT NULL, 
												CONCAT(tr_event.DESCRIPTION,CHAR(13)+char(10),tr_event.CREATEDDATE,' ',tr_event.task_notes__c)
												,tr_event.description),null)
												,superion_Eventid= e.id
												,superion_legacyid=e.legacy_id__c
				    into Event_Tritech_SFDC_Preload_VIP_Account
  FROM [Tritech_PROD].[dbo].[Event] tr_event 
  LEFT JOIN CteWhatData AS Wt
  ON Wt.Legacy_id_orig = tr_event.WhatId
  LEFT JOIN CteWhoData AS Wo 
  ON Wo.Legacy_id_orig = tr_event.WhoId
  ------------------------------------------------------------------------------------------------------------------------
  left join superion_production.dbo.Account Tar_Account
  on Tar_Account.LegacySFDCAccountId__c=Tr_event.AccountId
--and Tar_Account.Legacy_Source_System__c='Tritech'
  ------------------------------------------------------------------------------------------------------------------------
  left join superion_production.dbo.[User] Target_Create
  on Target_Create.Legacy_Tritech_Id__c=tr_event.CreatedById
  and Target_Create.Legacy_Source_System__c='Tritech'
  --------------------------------------------------------------------------------------------------------------------------
  left join superion_production.dbo.[User] Target_Owner
  on Target_Owner.Legacy_Tritech_Id__c=tr_event.OwnerId
  and Target_Owner.Legacy_Source_System__c='Tritech'
  ----------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser
  on Legacyuser.Id = tr_event.createdbyId
  ------------------------------------------------------------------------------------------------------------------------------
   left join Tritech_Prod.dbo.[user] Legacyuser1
  on Legacyuser1.Id = tr_event.OwnerId
  -----------------------------------------------------------------------------------------------------------------------------
  left join superion_production.dbo.Event e 
  on e.legacy_id__c= tr_event.id
  where 1=1
  and Tr_event.IsDeleted='false'
  and (Wt.parent_id IS NOT NULL )
  and (Tr_event.IsArchived='false' or Tr_event.WhatId like '500%')
  
  ;

  --(1 row(s) affected)


  -- COUNT OF THE Event RECORDS
select *
from Event_Tritech_SFDC_Preload_VIP_Account 

--1

-- CHECK FOR THE DUPLICATES.
select Legacy_ID__c,Count(*) from Event_Tritech_SFDC_Preload_VIP_Account
group by Legacy_ID__c
having count(*)>1


 
--(1 row(s) affected)


--drop table Event_Tritech_SFDC_load_VIP_Account_VIP_Account
select * 
into Event_Tritech_SFDC_load_VIP_Account
from Event_Tritech_SFDC_Preload_VIP_Account a 
where 1=1 and superion_eventid is null
 and (whoid is not null or whatid is not null) 
 --(1 row(s) affected)


-- LOAD TABLE RECORD COUNT
select count(*) 
from Event_Tritech_SFDC_load_VIP_Account
--1

-- check for the duplicates in the final load table.
select Legacy_ID__c,Count(*) from Event_Tritech_SFDC_load_VIP_Account
group by Legacy_ID__c
having count(*)>1


select * from Event_Tritech_SFDC_load_VIP_Account where whatid like '500%'_VIP_Account;

--: Run batch program to create Event
  
--exec SF_ColCompare 'Insert','sl_superion_prod ', 'Event_Tritech_SFDC_load_VIP_Account' 

--check column names
  
--Exec SF_BulkOps 'Insert:batchsize(1)','sl_superion_prod ','Event_Tritech_SFDC_load_VIP_Account'
/*
Salesforce object Event does not contain column AccountID_orig
Salesforce object Event does not contain column CreatedbyId_orig
Salesforce object Event does not contain column CreatedbyId_target
Salesforce object Event does not contain column ID_orig
Salesforce object Event does not contain column IsArchived_orig
Salesforce object Event does not contain column IsDeleted_orig
Salesforce object Event does not contain column OwnerId_orig
Salesforce object Event does not contain column ownerId_target
Salesforce object Event does not contain column RecurrenceActivityID_orig
Salesforce object Event does not contain column whatID_orig
Salesforce object Event does not contain column whatid_parent_object_name
Salesforce object Event does not contain column whatid_legacy_id_orig
Salesforce object Event does not contain column whoid_orig
Salesforce object Event does not contain column whoId_parent_object_name
Salesforce object Event does not contain column whoId_legacy_id_orig
Salesforce object Event does not contain column Logged_Wellness_Check__c
Salesforce object Event does not contain column Description__orig
Salesforce object Event does not contain column Description_Comments_summary__C_orig
Salesforce object Event does not contain column Description_Task_notes__C_orig
Salesforce object Event does not contain column superion_Eventid
Salesforce object Event does not contain column superion_legacyid
Column AccountId is not insertable into the salesforce object Event*/

-- Logged_Wellness_Check__c 
select error,count(*) from Event_Tritech_SFDC_load_VIP_Account
group by error


select count(*) from Event_Tritech_SFDC_load_VIP_Account 
where error<>'Operation Successful.'
--2
