 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Representative__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

 /*
Use Tritech_PROD
EXEC SF_Refresh 'EN_Tritech_PROD','BGIntegration__Representative__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Representative__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta2;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,BGIntegration__BomgarSession__c_orig      =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c           =  bs.Id

,BGIntegration__Display_Name__c            =  bg.BGIntegration__Display_Name__c
,BGIntegration__Display_Number__c          =  bg.BGIntegration__Display_Number__c
,BGIntegration__Embassy__c                 =  bg.BGIntegration__Embassy__c
,BGIntegration__GS_Number__c               =  bg.BGIntegration__GS_Number__c
,BGIntegration__Hostname__c                =  bg.BGIntegration__Hostname__c
,BGIntegration__Invited__c                 =  bg.BGIntegration__Invited__c
,BGIntegration__Operating_System__c        =  bg.BGIntegration__Operating_System__c
,BGIntegration__Owner__c                   =  bg.BGIntegration__Owner__c 
,BGIntegration__Primary__c                 =  bg.BGIntegration__Primary__c
,BGIntegration__Private_Display_Name__c    =  bg.BGIntegration__Private_Display_Name__c
,BGIntegration__Private_IP__c              =  bg.BGIntegration__Private_IP__c
,BGIntegration__Public_Display_Name__c     =  bg.BGIntegration__Public_Display_Name__c
,BGIntegration__Public_IP__c               =  bg.BGIntegration__Public_IP__c
,BGIntegration__Representative_ID__c       =  bg.BGIntegration__Representative_ID__c
,BGIntegration__Seconds_Involved__c        =  bg.BGIntegration__Seconds_Involved__c

,CreatedById_orig                          =  bg.CreatedById
,CreatedById                               =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                               =  bg.CreatedDate
--,LegacyID__c                             =  Id
,Name                                      =  bg.[Name]
,TargetId                                  =  trgt.ID
,LastModifiedDate_orig                     =  bg.LastModifiedDate
,LastModifiedById_orig                     =  trgt.LastModifiedById

into BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta2

from Tritech_PROD.dbo.BGIntegration__Representative__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

--For Delta load
left join Superion_PRODUCTION.dbo.BGIntegration__Representative__c trgt on
trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(133232 row(s) affected)

-----------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Representative__c;--133232

Select count(*) from BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta2;--133232

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta2
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta2;

Select * into 
BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta2
from BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta2
where TargetId IS NULL; --(65 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta2
where CreatedById is null ;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta2' 

/*
Salesforce object BGIntegration__Representative__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Representative__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Representative__c does not contain column TargetId
Salesforce object BGIntegration__Representative__c does not contain column LastModifiedDate_orig
Salesforce object BGIntegration__Representative__c does not contain column LastModifiedById_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta2' --(6:30)

----------------------------------------------------------------------------------------


--Drop table Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Update_Delta2;

Use Staging_PROD;

Select a.LastModifiedDate_orig,c.LastModifiedDate_orig, a.*,b.LastModifiedById LastModifiedById_sup 
--into BGIntegration__Representative__c_Tritech_SFDC_Update_Delta2
from BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta2 a
left outer join Superion_Production.dbo.BGIntegration__Representative__c b
on a.TargetId=b.id
left outer join BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta2 c
on a.legacy_id__C=c.legacy_id__C
where a.TargetId is NOT NULL and a.LastModifiedDate_orig>='30-MAR-2019'
and b.LastModifiedById='0056A0000025rGcQAI' and c.LastModifiedDate_orig<>a.LastModifiedDate_orig--0

Select * from Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Update_Delta2
where CreatedById is null ;

--Update a set Id=TargetId
from BGIntegration__Representative__c_Tritech_SFDC_Update_Delta2 a;

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'BGIntegration__Representative__c_Tritech_SFDC_Update_Delta2' 

/*
Salesforce object BGIntegration__Representative__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Representative__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Representative__c does not contain column TargetId
Salesforce object BGIntegration__Representative__c does not contain column LastModifiedDate_orig
Column BGIntegration__BomgarSession__c is not updateable in the salesforce object BGIntegration__Representative__c
Column CreatedById is not updateable in the salesforce object BGIntegration__Representative__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__Representative__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__Representative__c_Tritech_SFDC_Update_Delta2'