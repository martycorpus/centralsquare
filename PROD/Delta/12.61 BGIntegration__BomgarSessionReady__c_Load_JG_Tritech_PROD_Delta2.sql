/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__BomgarSessionReady__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Refresh 'EN_Tritech_PROD','BGIntegration__BomgarSessionReady__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSessionReady__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload_Delta2;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__CustomerExitSurveyReady__c  =  bg.BGIntegration__CustomerExitSurveyReady__c
,BGIntegration__LSID__c                     =  bg.BGIntegration__LSID__c
,BGIntegration__RepExitSurveyReady__c       =  bg.BGIntegration__RepExitSurveyReady__c
,BGIntegration__SessionReady__c             =  bg.BGIntegration__SessionReady__c

,CreatedById_orig                           =  bg.CreatedById
,CreatedById                                =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                                =  bg.CreatedDate
--,Legacy_ID__c                               =  Id
,Name                                       =  bg.[Name]
,TargetId                                  =  trgt.ID
,LastModifiedDate_orig                     =  bg.LastModifiedDate
,LastModifiedById_sup                      =  trgt.LastModifiedById

,OwnerId_orig                               =  bg.OwnerId
,OwnerId                                    =  IIF(owneruser.Id IS NULL,@DefaultUser,owneruser.Id)

 into BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload_Delta2

from Tritech_PROD.dbo.BGIntegration__BomgarSessionReady__c bg 

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching OwnerId(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] owneruser 
on owneruser.Legacy_Tritech_Id__c=bg.OwnerId

--For Delta load
left join Superion_PRODUCTION.dbo.BGIntegration__BomgarSessionReady__c trgt on
trgt.Legacy_Id__c=bg.Id

;--(126924 row(s) affected)

----------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__BomgarSessionReady__c;--126924

Select count(*) from BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload_Delta2;--126924

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload_Delta2
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Insert_Delta2;

Select * into 
BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Insert_Delta2
from BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload_Delta2
where TargetId IS NULL; --(60 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Insert_Delta2
where error<>'Operation Successful.';


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Insert_Delta2' 

/*
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column CreatedById_orig
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column TargetId
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column LastModifiedDate_orig
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column LastModifiedById_sup
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column OwnerId_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Insert_Delta2'

----------------------------------------------------------------------------------------

--Drop table Staging_PROD.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Update_Delta2;

Use Staging_PROD;

Select a.LastModifiedDate_orig,c.LastModifiedDate_orig, a.*,b.LastModifiedById LastModifiedById_sup 
--into BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Update_Delta2
from BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload_Delta2 a
left outer join Superion_Production.dbo.BGIntegration__BomgarSessionReady__c b
on a.TargetId=b.id
left outer join BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Preload_Delta2 c
on a.legacy_id__C=c.legacy_id__C
where a.TargetId is NOT NULL and a.LastModifiedDate_orig>='30-MAR-2019'
and b.LastModifiedById='0056A0000025rGcQAI' and c.LastModifiedDate_orig<>a.LastModifiedDate_orig--0 

--Update a set Id=TargetId
from BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Update_Delta2 a;--5

Select * from Staging_PROD.dbo.BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Update_Delta2;



--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Update_Delta2' 

/*
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column CreatedById_orig
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column TargetId
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column LastModifiedDate_orig
Salesforce object BGIntegration__BomgarSessionReady__c does not contain column OwnerId_orig
Column CreatedById is not updateable in the salesforce object BGIntegration__BomgarSessionReady__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__BomgarSessionReady__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__BomgarSessionReady__c_Tritech_SFDC_Update_Delta2'