 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Representative__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

 /*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','BGIntegration__Representative__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Representative__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,BGIntegration__BomgarSession__c_orig      =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c           =  bs.Id

,BGIntegration__Display_Name__c            =  bg.BGIntegration__Display_Name__c
,BGIntegration__Display_Number__c          =  bg.BGIntegration__Display_Number__c
,BGIntegration__Embassy__c                 =  bg.BGIntegration__Embassy__c
,BGIntegration__GS_Number__c               =  bg.BGIntegration__GS_Number__c
,BGIntegration__Hostname__c                =  bg.BGIntegration__Hostname__c
,BGIntegration__Invited__c                 =  bg.BGIntegration__Invited__c
,BGIntegration__Operating_System__c        =  bg.BGIntegration__Operating_System__c
,BGIntegration__Owner__c                   =  bg.BGIntegration__Owner__c 
,BGIntegration__Primary__c                 =  bg.BGIntegration__Primary__c
,BGIntegration__Private_Display_Name__c    =  bg.BGIntegration__Private_Display_Name__c
,BGIntegration__Private_IP__c              =  bg.BGIntegration__Private_IP__c
,BGIntegration__Public_Display_Name__c     =  bg.BGIntegration__Public_Display_Name__c
,BGIntegration__Public_IP__c               =  bg.BGIntegration__Public_IP__c
,BGIntegration__Representative_ID__c       =  bg.BGIntegration__Representative_ID__c
,BGIntegration__Seconds_Involved__c        =  bg.BGIntegration__Seconds_Involved__c

,CreatedById_orig                          =  bg.CreatedById
,CreatedById                               =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                               =  bg.CreatedDate
--,LegacyID__c                             =  Id
,Name                                      =  bg.[Name]
,TargetId                                  =  trgt.ID
,LastModifiedDate_orig                     =  bg.LastModifiedDate

into BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.BGIntegration__Representative__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

--For Delta load
left join Superion_PRODUCTION.dbo.BGIntegration__Representative__c trgt on
trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(133167 row(s) affected)

-----------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Representative__c;--133167

Select count(*) from BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta;--133167

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta;

Select * into 
BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta
from BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta
where TargetId IS NULL; --(1516 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta
where CreatedById is null ;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object BGIntegration__Representative__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Representative__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Representative__c does not contain column TargetId
Salesforce object BGIntegration__Representative__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Representative__c_Tritech_SFDC_Insert_Delta' --(6:30)

----------------------------------------------------------------------------------------


--Drop table Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Update_Delta;

Select * into 
BGIntegration__Representative__c_Tritech_SFDC_Update_Delta
from BGIntegration__Representative__c_Tritech_SFDC_Preload_Delta
where TargetId IS NOT NULL and LastModifiedDate_orig>='25-MAR-2019'; --(5 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Representative__c_Tritech_SFDC_Update_Delta
where CreatedById is null ;

--Update a set Id=TargetId
from BGIntegration__Representative__c_Tritech_SFDC_Update_Delta a;

--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'BGIntegration__Representative__c_Tritech_SFDC_Update_Delta' 

/*
Salesforce object BGIntegration__Representative__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Representative__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Representative__c does not contain column TargetId
Salesforce object BGIntegration__Representative__c does not contain column LastModifiedDate_orig
Column BGIntegration__BomgarSession__c is not updateable in the salesforce object BGIntegration__Representative__c
Column CreatedById is not updateable in the salesforce object BGIntegration__Representative__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__Representative__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__Representative__c_Tritech_SFDC_Update_Delta'