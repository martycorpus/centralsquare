 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Recording__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','BGIntegration__Recording__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Recording__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Recording__c_Tritech_SFDC_Preload_Delta;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');
 
 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__BomgarSession__c_orig   =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c   =  bs.Id

,BGIntegration__Download_URL__c    =  bg.BGIntegration__Download_URL__c
,BGIntegration__Instance__c        =  bg.BGIntegration__Instance__c
,BGIntegration__Recording_Type__c  =  bg.BGIntegration__Recording_Type__c
,BGIntegration__View_URL__c        =  bg.BGIntegration__View_URL__c

,CreatedById_orig                  =  bg.CreatedById
,CreatedById                       =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                       =  bg.CreatedDate
--,LegacyID__c                       =  Id
,TargetId                                  =  trgt.ID
,LastModifiedDate_orig                     =  bg.LastModifiedDate

into BGIntegration__Recording__c_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.BGIntegration__Recording__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

----For Delta Load
left join Superion_PRODUCTION.dbo.BGIntegration__Recording__c trgt
on trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(122738 row(s) affected)

--------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Recording__c;--122738

Select count(*) from BGIntegration__Recording__c_Tritech_SFDC_Preload_Delta;--122738

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Recording__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Recording__c_Tritech_SFDC_Insert_Delta;

Select * into 
BGIntegration__Recording__c_Tritech_SFDC_Insert_Delta
from BGIntegration__Recording__c_Tritech_SFDC_Preload_Delta
where TargetId IS NULL; --(1365 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Recording__c_Tritech_SFDC_Insert_Delta;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'BGIntegration__Recording__c_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object BGIntegration__Recording__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Recording__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Recording__c does not contain column TargetId
Salesforce object BGIntegration__Recording__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Recording__c_Tritech_SFDC_Insert_Delta' --(5:24)

----------------------------------------------------------------------------------------


--Drop table Staging_PROD.dbo.BGIntegration__Recording__c_Tritech_SFDC_Update_Delta;

Select * into 
BGIntegration__Recording__c_Tritech_SFDC_Update_Delta
from BGIntegration__Recording__c_Tritech_SFDC_Preload_Delta
where TargetId IS NOT NULL AND LastModifiedDate_orig>='25-MAR-2019'; --(5 row(s) affected)

--Update a set Id=TargetId
from BGIntegration__Recording__c_Tritech_SFDC_Update_Delta a;

Select * from Staging_PROD.dbo.BGIntegration__Recording__c_Tritech_SFDC_Update_Delta;


--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'BGIntegration__Recording__c_Tritech_SFDC_Update_Delta' 

/*
Salesforce object BGIntegration__Recording__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Recording__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Recording__c does not contain column TargetId
Salesforce object BGIntegration__Recording__c does not contain column LastModifiedDate_orig
Column BGIntegration__BomgarSession__c is not updateable in the salesforce object BGIntegration__Recording__c
Column CreatedById is not updateable in the salesforce object BGIntegration__Recording__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__Recording__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__Recording__c_Tritech_SFDC_Update_Delta'