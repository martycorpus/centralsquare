/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Custom_Attribute__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','BGIntegration__Custom_Attribute__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Custom_Attribute__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload_Delta;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'

,BGIntegration__BomgarSession__c_orig  =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c  =  bs.Id

,BGIntegration__Code__c           =  bg.BGIntegration__Code__c
,BGIntegration__Value__c          =  bg.BGIntegration__Value__c

,CreatedById_orig                 =  bg.CreatedById
,CreatedById                      =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                      =  bg.CreatedDate
--,Legacy_ID__c                     =  Id
,Name                             =  bg.Name
,TargetId                         =  trgt.ID
,LastModifiedDate_orig            =  bg.LastModifiedDate

into BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload_Delta

from 
Tritech_PROD.dbo.BGIntegration__Custom_Attribute__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

left join Superion_PRODUCTION.dbo.BGIntegration__Custom_Attribute__c trgt 
on trgt.Legacy_Id__c = bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(116382 row(s) affected)

----------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Custom_Attribute__c;--116382

Select count(*) from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload_Delta;--116382

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Insert_Delta;

Select * into 
BGIntegration__Custom_Attribute__c_Tritech_SFDC_Insert_Delta
from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload_Delta
where TargetId IS NULL; --(1151 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Insert_Delta;


--Exec SF_ColCompare_test 'Insert','SL_SUPERION_PROD', 'BGIntegration__Custom_Attribute__c_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object BGIntegration__Custom_Attribute__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Custom_Attribute__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Custom_Attribute__c does not contain column TargetId
Salesforce object BGIntegration__Custom_Attribute__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Custom_Attribute__c_Tritech_SFDC_Insert_Delta' --(5:49)

----------------------------------------------------------------------------------------

--Drop table Staging_PROD.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Update_Delta;

Select * into 
BGIntegration__Custom_Attribute__c_Tritech_SFDC_Update_Delta
from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Preload_Delta
where TargetId IS NOT NULL AND LastModifiedDate_orig>='25-MAR-2019'; --(4 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Custom_Attribute__c_Tritech_SFDC_Update_Delta;

--Update a set Id=TargetId
from BGIntegration__Custom_Attribute__c_Tritech_SFDC_Update_Delta a;--4

--Exec SF_ColCompare_test 'Update','SL_SUPERION_PROD', 'BGIntegration__Custom_Attribute__c_Tritech_SFDC_Update_Delta' 

/*
Salesforce object BGIntegration__Custom_Attribute__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Custom_Attribute__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Custom_Attribute__c does not contain column TargetId
Salesforce object BGIntegration__Custom_Attribute__c does not contain column LastModifiedDate_orig
Column BGIntegration__BomgarSession__c is not updateable in the salesforce object BGIntegration__Custom_Attribute__c
Column CreatedById is not updateable in the salesforce object BGIntegration__Custom_Attribute__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__Custom_Attribute__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__Custom_Attribute__c_Tritech_SFDC_Update_Delta'