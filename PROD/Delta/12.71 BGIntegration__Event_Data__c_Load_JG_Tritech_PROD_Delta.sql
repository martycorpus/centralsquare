 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Event_Data__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','BGIntegration__Event_Data__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Replicate 'SL_SUPERION_PROD','User'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Event__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Event_Data__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Event_Data__c_Tritech_SFDC_Preload_Delta;
 
 DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

 Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__Event__c_orig  =  bg.BGIntegration__Event__c
,BGIntegration__Event__c  =  evnt.Id

,BGIntegration__Value__c  =  bg.BGIntegration__Value__c

,CreatedById_orig         =  bg.CreatedById
,CreatedById              =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate              =  bg.CreatedDate
--,LegacyID__c              =  Id
,Name                     =  bg.[Name]
,TargetId                                  =  trgt.ID
,LastModifiedDate_orig                     =  bg.LastModifiedDate

into BGIntegration__Event_Data__c_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.BGIntegration__Event_Data__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BGIntegrationEventId(BGIntegration__Event__c Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__Event__c evnt
on evnt.Legacy_id__c=bg.BGIntegration__Event__c

--For Delta Load
left join Superion_PRODUCTION.dbo.BGIntegration__Event_Data__c trgt on
trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

;--(3726846 row(s) affected)

-------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Event_Data__c;--3726846

Select count(*) from BGIntegration__Event_Data__c_Tritech_SFDC_Preload_Delta;--3726846

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Insert_Delta;

Select * into 
BGIntegration__Event_Data__c_Tritech_SFDC_Insert_Delta
from BGIntegration__Event_Data__c_Tritech_SFDC_Preload_Delta
where TargetId IS NULL; --(41924 row(s) affected)

Select count(*) from Staging_PROD.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Insert_Delta;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'BGIntegration__Event_Data__c_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object BGIntegration__Event_Data__c does not contain column BGIntegration__Event__c_orig
Salesforce object BGIntegration__Event_Data__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Event_Data__c does not contain column TargetId
Salesforce object BGIntegration__Event_Data__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Event_Data__c_Tritech_SFDC_Insert_Delta' 

--------------------------------------------------------------------
--Drop table Staging_PROD.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Update_Delta;

Select * into 
BGIntegration__Event_Data__c_Tritech_SFDC_Update_Delta
from BGIntegration__Event_Data__c_Tritech_SFDC_Preload_Delta
where TargetId IS NOT NULL AND LastModifiedDate_orig>='25-MAR-2019'; --(92 row(s) affected)

--Update a set Id=TargetId
from BGIntegration__Event_Data__c_Tritech_SFDC_Update_Delta a;--92

Select * from Staging_PROD.dbo.BGIntegration__Event_Data__c_Tritech_SFDC_Update_Delta;


--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'BGIntegration__Event_Data__c_Tritech_SFDC_Update_Delta' 

/*
Salesforce object BGIntegration__Event_Data__c does not contain column BGIntegration__Event__c_orig
Salesforce object BGIntegration__Event_Data__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Event_Data__c does not contain column TargetId
Salesforce object BGIntegration__Event_Data__c does not contain column LastModifiedDate_orig
Column BGIntegration__Event__c is not updateable in the salesforce object BGIntegration__Event_Data__c
Column CreatedById is not updateable in the salesforce object BGIntegration__Event_Data__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__Event_Data__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__Event_Data__c_Tritech_SFDC_Update_Delta' 


------------------------------------------------------------------------
select *, ntile(4) over(order by Legacy_id__c) series
into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
from BGIntegration__Event_Data__c_Tritech_SFDC_Load a;--3669033

--Series1
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_1 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=1;--917259

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_1'

--Series2
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_2 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=2;--917258

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_2'

--Series3
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_3 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=3;--917258

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_3'

--Series4
select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_4 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
where series=4;--917258

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_4'

--Drop table BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series;

--Select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_1;

--Insert into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_2;

--Insert into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_3;

--Insert into BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series
Select * from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series_4;

Select count(*) from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series;--3669033

--Drop table BGIntegration__Event_Data__c_Tritech_SFDC_Load;

--Select * into BGIntegration__Event_Data__c_Tritech_SFDC_Load
 from BGIntegration__Event_Data__c_Tritech_SFDC_Load_with_Series;

 Select count(*) from BGIntegration__Event_Data__c_Tritech_SFDC_Load;--3669033
----------------------------------------------------------------------------------------
