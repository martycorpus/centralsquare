  /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Customer__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */
 
  /*
Use Tritech_PROD
EXEC SF_Refresh 'EN_Tritech_PROD','BGIntegration__Customer__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Replicate 'SL_SUPERION_PROD','User'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Customer__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Customer__c_Tritech_SFDC_Preload_Delta2;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

 Select 

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__BomgarSession__c_orig   =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c        =  bs.Id

,BGIntegration__Company__c              =  bg.BGIntegration__Company__c
,BGIntegration__Company_Code__c         =  bg.BGIntegration__Company_Code__c
,BGIntegration__Details__c              =  bg.BGIntegration__Details__c
,BGIntegration__GS_Number__c            =  bg.BGIntegration__GS_Number__c
,BGIntegration__Hostname__c             =  bg.BGIntegration__Hostname__c 
,BGIntegration__Issue__c                =  bg.BGIntegration__Issue__c
,BGIntegration__Operating_System__c     =  bg.BGIntegration__Operating_System__c
,BGIntegration__Primary__c              =  bg.BGIntegration__Primary__c
,BGIntegration__Private_IP__c           =  bg.BGIntegration__Private_IP__c
,BGIntegration__Public_IP__c            =  bg.BGIntegration__Public_IP__c
,BGIntegration__Username__c             =  bg.BGIntegration__Username__c

,CreatedById_orig                       =  bg.CreatedById
,CreatedById                            =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                            =  bg.CreatedDate
--,Legacy_ID__c                         =  Id
,Name                                   =  bg.[Name]
,TargetId                               =  trgt.ID
,LastModifiedDate_orig                  =  bg.LastModifiedDate

into BGIntegration__Customer__c_Tritech_SFDC_Preload_Delta2

from Tritech_PROD.dbo.BGIntegration__Customer__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

----For DeltaLoad
left join Superion_PRODUCTION.dbo.BGIntegration__Customer__c trgt
on trgt.Legacy_ID__c =bg.Id

--where trgt.Legacy_ID__c IS NULL

;--(126922 row(s) affected)

---------------------------------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Customer__c;--126922

Select count(*) from BGIntegration__Customer__c_Tritech_SFDC_Preload_Delta2;--126922

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Customer__c_Tritech_SFDC_Preload_Delta2
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Customer__c_Tritech_SFDC_Insert_Delta2;

Select * into 
BGIntegration__Customer__c_Tritech_SFDC_Insert_Delta2
from BGIntegration__Customer__c_Tritech_SFDC_Preload_Delta2
where TargetId is NULL; --59

Select * from Staging_PROD.dbo.BGIntegration__Customer__c_Tritech_SFDC_Insert_Delta2;


--Exec SF_ColCompare_test 'Insert','SL_SUPERION_PROD', 'BGIntegration__Customer__c_Tritech_SFDC_Insert_Delta2' 

/*
Salesforce object BGIntegration__Customer__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Customer__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Customer__c does not contain column TargetId
Salesforce object BGIntegration__Customer__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Customer__c_Tritech_SFDC_Insert_Delta2' --(:)

----------------------------------------------------------------------------------------


--Drop table Staging_PROD.dbo.BGIntegration__Customer__c_Tritech_SFDC_Update_Delta2;

Use Staging_PROD;

Select a.LastModifiedDate_orig,c.LastModifiedDate_orig, a.*,b.LastModifiedById LastModifiedById_sup 
--into BGIntegration__Customer__c_Tritech_SFDC_Update_Delta2
from BGIntegration__Customer__c_Tritech_SFDC_Preload_Delta2 a
left outer join Superion_Production.dbo.BGIntegration__Customer__c b
on a.TargetId=b.id
left outer join BGIntegration__Customer__c_Tritech_SFDC_preload_Delta c
on a.legacy_id__C=c.legacy_id__C
where a.TargetId is NOT NULL and a.LastModifiedDate_orig>='30-MAR-2019'
and b.LastModifiedById='0056A0000025rGcQAI' and c.LastModifiedDate_orig<>a.LastModifiedDate_orig--0

Select * from Staging_PROD.dbo.BGIntegration__Customer__c_Tritech_SFDC_Update_Delta2;

--Update a set Id=TargetId
from BGIntegration__Customer__c_Tritech_SFDC_Update_Delta2 a;--5


--Exec SF_ColCompare_test 'Update','SL_SUPERION_PROD', 'BGIntegration__Customer__c_Tritech_SFDC_Update_Delta2' 

/*
Salesforce object BGIntegration__Customer__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Customer__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Customer__c does not contain column TargetId
Salesforce object BGIntegration__Customer__c does not contain column LastModifiedDate_orig
Column BGIntegration__BomgarSession__c is not updateable in the salesforce object BGIntegration__Customer__c
Column CreatedById is not updateable in the salesforce object BGIntegration__Customer__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__Customer__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__Customer__c_Tritech_SFDC_Update_Delta2' --(:)
