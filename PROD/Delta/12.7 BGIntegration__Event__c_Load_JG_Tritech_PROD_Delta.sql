 /*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Event__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','BGIntegration__Event__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Event__c','Yes'
*/ 

--2018-12-06 Marty Executed for UAT2 LOAD.
 --Exec SF_ColCompare 'Insert','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_Load' 
 --- Starting SF_ColCompare V3.6.9
--Problems found with BGIntegration__Event__c_Tritech_SFDC_Load. See output table for details.
--Msg 50000, Level 16, State 1, Procedure SF_ColCompare, Line 134 [Batch Start Line 25]
--- Ending SF_ColCompare. Operation FAILED.
--ErrorDesc
--Salesforce object BGIntegration__Event__c does not contain column BGIntegration__BomgarSession__c_orig
--Salesforce object BGIntegration__Event__c does not contain column CreatedById_orig

--select count(*) from BGIntegration__Event__c_Tritech_SFDC_Load --2,457,573

--Exec SF_BulkOps 'Insert:bulkapi,batchsize(10000)','SL_Superion_FullSB','BGIntegration__Event__c_Tritech_SFDC_Load'
--bulkapi job id = 7500v000003kEtt

--00:41:05: Job still running.
--00:42:05: Job Complete.
--00:42:05: Retrieving Job Status.
--01:22:39: 2457573 rows read from SQL Table.
--01:22:39: 2457573 rows successfully processed.
--01:22:39: 0 rows failed.
--01:22:39: 2457573 rows marked with current status.
--01:22:50: Percent Failed = 0.000.
----- Ending SF_BulkOps. Operation successful. 02:33:39

Use Staging_PROD;

--Drop table BGIntegration__Event__c_Tritech_SFDC_Preload_Delta;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

Select

ID = CAST(SPACE(18) as NVARCHAR(18)),
ERROR = CAST(SPACE(255) as NVARCHAR(255)),
Legacy_id__c =bg.Id, 
Legacy_Source_System__c='Tritech',
Migrated_Record__c='True',

BGIntegration__Body__c                    =  bg.BGIntegration__Body__c,

BGIntegration__BomgarSession__c_orig      =  bg.BGIntegration__BomgarSession__c,
BGIntegration__BomgarSession__c           =  bs.Id,

BGIntegration__Destination__c             =  bg.BGIntegration__Destination__c,
BGIntegration__Destination_GS_Number__c   =  bg.BGIntegration__Destination_GS_Number__c,
BGIntegration__Destination_Type__c        =  bg.BGIntegration__Destination_Type__c,
BGIntegration__File_Name__c               =  bg.BGIntegration__File_Name__c,
BGIntegration__File_Size__c               =  bg.BGIntegration__File_Size__c,
BGIntegration__Number__c                  =  bg.BGIntegration__Number__c,
BGIntegration__Performed_By__c            =  bg.BGIntegration__Performed_By__c,
BGIntegration__Performed_By_GS_Number__c  =  bg.BGIntegration__Performed_By_GS_Number__c,
BGIntegration__Performed_By_Type__c       =  bg.BGIntegration__Performed_By_Type__c,
BGIntegration__Timestamp__c               =  bg.BGIntegration__Timestamp__c,

CreatedById_orig                          =  bg.CreatedById,
CreatedById                               =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id),

CreatedDate                               =  bg.CreatedDate,
--Legacy_ID__c                              =  bg.Id,
Name                                      =  bg.[Name]
,TargetId                                  =  trgt.ID
,LastModifiedDate_orig                     =  bg.LastModifiedDate

into BGIntegration__Event__c_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.BGIntegration__Event__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

--For Delta Load
left join Superion_PRODUCTION.dbo.BGIntegration__Event__c trgt on
trgt.Legacy_Id__c=bg.Id

--where trgt.Legacy_Id__c IS NULL

; --(3026111 row(s) affected)

---------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Event__c;--3026111

Select count(*) from BGIntegration__Event__c_Tritech_SFDC_Preload_Delta;--3026111

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Event__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Event__c_Tritech_SFDC_Insert_Delta;

Select * into 
BGIntegration__Event__c_Tritech_SFDC_Insert_Delta
from BGIntegration__Event__c_Tritech_SFDC_Preload_Delta
where TargetId IS NULL; --(33789 row(s) affected)

Select * from Staging_PROD.dbo.BGIntegration__Event__c_Tritech_SFDC_Insert_Delta 
where  BGIntegration__BomgarSession__c IS NULL;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'BGIntegration__Event__c_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object BGIntegration__Event__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Event__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Event__c does not contain column TargetId
Salesforce object BGIntegration__Event__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Event__c_Tritech_SFDC_Insert_Delta' 
-------------------------------------------------------


--Drop table Staging_PROD.dbo.BGIntegration__Event__c_Tritech_SFDC_Update_Delta;

Select * into 
BGIntegration__Event__c_Tritech_SFDC_Update_Delta
from BGIntegration__Event__c_Tritech_SFDC_Preload_Delta
where TargetId IS NOT NULL AND LastModifiedDate_orig>='25-MAR-2019'; --(74 row(s) affected)

--Update a set Id=TargetId
from BGIntegration__Event__c_Tritech_SFDC_Update_Delta a;--74

Select * from Staging_PROD.dbo.BGIntegration__Event__c_Tritech_SFDC_Update_Delta 
where  BGIntegration__BomgarSession__c IS NULL;


--Exec SF_ColCompare 'Update','SL_SUPERION_PROD', 'BGIntegration__Event__c_Tritech_SFDC_Update_Delta' 

/*
Salesforce object BGIntegration__Event__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Event__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Event__c does not contain column TargetId
Salesforce object BGIntegration__Event__c does not contain column LastModifiedDate_orig
Column BGIntegration__BomgarSession__c is not updateable in the salesforce object BGIntegration__Event__c
Column CreatedById is not updateable in the salesforce object BGIntegration__Event__c
Column CreatedDate is not updateable in the salesforce object BGIntegration__Event__c
*/

--Exec SF_BulkOps 'Update','SL_SUPERION_PROD','BGIntegration__Event__c_Tritech_SFDC_Update_Delta' 