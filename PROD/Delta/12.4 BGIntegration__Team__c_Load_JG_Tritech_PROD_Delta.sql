/*
-- Author       : Jagan	
-- Created Date : 06-Nov-18
-- Modified Date: 
-- Modified By  :   
-- Description  : 

-----------Tritech BGIntegration__Team__c----------
Requirement No  : REQ-0877
Total Records   :  
Scope: Migrate all records.
 */

/*
Use Tritech_PROD
EXEC SF_Refresh 'MC_Tritech_PROD','BGIntegration__Team__c','Yes'

Use SUPERION_PRODUCTION
EXEC SF_Refresh 'SL_SUPERION_PROD','User','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__BomgarSession__c','Yes'
EXEC SF_Refresh 'SL_SUPERION_PROD','BGIntegration__Team__c','Yes'
*/ 

Use Staging_PROD;

--Drop table BGIntegration__Team__c_Tritech_SFDC_Preload_Delta;

DECLARE @DefaultUser NVARCHAR(18) = (Select top 1 Id from SUPERION_PRODUCTION.dbo.[User] 
where Name like 'Bomgar Site Guest User');

 Select

 ID = CAST(SPACE(18) as NVARCHAR(18))
,ERROR = CAST(SPACE(255) as NVARCHAR(255))
,Legacy_id__c =bg.Id 
,Legacy_Source_System__c='Tritech'
,Migrated_Record__c='True'
 
,BGIntegration__BomgarSession__c_orig  =  bg.BGIntegration__BomgarSession__c
,BGIntegration__BomgarSession__c  =  bs.Id

,BGIntegration__Primary__c        =  bg.BGIntegration__Primary__c
,BGIntegration__Team_ID__c        =  bg.BGIntegration__Team_ID__c

,CreatedById_orig                 =  bg.CreatedById
,CreatedById                      =  IIF(createdbyuser.Id IS NULL,@DefaultUser,createdbyuser.Id)

,CreatedDate                      =  bg.CreatedDate
--,LegacyID__c                      =  Id
,Name                             =  bg.[Name]
,TargetId                         =  trgt.ID
,LastModifiedDate_orig            =  bg.LastModifiedDate

into BGIntegration__Team__c_Tritech_SFDC_Preload_Delta

from Tritech_PROD.dbo.BGIntegration__Team__c bg

--Fetching CreatedById(UserLookup)
left join SUPERION_PRODUCTION.dbo.[User] createdbyuser 
on createdbyuser.Legacy_Tritech_Id__c=bg.CreatedById

--Fetching BomgarSessionId (BomgarSession Lookup)
left join SUPERION_PRODUCTION.dbo.BGIntegration__BomgarSession__c bs
on  bs.Legacy_Id__c=bg.BGIntegration__BomgarSession__c

--For Delta load
left join Superion_PRODUCTION.dbo.BGIntegration__Team__c trgt on
trgt.Legacy_Id__c=bg.Id

;--(299 row(s) affected)

-------------------------------------------------------------------------

Select count(*) from Tritech_PROD.dbo.BGIntegration__Team__c;--299

Select count(*) from BGIntegration__Team__c_Tritech_SFDC_Preload_Delta;--299

Select Legacy_Id__c,count(*) from Staging_PROD.dbo.BGIntegration__Team__c_Tritech_SFDC_Preload_Delta
group by Legacy_Id__c
having count(*)>1; --0

--Drop table Staging_PROD.dbo.BGIntegration__Team__c_Tritech_SFDC_Insert_Delta;

Select * into 
BGIntegration__Team__c_Tritech_SFDC_Insert_Delta
from BGIntegration__Team__c_Tritech_SFDC_Preload_Delta
where TargetId IS NULL; --2

Select * from Staging_PROD.dbo.BGIntegration__Team__c_Tritech_SFDC_Insert_Delta 
where CreatedById is null;


--Exec SF_ColCompare 'Insert','SL_SUPERION_PROD', 'BGIntegration__Team__c_Tritech_SFDC_Insert_Delta' 

/*
Salesforce object BGIntegration__Team__c does not contain column BGIntegration__BomgarSession__c_orig
Salesforce object BGIntegration__Team__c does not contain column CreatedById_orig
Salesforce object BGIntegration__Team__c does not contain column TargetId
Salesforce object BGIntegration__Team__c does not contain column LastModifiedDate_orig
*/

--Exec SF_BulkOps 'Insert','SL_SUPERION_PROD','BGIntegration__Team__c_Tritech_SFDC_Insert_Delta' --(0:01)

----------------------------------------------------------------------------------------

--Drop table Staging_PROD.dbo.BGIntegration__Team__c_Tritech_SFDC_Update_Delta;

Select * into 
BGIntegration__Team__c_Tritech_SFDC_Update_Delta
from BGIntegration__Team__c_Tritech_SFDC_Preload_Delta
where TargetId IS NOT NULL AND LastModifiedDate_orig>='25-MAR-2019'; --0

